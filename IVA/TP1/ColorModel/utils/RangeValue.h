/*
 * RangeValue.h
 *
 *  Created on: 22 mai 2015
 *      Author: davidroussel
 */

#ifndef RANGEVALUE_H_
#define RANGEVALUE_H_

#include <utils/AbstractRangeValue.h>

/**
 * Bounded numeric value template.
 * RangeValue is a value contained in a range [minValue .. maxValue] with
 * a stepValue allowing to determine the next or the previous value
 * of the current value (within the range), by adding the step value.
 * 		- value should be within [min ... max]
 * 		- max should be reachable by value + (n * step)
 * 		- min should be reachable by value - (n * step)
 * 		- step should be positive and non null
 */
template <class T>
class RangeValue: public AbstractRangeValue<T>
{
	protected:
		/*
		 * Reminder of AbstractRangeValue<T> members used here
		 * This allow to use these members without AbstractRangeValue<T>::
		 * prefix.
		 */
		using AbstractRangeValue<T>::_value;
		using AbstractRangeValue<T>::_minValue;
		using AbstractRangeValue<T>::_maxValue;
		using AbstractRangeValue<T>::_stepValue;
		using AbstractRangeValue<T>::EPSILON;

	public:

		using AbstractRangeValue<T>::operator =; // weird !
		using typename AbstractRangeValue<T>::WhichValue;

		// --------------------------------------------------------------------
		// Constructors / destructor
		// --------------------------------------------------------------------
		/**
		 * Default constuctor
		 */
		RangeValue();

		/**
		 * Valued constructor with currents value, range values and step value.
		 * 	- minValue should be less than current value, otherwise it is set to
		 * 	the current value
		 * 	- maxValue should be greater than current value, otherwise it is set
		 * 	to the current value
		 * 	- stepValue should be such that the next value should
		 * 	be less than the max value and previous value should be greater than
		 * 	min value, otherwise actual step value is reduced to meet these
		 * 	conditions
		 * @note In any cases minValue, value & maxValue are eventually
		 * reorderded to ensure minValue < value < maxValue
		 * @param value the current value
		 * @param min the minimum possible value
		 * @param max the maximum possible value
		 * @param step the step value to get to the next/previous value
		 * [default is EPSILON]
		 */
		RangeValue(const T & value,
				   const T & min,
				   const T & max,
				   const T & step = AbstractRangeValue<T>::EPSILON);

		/**
		 * Valued constructor from tuple
		 * @param tupleValues tuple containing value, min, max & step
		 */
		RangeValue(const tuple<T, T, T, T> & tupleValues);

		/**
		 * Copy constructor
		 * @param arv the other RangeValue to copy
		 */
		RangeValue(const AbstractRangeValue<T> & arv);

		/**
		 * Move constructor
		 * @param arv the other RangeValue to move
		 */
		RangeValue(AbstractRangeValue<T> && arv);

		/**
		 * Destructor
		 */
		virtual ~RangeValue();

		// --------------------------------------------------------------------
		// RangeValue specific operations
		// --------------------------------------------------------------------
		/**
		 * number of steps in the range
		 * @return number of steps to get from min to max value
		 */
		size_t nbSteps() const;

		/**
		 * the index corresponding to the current value in terms of how many
		 * steps from minimum value.
		 * @return the number of steps from min value to current value
		 */
		size_t index() const;

		/**
		 * Current value setter.
		 * The new value has to be within min/max value range and if there is
		 * a step mode it should be accessible from the current value with a
		 * finite number of steps.
		 * @param value the new current value to set
		 * @return true if the new current value has been set, false otherwise
		 */
		bool setValue(const T & value);

		/**
		 * Any value setter.
		 * Relies on other specialized setters to set the value according to
		 * the "which" parameter.
		 * @param value the new value to set
		 * @param which an enum value indicating which value to set
		 * @return true if the new value has been set, false otherwise
		 */
		bool setValue(const T & value,
					  const WhichValue which);

		/**
		 * Current value setter to the closest possible value.
		 * if the new value is > max then the current value is set to max.
		 * if the new value is < min then the current value is set to min.
		 * if the new value is >= min & <= max it is set to the closest accessible
		 * value according to steps.
		 * @param value the new current value to set
		 * @return true if the value in argument has been set exactly, false if
		 * the new value is slightly different
		 * @post a new value has been set as the closest value to the argument
		 * @note This implementation is supposed to be more efficient than
		 * the one provided by
		 * AbstractRangeValue<T>::setClosestValue(const T & value) by computing
		 * the reachable value instead of trying to reach it iteratively
		 */
		bool setClosestValue(const T & value);

		/**
		 * Sets the value corresponding to this index, which means the value
		 * located at "index" steps distance from the min value.
		 * @param index the index to set value to
		 * @return true if the index was valid [i.e. corresponding to a value
		 * between min and max] and value has been set, false otherwise
		 */
		bool setIndex(const size_t index);

		/**
		 * Step value setter.
		 * The new step value should be such that the next value according to
		 * this new step value is less than the max value and the previous
		 * value should be greater than the min value
		 * @param value the new step value. if this value is <= EPSILON then
		 * this range turns its mode to NONE and can't fetch next or previous
		 * values anymore.
		 * @return true if the new max value has been set
		 */
		bool setStep(const T & value);

		/**
		 * Computes next value
		 * @return the new current value after reaching the next value according
		 * to _stepValue or _maxValue if next value is greater than _maxValue
		 */
		T next();

		/**
		 * Indicates this range has a next value < max value
		 * @return true if the next value is < max value and false otherwise or
		 * if the range mode is NONE
		 */
		bool hasNext() const;

		/**
		 * Computes previous value
		 * @return the new current value after reaching the previous value
		 * according to _stepValue or _minValue if previous value is less than
		 * _minValue
		 */
		T previous();

		/**
		 * Indicates this range has a previous value > min value
		 * @return true if the previous value is > min value and false
		 * otherwise or if the range mode is NONE
		 */
		bool hasPrevious() const;

		// --------------------------------------------------------------------
		// Operators
		// --------------------------------------------------------------------
		/**
		 * Comparison operator with another AbstractRangeValue
		 * @param rv another AbstractRangeValue
		 * @return true if both AbstractRangeValue have the same current, min, max, step
		 * and are of type RangeValue, false otherwise
		 */
		bool operator ==(const AbstractRangeValue<T> & rv) const;

		/**
		 * Comparison operator with another RangeValue
		 * @param rv another RangeValue
		 * @return true if both RangeValue have the same current, min, max,
		 * step, false otherwise
		 */
		virtual bool operator ==(const RangeValue<T> & rv) const;

		/**
		 * Difference operator with another RangeValue
		 * @param rv another RangeValue
		 * @return false if both RangeValue have the same current, min, max, step
		 * and are of type RangeValue, true otherwise
		 */
		virtual bool operator !=(const RangeValue<T> & rv) const;

		/**
		 * Copy operator from another AbstractRangeValue.
		 * Replaces the current attributes with those of the other AbstractRangeValue
		 * @param rv another AbstractRangeValue
		 * @return a reference to the modified RangeValue
		 * @post The AbstractRangeValue rv is only copied if it is an RangeValue,
		 * otherwise "this" is not modified
		 */
		AbstractRangeValue<T> & operator =(const AbstractRangeValue<T> & rv);

		/**
		 * Move operator from another AbstractRangeValue.
		 * Replaces the current attributes with those of the other AbstractRangeValue
		 * which can then be destroyed.
		 * @param rv another AbstractRangeValue
		 * @return a reference to the modified AbstractRangeValue
		 * @post The AbstractRangeValue rv is only copied if it is an RangeValue,
		 * otherwise "this" is not modified
		 */
		AbstractRangeValue<T> & operator =(AbstractRangeValue<T> && rv);

		/**
		 * Addition operator with a regular value.
		 * The argument value is added to the current value iff it is reachable
		 * by a finite number of next or previous values iterations and stays
		 * within range. Otherwise the current value is not modified
		 * @param value the value to add to the current value
		 * @return a new RangeValue (with eventually) a modified current value
		 */
		RangeValue<T> operator +(const T & value) const;

		/**
		 * Substraction operator with a regular value.
		 * The argument value is substracted to the current value iff it is
		 * reachable by a finite number of next or previous values iterations
		 * and stays within range. Otherwise the current value is not modified
		 * @param value the value to add to the current value
		 * @return a new RangeValue (with eventually) a modified current value
		 */
		RangeValue<T> operator -(const T & value) const;

		/**
		 * Multiplication operator with a regular value.
		 * The argument value is multiplied by the current value iff it is
		 * reachable by a finite number of next or previous values iterations
		 * and stays within range. Otherwise the current value is not modified
		 * @param value the value to add to the current value
		 * @return a new RangeValue (with eventually) a modified current value
		 */
		RangeValue<T> operator *(const T & value) const;

		/**
		 * Division operator with a regular value.
		 * The current value is divided byt the argument iff it is
		 * reachable by a finite number of next or previous values iterations
		 * and stays within range. Otherwise the current value is not modified
		 * @param value the value to add to the current value
		 * @return a new RangeValue (with eventually) a modified current value
		 */
		RangeValue<T> operator /(const T & value) const;

	public:
		// --------------------------------------------------------------------
		// Utility methods
		// --------------------------------------------------------------------
		/**
		 * Number of increments (positive or negative) needed to reach the
		 * value in argument from the internal #_value
		 * @param value the value to reach
		 * @return the numbr of increments (positive or negative) to reach
		 * this value
		 * @pre value is supposed to be within [#_minValue ... #_maxValue]
		 * otherwise this method might fail.
		 */
		double incrementsToValue(const T value);

		/**
		 * Number of increments to reach _maxValue from _value
		 * @return the number of increments or multiplications of _value to
		 * reach _maxValue. If this number is not an integer then _maxValue
		 * is not reachable
		 */
		double incrementsToMax() const;

		/**
		 * Number of decrements to reach _minValue from _value
		 * @return the number of decrements or divisions of _value to
		 * reach _minValue. If this number is not an integer then _minValue
		 * is not reachable
		 */
		double incrementsToMin() const;

		/**
		 * Ensures _maxValue is reachable adding _stepValue n times to _value.
		 * 	And fix _maxValue to the closest reachable value
		 */
		void fixMax();

		/**
		 * Ensures _minValue is reachable by substracting _stepValue n times
		 * to _value.
		 * 	And fix _minValue to the closest reachable value
		 */
		void fixMin();

		/**
		 * Ensure _stepValue value consistency:
		 * 	- _stepValue should be positive
		 * 	- _stepValue < _maxValue - _value
		 * 	- _stepValue < _value - _minValue
		 */
		void fixStep();

		/**
		 * Stream Range value to output stream
		 * @param out the output stream
		 * @return a reference to the modified output stream
		 */
		ostream & toStream(ostream & out) const;

		/**
		 * Stream Range value to output stream template
		 * @tparam Stream the type of stream
		 * @param out the output stream
		 * @return a reference to the modified output stream
		 * @note this template method needs to be implemented in the header
		 * so it can be instatiated in any sourcee file (.cpp) that needs a
		 * specific instantiation
		 */
		template <typename Stream>
		Stream & toStream_Impl(Stream & out) const
		{
			out << "(±" << _stepValue << ")";
			return out;
		}
};

#endif /* RANGEVALUE_H_ */
