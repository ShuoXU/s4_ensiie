#include "mydelegate.h"
#include <QIcon>
#include <QPainter>
#include <QDebug>
#include <QApplication>
#include <QMouseEvent>
#include <QMessageBox>

MyDelegate::MyDelegate(bool flag, QObject *parent)
    :QItemDelegate (parent), delFlag(flag)
{

}
MyDelegate::~MyDelegate(){}

QWidget* MyDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    QLineEdit *m_firstname = new QLineEdit(parent);
    QLineEdit *m_lastname = new QLineEdit(parent);
    QLineEdit *m_nickname = new QLineEdit(parent);

    QLineEdit *adr_Road = new QLineEdit(parent);
    QLineEdit *adr_Zipcode = new QLineEdit(parent);
    QLineEdit *adr_Localite = new QLineEdit(parent);
    QLineEdit *adr_Region = new QLineEdit(parent);
    QLineEdit *adr_Country = new QLineEdit(parent);

    QLineEdit *m_tel = new QLineEdit(parent);
    QLineEdit *m_email = new QLineEdit(parent);
    QLineEdit *m_site = new QLineEdit(parent);
    QLineEdit *m_note = new QLineEdit(parent);

    QLineEdit *m_organisation = new QLineEdit(parent);


    QComboBox *m_sex = new QComboBox(parent);
    QComboBox *adr_type = new QComboBox(parent);
    QComboBox *tel_type = new QComboBox(parent);

    QRegExp regxmail("\\b[A-Z0-9a-z._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}\\b");
    QRegExp regxtel("[0-9]{0,14}$");

    QValidator *validatormail = new QRegExpValidator(regxmail,m_email);
    QValidator *validatortel = new QRegExpValidator(regxtel, m_tel);

    if(delFlag)
    {
        switch(index.row())
        {
        case 0:
            if(index.column() == 1)
                return m_firstname;
            return nullptr;

        case 1:
            if(index.column() == 1)
                return m_lastname;
            return nullptr;

        case 2:
            if(index.column() == 1)
                return m_nickname;
            return nullptr;

        case 3:

            if(index.column() == 1)
            {
                m_sex->addItem(QIcon(":/images/male.png"), QStringLiteral("male"));
                m_sex->addItem(QIcon(":/images/female.png"), QStringLiteral("female"));
                return m_sex;
            }
            return nullptr;

        case 4:
            if(index.column() == 0)
            {
                tel_type->addItem(QIcon(":/images/home.png"), QStringLiteral("home"));
                tel_type->addItem(QIcon(":/images/work.png"), QStringLiteral("work"));
                return tel_type;
                // break;
            }
            else {
                m_tel->setValidator(validatortel);
                return m_tel;
                // break;
            }

        case 5:
            if(index.column() == 1)
            {
                m_email->setValidator(validatormail);
                return m_email;
            }
            return nullptr;

        case 6:
            if(index.column() == 1)
                return m_site;
            return nullptr;

        case 7:
            if(index.column() == 1)
            {
                adr_type->addItem(QIcon(":/images/home.png"), QStringLiteral("home"));
                adr_type->addItem(QIcon(":/images/work.png"), QStringLiteral("work"));
                return adr_type;
            }
            return nullptr;

        case 8:
            if(index.column() == 1)
                return adr_Road;
            return nullptr;

        case 9:
            if(index.column() == 1)
                return adr_Zipcode;
            return nullptr;

        case 10:
            if(index.column() == 1)
                return adr_Localite;
            return nullptr;

        case 11:
            if(index.column() == 1)
                return adr_Region;
            return nullptr;

        case 12:
            if(index.column() == 1)
                return adr_Country;
            return nullptr;

        case 13:
            if(index.column() == 1)
                return m_note;
            return nullptr;

        case 14:
            if(index.column() == 1)
                return m_organisation;
            return nullptr;


        }
    }
    else {
        QLineEdit *name = new QLineEdit(parent);
        switch(index.row())
        {
        case 0:
            if(index.column() == 1)
                return name;
            return nullptr;

        case 1:
            if(index.column() == 1)
            {
                m_tel->setValidator(validatortel);
                return m_tel;
            }
            return nullptr;

        case 2:
            if(index.column() == 1)
            {
                m_email->setValidator(validatormail);
                return m_email;
            }
            return nullptr;

        case 3:

            if(index.column() == 1)
                return m_site;
            return nullptr;

        case 4:
            if(index.column() == 1)
                return adr_Road;
            return nullptr;

        case 5:
            if(index.column() == 1)
                return adr_Zipcode;
            return nullptr;

        case 6:
            if(index.column() == 1)
                return adr_Localite;
            return nullptr;

        case 7:
            if(index.column() == 1)
                return adr_Region;
            return nullptr;

        case 8:
            if(index.column() == 1)
                return adr_Country;
            return nullptr;

        case 9:
            if(index.column() == 1)
                return m_note;
            return nullptr;

        }

    }
    return new QLineEdit(parent);
}

void MyDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{


    if(!delFlag)
        return QItemDelegate::setEditorData(editor,index);
    switch (index.row()) {
    case 3:
        if(index.column() == 1)
        {
            QString str =index.model()->data(index).toString();

            QComboBox *box = static_cast<QComboBox*>(editor);
            int i=box->findText(str);
            qDebug()<<i<<"box";
            box->setCurrentIndex(i);
        }
        break;
    case 4:
        if(index.column() == 0)
        {
            QString str =index.model()->data(index).toString();

            QComboBox *box = static_cast<QComboBox*>(editor);
            int i=box->findText(str);
            qDebug()<<i<<"box";
            box->setCurrentIndex(i);
        }
        else {
            return QItemDelegate::setEditorData(editor,index);
        }
        break;
    case 7:
        if(index.column() == 1)
        {
            QString str =index.model()->data(index).toString();

            QComboBox *box = static_cast<QComboBox*>(editor);
            int i=box->findText(str);
            qDebug()<<i<<"box";
            box->setCurrentIndex(i);
        }
        break;
    default:
        return QItemDelegate::setEditorData(editor,index);
    }



}



void MyDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    if(!delFlag)
        return QItemDelegate::setModelData(editor, model, index);

    switch (index.row()) {
    case 3:
        if(index.column() == 1)
        {
            QComboBox *box = static_cast<QComboBox*>(editor);
            int i = box->currentIndex();
            model->setData(index,i);
        }
        break;
    case 4:
        if(index.column() == 0)
        {
            QComboBox *box = static_cast<QComboBox*>(editor);
            int i = box->currentIndex();
            model->setData(index,i);
        }
        else
        {
            return QItemDelegate::setModelData(editor, model, index);
        }
        break;
    case 7:
        if(index.column() == 1)
        {
            QComboBox *box = static_cast<QComboBox*>(editor);
            int i = box->currentIndex();
            model->setData(index,i);
        }
        break;
    default:
        return QItemDelegate::setModelData(editor,model,index);
    }

}


