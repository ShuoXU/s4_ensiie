#include "myfilter.h"

Myfilter::Myfilter(QObject *parent)
    :QSortFilterProxyModel(parent)
{

}
 bool Myfilter::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
 {

     QModelIndex source_index = sourceModel()->index(sourceRow, 0, sourceParent);
         if (sourceModel()->rowCount(source_index)>0)
             return true;
         else
             return QSortFilterProxyModel::filterAcceptsRow(sourceRow, sourceParent);

 }
