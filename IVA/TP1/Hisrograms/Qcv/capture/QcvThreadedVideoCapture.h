#ifndef QCVTHREADEDVIDEOCAPTURE_H
#define QCVTHREADEDVIDEOCAPTURE_H

#include <QObject>
#include <QSize>
#include <QString>
#include <QElapsedTimer>	// for measuring framerate
#include <QRegExp>

#include <system_error>	// for throwing a std::no_such_file_or_directory
#include <utility>		// for pair<T,V>

#include <opencv2/highgui/highgui.hpp>	// for VideoCapture

#include <Qcv/capture/QcvVideoCapture.h>
#include <Qcv/capture/QcvCaptureWorker.h>

/*
 * Forward declaration of QcvCaptureWorker because
 *	- QcvThreadedVideoCapture includes QcvCaptureWorker
 *	- and QcvCaptureWorker includes QcvThreadedVideoCapture
 */
class QcvCaptureWorker;

/**
 * New implementation (06/2017) of Qt + OpenCV Capture.
 * @note This implementation delegates the capture update to a separate thread
 * and therefore only methods other than update are performed in the current
 * thread (hence the need of a QMutex to ensure atomic operations on capture).
 * @note The update thread (#worker) can perform in two modes:
 *	- asynchrone : where the update is performed as fast as possible (useful for
 *	camera source)
 *	- synchrone : where the update is performed at a specific frame rate (useful
 *	for video file playback) as long as the requested frame rate is slower than
 *	capture update time. If requested frame rate is higher than capture maximum
 *	refresh rate then requested frame rate might not be attainable.
 * @note This capture is automatically "skippable", meaning, when capture update
 * can't get lock, it will simply wait until lock can be obtained before updating
 */
class QcvThreadedVideoCapture : public QcvVideoCapture
{
	Q_OBJECT

	/**
	 * Capture worker is defined as a friend of QcvThreadedVideoCapture, so
	 * it can access its members directly
	 * @note QcvCaptureWorker could have been declared as a nested class but
	 * Qt's Meta-Object Compiler (moc) does not support nested classes.
	 */
	friend class QcvCaptureWorker;

	private:
		/**
		 * Flag indicating images are captured by reading image files rather
		 * than reading video.
		 */
		bool stillImagesCapture;

		/**
		 * Directory where the images should be read from
		 */
		QString * imagesDirectory;

		/**
		 * List of (numbered) image files to read when capturing from still
		 * images
		 */
		QStringList * imageFiles;

		/**
		 * Iterator on the imageFiles
		 */
		QStringList::const_iterator imageFilesIterator;

		/**
		 * Iterator to the end of imageFiles
		 */
		QStringList::const_iterator imageFilesEnd;

		/**
		 * Regular expression to valid numbered image files.
		 * Valid file patterns are:
		 *	- <digits>.[png|jpg|tif]
		 *	- Name<digits>.[png|jpg|tif]
		 *	- Name_<digits>.[png|jpg|tif]
		 *	- Name-<digits>.[png|jpg|tif]
		 */
		QRegExp filesPattern = QRegExp("^([A-Za-z_-]*)(\\d+)\\.(png|jpg|tif)$");

		/**
		 * Acceptable image files extensions
		 */
		QStringList filesExtensions = {
			"*.jpg",
			"*.png",
			"*.tif"
		};

		/**
		 * The worker thread updating captured images in another thread
		 * @note when #live is true the worker works in unsynchronized mode
		 * where it updates this capture as fast as posssible. Otherwise we're
		 * reading video or image files and worker should work in synchronized
		 * mode with a specified frameRate (either provided or extracted from
		 * video file): In this mode the worker waits for the current frame
		 * period to be elapsed before updating this capture again.
		 */
		QcvCaptureWorker * worker;

		/**
		 * Indicates we're trying to restart video playback from a file
		 * which should not be tried more than once.
		 */
		bool restarted;

	public:

		/**
		 * Constructor for live video from deviceId
		 * @param deviceId the ID of the camera to open
		 * @param flipVideo indicates if the video should be flipped horizontally
		 * @param gray indicates if image should be converted to gray
		 * @param width desired width (or 0 to keep original capture image width)
		 * @param height desired height (or 0 to keep original capture image height)
		 * @param parent the parent QObject
		 */
		explicit QcvThreadedVideoCapture(const int deviceId = 0,
										 const bool flipVideo = false,
										 const bool gray = false,
										 const size_t width = 0,
										 const size_t height = 0,
										 QObject * parent = nullptr);

		/**
		 * Constructor from file name
		 * @param fileName video file to open OR directory containing images
		 * files to open
		 * @param flipVideo mirror image
		 * @param gray convert image to gray
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @param parent the parent QObject
		 */
		explicit QcvThreadedVideoCapture(const QString & fileName,
										 const bool flipVideo = false,
										 const bool gray = false,
										 const size_t width = 0,
										 const size_t height = 0,
										 QObject * parent = nullptr);

		/**
		 * Constructor from dirName or numbered file name for reading
		 * numbered image files
		 * @param name directory name or numbered file name
		 * @param frameRate frame rate at which images should be read
		 * @param flipVideo mirror image
		 * @param gray convert image to gray
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @param parent the parent QObject
		 * @throw std::system_error when name does not correspond to an existing
		 * file or a directory
		 */
		explicit QcvThreadedVideoCapture(const QString & name,
										 const double frameRate,
										 const bool flipVideo = false,
										 const bool gray = false,
										 const size_t width = 0,
										 const size_t height = 0,
										 QObject * parent = nullptr)
			throw (std::system_error);

		/**
		 * Destructor.
		 * releases video capture and image(s)
		 */
		virtual ~QcvThreadedVideoCapture();

		/**
		 * Indicates if capture works in synchronized mode where
		 * frame rate is imposed.
		 * @return true is capture works in synchronized mode, false
		 * otherwise
		 */
		bool isSynchronized() const override;

		/**
		 * Gets the contrained frame rate at which capture should operate
		 * when synchronized mode is on
		 * @return the constrained frame rate
		 */
		double getContrainedFrameRate() const override;

	public slots:
		/**
		 * Open new device Id
		 * @param deviceId device number to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if device has been opened and checked and timer launched
		 */
		bool openDevice(const int deviceId,
						const size_t width = 0,
						const size_t height = 0) override;

		/**
		 * Open new video file
		 * @param fileName video file to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if video has been opened and timer launched
		 */
		bool openFile(const QString & fileName,
					  const size_t width = 0,
					  const size_t height = 0) override;

		/**
		 * Open Directory containing files to read (does nothing in this
		 * implementation)
		 * @param dirName directory to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return always false
		 */
		bool openDirectory(const QString & dirName,
						   const size_t width = 0,
						   const size_t height = 0) override;

//		/**
//		 * Sets the "flipped" status of captured video
//		 * @param value the new "flipped" status
//		 */
//		void setFlipped(const bool value) override;

		/**
		 * Sets the gray conversion status of captured video
		 * @param value the new value of gray conversion status
		 * @post The #imageChanged(cv::Mat *); signal have been emitted
		 */
		void setGray(const bool value) override;

		/**
		 * Sets capture to synchronized mode (when true) where frame rate is
		 * contrained (whenever possible). Otherwise frame rate is not
		 * constrained and capture may proceed as fast as possible.
		 * @param value the new value of synchronized mode
		 * @post The #synchronized(bool); signal has been emitted
		 */
		void setSynchronized(const bool value) override;

		/**
		 * Releases capture and emits #finished() signal
		 */
		virtual void finish() override;

		/**
		 * Sets the desired frame rate on capture ONLY when capture is in
		 * synchronized mode
		 *	- If the new frame rate is 0.0 then use natural capture frame rate.
		 *	- If the new frame rate is too high for capture then capture will
		 *	keeps its highest possible frame rate according to the device in use
		 * @param frameRate the new frame rate to set
		 * @note Please note that this method has no effect on capture when
		 * synchronized mode is off
		 * @post The #constrainedFrameRateChanged(double) signal have been emitted
		 */
		void setFrameRate(const double frameRate) override;

	protected slots:

		/**
		 * Update capture (with proper locking/unlocking):
		 *	- Grabs a new image
		 *	- if gray is on then convert
		 *	- if resize is on then resize image into imageResized
		 *	(otherwise set imageResized to image)
		 *	- if flipped is on then flip imageResized into imageFlipped
		 *	(otherwise set imageFlipped to imageResized)
		 *	- if gray is on then converts imageFlipped into imageDisplay
		 *	(otherwise set imageDisplay to imageFlipped)
		 * @return true if new image has been grabbed from capture or false
		 * when there is no more images to grab from video capture
		 * @post When no image has been grabbed resizing, flipping and
		 * converting to gray are not performed
		 * @post If a new image is available #updated(); signal has been emitted
		 */
		bool update() override;

	protected:
		/**
		 * After capture has been created,
		 *	- grab a first image to setup sizes
		 *	- launch the worker
		 * @param width required image width
		 * @param height required image height
		 * @pre capture is not null and opened
		 */
		void setupAndLaunch(const size_t width = 0,
							const size_t height = 0);

		/**
		 * Sets the desired size of captured image
		 * @param width the desired width of captured image
		 * @param height the desired height of captured image
		 * @post The #messageChanged(QString); signal is emitted with new size
		 * values
		 * @see #size
		 */
		void setSize_Impl(const size_t width, const size_t height) override;

		/**
		 * Grabs new image from capture
		 * @return true if new image has been grabbed from capture or false
		 * when there is no more images to grab from video capture
		 * @note This method eventually deals with automatically restarting
		 * the capture when video file playback has reached the end of the file
		 * @warning This method does not perform locking/unlocking so it not
		 * safe to use it without proper mutex's operations
		 * @post #messageChanged(); signal has been emitted if either:
		 *	- There is no more frames to capture (then capture is released)
		 *	- Capture has been restarted to loop video files
		 */
		bool grab();

		/**
		 * If resizing is needed, resize the image
		 * @return true if actual resizing has been performed, false if
		 * #imageResized has only been copied from #image
		 * @warning This method does not perform locking/unlocking so it is not
		 * safe to use it without proper mutex's operations
		 */
		bool performResize();

		/**
		 * if flipping is needed, flip the image
		 * @return true if actual flip has been performed and false if
		 * #imageFlipped has only been copied from #imageResized
		 * @warning This method does not perform locking/unlocking so it not
		 * safe to use it without proper mutex's operations
		 */
		bool performFlip();

		/**
		 * if gray conversion is needed convert to gray image
		 * @return true if actual conversion has been performed and false if
		 * #imageDisplay has only been copied from #imageFlipped
		 * @warning This method does not perform locking/unlocking so it is not
		 * safe to use it without proper mutex's operations
		 */
		bool performConvert();

		/**
		 * Release capture by either:
		 *	- releasing #capture or
		 *	- setting #imageFilesIterator to the end and clearing #imageFiles
		 * These opeation are performed within a critical section locked with
		 * #mutex
		 * Then wait for #worker to terminate it's run method.
		 */
		void releaseCapture();

		/**
		 * Release old capture elements in order to switch to a new video capture.
		 *	- Old capture is cleaned up
		 *	- Worker gets terminated and destroyed
		 *	- old capture is replaced by new one
		 * @param newCapture the new capture to switch on
		 * @param synchronized mode for the new capture
		 * @param frameRate if synchronized mode is used the frame rate for
		 * the synchronized mode
		 * @pre new capture needs to be non null an opened
		 * @pre Capture attributes such as #stillImagesCapture,
		 * QcvVideoCapture::live and QcvCapture::frameRate haven't been changed
		 * yet to new values
		 * @post new still needs to be setup and a new worker recreated and
		 * launched with #setupAndLaunch();
		 */
		void switchToNewVideoCapture(cv::VideoCapture * newCapture);
//								const bool synchronized = false,
//								const double frameRate = defaultFrameRate);

		/**
		 * Release old capture elements in order to switch to a new video capture.
		 *	- Old capture is cleaned up
		 *	- Worker gets terminated and destroyed
		 *	- old capture is replaced by new one
		 * @param imagesDirectory the new image directory for image files
		 * @param imageFiles the new list of image files
		 * @pre provided imagesDirectory and imageFiles need to be non null
		 * and properly initialized
		 * @pre Capture attributes such as #stillImagesCapture,
		 * QcvVideoCapture::live and QcvCapture::frameRate haven't been changed
		 * yet to new values
		 * @post new still needs to be setup and a new worker recreated and
		 * launched with #setupAndLaunch();
		 */
		void switchToNewStillCapture(QString * imagesDirectory,
									 QStringList * imageFiles);

		/**
		 * Setup Still Image capture with provided directory or file name
		 * @param name directory or numbered file name
		 * @throw std::system_error when name does not correspond to an existing
		 * file or a directory
		 * @return a new pair of <QString *, QStringList *> which can be applied
		 * to #imagesDirectory and #imageFiles
		 */
		std::pair<QString*, QStringList*> setupStillImages(const QString & name)
			throw (std::system_error);

		/**
		 * Wait for at least one call to update method in the worker thread:
		 *	- blocks all signals (so other classes are NOT notified of update)
		 *	- Get an update condition from the worker
		 *	- Wait on this condition using update condition's wait(&mutex) method
		 *	- unblocks all signals (so other classes can be notified of update)
		 * @pre mutex should be locked prior to calling this method
		 */
		void waitForUpdate();
};

#endif // QCVTHREADEDVIDEOCAPTURE_H
