/*
 * AbstractRangeValue.cpp
 *
 *  Created on: 17 mai 2015
 *      Author: davidroussel
 */

#include <cmath>		// for math funcs
#include <iostream>		// for ostream
#include <algorithm>	// for sort

using namespace std;

#ifndef MIN
	#define MIN(a,b) (a < b ? a : b)
#endif
#ifndef MAX
	#define MAX(a,b) (a > b ? a : b)
#endif

#include <utils/AbstractRangeValue.h>

// Dummy initialisation (specialised later)
template <typename T> const T AbstractRangeValue<T>::EPSILON = T(0);

// ----------------------------------------------------------------------------
// Constructors / destructor
// ----------------------------------------------------------------------------

/*
 * AbstractRangeValue default constructor (for default constructible
 * compatibility)
 */
template <typename T>
AbstractRangeValue<T>::AbstractRangeValue() :
	AbstractRangeValue(T(0), T(0), T(0), T(0)) // c++ 11 only
{
}

/*
 * Valued constructor with currents value, range values and evt
 * step value and step mode.
 * 	- minValue should be less than current value, otherwise it is set to
 * 	the current value
 * 	- maxValue should be greater than current value, otherwise it is set
 * 	to the current value
 * 	- stepValue and stepMode should sbe such that the next value should
 * 	be less than the max value and previous value should be greater than
 * 	min value, otherwise actual step value is reduced to meet these
 * 	conditions
 * @param value the current value
 * @param minValue the minimum possible value
 * @param maxValue the maximum possible value
 * @param stepValue the step value to get to the next/previous value
 * [default is 0]
 * @param step the step mode to get to the next/previous value
 * [default is NONE]
 */
template <typename T>
AbstractRangeValue<T>::AbstractRangeValue(const T & value,
										  const T & minValue,
										  const T & maxValue,
										  const T & stepValue) :
	_value(value),
	_defaultValue(value),
	_minValue(minValue),
	_maxValue(maxValue),
	_stepValue(stepValue)
{
	/*
	 * If step value is negative then set it to opposite step value
	 */
	if (stepValue < T(0))
	{
		_stepValue = -stepValue;
	}

	/*
	 * If step value is too small set it to epsilon
	 */
	if (abs(double(stepValue)) <= EPSILON)
	{
		_stepValue = EPSILON;
	}

	/*
	 * Check _minValue < _value < _maxValue
	 */
	if ((_value < _minValue) || (_value > _maxValue))
	{
		fixValueAndRange();
	}

	fixStep();

	if (_value != value)
	{
		_defaultValue = _value;
	}
}

/*
 * Valued constructor from tuple
 * @param tupleValues tuple containing value, min max & step
 */
template <typename T>
AbstractRangeValue<T>::AbstractRangeValue(const tuple<T, T, T, T> & tupleValues) :
	AbstractRangeValue(get<0>(tupleValues),
					   get<1>(tupleValues),
					   get<2>(tupleValues),
					   get<3>(tupleValues))
{
}

/*
 * Copy constructor
 * @param rv another AbstractRangeValue
 */
template <typename T>
AbstractRangeValue<T>::AbstractRangeValue(const AbstractRangeValue<T> & rv) :
	AbstractRangeValue(rv._value,
					   rv._minValue,
					   rv._maxValue,
					   rv._stepValue)	// C++ 11 only
{
}

/*
 * Move constructor
 * @param rv another AbstractRangeValue
 */
template <typename T>
AbstractRangeValue<T>::AbstractRangeValue(AbstractRangeValue<T> && rv) :
	AbstractRangeValue(rv._value,
					   rv._minValue,
					   rv._maxValue,
					   rv._stepValue)	// C++ 11 only
{
}

/*
 * Destructor
 */
template <typename T>
AbstractRangeValue<T>::~AbstractRangeValue()
{
}
// ----------------------------------------------------------------------------
// AbstractRangeValue specific operations
// ----------------------------------------------------------------------------

/*
 * Current value read accessor
 * @return a copy of the current value
 */
template <typename T>
T AbstractRangeValue<T>::value() const
{
	return _value;
}

/*
 * Multiple values read accessor
 * @param which enum indicating which value is required
 * @return the required value
 * @see WhichValue
 */
template <typename T>
T AbstractRangeValue<T>::value(const AbstractRangeValue<T>::WhichValue which) const
{
	switch (which)
	{
		case WhichValue::DEFAULT_VALUE:
			return _defaultValue;
		case WhichValue::MIN_VALUE:
			return _minValue;
		case WhichValue::MAX_VALUE:
			return _maxValue;
		case WhichValue::STEP_VALUE:
			return _stepValue;
		case WhichValue::VALUE:
		default:
			return _value;
	}
}

/*
 * Default value read accessor
 * @return a copy of the default value
 */
template <typename T>
T AbstractRangeValue<T>::defaultValue() const
{
	return _defaultValue;
}

/*
 * Min value read accessor
 * @return a copy of the current min value
 */
template <typename T>
T AbstractRangeValue<T>::min() const
{
	return _minValue;
}

/*
 * Max value read accessor
 * @return a copy of the current max value
 */
template <typename T>
T AbstractRangeValue<T>::max() const
{
	return _maxValue;
}

/*
 * Range size
 * @return the (max - min) size of the this range
 */
template <typename T>
T AbstractRangeValue<T>::range() const
{
	return _maxValue - _minValue;
}

/*
 * Step value read accessor
 * @return a copy of the current step value
 */
template <typename T>
T AbstractRangeValue<T>::step() const
{
	return _stepValue;
}

/*
 * Current value setter to the closest possible value.
 * if the new value is > max then the current value is set to max.
 * if the new value is < min then the current value is set to min.
 * if the new value is >= min & <= max it is set to the closest accessible
 * value according to steps.
 * @param value the new current value to set
 * @return true if the value in argumenthas been set exactly, false if
 * the new value is slightly different
 * @post a new value has been set as the closest value to the argument
 */
template <typename T>
bool AbstractRangeValue<T>::setClosestValue(const T & value)
{
	if (value < _minValue)
	{
		_value = _minValue;
		return false;
	}

	if (value > _maxValue)
	{
		_value = _maxValue;
		return false;
	}

	/*
	 * value is >= min && <= max so it should be reachable
	 * Naïve iterative implementation: should be refined by more efficient
	 * implementation in subclasses
	 */
	double incs = incrementsToValue(value);
	if (incs > 0.0)
	{
		for (; _value < value && hasNext(); )
		{
			next();
		}
	}
	else
	{
		for (; _value > value && hasPrevious(); )
		{
			previous();
		}
	}

	/*
	 * Return true if the reached is close enough to required value
	 */
	return abs(double(_value) - double(value)) < EPSILON;
}

/*
 * Max value setter.
 * The new max value should be greater than the current value
 * @param value the new max value
 * @return true if the new max value has been set
 * @post _stepValue might have been adjusted to avoid overflow by
 * next value
 */
template <typename T>
bool AbstractRangeValue<T>::setMax(const T & value)
{
	if (value >= _value)
	{
		_maxValue = value;
		fixStep();
		return true;
	}

	return false;
}

/*
 * Min value setter.
 * The new min value should be less than the current value
 * @param value the new min value
 * @return true if the new max value has been set
 * @post _stepValue might have been adjusted to avoid underflow by
 * previous value
 */
template <typename T>
bool AbstractRangeValue<T>::setMin(const T & value)
{
	if (value <= _value)
	{
		_minValue = value;
		fixStep();
		return true;
	}

	return false;
}

/*
 * Step value setter.
 * The new step value should be such that the next value according to
 * this new step value is less than the max value and the previous
 * value should be greater than the min value
 * @param value the new step value
 * @return true if the new max value has been set
 */
template <typename T>
bool AbstractRangeValue<T>::setStep(const T & value)
{
	if (value < T(0))
	{
		_stepValue = -value;
	}
	else
	{
		_stepValue = value;
	}

	// positive step value should always be > EPSILON in any cases
	if (_stepValue < EPSILON)
	{
		_stepValue = EPSILON;
	}

	// Reimplementation in child classes may return true but this one is
	// incomplete and should always return false
	return false;
}

/*
 * Reset to default value
 * @return true if the default value has been set, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::reset()
{
	return setValue(_defaultValue);
}

// ----------------------------------------------------------------------------
// Operators
// ----------------------------------------------------------------------------

/*
 * Comparison operator with another AbstractRangeValue
 * @param rv another AbstractRangeValue
 * @return true if both AbstractRangeValue have the same current, min, max, step
 * and stepMode values, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator ==(const AbstractRangeValue<T> & rv) const
{
	return ((_value        == rv._value) &&
			(_defaultValue == rv._defaultValue) &&
			(_minValue     == rv._minValue) &&
			(_maxValue     == rv._maxValue) &&
			(_stepValue    == rv._stepValue));
}

/*
 * Difference operator with another AbstractRangeValue
 * @param rv another AbstractRangeValue
 * @return false if both AbstractRangeValue have the same current, min, max, step
 * and stepMode values, true otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator !=(const AbstractRangeValue<T> & rv) const
{
	return !operator == (rv);
}

/*
 * Comparison operator with regular value
 * @param value the regular valur to compare
 * @return true if the current value is the same as the argument, false
 * otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator ==(const T & value) const
{
	return _value == value;
}

/*
 * Difference operator with regular value
 * @param value the regular valur to compare
 * @return false if the current value is the same as the argument, true
 * otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator != (const T & value) const
{
	return !operator ==(value);
}

/*
 * Less than operator with another Range Value
 * @param arv the other range value
 * @return return true if current _value < arv._value, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator <(const AbstractRangeValue<T> & arv) const
{
	return _value < arv._value;
}

/*
 * Greater than operator with another Range Value
 * @param arv the other range value
 * @return return true if current _value > arv._value, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator >(const AbstractRangeValue<T> & arv) const
{
	return arv.operator <(*this);
}

/*
 * Less or equal operator with another Range Value
 * @param arv the other range value
 * @return return true if current _value <= arv._value, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator <=(const AbstractRangeValue<T> & arv) const
{
	return !arv.operator <(*this);
}

/*
 * Greater or equal operator with another Range Value
 * @param arv the other range value
 * @return return true if current _value <= arv._value, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator >=(const AbstractRangeValue<T> & arv) const
{
	return !operator <(arv);
}

/*
 * Less than operator with a value of type T
 * @param value the other range value
 * @return return true if current _value < value, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator <(const T & value) const
{
	return _value < value;
}

/*
 * Greater than operator with a value of type T
 * @param value the other range value
 * @return return true if current _value > value, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator >(const T & value) const
{
	return _value > value;
}

/*
 * Less or equal operator with a value of type T
 * @param value the other range value
 * @return return true if current _value <= value, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator <=(const T & value) const
{
	return _value <= value;
}

/*
 * Greater or equal operator with a value of type T
 * @param value the other range value
 * @return return true if current _value <= value, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::operator >=(const T & value) const
{
	return _value >= value;
}

/*
 * Copy operator from another AbstractRangeValue.
 * Replaces the current attributes with those of the other AbstractRangeValue
 * @param rv another AbstractRangeValue
 * @return a reference to the modified AbstractRangeValue
 */
template <typename T>
AbstractRangeValue<T> & AbstractRangeValue<T>::operator =(const AbstractRangeValue<T> & rv)
{
	_value = rv._value;
	_defaultValue = rv._defaultValue;
	_minValue = rv._minValue;
	_maxValue = rv._maxValue;
	_stepValue = rv._stepValue;

	return *this;
}

/*
 * Move operator from another AbstractRangeValue.
 * Replaces the current attributes with those of the other AbstractRangeValue
 * which can then be destroyed.
 * @param rv another AbstractRangeValue
 * @return a reference to the modified AbstractRangeValue
 */
template <typename T>
AbstractRangeValue<T> & AbstractRangeValue<T>::operator =(AbstractRangeValue<T> && rv)
{
	_value = rv._value;
	_defaultValue = rv._defaultValue;
	_minValue = rv._minValue;
	_maxValue = rv._maxValue;
	_stepValue = rv._stepValue;

	return *this;
}

/*
 * Copy operator from a regular value.
 * Replaces the current value (iff possible according to the rules of
 * #setValue) with the value in argument.
 * @param value a regular value
 * @return a reference to the AbstractRangeValue (which could be modified or not
 * depending the the value)
 * @see #setValue(const T & value)
 */
template <typename T>
AbstractRangeValue<T> & AbstractRangeValue<T>::operator =(const T & value)
{
	setClosestValue(value);
	return *this;
}

/*
 * Self addition operator with a regular value.
 * Self addition of the current value is performed only if the expected
 * value is reachable through a finite number of next or previous values
 * iteration and stays within range. Otherwise the current value is
 * not modified
 * @param value the value to add to the current value
 * @return a reference to the AbstractRangeValue
 */
template <typename T>
AbstractRangeValue<T> & AbstractRangeValue<T>::operator +=(const T & value)
{
	setValue(_value + value);
	return *this;
}

/*
 * Self substraction operator with a regular value.
 * Self substraction of the current value is performed only if the
 * expected value is reachable through a finite number of next or
 * previous values iterations and stays within range. Otherwise the
 * current value is not modified
 * @param value the value to substract to the current value
 * @return a reference to the AbstractRangeValue
 */
template <typename T>
AbstractRangeValue<T> & AbstractRangeValue<T>::operator -=(const T & value)
{
	setValue(_value - value);
	return *this;
}

/*
 * Self multiplication operator with a regular value.
 * Self multiplication of the current value is performed only if the
 * expected value is reachable through a finite number of next or
 * previous values iterations and stays within range. Otherwise the
 * current value is not modified
 * @param value the value to multiply by the current value
 * @return a reference to the AbstractRangeValue
 */
template <typename T>
AbstractRangeValue<T> & AbstractRangeValue<T>::operator *=(const T & value)
{
	setValue(_value * value);
	return *this;
}

/*
 * Self division operator with a regular value.
 * Self division of the current value is performed only if the
 * expected value is reachable through a finite number of next or
 * previous values iterations and stays within range. Otherwise the
 * current value is not modified
 * @param value the value to divide the current value
 * @return a reference to the AbstractRangeValue
 */
template <typename T>
AbstractRangeValue<T> & AbstractRangeValue<T>::operator /=(const T & value)
{
	setValue(_value / value);
	return *this;
}

/*
 * Cast operator to regular value
 * @return the current value
 * @par Usage
 * @code
 * 	AbstractRangeValue<Type> rv = ...;
 * 	...
 * 	Type v = (Type)rv;
 * @endcode
 */
template <typename T>
AbstractRangeValue<T>::operator T() const
{
	return _value;
}

// ----------------------------------------------------------------------------
// Utility methods
// ----------------------------------------------------------------------------

/*
 * Indicates if _maxValue can be reached by multiple incrementation /
 * multiplication by _stepValue
 * @return true if _maxValue is reachable, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::isMaxReachable() const
{
	double n = incrementsToMax();
	double in = round(n);
	return abs(n - in) <= EPSILON;
}

/*
 * Indicates if _minValue can be reached by multiple decrementation /
 * division by _stepValue
 * @return true if _minValue is reachable, false otherwise
 */
template <typename T>
bool AbstractRangeValue<T>::isMinReachable() const
{
	double n = incrementsToMin();
	double in = round(n);
	return abs(n - in) <= EPSILON;
}

/*
 * Ensures _maxValue & _minValue are reachable
 * 	- In additive mode:
 * 		- _maxValue should be reachable by adding _stepValue n times to
 * 		_value
 * 		- _minValue should be reachable by substracting _stepValue n
 * 		times to _value
 * 	- In Multiplicative mode:
 * 		- _maxValue should be reachable by multiplying _value n times
 * 		by _stepValue
 * 		- _minValue should be reachable by dividing _value n times
 * 		by _stepValue
 * 	_maxValue and _minValue are fixed to the closest reachable values
 */
template <typename T>
void AbstractRangeValue<T>::fixBounds()
{
	fixMax();
	fixMin();
}

/*
 * Ensure _minValue < _value < _maxValue by reordering the three values
 */
template <typename T>
void AbstractRangeValue<T>::fixValueAndRange()
{
	T values[3] = {_minValue, _value, _maxValue};
	sort(values, values + 3);
	_minValue = values[0];
	_value = values[1];
	_maxValue = values[2];
}

/*
 * Ensure _stepValue value consistency:
 * 	- _stepValue should be positive
 * 	- If _stepMode is ADDITIVE then _stepValue is such that:
 * 		- _stepValue < _maxValue - _value
 * 		- _stepValue < _value - _minValue
 * 	- If _stepMode is MULTIPLICATIVE then _stepValue is such that:
 * 		- _stepValue < _maxValue / _value
 * 		- _stepValue < _minValue * _value
 * 	- if _stepMode is not NONE, then _stepValue should not be null
 */
template <typename T>
void AbstractRangeValue<T>::fixStep()
{
	if (_stepValue < T(0))
	{
		_stepValue = -_stepValue;
	}

	if (_stepValue < EPSILON)
	{
		_stepValue = EPSILON;
	}
}

/*
 * Stream Range value to output stream
 * @param out the output stream
 * @return a reference to the modified output stream
 */
template <typename T>
ostream & AbstractRangeValue<T>::toStream(ostream & out) const
{
	return toStream_Impl<ostream>(out);
}

// ----------------------------------------------------------------------------
// Friend functions
// ----------------------------------------------------------------------------

/*
 * Standard output operator
 * @param out output stream
 * @param rv AbstractRangeValue to print out
 * @return a reference to the output stream
 */
template <typename T>
ostream & operator <<(ostream & out, const AbstractRangeValue<T> & rv)
{
	return rv.toStream(out);
}

// ----------------------------------------------------------------------------
// Specialisations
// ----------------------------------------------------------------------------
/*
 * Constants specialisations
 */
template <> const int AbstractRangeValue<int>::EPSILON = 1;
template <> const float AbstractRangeValue<float>::EPSILON = 1e-3;
template <> const double AbstractRangeValue<double>::EPSILON = 1e-6;

// ----------------------------------------------------------------------------
// Proto instantiations
// ----------------------------------------------------------------------------
/*
 * Template instantiation with int
 */
template class AbstractRangeValue<int>;
template ostream & AbstractRangeValue<int>::toStream_Impl(ostream &) const;

/*
 * Friend functions instancation with AbstractRangeValue<int>
 */
template ostream & operator <<(ostream &, const AbstractRangeValue<int> &);

/*
 * Template instantiation with float
 */
template class AbstractRangeValue<float>;
template ostream & AbstractRangeValue<float>::toStream_Impl(ostream &) const;

/*
 * Friend functions instancation with AbstractRangeValue<float>
 */
template ostream & operator <<(ostream &, const AbstractRangeValue<float> &);

/*
 * Template instantiation with double
 */
template class AbstractRangeValue<double>;
template ostream & AbstractRangeValue<double>::toStream_Impl(ostream &) const;

/*
 * Friend functions instancation with AbstractRangeValue<double>
 */
template ostream & operator <<(ostream &, const AbstractRangeValue<double> &);
