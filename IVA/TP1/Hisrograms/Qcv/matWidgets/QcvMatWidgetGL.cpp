/*
 * QcvMatWidgetGL.cpp
 *
 *  Created on: 28 févr. 2011
 *	  Author: davidroussel
 */
#include <QDebug>	// for qDebug() <<
#include <QTime>	// for timed debug
#include <QThread>	// for identifying thread in debug output

#include <Qcv/matWidgets/QcvMatWidgetGL.h>

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * OpenCV QT Widget default constructor
 * @param parent parent widget
 * @param lock the lock to use on sourceImage in multi-threaded
 * environments
 * @param mouseSense mouse sensivity
 */
QcvMatWidgetGL::QcvMatWidgetGL(QWidget *parent,
							   QMutex * lock,
							   MouseSense mouseSense) :
	QcvMatWidget(parent, lock, mouseSense),
	gl(NULL)
{
}

/*
 * OpenCV QT Widget constructor
 * @param sourceImage the source image
 * @param parent parent widget
 * @param lock the lock to use on sourceImage in multi-threaded
 * environments
 * @param mouseSense mouse sensivity
 */
QcvMatWidgetGL::QcvMatWidgetGL(Mat * sourceImage,
							   QWidget *parent,
							   QMutex * lock,
							   MouseSense mouseSense) :
	QcvMatWidget(sourceImage, parent, lock, mouseSense),
	gl(NULL)
{
	setSourceImage(sourceImage);
}

/*
 * OpenCV Widget destructor.
 */
QcvMatWidgetGL::~QcvMatWidgetGL()
{
	if (gl != nullptr)
	{
		layout->removeWidget(gl);
		delete gl;
	}
}

/*
 * Sets new source image
 * @param sourceImage the new source image
 */
void QcvMatWidgetGL::setSourceImage(Mat *sourceImage)
{
	QcvMatWidget::setSourceImage(sourceImage);

	if (gl != nullptr)
	{
		layout->removeWidget(gl);
		delete gl;
	}

	convertImage();

	gl = new QGLImageRender(displayImage, GL_RGB, &pixelScale, this);

	layout->addWidget(gl, 0, Qt::AlignCenter);
}

// ----------------------------------------------------------------------------
// Protected Methods
// ----------------------------------------------------------------------------

/*
 * paint event reimplemented to draw content
 * @param event the paint event
 */
void QcvMatWidgetGL::paintEvent(QPaintEvent * event)
{
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay()
//					   << QThread::currentThreadId()
//					   << "QcvMatWidgetGL::paintEvent start";

	QcvMatWidget::paintEvent(event);
	gl->update();

//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay()
//					   << QThread::currentThreadId()
//					   << "QcvMatWidgetGL::paintEvent end";

}
