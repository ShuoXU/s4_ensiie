/*
 * QcvGFilter.h
 *
 *  Created on: 27 févr. 2012
 *      Author: davidroussel
 */

#ifndef QCVGFILTER_H_
#define QCVGFILTER_H_

#include <Qcv/QcvProcessor.h>
#include <Qcv/controllers/QRangeIntController.h>
#include <Qcv/controllers/QRangeDoubleController.h>
#include <Qcv/controllers/QEnumController.h>

#include "CvGFilter.h"

/**
 * QT flavored class to process source image with gaussian filters
 */
class QcvGFilter: public QcvProcessor, public CvGFilter
{
	Q_OBJECT

	protected:

		/**
		 * Display images names
		 */
		QStringList displayImagesNames =
		{
			tr("Input image"),
			tr("Input gray image"),
			tr("Gaussian blurred gray image"),
			tr("Horizontal gradient component"),
			tr("Vertical gradient component"),
			tr("Gradient magnitude"),
			tr("Gradient angle"),
			tr("Edge map"),
			tr("Laplacian"),
			tr("Cornerness measure"),
			tr("Harris cornerness measure")
		};

		/**
		 * Edge map modes names
		 */
		QStringList edgeModeNames =
		{
			tr("Gradient edge map"),
			tr("Canny's edge map"),
			tr("Merged edge map")
		};

		/**
		 * Range controller for #kernelSize
		 */
		QRangeIntController kernelSizeController;

		/**
		 * Range controller for #sigma
		 */
		QRangeDoubleController sigmaController;

		/**
		 * Range controller for #thresholdLevel
		 */
		QRangeIntController thresholdLevelController;

		/**
		 * Range controller for #harrisKappa
		 */
		QRangeDoubleController harrisKappaController;

		/**
		 * Enum controller for #displayMode
		 */
		QEnumController displayModeController;

		/**
		 * Enum controller for #edgeMode
		 */
		QEnumController edgeModeController;

	public:

		/**
		 * QcvGFilter constructor
		 * @param image the source image
		 * @param imageLock the mutex on source image
		 * @param updateThread the thread in which this processor runs
		 * @param parent parent QObject
		 */
		QcvGFilter(Mat * image,
				   QMutex * imageLock = NULL,
				   QThread * updateThread = NULL,
				   QObject * parent = NULL);

		/**
		 * QcvGFilter destructor
		 */
		virtual ~QcvGFilter();

		/**
		 * Get the controller for #kernelSize
		 * @return the controller for #kernelSize
		 */
		QRangeIntController * getKernelSizeController();

		/**
		 * Get the controller for #sigma
		 * @return the controller for #sigma
		 */
		QRangeDoubleController * getSigmaController();

		/**
		 * Get the controller for #thresholdLevel
		 * @return the controller for #thresholdLevel
		 */
		QRangeIntController * getThresholdLevelController();

		/**
		 * Get the controller for #harrisKappa
		 * @return the controller for #harrisKappa
		 */
		QRangeDoubleController * getHarrisKappaController();

		/**
		 * Get the controller for #displayMode
		 * @return the controller for #displayMode
		 */
		QEnumController * getDisplayModeController();

		/**
		 * Get the controller for #edgeMode
		 * @return the controller for #edgeMode
		 */
		QEnumController * getEdgeModeController();

		// --------------------------------------------------------------------
		// Options settings with message notification
		// --------------------------------------------------------------------

	public slots:
		/**
		 * Update computed images slot and sends updated signal
		 * required
		 */
		void update();

		/**
		 * Changes source image slot.
		 * Attributes needs to be cleaned up then set up again
		 * @param image the new source Image
		 */
		void setSourceImage(Mat * image)
			throw (CvProcessorException);

		/**
		 * Sets a new display mode and emit corresponding imageChanged(Mat*)
		 * signal
		 * @param displayMode the new display mode to set
		 */
		void setDisplayMode(const int displayMode);

		/**
		 * Set a new edge didsplay mode in standalone mode and emit corresponding
		 * imageChanged(Mat*) signal if needed
		 * @param edgeMode the new edge mode
		 */
		void setEdgeMode(const int edgeMode);

	signals:
		/**
		 * Signal emitted when kernelSize changed because sigma values (min,
		 * max, step & value) are changed by new kernel size values
		 * @param kernelSize the new kernelSize
		 */
		void kernelSizeChanged(int kernelSize);

		/**
		 * Signal to send when edge mode is activated or deactivated
		 * @param activated the current status of edge mode
		 */
		void edgeModeChanged(const bool activated);

	protected:
		/**
		 * Set a new edge didsplay mode and emit corresponding
		 * imageChanged(Mat*) signal if needed
		 * @param edgeMode the new edge mode
		 * @param standalone if setEdgeMode is used by itself then true, but
		 * false when used inside setDisplayMode
		 */
		void setEdgeMode_Impl(const int edgeMode, const bool standalone = true);
};

#endif /* QCVGFILTER_H_ */
