/*
 * QcvHistograms.cpp
 *
 *  Created on: 14 févr. 2012
 *      Author: davidroussel
 */

#include <functional>	// for bind

#include <QDebug>	// for qDebug() << ...
#include <QTime>	// for time in debug

#include "QcvHistograms.h"

/*
 * QcvHistograms constructor
 * @param image the source image
 * @param computeGray indicates if an aditionnal gray level histogram
 * should be computed
 * @param drawHeight histogram drawing height
 * @param drawWidth histogram drawing width
 * @param timeCumulation indicates if timecumulation is on for histogram
 * computation
 * @param imageLock the mutex for concurrent access to the source image.
 * In order to avoid concurrent access to the same image
 * @param updateThread the thread in which this processor should run
 * @param parent parent QObject
 * @param settings the settings used to load or save parameters value
 */
QcvHistograms::QcvHistograms(Mat * image,
							 QMutex * imageLock,
							 QThread * updateThread,
							 const bool computeGray,
							 const size_t drawHeight,
							 const size_t drawWidth,
							 const bool timeCumulation,
							 QObject * parent,
							 QSettings * settings) :
	CvProcessor(image),
	QcvProcessor(image, imageLock, updateThread, parent, NB_LOCKS, settings),
	CvHistograms8UC3(image, computeGray, drawHeight, drawWidth, timeCumulation),
	redChannelController(&showComponentRed,
						 nullptr, // Locks are used in #setShowComponent(...);
						 new function<void(const bool)>(std::bind(&QcvHistograms::setShowComponent,
																  this,
																  size_t(integral(ColorIdx::HIST_RED)),
																  std::placeholders::_1)),
						 (updateThread == nullptr ? this : nullptr),
						 settings,
						 "process/show_channels/red"),
	greenChannelController(&showComponentGreen,
						   nullptr, // Locks are used in #setShowComponent(...);
						   new function<void(const bool)>(std::bind(&QcvHistograms::setShowComponent,
																	this,
																	size_t(integral(ColorIdx::HIST_GREEN)),
																	std::placeholders::_1)),
						   (updateThread == nullptr ? this : nullptr),
						   settings,
						   "process/show_channels/green"),
	blueChannelController(&showComponentBlue,
						  nullptr, // Locks are used in #setShowComponent(...);
						  new function<void(const bool)>(std::bind(&QcvHistograms::setShowComponent,
																   this,
																   size_t(integral(ColorIdx::HIST_BLUE)),
																   std::placeholders::_1)),
						  (updateThread == nullptr ? this : nullptr),
						  settings,
						  "process/show_channels/blue"),
	grayChannelController(&showComponentGray,
						  nullptr, // Locks are used in #setShowComponent(...);
						  new function<void(const bool)>(std::bind(&QcvHistograms::setShowComponent,
																   this,
																   size_t(integral(ColorIdx::HIST_GRAY)),
																   std::placeholders::_1)),
						  (updateThread == nullptr ? this : nullptr),
						  settings,
						  "process/show_channels/gray"),
	histModeController(&histogramMode,
					   CvHistograms8UC3::HistMode::NORMAL,
					   CvHistograms8UC3::HistMode::NbHistModes,
					   histogramsModeNames,
					   locks[HIST_IM],
					   nullptr, // no callback, the controller handles it itself
					   (updateThread == nullptr ? this : nullptr),
					   settings,
					   "process/histogram/mode"),
	lutController(&lutType,
				  CvHistograms8UC3::LUTType::NONE,
				  CvHistograms8UC3::LUTType::NBTRANS,
				  lutNames,
				  nullptr, // Lock is already used in #setLutType
				  new function<void(const int)>(std::bind(&QcvHistograms::setLutType,
														  this,
														  std::placeholders::_1)),
				  (updateThread == nullptr ? this : nullptr),
				  settings,
				  "process/histogram/lut"),
	dynamicTypeController(&dynamicType,
						  CvHistograms8UC3::DynamicType::LOW,
						  CvHistograms8UC3::DynamicType::NBTYPES,
						  dynNames,
						  nullptr, // Lock is already used in #setDynamicType
						  new function<void(const int)>(std::bind(&QcvHistograms::setDynamicType,
																 this,
																 std::placeholders::_1)),
						  (updateThread == nullptr ? this : nullptr),
						  settings,
						  "process/histogram/dynamic_type"),
	colorLUTController(&colorLUT,
					   locks[LUT_COMPUTE],
					   nullptr,// no callback, the controller handles it itself
					   (updateThread == nullptr ? this : nullptr),
					   settings,
					   "process/histogram/color_lut"),
	lutParamController(&lutParam,
					   locks[LUT_COMPUTE],
					   1,
					   new function<void(const int)>(std::bind(&CvHistograms8UC3::setLUTParam,
															   this,
															   std::placeholders::_1)),
					   (updateThread == nullptr ? this : nullptr),
					   settings,
					   "process/histogram/lut_param"),
	clipLimitController(&claheClipLimit,
						locks[LUT_COMPUTE],
						1.0,
						new function<void(const double)>(std::bind(&CvHistograms8UC3::setCLAHEClipLimit,
																   this,
																   std::placeholders::_1)),
						(updateThread == nullptr ? this : nullptr),
						settings,
						"process/CLAHE/clip_limit"),
	tilingController(&claheTiling,
					   locks[LUT_COMPUTE],
					   1,
					   new function<void(const int)>(std::bind(&CvHistograms8UC3::setCLAHETiling,
															   this,
															   std::placeholders::_1)),
					   (updateThread == nullptr ? this : nullptr),
					   settings,
					   "process/CLAHE/tile_size"),
	postProcessingController(&postProcessing,
							 locks[LUT_COMPUTE],
							 new function<void(const bool)>(std::bind(&CvHistograms8UC3::setPostProcessing,
																	  this,
																	  std::placeholders::_1)),
							 (updateThread == nullptr ? this : nullptr),
							 settings,
							 "process/post_processing/post_processing"),
	kernelSizeController(&kernelSize,
						 locks[LUT_COMPUTE],
						 1,
						 new function<void(const int)>(std::bind(&CvHistograms8UC3::setKernelSize,
																 this,
																 std::placeholders::_1)),
						 (updateThread == nullptr ? this : nullptr),
						 settings,
						 "process/post_processing/kernel_size")
{
	QcvProcessor::setNumberFormat("%7.0f");
}

/*
 * QImageHistogram destructor
 */
QcvHistograms::~QcvHistograms()
{
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[TERMINATE] << ";"
//					   << "will lock";
	lockMutexes(TERMINATE); // ~QcvProcessor() will unlock & delete non null mutexes
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[TERMINATE] << ";"
//					   << "locked";
	updateHistogramTime1String.clear();
	computeLUTTimeString.clear();
	drawLUTTimeString.clear();
	applyLUTTimeString.clear();
	updateHistogramTime2String.clear();
	drawHistogramTimeString.clear();
}

/*
 * Red Channel controller accessor
 * @return a pointer to the Red Channel Controller
 */
QBoolController * QcvHistograms::getRedChannelController()
{
	return &redChannelController;
}

/*
 * Green Channel controller accessor
 * @return a pointer to the Green Channel Controller
 */
QBoolController * QcvHistograms::getGreenChannelController()
{
	return &greenChannelController;
}

/*
 * Blue Channel controller accessor
 * @return a pointer to the Blue Channel Controller
 */
QBoolController * QcvHistograms::getBlueChannelController()
{
	return &blueChannelController;
}

/*
 * Gray Channel controller accessor
 * @return a pointer to the Gray Channel Controller
 */
QBoolController * QcvHistograms::getGrayChannelController()
{
	return &grayChannelController;
}

/*
 * Hist. Mode controller accessor
 * @return a pointer to the Hist. Mode Controller
 */
QEnumController * QcvHistograms::getHistModeController()
{
	return &histModeController;
}

/*
 * LUT Type controller accessor
 * @return a pointer to the Hist. Mode Controller
 */
QEnumController * QcvHistograms::getLUTController()
{
	return &lutController;
}

/*
 * Optimal Dynamic Type controller accessor
 * @return a pointer to the Optimal Dynamic type controller
 */
QEnumController * QcvHistograms::getDynamicTypeController()
{
	return &dynamicTypeController;
}

/*
 * Color LUT controller accessor
 * @return a pointer to the Gray Channel Controller
 */
QBoolController * QcvHistograms::getColorLUTController()
{
	return &colorLUTController;
}

/*
 * LUT Parameter controller accessor
 * @return a pointer to the LUT Paramater Controller
 */
QRangeIntController * QcvHistograms::getLUTParamController()
{
	return &lutParamController;
}

/*
 * CLAHE Clip Limt controller
 * @return a pointer to the CLAHE Clip limit controller
 */
QRangeDoubleController * QcvHistograms::getCLipLimitController()
{
	return &clipLimitController;
}

/*
 * CLAHE Tile Size controller
 * @return a pointer to the CLAHE Tile Size controller
 */
QRangeIntController * QcvHistograms::getTilingController()
{
	return &tilingController;
}

/*
 * Post processing controller
 * @return a pointer to the post processing controller
 */
QBoolController * QcvHistograms::getPostProcessingController()
{
	return &postProcessingController;
}

/*
 * Post processing filter size controller
 * @return a pointer to the post processing filter size controller
 */
QRangeIntController * QcvHistograms::getKernelSizeController()
{
	return &kernelSizeController;
}

/*
 * Update computed images and sends displayImageChanged signal if
 * required
 */
void QcvHistograms::update()
{
	if (!tryLockMutexes(TERMINATE))
	{
//		qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//						   << QThread::currentThreadId() << ";"
//						   << Q_FUNC_INFO << ";"
//						   << locks[TERMINATE] << ";"
//						   <<  "interrupted by destructor";
		return;
	}

//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[SOURCE]
//					   << locks[HIST_COMPUTE_1]
//					   << locks[LUT_COMPUTE]
//					   << locks[HIST_COMPUTE_2]
//					   << locks[TRANSFORMED_IM]
//					   << locks[LUT_IM]
//					   << locks[HIST_IM] << ";"
//					   <<  "will lock";
	lockMutexes(SOURCE,
				HIST_COMPUTE_1,
				LUT_COMPUTE,
				LUT_IM,
				TRANSFORMED_IM,
				HIST_COMPUTE_2,
				HIST_IM);
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[SOURCE]
//					   << locks[HIST_COMPUTE_1]
//					   << locks[LUT_COMPUTE]
//					   << locks[HIST_COMPUTE_2]
//					   << locks[TRANSFORMED_IM]
//					   << locks[LUT_IM]
//					   << locks[HIST_IM] << ";"
//					   <<  "locked";

	CvHistograms8UC3::update();

	unlockMutexes(SOURCE,
				  HIST_COMPUTE_1,
				  LUT_COMPUTE,
				  LUT_IM,
				  TRANSFORMED_IM,
				  HIST_COMPUTE_2,
				  HIST_IM);
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[SOURCE]
//					   << locks[HIST_COMPUTE_1]
//					   << locks[LUT_COMPUTE]
//					   << locks[HIST_COMPUTE_2]
//					   << locks[TRANSFORMED_IM]
//					   << locks[LUT_IM]
//					   << locks[HIST_IM] << ";"
//					   <<  "unlocked";

	/*
	 * emit updated signal
	 */
	QcvProcessor::update(); // emits updated signal
//	qDebug() << Q_FUNC_INFO << "emits histogram image updated";
	emit histogramImageUpdated();
//	qDebug() << Q_FUNC_INFO << "emits lut image updated";
	emit lutImageUpdated();

	/*
	 * emit time measurement signals
	 */
	const char * format = meanStdFormat.toStdString().c_str();
	updateHistogramTime1String.sprintf(format,
									   getMeanProcessTime(integral(TimeIdx::UPDATE_HISTOGRAM)) / 1000.0,
									   getStdProcessTime(integral(TimeIdx::UPDATE_HISTOGRAM)) / 1000.0);
	emit(histogramTime1Updated(updateHistogramTime1String));

	computeLUTTimeString.sprintf(format,
								 getMeanProcessTime(integral(TimeIdx::COMPUTE_LUT)) / 1000.0,
								 getStdProcessTime(integral(TimeIdx::COMPUTE_LUT)) / 1000.0);
	emit(computeLUTTimeUpdated(computeLUTTimeString));
	if (isLUTUpdated())
	{
		drawLUTTimeString.sprintf(format,
								  getMeanProcessTime(integral(TimeIdx::DRAW_LUT)) / 1000.0,
								  getStdProcessTime(integral(TimeIdx::DRAW_LUT)) / 1000.0);
		emit(drawLUTTimeUpdated(drawLUTTimeString));
	}

	applyLUTTimeString.sprintf(format,
							   getMeanProcessTime(integral(TimeIdx::APPLY_LUT)) / 1000.0,
							   getStdProcessTime(integral(TimeIdx::APPLY_LUT)) / 1000.0);
	emit(applyLUTTimeUpdated(applyLUTTimeString));

	if ((lut != NULL) && (lutType != LUTType::NONE))
	{
		updateHistogramTime2String.sprintf(format,
										   getMeanProcessTime(integral(TimeIdx::UPDATE_HISTOGRAM_AFTER_LUT)) / 1000.0,
										   getStdProcessTime(integral(TimeIdx::UPDATE_HISTOGRAM_AFTER_LUT)) / 1000.0);
		emit(histogramTime2Updated(updateHistogramTime2String));
	}

	drawHistogramTimeString.sprintf(format,
									getMeanProcessTime(integral(TimeIdx::DRAW_HISTOGRAM)) / 1000.0,
									getStdProcessTime(integral(TimeIdx::DRAW_HISTOGRAM)) / 1000.0);
	emit(drawHistogramTimeUpdated(drawHistogramTimeString));

	unlockMutexes(TERMINATE);
}

/*
 * Changes source image slot.
 * Attributes needs to be cleaned up then set up again
 * @param image the new source Image
 */
void QcvHistograms::setSourceImage(Mat * image) throw (CvProcessorException)
{
	Size previousSize(sourceImage->size());
	int previousNbChannels(nbChannels);

	// Locks all Locks
	for (int i = 0; i < nbLocks; i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->lock();
		}
	}

	CvProcessor::setSourceImage(image);

	// Unlocks all Locks
	for (int i = 0; i < nbLocks; i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->unlock();
		}
	}

	qDebug() << "Emits image change with source image";
	emit imageChanged(sourceImage);

	emit imageChanged();

	if ((previousSize.width != image->cols) ||
		(previousSize.height != image->rows))
	{
		emit imageSizeChanged();
	}

	if (previousNbChannels != nbChannels)
	{
		emit imageColorsChanged();
	}

	// notifies any connected component to change source images
	emit imageChanged(&outDisplayFrame);
	emit histogramImageChanged(&histDisplayFrame);
	emit lutImageChanged(&lutDisplayFrame);
}

/*
 * Ith histogram component shown status write access
 * @param i the ith histogram component
 * @param value the value to set for this component show status
 */
void QcvHistograms::setShowComponent(const size_t i, const bool value)
{
	lockMutexes(LUT_IM, HIST_IM);

	CvHistograms8UC3::setShowComponent(i, value);

	unlockMutexes(LUT_IM, HIST_IM);

	message.clear();
	switch ((ColorIdx)i)
	{
		case ColorIdx::HIST_RED:
			message.append(tr("Red"));
			break;
		case ColorIdx::HIST_GREEN:
			message.append(tr("Green"));
			break;
		case ColorIdx::HIST_BLUE:
			message.append(tr("Blue"));
			break;
		case ColorIdx::HIST_GRAY:
			message.append(tr("Gray"));
			break;
		default:
			message.append(tr("Unkown"));
			break;
	}
	message.append(tr(" histogram Component is "));

	if (value)
	{
		message.append(tr("on"));
	}
	else
	{
		message.append(tr("off"));
	}

//	qDebug() << message;

	emit sendMessage(message, defaultTimeOut);
}

/*
 * Sets the current LUT type
 * @param lutType the new LUT type
 */
void QcvHistograms::setLutType(const int lutType)
{
	lockMutexes(LUT_COMPUTE);

	CvHistograms8UC3::setLutType(static_cast<LUTType>(lutType));

	unlockMutexes(LUT_COMPUTE);

	message.clear();
	message.append(tr("Current transfert function is "));
	switch (static_cast<LUTType>(lutType))
	{
		case LUTType::NONE:
			message.append(tr("Identity"));
			break;
		case LUTType::NEGATIVE:
			message.append(tr("Inverse"));
			break;
		case LUTType::GAMMA:
			message.append(tr("Gamma"));
			break;
		case LUTType::THRESHOLD:
			if (colorLUT)
			{
				message.append(tr("Threshold based on color histograms"));
			}
			else
			{
				message.append(tr("Threshold based on gray histogram"));
			}
			break;
		case LUTType::DYNAMIC:
			if (colorLUT)
			{
				message.append(tr("Optimal dynamic based on color histograms"));
			}
			else
			{
				message.append(tr("Optimal dynamic based on gray histogram"));
			}
			break;
		case LUTType::EQUALIZE:
			if (colorLUT)
			{
				message.append(tr("Equalize based on color histograms"));
			}
			else
			{
				message.append(tr("Equalize based on gray histogram"));
			}
			break;
		case LUTType::CLAHE:
			if (colorLUT)
			{
				message.append(tr("Contrast Limited Adaptive Histogram Equalization on color channels"));
			}
			else
			{
				message.append(tr("Contrast Limited Adaptive Histogram Equalization on gray channel"));
			}
			break;
		default:
			message.append(tr("unknown"));
			break;
	}

	emit sendMessage(message, defaultTimeOut);
}

/*
 * Current Optimal Dynamic type with notification
 * @param dynamicType the new Optimal Dynamic type
 */
void QcvHistograms::setDynamicType(const int dynamicType)
{
	lockMutexes(LUT_COMPUTE);

	CvHistograms8UC3::setDynamicType(static_cast<DynamicType>(dynamicType));

	unlockMutexes(LUT_COMPUTE);

	message.clear();
	message.append(tr("Current Optimal dynamic is on "));
	switch (static_cast<DynamicType>(dynamicType))
	{
		case DynamicType::LOW:
			message.append(tr("Low part"));
			break;
		case DynamicType::HIGH:
			message.append(tr("High part"));
			break;
		case DynamicType::LOWHIGH:
			message.append(tr("Low and High parts"));
			break;
		default:
			message.append(tr("unknown part"));
			break;
	}
	message.append(" of histogram");

	emit sendMessage(message, defaultTimeOut);
}

/*
 * Reset mean and std process time in order to re-start computing
 * new mean and std process time values.
 */
void QcvHistograms::resetMeanProcessTime()
{
	CvHistograms8UC3::resetMeanProcessTime();
}
