 /**
 * @brief definitiong of tablemodel inherit QAbstractTableModel
 */
#ifndef TABLEMODEL_H
#define TABLEMODEL_H
#include "datastruct.h"

#include <QAbstractTableModel>
#include <QVector>
#include <QIcon>

class Tablemodel:public QAbstractTableModel
{
public:
    /**
     * @brief constructor
     * @param i current index
     * @param addf // true->people, false->organisation
     * @param pObj
     */
    Tablemodel(int i, bool addf = true, QObject *pObj=nullptr);
    ~Tablemodel() override;

    /**
     * @param parent
     * @return the number of the rows of tablemodel
     */
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    /**
     * @param parent
     * @return the number of the columns of tablemodel
     */
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    /**
     * @param index
     * @param role
     * @return the data at index of tablemodel
     */
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    /**
     * @brief set data at index
     * @param index
     * @param value
     * @param role
     * @return
     */
    virtual bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;
    /**
     * @brief define headerdata
     * @param section
     * @param orientation
     * @param role
     * @return
     */
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    /**
     * @brief define item flag
     * @param index
     * @return
     */
    virtual Qt::ItemFlags flags(const QModelIndex & index) const override;

    /**
     * @brief a getter of vector peoples
     * @return
     */
    QVector<People> getPeoples() const;
    /**
     * @brief a setter of vector peoples
     * @param p
     */
    void setPeoples(const QVector<People>& p){peoples = p;}
    QVector<People> peoples;
    QVector<Organisation> organisation;
    int getIndex() const{return m_index;}
private:
    bool addFlag;// true -> people false->organisation
    int m_index; //current index for vectors
};

#endif // TABLEMODEL_H
