/*
 * CvRegistrar.h
 *
 *  Created on: 27 mars 2012
 *	  Author: davidroussel
 */

#ifndef CVREGISTRAR_H_
#define CVREGISTRAR_H_

#include <vector>
#include <ctime>	// for clock_t
using namespace std;

#include <opencv2/core/core.hpp>	// Mat, Point2f
#include <opencv2/calib3d/calib3d.hpp>	// for solvePNP algrorithms
using namespace cv;

#include <CvProcessor.h>
#include <utils/Pose.h>
#include <utils/RangeValue.h>
#include <utils/MeanValue.h>

/**
 * OpenCV class to register points detected in a model image against
 * coplanar points detected in a scene image:
 * 	- We suppose these two sets of points have already been matched together
 * 	with a matcher such as CvMatcher.
 * 	- If the number of points is greater than 4 we can compute an homography
 * 	between these two sets of points and then appluy this homography on model
 * 	points to see where they project on the scene image.
 * 	We can the compare reprojected model points and scene points to remove
 * 	outliers model reprojected points that projects too far away from scene
 * 	points. Hence getting a subset of inliers points that project correctly
 * 	on the scene image with homography.
 * 	- If we know the size of the printed model in the scene space then we can
 * 	convert model points to real scale 3D points lying on a z=0 plane.
 * 	- If the number of points is greater than 6 AND if camera intrinsic
 * 	parameters are available (from camera calibration), then we can compute
 * 	the camera extrinsic parameters (pose and orientation of the camera
 * 	relative to the printed model)
 * @author David Roussel
 * @date 2012/03/27
 */
class CvRegistrar : public CvProcessor
{
	public:
		/**
		 * Indexes for requested pose: current, corrected or predicted
		 */
		enum struct RequestedPose : size_t
		{
			CURRENT = 0,	//!< Current pose independently of the pose filter
			CORRECTED,		//!< Pose corrected by the pose filter
			PREDICTED		//!< Pose predicted by the pose filter for next frame
		};

		/**
		 * Method used to find an object pose from 3D-2D point correspondences
		 * @see calib3d.hpp in OpenCV library
		 */
		enum struct SolvePNP : int
		{
			ITERATIVE = SOLVEPNP_ITERATIVE, //!< Iterative pose computation method
			EPNP      = SOLVEPNP_EPNP, //!< EPnP: Efficient Perspective-n-Point Camera Pose Estimation (lepetit2009epnp)
			P3P       = SOLVEPNP_P3P, //!< Complete Solution Classification for the Perspective-Three-Point Problem (gao2003complete)
			DLS       = SOLVEPNP_DLS, //!< A Direct Least-Squares (DLS) Method for PnP  (hesch2011direct)
			UPNP      = SOLVEPNP_UPNP,  //!< Exhaustive Linearization for Robust Camera Pose and Focal Length Estimation (penate2013exhaustive)
			NB_POSE_COMPUTING_METHODS = 5
		};

	protected:
		/**
		 * Model matched image points to register on scene points
		 */
		vector<Point2f> * modelPoints;

		/**
		 * Scene matched image points to register with model points
		 */
		vector<Point2f> * scenePoints;

		/**
		 * Number of points in model and scene points
		 */
		size_t nbPoints;

		/**
		 * Homography to compute.
		 * Homography contains the transformation to apply on model points
		 * to find them in scene points.
		 */
		Mat homography;

		/**
		 * Indicates homography has been succesfully computed.
		 * So 2D Model points can be reprojected to Scene space
		 */
		bool homographyOk;

		/**
		 * Model points reprojected on scene space with homography
		 */
		vector<Point2f> reprojModelPoints;

		/**
		 * Reprojection threshold error distance to consider inliers
		 * from outliers: [0.1...1.0...10.0](±0.1)
		 */
		RangeValue<double> reprojThreshold = {1.0, 0.1, 10.0, 0.1};

		/**
		 * Desired probability that the PNP algorithm produces a useful result:
		 * [0.50...0.99...1.00](±0.01)
		 */
		RangeValue<double> pnpConfidence = {0.99, 0.50, 1.00, 0.01};

		/**
		 * Number of inliers when reproject in model points on scene space
		 * with homography.
		 * Inliers are reprojected points closer than reprojThreshold.
		 */
		int nbInliers;

		/**
		 * Indices of inliers points based on scene points reprojected on
		 * scene image.
		 */
		vector<int> inliersIndex;

		/**
		 * Mean reprojection error when reprojecting model points on scene
		 * space whith computed homography
		 */
		MeanValue<double> meanReprojectionError;

		/**
		 * Matched Model points converted to scene space by scaling model matched
		 * image points with printScale.
		 * Used to solve camera pose by using 3D/2D correspondences between
		 * these points and matched scene points (2D) with RANSAC
		 */
		vector<Point3f> modelMatchedPoints3D;

		/**
		 * Inliers Model points converted to scene space by scaling model inliers
		 * image points with printScale.
		 * Used to solve camera pose by using 3D/2D correspondences between
		 * these points and inliers scene points (2D)
		 */
		vector<Point3f> modelInliersPoints3D;

		/**
		 * Inliers scene points.
		 * Used to solve camera pose by using 2D/3D correspondences between
		 * these points and inliers model points scaled to printed model (3D)
		 */
		vector<Point2f> sceneInliersPoints2D;

		/**
		 * Indicates if camera pose should be computed or not.
		 * if computePose is true then printScale, cameraMatrix and
		 * distortionCoefs must be set in order to compute camera pose
		 */
		bool computePose;

		/**
		 * Choose pose computing method:
		 *	- iterative mode with SOLVEPNP_ITERATIVE
		 *	- EPnP: Efficient Perspective-n-Point Camera Pose Estimation with
		 *	SOLVEPNP_EPNP
		 *	- Complete Solution Classification for the Perspective-Three-Point
		 *	Problem with SOLVEPNP_P3P
		 *	- A Direct Least-Squares (DLS) Method for PnP with SOLVEPNP_DLS
		 *	- Exhaustive Linearization for Robust Camera Pose and Focal Length
		 *	Estimation with SOLVEPNP_UPNP
		 */
		SolvePNP poseComputingMethod;

		/**
		 * Use solvePnP with or without Ransac to avoid outliers
		 */
		bool useRansacPnP;

		/**
		 * When solvePnP uses Ransac, this is the number of iterations
		 * used in the ransac process as well as the minimum number of
		 * iterations before ransac stops iterating: [10...100...1000](±10)
		 */
		RangeValue<int> ransacPnPIterations = {100, 10, 1000, 10};

		/**
		 * PrintScale : the ratio to apply on model points pixel values
		 * to obtain model points paper scale values in order to transform
		 * model image points to real scale model points and solve camera pose
		 * with 2D scene images points / 3D model points correspondances.
		 */
		double printScale;

		/**
		 * Camera Matrix (if any)
		 */
		Mat cameraMatrix;

		/**
		 * Distortion coefs (if any)
		 */
		Mat distortionCoefs;

		/**
		 * Indicates Camera matrix is set and print scale is valid so
		 * pose can be estimated
		 */
		bool cameraSet;

		/**
		 * Extrinsic rotation vector obtained from estimating pose
		 */
		Mat rotationVec;

		/**
		 * Extrinsic translation vector obtained from estimating pose
		 */
		Mat translationVec;

		/**
		 * If true the previous computed pose (rotationVec and translationVec)
		 * is used to initialize pose computing and further optimized by
		 * pose computing
		 */
		bool usePreviousPose;

		/**
		 * Indicates camera pose has been successfully computed.
		 * So 3D points can be reprojected to Scene space
		 */
		bool poseOk;

		/**
		 * Current pose as obtained by solvePNP
		 */
		Pose currentPose;

		/**
		 * Indicates if current pose should be printed on the console
		 */
		bool recordPose;

		/**
		 * Indicates the index of the recoreded pose
		 */
		size_t poseStep;

	public:
		/**
		 * Registrar constructor
		 * @param sourceImage The source image (not used here except for drawing)
		 * @param modelPoints model matched points from matcher
		 * @param scenePoints scene matched points from matcher
		 * @param reprojectionError maximum reprojection error to sort model
		 * points reprojected in scene space as outliers or inliers
		 * @param computePose should pose be computed.
		 * In such case the three next parameters should be also set
		 * @param printScale Print scale relating model image points to
		 * printed model real scale points
		 * @param cameraMatrix camera calibration intrinsic coefs matrix
		 * @param distortionCoefs camera calibration distotion coefs matrix
		 * @param verboseLevel verbosity
		 */
		CvRegistrar(Mat * sourceImage,
					vector<Point2f> * modelPoints = NULL,
					vector<Point2f> * scenePoints = NULL,
					const double reprojectionError = 3.0,
					const bool computePose = false,
					double printScale = 0.0,
					const Mat & cameraMatrix = Mat(),
					const Mat & distortionCoefs = Mat(),
					const VerboseLevel verboseLevel = CvProcessor::VERBOSE_NONE);

		/**
		 * Registrar destructor
		 */
		virtual ~CvRegistrar();

		/**
		 * Get the source image
		 * @return a reference to the source image
		 */
		const Mat & getDisplayImage() const;

		/**
		 * Get the pointer to the source image
		 * @return a pointer to the source image
		 */
		Mat * getDisplayImagePtr();

		/**
		 * Find homography and/or camera pose from 2D scene points and 3D
		 * model points correspondances
		 * @par Algo Algotithm:
		 *	- calls #homographyComputing(); then if #computePose && homographyOk
		 *	- calls #poseComputing();
		 *	- if !#homographyOk then predict Pose and updates pose filter with
		 *	prediction
		 */
		virtual void update();

		/**
		 * Conditional update.
		 * launch #update only if condition is true
		 * @param condition
		 */
		virtual void update(const bool condition);

		/**
		 * Compute homography.
		 * Needs to be reimplemented in subclasses to send signal when done
		 * @par Algo Algorithm:
		 *	- If the number of matched points is bigger than 4 then
		 *		- Find #homography between matched model and scene points
		 *		- Reproject model points to scene using the homography
		 *		- For each reprojected model points if the reprojection error
		 *		with scene points is smaller than #reprojThreshold then we have
		 *		an inlier so add the current index to the #inliersIndex and
		 *		updates the #meanReprojectionError and set #homographyOk to true
		 *	- Else set #homographyOk to false
		 */
		virtual void homographyComputing();

		/**
		 * Compute camera pose.
		 * Needs to be reimplemented in subclasses to send signal when done
		 * @par Algo Algorithm:
		 *	- If camera is set (calibrated)
		 *		- If number of (inliers > 6) or (we use ransac and nb of points > 6)
		 *			- Create #modelMatchedPoints3D which is a set of 3D points
		 *			obtained from matched model 2D points scaled to target
		 *			printScale in 3D.
		 *			- Extract #modelInliersPoints3D from these points using
		 *			#inliersIndex
		 *			- Extract #sceneInliersPoints2D from scene points using
		 *			#inliersIndex
		 *			- Solve Pose using one of the solvePNP algos between
		 *			#modelInliersPoints3D and #sceneInliersPoints2D and set
		 *			#poseOk to true
		 *			- if #correctedPose then predict pose in #predictedPose
		 *			(just to see)
		 *			- update #currentPose with #translationVec & #rotationVec
		 *			from the solve PNP algo
		 *			- if #correctPose, update the #poseFilter with #currentPose
		 *			and compute #correctedPose from filter
		 *		- Else
		 *			- set #poseOk to false
		 *			- if #correctPose
		 *				- predict pose
		 *				- update #poseFilter with predicted pose
		 *		- increment #poseStep
		 */
		virtual void poseComputing();

		/**
		 * Get current, corrected or predicted pose based on the #poseFilter
		 * @return the current pose
		 */
		const Pose & getPose() const;

		/**
		 * Reproject model points into scene points by applying an homography
		 * on them
		 * @param points points vector to be reprojected (points.size() = N)
		 * @param rPoints points transformed by homography
		 */
		void reprojectPoints2D(const vector<Point2f> & points,
							   vector<Point2f> & rPoints);

		/**
		 * Reproject 3D points from Model to 2D point in image using extrinsic
		 * parameters matrix from pose evaluation and intrinsic parameters
		 * matrix from calibration
		 * @param points3D 3D points vector to project
		 * @param pose the camera pose (position/orientation) to use to reproject
		 * 3D points to 2D points
		 * @param points2D 2D projected points vector
		 */
		void reprojectPoints3D(const vector<Point3f> & points3D,
							   const Pose & pose,
							   vector<Point2f> & points2D);

		/**
		 * Set model image poinst
		 * @param modelPoints the new model image points
		 */
		virtual void setModelPoints(vector<Point2f> *modelPoints);

		/**
		 * Set scene image points
		 * @param scenePoints the new scene image points
		 */
		virtual void setScenePoints(vector<Point2f> *scenePoints);

		/**
		 * Gets the number of points in model and scene points
		 * @return the number of points in model and scene points
		 */
		size_t getNbPoints() const;

		/**
		 * Get the current homography
		 * @return the current homography
		 * @note default homography might be identity if no bomography has
		 * been computed yet
		 */
		const Mat & getHomography() const;

		/**
		 * Get computed homography status
		 * @return true if homography has been computed, false if homography
		 * has not been computed yet or if there ise not enough points
		 * to compute homography (we need at least 4 points to compute
		 * homography)
		 */
		bool isHomographyOk() const;

		/**
		 * Gets model points reprojected in scene space by homography
		 * @return the model points reprojected in scene space by homography
		 */
		const vector<Point2f> & getReprojModelPoints() const;

		/**
		 * Get the current reprojection error threshold to sort oultliers from
		 * inliers
		 * @return the current reprojection error threshold
		 */
		double getReprojThreshold() const;

		/**
		 * Set a new reprojection error threshold to sort inliers from
		 * outliers
		 * @param reprojThreshold the new reprojection error threshold
		 */
		virtual void setReprojThreshold(const double reprojThreshold);

//		/**
//		 * Get the maximum reprojection error threshold to sort oultliers from
//		 * inliers
//		 * @return the maximum reprojection error threshold
//		 */
//		static double getReprojThresholdMax();

		/**
		 * Get the current desired PNP algorithm confidence
		 * @return the current deisred PNP algorithm confidence
		 */
		double getPnPConfidence() const;

		/**
		 * Set a new PNP algorithm desired confidence [0.0 ... 1.0[
		 * @param pnpConfidence the new PNP confidence value
		 * @post the new PNP confidence value is set iff it is contained within
		 * [0.0 ... 1.0[
		 */
		virtual void setPnPConfidence(const double pnpConfidence);

		/**
		 * Get the number of inliers after reprojecting model points to
		 * scene space
		 * @return the number of inliers
		 */
		int getNbInliers() const;

		/**
		 * Gets the indices of inliers points amongst model and scene points
		 * in order to extract inliers points from these two sets
		 * @return the indices of inliers points amongst model and scene points
		 */
		const vector<int> & getInliersIndexes() const;

		/**
		 * Gets the current mean reprojection error when reprojecting model
		 * points to scene space with homography
		 * @return the current mean reprojection error
		 */
		double getMeanReprojectionError() const;

		/**
		 * Gets model points converted to 3D Real space (with z = 0) using
		 * printScale
		 * @return the 3D model points in real space
		 */
		const vector<Point3f> & getInliersModelPoints3D() const;

		/**
		 * Gets the current print scale. Scale factor between model image
		 * points and model space points wich depends on the printed model image
		 * size.
		 * @return the current print scale.
		 */
		double getPrintScale() const;

		/**
		 * Set a new printscale for converting model image points to real space
		 * points
		 * @param printScale the new printscale
		 * @return true if printScale have been set and all camera parameters
		 * and printscale are correct (acccording to isCameraSet)
		 */
		virtual bool setPrintScale(const double printScale);

		/**
		 * Sets Camera matrix in order to compute camera pose
		 * @param cameraMatrix the new camera matrix
		 * @return true if camera matrix have been set and all camera parameters
		 * and printscale are correct (acccording to isCameraSet)
		 */
		virtual bool setCameraMatrix(const Mat & cameraMatrix);

		/**
		 * Sets new Camera distorsions coefficients
		 * @param distortionCoefs the new camera distorsions coefficients
		 * @return true if distortions have been set and all camera parameters
		 * and printscale are correct (acccording to isCameraSet)
		 */
		virtual bool setDistortionCoefs(const Mat & distortionCoefs);

//		/**
//		 * Checks camera matrix is set (along with distortion coefs) and
//		 * printScale is valid so pose computation could be computed
//		 * @return true if camera matrix (and evt distortion coefs) is set
//		 * and printScale is valie
//		 * @post set cameraSet attribute
//		 */
//		bool checkCameraSet();

		/**
		 * Indicates whether camera matrix and camera distorsions coefficients
		 * have been set (in order to solve camera pose)
		 * @return true if camera intrinsisc parameters matrix and distorsions
		 * coefficients have been set, false otherwise
		 */
		bool isCameraSet();

		/**
		 * Get the compute camera pose status
		 * @return the current camera pose status
		 */
		bool isComputePose() const;

		/**
		 * Set a new camera pose computing status
		 * @param computePose the new camera pose computing status
		 * @return if computePose is set to be true isCameraSet is called first
		 * to check all camera parameters and printScale are set correctly
		 * So the result might not be equal to computePose if all camera
		 * parameters are not set porperly
		 */
		virtual bool setComputePose(const bool computePose);

		/**
		 * get current pose computing mode:
		 * 	- CV_ITERATIVE: iterative mode
		 * 	- CV_EPNP : E-PnP mode
		 * @return the current pose computing methof index
		 */
		int getPoseComputingMethod() const;

		/**
		 * set new pose computing method if it is recognized
		 * @param poseComputingMethod the new pose computing method:
		 * 	- SOLVEPNP_ITERATIVE: iterative mode
		 * 	- SOLVEPNP_EPNP: EPnP: Efficient Perspective-n-Point Camera Pose Estimation
		 *	- SOLVEPNP_P3P: Complete Solution Classification for the Perspective-Three-Point Problem
		 *	- SOLVEPNP_DLS: A Direct Least-Squares (DLS) Method for PnP
		 *	- SOLVEPNP_UPNP: Exhaustive Linearization for Robust Camera Pose and Focal Length Estimation
		 */
		void setPoseComputingMethod(const SolvePNP poseComputingMethod);

		/**
		 * Get the current number of ransac iterations when pose computing
		 * uses ransac.
		 * @return current number of ransac iterations
		 */
		int getRansacPnPIterations() const;

		/**
		 * Sets a new number of iterations when pose computing
		 * uses ransac.
		 * @param ransacPnPIterations the new number of iterations
		 */
		virtual void setRansacPnPIterations(const int ransacPnPIterations);

		/**
		 * Get the use of ransac in solve PnP
		 * @return true if solve Pnp uses ransac, false otherwise
		 */
		bool isRansacPnP() const;

		/**
		 * Change the use of ransac in solve PnP
		 * @param useRansacPnP the new ransac use status
		 */
		virtual void setRansacPnP(const bool useRansacPnP);

		/**
		 * Get the current camera rotation vector computed by solving camera
		 * pose
		 * @return the current camera pose rotation vector
		 */
		const Mat & getRotationVec() const;

		/**
		 * Get the current camera translation vector computed by solving camera
		 * pose
		 * @return the current camera pose rotation vector
		 */
		const Mat & getTranslationVec() const;

		/**
		 * Get the current use previous pose status in pose computing.
		 * If usePreviousPose is true then pose computing is initialized
		 * with current rotationVec et translationVec and further optimized
		 * in pose computing
		 * @return the current "use previous pose" status in pose computing
		 */
		bool isUsePreviousPose();

		/**
		 * Set a new "use previous pose" status in pose computing
		 * @param use the new "use previous pose" status
		 */
		virtual void setUsePreviousPose(const bool use);

		/**
		 * Get computed pose status
		 * @return true if number of inliers are greater than 6, camera
		 * intrinsic matrix is set and camera pose has been computed, false
		 * otherwise
		 */
		bool isPoseOk() const;

		/**
		 * Get the current verbose level
		 * @return the current verbose level
		 */
		CvProcessor::VerboseLevel getVerboseLevel() const;

		/**
		 * Set new verbose level
		 * @param level the new verobse level
		 */
		virtual void setVerboseLevel(const CvProcessor::VerboseLevel level);

		/**
		 * Gets the registration time in ticks or ticks/inliers points
		 * depending on CvProcessor#timePerFeature attibute
		 * @param index index of the step which processing time is required,
		 * not used here
		 * @return number of ticks used to process current image in absolute
		 * time or time per inliers points depending on
		 * CvProcessor#timePerFeature attribute
		 * @see CvProcessor#timePerFeature
		 */
		double getProcessTime(const size_t index = 0) const;

		/**
		 * Gets the mean registration time in ticks or ticks/inliers points
		 * depending on CvProcessor#timePerFeature attibute
		 * @param index index of the step which processing mean time is required,
		 * not used here
		 * @return mean number of ticks used to process current image in absolute
		 * time or time per inliers points depending on
		 * CvProcessor#timePerFeature attribute
		 * @see CvProcessor#timePerFeature
		 */
		double getMeanProcessTime(const size_t index = 0) const;

		/**
		 * Gets the registration time stddev in ticks or ticks/inliers points
		 * depending on CvProcessor#timePerFeature attibute
		 * @param index index of the step which processing time standard
		 * deviation is required, not used here
		 * @return number of ticks deviation from mean used to process current
		 * image in absolute time or time per inliers points
		 */
		double getStdProcessTime(const size_t index = 0) const;

		/**
		 * Get record pose state
		 * @return the record pose state
		 */
		bool isRecordPose() const;

		/**
		 * Set record pose state
		 * @param value the new record pose state
		 */
		virtual void setRecordPose(const bool value);
};

#endif /* CVREGISTRAR_H_ */
