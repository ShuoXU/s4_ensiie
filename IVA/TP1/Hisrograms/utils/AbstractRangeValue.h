/*
 * AbstractRangeValue.h
 *
 *  Created on: 17 mai 2015
 *      Author: davidroussel
 */

#ifndef ABSTRACTRANGEVALUE_H_
#define ABSTRACTRANGEVALUE_H_

#include <cstddef>	// for size_t
#include <iostream>	// for ostream
#include <tuple>	// for tuple
#include <initializer_list>	// for initializer lists
#include <typeinfo>			// for typeid
using namespace std;

// Forward declaration of friend functions and operators
template <typename T> class AbstractRangeValue;
template <typename T> ostream & operator <<(ostream & out, const AbstractRangeValue<T> & p);

/**
 * Bounded numeric value template.
 * AbstractRangeValue is a value contained in a range [minValue .. maxValue] with
 * eventually a stepValue allowing to determine the next or the previous value
 * of the current value (within the range), by adding or multiply with the step
 * value depending on the mode (additive or multiplicative)
 * 	- Additive mode:
 * 		- value should be within [min ... max]
 * 		- max should be reachable by value + (n * step)
 * 		- min should be reachable by value - (n * step)
 * 		- step should be positive and non null
 *	- Multiplicative mode:
 *		- value should be within [min ... max]
 * 		- max should be reachable by value * step^n
 * 		- min should be reachable by value / step^n
 * 		- step should be positive and non null
 * 		- therefore min, value and max should be positives
 */
template <typename T> class AbstractRangeValue
{
	public:
		/**
		 * The kind of values one can get from an AbstractRangeValue
		 */
		enum struct WhichValue : int
		{
			VALUE = 0,	//!< The current value
			DEFAULT_VALUE, //!< The default value one can reset to
			MIN_VALUE,	//!< The minimum value
			MAX_VALUE,	//!< The maximum value
			STEP_VALUE,	//!< The step value
			NB_VALUES	//!< The number of values natures in an AbstractRangeValue
		};

	protected:
		/**
		 * The current value
		 */
		T _value;

		/**
		 * The default value to reset to.
		 * The default value is set as the value used to construct the range
		 * value, then any range value could be reset to its default value with
		 * #reset() method
		 */
		T _defaultValue;

		/**
		 * The minimum value
		 */
		T _minValue;

		/**
		 * The maximum value
		 */
		T _maxValue;

		/**
		 * the step value
		 */
		T _stepValue;

	public:

		/**
		 * Epsilon used to compare values
		 */
		static const T EPSILON;

		// --------------------------------------------------------------------
		// Constructors / destructor
		// --------------------------------------------------------------------

		/**
		 * AbstractRangeValue default constructor (for default constructible
		 * compatibility)
		 */
		AbstractRangeValue();

		/**
		 * Valued constructor with currents value, range values and evt
		 * step value and step mode.
		 * 	- minValue should be less than current value, otherwise it is set to
		 * 	the current value
		 * 	- maxValue should be greater than current value, otherwise it is set
		 * 	to the current value
		 * 	- stepValue and stepMode should be such that the next value should
		 * 	be less than the max value and previous value should be greater than
		 * 	min value, otherwise actual step value is reduced to meet these
		 * 	conditions
		 * @note In any cases minValue, value & maxValue are eventually
		 * reorderded to ensure minValue < value < maxValue
		 * @param value the current value
		 * @param minValue the minimum possible value
		 * @param maxValue the maximum possible value
		 * @param stepValue the step value to get to the next/previous value
		 * [default is 0]
		 */
		AbstractRangeValue(const T & value,
						   const T & minValue,
						   const T & maxValue,
						   const T & stepValue = T(0));

		/**
		 * Valued constructor from tuple
		 * @param tupleValues tuple containing value, min, max & step
		 */
		AbstractRangeValue(const tuple<T, T, T, T> & tupleValues);

		/**
		 * Copy constructor
		 * @param rv another AbstractRangeValue
		 */
		AbstractRangeValue(const AbstractRangeValue<T> & rv);

		/**
		 * Move constructor
		 * @param rv another AbstractRangeValue
		 */
		AbstractRangeValue(AbstractRangeValue<T> && rv);

		/**
		 * Destructor
		 */
		virtual ~AbstractRangeValue();

		// --------------------------------------------------------------------
		// AbstractRangeValue specific operations
		// --------------------------------------------------------------------
		/**
		 * Current value read accessor
		 * @return a copy of the current value
		 */
		T value() const;

		/**
		 * Multiple values read accessor
		 * @param which enum indicating which value is required
		 * @return the required value
		 * @see WhichValue
		 */
		T value(const WhichValue which) const;

		/**
		 * Default value read accessor
		 * @return a copy of the default value
		 */
		T defaultValue() const;

		/**
		 * Min value read accessor
		 * @return a copy of the current min value
		 */
		T min() const;

		/**
		 * Max value read accessor
		 * @return a copy of the current max value
		 */
		T max() const;

		/**
		 * Range size
		 * @return the (max - min) size of the this range
		 */
		T range() const;

		/**
		 * Step value read accessor
		 * @return a copy of the current step value
		 */
		T step() const;

		/**
		 * number of steps in the range
		 * @return number of steps to get from min to max value
		 */
		virtual size_t nbSteps() const = 0;

		/**
		 * the index corresponding to the current value in terms of how many
		 * steps from minimum value.
		 * @return the number of steps from min value to current value
		 */
		virtual size_t index() const = 0;

		/**
		 * Current value setter.
		 * The new value has to be within min/max value range and if there is
		 * a step mode it should be accessible from the current value with a
		 * finite number of steps.
		 * @param value the new current value to set
		 * @return true if the new current value has been set, false otherwise
		 */
		virtual bool setValue(const T & value) = 0;

		/**
		 * Any value setter.
		 * Relies on other specialized setters to set the value according to
		 * the "which" parameter.
		 * @param value the new value to set
		 * @param which an enum value indicating which value to set
		 * @return true if the new value has been set, false otherwise
		 */
		virtual bool setValue(const T & value, const WhichValue which) = 0;

		/**
		 * Current value setter to the closest possible value.
		 * if the new value is > max then the current value is set to max.
		 * if the new value is < min then the current value is set to min.
		 * if the new value is >= min & <= max it is set to the closest accessible
		 * value according to steps.
		 * @param value the new current value to set
		 * @return true if the value in argumenthas been set exactly, false if
		 * the new value is slightly different
		 * @post a new value has been set as the closest value to the argument
		 */
		virtual bool setClosestValue(const T & value);

		/**
		 * Sets the value corresponding to this index, which means the value
		 * located at "index" steps distance from the min value.
		 * @param index the index to set value to
		 * @return true if the index was valid [i.e. corresponding to a value
		 * between min and max] and value has been set, false otherwise.
		 */
		virtual bool setIndex(const size_t index) = 0;

		/**
		 * Max value setter.
		 * The new max value should be greater than the current value
		 * @param value the new max value
		 * @return true if the new max value has been set
		 * @post _stepValue might have been adjusted to avoid overflow by
		 * next value
		 */
		virtual bool setMax(const T & value);

		/**
		 * Min value setter.
		 * The new min value should be less than the current value
		 * @param value the new min value
		 * @return true if the new max value has been set
		 * @post _stepValue might have been adjusted to avoid underflow by
		 * previous value
		 */
		virtual bool setMin(const T & value);

		/**
		 * Step value setter.
		 * The new step value should be such that the next value according to
		 * this new step value is less than the max value and the previous
		 * value should be greater than the min value
		 * @param value the new step value. if this value is <= EPSILON then
		 * this range turns its mode to NONE and can't fetch next or previous
		 * values anymore.
		 * @return true if the new max value has been set
		 */
		virtual bool setStep(const T & value);

		/**
		 * Reset to default value
		 * @return true if the default value has been set, false otherwise
		 */
		virtual bool reset();

		/**
		 * Computes next value
		 * @return the new current value after reaching the next value according
		 * to _stepValue and _stepMode or _maxValue if next value is greater
		 * than _maxValue
		 */
		virtual T next() = 0;

		/**
		 * Indicates this range has a next value < max value
		 * @return true if the next value is < max value and false otherwise or
		 * if the range mode is NONE
		 */
		virtual bool hasNext() const = 0;

		/**
		 * Computes previous value
		 * @return the new current value after reaching the previous value
		 * according to _stepValue and _stepMode or _minValue if previous value
		 * is less than _minValue
		 */
		virtual T previous() = 0;

		/**
		 * Indicates this range has a previous value > min value
		 * @return true if the previous value is > min value and false
		 * otherwise or if the range mode is NONE
		 */
		virtual bool hasPrevious() const = 0;

		// --------------------------------------------------------------------
		// Operators
		// --------------------------------------------------------------------
		/**
		 * Comparison operator with another AbstractRangeValue
		 * @param rv another AbstractRangeValue
		 * @return true if both AbstractRangeValue have the same current, min, max, step
		 * and stepMode values, false otherwise
		 */
		virtual bool operator ==(const AbstractRangeValue<T> & rv) const;

		/**
		 * Difference operator with another AbstractRangeValue
		 * @param rv another AbstractRangeValue
		 * @return false if both AbstractRangeValue have the same current, min, max, step
		 * and stepMode values, true otherwise
		 */
		virtual bool operator !=(const AbstractRangeValue<T> & rv) const;

		/**
		 * Comparison operator with regular value
		 * @param value the regular valur to compare
		 * @return true if the current value is the same as the argument, false
		 * otherwise
		 */
		bool operator ==(const T & value) const;

		/**
		 * Difference operator with regular value
		 * @param value the regular valur to compare
		 * @return false if the current value is the same as the argument, true
		 * otherwise
		 */
		bool operator !=(const T & value) const;

		/**
		 * Less than operator with another Range Value
		 * @param arv the other range value
		 * @return return true if current _value < arv._value, false otherwise
		 */
		bool operator <(const AbstractRangeValue<T> & arv) const;

		/**
		 * Greater than operator with another Range Value
		 * @param arv the other range value
		 * @return return true if current _value > arv._value, false otherwise
		 */
		bool operator >(const AbstractRangeValue<T> & arv) const;

		/**
		 * Less or equal operator with another Range Value
		 * @param arv the other range value
		 * @return return true if current _value <= arv._value, false otherwise
		 */
		bool operator <=(const AbstractRangeValue<T> & arv) const;

		/**
		 * Greater or equal operator with another Range Value
		 * @param arv the other range value
		 * @return return true if current _value <= arv._value, false otherwise
		 */
		bool operator >=(const AbstractRangeValue<T> & arv) const;

		/**
		 * Less than operator with a value of type T
		 * @param value the other range value
		 * @return return true if current _value < value, false otherwise
		 */
		bool operator <(const T & value) const;

		/**
		 * Greater than operator with a value of type T
		 * @param value the other range value
		 * @return return true if current _value > value, false otherwise
		 */
		bool operator >(const T & value) const;

		/**
		 * Less or equal operator with a value of type T
		 * @param value the other range value
		 * @return return true if current _value <= value, false otherwise
		 */
		bool operator <=(const T & value) const;

		/**
		 * Greater or equal operator with a value of type T
		 * @param value the other range value
		 * @return return true if current _value <= value, false otherwise
		 */
		bool operator >=(const T & value) const;

		/**
		 * Copy operator from another AbstractRangeValue.
		 * Replaces the current attributes with those of the other AbstractRangeValue
		 * @param rv another AbstractRangeValue
		 * @return a reference to the modified AbstractRangeValue
		 */
		virtual AbstractRangeValue<T> & operator =(const AbstractRangeValue<T> & rv);

		/**
		 * Move operator from another AbstractRangeValue.
		 * Replaces the current attributes with those of the other AbstractRangeValue
		 * which can then be destroyed.
		 * @param rv another AbstractRangeValue
		 * @return a reference to the modified AbstractRangeValue
		 */
		virtual AbstractRangeValue<T> & operator =(AbstractRangeValue<T> && rv);

		/**
		 * Copy operator from a regular value.
		 * Replaces the current value (iff possible according to the rules of
		 * #setValue) with the value in argument.
		 * @param value a regular value
		 * @return a reference to the AbstractRangeValue (which could be modified or not
		 * depending the the value)
		 * @see #setValue(const T & value)
		 */
		AbstractRangeValue<T> & operator =(const T & value);

		/**
		 * Self addition operator with a regular value.
		 * Self addition of the current value is performed only if the expected
		 * value is reachable through a finite number of next or previous values
		 * iteration and stays within range. Otherwise the current value is
		 * not modified
		 * @param value the value to add to the current value
		 * @return a reference to the AbstractRangeValue
		 */
		AbstractRangeValue<T> & operator +=(const T & value);

		/**
		 * Self substraction operator with a regular value.
		 * Self substraction of the current value is performed only if the
		 * expected value is reachable through a finite number of next or
		 * previous values iterations and stays within range. Otherwise the
		 * current value is not modified
		 * @param value the value to substract to the current value
		 * @return a reference to the AbstractRangeValue
		 */
		AbstractRangeValue<T> & operator -=(const T & value);

		/**
		 * Self multiplication operator with a regular value.
		 * Self multiplication of the current value is performed only if the
		 * expected value is reachable through a finite number of next or
		 * previous values iterations and stays within range. Otherwise the
		 * current value is not modified
		 * @param value the value to multiply by the current value
		 * @return a reference to the AbstractRangeValue
		 */
		AbstractRangeValue<T> & operator *=(const T & value);

		/**
		 * Self division operator with a regular value.
		 * Self division of the current value is performed only if the
		 * expected value is reachable through a finite number of next or
		 * previous values iterations and stays within range. Otherwise the
		 * current value is not modified
		 * @param value the value to divide the current value
		 * @return a reference to the AbstractRangeValue
		 */
		AbstractRangeValue<T> & operator /=(const T & value);

		/*
		 * Addition operator with a regular value.
		 * The argument value is added to the current value iff it is reachable
		 * by a finite number of next or previous values iterations and stays
		 * within range. Otherwise the current value is not modified
		 * @param value the value to add to the current value
		 * @return a new AbstractRangeValue (with eventually) a modified current value
		 */
		// virtual AbstractRangeValue<T> operator +(const T & value) const = 0;

		/*
		 * Substraction operator with a regular value.
		 * The argument value is substracted to the current value iff it is
		 * reachable by a finite number of next or previous values iterations
		 * and stays within range. Otherwise the current value is not modified
		 * @param value the value to add to the current value
		 * @return a new AbstractRangeValue (with eventually) a modified current value
		 */
		// virtual AbstractRangeValue<T> operator -(const T & value) const = 0;

		/*
		 * Multiplication operator with a regular value.
		 * The argument value is multiplied by the current value iff it is
		 * reachable by a finite number of next or previous values iterations
		 * and stays within range. Otherwise the current value is not modified
		 * @param value the value to add to the current value
		 * @return a new AbstractRangeValue (with eventually) a modified current value
		 */
		// virtual AbstractRangeValue<T> operator *(const T & value) const = 0;

		/*
		 * Division operator with a regular value.
		 * The current value is divided byt the argument iff it is
		 * reachable by a finite number of next or previous values iterations
		 * and stays within range. Otherwise the current value is not modified
		 * @param value the value to add to the current value
		 * @return a new AbstractRangeValue (with eventually) a modified current value
		 */
		// virtual AbstractRangeValue<T> operator /(const T & value) const = 0;

		/**
		 * Cast operator to regular value
		 * @return the current value
		 * @par Usage
		 * @code
		 * 	AbstractRangeValue<Type> rv = ...;
		 * 	...
		 * 	Type v1 = (Type)rv;
		 *  ...
		 *  Type v2 = static_cast<Type>(rv);
		 * @endcode
		 */
		operator T() const;

		// --------------------------------------------------------------------
		// Friend functions
		// --------------------------------------------------------------------
		/**
		 * Friend standard output operator
		 * @param out output stream
		 * @param rv AbstractRangeValue to print out
		 * @return a reference to the output stream
		 */
		friend ostream & operator << <>(ostream & out, const AbstractRangeValue<T> & rv);

	public:
		// --------------------------------------------------------------------
		// Utility methods
		// --------------------------------------------------------------------
		/**
		 * Number of increments (positive or negative) needed to reach the
		 * value in argument from the internal #_value
		 * @param value the value to reach
		 * @return the numbr of increments (positive or negative) to reach
		 * this value
		 * @pre value is supposed to be within [#_minValue ... #_maxValue]
		 * otherwise this method might fail.
		 */
		virtual double incrementsToValue(const T value) = 0;

		/**
		 * Number of increments to reach _maxValue from _value
		 * @return the number of increments or multiplications of _value to
		 * reach _maxValue. If this number is not an integer then _maxValue
		 * is not reachable
		 */
		virtual double incrementsToMax() const = 0;

		/**
		 * Number of decrements to reach _minValue from _value
		 * @return the number of decrements or divisions of _value to
		 * reach _minValue. If this number is not an integer then _minValue
		 * is not reachable
		 */
		virtual double incrementsToMin() const = 0;

		/**
		 * Indicates if _maxValue can be reached by multiple incrementation /
		 * multiplication by _stepValue
		 * @return true if _maxValue is reachable, false otherwise
		 */
		bool isMaxReachable() const;

		/**
		 * Indicates if _minValue can be reached by multiple decrementation /
		 * division by _stepValue
		 * @return true if _minValue is reachable, false otherwise
		 */
		bool isMinReachable() const;

		/**
		 * Ensures _maxValue & _minValue are reachable
		 * 	- In additive mode:
		 * 		- _maxValue should be reachable by adding _stepValue n times to
		 * 		_value
		 * 		- _minValue should be reachable by substracting _stepValue n
		 * 		times to _value
		 * 	- In Multiplicative mode:
		 * 		- _maxValue should be reachable by multiplying _value n times
		 * 		by _stepValue
		 * 		- _minValue should be reachable by dividing _value n times
		 * 		by _stepValue
		 * 	_maxValue and _minValue are fixed to the closest reachable values
		 */
		void fixBounds();

		/**
		 * Ensures _maxValue is reachable
		 * 	- In additive mode by adding _stepValue n times to _value
		 * 	- In Multiplicative mode by multiplying _value n times by _stepValue
		 * 	And fix _maxValue to the closest reachable value
		 */
		virtual void fixMax() = 0;

		/**
		 * Ensures _minValue is reachable
		 * 	- In additive mode by substracting _stepValue n times to _value
		 * 	- In Multiplicative mode by dividing _value n times by _stepValue
		 * 	And fix _minValue to the closest reachable value
		 */
		virtual void fixMin() = 0;

		/**
		 * Ensures
		 * 	- _minValue < _value < _maxValue by reordering the three values
		 * 	- if mode is multiplicative _minValue, _value and _maxValue should
		 * 	be positives
		 */
		virtual void fixValueAndRange();

		/**
		 * Ensure _stepValue value consistency:
		 * 	- _stepValue should be positive
		 * 	- If _stepMode is ADDITIVE then _stepValue is such that:
		 * 		- _stepValue < _maxValue - _value
		 * 		- _stepValue < _value - _minValue
		 * 	- If _stepMode is MULTIPLICATIVE then _stepValue is such that:
		 * 		- _stepValue < _maxValue / _value
		 * 		- _stepValue < _minValue * _value
		 * 	- if _stepMode is not NONE, then _stepValue should not be null
		 */
		virtual void fixStep();

		/**
		 * Stream Range value to output stream
		 * @param out the output stream
		 * @return a reference to the modified output stream
		 */
		virtual ostream & toStream(ostream & out) const;

		/**
		 * Stream Range value to output stream template
		 * @tparam Stream  stream type (ostream or QDebug or whatever...)
		 * @param out the output stream
		 * @return a reference to the modified output stream
		 * @note template methods can't be virtual
		 * @note this template method needs to be implemented in the header
		 * so it can be instantiated with type replacing "Stream" in any source
		 * file (.cpp) that needs it.
		 */
		template <typename Stream>
		Stream & toStream_Impl(Stream & out) const
		{
			if (typeid(T) == typeid(int))
			{
				out << "<int>";
			}
			else if (typeid(T) == typeid(float))
			{
				out << "<float>";
			}
			else if (typeid(T) == typeid(double))
			{
				out << "<double>";
			}
			else
			{
				out << "<unknown>";
			}

			out << "[" << _minValue << "..." << _value << "..." << _maxValue << "]";
			return out;
		}
};

#endif /* ABSTRACTRANGEVALUE_H_ */
