/*
 * CvColorSpaces.cpp
 *
 *  Created on: 8 févr. 2012
 *      Author: davidroussel
 */
#include <cassert>	// for assert
#include <iostream>	// for cerr
#include <algorithm> // for min
#include <cmath>
using namespace std;

#include <opencv2/imgproc/imgproc.hpp> // for cvtColor

#include "mapRed.h"
#include "mapGreen.h"
#include "mapBlue.h"
#include "mapHSV.h"
#include "mapCb.h"
#include "mapCr.h"

#include "CvColorSpaces.h"

/*
 * Color spaces constructor
 * @param sourceImage input image
 */
CvColorSpaces::CvColorSpaces(Mat * sourceImage) :
	CvProcessor(sourceImage),
	showColoredChannels(true),
	inFrameGray(sourceImage->size(), CV_8UC1),
	maxBGChannels(sourceImage->size(), CV_8UC1),
	maxBGRChannels(sourceImage->size(), CV_8UC1),
	maxBGChannelsColor(sourceImage->size(), CV_8UC3),
	maxBGRChannelsColor(sourceImage->size(), CV_8UC3),
	inFrameXYZ(sourceImage->size(), CV_64FC3),
	inFrameHSV(sourceImage->size(), CV_8UC3),
	hueColorImage(sourceImage->size(), CV_8UC3),
	hueMixImage(sourceImage->size(), CV_8UC3),
	hueMixedColorImage(sourceImage->size(), CV_8UC3),
	hueDisplay(HueDisplay::HUECOLOR),
	inFrameYCrCb(sourceImage->size(), CV_8UC3),
	crColoredImage(sourceImage->size(), CV_8UC3),
	cbColoredImage(sourceImage->size(), CV_8UC3),
	redMap(mapRed),
	greenMap(mapGreen),
	blueMap(mapBlue),
	hMap(mapHSV),
	cbMap(mapCb),
	crMap(mapCr),
	imageDisplayIndex(Display::INPUT),
	displayImageChanged(false),
	snr(0, 0, 0, 0)
{
	setup(sourceImage, false);
//	addImage("display", &displayImage);
}

/*
 * Color spaces destructor
 */
CvColorSpaces::~CvColorSpaces()
{
	cleanup();
}

/*
 * Setup internal attributes according to source image
 * @param sourceImage a new source image
 * @param fullSetup full setup is needed when source image is changed
 * @pre sourceimage is not NULL
 * @note this method should be reimplemented in sub classes
 */
void CvColorSpaces::setup(Mat * sourceImage, bool fullSetup)
{
//	clog << "CvColorSpaces::"<< (fullSetup ? "full " : "") <<"setup" << endl;

	assert(sourceImage != NULL);

	CvProcessor::setup(sourceImage, fullSetup);

	// Full setup starting point
	if (fullSetup) // only when sourceImage changes
	{
		inFrameGray.create(sourceImage->size(), CV_8UC1);
		maxBGChannels.create(sourceImage->size(), CV_8UC1);
		maxBGRChannels.create(sourceImage->size(), CV_8UC1);
		maxBGChannelsColor.create(sourceImage->size(), CV_8UC3);
		maxBGRChannelsColor.create(sourceImage->size(), CV_8UC3);
		inFrameXYZ.create(sourceImage->size(), CV_64FC3),
		inFrameHSV.create(sourceImage->size(), CV_8UC3);
		hueColorImage.create(sourceImage->size(), CV_8UC3);
		hueMixImage.create(sourceImage->size(), CV_8UC3);
		hueMixedColorImage.create(sourceImage->size(), CV_8UC3);
		inFrameYCrCb.create(sourceImage->size(), CV_8UC3);
		crColoredImage.create(sourceImage->size(), CV_8UC3);
		cbColoredImage.create(sourceImage->size(), CV_8UC3);
		processTime = 0;
	}
	else // only at construction
	{
		bgrMap[0] = &blueMap;
		bgrMap[1] = &greenMap;
		bgrMap[2] = &redMap;

		showColoredChannels = true;
	}

	// Partial setup starting point (in both cases)
	for (int i=0; i < 3; i++)
	{
		bgrChannels.push_back(Mat(sourceImage->size(), CV_8UC1));
		bgrColoredChannels[i].create(sourceImage->size(), CV_8UC3);
		xyzGrayChannels[i].create(sourceImage->size(), CV_64FC1);
		xyzDisplayChannels[i].create(sourceImage->size(), CV_8UC1);
		hsvChannels.push_back(Mat(sourceImage->size(), CV_8UC1));
		hueMixChannels[i].create(sourceImage->size(), CV_8UC1);
		yCrCbChannels.push_back(Mat(sourceImage->size(), CV_8UC1));
	}
}

/*
 * Clean up images before changing source image or terminating
 * CvColorSpaces
 */
void CvColorSpaces::cleanup()
{
//	clog << "CvColorSpaces::cleanup()" << endl;

	cbColoredImage.release();
	crColoredImage.release();
	for (size_t i = 0; i < yCrCbChannels.size(); i++)
	{
		yCrCbChannels[i].release();
	}
	yCrCbChannels.clear();
	inFrameYCrCb.release();

	hueMixedColorImage.release();
	hueMixImage.release();
	for (size_t i = 0; i < 3; i++)
	{
		hueMixChannels[i].release();
	}
	hueColorImage.release();
	for (size_t i = 0; i < hsvChannels.size(); i++)
	{
		hsvChannels[i].release();
	}
	hsvChannels.clear();
	inFrameHSV.release();

	for (size_t i = 0; i < bgrChannels.size(); i++)
	{
		bgrChannels[i].release();
		bgrColoredChannels[i].release();
		xyzGrayChannels[i].release();
		xyzDisplayChannels[i].release();
	}
	bgrChannels.clear();

	inFrameXYZ.release();

	maxBGRChannelsColor.release();

	maxBGChannelsColor.release();

	maxBGRChannels.release();

	maxBGChannels.release();

	inFrameGray.release();

	displayImage.release();

	CvProcessor::cleanup();
}


/*
 * Update compute selected image for display according to
 * selected parameters such as imageDisplayIndex, showColorChannel,
 * and eventually hueDisplay
 * @return true if display image has changed, false otherwise
 */
void CvColorSpaces::update()
{
	clock_t start, end;
	start = clock();
	// ------------------------------------------------------------------------
	// Compute needed images
	// ------------------------------------------------------------------------
	switch (imageDisplayIndex)
	{
		case Display::INPUT:
			// Ain't got nothin to do here : input image doesn't need to be processed
			break;

		// --------------------------------------------------------------------
		// Gray level conversion
		// --------------------------------------------------------------------
		case Display::GRAY:
			// Converts to gray
			// sourceImage -> inFramegray
			// TODO 01 à compléter ...
            cvtColor(*sourceImage, inFrameGray, CV_BGR2GRAY);
			break;

		// --------------------------------------------------------------------
		// RGB Decomposition
		// --------------------------------------------------------------------
		case Display::RED:
		case Display::GREEN:
		case Display::BLUE:
		case Display::MAX_BGR:
			// Split BGR channels : sourceImage -> bgrChannels
			// TODO 03a Compléter ...
            split(*sourceImage, bgrChannels);

			// TODO 03b Build colored image from channels : red channel leads to a
			// red colored image, and so on ...
			// by applying bgrMap[x] on bgrChannels[x] to produce
			// bgrColoredChannels[x]
			// bgrChannels[i] -> bgrColoredChannels[i]
			for (size_t i = 0; i < bgrChannels.size(); i++)
			{
				// TODO 03c Compléter ...
                bgrMap[i]->applyPalette(bgrChannels[i], bgrColoredChannels[i]);
			}

			if (imageDisplayIndex == Display::MAX_BGR)
			{
				if (!showColoredChannels)
				{
					// Compute maximum of BGR channels using cv::max
					// bgrChannels[0 & 1] -> maxBGChannels
					// bgrChannels[2] & maxBGChannels -> maxBGRChannels
					// TODO 03d à compléter ...
                    cv::max(bgrChannels[0], bgrChannels[1], maxBGChannels);
                    cv::max(bgrChannels[2], maxBGChannels, maxBGRChannels);
				}
				else
				{
					// Compute colored maximum of BGR channels using normMax
					// bgrColoredChannels[0 & 1] -> maxBGChannelsColor
					// bgrColoredChannels[2] & maxBGChannelsColor -> maxBGRChannelsColor
					// TODO 03e compléter la fonction normMax puis utiliser normMax ...
                    normMax(bgrColoredChannels[0], bgrColoredChannels[1], maxBGChannelsColor);
                    normMax(bgrColoredChannels[2], maxBGChannelsColor, maxBGRChannelsColor);
				}
			}

			/*
			 * TODO 03g What are the characteristics of blue component vs
			 * green or red ?
			 * Tip: use gray images instead of colored images to compare
			 * Answer below:
			 *
			 */

			break;

		// --------------------------------------------------------------------
		// XYZ conversion
		// --------------------------------------------------------------------
		case Display::XYZ_X:
		case Display::XYZ_Y:
		case Display::XYZ_Z:
			// Converts to XYZ : sourceImage -> inFrameXYZ
			// TODO 02a à compléter ...
            cvtColor(*sourceImage, inFrameXYZ, CV_BGR2XYZ);

			// Splits inFrameXYZ to channels xyzGrayChannels
            // TODO 02b à compléter ...
            convertScaleAbs(inFrameXYZ, *xyzGrayChannels);

			// Converts floating point channels to display channels
			// xyzGrayChannels[...] -> xyzDisplayChannels[...]
			for( size_t i=0; i < 3; i++)
			{
				// TODO 02c à compléter ...

                xyzDisplayChannels[i] = xyzGrayChannels[i].clone();
			}

			/*
			 * TODO 02d What component X, Y or Z looks more like luminance to you ?
			 * Answer below:
			 *
			 */

			break;
		// --------------------------------------------------------------------
		// HSV conversion
		// --------------------------------------------------------------------
		case Display::HUE:
		case Display::SATURATION:
		case Display::VALUE:
			// Converts to HSV : sourceImage -> inFrameHSV
			// TODO 04a à compléter ...
            cvtColor(*sourceImage, inFrameHSV, CV_BGR2HSV);

			// Split HSV channels : inFrameHSV -> hsvChannels
			// TODO 04b à compléter ...
            split(inFrameHSV, hsvChannels);

			// evt show min/max of H component : should be [0...179]°
            //showMinMaxLoc(hsvChannels[0]);

			// Normalize hue from 0 to 255 because hsv colormap (hMap)
			// applied below expects value within 0 to 255 range
			// hsvChannels[0] -> hsvChannels[0]
			// TODO 04c à compléter ...
            convertScaleAbs(hsvChannels[0],hsvChannels[0], 255/179, 0);
			// Build colored Hue image : hsvChannels[0] -> hueColorImage
			// TODO 04d à compléter ...
            hsvChannels[0] = hueColorImage.clone();

			// Build Mixed Hue Color and (Saturation or Value) image
			if ((hueDisplay > HueDisplay::HUECOLOR) &&
				(hueDisplay < HueDisplay::HUEGRAY))
			{
				// Creates a 3 channel image from saturation or value channel
				// depending on hueDisplay value
				// hsvChannels -> hueMixChannels
				// TODO 04e à compléter ...
                hueMixChannels[0] = hsvChannels[0].clone();
                hueMixChannels[1] = hsvChannels[1].clone();
                hueMixChannels[2] = hsvChannels[2].clone();

				// merge mix channels into color image
				// hueMixChannels --> hueMixImage
				// TODO 04f à compléter ...
                merge(*hueMixChannels, hueMixImage);

				// Build colored Hue image \times Saturation or Value
				// hueColorImage x hueMixImage -> hueMixedColorImage
				// TODO 04g à compléter ...
                hueMixedColorImage = hueColorImage * hueMixImage;
			}
			break;

			/*
			 * TODO 04h To what other component the V component of HSV space looks
			 * like ?
			 * Tip: Use gray images instead of colored images to compare.
			 * Answer below:
			 *
			 */

		// --------------------------------------------------------------------
		// YCbCr conversion
		// --------------------------------------------------------------------
		case Display::Y:
		case Display::Cr:
		case Display::Cb:
			// Converts to YCrCb : sourceImage -> inFrameYCrCb
			// TODO 05a à compléter ...
            cvtColor(*sourceImage, inFrameYCrCb, CV_BGR2YCrCb);

			// Split YCrCb channels : inFrameYCrCb -> yCrCbChannels
			// TODO 05b à compléter ...
            split(inFrameYCrCb, yCrCbChannels);

			// Apply palette on cr & cb components
			// crmap, yCrCbChannels[1] -> crColoredImage
			// TODO 05c à compléter ...
            crMap.applyPalette(yCrCbChannels[1], crColoredImage);
			// cbmap, yCrCbChannels[2] -> cbColoredImage
			// TODO 05d à compléter ...
            cbMap.applyPalette(yCrCbChannels[2], cbColoredImage);
			break;
		default:
			cerr << "unknown image display index" << integral(imageDisplayIndex) << endl;
			break;
		/*
		 * TODO 05e How does the Y component compares to the gray component ?
		 * Answer below :
		 */

		/*
		 * TODO 05f What can you tell about the details in Cr or Cb components vs
		 * the details in the Y component ?
		 * Answer below :
		 */
	}

	// ------------------------------------------------------------------------
	// select image to display ...
	// ------------------------------------------------------------------------

	uchar * previousImageData = displayImage.data;

	switch (imageDisplayIndex)
	{
		case Display::INPUT:
			displayImage = *sourceImage;
			break;
		case Display::GRAY:
			displayImage = inFrameGray;
			break;
		case Display::RED:
			if (showColoredChannels)
			{
				displayImage = bgrColoredChannels[integral(RGBIndex::Red)];
			}
			else
			{
				displayImage = bgrChannels[integral(RGBIndex::Red)];
			}
			break;
		case Display::GREEN:
			if (showColoredChannels)
			{
				displayImage = bgrColoredChannels[integral(RGBIndex::Green)];
			}
			else
			{
				displayImage = bgrChannels[integral(RGBIndex::Green)];
			}
			break;
		case Display::BLUE:
			if (showColoredChannels)
			{
				displayImage = bgrColoredChannels[integral(RGBIndex::Blue)];
			}
			else
			{
				displayImage = bgrChannels[integral(RGBIndex::Blue)];
			}
			break;
		case Display::MAX_BGR:
			if (showColoredChannels)
			{
				displayImage = maxBGRChannelsColor;
			}
			else
			{
				displayImage = maxBGRChannels;
			}
			break;
		case Display::XYZ_X:
			displayImage = xyzDisplayChannels[0];
			break;
		case Display::XYZ_Y:
			displayImage = xyzDisplayChannels[1];
			break;
		case Display::XYZ_Z:
			displayImage = xyzDisplayChannels[2];
			break;
		case Display::HUE:
			switch (hueDisplay)
			{
				case HueDisplay::HUECOLOR:
					if (showColoredChannels)
					{
						displayImage = hueColorImage;
					}
					else
					{
						displayImage = hsvChannels[0];
					}
					break;
				case HueDisplay::HUESATURATE:
				case HueDisplay::HUEVALUE:
					displayImage = hueMixedColorImage;
					break;
				case HueDisplay::HUEGRAY:
					displayImage = hsvChannels[0];
					break;
				case HueDisplay::NBHUES:
				default:
					cerr << "unknown Hue display mode "<< integral(hueDisplay)
						 << endl;
					break;
			}
			break;
		case Display::SATURATION:
			displayImage = hsvChannels[1];
			break;
		case Display::VALUE:
			displayImage = hsvChannels[2];
			break;
		case Display::Y:
			displayImage = yCrCbChannels[0];
			break;
		case Display::Cr:
			if (showColoredChannels)
			{
				displayImage = crColoredImage;
			}
			else
			{
				displayImage = yCrCbChannels[1];
			}
			break;
		case Display::Cb:
			if (showColoredChannels)
			{
				displayImage = cbColoredImage;
			}
			else
			{
				displayImage = yCrCbChannels[2];
			}
			break;
		default:
			cerr << "unknown display image index " << integral(imageDisplayIndex) << endl;
			displayImage = *sourceImage;
			break;
	}

	displayImageChanged = previousImageData != displayImage.data;

	computeSignalToNoiseRatio();

//	cout << "SNR = " << snr[0] << ", " << snr[1] << ", " << snr[2] << endl;

	end = clock();
	processTime = (end - start);
	meanProcessTime += processTime;
	if (displayImageChanged)
	{
		resetMeanProcessTime();
	}
}

/*
 * Gets the image selected for display
 * @return the display image
 */
const Mat & CvColorSpaces::getDisplayImage() const
{
	return displayImage;
}

/*
 * Gets Image pointer corresponding to the current displayMode and
 * edgeMode
 * @return Image reference corresponding to the current displayMode and
 * edgeMode
 */
Mat * CvColorSpaces::getDisplayImagePtr()
{
	return &displayImage;
}

/*
 * Get currently selected image index
 * @return the currently selected image for display index
 */
CvColorSpaces::Display CvColorSpaces::getDisplayImageIndex()
{
	return imageDisplayIndex;
}

/*
 * Select image to set in displayImage :
 * 	- INPUT selects input image for display
 * 	- GRAY selects gray converted input image for display
 * 	- RED selects BGR red component image for display
 * 	- GREEN selects BGR green component image for display
 * 	- BLUE selects BGR blue component image for display
 * 	- HUE selects HSV hue component image for display
 * 	- SATURATION selects HSV saturation component image for display
 * 	- VALUE selects HSV value component image for display
 * 	- Y selects YCrCb Y component image for display
 * 	- Cr selects YCrCb Cr component image for display
 * 	- Cb selects YCrCb Cb component image for display
 * @param select the index to select display image
 */
void CvColorSpaces::setDisplayImageIndex(const Display index)
{
	if (index < Display::NBDISPLAY)
	{
		imageDisplayIndex = index;
		processTime = 0;
	}
	else
	{
		cerr << __PRETTY_FUNCTION__ << " : index " << integral(index)
			 << " out of bounds" << endl;
	}
}

/*
 * Read accessor for #showColoredChannels
 * @return the current #showColoredChannels
 */
bool CvColorSpaces::isChannelsColor() const
{
	return showColoredChannels;
}

/*
 * Write accessor for #showColoredChannels
 * @param status the new colored channels status
 */
void CvColorSpaces::setChannelsColor(const bool status)
{
	showColoredChannels = status;
}

/*
 * Get currently selected hue display mode
 * @return the currenlty selected hue display mode
 */
CvColorSpaces::HueDisplay CvColorSpaces::getHueDisplaymode()
{
	return hueDisplay;
}

/*
 * Select hue display mode :
 *	- HUECOLOR Normal Hue mode
 *	- HUESATURATE Hue*Saturatin mode
 *	- HUEVALUE Hue*Value mode
 *	- HUEGRAY Gray mode
 * @param mode the mode so select
 */
void CvColorSpaces::setHueDisplayMode(const HueDisplay mode)
{
	if (mode < HueDisplay::NBHUES)
	{
		hueDisplay = mode;
		processTime = 0;
	}
	else
	{
		cerr << __PRETTY_FUNCTION__ << " : index " << integral(mode)
			 << " out of bounds" << endl;
	}
}

/*
 * Get Signal/Noise Ratio on channel "channel" as computed by
 * #computeSignalToNoiseRatio();
 * @param channel the channel to get the SNR on
 * @return the SNR of this channel
 * @see #snr
 */
double CvColorSpaces::getSNR(const size_t channel)
{
	size_t realChannel = std::min<size_t>(channel, 2u);
	return snr[realChannel];
}

/*
 * Current block size for SNR computing
 * @return current block size for SNR computing
 * @see #snrSize
 */
int CvColorSpaces::getSNRSize() const
{
	return snrSize.value();
}

/*
 * Block size for SNR computing setter
 * @param value the new block size for snr comuting
 * @return true if the value in argument has been set exactly, false if
 * the new value is slightly different
 * @see #snrSize
 */
void CvColorSpaces::setSNRSize(const int value)
{
	snrSize.setClosestValue(value);
}


/*
 * Compute signal to noise ratio in the current #displayImage
 */
void CvColorSpaces::computeSignalToNoiseRatio()
{
	Scalar meanValue(0.0, 0.0, 0.0, 0.0);
	Scalar stdValue(0.0, 0.0, 0.0, 0.0);

//	if (snrSize > 1)
//	{
		size_t nbHBlocks = size_t(ceil(double(displayImage.cols) / double(snrSize)));
		size_t nbVBlocks = size_t(ceil(double(displayImage.rows) / double(snrSize)));
		size_t nbBlocks = nbHBlocks * nbVBlocks;
		for (size_t v = 0; v < nbVBlocks; ++v)
		{
			int top = v * snrSize;
			int bottom = top + snrSize - 1;
			for(size_t h = 0; h < nbHBlocks; ++h)
			{
				int left = h * snrSize;
				int right = left + snrSize - 1;

				Rect roi(Point2i(left, top),
						 Point2i(min(right, displayImage.cols),
								 min(bottom, displayImage.rows)));
				Mat block = displayImage(roi);
				Scalar localMean;
				Scalar localStd;
				cv::meanStdDev(block, localMean, localStd);
				meanValue += localMean;
				stdValue += localStd;
			}
		}

		meanValue /= double(nbBlocks);
		stdValue /= double(nbBlocks);
//	}
//	else
//	{
//		cv::meanStdDev(displayImage, meanValue, stdValue);
//	}

	for (size_t i = 0; i < componentsSize; ++i)
	{
		if (stdValue[i] != 0.0)
		{
			snr[i] = 10 * log10(meanValue[i] / stdValue[i]);
		}
		else
		{
			snr[i] = 0.0;
		}
	}

	// cout << "mean = " << meanValue << ", std = " << stdValue << ", snr = " << snr << endl;
}

/*
 * Show Min and Max values and locations for a matrix
 * @param m the matrix to consider
 */
void CvColorSpaces::showMinMaxLoc(const Mat & m)
{
	// search for min & max value locations
	double minVal, maxVal;
	Point minLoc, maxLoc;
	minMaxLoc(m,&minVal, &maxVal, &minLoc, &maxLoc);
	clog << "sat values : min = " << minVal << " at (" << minLoc.x
		 << ", " << minLoc.y << ") max = " << maxVal << " at ("
		 << maxLoc.x << ", " << maxLoc.y << ")" << endl;
}

/*
 * Compute Maximum of color images by computing a channel wide norm
 * to find which is the greatest rather than mixing channels
 * @param src1 the first color (or gray) image
 * @param src2 the second color (or gray) image
 * @param dst the color (or gray) destination
 */
void CvColorSpaces::normMax(const Mat& src1, const Mat& src2, Mat& dst)
{
	// first check if src1, src2 && dst have the same sizes and type
	if ( (src1.rows == src2.rows) &&
		 (src1.rows == dst.rows) &&
		 (src1.cols == src2.cols) &&
		 (src1.cols == dst.cols) &&
		 (src1.type() == src2.type()) &&
		 (src1.type() == dst.type()) )
	{
		if (src1.type() == CV_8UC3)
		{
			// Compute max by pixel norm rather than mixing pixels
			for(int i = 0; i < src1.rows; ++i)
			{
				for (int j = 0; j < src1.cols; ++j)
				{
					/*
					 * TODO 03f compute pixel norms from src1 & src2 using
					 * ddot (scalar product) on each pixel
					 * the result (dst) is the pixel with the greatest norm
					 */
                    Vec3b p1 = src1.at<Vec3b>(i,j);
                    Vec3b p2 = src2.at<Vec3b>(i,j);
                    if (p1.ddot(p1)>p2.ddot(p2)){
                        dst.at<Vec3b>(i,j) = src1.at<Vec3b>(i,j);
                    }
                    else{
                        dst.at<Vec3b>(i,j) = src2.at<Vec3b>(i,j);
                    }
				}
			}
		}
		else
		{
			// compute max the regular way with max function
			cv::max(src1, src2, dst);
		}
	}
	else
	{
		// Do nothing
		cerr << __PRETTY_FUNCTION__ << " : incompatible images" << endl;
	}
}
