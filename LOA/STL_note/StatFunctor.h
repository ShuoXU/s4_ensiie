/*
 * StatFunctor.h
 *
 *  Created on: 21 févr. 2013
 *      Author: davidroussel
 */

#ifndef STATFUNCTOR_H_
#define STATFUNCTOR_H_

#include <functional>	// for unary_function
using namespace std;

#include "TypeException.h"

/**
 * Foncteur unaire pour calculer la moyenne et l'écart type d'un ensemble
 * de valeurs
 * @tparam T type des valeurs
 * @note Attention, la valeur moyenne et l'écart type doivent renvoyer des
 * valeurs "double" quel que soit le type T.
 */
template<class T>
#if __cplusplus < 201703L
class StatFunctor: public unary_function<T, void>
#else
class StatFunctor
#endif
{
	private:
		/*
		 * TODO Attributs
		 */
		double sum;
		double sum_carre;
		int size;
	public:

		/*
		 * TODO Méthodes
		 * 	- Constructeur
		 * 	- opérateur d'appel de fonction
		 * 	- mean
		 * 	- std
		 * 	- reset
		 */
		//StatFuntor(const T & value):subValue(value){};

		/**
		 * Constructeur par défaut.
		 * Initialise la somme des éléments, la somme des éléments au carré
		 * ainsi que le nombre d'éléments à 0.
		 * @throw TypeException si le type T n'est pas arithmetique
		 */
		#if __cplusplus < 201703L
		StatFunctor() throw (TypeException);
		#else
		StatFunctor() noexcept(false);
		#endif

		/**
		 * TODO Opérateur d'appel de fonction
		 * @param value valeur à ajouter à la somme des valeurs et à la somme
		 * des valeurs au carré.
		 * @post le nombre d'éléments est incrémenté.
		 */
		void operator()(const T & n);
		/*
		 * TODO calcul de la valeur de la moyenne : E(X) = sum/nbElements
		 * @return la valeur moyenne des valeurs additionnées.
		 */
		double moyenne();
		/**
		 * TODO calcul de l'écart type : sqrt(E(X^2) - E(X)^2)
		 * @return l'écart type des valeurs additionnées
		 */
		double ecart_type ();
		/**
		 * TODO Remise à zéro des statistiques
		 * @post la somme des élements, la somme des éléments au carré ainsi
		 * que le nombre d'éléments ont été remis à 0
		 */
		void remise();
};

#endif /* STATFUNCTOR_H_ */
