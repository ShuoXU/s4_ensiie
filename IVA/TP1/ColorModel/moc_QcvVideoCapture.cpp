/****************************************************************************
** Meta object code from reading C++ file 'QcvVideoCapture.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Qcv/capture/QcvVideoCapture.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QcvVideoCapture.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QcvVideoCapture_t {
    QByteArrayData data[22];
    char stringdata0[228];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QcvVideoCapture_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QcvVideoCapture_t qt_meta_stringdata_QcvVideoCapture = {
    {
QT_MOC_LITERAL(0, 0, 15), // "QcvVideoCapture"
QT_MOC_LITERAL(1, 16, 8), // "finished"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 16), // "frameRateChanged"
QT_MOC_LITERAL(4, 43, 5), // "value"
QT_MOC_LITERAL(5, 49, 27), // "constrainedFrameRateChanged"
QT_MOC_LITERAL(6, 77, 9), // "restarted"
QT_MOC_LITERAL(7, 87, 12), // "synchronized"
QT_MOC_LITERAL(8, 100, 4), // "sync"
QT_MOC_LITERAL(9, 105, 10), // "openDevice"
QT_MOC_LITERAL(10, 116, 8), // "deviceId"
QT_MOC_LITERAL(11, 125, 6), // "size_t"
QT_MOC_LITERAL(12, 132, 5), // "width"
QT_MOC_LITERAL(13, 138, 6), // "height"
QT_MOC_LITERAL(14, 145, 8), // "openFile"
QT_MOC_LITERAL(15, 154, 8), // "fileName"
QT_MOC_LITERAL(16, 163, 10), // "setFlipped"
QT_MOC_LITERAL(17, 174, 7), // "setGray"
QT_MOC_LITERAL(18, 182, 15), // "setSynchronized"
QT_MOC_LITERAL(19, 198, 12), // "setFrameRate"
QT_MOC_LITERAL(20, 211, 9), // "frameRate"
QT_MOC_LITERAL(21, 221, 6) // "finish"

    },
    "QcvVideoCapture\0finished\0\0frameRateChanged\0"
    "value\0constrainedFrameRateChanged\0"
    "restarted\0synchronized\0sync\0openDevice\0"
    "deviceId\0size_t\0width\0height\0openFile\0"
    "fileName\0setFlipped\0setGray\0setSynchronized\0"
    "setFrameRate\0frameRate\0finish"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QcvVideoCapture[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x06 /* Public */,
       3,    1,   95,    2, 0x06 /* Public */,
       5,    1,   98,    2, 0x06 /* Public */,
       6,    0,  101,    2, 0x06 /* Public */,
       7,    1,  102,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    3,  105,    2, 0x0a /* Public */,
       9,    2,  112,    2, 0x2a /* Public | MethodCloned */,
       9,    1,  117,    2, 0x2a /* Public | MethodCloned */,
      14,    3,  120,    2, 0x0a /* Public */,
      14,    2,  127,    2, 0x2a /* Public | MethodCloned */,
      14,    1,  132,    2, 0x2a /* Public | MethodCloned */,
      16,    1,  135,    2, 0x0a /* Public */,
      17,    1,  138,    2, 0x0a /* Public */,
      18,    1,  141,    2, 0x0a /* Public */,
      19,    1,  144,    2, 0x0a /* Public */,
      21,    0,  147,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    4,
    QMetaType::Void, QMetaType::Double,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    8,

 // slots: parameters
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 11, 0x80000000 | 11,   10,   12,   13,
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 11,   10,   12,
    QMetaType::Bool, QMetaType::Int,   10,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 11, 0x80000000 | 11,   15,   12,   13,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 11,   15,   12,
    QMetaType::Bool, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Void, QMetaType::Double,   20,
    QMetaType::Void,

       0        // eod
};

void QcvVideoCapture::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QcvVideoCapture *_t = static_cast<QcvVideoCapture *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->finished(); break;
        case 1: _t->frameRateChanged((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 2: _t->constrainedFrameRateChanged((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 3: _t->restarted(); break;
        case 4: _t->synchronized((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 5: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 7: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 8: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 9: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 10: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 11: _t->setFlipped((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 12: _t->setGray((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 13: _t->setSynchronized((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 14: _t->setFrameRate((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 15: _t->finish(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QcvVideoCapture::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvVideoCapture::finished)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QcvVideoCapture::*_t)(const double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvVideoCapture::frameRateChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QcvVideoCapture::*_t)(const double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvVideoCapture::constrainedFrameRateChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QcvVideoCapture::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvVideoCapture::restarted)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QcvVideoCapture::*_t)(const bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvVideoCapture::synchronized)) {
                *result = 4;
                return;
            }
        }
    }
}

const QMetaObject QcvVideoCapture::staticMetaObject = {
    { &QcvCapture::staticMetaObject, qt_meta_stringdata_QcvVideoCapture.data,
      qt_meta_data_QcvVideoCapture,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QcvVideoCapture::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QcvVideoCapture::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QcvVideoCapture.stringdata0))
        return static_cast<void*>(this);
    return QcvCapture::qt_metacast(_clname);
}

int QcvVideoCapture::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QcvCapture::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void QcvVideoCapture::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QcvVideoCapture::frameRateChanged(const double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QcvVideoCapture::constrainedFrameRateChanged(const double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QcvVideoCapture::restarted()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void QcvVideoCapture::synchronized(const bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
