#include "CvProcessorException.h"

#include "QcvRangeValueIntWidget.h"
#include "ui_QcvRangeValueIntWidget.h"

/*
 * Valued constructor
 * @param rangeValue the range value to control
 * @param title the title of the widget
 * @param description values description to appear on the tooltip
 * @param lock the mutex to check before setting new values
 * @param options flags indicating the presence of the spinner, the slider
 * and the reset button (all by default)
 * @param parent the parent widget (if any)
 * @throw exception if rangeValue is null
 */
QcvRangeValueIntWidget::QcvRangeValueIntWidget(RangeValue<int> * rangeValue,
											   const QString & title,
											   const QString & description,
											   QcvRangeValueControlInt::Options options,
											   QMutex * lock,
											   QWidget * parent) throw (exception) :
	QWidget(parent),
	QcvRangeValueControlInt(rangeValue, options, lock),
	ui(new Ui::QcvRangeValueIntWidget)
{
	ui->setupUi(this);

	// No spinner
	if (options.testFlag(QcvRangeValueControlInt::NoSpinner))
	{
		ui->horizontalLayout_1->removeWidget(ui->spinBox);
		// Do Not destroy the spinbox since it's private slot
		// actually changes the range value
	}

	// No reset button
	if (options.testFlag(QcvRangeValueControlInt::NoResetButton))
	{
		ui->horizontalLayout_1->removeWidget(ui->resetButton);
		ui->resetButton->setParent(nullptr);
		ui->resetButton->deleteLater();
	}

	// No slider
	if (options.testFlag(QcvRangeValueControlInt::NoSlider))
	{
		ui->horizontalLayout_2->removeWidget(ui->slider);
		ui->slider->setParent(nullptr);
		ui->slider->deleteLater();
	}

	// All widgets on a single line
	if (options.testFlag(QcvRangeValueControlInt::SingleLine))
	{
		if (!options.testFlag(QcvRangeValueControlInt::NoResetButton))
		{
			ui->horizontalLayout_1->removeWidget(ui->resetButton);
		}
		if (!options.testFlag(QcvRangeValueControlInt::NoSpinner))
		{
			ui->horizontalLayout_1->removeWidget(ui->spinBox);
		}
		if (!options.testFlag(QcvRangeValueControlInt::NoSlider))
		{
			ui->horizontalLayout_2->removeWidget(ui->slider);
			ui->horizontalLayout_1->addWidget(ui->slider);
		}
		if (!options.testFlag(QcvRangeValueControlInt::NoSpinner))
		{
			ui->horizontalLayout_1->addWidget(ui->spinBox);
		}
		if (!options.testFlag(QcvRangeValueControlInt::NoResetButton))
		{
			ui->horizontalLayout_1->addWidget(ui->resetButton);
		}
	}

	ui->titleLabel->setText(title);
	ui->titleLabel->setToolTip(description);

	int min = rangeValue->min();
	int max = rangeValue->max();
	int value = rangeValue->value();
	int step = rangeValue->step();
	if (!options.testFlag(QcvRangeValueControlInt::NoSpinner))
	{
		ui->spinBox->blockSignals(true);
		ui->spinBox->setMinimum(min);
		ui->spinBox->setMinimum(max);
		ui->spinBox->setSingleStep(step);
		ui->spinBox->setValue(value);
		ui->spinBox->blockSignals(false);
	}

	if (!options.testFlag(QcvRangeValueControlInt::NoSlider))
	{
		ui->slider->blockSignals(true);
		ui->slider->setMinimum(min);
		ui->slider->setMaximum(max);
		ui->slider->setSingleStep(step);
		ui->slider->setValue(value);
		ui->slider->blockSignals(false);
	}

//	qApp->processEvents();
}

QcvRangeValueIntWidget::~QcvRangeValueIntWidget()
{
	delete ui;
}

/*
 * Slot used when reset button is hit
 */
void QcvRangeValueIntWidget::on_resetButton_clicked()
{
	resetValue();
	emit valueChanged(rangeValue->value());
}

/*
 * Slot used when spinbox value changes
 * @param value the new value to set
 * @post the closest value has been set in the #rangeValue and
 * depending on the result a #valueChanged() signal is emitted
 * and evt a #message(...) signal is emitted if setting value failed
 */
void QcvRangeValueIntWidget::on_spinBox_valueChanged(int value)
{
	bool result = setValue(value);

	emit valueChanged(rangeValue->value());

	if (!result)
	{
		emit message(ui->titleLabel->text() + " set value " +
					 QString::number(value) + " failed");
	}
}
