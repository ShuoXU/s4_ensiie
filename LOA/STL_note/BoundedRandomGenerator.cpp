/*
 * BoundedRandomGenerator.cpp
 *
 *  Created on: 21 févr. 2013
 *      Author: davidroussel
 */

#include "BoundedRandomGenerator.h"
#include <cstdlib>	// for random & srandom
#include <ctime>	// for time

// ----------------------------------------------------------------------------
// TODO BoundedRandomGenerator implementation
// ----------------------------------------------------------------------------

template <class T>
BoundedRandomGenerator<T>::BoundedRandomGenerator(const T & min, const T & max)
:min(min),max(max)
	// TODO Liste d'initialisation
{
	// TODO initialisation du random generator
	srandom(time(NULL));
	//this->min = min;
	//this->max = max;

}

/*
 * TODO Opérateur d'appel de fonction
 * @return un nouveau nombre aléatoire compris entre min et max
 */
template <class T>
T BoundedRandomGenerator<T>::operator()(void)
{

	T t;
	t = ((double)rand()/RAND_MAX)*(this->max - this->min)+ this->min;
	return t;
};


// ----------------------------------------------------------------------------
// Proto instanciations
// ----------------------------------------------------------------------------
/*
 * Instanciation du template BoundedRandomGenerator avec des <int>
 */
template class BoundedRandomGenerator<int>;

/*
 * Instanciation du template BoundedRandomGenerator avec des <float>
 */
template class BoundedRandomGenerator<float>;

/*
 * Instanciation du template BoundedRandomGenerator avec des <double>
 */
template class BoundedRandomGenerator<double>;
