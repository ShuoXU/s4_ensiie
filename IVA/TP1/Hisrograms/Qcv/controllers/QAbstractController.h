/*
 * QAbstractController.h
 *
 *  Created on: 16 juin 2015
 *      Author: davidroussel
 */

#ifndef QABSTRACTCONTROLLER_H_
#define QABSTRACTCONTROLLER_H_

#include <functional>	// for binary predicates constraints
using namespace std;

#include <QMutex>
#include <QSet>
#include <QList>
#include <QMultiMap>
#include <QSettings>

/**
 * Abstract controller for a value of type T allowing subclasses to
 * setup and connect to various widgets in the UI without having to setup
 * calllbacks.
 * 	- The controller contains an element of type T which could manage values of
 * 	type V (for example T = RangeValue<V>). Or more simply V can be equal to T.
 * 	- The controller contains a list of widget connected to this controller
 * 	- the controller contains a list of other controllers which share a constraint
 * 	with this controller (constraints are stored in a seperate list).
 * 	- The constraints are of type function<bool(V, V)>
 * 	- One of the connected widget might send a signal triggering a
 * 	setValue (future slot in subclasses), then the setValue changes the
 * 	value (or not, depending on the constraints) and sends back a signal
 * 	indicating the value has been modified. In order to avoid signal/slot
 * 	infinite loops, all connected widgets signals are blocked before emitting
 * 	the signal, then signals in connected widgets are restored
 * @tparam T the type of value managed by this controller.
 *	- T must have a cast operator to V in order to cast T value into V:
 *		operator V() const
 *	- T must also have a copy operator from V allowing V values to be copied
 *	into T values:
 *		T & operator =(const V & value);
 * @tparam V the type of values inside T and used to set value and check
 * predicates allowing the value to be set
 */
template <typename T, typename V = T>
class QAbstractController
{
	protected:

		/**
		 * The value to be managed by this controller.
		 * @note this value is a pointer since it already exists in the
		 * CvProcessor, this class only manages the relationship between
		 * this value and one or several widgets in the UI representing this
		 * value
		 */
		T * const managedValue;

		/**
		 * Mutex to use before changing a value.
		 * Typically this is the mutex of the QCvProcessor owning the
		 * value. This mutex could also be NULL is no atomicity is required
		 * when modifying the value
		 */
		QMutex * lock;

		/**
		 * List of widgets and actions connected to this controller
		 */
		QList<QObject *> sources;

		/**
		 * Signal blocking status of each connected widget or action
		 */
		QList<bool> sourceLockStatus;

		/**
		 * List of other controllers which have a constraint with the current
		 * controller
		 */
		QList<QAbstractController<T, V> *> constraintControllers;

		/**
		 * List of predicates that should be checked before changing a value
		 * such that all
		 * @code
		 * constraints[i](value, otherControllers[i]->value)
		 * @endcode
		 * are true
		 */
		QList<function<bool(V, V)> > constraints;

		/**
		 * Multi map of linked controllers in order to propagate specific
		 * values to specific controllers when a value is set in #setValue
		 * @see #addLinkedValue
		 * @see #removeLinkedValue
		 */
		QMultiMap<V, QAbstractController<T, V> *> linkedControllers;

		/**
		 * List of controllers which should mirror this controller's value
		 * settings.
		 * Whenever a value is set in this controller it is also set in
		 * all controllers of this list
		 * @see #addMirrorController
		 * @see #removeMirrorController
		 */
		QList<QAbstractController<T, V> *> mirrorControllers;

		/**
		 * The callback to use for setting the value in setValue.
		 * Sometimes, setting a value is more complex than just changing the
		 * value (some other variables may also be involved).
		 * If this callback in non null it will be used in setValue to
		 * effectively set the value instead of changing the value directly.
		 * see #setValue(const V)
		 */
		function<void(const V)> * callback;

		/**
		 * Indicates if GUI elements linked to this controller are enabled
		 */
		bool enabled;

		/**
		 * A pointer to an instance of settings so changes in this controller
		 * can be reflected in some preferences settings if the pointer is not
		 * null. Also if the pointer is not null settings can be used to set
		 * an initial value if such value is present in the preferences stored
		 * in settings
		 */
		QSettings * settings;

		/**
		 * The name to use to read or write values from or to settings
		 * if #settings is not null
		 */
		QString settingsName;

	public:
		/**
		 * Constructor from value, mutex and evt callback
		 * @param value a constant pointer to the value to manage
		 * @param lock a constant pointer to the lock (from the processor
		 * containing the value) to check when modifying the value
		 * @param callback the call back to call for setting the value instead
		 * of settting the value directly
		 * @param settings the settings to use to set initial value and/or
		 * save the current value (if different from default value) to settings
		 * @param settingsName the name to use when reading or writing the
		 * parameter value from or to settings (if any)
		 * @warning If lock is null then we are supposed to use a callback which
		 * uses a mutex. If callback is null then we are supposed to use a mutex
		 * on the managed value. In other words : lock AND callback should NOT
		 * be both null.
		 */
		QAbstractController(T * const managedValue,
							QMutex * const lock = nullptr,
							function<void(const V)> * callback = nullptr,
							QSettings * settings = nullptr,
							const QString & settingsName = QString());

		/**
		 * Copy Constructor
		 * @param controller the controller to copy
		 */
		QAbstractController(const QAbstractController<T, V> & controller);

		/**
		 * Move Constructor
		 * @param controller the controller to move
		 */
		QAbstractController(QAbstractController<T, V> && controller);

		/**
		 * Destructor
		 */
		virtual ~QAbstractController();

		/**
		 * Check if this object is already in sources
		 * @param source the source to find in #sources
		 * @return true if this QObject is already part of #sources, and false
		 * otherwise
		 */
		bool has(QObject * source);

		/**
		 * Indicates if GUI elements linked to this controller are enabled
		 * @return the enabled state
		 */
		bool isEnabled() const;

		/**
		 * Changes the #enabled state of GUI elements connected to this
		 * controller
		 * @param value the new enabled state
		 */
		virtual void setEnabled(const bool value);

		/**
		 * Setup a QObject source (typically a widget) to reflect the managed
		 * value.
		 * Since we do not know yet what to do with the source, this method only
		 * checks if the source is already part of the connected sources or not.
		 * @param source the source to setup
		 * @return true if the source was correclty set up, false otherwise
		 * (for example when the widget was already part of connected widgets).
		 * @note subclasses overloads might actually configure this source
		 * properly and also setup connections from/to this source.
		 */
		virtual bool setupSource(QObject * source);

		/**
		 * Sets new value.
		 * @param value the value to set
		 * @return true if the value has been set without correction, false
		 * otherwise.
		 * @post The following signals have been emitted (by subclasses):
		 *	- valueChanged(V)
		 *	- updated()
		 */
		virtual bool setValue(const V value);

		/**
		 * Adds constraint regarding another QController
		 * @param controller the controller to set constraint with
		 * @param predicate the predicate to respect when setting a new value
		 * such that predicate(this->value, controller.value) should be true in
		 * order to set a value in the embedded value
		 * @return true if this constraint has been added to the list of
		 * constraints, false if the list of constraints already contained a
		 * constraint relative to "controller" OR if the constraint was not
		 * satisfied, which means the constraint need to be verified before
		 * adding it.
		 */
		bool addConstraint(QAbstractController<T, V> * controller,
						   const function<bool(V, V)> & predicate);

		/**
		 * Remove constraint relative to another controller
		 * @param controller the controller to remove from constraints lists.
		 * @return true if controller was part of the contraints list and
		 * removed, false otherwise.
		 */
		bool removeConstraint(QAbstractController<T, V> * controller);

		/**
		 * Adds a pair of value/controller to the #linkedControllers so that
		 * when this value is set in #setValue, it is also propagated to the
		 * linked controllers corresponding to this value in the multimap.
		 * @param controller the controller to progagate to
		 * @param value the value to propagate to the other controller
		 * @return true if the controller was not already linked and the pair
		 * value/controller has been added
		 */
		bool addLinkedValue(const V value,
							QAbstractController<T, V> * controller);

		/**
		 * Removes all elements with this specific keys in the
		 * #linkedControllers map
		 * @param value the key to remove from the map
		 * @param controller the controller to remove from the map. If
		 * controller is NULL the all keys "value" are removed
		 * @return true if the key has been removed from the map
		 */
		bool removeLinkedValue(const V value,
							   QAbstractController<T, V> * controller = NULL);

		/**
		 * Adds a controller to mirror value settings
		 * @param controller the controller which  should mirror value settings
		 * in this controller
		 * @return true if the controller wasn't already part of mirror
		 * controllers and has been added in #mirrorContollers
		 * @warning Do NOT use this method in a critical section since adding
		 * a mirror controller will trigger a setValue slot on the mirrored
		 * controller which uses the #lock mutex (if provided)
		 * @post valueChanged(V) signal have been emitted by subclasses
		 */
		bool addMirrorController(QAbstractController<T, V> * controller);

		/**
		 * Removes a controller from mirror controllers list
		 * @param controller the controller to remove from mirror controllers
		 * list.
		 * if this parameter is NULL then all controllers in mirror controllers
		 * list are removed
		 * @return true if the controllers was part of mirror controllers list
		 * and has been remove. Or if it is NULL if any controller has been
		 * removed from the mirror list
		 * @note subclasses may disconnect their valueChanged signal
		 * from the controller's setValue slot
		 */
		bool removeMirrorController(QAbstractController<T, V> * controller = nullptr);

		/**
		 * Sets a lock to guard setting values
		 * @param lock the lock to use when operating values [sets a null lock
		 * by default, which means no lock]
		 */
		void setLock(QMutex * lock = nullptr);

		/**
		 * Indicates if a callback has been set in #callback to set values
		 * @return true if the #callback is not null, false otherwise
		 */
		bool hasCallback() const;

		/**
		 * Sets a callback to set the value in setValue rather than setting
		 * the value directly.
		 * This callback (if not null) will be used in setValue
		 * @param callback pointer to the member function to set the value
		 */
		void setCallBack(function<void(const V)> * callback = nullptr);

	protected:

		/**
		 * Checks if all constraints are satisfied
		 * @return true if all predicates returned true, false otherwise
		 */
		bool checkConstraints() const;

		/**
		 * Propagate value to linked controllers
		 * @param value the value to propagate
		 * @note Caution: since #setValue might triggered on other controllers
		 * this method should not be used within an atomic section (between
		 * lock->lock(); and lock->unlock();)
		 */
		void propagateLinkedValue(const V value);

		/**
		 * Block signals on all connected widgets
		 */
		void blockSignalsOnConnectedWidgets();

		/**
		 * Unlock signals on all connected widgets
		 */
		void unblockSignalsOnConnectedWidgets();

		// --------------------------------------------------------------------
		// Methods to encapsulate future signals/slots
		// --------------------------------------------------------------------
		/**
		 * Checks if the argument controller is compatible with this controller
		 * @param controller the controller to check
		 * @return true if the argument controller is compatible with
		 * this controller.
		 * @note This implementation always return true but subclasses may
		 * overload this method to introduce more compatibility controls
		 */
		virtual bool checkMirror(QAbstractController<T, V> * controller);

		/**
		 * Connects this controller's valueChanged signal to "controller"'s
		 * setValue Slot
		 * @param controller the controller to connect
		 * @note concrete subclasses must implement this method
		 */
		virtual void connectMirror(QAbstractController<T, V> * controller) = 0;

		/**
		 * Disconnects this controller's valueChanged signal from "controller"'s
		 * setValue Slot
		 * @param controller the controller to disconnect
		 * @note concrete subclasses must implement this method
		 */
		virtual void disconnectMirror(QAbstractController<T, V> * controller) = 0;

		/**
		 * sends a signal when anything changes
		 */
		virtual void emitUpdated() = 0;

		/**
		 * Sends a signal when value has been set
		 * @param value the new value
		 */
		virtual void emitValueChanged(const V value) = 0;
};

#endif /* QABSTRACTCONTROLLER_H_ */
