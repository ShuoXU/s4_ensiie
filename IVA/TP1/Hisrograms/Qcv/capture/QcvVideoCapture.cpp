/*
 * QcvVideoCapture.cpp
 *
 *  Created on: 29 janv. 2012
 *	  Author: davidroussel
 */

#include <QDebug>
#include <QFileInfo>
#include <QTime>	// for time in debug messages
#include <QThread>	// for thread id in debug messages

#include "QcvVideoCapture.h"

/*
 * Protected Constructor without capture initialisation.
 * Subclasses not using #capture instance can use this constructor.
 * @param flipVideo indicates if the video should be flipped horizontally
 * @param gray indicates if image should be converted to gray
 * @param width desired width (or 0 to keep original capture image width)
 * @param height desired height (or 0 to keep original capture image height)
 * @param parent the parent QObject
 */
QcvVideoCapture::QcvVideoCapture(const bool flipVideo,
								 const bool gray,
								 const size_t width,
								 const size_t height,
								 QObject * parent) :
	QcvCapture(flipVideo, gray, width, height, parent),
	capture(nullptr),
	live(false),
	directResize(false)
	// statusMessage is default constructed
{
}


// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------
/*
 * Constructor for live video from deviceId
 * @param deviceId the ID of the camera to open
 * @param flipVideo indicates if the video should be flipped horizontally
 * @param gray indicates if image should be converted to gray
 * @param width desired width (or 0 to keep original capture image width)
 * @param height desired height (or 0 to keep original capture image height)
 * @param parent the parent QObject
 */
QcvVideoCapture::QcvVideoCapture(const int deviceId,
								 const bool flipVideo,
								 const bool gray,
								 const size_t width,
								 const size_t height,
								 QObject * parent) :
	QcvCapture(flipVideo, gray, width, height, parent),
	capture(new cv::VideoCapture(deviceId)),
	live(true),
	directResize(false) // set later
	// statusMessage is default constructed
{
}

/*
 * Constructor from file name
 * @param fileName video file to open
 * @param flipVideo mirror image
 * @param gray convert image to gray
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @param parent the parent QObject
 */
QcvVideoCapture::QcvVideoCapture(const QString & fileName,
								 const bool flipVideo,
								 const bool gray,
								 const size_t  width,
								 const size_t height,
								 QObject * parent) :
	QcvCapture(flipVideo, gray, width, height, parent),
	capture(new cv::VideoCapture(fileName.toStdString())),
	live(false)
	// statusMessage is default constructed
{
}

/*
 * Destructor.
 * releases video capture and image(s)
 */
QcvVideoCapture::~QcvVideoCapture()
{
	mutex.lock();

	if (capture != nullptr)
	{
		capture->release();
	}

	mutex.unlock();

	if (capture != nullptr)
	{
		delete capture;
	}
}

/*
 * Get the "live" status of the captured video
 * @return the "live" status of the video
 * @see #live
 */
bool QcvVideoCapture::isLive() const
{
	return live;
}

/*
 * Get the "direct resizing" status of the captured video
 * @return the "direct resizing" status of the captured video
 * @see #directResize
 */
bool QcvVideoCapture::isDirectResize() const
{
	return directResize;
}

// ----------------------------------------------------------------------------
// Public Slots
// ----------------------------------------------------------------------------

/*
 * Releases capture and emits #finished() signal
 */
void QcvVideoCapture::finish()
{
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO << "will lock";

	mutex.lock();

//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO << "locked";

	if (capture != nullptr)
	{
		if (capture->isOpened())
		{
			capture->release();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "capture released";
		}
	}

	emit finished();
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO << "capture finished emitted";

	mutex.unlock();
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO << "unlocked";
}


// ----------------------------------------------------------------------------
// Protected Slots
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Protected Methods
// ----------------------------------------------------------------------------
/*
 * Tries to set capture size directly on capture by setting properties.
 * 	- CAP_PROP_FRAME_WIDTH to set frame width
 * 	- CAP_PROP_FRAME_HEIGHT to set frame height
 * @param width the width property to set on capture
 * @param height the height property to set on capture
 * @return true if capture is opened and if width and height have been
 * set successfully through @code capture.set(...) @endcode. Returns
 * false otherwise.
 */
bool QcvVideoCapture::setDirectSize(const size_t width,
									const size_t height)
{
	/*
	 * Video for Linux (V4L) does not support setting
	 * frame width and height, so don't even try...
	 */
#ifdef Q_OS_LINUX
	Q_UNUSED(width)
	Q_UNUSED(height)
	return false;
#else
	bool result = false;

	if (capture != nullptr && capture->isOpened())
	{
		bool widthSet = capture->set(CAP_PROP_FRAME_WIDTH,
									static_cast<double>(width));
		bool heightSet = capture->set(CAP_PROP_FRAME_HEIGHT,
									 static_cast<double>(height));
		if (widthSet || heightSet)
		{
			image.release();

			// force grabbing to get the right size
			(*capture) >> image;

			size.setWidth(image.cols);
			size.setHeight(image.rows);

			result = true;
		}
	}

	return result;
#endif
}

/*
 * Wrapper method to emit finished signal for other classes to use
 * @see #finished()
 */
void QcvVideoCapture::emitFinished()
{
	emit finished();
}

/*
 * Wrapper method to emit restarted signal for other classes to use
 * @see #restarted();
 */
void QcvVideoCapture::emitRestarted()
{
	emit restarted();
}
