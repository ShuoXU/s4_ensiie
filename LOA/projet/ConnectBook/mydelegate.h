/**
 * @brief this class is for editing the item of tableview inherite class QItemDelegate
 */
#ifndef MYDELEGATE_H
#define MYDELEGATE_H
#include <QItemDelegate>
#include <QLineEdit>
#include <QComboBox>

class MyDelegate : public QItemDelegate
{
public:
    /**
     * @brief constructure
     * @param flag
     * @param parent
     */
    MyDelegate(bool flag = true,QObject*parent = nullptr);
    ~MyDelegate() override;

public:
    /**
     * @brief Returns the widget used to edit the item specified by index for editing. The parent widget and style option are used to control how the editor widget appears.
     * @param parent
     * @param option
     * @param index
     * @return
     */
    virtual QWidget* createEditor(QWidget* parent,
                                  const QStyleOptionViewItem& option,
                                  const QModelIndex & index) const override;
    /**
     * @brief Sets the data to be displayed and edited by the editor from the data model item specified by the model index.
     * @param editor
     * @param index
     */
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const override;
    /**
     * @brief Gets data from the editor widget and stores it in the specified model at the item index.
     * @param editor
     * @param model
     * @param index
     */
    virtual void setModelData(QWidget* editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    //void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    //bool editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index) override;


private:

    bool delFlag;//true -> people false->organisation
};

#endif // MYDELEGATE_H
