process
	process/show_channels/red (storage = bool)
		false
		true
	process/show_channels/green (storage = bool)
		false
		true
	process/show_channels/blue (storage = bool)
		false
		true
	process/show_channels/gray (storage = bool)
		false
		true
	process/histogram/mode (storage = int)
		Normal = 0
		Cumulative = 1
		Time Cumulative = 2
	process/histogram/lut (storage = int)
		Identity = 0
		Inverse = 1
		Gamma = 2
		Threshold = 3
		Dynamic = 4
		Equalize = 5
		Clahe = 6
	process/histogram/color_lut (storage = bool)
		false
		true
	process/histogram/lut_param (storage = int)
		current value = 80 in range [5 ... 95]
	process/histogram/dynamic_type (storage = int)
		Low = 0
		High = 1
		LowHigh = 2
	process/CLAHE/clip_limit (storage = double)
		current value = 0.1 in range [0.1 ... 40.0]±0.1
	process/CLAHE/tile_size (storage = int)
		current value = 8 in range [4 ... 32]±4
	process/post_processing/post_processing (storage = bool)
		false
		true
	process/post_processing/kernel_size (storage = int)
		current value = 3 in range [3 ... 15]±2




