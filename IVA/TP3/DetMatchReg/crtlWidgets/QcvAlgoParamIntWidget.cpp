#include <QDebug>

#include "QcvAlgoParam.h"

#include "QcvAlgoParamIntWidget.h"
#include "ui_QcvAlgoParamIntWidget.h"

/*
 * Constructor
 * @param param the associated parameter
 * @param slider indicates if there should be a slider beneath label
 * and spinbox
 * @param direct direct operation: if true indicates that parameter
 * value setting is performed directly through
 * param->setIntValue(...) which is blocking in
 * multithread environments. If false indicates that parameter value
 * setting is performed through signal/slot mechanism, which in
 * multithreaded environments is non blocking with queued connections.
 * @param settings the settings to use to set initial value and/or
 * save the current value (if different from default value) to settings
 * @param settingsName the name to use when reading or writing the
 * parameter value to settings
 * @param parent the parent widget
 */
QcvAlgoParamIntWidget::QcvAlgoParamIntWidget(CvAlgoParam * param,
											 const bool slider,
											 const bool direct,
											 QSettings * settings,
											 const QString & settingsName,
											 QWidget *parent) :
	QcvAlgoParamWidget(param, direct, settings, settingsName, parent),
	ui(new Ui::QcvAlgoParamIntWidget),
	slider(slider)
{
//	qDebug() << this << "->" << Q_FUNC_INFO;
	ui->setupUi(this);

	assert(parameter->getType() == CvAlgoParam::ParamType::INT);
	assert(parameter->getStepMode() != CvAlgoParam::StepMode::MUL);

	// If settings are available then try to read value from settings
	readSetting();

	// Setup title
	ui->label->setText(QString::fromStdString(parameter->getName()));

	// Setup tooltip
	ui->label->setToolTip(QString::fromStdString(parameter->getDescription()));

	// Setup QSpinBox
	ui->spinBox->blockSignals(true);

	ui->spinBox->setRange(parameter->getIntValue(CvAlgoParam::WhichValue::MINVALUE),
						  parameter->getIntValue(CvAlgoParam::WhichValue::MAXVALUE));
	ui->spinBox->setSingleStep(parameter->getIntValue(CvAlgoParam::WhichValue::STEPVALUE));

	ui->spinBox->setValue(parameter->getIntValue());
	bool settable = parameter->isSettable();
	if (!settable)
	{
		ui->spinBox->setEnabled(false);
	}

	ui->spinBox->blockSignals(false);

	if (slider)
	{
		// Setup QSlider
		ui->horizontalSlider->blockSignals(true);

		ui->horizontalSlider->setRange(parameter->getIntValue(CvAlgoParam::WhichValue::MINVALUE),
									   parameter->getIntValue(CvAlgoParam::WhichValue::MAXVALUE));
		ui->horizontalSlider->setSingleStep(parameter->getIntValue(CvAlgoParam::WhichValue::STEPVALUE));

		ui->horizontalSlider->setValue(parameter->getIntValue());

		if (!settable)
		{
			ui->horizontalSlider->setEnabled(false);
			ui->resetButton->setEnabled(false);
		}

		ui->horizontalSlider->blockSignals(false);
	}
	else
	{
		// Disconnect ui->spinBox <--> ui->horizontalSlider connections
		disconnect(ui->spinBox, SIGNAL(valueChanged(int)),
				   ui->horizontalSlider, SLOT(setValue(int)));
		disconnect(ui->horizontalSlider, SIGNAL(valueChanged(int)),
				   ui->spinBox, SLOT(setValue(int)));

		// Delete slider
		ui->sliderHorizontalLayout->removeWidget(ui->horizontalSlider);
		ui->horizontalSlider->setParent(NULL);
		ui->horizontalSlider->deleteLater();
	}

	// Setup connections if no direct operations
	if (!directOp && settable)
	{
		QcvAlgoParam * qParameter = dynamic_cast<QcvAlgoParam *>(parameter);
		if (qParameter != nullptr)
		{
			connect(this, SIGNAL(parameterUpdated(int)),
					qParameter, SLOT(setIntValue(int)));
			connect(this, SIGNAL(parameterReset()),
					qParameter, SLOT(resetToDefaultValue()));
		}
		else
		{
			qDebug() << "QcvAlgoParamIntWidget unable to convert param"
					 << parameter->getName().c_str()
					 << "to Qt flavored param with signals/slots";
			directOp = true;
		}
	}
//	qApp->processEvents();
}

/*
 * Destructor.
 * Clears the UI.
 */
QcvAlgoParamIntWidget::~QcvAlgoParamIntWidget()
{
//	qDebug() << this << "->" << Q_FUNC_INFO;
	delete ui;
	// All signals deconnection is performed in ~QcvAlgoParamWidget()
}

/*
 * Read value from settings (iff #settings is non null and #settingsName
 * is non empty)
 * @return true if a value has been found in settings, and if this value
 * is different from the current value of the parameter and has been set,
 * false otherwise
 */
bool QcvAlgoParamIntWidget::readSetting()
{
	if ((settings != nullptr) &&
		!settingsName.isEmpty() &&
		!parameter->isConstant())
	{
		QVariant variant = settings->value(settingsName);
		if (variant.isValid())
		{
			int intPrefValue = variant.toInt();
			if (intPrefValue != parameter->getIntValue(CvAlgoParam::WhichValue::VALUE) &&
				intPrefValue >= parameter->getIntValue(CvAlgoParam::WhichValue::MINVALUE) &&
				intPrefValue <= parameter->getIntValue(CvAlgoParam::WhichValue::MAXVALUE))
			{
//				qDebug() << Q_FUNC_INFO << settingsName << intPrefValue;
				// Sets value directly in the parameter
				parameter->setIntValue(intPrefValue);
			}
		}
	}

	return false;
}

/*
 * Slot triggered when the spinbox value changes
 * @param value the new spinbox value
 * @note if the parameter is not settable, this method should
 * (theoretically) never be triggered
 * @post the associated parameter is set to this value and
 * parameterUpdated() signal is emitted
 */
void QcvAlgoParamIntWidget::on_spinBox_valueChanged(int value)
{
//	qDebug() << Q_FUNC_INFO << "(" << value << ")";

	if (directOp)
	{
		parameter->setIntValue(value);
	}
	else
	{
		emit(parameterUpdated(value));
	}

	emit(parameterUpdated());

	// If settings are available then save this value to settings
	writeSetting(QVariant(value));
}

/*
 * Slot triggered when reset button is clicked.
 * @post default value is retreived from parameter and spinbox
 * value is changed to this value (this implies a call to
 * on_spinBox_valueChanged())
 */
void QcvAlgoParamIntWidget::on_resetButton_clicked()
{
	int iValue = parameter->getIntValue(CvAlgoParam::WhichValue::DEFAULTVALUE);
	ui->spinBox->blockSignals(true);
	ui->spinBox->setValue(iValue); // will trigger on_spinBox_valueChanged (if signals aren't blocked)
	ui->spinBox->blockSignals(false);

	if (slider)
	{
		ui->horizontalSlider->blockSignals(true);
		ui->horizontalSlider->setValue(iValue);
		ui->horizontalSlider->blockSignals(false);
	}

	if (directOp)
	{
		parameter->resetToDefaultValue();
	}
	else
	{
		emit parameterReset();
	}

	emit(parameterUpdated());

	removeSetting();
}
