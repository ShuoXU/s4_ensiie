/*
 * QcvDMR.cpp
 *
 *  Created on: 31 mars 2012
 *	  Author: davidroussel
 */

#include <QDebug>
#include <QTime>	// for time info in debug messages

#include "QcvDMR.h"

/*
 * QcvDMR Detector / Matcher / Registrar constructor
 * @param sourceImage the source image from video capture [mandatory]
 * @param modelImageFilename the model image file name [mandatory]
 * @param cameraFilename the camera parameters file [default value is
 * empty file name, in which case pose camera would not be computed]
 * @param featureType the type of feature point to detect in images
 * @param descriptorType the type of point descriptor to compute
 * at feature points locations
 * @param matcherType the kind of matcher touse when matching points
 * descriptors
 * @param matchType the kind of match to perform between points
 * descriptors
 * @param reprojectionError the maximum acceptable reprojection when
 * reprojecting model points on scene points when reprojecting with
 * homography
 * @param printScale the print scale of the printed model image in order
 * to convert model image points to model printed points at real scale
 * @param paramSets Pool of parameters for all algorithms
 * @param verboseLevel verbosity level for messages printing
 * @param imageLock the mutex on source image
 * @param updateThread the thread in which this processor runs
 * @param parent object
 * @throw CvProcessorException when
 * 	- modelImage can't be opened
 * 	- camera parameters file can't be opened
 */
QcvDMR::QcvDMR(Mat * inFrame,
			   const string & modelImageFilename,
			   const string & cameraFilename,
			   const CvDetector::FeatureType featureType,
			   const CvDetector::DescriptorExtractorType descriptorType,
			   const CvMatcher::MatcherType matcherType,
			   const CvMatcher::MatchType matchType,
			   const double reprojectionError,
			   const double printSize,
			   QcvAlgoParamSets * paramSets,
			   const VerboseLevel verboseLevel,
			   QMutex * imageLock,
			   QThread * updateThread,
			   QObject * parent) throw (CvProcessorException) :
	CvProcessor(inFrame), // <-- virtual base class constructor first
	QcvProcessor(inFrame, imageLock, updateThread, parent),
	CvDMR(inFrame,
		  modelImageFilename,
		  cameraFilename,
		  featureType,
		  descriptorType,
		  matcherType,
		  matchType,
		  reprojectionError,
		  printSize,
		  paramSets,
		  verboseLevel),
	previousSceneKeypointsNumber(0),
	scenekeyPointsVariationPercent(0.0f)
{
//	qDebug() << Q_FUNC_INFO << "started";
	if (updateThread != nullptr)
	{
		paramSets->setParent(nullptr);
		paramSets->moveToThread(updateThread);
	}
	// QcvProcessor::setNumberFormat("%7.0f");
	paramSets->setLock(locks[RESULT]);
//	qDebug() << Q_FUNC_INFO << "terminated";
}

/*
 * QcvDMR destructor
 */
QcvDMR::~QcvDMR()
{
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[TERMINATE] << ";"
//					   << "will lock";
	lockMutexes(TERMINATE);
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[TERMINATE] << ";"
//					   << "locked";
	reprojectionErrorString.clear();
	minMaxMatchDistanceString.clear();
	meanMatchDistanceString.clear();
	inliersKeypointsNumberString.clear();
	matchedKeypointsNumberString.clear();
	sceneKeypointsNumberString.clear();
	modelKeypointsNumberString.clear();
	detectionTimeString.clear();
	extractionTimeString.clear();
	matchingTimeString.clear();
	registrationTimeString.clear();

//	qDebug() << "QcvDMR destroyed";
}

/*
 * Self lock accessor
 * @return a pointer to the processor's self lock
 */
//QMutex * QcvDMR::getLock()
//{
//	return selfLock;
//}

/*
 * Update computed images and sends displayImageChanged signal if
 * required
 */
void QcvDMR::update()
{
	if (!tryLockMutexes(TERMINATE))
	{
//		qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//						   << QThread::currentThreadId() << ";"
//						   << Q_FUNC_INFO << ";"
//						   << locks[TERMINATE] << ";"
//						   << "aborted because of termination";
		return;
	}
//	else
//	{
//		qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//						   << QThread::currentThreadId() << ";"
//						   << Q_FUNC_INFO << ";"
//						   << locks[TERMINATE] << ";"
//						   << "locked";

//	}

//	bool updateModelDetectorKeypoints = (modelDetectorUpdate && detecting);
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[SOURCE] << ", " << locks[RESULT] << ";"
//					   << "will lock";
	lockMutexes(SOURCE, RESULT);
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[SOURCE] << ", " << locks[RESULT] << ";"
//					   << "locked";

	CvDMR::update();

	unlockMutexes(SOURCE);
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[SOURCE] << ";"
//					   << "unlocked";

	unlockMutexes(RESULT);
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << locks[RESULT] << ";"
//					   << "unlocked";

	QcvProcessor::update(); // emits updated signal

	unlockMutexes(TERMINATE);
}

/*
 * Update Scene detector
 */
void QcvDMR::updateDetector()
{
	bool localModelDetectorUpdate = modelDetectorUpdate;

	CvDMR::updateDetector();

	if (localModelDetectorUpdate)
	{
		emit(modelKeypointNumberChanged(modelDetector->getNbKeypoints()));
	}

	int sceneKeypointsNumber = sceneDetector->getNbKeypoints();

	// build scene keypoints number string
	if (sceneKeypointsNumber != previousSceneKeypointsNumber)
	{
		scenekeyPointsVariationPercent = (float)(sceneKeypointsNumber - previousSceneKeypointsNumber) / (float)sceneKeypointsNumber;
	}
	else
	{
		scenekeyPointsVariationPercent = 0.0f;
	}

	sceneKeypointsNumberString.sprintf("%4d %+3d%%",
									   sceneKeypointsNumber,
									   (int)round(scenekeyPointsVariationPercent*100));

	emit(updateSceneKeypointsNumberString(sceneKeypointsNumberString));

	detectionTimeString.sprintf(numberFormat.toStdString().c_str(),
								CvDMR::getMeanProcessTime(integral(ProcessTimeIndex::DETECTION)),
								CvDMR::getStdProcessTime(integral(ProcessTimeIndex::DETECTION)));
	emit(updateDetectionTimeString(detectionTimeString));

	extractionTimeString.sprintf(numberFormat.toStdString().c_str(),
								 CvDMR::getMeanProcessTime(integral(ProcessTimeIndex::EXTRACTION)),
								 CvDMR::getStdProcessTime(integral(ProcessTimeIndex::EXTRACTION)));
	emit(updateExtractionTimeString(extractionTimeString));

	previousSceneKeypointsNumber = sceneKeypointsNumber;
}

/*
 * update matcher and extract matched keypoints
 */
void QcvDMR::updateMatcher()
{
	CvDMR::updateMatcher();

	matchedKeypointsNumberString =
		QString::number(matcher->getNbMatches());
	meanMatchDistanceString = QString::number(matcher->getMatchError(MeanValue<double>::WhichValue::MEAN_VALUE), 'f', 2) +
							  QString::fromUtf8(" ± ") +
							  QString::number(matcher->getMatchError(MeanValue<double>::WhichValue::STD_VALUE), 'f', 2);

	minMaxMatchDistanceString = QString::number(matcher->getMatchError(MeanValue<double>::WhichValue::MIN_VALUE), 'f', 2) +
							 QString::fromUtf8(" / ") +
							 QString::number(matcher->getMatchError(MeanValue<double>::WhichValue::MAX_VALUE), 'f', 2);


	matchingTimeString.sprintf(numberFormat.toStdString().c_str(),
							   getMeanProcessTime((size_t)ProcessTimeIndex::MATCHING),
							   getStdProcessTime((size_t)ProcessTimeIndex::MATCHING));

	emit (updateMatchedPointsNumberString(matchedKeypointsNumberString));
	emit (updateMeanMatchDistanceString(meanMatchDistanceString));
	emit (updateMinMaxMatchDistanceString(minMaxMatchDistanceString));
	emit (updateMatchingTimeString(matchingTimeString));
}

/*
 * Convert matched keypoints to points, update registrar and extract
 * inliers keypoints
 */
void QcvDMR::updateRegistrar()
{
	CvDMR::updateRegistrar();

	inliersKeypointsNumberString = QString::number(registrar->getNbInliers());
	reprojectionErrorString = QString::number(registrar->getMeanReprojectionError(), 'f', 2);
	registrationTimeString.sprintf(numberFormat.toStdString().c_str(),
								   getMeanProcessTime((size_t)ProcessTimeIndex::REGISTRATION),
								   getStdProcessTime((size_t)ProcessTimeIndex::REGISTRATION));

	emit(updateInliersNumberString(inliersKeypointsNumberString));
	emit(updateReprojectionErrorString(reprojectionErrorString));
	emit(updateRegistrationTimeString(registrationTimeString));
}

/*
 * Read model image from file with notifications
 * @param filename the model image file name
 * @post the model image is loaded in modelImage, grayModel and
 * displayModelImage images are and modelDetector is instanciated with
 * grayModelImage if it was not created otherwise it is just set to
 * grayModelImage, and finally descriptors ptr is provided to the
 * matcher
 * @throw CvProcessorException when the file can not be read
 */
void QcvDMR::updateModelImage(const string & filename) throw (CvProcessorException)
{
	lockMutexes(RESULT);

	CvDMR::updateModelImage(filename);

	unlockMutexes(RESULT);

	// update model image
	emit(modelImageChanged(getDisplayModelImagePtr()));

	// update model detector keypoints number label
	int nbkps = static_cast<int>(modelDetector->getNbKeypoints());
//	qDebug() << Q_FUNC_INFO
//			 << "emits model keypoints number changed"
//			 << nbkps;
	emit modelKeypointNumberChanged(nbkps);

	// send feature type to task bar
	message.clear();
	message.append(filename.c_str());
	message.append(tr(" Opened"));
	sendMessage(message, defaultTimeOut);
}

/*
 * Changes source image slot.
 * Attributes needs to be cleaned up then set up again
 * @param image the new source Image
 * @post Various signals are emitted:
 * 	- imageChanged(&displaySourceImage)
 * 	- imageCchanged()
 * 	- if image size changed then imageSizeChanged() is emitted
 * 	- if image color space changed then imageColorsChanged() is emitted
 */
void QcvDMR::setSourceImage(Mat *image)
	throw (CvProcessorException)
{
	Size previousSize(sourceImage->size());
	int previousNbChannels(nbChannels);

	lockMutexes(RESULT);

	CvProcessor::setSourceImage(image);

	unlockMutexes(RESULT);

	/*
	 * displaySourceImage is emitted instead of sourceImage 'cause
	 * displaySourceImage is used to draw results in
	 */
	emit imageChanged(&displaySourceImage);

	emit imageChanged();

	if ((previousSize.width != image->cols) ||
		(previousSize.height != image->rows))
	{
		emit imageSizeChanged();
	}

	if (previousNbChannels != nbChannels)
	{
		emit imageColorsChanged();
	}

	// Force update
	// update();
}

/*
 * Read camera matrix from file
 * @param filename file containing camera matrix
 * @post cameraMatrix is set in registrar
 * @throw CvProcessorException when the faile can not be read
 */
void QcvDMR::setCameraParameters(const string & filename)
	throw (CvProcessorException)
{
	lockMutexes(RESULT);

	CvDMR::setCameraParameters(filename);

	unlockMutexes(RESULT);

	message.clear();
	message.append(tr("Camera parameters are set"));

	sendMessage(message, defaultTimeOut);
}

/*
 * Sets a new feature type with message notification
 * @param featureType the new feature type to set
 * @post current Feature detector is released and a new one is created
 * @throw CvProcessorException if feature detector is empty because it
 * couldn't be created properly
 */
void QcvDMR::setFeatureType(const CvDetector::FeatureType featureType)
	throw (CvProcessorException)
{
	if (featureType != sceneDetector->getFeatureType())
	{
		lockMutexes(RESULT);

		CvDMR::setFeatureType(featureType);

		unlockMutexes(RESULT);

		// update model detector keypoints number label
		int nbkps = static_cast<int>(modelDetector->getNbKeypoints());
		emit modelKeypointNumberChanged(nbkps);

		message.clear();
		message.append(tr("Feature point type set to: "));
		switch (featureType)
		{
			case CvDetector::FeatureType::FAST_FEATURE:
				message.append("FAST: Fast Corner Detection");
				break;
			case CvDetector::FeatureType::STAR_FEATURE:
				message.append("STAR feature");
				break;
			case CvDetector::FeatureType::SIFT_FEATURE:
				message.append("SIFT: Scale Invariant Feature Transform");
				break;
			case CvDetector::FeatureType::SURF_FEATURE:
				message.append("SURF: Speeded Up Robust Features");
				break;
			case CvDetector::FeatureType::ORB_FEATURE:
				message.append("ORB: Oriented BRIEF Features");
				break;
			case CvDetector::FeatureType::BRISK_FEATURE:
				message.append("BRISK: Binary Robust Invariant Scalable Keypoints");
				break;
			case CvDetector::FeatureType::MSER_FEATURE:
				message.append("MSER: Maximally Stable Extremal Regions");
				break;
			case CvDetector::FeatureType::GFTT_FEATURE:
				message.append("GFTT: Good Features To Track");
				break;
			case CvDetector::FeatureType::KAZE_FEATURE:
				message.append("KAZE: KAZE Features");
				break;
			case CvDetector::FeatureType::AKAZE_FEATURE:
				message.append("AKAZE: Accelerated KAZE Features");
				break;
			default:
				message.append(tr("unknown feature"));
				break;
		}

		sendMessage(message, defaultTimeOut);
	}
}

/*
 * Sets a new descriptor type with message notification
 * @param descriptorType the new descriptor type to set
 * @param checkMatcher checks if the current matcher is compatible with
 * the new descriptor type and if not change the current matcher.
 * @post current Descriptor extractor is released and a new one is created
 * @throw CvProcessorException if descriptor extractor is empty because it
 * couldn't be created properly
 * @note setting a descriptor with binary content such as ORB, BRISK or
 * BRIEF may involve having to set up a matcher supporting binary
 * descriptors. Conversely, setting a valued descriptor such as SIFT or
 * SURF, may imply to change the matcher so it can support valued
 * descriptors.
 */
void QcvDMR::setDescriptorExtractorType(const CvDetector::DescriptorExtractorType descriptorType,
							   const bool checkMatcher)
	throw (CvProcessorException)
{
	if (descriptorType != sceneDetector->getDescriptorExtractorType())
	{
		lockMutexes(RESULT);

		CvDMR::setDescriptorExtractorType(descriptorType, checkMatcher);

		unlockMutexes(RESULT);

		message.clear();
		message.append(tr("Desctiptor exctractor type set to: "));
		switch (descriptorType)
		{
			case CvDetector::DescriptorExtractorType::SIFT_DESCRIPTOR:
				message.append("SIFT Descriptor: Scale Invariant Feature Transform");
				break;
			case CvDetector::DescriptorExtractorType::SURF_DESCRIPTOR:
				message.append("SURF Descriptor: Speeded Up Robust Features");
				break;
			case CvDetector::DescriptorExtractorType::ORB_DESCRIPTOR:
				message.append("ORB descriptor : Oriented BRIEF");
				break;
			case CvDetector::DescriptorExtractorType::BRISK_DESCRIPTOR:
				message.append("BRISK Descriptor : Binary Robust Invariant Scalable Keypoints");
				break;
			case CvDetector::DescriptorExtractorType::BRIEF_DESCRIPTOR:
				message.append("BRIEF Descriptor: Binary Robust Independent Elementary Features");
				break;
			case CvDetector::DescriptorExtractorType::FREAK_DESCRIPTOR:
				message.append("FREAK Descriptor: Fast Retina Keypoint");
				break;
			default:
				message.append(tr("unknown descriptor"));
				break;
		}

		sendMessage(message, defaultTimeOut);
	}
}

/*
 * Set #modelDetectorUpdateNeeded attribute after locking the detectors
 * to true which should lead to a modelDetector update (which then will
 * reset it to false
 * when done)
 */
void QcvDMR::setModelDetectorUpdate()
{
//	qDebug() << "Model detector update required";

	lockMutexes(RESULT);

	CvDMR::setModelDetectorUpdate();

	unlockMutexes(RESULT);
}

/*
 * Sets a new matcher type after locking the matcher
 * @param matcherType the new matcher type to set
 * @param checkDescriptors check if current descriptors are compatible
 * with the new matcher and if not change the current descriptors.
 * @post the old DescriptorMatcher has been released and a new one
 * has been created
 * @throw CvProcessorException if matcher is empty because it
 * couldn't be created properly
 * @note Setting a new matcher type may involve to change the current
 * descriptors if the content of the current descriptors is not
 * supported by the new matcher.
 */
void QcvDMR::setMatcherType(const CvMatcher::MatcherType matcherType,
							const bool checkDescriptors)
	throw (CvProcessorException)
{
	if (matcherType != matcher->getMatcherType())
	{
		lockMutexes(RESULT);

		CvDMR::setMatcherType(matcherType, checkDescriptors);

		unlockMutexes(RESULT);
	}
}

/*
 * Sets a new matcher mode after locking the matcher
 * @param matcherMode the new matcher mode to set
 */
void QcvDMR::setMatcherMode(const CvMatcher::MatchType matcherMode)
{
	if (matcherMode != matcher->getMatcherMode())
	{
		lockMutexes(RESULT);

		CvDMR::setMatcherMode(matcherMode);

		unlockMutexes(RESULT);
	}
}

/*
 * Sets a new number of best matches to search for each descriptor
 * when using knnMatch after locking the matcher
 * @param knn the new number of best matches to search for each
 * descriptor
 */
void QcvDMR::setKnn(const int knn)
{
	if (knn != matcher->getKnn())
	{
		lockMutexes(RESULT);

		CvDMR::setKnn(knn);

		unlockMutexes(RESULT);
	}
}

/*
 * Sets a new radius Threshold to search for each descriptor when using
 * radiusMatch after locking the matcher
 * @param radiusThreshold the new radius threshold to set.
 * @post if the new threshold is contained within the bounds
 * [radiusThresholdMin..radiusThresholdMax] the new threshold is set,
 * otherwise min or max are set depending on the saturation side
 */
void QcvDMR::setRadiusThreshold(const double radiusThreshold)
{
	if (radiusThreshold != matcher->getRadiusThreshold())
	{
		lockMutexes(RESULT);

		CvDMR::setRadiusThreshold(radiusThreshold);

		unlockMutexes(RESULT);
	}
}

/*
 * Set a new reprojection error threshold to sort inliers from
 * outliers after locking the registrar
 * @param reprojThreshold the new reprojection error threshold
 */
void QcvDMR::setReprojThreshold(const double reprojThreshold)
{
	if (reprojThreshold != registrar->getReprojThreshold())
	{
		lockMutexes(RESULT);

		CvDMR::setReprojThreshold(reprojThreshold);

		unlockMutexes(RESULT);
	}
}

/*
 * Set a new PnP confidence level for PnP Algorithm
 * @param pnpConfidence the new PnP confidence
 */
void QcvDMR::setPnPConfidence(const double pnpConfidence)
{
	if (pnpConfidence != registrar->getPnPConfidence())
	{
		lockMutexes(RESULT);

		CvDMR::setPnPConfidence(pnpConfidence);

		unlockMutexes(RESULT);
	}
}

/*
 * Set a new mode print size and modifies print scale after locking the
 * registrar
 * @param printSize the new print size of the image model
 * @return true if printScale have been set and all camera parameters
 * and printscale are correct (acccording to isCameraSet)
 */
bool QcvDMR::setPrintSize(const double printSize)
{
	lockMutexes(RESULT);

	bool result = CvDMR::setPrintSize(printSize);

	unlockMutexes(RESULT);

	return result;
}

/*
 * Set a new camera pose computing status after locking the registrar
 * @param computePose the new camera pose computing status
 * @return if computePose is set to be true isCameraSet is called first
 * to check all camera parameters and printScale are set correctly
 * So the result might not be equal to computePose if all camera
 * parameters are not set porperly
 */
bool QcvDMR::setComputePose(const bool computePose)
{
	bool currentValue = registrar->isComputePose();
	if (computePose != currentValue)
	{
		lockMutexes(RESULT);

		bool result = CvDMR::setComputePose(computePose);

		unlockMutexes(RESULT);

		return result;
	}

	return currentValue;
}

/*
 * set new pose computing method if it is recognized after locking the
 * registrar
 * @param poseComputingMethod the new pose computing method:
 * 	- CV_ITERATIVE: iterative mode
 * 	- CV_EPNP : E-PnP mode
 */
void QcvDMR::setPoseComputingMethod(const int poseComputingMethod)
{
	if (poseComputingMethod != registrar->getPoseComputingMethod())
	{
		lockMutexes(RESULT);

		CvDMR::setPoseComputingMethod(poseComputingMethod);

		unlockMutexes(RESULT);
	}
}

/*
 * Sets a new number of iterations when pose computing
 * uses ransac after locking the registrar.
 * @param ransacPnPIterations the new number of iterations
 */
void QcvDMR::setRansacPnPIterations(const int ransacPnPIterations)
{
	if (ransacPnPIterations != registrar->getRansacPnPIterations())
	{
		lockMutexes(RESULT);

		CvDMR::setRansacPnPIterations(ransacPnPIterations);

		unlockMutexes(RESULT);
	}
}

/*
 * Change the use of ransac in solve PnP after locking the registrar
 * @param useRansacPnP the new ransac use status
 */
void QcvDMR::setRansacPnP(const bool useRansacPnP)
{
	if (useRansacPnP != registrar->isRansacPnP())
	{
		lockMutexes(RESULT);

		CvDMR::setRansacPnP(useRansacPnP);

		unlockMutexes(RESULT);
	}
}

/*
 * Set a new "use previous pose" status in pose computing after locking
 * the registrar
 * @param use the new "use previous pose" status
 */
void QcvDMR::setUsePreviousPose(const bool use)
{
	if (use != registrar->isUsePreviousPose())
	{
		lockMutexes(RESULT);

		CvDMR::setUsePreviousPose(use);

		unlockMutexes(RESULT);
	}
}

/*
 * Sets a new feature points detecting status after locking the
 * detectors
 * @param detecting the new feature points detecting status
 */
void QcvDMR::setDetecting(const bool detecting)
{
	if (detecting != this->detecting)
	{
		lockMutexes(RESULT);

		CvDMR::setDetecting(detecting);

		unlockMutexes(RESULT);

		// update model detector keypoints number label
		int nbkps = static_cast<int>(modelDetector->getNbKeypoints());
		emit modelKeypointNumberChanged(nbkps);
	}
}

/*
 * Sets a new feature points matching status after locking the matcher
 * @param matching the new feature points matching status
 * @return true if matching is set or false is matching is unset or
 * if matching can not be set because model image is not ready
 * or detecting is off
 * @note detecting should be on in order to turn matching on
 */
bool QcvDMR::setMatching(const bool matching)
{
	if (matching != this->matching)
	{
		lockMutexes(RESULT);

		bool result = CvDMR::setMatching(matching);

		unlockMutexes(RESULT);

		return result;
	}

	return this->matching;
}

/*
 * Sets a new feature points registering status after locking the
 * registrar
 * @param registering the new feature points registering status
 * @return true if registering is set or false if registering is unset
 * or if matching is off
 * @note matching should be on in order to turn registering on
 */
bool QcvDMR::setRegistering(const bool registering)
{
	if (registering != this->registering)
	{
		lockMutexes(RESULT);

		bool result = CvDMR::setRegistering(registering);

		unlockMutexes(RESULT);

		return result;
	}

	return this->registering;
}

/*
 * Set new verbose level after selflocking
 * @param level the new verobse level
 */
void QcvDMR::setVerboseLevel(const VerboseLevel level)
{
	if (level != this->verboseLevel)
	{
		lockMutexes(RESULT);

		CvDMR::setVerboseLevel(level);

		unlockMutexes(RESULT);
	}
}

/*
 * Sets a new keypoints drawing mode after self locking
 * @param mode the new keypoints drawing mode
 */
void QcvDMR::setKeyPointsMode(const KeyPointShow mode)
{

	if (mode != this->keyPointsMode)
	{
//		qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//						   << QThread::currentThreadId() << ";"
//						   << Q_FUNC_INFO << ";"
//						   << locks[RESULT] << ";"
//						   << "will lock";
		lockMutexes(RESULT);
//		qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//						   << QThread::currentThreadId() << ";"
//						   << Q_FUNC_INFO << ";"
//						   << locks[RESULT] << ";"
//						   << "locked";

		CvDMR::setKeyPointsMode(mode);

		unlockMutexes(RESULT);
//		qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//						   << QThread::currentThreadId() << ";"
//						   << Q_FUNC_INFO << ";"
//						   << locks[RESULT] << ";"
//						   << "unlocked";
	}
}

/*
 * Sets a new drawing status for model box in scene image after self
 * locking
 * @param showModelFrame the new drawing status for model box in scene
 * image
 * @return true if show model box is set, false if it is unset or
 * if it can't be set because registering is off
 */
bool QcvDMR::setShowModelFrame(const bool showModelFrame)
{
	if (showModelFrame != this->showModelFrame)
	{
		lockMutexes(RESULT);

		bool result = CvDMR::setShowModelFrame(showModelFrame);

		unlockMutexes(RESULT);

		return result;
	}

	return this->showModelFrame;
}

/*
 * Sets a new drawing status for model frame in scene image after self
 * locking
 * @param showModelBox the new drawing status for model frame in scene
 * image
 * @return true if show model Frame is set, false if it is unset or
 * if show model box could not be stated because registering is off
 */
bool QcvDMR::setShowModelBox(const bool showModelBox)
{
	if (showModelBox != this->showModelBox)
	{
		lockMutexes(RESULT);

		bool result = CvDMR::setShowModelBox(showModelBox);

		unlockMutexes(RESULT);

		return result;
	}

	return this->showModelBox;
}

/*
 * Sets a new model image box height in scene image after self locking
 * @param height the new model image box height (in mm)
 * @post the last 4 points of modelBoxPoints3D had their last (Z)
 * coodinates changed to -height.
 */
void QcvDMR::setBoxHeight(const double height)
{
	if (height != this->boxHeight)
	{
		lockMutexes(RESULT);

		CvDMR::setBoxHeight(height);

		unlockMutexes(RESULT);
	}
}

/*
 * Sets Time per feature processing time unit slot after self locking
 * @param value the time per feature value (true or false)
 */
void QcvDMR::setTimePerFeature(const bool value)
{
	if (value != this->timePerFeature)
	{
		lockMutexes(RESULT);

		CvDMR::setTimePerFeature(value);

		unlockMutexes(RESULT);
	}
}

/*
 * Set record pose state after locking registrar
 * @param value the new record pose
 */
void QcvDMR::setRecordPose(const bool value)
{
	if (value != registrar->isRecordPose())
	{
		lockMutexes(RESULT);

		CvDMR::setRecordPose(value);

		unlockMutexes(RESULT);
	}
}


/*
 * Reset mean and std process times in order to re-start computing
 * new mean and std process time values.
 */
void QcvDMR::resetMeanProcessTime()
{
	lockMutexes(RESULT);

	CvDMR::resetMeanProcessTime();

	unlockMutexes(RESULT);
}

/*
 * Draw results in displaySceneImage and evt displayModelImage
 * @param redrawModelImage model image redraw needed
 * @return true if displayModelImage has also been updated
 */
bool QcvDMR::drawResults(const bool redrawModelImage)
{
	bool updated = CvDMR::drawResults(redrawModelImage);

	if (updated)
	{
		emit(modelUpdated());
	}

	return updated;
}
