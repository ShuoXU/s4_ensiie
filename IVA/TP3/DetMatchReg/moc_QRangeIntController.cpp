/****************************************************************************
** Meta object code from reading C++ file 'QRangeIntController.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Qcv/controllers/QRangeIntController.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QRangeIntController.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QRangeIntController_t {
    QByteArrayData data[19];
    char stringdata0[183];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QRangeIntController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QRangeIntController_t qt_meta_stringdata_QRangeIntController = {
    {
QT_MOC_LITERAL(0, 0, 19), // "QRangeIntController"
QT_MOC_LITERAL(1, 20, 7), // "updated"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 12), // "valueChanged"
QT_MOC_LITERAL(4, 42, 5), // "value"
QT_MOC_LITERAL(5, 48, 12), // "indexChanged"
QT_MOC_LITERAL(6, 61, 5), // "index"
QT_MOC_LITERAL(7, 67, 10), // "minChanged"
QT_MOC_LITERAL(8, 78, 10), // "maxChanged"
QT_MOC_LITERAL(9, 89, 12), // "rangeChanged"
QT_MOC_LITERAL(10, 102, 3), // "min"
QT_MOC_LITERAL(11, 106, 3), // "max"
QT_MOC_LITERAL(12, 110, 11), // "stepChanged"
QT_MOC_LITERAL(13, 122, 17), // "indexRangeChanged"
QT_MOC_LITERAL(14, 140, 10), // "setEnabled"
QT_MOC_LITERAL(15, 151, 8), // "setValue"
QT_MOC_LITERAL(16, 160, 8), // "setIndex"
QT_MOC_LITERAL(17, 169, 5), // "reset"
QT_MOC_LITERAL(18, 175, 7) // "refresh"

    },
    "QRangeIntController\0updated\0\0valueChanged\0"
    "value\0indexChanged\0index\0minChanged\0"
    "maxChanged\0rangeChanged\0min\0max\0"
    "stepChanged\0indexRangeChanged\0setEnabled\0"
    "setValue\0setIndex\0reset\0refresh"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QRangeIntController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,
       3,    1,   80,    2, 0x06 /* Public */,
       5,    1,   83,    2, 0x06 /* Public */,
       7,    1,   86,    2, 0x06 /* Public */,
       8,    1,   89,    2, 0x06 /* Public */,
       9,    2,   92,    2, 0x06 /* Public */,
      12,    1,   97,    2, 0x06 /* Public */,
      13,    2,  100,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    1,  105,    2, 0x0a /* Public */,
      15,    1,  108,    2, 0x0a /* Public */,
      16,    1,  111,    2, 0x0a /* Public */,
      17,    0,  114,    2, 0x0a /* Public */,
      18,    0,  115,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   10,   11,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   10,   11,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Bool, QMetaType::Int,    4,
    QMetaType::Bool, QMetaType::Int,    6,
    QMetaType::Bool,
    QMetaType::Void,

       0        // eod
};

void QRangeIntController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QRangeIntController *_t = static_cast<QRangeIntController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updated(); break;
        case 1: _t->valueChanged((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 2: _t->indexChanged((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 3: _t->minChanged((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 4: _t->maxChanged((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 5: _t->rangeChanged((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const int(*)>(_a[2]))); break;
        case 6: _t->stepChanged((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 7: _t->indexRangeChanged((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const int(*)>(_a[2]))); break;
        case 8: _t->setEnabled((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 9: { bool _r = _t->setValue((*reinterpret_cast< const int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 10: { bool _r = _t->setIndex((*reinterpret_cast< const int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 11: { bool _r = _t->reset();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 12: _t->refresh(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QRangeIntController::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeIntController::updated)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QRangeIntController::*_t)(const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeIntController::valueChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QRangeIntController::*_t)(const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeIntController::indexChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QRangeIntController::*_t)(const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeIntController::minChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QRangeIntController::*_t)(const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeIntController::maxChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QRangeIntController::*_t)(const int , const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeIntController::rangeChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QRangeIntController::*_t)(const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeIntController::stepChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QRangeIntController::*_t)(const int , const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeIntController::indexRangeChanged)) {
                *result = 7;
                return;
            }
        }
    }
}

const QMetaObject QRangeIntController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QRangeIntController.data,
      qt_meta_data_QRangeIntController,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QRangeIntController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QRangeIntController::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QRangeIntController.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QRangeAbstractController<int>"))
        return static_cast< QRangeAbstractController<int>*>(this);
    return QObject::qt_metacast(_clname);
}

int QRangeIntController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void QRangeIntController::updated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QRangeIntController::valueChanged(const int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QRangeIntController::indexChanged(const int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QRangeIntController::minChanged(const int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QRangeIntController::maxChanged(const int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QRangeIntController::rangeChanged(const int _t1, const int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QRangeIntController::stepChanged(const int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QRangeIntController::indexRangeChanged(const int _t1, const int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
