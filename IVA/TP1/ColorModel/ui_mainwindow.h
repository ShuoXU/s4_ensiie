/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <Qcv/matWidgets/QcvMatWidget.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionCamera_0;
    QAction *actionCamera_1;
    QAction *actionFile;
    QAction *actionFlip;
    QAction *actionOriginalSize;
    QAction *actionConstrainedSize;
    QAction *actionQuit;
    QAction *actionRenderImage;
    QAction *actionRenderPixmap;
    QAction *actionRenderOpenGL;
    QAction *actionDisplayInput;
    QAction *actionDisplayGray;
    QAction *actionDisplayRGB_Red;
    QAction *actionDisplayRGB_Green;
    QAction *actionDisplayRGB_Blue;
    QAction *actionDisplayRGB_Max;
    QAction *actionDisplayXYZ_X;
    QAction *actionDisplayXYZ_Y;
    QAction *actionDisplayXYZ_Z;
    QAction *actionDisplayHSV_Hue;
    QAction *actionDisplayHSV_Saturation;
    QAction *actionDisplayHSV_Value;
    QAction *actionDisplayYCbCr_Y;
    QAction *actionDisplayYCbCr_Cr;
    QAction *actionDisplayYCbCr_Cb;
    QAction *actionChannelColor;
    QAction *actionMixHue;
    QAction *actionMixHueSat;
    QAction *actionMixHueValue;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QScrollArea *scrollArea;
    QcvMatWidget *widgetImage;
    QTabWidget *tabWidget;
    QWidget *sizeTab;
    QVBoxLayout *verticalLayout;
    QRadioButton *radioButtonOrigSize;
    QRadioButton *radioButtonCustomSize;
    QLabel *labelWidth;
    QSpinBox *spinBoxWidth;
    QLabel *labelHeight;
    QSpinBox *spinBoxHeight;
    QCheckBox *checkBoxFlip;
    QSpacerItem *verticalSpacer;
    QWidget *colorTab;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *displayBox;
    QVBoxLayout *verticalLayout_3;
    QRadioButton *radioButtonInput;
    QRadioButton *radioButtonGray;
    QRadioButton *radioButtonRed;
    QRadioButton *radioButtonGreen;
    QRadioButton *radioButtonBlue;
    QRadioButton *radioButtonMaxBGR;
    QRadioButton *radioButtonXYZ_X;
    QRadioButton *radioButtonXYZ_Y;
    QRadioButton *radioButtonXYZ_Z;
    QRadioButton *radioButtonHue;
    QRadioButton *radioButtonSaturation;
    QRadioButton *radioButtonValue;
    QRadioButton *radioButtonY;
    QRadioButton *radioButtonCr;
    QRadioButton *radioButtonCb;
    QCheckBox *checkBoxChColor;
    QGroupBox *hueBox;
    QVBoxLayout *verticalLayout_5;
    QRadioButton *radioButtonMixHue;
    QRadioButton *radioButtonMixHueSat;
    QRadioButton *radioButtonMixHueVal;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label;
    QSpinBox *spinBoxBlockSize;
    QProgressBar *progressBarNoise0;
    QProgressBar *progressBarNoise1;
    QProgressBar *progressBarNoise2;
    QVBoxLayout *processTimeLayout;
    QLabel *labelProcessTime;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelProcessTimeValue;
    QLabel *labelPM;
    QLabel *labelProcessTimeStd;
    QLabel *labelTimeUnit;
    QSpacerItem *verticalSpacer_2;
    QMenuBar *menuBar;
    QMenu *menuSources;
    QMenu *menuVideo;
    QMenu *menuSize;
    QMenu *menuRender;
    QMenu *menuDisplay;
    QMenu *menuHue_Mix;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(840, 649);
        MainWindow->setUnifiedTitleAndToolBarOnMac(true);
        actionCamera_0 = new QAction(MainWindow);
        actionCamera_0->setObjectName(QStringLiteral("actionCamera_0"));
        actionCamera_1 = new QAction(MainWindow);
        actionCamera_1->setObjectName(QStringLiteral("actionCamera_1"));
        actionFile = new QAction(MainWindow);
        actionFile->setObjectName(QStringLiteral("actionFile"));
        actionFlip = new QAction(MainWindow);
        actionFlip->setObjectName(QStringLiteral("actionFlip"));
        actionFlip->setCheckable(true);
        actionOriginalSize = new QAction(MainWindow);
        actionOriginalSize->setObjectName(QStringLiteral("actionOriginalSize"));
        actionOriginalSize->setCheckable(true);
        actionConstrainedSize = new QAction(MainWindow);
        actionConstrainedSize->setObjectName(QStringLiteral("actionConstrainedSize"));
        actionConstrainedSize->setCheckable(true);
        actionQuit = new QAction(MainWindow);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        actionQuit->setMenuRole(QAction::QuitRole);
        actionRenderImage = new QAction(MainWindow);
        actionRenderImage->setObjectName(QStringLiteral("actionRenderImage"));
        actionRenderImage->setCheckable(true);
        actionRenderImage->setChecked(true);
        actionRenderPixmap = new QAction(MainWindow);
        actionRenderPixmap->setObjectName(QStringLiteral("actionRenderPixmap"));
        actionRenderPixmap->setCheckable(true);
        actionRenderOpenGL = new QAction(MainWindow);
        actionRenderOpenGL->setObjectName(QStringLiteral("actionRenderOpenGL"));
        actionRenderOpenGL->setCheckable(true);
        actionDisplayInput = new QAction(MainWindow);
        actionDisplayInput->setObjectName(QStringLiteral("actionDisplayInput"));
        actionDisplayInput->setCheckable(true);
        actionDisplayGray = new QAction(MainWindow);
        actionDisplayGray->setObjectName(QStringLiteral("actionDisplayGray"));
        actionDisplayGray->setCheckable(true);
        actionDisplayRGB_Red = new QAction(MainWindow);
        actionDisplayRGB_Red->setObjectName(QStringLiteral("actionDisplayRGB_Red"));
        actionDisplayRGB_Red->setCheckable(true);
        actionDisplayRGB_Green = new QAction(MainWindow);
        actionDisplayRGB_Green->setObjectName(QStringLiteral("actionDisplayRGB_Green"));
        actionDisplayRGB_Green->setCheckable(true);
        actionDisplayRGB_Blue = new QAction(MainWindow);
        actionDisplayRGB_Blue->setObjectName(QStringLiteral("actionDisplayRGB_Blue"));
        actionDisplayRGB_Blue->setCheckable(true);
        actionDisplayRGB_Max = new QAction(MainWindow);
        actionDisplayRGB_Max->setObjectName(QStringLiteral("actionDisplayRGB_Max"));
        actionDisplayRGB_Max->setCheckable(true);
        actionDisplayXYZ_X = new QAction(MainWindow);
        actionDisplayXYZ_X->setObjectName(QStringLiteral("actionDisplayXYZ_X"));
        actionDisplayXYZ_X->setCheckable(true);
        actionDisplayXYZ_Y = new QAction(MainWindow);
        actionDisplayXYZ_Y->setObjectName(QStringLiteral("actionDisplayXYZ_Y"));
        actionDisplayXYZ_Y->setCheckable(true);
        actionDisplayXYZ_Z = new QAction(MainWindow);
        actionDisplayXYZ_Z->setObjectName(QStringLiteral("actionDisplayXYZ_Z"));
        actionDisplayXYZ_Z->setCheckable(true);
        actionDisplayHSV_Hue = new QAction(MainWindow);
        actionDisplayHSV_Hue->setObjectName(QStringLiteral("actionDisplayHSV_Hue"));
        actionDisplayHSV_Hue->setCheckable(true);
        actionDisplayHSV_Saturation = new QAction(MainWindow);
        actionDisplayHSV_Saturation->setObjectName(QStringLiteral("actionDisplayHSV_Saturation"));
        actionDisplayHSV_Saturation->setCheckable(true);
        actionDisplayHSV_Value = new QAction(MainWindow);
        actionDisplayHSV_Value->setObjectName(QStringLiteral("actionDisplayHSV_Value"));
        actionDisplayHSV_Value->setCheckable(true);
        actionDisplayYCbCr_Y = new QAction(MainWindow);
        actionDisplayYCbCr_Y->setObjectName(QStringLiteral("actionDisplayYCbCr_Y"));
        actionDisplayYCbCr_Y->setCheckable(true);
        actionDisplayYCbCr_Cr = new QAction(MainWindow);
        actionDisplayYCbCr_Cr->setObjectName(QStringLiteral("actionDisplayYCbCr_Cr"));
        actionDisplayYCbCr_Cr->setCheckable(true);
        actionDisplayYCbCr_Cb = new QAction(MainWindow);
        actionDisplayYCbCr_Cb->setObjectName(QStringLiteral("actionDisplayYCbCr_Cb"));
        actionDisplayYCbCr_Cb->setCheckable(true);
        actionChannelColor = new QAction(MainWindow);
        actionChannelColor->setObjectName(QStringLiteral("actionChannelColor"));
        actionChannelColor->setCheckable(false);
        actionChannelColor->setChecked(false);
        actionMixHue = new QAction(MainWindow);
        actionMixHue->setObjectName(QStringLiteral("actionMixHue"));
        actionMixHue->setCheckable(true);
        actionMixHueSat = new QAction(MainWindow);
        actionMixHueSat->setObjectName(QStringLiteral("actionMixHueSat"));
        actionMixHueSat->setCheckable(true);
        actionMixHueValue = new QAction(MainWindow);
        actionMixHueValue->setObjectName(QStringLiteral("actionMixHueValue"));
        actionMixHueValue->setCheckable(true);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(6, 0, 6, 0);
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(scrollArea->sizePolicy().hasHeightForWidth());
        scrollArea->setSizePolicy(sizePolicy);
        scrollArea->setWidgetResizable(true);
        scrollArea->setAlignment(Qt::AlignCenter);
        widgetImage = new QcvMatWidget();
        widgetImage->setObjectName(QStringLiteral("widgetImage"));
        widgetImage->setGeometry(QRect(0, 0, 652, 598));
        scrollArea->setWidget(widgetImage);

        horizontalLayout->addWidget(scrollArea);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy1);
        tabWidget->setUsesScrollButtons(false);
        sizeTab = new QWidget();
        sizeTab->setObjectName(QStringLiteral("sizeTab"));
        verticalLayout = new QVBoxLayout(sizeTab);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(8, 8, 8, 8);
        radioButtonOrigSize = new QRadioButton(sizeTab);
        radioButtonOrigSize->setObjectName(QStringLiteral("radioButtonOrigSize"));
        radioButtonOrigSize->setChecked(true);

        verticalLayout->addWidget(radioButtonOrigSize);

        radioButtonCustomSize = new QRadioButton(sizeTab);
        radioButtonCustomSize->setObjectName(QStringLiteral("radioButtonCustomSize"));

        verticalLayout->addWidget(radioButtonCustomSize);

        labelWidth = new QLabel(sizeTab);
        labelWidth->setObjectName(QStringLiteral("labelWidth"));

        verticalLayout->addWidget(labelWidth);

        spinBoxWidth = new QSpinBox(sizeTab);
        spinBoxWidth->setObjectName(QStringLiteral("spinBoxWidth"));
        spinBoxWidth->setMaximum(1600);
        spinBoxWidth->setSingleStep(4);

        verticalLayout->addWidget(spinBoxWidth);

        labelHeight = new QLabel(sizeTab);
        labelHeight->setObjectName(QStringLiteral("labelHeight"));

        verticalLayout->addWidget(labelHeight);

        spinBoxHeight = new QSpinBox(sizeTab);
        spinBoxHeight->setObjectName(QStringLiteral("spinBoxHeight"));
        spinBoxHeight->setMaximum(1200);
        spinBoxHeight->setSingleStep(4);

        verticalLayout->addWidget(spinBoxHeight);

        checkBoxFlip = new QCheckBox(sizeTab);
        checkBoxFlip->setObjectName(QStringLiteral("checkBoxFlip"));

        verticalLayout->addWidget(checkBoxFlip);

        verticalSpacer = new QSpacerItem(20, 228, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        tabWidget->addTab(sizeTab, QString());
        colorTab = new QWidget();
        colorTab->setObjectName(QStringLiteral("colorTab"));
        verticalLayout_2 = new QVBoxLayout(colorTab);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(8, 8, 8, 8);
        displayBox = new QGroupBox(colorTab);
        displayBox->setObjectName(QStringLiteral("displayBox"));
        verticalLayout_3 = new QVBoxLayout(displayBox);
        verticalLayout_3->setSpacing(2);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(8, 8, 8, 8);
        radioButtonInput = new QRadioButton(displayBox);
        radioButtonInput->setObjectName(QStringLiteral("radioButtonInput"));
        radioButtonInput->setChecked(true);

        verticalLayout_3->addWidget(radioButtonInput);

        radioButtonGray = new QRadioButton(displayBox);
        radioButtonGray->setObjectName(QStringLiteral("radioButtonGray"));

        verticalLayout_3->addWidget(radioButtonGray);

        radioButtonRed = new QRadioButton(displayBox);
        radioButtonRed->setObjectName(QStringLiteral("radioButtonRed"));

        verticalLayout_3->addWidget(radioButtonRed);

        radioButtonGreen = new QRadioButton(displayBox);
        radioButtonGreen->setObjectName(QStringLiteral("radioButtonGreen"));

        verticalLayout_3->addWidget(radioButtonGreen);

        radioButtonBlue = new QRadioButton(displayBox);
        radioButtonBlue->setObjectName(QStringLiteral("radioButtonBlue"));

        verticalLayout_3->addWidget(radioButtonBlue);

        radioButtonMaxBGR = new QRadioButton(displayBox);
        radioButtonMaxBGR->setObjectName(QStringLiteral("radioButtonMaxBGR"));

        verticalLayout_3->addWidget(radioButtonMaxBGR);

        radioButtonXYZ_X = new QRadioButton(displayBox);
        radioButtonXYZ_X->setObjectName(QStringLiteral("radioButtonXYZ_X"));

        verticalLayout_3->addWidget(radioButtonXYZ_X);

        radioButtonXYZ_Y = new QRadioButton(displayBox);
        radioButtonXYZ_Y->setObjectName(QStringLiteral("radioButtonXYZ_Y"));

        verticalLayout_3->addWidget(radioButtonXYZ_Y);

        radioButtonXYZ_Z = new QRadioButton(displayBox);
        radioButtonXYZ_Z->setObjectName(QStringLiteral("radioButtonXYZ_Z"));

        verticalLayout_3->addWidget(radioButtonXYZ_Z);

        radioButtonHue = new QRadioButton(displayBox);
        radioButtonHue->setObjectName(QStringLiteral("radioButtonHue"));

        verticalLayout_3->addWidget(radioButtonHue);

        radioButtonSaturation = new QRadioButton(displayBox);
        radioButtonSaturation->setObjectName(QStringLiteral("radioButtonSaturation"));

        verticalLayout_3->addWidget(radioButtonSaturation);

        radioButtonValue = new QRadioButton(displayBox);
        radioButtonValue->setObjectName(QStringLiteral("radioButtonValue"));

        verticalLayout_3->addWidget(radioButtonValue);

        radioButtonY = new QRadioButton(displayBox);
        radioButtonY->setObjectName(QStringLiteral("radioButtonY"));

        verticalLayout_3->addWidget(radioButtonY);

        radioButtonCr = new QRadioButton(displayBox);
        radioButtonCr->setObjectName(QStringLiteral("radioButtonCr"));

        verticalLayout_3->addWidget(radioButtonCr);

        radioButtonCb = new QRadioButton(displayBox);
        radioButtonCb->setObjectName(QStringLiteral("radioButtonCb"));

        verticalLayout_3->addWidget(radioButtonCb);


        verticalLayout_2->addWidget(displayBox);

        checkBoxChColor = new QCheckBox(colorTab);
        checkBoxChColor->setObjectName(QStringLiteral("checkBoxChColor"));
        checkBoxChColor->setChecked(true);
        checkBoxChColor->setTristate(false);

        verticalLayout_2->addWidget(checkBoxChColor);

        hueBox = new QGroupBox(colorTab);
        hueBox->setObjectName(QStringLiteral("hueBox"));
        verticalLayout_5 = new QVBoxLayout(hueBox);
        verticalLayout_5->setSpacing(2);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(8, 8, 8, 8);
        radioButtonMixHue = new QRadioButton(hueBox);
        radioButtonMixHue->setObjectName(QStringLiteral("radioButtonMixHue"));
        radioButtonMixHue->setChecked(true);

        verticalLayout_5->addWidget(radioButtonMixHue);

        radioButtonMixHueSat = new QRadioButton(hueBox);
        radioButtonMixHueSat->setObjectName(QStringLiteral("radioButtonMixHueSat"));

        verticalLayout_5->addWidget(radioButtonMixHueSat);

        radioButtonMixHueVal = new QRadioButton(hueBox);
        radioButtonMixHueVal->setObjectName(QStringLiteral("radioButtonMixHueVal"));

        verticalLayout_5->addWidget(radioButtonMixHueVal);


        verticalLayout_2->addWidget(hueBox);

        groupBox = new QGroupBox(colorTab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setSpacing(4);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(8, 8, 8, 8);
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_4->addWidget(label);

        spinBoxBlockSize = new QSpinBox(groupBox);
        spinBoxBlockSize->setObjectName(QStringLiteral("spinBoxBlockSize"));

        horizontalLayout_4->addWidget(spinBoxBlockSize);


        verticalLayout_4->addLayout(horizontalLayout_4);

        progressBarNoise0 = new QProgressBar(groupBox);
        progressBarNoise0->setObjectName(QStringLiteral("progressBarNoise0"));
        QFont font;
        font.setPointSize(10);
        progressBarNoise0->setFont(font);
        progressBarNoise0->setStyleSheet(QLatin1String("QProgressBar{height:10px}\n"
"QProgressBar::chunk{background-color: rgb(0, 128, 255);}"));
        progressBarNoise0->setValue(24);
        progressBarNoise0->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_4->addWidget(progressBarNoise0);

        progressBarNoise1 = new QProgressBar(groupBox);
        progressBarNoise1->setObjectName(QStringLiteral("progressBarNoise1"));
        progressBarNoise1->setFont(font);
        progressBarNoise1->setStyleSheet(QStringLiteral("QProgressBar{height:10px}QProgressBar::chunk{background-color: rgb(0, 255, 128);}"));
        progressBarNoise1->setValue(24);
        progressBarNoise1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        progressBarNoise1->setInvertedAppearance(false);

        verticalLayout_4->addWidget(progressBarNoise1);

        progressBarNoise2 = new QProgressBar(groupBox);
        progressBarNoise2->setObjectName(QStringLiteral("progressBarNoise2"));
        progressBarNoise2->setFont(font);
        progressBarNoise2->setStyleSheet(QStringLiteral("QProgressBar{height:10px}QProgressBar::chunk{background-color: rgb(255, 128, 0);}"));
        progressBarNoise2->setValue(24);
        progressBarNoise2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_4->addWidget(progressBarNoise2);


        verticalLayout_2->addWidget(groupBox);

        processTimeLayout = new QVBoxLayout();
        processTimeLayout->setSpacing(1);
        processTimeLayout->setObjectName(QStringLiteral("processTimeLayout"));
        labelProcessTime = new QLabel(colorTab);
        labelProcessTime->setObjectName(QStringLiteral("labelProcessTime"));

        processTimeLayout->addWidget(labelProcessTime);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        labelProcessTimeValue = new QLabel(colorTab);
        labelProcessTimeValue->setObjectName(QStringLiteral("labelProcessTimeValue"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(labelProcessTimeValue->sizePolicy().hasHeightForWidth());
        labelProcessTimeValue->setSizePolicy(sizePolicy2);
        labelProcessTimeValue->setFont(font);
        labelProcessTimeValue->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(labelProcessTimeValue);

        labelPM = new QLabel(colorTab);
        labelPM->setObjectName(QStringLiteral("labelPM"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(labelPM->sizePolicy().hasHeightForWidth());
        labelPM->setSizePolicy(sizePolicy3);
        labelPM->setFont(font);
        labelPM->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(labelPM);

        labelProcessTimeStd = new QLabel(colorTab);
        labelProcessTimeStd->setObjectName(QStringLiteral("labelProcessTimeStd"));
        sizePolicy2.setHeightForWidth(labelProcessTimeStd->sizePolicy().hasHeightForWidth());
        labelProcessTimeStd->setSizePolicy(sizePolicy2);
        labelProcessTimeStd->setFont(font);
        labelProcessTimeStd->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(labelProcessTimeStd);

        labelTimeUnit = new QLabel(colorTab);
        labelTimeUnit->setObjectName(QStringLiteral("labelTimeUnit"));
        sizePolicy3.setHeightForWidth(labelTimeUnit->sizePolicy().hasHeightForWidth());
        labelTimeUnit->setSizePolicy(sizePolicy3);
        labelTimeUnit->setFont(font);
        labelTimeUnit->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(labelTimeUnit);


        processTimeLayout->addLayout(horizontalLayout_2);


        verticalLayout_2->addLayout(processTimeLayout);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        tabWidget->addTab(colorTab, QString());

        horizontalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 840, 20));
        menuSources = new QMenu(menuBar);
        menuSources->setObjectName(QStringLiteral("menuSources"));
        menuVideo = new QMenu(menuBar);
        menuVideo->setObjectName(QStringLiteral("menuVideo"));
        menuSize = new QMenu(menuVideo);
        menuSize->setObjectName(QStringLiteral("menuSize"));
        menuRender = new QMenu(menuBar);
        menuRender->setObjectName(QStringLiteral("menuRender"));
        menuDisplay = new QMenu(menuBar);
        menuDisplay->setObjectName(QStringLiteral("menuDisplay"));
        menuHue_Mix = new QMenu(menuDisplay);
        menuHue_Mix->setObjectName(QStringLiteral("menuHue_Mix"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuSources->menuAction());
        menuBar->addAction(menuVideo->menuAction());
        menuBar->addAction(menuRender->menuAction());
        menuBar->addAction(menuDisplay->menuAction());
        menuSources->addAction(actionCamera_0);
        menuSources->addAction(actionCamera_1);
        menuSources->addAction(actionFile);
        menuSources->addSeparator();
        menuSources->addAction(actionQuit);
        menuVideo->addAction(actionFlip);
        menuVideo->addSeparator();
        menuVideo->addAction(menuSize->menuAction());
        menuSize->addAction(actionOriginalSize);
        menuSize->addAction(actionConstrainedSize);
        menuRender->addAction(actionRenderImage);
        menuRender->addAction(actionRenderPixmap);
        menuRender->addAction(actionRenderOpenGL);
        menuDisplay->addAction(actionDisplayInput);
        menuDisplay->addAction(actionDisplayGray);
        menuDisplay->addAction(actionDisplayRGB_Red);
        menuDisplay->addAction(actionDisplayRGB_Green);
        menuDisplay->addAction(actionDisplayRGB_Blue);
        menuDisplay->addAction(actionDisplayRGB_Max);
        menuDisplay->addAction(actionDisplayXYZ_X);
        menuDisplay->addAction(actionDisplayXYZ_Y);
        menuDisplay->addAction(actionDisplayXYZ_Z);
        menuDisplay->addAction(actionDisplayHSV_Hue);
        menuDisplay->addAction(actionDisplayHSV_Saturation);
        menuDisplay->addAction(actionDisplayHSV_Value);
        menuDisplay->addAction(actionDisplayYCbCr_Y);
        menuDisplay->addAction(actionDisplayYCbCr_Cr);
        menuDisplay->addAction(actionDisplayYCbCr_Cb);
        menuDisplay->addSeparator();
        menuDisplay->addAction(actionChannelColor);
        menuDisplay->addAction(menuHue_Mix->menuAction());
        menuHue_Mix->addAction(actionMixHue);
        menuHue_Mix->addAction(actionMixHueSat);
        menuHue_Mix->addAction(actionMixHueValue);

        retranslateUi(MainWindow);
        QObject::connect(radioButtonCustomSize, SIGNAL(clicked(bool)), actionConstrainedSize, SLOT(setChecked(bool)));
        QObject::connect(actionConstrainedSize, SIGNAL(triggered(bool)), radioButtonCustomSize, SLOT(setChecked(bool)));
        QObject::connect(radioButtonOrigSize, SIGNAL(clicked(bool)), actionOriginalSize, SLOT(setChecked(bool)));
        QObject::connect(actionOriginalSize, SIGNAL(triggered(bool)), radioButtonOrigSize, SLOT(setChecked(bool)));
        QObject::connect(checkBoxFlip, SIGNAL(clicked(bool)), actionFlip, SLOT(setChecked(bool)));
        QObject::connect(actionFlip, SIGNAL(triggered(bool)), checkBoxFlip, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayInput, SIGNAL(triggered(bool)), radioButtonInput, SLOT(setChecked(bool)));
        QObject::connect(radioButtonInput, SIGNAL(clicked(bool)), actionDisplayInput, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayGray, SIGNAL(triggered(bool)), radioButtonGray, SLOT(setChecked(bool)));
        QObject::connect(radioButtonGray, SIGNAL(clicked(bool)), actionDisplayGray, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayRGB_Red, SIGNAL(triggered(bool)), radioButtonRed, SLOT(setChecked(bool)));
        QObject::connect(radioButtonRed, SIGNAL(clicked(bool)), actionDisplayRGB_Red, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayRGB_Green, SIGNAL(triggered(bool)), radioButtonGreen, SLOT(setChecked(bool)));
        QObject::connect(radioButtonGreen, SIGNAL(clicked(bool)), actionDisplayRGB_Green, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayRGB_Blue, SIGNAL(triggered(bool)), radioButtonBlue, SLOT(setChecked(bool)));
        QObject::connect(radioButtonBlue, SIGNAL(clicked(bool)), actionDisplayRGB_Blue, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayRGB_Max, SIGNAL(triggered(bool)), radioButtonMaxBGR, SLOT(setChecked(bool)));
        QObject::connect(radioButtonMaxBGR, SIGNAL(clicked(bool)), actionDisplayRGB_Max, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayXYZ_X, SIGNAL(triggered(bool)), radioButtonXYZ_X, SLOT(setChecked(bool)));
        QObject::connect(radioButtonXYZ_X, SIGNAL(clicked(bool)), actionDisplayXYZ_X, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayXYZ_Y, SIGNAL(triggered(bool)), radioButtonXYZ_Y, SLOT(setChecked(bool)));
        QObject::connect(radioButtonXYZ_X, SIGNAL(clicked(bool)), actionDisplayXYZ_Y, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayXYZ_Z, SIGNAL(triggered(bool)), radioButtonXYZ_Z, SLOT(setChecked(bool)));
        QObject::connect(radioButtonXYZ_Z, SIGNAL(clicked(bool)), actionDisplayXYZ_Z, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayHSV_Hue, SIGNAL(triggered(bool)), radioButtonHue, SLOT(setChecked(bool)));
        QObject::connect(radioButtonBlue, SIGNAL(clicked(bool)), actionDisplayHSV_Hue, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayHSV_Saturation, SIGNAL(triggered(bool)), radioButtonSaturation, SLOT(setChecked(bool)));
        QObject::connect(radioButtonSaturation, SIGNAL(clicked(bool)), actionDisplayHSV_Saturation, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayHSV_Value, SIGNAL(triggered(bool)), radioButtonValue, SLOT(setChecked(bool)));
        QObject::connect(radioButtonValue, SIGNAL(clicked(bool)), actionDisplayHSV_Value, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayYCbCr_Y, SIGNAL(triggered(bool)), radioButtonY, SLOT(setChecked(bool)));
        QObject::connect(radioButtonY, SIGNAL(clicked(bool)), actionDisplayYCbCr_Y, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayYCbCr_Cr, SIGNAL(triggered(bool)), radioButtonCr, SLOT(setChecked(bool)));
        QObject::connect(radioButtonCr, SIGNAL(clicked(bool)), actionDisplayYCbCr_Cr, SLOT(setChecked(bool)));
        QObject::connect(actionDisplayYCbCr_Cb, SIGNAL(triggered(bool)), radioButtonCb, SLOT(setChecked(bool)));
        QObject::connect(radioButtonCb, SIGNAL(clicked(bool)), actionDisplayYCbCr_Cb, SLOT(setChecked(bool)));
        QObject::connect(actionMixHue, SIGNAL(triggered(bool)), radioButtonMixHue, SLOT(setChecked(bool)));
        QObject::connect(radioButtonMixHue, SIGNAL(clicked(bool)), actionMixHue, SLOT(setChecked(bool)));
        QObject::connect(actionMixHueSat, SIGNAL(triggered(bool)), radioButtonMixHueSat, SLOT(setChecked(bool)));
        QObject::connect(radioButtonMixHueSat, SIGNAL(clicked(bool)), actionMixHueSat, SLOT(setChecked(bool)));
        QObject::connect(actionMixHueValue, SIGNAL(triggered(bool)), radioButtonMixHueVal, SLOT(setChecked(bool)));
        QObject::connect(radioButtonMixHueVal, SIGNAL(clicked(bool)), actionMixHueValue, SLOT(setChecked(bool)));

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Color spaces", nullptr));
        actionCamera_0->setText(QApplication::translate("MainWindow", "Camera 0", nullptr));
#ifndef QT_NO_TOOLTIP
        actionCamera_0->setToolTip(QApplication::translate("MainWindow", "cam\303\251ra interne", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionCamera_0->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+0", nullptr));
#endif // QT_NO_SHORTCUT
        actionCamera_1->setText(QApplication::translate("MainWindow", "Camera 1", nullptr));
#ifndef QT_NO_TOOLTIP
        actionCamera_1->setToolTip(QApplication::translate("MainWindow", "cam\303\251ra externe", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionCamera_1->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+1", nullptr));
#endif // QT_NO_SHORTCUT
        actionFile->setText(QApplication::translate("MainWindow", "File", nullptr));
#ifndef QT_NO_TOOLTIP
        actionFile->setToolTip(QApplication::translate("MainWindow", "fichier video", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionFile->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_NO_SHORTCUT
        actionFlip->setText(QApplication::translate("MainWindow", "flip", nullptr));
#ifndef QT_NO_SHORTCUT
        actionFlip->setShortcut(QApplication::translate("MainWindow", "Ctrl+F", nullptr));
#endif // QT_NO_SHORTCUT
        actionOriginalSize->setText(QApplication::translate("MainWindow", "Originale", nullptr));
#ifndef QT_NO_TOOLTIP
        actionOriginalSize->setToolTip(QApplication::translate("MainWindow", "taille originale", nullptr));
#endif // QT_NO_TOOLTIP
        actionConstrainedSize->setText(QApplication::translate("MainWindow", "Custom", nullptr));
        actionConstrainedSize->setIconText(QApplication::translate("MainWindow", "contrainte", nullptr));
#ifndef QT_NO_TOOLTIP
        actionConstrainedSize->setToolTip(QApplication::translate("MainWindow", "taille impos\303\251e", nullptr));
#endif // QT_NO_TOOLTIP
        actionQuit->setText(QApplication::translate("MainWindow", "Quitter", nullptr));
#ifndef QT_NO_TOOLTIP
        actionQuit->setToolTip(QApplication::translate("MainWindow", "quitter", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionQuit->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", nullptr));
#endif // QT_NO_SHORTCUT
        actionRenderImage->setText(QApplication::translate("MainWindow", "Image", nullptr));
        actionRenderPixmap->setText(QApplication::translate("MainWindow", "Pixmap", nullptr));
        actionRenderOpenGL->setText(QApplication::translate("MainWindow", "OpenGL", nullptr));
        actionDisplayInput->setText(QApplication::translate("MainWindow", "Input", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayInput->setToolTip(QApplication::translate("MainWindow", "Display input image", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayInput->setShortcut(QApplication::translate("MainWindow", "I", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayGray->setText(QApplication::translate("MainWindow", "Gray", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayGray->setToolTip(QApplication::translate("MainWindow", "Display gray level image", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayGray->setShortcut(QApplication::translate("MainWindow", "K", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayRGB_Red->setText(QApplication::translate("MainWindow", "RGB: Red", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayRGB_Red->setToolTip(QApplication::translate("MainWindow", "Display RGB red component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayRGB_Red->setShortcut(QApplication::translate("MainWindow", "R", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayRGB_Green->setText(QApplication::translate("MainWindow", "RGB: Green", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayRGB_Green->setToolTip(QApplication::translate("MainWindow", "Display RGB green component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayRGB_Green->setShortcut(QApplication::translate("MainWindow", "G", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayRGB_Blue->setText(QApplication::translate("MainWindow", "RBG: Blue", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayRGB_Blue->setToolTip(QApplication::translate("MainWindow", "Display RGB blue component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayRGB_Blue->setShortcut(QApplication::translate("MainWindow", "B", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayRGB_Max->setText(QApplication::translate("MainWindow", "RGB: Max", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayRGB_Max->setToolTip(QApplication::translate("MainWindow", "Display RGB Max image", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayRGB_Max->setShortcut(QApplication::translate("MainWindow", "M", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayXYZ_X->setText(QApplication::translate("MainWindow", "XYZ: X", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayXYZ_X->setToolTip(QApplication::translate("MainWindow", "Display XYZ X component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayXYZ_X->setShortcut(QApplication::translate("MainWindow", "X", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayXYZ_Y->setText(QApplication::translate("MainWindow", "XYZ: Y", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayXYZ_Y->setToolTip(QApplication::translate("MainWindow", "Display XYZ Y component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayXYZ_Y->setShortcut(QApplication::translate("MainWindow", "W", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayXYZ_Z->setText(QApplication::translate("MainWindow", "XYZ: Z", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayXYZ_Z->setToolTip(QApplication::translate("MainWindow", "Display XYZ Z component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayXYZ_Z->setShortcut(QApplication::translate("MainWindow", "Z", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayHSV_Hue->setText(QApplication::translate("MainWindow", "HSV: Hue", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayHSV_Hue->setToolTip(QApplication::translate("MainWindow", "Display HSV hue component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayHSV_Hue->setShortcut(QApplication::translate("MainWindow", "H", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayHSV_Saturation->setText(QApplication::translate("MainWindow", "HSV: Saturation", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayHSV_Saturation->setToolTip(QApplication::translate("MainWindow", "Display HSV saturation component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayHSV_Saturation->setShortcut(QApplication::translate("MainWindow", "S", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayHSV_Value->setText(QApplication::translate("MainWindow", "HSV: Value", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayHSV_Value->setToolTip(QApplication::translate("MainWindow", "Display HSV value component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayHSV_Value->setShortcut(QApplication::translate("MainWindow", "V", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayYCbCr_Y->setText(QApplication::translate("MainWindow", "YCbCr: Y", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayYCbCr_Y->setToolTip(QApplication::translate("MainWindow", "Display YCbCr Y component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayYCbCr_Y->setShortcut(QApplication::translate("MainWindow", "Y", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayYCbCr_Cr->setText(QApplication::translate("MainWindow", "YCbCr: Cr", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayYCbCr_Cr->setToolTip(QApplication::translate("MainWindow", "Display YCbCr Cr component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayYCbCr_Cr->setShortcut(QApplication::translate("MainWindow", "U", nullptr));
#endif // QT_NO_SHORTCUT
        actionDisplayYCbCr_Cb->setText(QApplication::translate("MainWindow", "YCbCr: Cb", nullptr));
#ifndef QT_NO_TOOLTIP
        actionDisplayYCbCr_Cb->setToolTip(QApplication::translate("MainWindow", "Display YCbCr Cb component", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionDisplayYCbCr_Cb->setShortcut(QApplication::translate("MainWindow", "T", nullptr));
#endif // QT_NO_SHORTCUT
        actionChannelColor->setText(QApplication::translate("MainWindow", "Colored Channels", nullptr));
#ifndef QT_NO_TOOLTIP
        actionChannelColor->setToolTip(QApplication::translate("MainWindow", "Show components in color", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionChannelColor->setShortcut(QApplication::translate("MainWindow", "C", nullptr));
#endif // QT_NO_SHORTCUT
        actionMixHue->setText(QApplication::translate("MainWindow", "Hue", nullptr));
#ifndef QT_NO_TOOLTIP
        actionMixHue->setToolTip(QApplication::translate("MainWindow", "Pure Hue", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionMixHue->setShortcut(QApplication::translate("MainWindow", "Ctrl+U", nullptr));
#endif // QT_NO_SHORTCUT
        actionMixHueSat->setText(QApplication::translate("MainWindow", "Hue x Sat", nullptr));
#ifndef QT_NO_TOOLTIP
        actionMixHueSat->setToolTip(QApplication::translate("MainWindow", "Mix Hue with Saturation", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionMixHueSat->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_NO_SHORTCUT
        actionMixHueValue->setText(QApplication::translate("MainWindow", "Hue x Value", nullptr));
#ifndef QT_NO_TOOLTIP
        actionMixHueValue->setToolTip(QApplication::translate("MainWindow", "Mix Hue with Value", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionMixHueValue->setShortcut(QApplication::translate("MainWindow", "Ctrl+V", nullptr));
#endif // QT_NO_SHORTCUT
        radioButtonOrigSize->setText(QApplication::translate("MainWindow", "Originale", nullptr));
        radioButtonCustomSize->setText(QApplication::translate("MainWindow", "Custom", nullptr));
        labelWidth->setText(QApplication::translate("MainWindow", "Largeur", nullptr));
        labelHeight->setText(QApplication::translate("MainWindow", "Hauteur", nullptr));
        checkBoxFlip->setText(QApplication::translate("MainWindow", "Flip", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(sizeTab), QApplication::translate("MainWindow", "Size", nullptr));
        displayBox->setTitle(QApplication::translate("MainWindow", "Display", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonInput->setToolTip(QApplication::translate("MainWindow", "Input Image (I)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonInput->setText(QApplication::translate("MainWindow", "Input", nullptr));
#ifndef QT_NO_SHORTCUT
        radioButtonInput->setShortcut(QString());
#endif // QT_NO_SHORTCUT
#ifndef QT_NO_TOOLTIP
        radioButtonGray->setToolTip(QApplication::translate("MainWindow", "Grayscale Image (K)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonGray->setText(QApplication::translate("MainWindow", "Gray", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonRed->setToolTip(QApplication::translate("MainWindow", "Red Component (R)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonRed->setText(QApplication::translate("MainWindow", "RGB: Red", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonGreen->setToolTip(QApplication::translate("MainWindow", "Green Component (G)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonGreen->setText(QApplication::translate("MainWindow", "RGB: Green", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonBlue->setToolTip(QApplication::translate("MainWindow", "Blue Component (B)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonBlue->setText(QApplication::translate("MainWindow", "RGB: Blue", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonMaxBGR->setToolTip(QApplication::translate("MainWindow", "RGB Maximum Image", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonMaxBGR->setText(QApplication::translate("MainWindow", "RGB: Max", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonXYZ_X->setToolTip(QApplication::translate("MainWindow", "X Component (X)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonXYZ_X->setText(QApplication::translate("MainWindow", "XYZ: X", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonXYZ_Y->setToolTip(QApplication::translate("MainWindow", "Y component (W)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonXYZ_Y->setText(QApplication::translate("MainWindow", "XYZ: Y", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonXYZ_Z->setToolTip(QApplication::translate("MainWindow", "Z Component (Z)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonXYZ_Z->setText(QApplication::translate("MainWindow", "XYZ: Z", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonHue->setToolTip(QApplication::translate("MainWindow", "Hue Component (H)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonHue->setText(QApplication::translate("MainWindow", "HSV: Hue", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonSaturation->setToolTip(QApplication::translate("MainWindow", "Saturation Component (S)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonSaturation->setText(QApplication::translate("MainWindow", "HSV: Saturation", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonValue->setToolTip(QApplication::translate("MainWindow", "Value Component (V)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonValue->setText(QApplication::translate("MainWindow", "HSV: Value", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonY->setToolTip(QApplication::translate("MainWindow", "Y Component (Y)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonY->setText(QApplication::translate("MainWindow", "YCbCr: Y", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonCr->setToolTip(QApplication::translate("MainWindow", "Green/Magenta Chrominance (U)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonCr->setText(QApplication::translate("MainWindow", "YCbCr: Cr", nullptr));
#ifndef QT_NO_TOOLTIP
        radioButtonCb->setToolTip(QApplication::translate("MainWindow", "Blue/Yellow Chrominance (T)", nullptr));
#endif // QT_NO_TOOLTIP
        radioButtonCb->setText(QApplication::translate("MainWindow", "YCbCr: Cb", nullptr));
        checkBoxChColor->setText(QApplication::translate("MainWindow", "Colored Channels", nullptr));
        hueBox->setTitle(QApplication::translate("MainWindow", "Hue mix", nullptr));
        radioButtonMixHue->setText(QApplication::translate("MainWindow", "Hue", nullptr));
        radioButtonMixHueSat->setText(QApplication::translate("MainWindow", "Hue x Sat", nullptr));
        radioButtonMixHueVal->setText(QApplication::translate("MainWindow", "Hue x Value", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "Signal / Noise Ratio", nullptr));
        label->setText(QApplication::translate("MainWindow", "block size", nullptr));
        labelProcessTime->setText(QApplication::translate("MainWindow", "process time:", nullptr));
        labelProcessTimeValue->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelPM->setText(QApplication::translate("MainWindow", "\302\261", nullptr));
        labelProcessTimeStd->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelTimeUnit->setText(QApplication::translate("MainWindow", "ms", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(colorTab), QApplication::translate("MainWindow", "Color", nullptr));
        menuSources->setTitle(QApplication::translate("MainWindow", "Sources", nullptr));
        menuVideo->setTitle(QApplication::translate("MainWindow", "Video", nullptr));
        menuSize->setTitle(QApplication::translate("MainWindow", "taille", nullptr));
        menuRender->setTitle(QApplication::translate("MainWindow", "Render", nullptr));
        menuDisplay->setTitle(QApplication::translate("MainWindow", "Display", nullptr));
        menuHue_Mix->setTitle(QApplication::translate("MainWindow", "Hue Mix", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
