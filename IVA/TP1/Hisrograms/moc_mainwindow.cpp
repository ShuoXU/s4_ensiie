/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[39];
    char stringdata0[695];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 11), // "sendMessage"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 7), // "message"
QT_MOC_LITERAL(4, 32, 7), // "timeout"
QT_MOC_LITERAL(5, 40, 11), // "sizeChanged"
QT_MOC_LITERAL(6, 52, 4), // "size"
QT_MOC_LITERAL(7, 57, 12), // "deviceOpened"
QT_MOC_LITERAL(8, 70, 8), // "deviceId"
QT_MOC_LITERAL(9, 79, 6), // "size_t"
QT_MOC_LITERAL(10, 86, 5), // "width"
QT_MOC_LITERAL(11, 92, 6), // "height"
QT_MOC_LITERAL(12, 99, 11), // "videoOpened"
QT_MOC_LITERAL(13, 111, 8), // "fileName"
QT_MOC_LITERAL(14, 120, 15), // "directoryOpened"
QT_MOC_LITERAL(15, 136, 7), // "dirName"
QT_MOC_LITERAL(16, 144, 9), // "flipVideo"
QT_MOC_LITERAL(17, 154, 4), // "flip"
QT_MOC_LITERAL(18, 159, 8), // "finished"
QT_MOC_LITERAL(19, 168, 20), // "setupProcessorFromUI"
QT_MOC_LITERAL(20, 189, 27), // "on_actionCamera_0_triggered"
QT_MOC_LITERAL(21, 217, 27), // "on_actionCamera_1_triggered"
QT_MOC_LITERAL(22, 245, 23), // "on_actionFile_triggered"
QT_MOC_LITERAL(23, 269, 28), // "on_actionDirectory_triggered"
QT_MOC_LITERAL(24, 298, 23), // "on_actionQuit_triggered"
QT_MOC_LITERAL(25, 322, 23), // "on_actionFlip_triggered"
QT_MOC_LITERAL(26, 346, 31), // "on_actionOriginalSize_triggered"
QT_MOC_LITERAL(27, 378, 34), // "on_actionConstrainedSize_trig..."
QT_MOC_LITERAL(28, 413, 30), // "on_actionRenderImage_triggered"
QT_MOC_LITERAL(29, 444, 31), // "on_actionRenderPixmap_triggered"
QT_MOC_LITERAL(30, 476, 31), // "on_actionRenderOpenGL_triggered"
QT_MOC_LITERAL(31, 508, 30), // "on_radioButtonOrigSize_clicked"
QT_MOC_LITERAL(32, 539, 32), // "on_radioButtonCustomSize_clicked"
QT_MOC_LITERAL(33, 572, 28), // "on_spinBoxWidth_valueChanged"
QT_MOC_LITERAL(34, 601, 5), // "value"
QT_MOC_LITERAL(35, 607, 29), // "on_spinBoxHeight_valueChanged"
QT_MOC_LITERAL(36, 637, 23), // "on_checkBoxFlip_clicked"
QT_MOC_LITERAL(37, 661, 27), // "on_tabWidget_currentChanged"
QT_MOC_LITERAL(38, 689, 5) // "index"

    },
    "MainWindow\0sendMessage\0\0message\0timeout\0"
    "sizeChanged\0size\0deviceOpened\0deviceId\0"
    "size_t\0width\0height\0videoOpened\0"
    "fileName\0directoryOpened\0dirName\0"
    "flipVideo\0flip\0finished\0setupProcessorFromUI\0"
    "on_actionCamera_0_triggered\0"
    "on_actionCamera_1_triggered\0"
    "on_actionFile_triggered\0"
    "on_actionDirectory_triggered\0"
    "on_actionQuit_triggered\0on_actionFlip_triggered\0"
    "on_actionOriginalSize_triggered\0"
    "on_actionConstrainedSize_triggered\0"
    "on_actionRenderImage_triggered\0"
    "on_actionRenderPixmap_triggered\0"
    "on_actionRenderOpenGL_triggered\0"
    "on_radioButtonOrigSize_clicked\0"
    "on_radioButtonCustomSize_clicked\0"
    "on_spinBoxWidth_valueChanged\0value\0"
    "on_spinBoxHeight_valueChanged\0"
    "on_checkBoxFlip_clicked\0"
    "on_tabWidget_currentChanged\0index"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  144,    2, 0x06 /* Public */,
       1,    1,  149,    2, 0x26 /* Public | MethodCloned */,
       5,    1,  152,    2, 0x06 /* Public */,
       7,    3,  155,    2, 0x06 /* Public */,
      12,    3,  162,    2, 0x06 /* Public */,
      14,    3,  169,    2, 0x06 /* Public */,
      16,    1,  176,    2, 0x06 /* Public */,
      18,    0,  179,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      19,    0,  180,    2, 0x08 /* Private */,
      20,    0,  181,    2, 0x08 /* Private */,
      21,    0,  182,    2, 0x08 /* Private */,
      22,    0,  183,    2, 0x08 /* Private */,
      23,    0,  184,    2, 0x08 /* Private */,
      24,    0,  185,    2, 0x08 /* Private */,
      25,    0,  186,    2, 0x08 /* Private */,
      26,    0,  187,    2, 0x08 /* Private */,
      27,    0,  188,    2, 0x08 /* Private */,
      28,    0,  189,    2, 0x08 /* Private */,
      29,    0,  190,    2, 0x08 /* Private */,
      30,    0,  191,    2, 0x08 /* Private */,
      31,    0,  192,    2, 0x08 /* Private */,
      32,    0,  193,    2, 0x08 /* Private */,
      33,    1,  194,    2, 0x08 /* Private */,
      35,    1,  197,    2, 0x08 /* Private */,
      36,    0,  200,    2, 0x08 /* Private */,
      37,    1,  201,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    3,    4,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QSize,    6,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 9, 0x80000000 | 9,    8,   10,   11,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 9, 0x80000000 | 9,   13,   10,   11,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 9, 0x80000000 | 9,   15,   10,   11,
    QMetaType::Void, QMetaType::Bool,   17,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   34,
    QMetaType::Void, QMetaType::Int,   34,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   38,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->sendMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->sizeChanged((*reinterpret_cast< const QSize(*)>(_a[1]))); break;
        case 3: _t->deviceOpened((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3]))); break;
        case 4: _t->videoOpened((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3]))); break;
        case 5: _t->directoryOpened((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3]))); break;
        case 6: _t->flipVideo((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 7: _t->finished(); break;
        case 8: _t->setupProcessorFromUI(); break;
        case 9: _t->on_actionCamera_0_triggered(); break;
        case 10: _t->on_actionCamera_1_triggered(); break;
        case 11: _t->on_actionFile_triggered(); break;
        case 12: _t->on_actionDirectory_triggered(); break;
        case 13: _t->on_actionQuit_triggered(); break;
        case 14: _t->on_actionFlip_triggered(); break;
        case 15: _t->on_actionOriginalSize_triggered(); break;
        case 16: _t->on_actionConstrainedSize_triggered(); break;
        case 17: _t->on_actionRenderImage_triggered(); break;
        case 18: _t->on_actionRenderPixmap_triggered(); break;
        case 19: _t->on_actionRenderOpenGL_triggered(); break;
        case 20: _t->on_radioButtonOrigSize_clicked(); break;
        case 21: _t->on_radioButtonCustomSize_clicked(); break;
        case 22: _t->on_spinBoxWidth_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 23: _t->on_spinBoxHeight_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 24: _t->on_checkBoxFlip_clicked(); break;
        case 25: _t->on_tabWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (MainWindow::*_t)(const QString & , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::sendMessage)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const QSize & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::sizeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const int , const size_t , const size_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::deviceOpened)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const QString & , const size_t , const size_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::videoOpened)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const QString & , const size_t , const size_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::directoryOpened)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::flipVideo)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::finished)) {
                *result = 7;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::sendMessage(const QString & _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 2
void MainWindow::sizeChanged(const QSize & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MainWindow::deviceOpened(const int _t1, const size_t _t2, const size_t _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MainWindow::videoOpened(const QString & _t1, const size_t _t2, const size_t _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MainWindow::directoryOpened(const QString & _t1, const size_t _t2, const size_t _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void MainWindow::flipVideo(const bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void MainWindow::finished()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
