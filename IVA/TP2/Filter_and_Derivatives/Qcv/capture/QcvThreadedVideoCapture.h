#ifndef QCVTHREADEDVIDEOCAPTURE_H
#define QCVTHREADEDVIDEOCAPTURE_H

#include <QObject>
#include <QWaitCondition>	// for waiting for at least one upate from worker
#include <QSize>
#include <QString>
#include <QElapsedTimer>	// for measuring framerate

#include <opencv2/highgui/highgui.hpp>	// for VideoCapture

#include <Qcv/capture/QcvVideoCapture.h>
#include <Qcv/capture/QcvCaptureWorker.h>

/*
 * Forward declaration of QcvCaptureWorker because
 *	- QcvThreadedVideoCapture includes QcvCaptureWorker
 *	- and QcvCaptureWorker includes QcvThreadedVideoCapture
 */
class QcvCaptureWorker;

/**
 * New implementation (06/2017) of Qt + OpenCV Capture.
 * @note This implementation delegates the capture update to a separate thread
 * and therefore only methods other than update are performed in the current
 * thread (hence the need of a QMutex to ensure atomic operations on capture).
 * @note The update thread (#worker) can perform in two modes:
 *	- asynchrone : where the update is performed as fast as possible (useful for
 *	camera source)
 *	- synchrone : where the update is performed at a specific frame rate (useful
 *	for video file playback) as long as the requested frame rate is slower than
 *	capture update time. If requested frame rate is higher than capture maximum
 *	refresh rate then requested frame rate might not be attainable.
 * @note This capture is automatically "skippable", meaning, when capture update
 * can't get lock, it will simply wait until lock can be obtained before updating
 */
class QcvThreadedVideoCapture : public QcvVideoCapture
{
	Q_OBJECT

	/**
	 * Capture worker is defined as a friend of QcvThreadedVideoCapture, so
	 * it can access its members directly
	 * @note QcvCaptureWorker could have been declared as a nested class but
	 * Qt's Meta-Object Compiler (moc) does not support nested classes.
	 */
	friend class QcvCaptureWorker;

	private:

		/**
		 * The worker updating captured images in another thread
		 */
		QcvCaptureWorker * worker;

		/**
		 * Wait condition to wait for at least one update from #worker's thread
		 * @note this condition is used only when setting up a new capture
		 * and shall be destroyed afterwards otherwise the risk is to
		 * unlock the capture lock for any capture client in another thread
		 */
		QWaitCondition * updateCondition;

		/**
		 * Indicates we're trying to restart video playback from a file
		 * which should not be tried more than once.
		 */
		bool restarted;

		/**
		 * The default frame rate for synchronized capture
		 */
		static const double defaultConstrainedFrameRate;

	public:
		/**
		 * Constructor for live video from deviceId
		 * @param deviceId the ID of the camera to open
		 * @param flipVideo indicates if the video should be flipped horizontally
		 * @param gray indicates if image should be converted to gray
		 * @param width desired width (or 0 to keep original capture image width)
		 * @param height desired height (or 0 to keep original capture image height)
		 * @param parent the parent QObject
		 */
		explicit QcvThreadedVideoCapture(const int deviceId = 0,
										 const bool flipVideo = false,
										 const bool gray = false,
										 const size_t width = 0,
										 const size_t height = 0,
										 QObject * parent = nullptr);

		/**
		 * Constructor from file name
		 * @param fileName video file to open
		 * @param flipVideo mirror image
		 * @param gray convert image to gray
		 * @param skip indicates capture can skip an image. When the capture
		 * result has not been processed yet, or when false that capture should
		 * wait for the result to be processed before grabbing a new image.
		 * This only applies when #updateThread is not NULL.
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @param parent the parent QObject
		 */
		explicit QcvThreadedVideoCapture(const QString & fileName,
										 const bool flipVideo = false,
										 const bool gray = false,
										 const size_t width = 0,
										 const size_t height = 0,
										 QObject * parent = NULL);

		/**
		 * Destructor.
		 * releases video capture and image(s)
		 */
		virtual ~QcvThreadedVideoCapture();

		/**
		 * Indicates if capture works in synchronized mode where
		 * frame rate is imposed.
		 * @return true is capture works in synchronized mode, false
		 * otherwise
		 */
		bool isSynchronized() const override;

		/**
		 * Gets the contrained frame rate at which capture should operate
		 * when synchronized mode is on
		 * @return the constrained frame rate
		 */
		double getContrainedFrameRate() const override;

	public slots:
		/**
		 * Open new device Id
		 * @param deviceId device number to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if device has been opened and checked and timer launched
		 */
		bool openDevice(const int deviceId,
						const size_t width = 0,
						const size_t height = 0) override;

		/**
		 * Open new video file
		 * @param fileName video file to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if video has been opened and timer launched
		 */
		bool openFile(const QString & fileName,
					  const size_t width = 0,
					  const size_t height = 0) override;

		/**
		 * Sets the "flipped" status of captured video
		 * @param value the new "flipped" status
		 */
		void setFlipped(const bool value) override;

		/**
		 * Sets the gray conversion status of captured video
		 * @param value the new value of gray conversion status
		 * @post The #imageChanged(cv::Mat *); signal have been emitted
		 */
		void setGray(const bool value) override;

		/**
		 * Sets capture to synchronized mode (when true) where frame rate is
		 * contrained (whenever possible). Otherwise frame rate is not
		 * constrained and capture may proceed as fast as possible.
		 * @param value the new value of synchronized mode
		 * @post The #synchronized(bool); signal has been emitted
		 */
		void setSynchronized(const bool value) override;

		/**
		 * Sets the desired frame rate on capture ONLY when capture is in
		 * synchronized mode
		 *	- If the new frame rate is 0.0 then use natural capture frame rate.
		 *	- If the new frame rate is too high for capture then capture will
		 *	keeps its highest possible frame rate according to the device in use
		 * @param frameRate the new frame rate to set
		 * @note Please note that this method has no effect on capture when
		 * synchronized mode is off
		 * @post The #constrainedFrameRateChanged(double) signal have been emitted
		 */
		void setFrameRate(const double frameRate) override;

	protected:
		/**
		 * After capture has been created,
		 *	- grab a first image to setup sizes
		 *	- launch the worker
		 * @param width required image width
		 * @param height required image height
		 * @pre capture is not null and opened
		 */
		void setupAndLaunch(const size_t width = 0,
							const size_t height = 0);

		/**
		 * Sets the desired size of captured image
		 * @param width the desired width of captured image
		 * @param height the desired height of captured image
		 * @post The #messageChanged(QString); signal is emitted with new size
		 * values
		 * @see #size
		 */
		void setSize_Impl(const size_t width, const size_t height) override;

		/**
		 * Update capture (with proper locking/unlocking):
		 *	- Grabs a new image
		 *	- if gray is on then convert
		 *	- if resize is on then resize image into imageResized
		 *	(otherwise set imageResized to image)
		 *	- if flipped is on then flip imageResized into imageFlipped
		 *	(otherwise set imageFlipped to imageResized)
		 *	- if gray is on then converts imageFlipped into imageDisplay
		 *	(otherwise set imageDisplay to imageFlipped)
		 * @return true if new image has been grabbed from capture or false
		 * when there is no more images to grab from video capture
		 * @post When no image has been grabbed resizing, flipping and
		 * converting to gray are not performed
		 * @post If a new image is available #updated(); signal has been emitted
		 */
		bool update();

		/**
		 * Grabs new image from capture
		 * @return true if new image has been grabbed from capture or false
		 * when there is no more images to grab from video capture
		 * @note This method eventually deals with automatically restarting
		 * the capture when video file playback has reached the end of the file
		 * @warning This method does not perform locking/unlocking so it not
		 * safe to use it without proper mutex's operations
		 * @post #messageChanged(); signal has been emitted if either:
		 *	- There is no more frames to capture (then capture is released)
		 *	- Capture has been restarted to loop video files
		 */
		bool grab();

		/**
		 * If resizing is needed, resize the image
		 * @warning This method does not perform locking/unlocking so it not
		 * safe to use it without proper mutex's operations
		 */
		void performResize();

		/**
		 * if flipping is needed, flip the image
		 * @warning This method does not perform locking/unlocking so it not
		 * safe to use it without proper mutex's operations
		 */
		void performFlip();

		/**
		 * if gray conversion is needed convert to gray image
		 * @warning This method does not perform locking/unlocking so it not
		 * safe to use it without proper mutex's operations
		 */
		void performConvert();

		/**
		 * Release old capture in order to switch to a new capture.
		 *	- Old capture is released
		 *	- Worker gets terminated and destroyed
		 *	- old capture is replaced by new one
		 * @param newCapture the new capture to switch on
		 * @param synchronized mode for the new capture
		 * @param frameRate if synchronized mode is used the frame rate for
		 * the synchronized mode
		 * @pre new capture needs to be non null an opened
		 * @post new still needs to be setup and a neww worker reccreated and
		 * launched with #setupAndLaunch();
		 */
		void switchToNewCapture(cv::VideoCapture * newCapture,
								const bool synchronized = false,
								const double frameRate = defaultConstrainedFrameRate);

		/**
		 * Wait for at least one call to update method in the worker thread:
		 *	- blocks all signals (so other classes are NOT notified of update)
		 *	- Create a new #updateCondition
		 *	- Wait on this condition using updateCondition->wait(&mutex)
		 *	- Then delete #updateCondition and set it to nullptr
		 *	- unblocks all signals (so other classes can be notified of update)
		 * @pre #updateCondition shall be nullptr
		 * @pre mutex should be locked prior to calling this method
		 */
		void waitForUpdate();
};

#endif // QCVTHREADEDVIDEOCAPTURE_H
