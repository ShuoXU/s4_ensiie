/****************************************************************************
** Meta object code from reading C++ file 'QcvGFilter.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "QcvGFilter.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QcvGFilter.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QcvGFilter_t {
    QByteArrayData data[14];
    char stringdata0[148];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QcvGFilter_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QcvGFilter_t qt_meta_stringdata_QcvGFilter = {
    {
QT_MOC_LITERAL(0, 0, 10), // "QcvGFilter"
QT_MOC_LITERAL(1, 11, 17), // "kernelSizeChanged"
QT_MOC_LITERAL(2, 29, 0), // ""
QT_MOC_LITERAL(3, 30, 10), // "kernelSize"
QT_MOC_LITERAL(4, 41, 15), // "edgeModeChanged"
QT_MOC_LITERAL(5, 57, 9), // "activated"
QT_MOC_LITERAL(6, 67, 6), // "update"
QT_MOC_LITERAL(7, 74, 14), // "setSourceImage"
QT_MOC_LITERAL(8, 89, 4), // "Mat*"
QT_MOC_LITERAL(9, 94, 5), // "image"
QT_MOC_LITERAL(10, 100, 14), // "setDisplayMode"
QT_MOC_LITERAL(11, 115, 11), // "displayMode"
QT_MOC_LITERAL(12, 127, 11), // "setEdgeMode"
QT_MOC_LITERAL(13, 139, 8) // "edgeMode"

    },
    "QcvGFilter\0kernelSizeChanged\0\0kernelSize\0"
    "edgeModeChanged\0activated\0update\0"
    "setSourceImage\0Mat*\0image\0setDisplayMode\0"
    "displayMode\0setEdgeMode\0edgeMode"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QcvGFilter[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x06 /* Public */,
       4,    1,   47,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   50,    2, 0x0a /* Public */,
       7,    1,   51,    2, 0x0a /* Public */,
      10,    1,   54,    2, 0x0a /* Public */,
      12,    1,   57,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Bool,    5,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 8,    9,
    QMetaType::Void, QMetaType::Int,   11,
    QMetaType::Void, QMetaType::Int,   13,

       0        // eod
};

void QcvGFilter::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QcvGFilter *_t = static_cast<QcvGFilter *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->kernelSizeChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->edgeModeChanged((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 2: _t->update(); break;
        case 3: _t->setSourceImage((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        case 4: _t->setDisplayMode((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 5: _t->setEdgeMode((*reinterpret_cast< const int(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QcvGFilter::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvGFilter::kernelSizeChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QcvGFilter::*_t)(const bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvGFilter::edgeModeChanged)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject QcvGFilter::staticMetaObject = {
    { &QcvProcessor::staticMetaObject, qt_meta_stringdata_QcvGFilter.data,
      qt_meta_data_QcvGFilter,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QcvGFilter::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QcvGFilter::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QcvGFilter.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "CvGFilter"))
        return static_cast< CvGFilter*>(this);
    return QcvProcessor::qt_metacast(_clname);
}

int QcvGFilter::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QcvProcessor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void QcvGFilter::kernelSizeChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QcvGFilter::edgeModeChanged(const bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
