#include <cassert>

#include <QDebug>
#include <QVector>
#include <QDoubleSpinBox>

#include <Qcv/controllers/QRangeDoubleController.h>

/*
 * Constructor from RangeValue, mutex and parent object
 * @param range the RangeValue<double> to reference
 * @param lock the lock (from the processor containing the RangeValue) to
 * use when modifying the RangeValue
 * @param the factor to apply to the range values when emitting or
 * the factor to divide outside values when setting a new range value.
 * @param callback the call back to call for setting the value instead
 * of settting the value directly
 * @param parent parent object
 * @param settings the settings to use to set initial value and/or
 * save the current value (if different from default value) to settings
 * @param settingsName the name to use when reading or writing the
 * parameter value to settings
 */
QRangeDoubleController::QRangeDoubleController(RangeValue<double> * const range,
											   QMutex * const lock,
											   const double & factor,
											   function<void(const double)> * const callback,
											   QObject * parent,
											   QSettings * settings,
											   const QString & settingsName) :
	QObject(parent),
	QRangeAbstractController(range,
							 lock,
							 factor,
							 callback,
							 settings,
							 settingsName)
{
	if (settings != nullptr && settingsName.size() > 0)
	{
		// read "inside" value from preferences
		QVariant variant = settings->value(settingsName);
		if (variant.isValid())
		{
			double prefDoubleValue = variant.toDouble();
			if ((prefDoubleValue != managedValue->value()) &&
				(prefDoubleValue >= managedValue->min()) &&
				(prefDoubleValue <= managedValue->max()))
			{
				// sets a new "outside" value
				setValue(prefDoubleValue * factor);
			}
		}
	}
}

/*
 * Copy Constructor
 * @param rc the range controller to copy
 */
QRangeDoubleController::QRangeDoubleController(const QRangeDoubleController & rc) :
	QRangeDoubleController(rc.managedValue,
						   rc.lock,
						   rc.factor,
						   rc.callback,
						   rc.parent(),
						   rc.settings,
						   rc.settingsName)
{
}

/*
 * Move Constructor
 * @param rci the range controller to move
 */
QRangeDoubleController::QRangeDoubleController(QRangeDoubleController && rc) :
	QRangeDoubleController(rc.managedValue,
						   rc.lock,
						   rc.factor,
						   rc.callback,
						   rc.parent(),
						   rc.settings,
						   rc.settingsName)
{
}

/*
 * Destructor
 */
QRangeDoubleController::~QRangeDoubleController()
{
	disconnect(this, 0, 0, 0);
}

/*
 * Setup a spin box with the values in the RangeValue and connect
 * the necessary signals and slots
 * @param spinBox the spinbox to setup with
 * @return true if the spinBox has been correctly set up, false
 * otherwise
 * @post If this spinbox was not part of the widgets set, then the
 * spinBox has been setup with RangeValue values and bidirectionnal
 * connections between this and the spinBox have been created.
 */
bool QRangeDoubleController::setupSpinBox(QAbstractSpinBox * spinBox)
{
	bool setup = QRangeAbstractController<double>::setupSpinBox(spinBox);

	if (setup)
	{
		QDoubleSpinBox * doubleSpinBox = dynamic_cast<QDoubleSpinBox *>(spinBox);
		if (doubleSpinBox != nullptr)
		{
			connect(doubleSpinBox, SIGNAL(valueChanged(double)),
					this, SLOT(setValue(double)));
			/*
			 * this->setValue will block signals from spinBox before emitting
			 * valueChanged in order to avoid signal/slot loop
			 */
			connect(this, SIGNAL(valueChanged(double)),
					doubleSpinBox, SLOT(setValue(double)));
		}
		else
		{
			qWarning() << "QRangeDoubleController::setupSpinBox can't cast to QDoubleSpinBox";
		}
	}

	return setup;
}

/*
 * Setup a slider with the values in the RangeValue (and connects the
 * necessary signals and slots).
 * @param slider the slider to setup according to RangeValue values
 * @return true if the slider have been set up correctly, false
 * otherwise
 * @post If this slider was not part of the widgets set, then the
 * slider has been setup with RangeValue steps and bidirectionnal
 * connections between this and the slider have been created.
 */
bool QRangeDoubleController::setupSlider(QAbstractSlider * slider)
{
	bool setup = QRangeAbstractController<double>::setupSlider(slider);

	if (setup)
	{
		connect(slider, SIGNAL(sliderMoved(int)), this, SLOT(setIndex(int)));
		/*
		 * this->setIndex() will block signals from slider before emitting
		 * indexChanged() signal in order to avoid signal/slot loop
		 */
		connect(this, SIGNAL(indexChanged(int)), slider, SLOT(setValue(int)));
		connect(this, SIGNAL(indexRangeChanged(int,int)),
				slider, SLOT(setRange(int,int)));
	}

	return setup;
}

/*
 * Adds a reset button to this range controller in order to reset to the
 * default value
 * @param button the button to connect to the reset slot
 */
bool QRangeDoubleController::addResetButton(QAbstractButton * button)
{
	bool setup = QAbstractController<RangeValue<double>, double>::setupSource(button);

	if (setup)
	{
		connect(button, SIGNAL(clicked()), this, SLOT(reset()));
	}
	else
	{
		qWarning() << button << "has not been setup correctly";
	}

	return setup;
}

/*
 * Connects the corresponding xxxChanged(double) signal to the setNum
 * slot of a QLabel
 * @param label the label to show the minimum value
 * @param which the kind of value to be shown in the label
 * @return true if the label pointer was not null
 */
bool QRangeDoubleController::addNumberLabel(QLabel * label,
											RangeValue<double>::WhichValue which)
{
	bool result = QAbstractController<RangeValue<double>, double>::setupSource(label);

	if (result)
	{
		double value;
		switch (which)
		{
			case RangeValue<double>::WhichValue::VALUE:
				value = managedValue->value();
				result = connect(this, SIGNAL(valueChanged(double)),
								 label, SLOT(setNum(double)));
				break;
			case RangeValue<double>::WhichValue::MIN_VALUE:
				value = managedValue->min();
				result = connect(this, SIGNAL(minChanged(double)),
								 label, SLOT(setNum(double)));
				break;
			case RangeValue<double>::WhichValue::MAX_VALUE:
				value = managedValue->max();
				result = connect(this, SIGNAL(maxChanged(double)),
								 label, SLOT(setNum(double)));
				break;
			case RangeValue<double>::WhichValue::STEP_VALUE:
				value = managedValue->step();
				result = connect(this, SIGNAL(stepChanged(double)),
								 label, SLOT(setNum(double)));
				break;
			default:
				result = false;
				break;
		}

		if (result)
		{
			label->setNum(value);
		}
	}
	else
	{
		qWarning() << label << "has not been setup correctly";
	}


	return result;
}

/*
 * Slot to change the #enabled state of GUI elements connected to this
 * controller
 * @param value the new enabled state
 */
void QRangeDoubleController::setEnabled(const bool value)
{
	QAbstractController<RangeValue<double>, double>::setEnabled(value);
}

/*
 * Sets new value in the range value.
 * @param value the value to set
 * @return true if the value has been set without correction, false
 * otherwise. In any case a value is set according the rules of
 * RangeValue<T>::setClosestValue
 * @note slot typically used by QSpinBox when its value changes
 * @note Please note that if result is false a signal needs to be sent
 * to the connected UI to ajust the value since it may have been
 * slighltly modified.
 * @see QAbstractRangeController::setValue(const T &)
 */
bool QRangeDoubleController::setValue(const double value)
{
	return QRangeAbstractController<double>::setValue(value);
}

/*
 * Sets a new value corresponding to the index
 * @param index the index to set the new value to
 * @return true in index is valid (the corresponding value is less or
 * equal to the max value
 * @see QAbstractRangeController::setIndex(const int)
 */
bool QRangeDoubleController::setIndex(const int index)
{
	return QRangeAbstractController<double>::setIndex(index);
}

/*
 * Reset managed value to its default value
 * @return true if the value has been reset to its default value exactly,
 * false if the reset has triggered bounds or step modifications
 * @note This method needs to be be reimplemented since it is a slot now
 */
bool QRangeDoubleController::reset()
{
	return QRangeAbstractController<double>::reset();
}

/*
 * Refresh all connected UI.
 * When the internal managedValue has been modified outside of this
 * controller, all connected UIs should be refreshed
 */
void QRangeDoubleController::refresh()
{
	QRangeAbstractController<double>::refresh();

	/*
	 * Block signals in connected widgets to avoid signal/slots loops
	 */
	blockSignalsOnConnectedWidgets();

	/*
	 * Then for widgets that don't react to signals refresh their values directly
	 * Typically
	 *	- SpinBoxes : min, max & step values have to be set through setters
	 *  - Sliders : step has to be set through setters but is always 1
	 *	so it does not change
	 */
	for (int i = 0; i < sources.size(); ++i)
	{
		QDoubleSpinBox * spinBox = dynamic_cast<QDoubleSpinBox *>(sources[i]);

		if (spinBox != nullptr)
		{
			spinBox->setMinimum(managedValue->min());
			spinBox->setMaximum(managedValue->max());
			spinBox->setSingleStep(managedValue->step());
		}
	}

	/*
	 * Unblock signals in connected widgets
	 */
	unblockSignalsOnConnectedWidgets();
}

/*
 * Connects this controller's valueChanged signal to "controller"'s
 * setValue Slot
 * @param controller the controller to connect
 * @note concrete subclasses must implement this method
 */
void QRangeDoubleController::connectMirror(QAbstractController<RangeValue<double>, double> * controller)
{
	if (controller != nullptr)
	{
		QRangeDoubleController * rController = dynamic_cast<QRangeDoubleController *>(controller);
		if (rController != nullptr)
		{
			if (checkMirror(controller))
			{
				connect(this, SIGNAL(valueChanged(double)),
						rController, SLOT(setValue(double)));
			}
			else
			{
				qWarning() << Q_FUNC_INFO << "controller" << controller
						   << "is not compatible with"
						   << this;
			}
		}
		else
		{
			qWarning() << Q_FUNC_INFO << "controller " << controller
					   << "could not be casted to"
					   << this->metaObject()->className();
		}
	}
}

/*
 * Disconnects this controller's valueChanged signal from "controller"'s
 * setValue Slot
 * @param controller the controller to disconnect
 * @note concrete subclasses must implement this method
 */
void QRangeDoubleController::disconnectMirror(QAbstractController<RangeValue<double>, double> * controller)
{
	if (controller != nullptr)
	{
		QRangeDoubleController * rController = dynamic_cast<QRangeDoubleController *>(controller);
		if (rController != nullptr)
		{
			disconnect(this, SIGNAL(valueChanged(double)),
					   rController, SLOT(setValue(double)));
		}
		else
		{
			qWarning() << Q_FUNC_INFO << "controller " << controller
					   << "could not be casted to"
					   << this->metaObject()->className();
		}
	}
}

/*
 * sends a signal when anything changes
 */
void QRangeDoubleController::emitUpdated()
{
	emit updated();
}

/*
 * Sends a signal when value has been set
 * @param value the new value
 */
void QRangeDoubleController::emitValueChanged(const double value)
{
	emit valueChanged(value);
}

/*
 * Sends a signal when value has been set in the RangeValue which
 * changes the index of the current value
 * @param index the index of the new value
 * @note typically used by QSlider
 */
void QRangeDoubleController::emitIndexChanged(const int index)
{
	emit indexChanged(index);
}

/*
 * Sends a signal when min value in the range changes
 * @param value the new min value in the range
 */
void QRangeDoubleController::emitMinChanged(const double & value)
{
	emit minChanged(value);
}

/*
 * Sends a signal when max value in the range changes
 * @param value the new max value in the range
 */
void QRangeDoubleController::emitMaxChanged(const double & value)
{
	emit maxChanged(value);
}

/*
 * Sends a signal when min or max value changes
 * @param min the minimum value of the range
 * @param max the maximum value of the range
 */
void QRangeDoubleController::emitRangeChanged(const double & min,
											  const double & max)
{
	emit rangeChanged(min, max);
}

/*
 * Sends a signal when step value in the range changes
 * @param value the new step value in the range
 */
void QRangeDoubleController::emitStepChanged(const double & value)
{
	emit stepChanged(value);
}


/*
 * Sends a signal when min or max changes (because it might affect the
 * indices of a slider for instance)
 * @param min the minimum index (always 0)
 * @param max the maximum index (the number of steps of the range value)
 */
void QRangeDoubleController::emitIndexRangeChanged(const int & minIndex,
												   const int & maxIndex)
{
	emit indexRangeChanged(minIndex, maxIndex);
}
