#include <QDebug>

#include "QcvAlgoParamWidget.h"

/*
 * Constructor
 * @param param the algorithm parameter
 * @param direct direct operation: if true indicates that parameter
 * value setting is performed directly through
 * param->set[Bool|Int|Double]Value(...) which is blocking in
 * multithread environments. If false indicates that parameter value
 * setting is performed through signal/slot mechanism, which in
 * multithreaded environments is non blocking with queued connections.
 * @param settings the settings to use to set initial value and/or
 * save the current value (if different from default value) to settings
 * @param settingsName the name to use when reading or writing the
 * parameter value to settings
 * @param parent the parent widget
 */
QcvAlgoParamWidget::QcvAlgoParamWidget(CvAlgoParam * param,
									   const bool direct,
									   QSettings * settings,
									   const QString & settingsName,
									   QWidget *parent) :
	QWidget(parent),
	parameter(param),
	directOp(direct),
	settings(settings),
	settingsName(settingsName)
{
	assert(parameter != nullptr);
}

/*
 * Destructor.
 * does nothing here but except removing all algorithms from parameter
 * and clears UI in subclasses.
 * @post the algorithms associated to this parameter have been released
 * from this parameter before it is destroyed.
 */
QcvAlgoParamWidget::~QcvAlgoParamWidget()
{
	// Disconnect anything still connected to any signal
	// disconnect(this, 0, 0, 0);
	disconnect();

	parameter->removeAllAlgorithms();
}

/*
 * Save a value to settings (iff #settings is non null and
 * #settingsName is non empty)
 * @param value the QVariant value to save into preferences
 * @note Should be implemented in subclasses to actually use the type
 * of the parameter to save it.
 */
void QcvAlgoParamWidget::writeSetting(const QVariant & value) const
{
	if ((settings != nullptr) && !settingsName.isEmpty())
	{
//		qDebug() << Q_FUNC_INFO << settingsName << value;
		settings->setValue(settingsName, value);
	}
}

/*
 * Remove settings corresponding to #settingsName from preferences (iff
 * #settings are available and #settingsName is non empty)
 */
void QcvAlgoParamWidget::removeSetting() const
{
	if (settings != nullptr && !settingsName.isEmpty())
	{
		settings->remove(settingsName);
	}
}


