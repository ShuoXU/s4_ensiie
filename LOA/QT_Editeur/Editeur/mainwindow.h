#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>
#include <QFileDialog>
#include <QTextEdit>
#include <QPlainTextEdit>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    int globalMark_exit;
    QTextEdit *textEdit;
   // QString showFileDialog(QFileDialog::AcceptMode mode, QString title);


private slots:
    void on_actionEnregistrer_triggered();

    void on_actionOuvrir_triggered();

private:
    Ui::MainWindow *ui;
//    QTextEdit* textEdit ;
    QPlainTextEdit mainEditor;
};

#endif // MAINWINDOW_H
