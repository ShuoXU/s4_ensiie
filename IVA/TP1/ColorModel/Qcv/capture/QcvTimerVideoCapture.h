/*
 * QcvVideoCapture.h
 *
 *  Created on: 29 janv. 2012
 *	  Author: davidroussel
 */

#ifndef QCVTIMERVIDEOCAPTURE_H
#define QCVTIMERVIDEOCAPTURE_H

#include <QObject>
#include <QSize>
#include <QTimer>
#include <QThread>
#include <QMutex>

#include <opencv2/highgui/highgui.hpp>
using namespace cv;

#include <Qcv/capture/QcvVideoCapture.h>

/**
 * Qt Class for capturing videos from cameras or files with OpenCV.
 * QcvVideoCapture opens streams and refresh itself automatically.
 * When frame has been refreshed a signal is emitted.
 * @note This implementation uses a timer to time image refresh. The delay
 * between two images is eventually estimated by grabbing several images.
 * @note This capture can eventually be threaded or can also run in the current
 * thread.
 * @note This capture is "skippable", meaning, in skippable mode when capture
 * can't get lock when updating, then capture is aborted until next update
 */
class QcvTimerVideoCapture: public QcvVideoCapture
{
	Q_OBJECT

	private:

		/**
		 * file name used to open video file.
		 * Used to reopen video file when video is finished.
		 */
		QString filename;

		/**
		 * refresh timer
		 */
		QTimer * timer;

		/**
		 * Independant thread to update capture.
		 * If independant thread is required, then update method is called
		 * from within this thread. Otherwise, update method is called from
		 * main thread.
		 */
		QThread * updateThread;

		/**
		 * Mutex lock state memory to avoid locking the mutex multiple times
		 * across multiple methods. When a mutex.lock() is performed locked
		 * should be set to true until mutex.unlock(). Hence, if a method
		 * requiring lock is performed, a second lock is avoided by checking
		 * this attribute.
		 */
		size_t lockLevel;

		/**
		 * Allow capture to skip an image capture when lock can't be acquired
		 * before grabbing a new image. Otherwise we'll wait until the lock
		 * is acquired before grabbing an new image. The lock might be acquired
		 * by another lenghty thread/processor during image processing.
		 */
		bool skip;

		/**
		 * default time interval between refresh
		 */
		static int defaultFrameDelay;

		/**
		 * Number of frames to test frame rate
		 */
		static size_t defaultFrameNumberTest;

	public:
		/**
		 * Constructor. Opens the default camera (0)
		 * @param flipped mirror image status
		 * @param gray convert image to gray status
		 * @param skip indicates capture can skip an image. When the capture
		 * result has not been processed yet, or when false that capture should
		 * wait for the result to be processed before grabbing a new image.
		 * This only applies when #updateThread is not NULL.
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * otherwise capture is updated in the current thread.
		 * @param updateThread the thread used to run this capture
		 * @param parent the parent QObject
		 */
		QcvTimerVideoCapture(const bool flipped = false,
							 const bool gray = false,
							 const bool skip = true,
							 const size_t  width = 0,
							 const size_t height = 0,
							 QThread * updateThread = NULL,
							 QObject * parent = NULL);

		/**
		 * Constructor with device Id
		 * @param deviceId the id of the camera to open
		 * @param flipped mirror image
		 * @param gray convert image to gray
		 * @param skip indicates capture can skip an image. When the capture
		 * result has not been processed yet, or when false that capture should
		 * wait for the result to be processed before grabbing a new image.
		 * This only applies when #updateThread is not NULL.
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @param updateThread the thread used to run this capture
		 * @param parent the parent QObject
		 * @post If the #grabTest() has been sucessful, the following signals
		 * have been emitted:
		 *	- #timerChanged(int);
		 *	- #frameRateChanged(double);
		 */
		QcvTimerVideoCapture(const int deviceId,
							 const bool flipped = false,
							 const bool gray = false,
							 const bool skip = true,
							 const size_t  width = 0,
							 const size_t height = 0,
							 QThread * updateThread = NULL,
							 QObject * parent = NULL);

		/**
		 * QcvVideoCapture constructor from file name
		 * @param fileName video file to open
		 * @param flipped mirror image
		 * @param gray convert image to gray
		 * @param skip indicates capture can skip an image. When the capture
		 * result has not been processed yet, or when false that capture should
		 * wait for the result to be processed before grabbing a new image.
		 * This only applies when #updateThread is not NULL.
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @param updateThread the thread used to run this capture
		 * @param parent the parent QObject
		 * @post If the #grabTest() has been sucessful, the following signals
		 * have been emitted:
		 *	- #timerChanged(int);
		 *	- #frameRateChanged(double);
		 */
		QcvTimerVideoCapture(const QString & fileName,
							 const bool flipped = false,
							 const bool gray = false,
							 const bool skip = true,
							 const size_t  width = 0,
							 const size_t height = 0,
							 QThread * updateThread = NULL,
							 QObject * parent = NULL);

		/**
		 * Destructor.
		 * releases video capture and image
		 */
		virtual ~QcvTimerVideoCapture();

		/**
		 * Gets the image skipping policy
		 * @return true if new image can be skipped when previous one has not
		 * been processed yet, false otherwise.
		 */
		bool isSkippable() const;

		/**
		 * Indicates if capture works in synchronized mode where
		 * frame rate is imposed.
		 * @return true is capture works in synchronized mode, false
		 * otherwise
		 * @note Please note that this implementation always return true since
		 * the capture is drived by a timer, although there cannot be a
		 * constrained frame rate for the very same reason.
		 */
		bool isSynchronized() const override;

		/**
		 * Gets the contrained frame rate at which capture should operate
		 * when synchronized mode is on
		 * @return the constrained frame rate
		 * @note Please note that this implementation will always return the
		 * same value as #getFrameRate() as the capture is always drived by a
		 * timer (#timer)
		 */
		double getContrainedFrameRate() const override;

	public slots:
		/**
		 * Open new device Id
		 * @param deviceId device number to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if device has been opened and checked and timer launched
		 * @post if the capture device is sucessfully opened, the following
		 * signals have been sent:
		 *	- #timerChanged(int);
		 *	- #frameRateChanged(double);
		 *	- #imageChanged(cv::Mat *);
		 */
		bool openDevice(const int deviceId,
						const size_t width = 0,
						const size_t height = 0) override;

		/**
		 * Open new video file
		 * @param fileName video file to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if video has been opened and timer launched
		 * @post if the file is sucessfully opened in capture, the following
		 * signals have been sent:
		 *	- #timerChanged(int);
		 *	- #frameRateChanged(double);
		 *	- #imageChanged(cv::Mat *);
		 */
		bool openFile(const QString & fileName,
					  const size_t width = 0,
					  const size_t height = 0) override;
		/**
		 * Sets video flipping
		 * @param value flipped video or not
		 * @post if flipped status is changed then the following signals
		 * have been emitted:
		 *	- #messageChanged(QString);
		 *	- #imageChanged(cv::Mat *);
		 */
		void setFlipped(const bool value) override;

		/**
		 * Sets video conversion to gray
		 * @param value the gray conversion status
		 * @post if gray status is changed then the following signals
		 * have been emitted:
		 *	- #messageChanged(QString);
		 *	#imageChanged(cv::Mat *); is emitted anyways
		 */
		void setGray(const bool value) override;

		/**
		 * Sets capture to synchronized mode (when true) where frame rate is
		 * contrained (whenever possible). Otherwise frame rate is not
		 * constrained and capture may proceed as fast as possible.
		 * @param value the new value of synchronized mode
		 * @note Please note that this method has NO effect on this class
		 */
		void setSynchronized(const bool value) override;

		/**
		 * Sets the desired frame rate on capture ONLY when capture is in
		 * synchronized mode
		 *	- If the new frame rate is 0.0 then use natural capture frame rate.
		 *	- If the new frame rate is too high for capture then capture will
		 *	keeps its highest possible frame rate according to the device in use
		 * @param frameRate the new frame rate to set
		 * @note Please note that this method has NO effect on this class
		 */
		void setFrameRate(const double frameRate) override;

		/**
		 * Releases capture and emits #finished() signal
		 * @note typically #finish(); is called from the update thread so it is
		 * useless to try to wait on the update thread to finish since a thread
		 * can't wait on itself.
		 */
		void finish() override;

	private:
		/**
		 * Performs a grab test to fill #image.
		 * if capture is opened then tries to grab and if grab succeeds then
		 * tries to retrieve image from grab and sets image size.
		 * @return true if capture is opened and successfully grabbed a first
		 * frame into #image, false otherwise
		 * @post Moreover this method determines if direct resizing is allowed
		 * on this capture instance by trying to set
		 * CV_CAP_PROP_FRAME_WIDTH and CV_CAP_PROP_FRAME_HEIGHT.
		 */
		bool grabTest();

		/**
		 * Get or compute interval between two frames in ms and sets the
		 * frameRate attribute.
		 * Tries to get CV_CAP_PROP_FPS from capture and if not available
		 * computes times between frames by grabbing defaultNumberTest images
		 * @return interval between two frames
		 * @param message message passed to grabInterval and display ahead of
		 * the framerate computed during grabInterval
		 * @pre capture is already instanciated
		 * @post message indicating frame rate has been emitted and interval
		 * between two frames has been returned
		 */
		int grabInterval(const QString & message);

		/**
		 * Sets #imageDisplay size according to preferred width and height
		 * @param width desired width
		 * @param height desired height
		 * @pre a first image have been grabbed
		 * @post the #messageChanged(QString); signal has been emitted
		 */
		void setSize_Impl(const size_t width,
						  const size_t height) override;

	protected slots:
		/**
		 * update slot trigerred by timer : Grabs a new image and sends updated()
		 * signal iff new image has been grabbed, otherwise there is no more
		 * images to grab so kills timer.
		 * @note If lock on OpenCV capture object can not be obtained then
		 * capture is skipped. This is not critical since update is called
		 * regularly by the #timer, so we'll try updating image next time.
		 * @post The following signals are emitted:
		 *	- #messageChanged(QString); is emitted when
		 *		- There is no more frames to capture
		 *		- When capture is restarted (when reading video files) and when
		 *		it fails to restart
		 *	- #restarted() is emitted when capture restarts
		 *	- #finished is emitted if capture restart fails
		 *	- #updated(); is emitted if a new image have been grabbed
		 */
		void update();

	signals:
		/**
		 * Signal emitted when timer is started whith a new delay
		 * @param delay the new timer delay value
		 */
		void timerChanged(const int delay);

		/**
		 * Signal to send when we need to stop the timer from another thread
		 */
		void stopTimer();
};

#endif /* QCVTIMERVIDEOCAPTURE_H */
