#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <QTime>
#include <QDir>
#include <QFile>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

//    textEdit = new QTextEdit(this);

//    this->setCentralWidget(textEdit);


    ui->textEdit->setText("Please enter your inputs.");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOuvrir_triggered()
{

    QString filename = QFileDialog::getOpenFileName(this, tr("open txt file"), tr("Txt files(*.txt)"));
    if(filename.isEmpty()){
        QMessageBox mesg;
        mesg.warning(this, "warning", "Fail to open this file!");
        return ;
    }
    else{
        QFile file(filename);
        if(file.open(QIODevice::ReadOnly)){
            QTextStream ts(&file);
            ui->textEdit->setText(ts.readAll());

        }
        file.close();

    }
}

void MainWindow::on_actionEnregistrer_triggered()
{

    if (globalMark_exit == 1){
        QString dirPath;
        QString filePath;
        QString saveName = "saved txt file";
        QDateTime time  = QDateTime::currentDateTime();
        QString str = time.toString("yyyyMMdd_hhmmss");
        filePath = QFileDialog::getExistingDirectory(this,"");
        if(filePath.isEmpty()){
            QMessageBox::information(this,"message","fail to save the file");
        }
        else{
            dirPath = QString("%1/save experiment%2").arg(filePath).arg(str);
            QDir *temp = new QDir;
            temp->mkdir(dirPath);
            QTextEdit *newEdit;
            newEdit = textEdit;
            QString savePath = QString("%1//%2.txt").arg(dirPath).arg(saveName);
            newEdit->saveGeometry();
            QMessageBox::information(this,"message","succeed in saving the file");

        }
    }
    else{
        QMessageBox::information(this,"message","fail to save the file");
    }
}

