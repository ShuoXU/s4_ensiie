/*
 * QAbstractController.cpp
 *
 *  Created on: 16 juin 2015
 *      Author: davidroussel
 */
#include <cassert>
#include <QDebug>
#include <QWidget>
#include <QAction>

#include <Qcv/controllers/QAbstractController.h>

/*
 * Constructor from value, mutex and evt callback
 * @param value a constant pointer to the value to manage
 * @param lock a constant pointer to the lock (from the processor
 * containing the value) to check when modifying the value
 * @param callback the call back to call for setting the value instead
 * of settting the value directly
 * @param settings the settings to use to set initial value and/or
 * save the current value (if different from default value) to settings
 * @param settingsName the name to use when reading or writing the
 * parameter value from or to settings (if any)
 * @warning If lock is null then we are supposed to use a callback which
 * uses a mutex. If callback is null then we are supposed to use a mutex
 * on the managed value. In other words : lock AND callback should NOT
 * be both null.
 */
template <typename T, typename V>
QAbstractController<T, V>::QAbstractController(T * const value,
											   QMutex * const lock,
											   function<void(const V)> * callback,
											   QSettings * settings,
											   const QString & settingsName) :
	managedValue(value),
	lock(lock),
	callback(callback),
	enabled(true),
	settings(settings),
	settingsName(settingsName)
{
	assert(managedValue != nullptr);
//	assert(lock != nullptr || callback != nullptr);
}

/*
 * Copy Constructor
 * @param controller the controller to copy
 */
template <typename T, typename V>
QAbstractController<T, V>::QAbstractController(const QAbstractController<T, V> & controller) :
	QAbstractController(controller.managedValue,
						controller.lock,
						controller.callback,
						controller.settings,
						controller.settingsName)
{
	sources = controller.sources;
	sourceLockStatus = controller.sourceLockStatus;
	constraintControllers = controller.constraintControllers;
	linkedControllers = controller.linkedControllers;
	mirrorControllers = controller.mirrorControllers;
}

/*
 * Move Constructor
 * @param controller the controller to move
 */
template <typename T, typename V>
QAbstractController<T, V>::QAbstractController(QAbstractController<T, V> && controller) :
	QAbstractController(controller.managedValue,
						controller.lock,
						controller.callback,
						controller.settings,
						controller.settingsName)
{
	sources = controller.sources;
	sourceLockStatus = controller.sourceLockStatus;
	constraintControllers = controller.constraintControllers;
	linkedControllers = controller.linkedControllers;
	mirrorControllers = controller.mirrorControllers;
	// controller.managedValue = nullptr; /* cannot change const */
	// controller.lock = nullptr; /* cannot change const */
	controller.sources.clear();
	controller.sourceLockStatus.clear();
	controller.constraintControllers.clear();
	controller.constraints.clear();
	controller.linkedControllers.clear();
	controller.mirrorControllers.clear();
	controller.callback = nullptr;
	controller.settings = nullptr;
	controller.settingsName.clear();
}

/*
 * Destructor
 */
template <typename T, typename V>
QAbstractController<T, V>::~QAbstractController()
{
	sources.clear();
	sourceLockStatus.clear();
	constraintControllers.clear();
	constraints.clear();
	linkedControllers.clear();
	mirrorControllers.clear();
	if (callback != nullptr)
	{
		delete callback;
	}
}

/*
 * Check if this object is already in sources
 * @param source the source to find in #sources
 * @return true if this QObject is already part of #sources, and false
 * otherwise
 */
template <typename T, typename V>
bool QAbstractController<T, V>::has(QObject * source)
{
	return sources.contains(source);
}

/*
 * Indicates if GUI elements linked to this controller are enabled
 * @return the enabled state
 */
template <typename T, typename V>
bool QAbstractController<T, V>::isEnabled() const
{
	return enabled;
}

/*
 * Changes the #enabled state of GUI elements connected to this
 * controller
 * @param value the new enabled state
 */
template <typename T, typename V>
void QAbstractController<T, V>::setEnabled(const bool value)
{
	enabled = value;
	for (QList<QObject *>::iterator it = sources.begin(); it != sources.end(); ++it)
	{
		QWidget * widget = dynamic_cast<QWidget *>(*it);
		if (widget != nullptr)
		{
			widget->setEnabled(value);
		}

		QAction * action = dynamic_cast<QAction *>(*it);
		if (action != nullptr)
		{
			action->setEnabled(value);
		}
	}
}

/*
 * Setup a QObject source (typically a widget) to reflect the managed
 * value.
 * Since we do not know yet what to do with the source, this method only
 * checks if the source is already part of the connected sources or not.
 * @param source the source to setup
 * @return true if the source was correclty set up, false otherwise
 * (for example when the widget was already part of connected widgets).
 * @note subclasses overloads might actually configure this source
 * properly and also setup connections from/to this source.
 */
template <typename T, typename V>
bool QAbstractController<T, V>::setupSource(QObject * source)
{
	if (source != nullptr)
	{
		// Adds this source to sources collection
		if (!sources.contains(source))
		{
			sources += source;
			sourceLockStatus += source->signalsBlocked();

			return true;
		}
		else
		{
			qWarning() << "source object" << source << "is already in sources";
		}
	}

	return false;
}

/*
 * Sets new value.
 * @param value the value to set
 * @return true if the value has been set without correction, false
 * otherwise.
 * @post The following signals have been emitted (by subclasses):
 *	- valueChanged(V)
 *	- updated()
 */
template <typename T, typename V>
bool QAbstractController<T, V>::setValue(const V value)
{
	// If the value to set is different from the stored value using T's operator cast to V
	// if (static_cast<V>(value) != static_cast<V>(*managedValue))
	if (value != static_cast<V>(*managedValue))
	{
		bool hasLock = (lock != nullptr);
		// Using T's operator cast to V
		V oldValue = static_cast<V>(*managedValue);
		bool result;
		bool propagate = false;

		if (hasLock)
		{
			lock->lock();
		}

		// Using T's copy operator from V
		if (callback == nullptr)
		{
			*managedValue = value;
		}
		else
		{
			(*callback)(value);
		}

		if (!checkConstraints())
		{
			qDebug() << Q_FUNC_INFO << "Contraints do not check out";
			// Using T's copy operator from V
			if (callback == nullptr)
			{
				*managedValue = oldValue;
			}
			else
			{
				(*callback)(oldValue);
			}
			result = false;
		}
		else
		{
			result = true;
			propagate = linkedControllers.contains(value);
		}

		// Save value to preferences (if possible)
		if ((settings != nullptr) &&
			(settingsName.size() > 0) &&
			(static_cast<V>(*managedValue) != oldValue))
		{
			settings->setValue(settingsName, static_cast<V>(*managedValue));
		}

		if (hasLock)
		{
			lock->unlock();
		}

		/*
		 * There is no need to update the controllers in #mirrorControllers
		 * directly since they should be connected to the future valuedChanged
		 * signal
		 */

		if (result && propagate)
		{
			propagateLinkedValue(value);
		}

		/*
		 * Block signals in connected widgets to avoid signal/slots loops
		 */
		blockSignalsOnConnectedWidgets();

		/*
		 * Emits signals
		 */
		emitValueChanged(static_cast<V>(*managedValue));
		emitUpdated();

		/*
		 * Unblock signals in connected widgets
		 */
		unblockSignalsOnConnectedWidgets();

		return result;
	}

	return false;
}

/*
 * Adds constraint regarding another QController
 * @param controller the controller to set constraint with
 * @param predicate the predicate to respect when setting a new value
 * such that predicate(this->value, controller.value) should be true in
 * order to set a value in the embedded value
 * @return true if this constraint has been added to the list of
 * constraints, false if the list of constraints already contained a
 * constraint relative to "controller" OR if the constraint was not
 * satisfied, which means the constraint need to be verified before
 * adding it.
 */
template <typename T, typename V>
bool QAbstractController<T, V>::addConstraint(QAbstractController<T, V> * controller,
											  const function<bool(V, V)> & predicate)
{
	if (controller != nullptr && controller != this)
	{
		if (!constraintControllers.contains(controller))
		{
			if (predicate(static_cast<V>(*managedValue),
						  static_cast<V>(*(controller->managedValue))))
			{
				constraintControllers.push_back(controller);
				constraints.push_back(predicate);
				return true;
			}
		}
	}

	return false;
}

/*
 * Remove constraint relative to another controller
 * @param controller the controller to remove from constraints lists.
 * @return true if controller was part of the contraints list and
 * removed, false otherwise.
 */
template <typename T, typename V>
bool QAbstractController<T, V>::removeConstraint(QAbstractController<T, V> * controller)
{
	if (controller != nullptr)
	{
		assert(constraintControllers.size() == constraints.size());

		int index = constraintControllers.indexOf(controller);
		if (index != -1)
		{
			constraintControllers.removeAt(index);
			constraints.removeAt(index);
			return true;
		}
	}

	return false;
}

/*
 * Adds a pair of value/controller to the #linkedControllers so that
 * when this value is set in #setValue, it is also propagated to the
 * linked controllers corresponding to this value in the multimap.
 * @param controller the controller to progagate to
 * @param value the value to propagate to the other controller
 * @return true if the controller was not already linked and the pair
 * value/controller has been added
 */
template <typename T, typename V>
bool QAbstractController<T, V>::addLinkedValue(const V value,
					QAbstractController<T, V> * controller)
{
	if (controller != nullptr && controller != this)
	{
		if (linkedControllers.constFind(value, controller) ==
			linkedControllers.cend())
		{
			// we haven't found this key/value in the map so add it
			linkedControllers.insert(value, controller);
			return true;
		}
	}

	return false;
}

/*
 * Removes all elements with this specific keys in the
 * #linkedControllers map
 * @param value the key to remove from the map
 * @param controller the controller to remove from the map. If
 * controller is nullptr the all keys "value" are removed
 * @return true if the key has been removed from the map
 */
template <typename T, typename V>
bool QAbstractController<T, V>::removeLinkedValue(const V value,
					QAbstractController<T, V> * controller)
{
	if (controller == nullptr)
	{
		// remove all keys "value"
		int removed = linkedControllers.remove(value);
		return removed > 0;
	}
	else
	{
		// remove pair value/controller
		int removed = linkedControllers.remove(value, controller);
		return removed > 0;
	}
	return false;
}

/*
 * Adds a controller to mirror value settings
 * @param controller the controller which  should mirror value settings
 * in this controller
 * @return true if the controller wasn't already part of mirror
 * controllers and has been added in #mirrorContollers
 * @warning Do NOT use this method in a critical section since adding
 * a mirror controller will trigger a setValue slot on the mirrored
 * controller which uses the #lock mutex (if provided)
 * @post valueChanged(V) signal have been emitted by subclasses
 */
template <typename T, typename V>
bool QAbstractController<T, V>::addMirrorController(QAbstractController<T, V> * controller)
{
	if (controller != nullptr)
	{
		if (!mirrorControllers.contains(controller))
		{
			mirrorControllers.push_back(controller);
			connectMirror(controller);
			/*
			 * Emits Value changed so the new mirror controller can pick up
			 * the current value
			 */
			emitValueChanged(static_cast<V>(*managedValue));
			return true;
		}
	}

	return false;
}

/*
 * Removes a controller from mirror controllers list
 * @param controller the controller to remove from mirror controllers
 * list.
 * if this parameter is NULL then all controllers in mirror controllers
 * list are removed
 * @return true if the controllers was part of mirror controllers list
 * and has been remove. Or if it is NULL if any controller has been
 * removed from the mirror list
 */
template <typename T, typename V>
bool QAbstractController<T, V>::removeMirrorController(QAbstractController<T, V> * controller)
{
	if (controller != nullptr)
	{
		if (mirrorControllers.contains(controller))
		{
			disconnectMirror(controller);
			int result = mirrorControllers.removeAll(controller);
			return result > 0;
		}
	}
	else // controller == NULL
	{
		bool removeable = mirrorControllers.size() > 0;
		if (removeable)
		{
			for (auto it = mirrorControllers.begin();
				 it != mirrorControllers.end(); ++it)
			{
				disconnectMirror(*it);
			}
			mirrorControllers.clear();
			return true;
		}
	}

	return false;
}

/*
 * Sets a lock to guard setting values
 * @param lock the lock to use when operating values
 */
template <typename T, typename V>
void QAbstractController<T, V>::setLock(QMutex * lock)
{
	QMutex * oldLock = this->lock;
	if (oldLock != nullptr)
	{
		oldLock->lock();
	}

	this->lock = lock;

	if (oldLock != nullptr)
	{
		oldLock->unlock();
	}
}

/*
 * Indicates if a callback has been set in #callback to set values
 * @return true if the #callback is not null, false otherwise
 */
template <typename T, typename V>
bool QAbstractController<T, V>::hasCallback() const
{
	return callback != nullptr;
}

/*
 * Sets a callback to set the value in setValue rather than setting
 * the value directly.
 * This callback (if not null) will be used in setValue
 * @param callback the callback to set the value
 */
template <typename T, typename V>
void QAbstractController<T, V>::setCallBack(function<void(const V)> * callback)
{
	if (this->callback != nullptr)
	{
		delete this->callback;
	}
	this->callback = callback;
}

/*
 * Checks if all constraints are satisfied
 * @return true if all predicates returned true, false otherwise
 */
template <typename T, typename V>
bool QAbstractController<T, V>::checkConstraints() const
{
	int size = constraintControllers.size();
	assert(size == constraints.size());
	bool result = true;

	for (int i = 0; i < size && result; ++i)
	{
		result = result &&
				 constraints[i](static_cast<V>(*managedValue),
								static_cast<V>(*(constraintControllers[i]->managedValue)));
	}

	return result;
}

/*
 * Propagate value to linked controllers
 * @param value the value to propagate
 */
template <typename T, typename V>
void QAbstractController<T, V>::propagateLinkedValue(const V value)
{
	auto it = linkedControllers.lowerBound(value);
	auto end = linkedControllers.upperBound(value);

	while (it != end)
	{
//		qDebug() << "Propagate value" << value;
		QAbstractController<T, V> * controller = it.value();
		controller->setValue(value);
		++it;
	}
}


/*
 * Block signals on all connected widgets
 */
template <typename T, typename V>
void QAbstractController<T, V>::blockSignalsOnConnectedWidgets()
{
	for (int i = 0; i < sources.size(); ++i)
	{
		sourceLockStatus[i] = sources[i]->blockSignals(true);
	}
}

/*
 * Unlock signals on all connected widgets
 */
template <typename T, typename V>
void QAbstractController<T, V>::unblockSignalsOnConnectedWidgets()
{
	for (int i = 0; i < sources.size(); ++i)
	{
		sources[i]->blockSignals(sourceLockStatus[i]);
	}
}

/*
 * Checks if the argument controller is compatible with this controller
 * @param controller the controller to check
 * @return true if the argument controller is compatible with
 * this controller.
 * @note This implementation always return true but subclasses may
 * overload this method to introduce more compatibility controls
 */
template <typename T, typename V>
bool QAbstractController<T, V>::checkMirror(QAbstractController<T, V> * controller)
{
	Q_UNUSED(controller);
	return true;
}

// ----------------------------------------------------------------------------
// Template proto instantiations
// ----------------------------------------------------------------------------

template class QAbstractController<bool, bool>;
template class QAbstractController<int, int>;

#include <utils/RangeValue.h>

extern template class AbstractRangeValue<int>;
extern template class RangeValue<int>;
template class QAbstractController<RangeValue<int>, int>;

extern template class AbstractRangeValue<double>;
extern template class RangeValue<double>;
template class QAbstractController<RangeValue<double>, double>;
