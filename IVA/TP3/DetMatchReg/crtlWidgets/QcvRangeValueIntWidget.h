#ifndef QCVRANGEVALUEINTWIDGET_H
#define QCVRANGEVALUEINTWIDGET_H

#include <QWidget>

#include "QcvRangeValueControl.h"

namespace Ui {
	class QcvRangeValueIntWidget;
}

/**
 * UI class to control a RangeValue containing integer values
 * The range value is control either by
 *	- a spinner
 *	- and/or a slider
 *	- and can also be reset to its default value
 */
class QcvRangeValueIntWidget : public QWidget, public QcvRangeValueControlInt
{
	Q_OBJECT

	private:
		/**
		 * The actual UI made with the designer
		 */
		Ui::QcvRangeValueIntWidget *ui;

	public:
		/**
		 * Valued constructor
		 * @param rangeValue the range value to control
		 * @param title the title of the widget
		 * @param description values description to appear on the tooltip
		 * @param lock the mutex to check before setting new values
		 * @param options flags indicating the presence of the spinner, the slider
		 * and the reset button (all by default)
		 * @param parent the parent widget (if any)
		 * @throw exception if rangeValue is null
		 */
		explicit QcvRangeValueIntWidget(RangeValue<int> * rangeValue,
										const QString & title = QString(),
										const QString & description = QString(),
										QcvRangeValueControlInt::Options options = 0,
										QMutex * lock = nullptr,
										QWidget * parent = nullptr)
			throw(exception);

		/**
		 * Destructor
		 */
		virtual ~QcvRangeValueIntWidget();

	signals:
		/**
		 * Signal emitted when value changes
		 * @param value the new value
		 */
		void valueChanged(int value);

		/**
		 * Signal emitted to display a message somewhere
		 * @param text the message
		 */
		void message(const QString & text);

	private slots:
		/**
		 * Slot used when reset button is hit
		 */
		void on_resetButton_clicked();

		/**
		 * Slot used when spinbox value changes
		 * @param value the new value to set
		 * @post the closest value has been set in the #rangeValue and
		 * depending on the result a #valueChanged() signal is emitted
		 * and evt a #message(...) signal is emitted if setting value failed
		 */
		void on_spinBox_valueChanged(int value);
};

#endif // QCVRANGEVALUEINTWIDGET_H
