/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <Qcv/matWidgets/QcvMatWidget.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionCamera_0;
    QAction *actionCamera_1;
    QAction *actionFile;
    QAction *actionFlip;
    QAction *actionOriginalSize;
    QAction *actionConstrainedSize;
    QAction *actionQuit;
    QAction *actionRenderImage;
    QAction *actionRenderPixmap;
    QAction *actionRenderOpenGL;
    QAction *actionGray;
    QAction *actionCalibData;
    QAction *actionModel;
    QAction *actionRecord;
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QSplitter *splitter;
    QScrollArea *scrollAreaModel;
    QcvMatWidget *widgetImageModel;
    QScrollArea *scrollAreaScene;
    QcvMatWidget *widgetImageScene;
    QTabWidget *tabWidget;
    QWidget *captureTab;
    QVBoxLayout *verticalLayout;
    QRadioButton *radioButtonOrigSize;
    QRadioButton *radioButtonCustomSize;
    QLabel *labelWidth;
    QSpinBox *spinBoxWidth;
    QLabel *labelHeight;
    QSpinBox *spinBoxHeight;
    QCheckBox *checkBoxFlip;
    QCheckBox *checkBoxGray;
    QSpacerItem *verticalSpacer;
    QWidget *processTab;
    QGridLayout *gridLayout_4;
    QGroupBox *groupBoxMatcher;
    QGridLayout *gridLayout_2;
    QLabel *labelMatcherType;
    QLabel *labelMatcherMode;
    QLabel *labelKnn;
    QComboBox *comboBoxMatcherType;
    QComboBox *comboBoxMatcherMode;
    QLabel *labelRadius;
    QDoubleSpinBox *doubleSpinBoxRadius;
    QLabel *labelMached;
    QLabel *labelMatchedNumber;
    QGroupBox *groupBoxMatchDistances;
    QGridLayout *gridLayout_3;
    QLabel *labelDistMean;
    QLabel *labelDistMin;
    QLabel *labelDistMeanNumber;
    QLabel *labelDistMinNumber;
    QSpinBox *spinBoxKnn;
    QLabel *labelMatcherTime;
    QLabel *labelMatcherTimeMs;
    QGroupBox *groupBoxRegistrar;
    QGridLayout *gridLayout_5;
    QCheckBox *checkBoxShowFrame;
    QCheckBox *checkBoxUsePreviousPose;
    QLabel *labelInliersNumber;
    QDoubleSpinBox *doubleSpinBoxReprojThres;
    QLabel *labelReprojError;
    QLabel *labelMethod;
    QLabel *labelInliers;
    QDoubleSpinBox *doubleSpinBoxPrintSize;
    QCheckBox *checkBoxCameraSet;
    QSpinBox *spinBoxRansacIterations;
    QCheckBox *checkBoxUseRansacPNP;
    QLabel *labelPrintSize;
    QComboBox *comboBoxMethod;
    QLabel *labelReprojThres;
    QLabel *labelRegTime;
    QLabel *labelRegTimeMS;
    QCheckBox *checkBoxShowModelBox;
    QLabel *labelReprojErrorNumber;
    QLabel *labelShowKP;
    QComboBox *comboBoxShowKPts;
    QSpacerItem *verticalSpacer_2;
    QComboBox *comboBoxTimeUnit;
    QLabel *labelTimeUnit;
    QGroupBox *groupBoxDetector;
    QGridLayout *gridLayout;
    QLabel *labelDetectorTimeMs;
    QLabel *labelDescriptors;
    QLabel *labelModel;
    QComboBox *comboBoxFeatures;
    QComboBox *comboBoxDescriptors;
    QLabel *labelModelKPts;
    QLabel *labelScene;
    QLabel *labelSceneKPts;
    QLabel *labelDescriptorsTime;
    QLabel *labelFeatures;
    QLabel *labelDescriptorsTimeMs;
    QLabel *labelDetectorTime;
    QWidget *parameterTab;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *detectorParamBox;
    QVBoxLayout *detectorBoxVerticalLayout;
    QGroupBox *extractorParamBox;
    QVBoxLayout *exctractorBoxVerticalLayout;
    QSpacerItem *verticalSpacer_3;
    QMenuBar *menuBar;
    QMenu *menuSources;
    QMenu *menuVideo;
    QMenu *menuSize;
    QMenu *menuRender;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1006, 790);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        actionCamera_0 = new QAction(MainWindow);
        actionCamera_0->setObjectName(QStringLiteral("actionCamera_0"));
        actionCamera_1 = new QAction(MainWindow);
        actionCamera_1->setObjectName(QStringLiteral("actionCamera_1"));
        actionFile = new QAction(MainWindow);
        actionFile->setObjectName(QStringLiteral("actionFile"));
        actionFlip = new QAction(MainWindow);
        actionFlip->setObjectName(QStringLiteral("actionFlip"));
        actionFlip->setCheckable(true);
        actionOriginalSize = new QAction(MainWindow);
        actionOriginalSize->setObjectName(QStringLiteral("actionOriginalSize"));
        actionOriginalSize->setCheckable(true);
        actionConstrainedSize = new QAction(MainWindow);
        actionConstrainedSize->setObjectName(QStringLiteral("actionConstrainedSize"));
        actionConstrainedSize->setCheckable(true);
        actionQuit = new QAction(MainWindow);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        actionRenderImage = new QAction(MainWindow);
        actionRenderImage->setObjectName(QStringLiteral("actionRenderImage"));
        actionRenderImage->setCheckable(true);
        actionRenderImage->setChecked(true);
        actionRenderPixmap = new QAction(MainWindow);
        actionRenderPixmap->setObjectName(QStringLiteral("actionRenderPixmap"));
        actionRenderPixmap->setCheckable(true);
        actionRenderOpenGL = new QAction(MainWindow);
        actionRenderOpenGL->setObjectName(QStringLiteral("actionRenderOpenGL"));
        actionRenderOpenGL->setCheckable(true);
        actionGray = new QAction(MainWindow);
        actionGray->setObjectName(QStringLiteral("actionGray"));
        actionGray->setCheckable(true);
        actionCalibData = new QAction(MainWindow);
        actionCalibData->setObjectName(QStringLiteral("actionCalibData"));
        actionModel = new QAction(MainWindow);
        actionModel->setObjectName(QStringLiteral("actionModel"));
        actionRecord = new QAction(MainWindow);
        actionRecord->setObjectName(QStringLiteral("actionRecord"));
        actionRecord->setCheckable(true);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/icons/rec-16.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionRecord->setIcon(icon);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        scrollAreaModel = new QScrollArea(splitter);
        scrollAreaModel->setObjectName(QStringLiteral("scrollAreaModel"));
        scrollAreaModel->setWidgetResizable(true);
        scrollAreaModel->setAlignment(Qt::AlignCenter);
        widgetImageModel = new QcvMatWidget();
        widgetImageModel->setObjectName(QStringLiteral("widgetImageModel"));
        widgetImageModel->setGeometry(QRect(0, 0, 356, 721));
        scrollAreaModel->setWidget(widgetImageModel);
        splitter->addWidget(scrollAreaModel);
        scrollAreaScene = new QScrollArea(splitter);
        scrollAreaScene->setObjectName(QStringLiteral("scrollAreaScene"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(scrollAreaScene->sizePolicy().hasHeightForWidth());
        scrollAreaScene->setSizePolicy(sizePolicy1);
        scrollAreaScene->setWidgetResizable(true);
        scrollAreaScene->setAlignment(Qt::AlignCenter);
        widgetImageScene = new QcvMatWidget();
        widgetImageScene->setObjectName(QStringLiteral("widgetImageScene"));
        widgetImageScene->setGeometry(QRect(0, 0, 355, 721));
        scrollAreaScene->setWidget(widgetImageScene);
        splitter->addWidget(scrollAreaScene);

        horizontalLayout->addWidget(splitter);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy2);
        tabWidget->setMaximumSize(QSize(250, 16777215));
        QFont font;
        font.setBold(false);
        font.setItalic(false);
        font.setUnderline(false);
        font.setWeight(50);
        font.setStrikeOut(false);
        font.setKerning(false);
        tabWidget->setFont(font);
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Triangular);
        tabWidget->setUsesScrollButtons(false);
        captureTab = new QWidget();
        captureTab->setObjectName(QStringLiteral("captureTab"));
        verticalLayout = new QVBoxLayout(captureTab);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(8, 8, 8, 8);
        radioButtonOrigSize = new QRadioButton(captureTab);
        radioButtonOrigSize->setObjectName(QStringLiteral("radioButtonOrigSize"));
        radioButtonOrigSize->setChecked(true);

        verticalLayout->addWidget(radioButtonOrigSize);

        radioButtonCustomSize = new QRadioButton(captureTab);
        radioButtonCustomSize->setObjectName(QStringLiteral("radioButtonCustomSize"));

        verticalLayout->addWidget(radioButtonCustomSize);

        labelWidth = new QLabel(captureTab);
        labelWidth->setObjectName(QStringLiteral("labelWidth"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(labelWidth->sizePolicy().hasHeightForWidth());
        labelWidth->setSizePolicy(sizePolicy3);

        verticalLayout->addWidget(labelWidth);

        spinBoxWidth = new QSpinBox(captureTab);
        spinBoxWidth->setObjectName(QStringLiteral("spinBoxWidth"));
        spinBoxWidth->setMaximum(1600);
        spinBoxWidth->setSingleStep(4);

        verticalLayout->addWidget(spinBoxWidth);

        labelHeight = new QLabel(captureTab);
        labelHeight->setObjectName(QStringLiteral("labelHeight"));
        sizePolicy3.setHeightForWidth(labelHeight->sizePolicy().hasHeightForWidth());
        labelHeight->setSizePolicy(sizePolicy3);

        verticalLayout->addWidget(labelHeight);

        spinBoxHeight = new QSpinBox(captureTab);
        spinBoxHeight->setObjectName(QStringLiteral("spinBoxHeight"));
        spinBoxHeight->setMaximum(1200);
        spinBoxHeight->setSingleStep(4);

        verticalLayout->addWidget(spinBoxHeight);

        checkBoxFlip = new QCheckBox(captureTab);
        checkBoxFlip->setObjectName(QStringLiteral("checkBoxFlip"));

        verticalLayout->addWidget(checkBoxFlip);

        checkBoxGray = new QCheckBox(captureTab);
        checkBoxGray->setObjectName(QStringLiteral("checkBoxGray"));

        verticalLayout->addWidget(checkBoxGray);

        verticalSpacer = new QSpacerItem(20, 228, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        tabWidget->addTab(captureTab, QString());
        processTab = new QWidget();
        processTab->setObjectName(QStringLiteral("processTab"));
        gridLayout_4 = new QGridLayout(processTab);
        gridLayout_4->setSpacing(3);
        gridLayout_4->setContentsMargins(11, 11, 11, 11);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setContentsMargins(2, 2, 2, 2);
        groupBoxMatcher = new QGroupBox(processTab);
        groupBoxMatcher->setObjectName(QStringLiteral("groupBoxMatcher"));
        sizePolicy3.setHeightForWidth(groupBoxMatcher->sizePolicy().hasHeightForWidth());
        groupBoxMatcher->setSizePolicy(sizePolicy3);
        QFont font1;
        font1.setPointSize(10);
        groupBoxMatcher->setFont(font1);
        groupBoxMatcher->setFlat(false);
        groupBoxMatcher->setCheckable(true);
        groupBoxMatcher->setChecked(false);
        gridLayout_2 = new QGridLayout(groupBoxMatcher);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setHorizontalSpacing(2);
        gridLayout_2->setVerticalSpacing(0);
        gridLayout_2->setContentsMargins(2, 2, 2, 2);
        labelMatcherType = new QLabel(groupBoxMatcher);
        labelMatcherType->setObjectName(QStringLiteral("labelMatcherType"));
        sizePolicy3.setHeightForWidth(labelMatcherType->sizePolicy().hasHeightForWidth());
        labelMatcherType->setSizePolicy(sizePolicy3);
        labelMatcherType->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelMatcherType, 0, 0, 1, 1);

        labelMatcherMode = new QLabel(groupBoxMatcher);
        labelMatcherMode->setObjectName(QStringLiteral("labelMatcherMode"));
        sizePolicy3.setHeightForWidth(labelMatcherMode->sizePolicy().hasHeightForWidth());
        labelMatcherMode->setSizePolicy(sizePolicy3);
        labelMatcherMode->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelMatcherMode, 2, 0, 1, 1);

        labelKnn = new QLabel(groupBoxMatcher);
        labelKnn->setObjectName(QStringLiteral("labelKnn"));
        sizePolicy3.setHeightForWidth(labelKnn->sizePolicy().hasHeightForWidth());
        labelKnn->setSizePolicy(sizePolicy3);
        labelKnn->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelKnn, 4, 0, 1, 1);

        comboBoxMatcherType = new QComboBox(groupBoxMatcher);
        comboBoxMatcherType->addItem(QString());
        comboBoxMatcherType->addItem(QString());
        comboBoxMatcherType->addItem(QString());
        comboBoxMatcherType->addItem(QString());
        comboBoxMatcherType->addItem(QString());
        comboBoxMatcherType->setObjectName(QStringLiteral("comboBoxMatcherType"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(comboBoxMatcherType->sizePolicy().hasHeightForWidth());
        comboBoxMatcherType->setSizePolicy(sizePolicy4);

        gridLayout_2->addWidget(comboBoxMatcherType, 0, 1, 1, 1);

        comboBoxMatcherMode = new QComboBox(groupBoxMatcher);
        comboBoxMatcherMode->addItem(QString());
        comboBoxMatcherMode->addItem(QString());
        comboBoxMatcherMode->addItem(QString());
        comboBoxMatcherMode->setObjectName(QStringLiteral("comboBoxMatcherMode"));
        sizePolicy4.setHeightForWidth(comboBoxMatcherMode->sizePolicy().hasHeightForWidth());
        comboBoxMatcherMode->setSizePolicy(sizePolicy4);

        gridLayout_2->addWidget(comboBoxMatcherMode, 2, 1, 1, 1);

        labelRadius = new QLabel(groupBoxMatcher);
        labelRadius->setObjectName(QStringLiteral("labelRadius"));
        sizePolicy3.setHeightForWidth(labelRadius->sizePolicy().hasHeightForWidth());
        labelRadius->setSizePolicy(sizePolicy3);
        labelRadius->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelRadius, 5, 0, 1, 1);

        doubleSpinBoxRadius = new QDoubleSpinBox(groupBoxMatcher);
        doubleSpinBoxRadius->setObjectName(QStringLiteral("doubleSpinBoxRadius"));
        sizePolicy4.setHeightForWidth(doubleSpinBoxRadius->sizePolicy().hasHeightForWidth());
        doubleSpinBoxRadius->setSizePolicy(sizePolicy4);
        doubleSpinBoxRadius->setDecimals(2);
        doubleSpinBoxRadius->setMaximum(1);
        doubleSpinBoxRadius->setSingleStep(0.01);
        doubleSpinBoxRadius->setValue(0.1);

        gridLayout_2->addWidget(doubleSpinBoxRadius, 5, 1, 1, 1);

        labelMached = new QLabel(groupBoxMatcher);
        labelMached->setObjectName(QStringLiteral("labelMached"));
        sizePolicy3.setHeightForWidth(labelMached->sizePolicy().hasHeightForWidth());
        labelMached->setSizePolicy(sizePolicy3);
        labelMached->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelMached, 6, 0, 1, 1);

        labelMatchedNumber = new QLabel(groupBoxMatcher);
        labelMatchedNumber->setObjectName(QStringLiteral("labelMatchedNumber"));
        sizePolicy3.setHeightForWidth(labelMatchedNumber->sizePolicy().hasHeightForWidth());
        labelMatchedNumber->setSizePolicy(sizePolicy3);
        labelMatchedNumber->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelMatchedNumber, 6, 1, 1, 1);

        groupBoxMatchDistances = new QGroupBox(groupBoxMatcher);
        groupBoxMatchDistances->setObjectName(QStringLiteral("groupBoxMatchDistances"));
        sizePolicy3.setHeightForWidth(groupBoxMatchDistances->sizePolicy().hasHeightForWidth());
        groupBoxMatchDistances->setSizePolicy(sizePolicy3);
        gridLayout_3 = new QGridLayout(groupBoxMatchDistances);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setHorizontalSpacing(2);
        gridLayout_3->setVerticalSpacing(0);
        gridLayout_3->setContentsMargins(2, 2, 2, 2);
        labelDistMean = new QLabel(groupBoxMatchDistances);
        labelDistMean->setObjectName(QStringLiteral("labelDistMean"));
        sizePolicy3.setHeightForWidth(labelDistMean->sizePolicy().hasHeightForWidth());
        labelDistMean->setSizePolicy(sizePolicy3);
        labelDistMean->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelDistMean, 0, 0, 1, 1);

        labelDistMin = new QLabel(groupBoxMatchDistances);
        labelDistMin->setObjectName(QStringLiteral("labelDistMin"));
        sizePolicy3.setHeightForWidth(labelDistMin->sizePolicy().hasHeightForWidth());
        labelDistMin->setSizePolicy(sizePolicy3);
        labelDistMin->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_3->addWidget(labelDistMin, 1, 0, 1, 1);

        labelDistMeanNumber = new QLabel(groupBoxMatchDistances);
        labelDistMeanNumber->setObjectName(QStringLiteral("labelDistMeanNumber"));
        sizePolicy3.setHeightForWidth(labelDistMeanNumber->sizePolicy().hasHeightForWidth());
        labelDistMeanNumber->setSizePolicy(sizePolicy3);

        gridLayout_3->addWidget(labelDistMeanNumber, 0, 1, 1, 1);

        labelDistMinNumber = new QLabel(groupBoxMatchDistances);
        labelDistMinNumber->setObjectName(QStringLiteral("labelDistMinNumber"));
        sizePolicy3.setHeightForWidth(labelDistMinNumber->sizePolicy().hasHeightForWidth());
        labelDistMinNumber->setSizePolicy(sizePolicy3);

        gridLayout_3->addWidget(labelDistMinNumber, 1, 1, 1, 1);


        gridLayout_2->addWidget(groupBoxMatchDistances, 7, 0, 1, 2);

        spinBoxKnn = new QSpinBox(groupBoxMatcher);
        spinBoxKnn->setObjectName(QStringLiteral("spinBoxKnn"));
        sizePolicy4.setHeightForWidth(spinBoxKnn->sizePolicy().hasHeightForWidth());
        spinBoxKnn->setSizePolicy(sizePolicy4);
        spinBoxKnn->setMaximum(5);
        spinBoxKnn->setValue(1);

        gridLayout_2->addWidget(spinBoxKnn, 4, 1, 1, 1);

        labelMatcherTime = new QLabel(groupBoxMatcher);
        labelMatcherTime->setObjectName(QStringLiteral("labelMatcherTime"));
        sizePolicy3.setHeightForWidth(labelMatcherTime->sizePolicy().hasHeightForWidth());
        labelMatcherTime->setSizePolicy(sizePolicy3);
        labelMatcherTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelMatcherTime, 8, 0, 1, 1);

        labelMatcherTimeMs = new QLabel(groupBoxMatcher);
        labelMatcherTimeMs->setObjectName(QStringLiteral("labelMatcherTimeMs"));
        sizePolicy3.setHeightForWidth(labelMatcherTimeMs->sizePolicy().hasHeightForWidth());
        labelMatcherTimeMs->setSizePolicy(sizePolicy3);
        labelMatcherTimeMs->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(labelMatcherTimeMs, 8, 1, 1, 1);


        gridLayout_4->addWidget(groupBoxMatcher, 5, 0, 1, 2);

        groupBoxRegistrar = new QGroupBox(processTab);
        groupBoxRegistrar->setObjectName(QStringLiteral("groupBoxRegistrar"));
        sizePolicy3.setHeightForWidth(groupBoxRegistrar->sizePolicy().hasHeightForWidth());
        groupBoxRegistrar->setSizePolicy(sizePolicy3);
        groupBoxRegistrar->setFont(font1);
        groupBoxRegistrar->setCheckable(true);
        groupBoxRegistrar->setChecked(false);
        gridLayout_5 = new QGridLayout(groupBoxRegistrar);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(11, 11, 11, 11);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        gridLayout_5->setHorizontalSpacing(2);
        gridLayout_5->setVerticalSpacing(0);
        gridLayout_5->setContentsMargins(2, 2, 2, 2);
        checkBoxShowFrame = new QCheckBox(groupBoxRegistrar);
        checkBoxShowFrame->setObjectName(QStringLiteral("checkBoxShowFrame"));

        gridLayout_5->addWidget(checkBoxShowFrame, 6, 0, 1, 1);

        checkBoxUsePreviousPose = new QCheckBox(groupBoxRegistrar);
        checkBoxUsePreviousPose->setObjectName(QStringLiteral("checkBoxUsePreviousPose"));

        gridLayout_5->addWidget(checkBoxUsePreviousPose, 8, 0, 1, 1);

        labelInliersNumber = new QLabel(groupBoxRegistrar);
        labelInliersNumber->setObjectName(QStringLiteral("labelInliersNumber"));
        sizePolicy3.setHeightForWidth(labelInliersNumber->sizePolicy().hasHeightForWidth());
        labelInliersNumber->setSizePolicy(sizePolicy3);
        labelInliersNumber->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelInliersNumber, 4, 1, 1, 1);

        doubleSpinBoxReprojThres = new QDoubleSpinBox(groupBoxRegistrar);
        doubleSpinBoxReprojThres->setObjectName(QStringLiteral("doubleSpinBoxReprojThres"));
        doubleSpinBoxReprojThres->setDecimals(1);
        doubleSpinBoxReprojThres->setMaximum(20);
        doubleSpinBoxReprojThres->setSingleStep(0.1);
        doubleSpinBoxReprojThres->setValue(3);

        gridLayout_5->addWidget(doubleSpinBoxReprojThres, 1, 1, 1, 1);

        labelReprojError = new QLabel(groupBoxRegistrar);
        labelReprojError->setObjectName(QStringLiteral("labelReprojError"));
        sizePolicy3.setHeightForWidth(labelReprojError->sizePolicy().hasHeightForWidth());
        labelReprojError->setSizePolicy(sizePolicy3);
        labelReprojError->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelReprojError, 3, 0, 1, 1);

        labelMethod = new QLabel(groupBoxRegistrar);
        labelMethod->setObjectName(QStringLiteral("labelMethod"));
        sizePolicy3.setHeightForWidth(labelMethod->sizePolicy().hasHeightForWidth());
        labelMethod->setSizePolicy(sizePolicy3);

        gridLayout_5->addWidget(labelMethod, 2, 0, 1, 1, Qt::AlignRight);

        labelInliers = new QLabel(groupBoxRegistrar);
        labelInliers->setObjectName(QStringLiteral("labelInliers"));
        sizePolicy3.setHeightForWidth(labelInliers->sizePolicy().hasHeightForWidth());
        labelInliers->setSizePolicy(sizePolicy3);
        labelInliers->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelInliers, 4, 0, 1, 1);

        doubleSpinBoxPrintSize = new QDoubleSpinBox(groupBoxRegistrar);
        doubleSpinBoxPrintSize->setObjectName(QStringLiteral("doubleSpinBoxPrintSize"));
        doubleSpinBoxPrintSize->setDecimals(1);
        doubleSpinBoxPrintSize->setMinimum(10);
        doubleSpinBoxPrintSize->setMaximum(300);
        doubleSpinBoxPrintSize->setSingleStep(0.1);
        doubleSpinBoxPrintSize->setValue(178.1);

        gridLayout_5->addWidget(doubleSpinBoxPrintSize, 0, 1, 1, 1);

        checkBoxCameraSet = new QCheckBox(groupBoxRegistrar);
        checkBoxCameraSet->setObjectName(QStringLiteral("checkBoxCameraSet"));
        checkBoxCameraSet->setLayoutDirection(Qt::RightToLeft);
        checkBoxCameraSet->setCheckable(false);

        gridLayout_5->addWidget(checkBoxCameraSet, 5, 1, 1, 1);

        spinBoxRansacIterations = new QSpinBox(groupBoxRegistrar);
        spinBoxRansacIterations->setObjectName(QStringLiteral("spinBoxRansacIterations"));
        spinBoxRansacIterations->setMinimum(1);
        spinBoxRansacIterations->setMaximum(1000);
        spinBoxRansacIterations->setValue(100);

        gridLayout_5->addWidget(spinBoxRansacIterations, 9, 1, 1, 1);

        checkBoxUseRansacPNP = new QCheckBox(groupBoxRegistrar);
        checkBoxUseRansacPNP->setObjectName(QStringLiteral("checkBoxUseRansacPNP"));

        gridLayout_5->addWidget(checkBoxUseRansacPNP, 9, 0, 1, 1);

        labelPrintSize = new QLabel(groupBoxRegistrar);
        labelPrintSize->setObjectName(QStringLiteral("labelPrintSize"));
        sizePolicy3.setHeightForWidth(labelPrintSize->sizePolicy().hasHeightForWidth());
        labelPrintSize->setSizePolicy(sizePolicy3);
        labelPrintSize->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelPrintSize, 0, 0, 1, 1);

        comboBoxMethod = new QComboBox(groupBoxRegistrar);
        comboBoxMethod->addItem(QString());
        comboBoxMethod->addItem(QString());
        comboBoxMethod->addItem(QString());
        comboBoxMethod->addItem(QString());
        comboBoxMethod->addItem(QString());
        comboBoxMethod->setObjectName(QStringLiteral("comboBoxMethod"));
        sizePolicy4.setHeightForWidth(comboBoxMethod->sizePolicy().hasHeightForWidth());
        comboBoxMethod->setSizePolicy(sizePolicy4);

        gridLayout_5->addWidget(comboBoxMethod, 2, 1, 1, 1);

        labelReprojThres = new QLabel(groupBoxRegistrar);
        labelReprojThres->setObjectName(QStringLiteral("labelReprojThres"));
        sizePolicy3.setHeightForWidth(labelReprojThres->sizePolicy().hasHeightForWidth());
        labelReprojThres->setSizePolicy(sizePolicy3);
        labelReprojThres->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelReprojThres, 1, 0, 1, 1);

        labelRegTime = new QLabel(groupBoxRegistrar);
        labelRegTime->setObjectName(QStringLiteral("labelRegTime"));
        sizePolicy3.setHeightForWidth(labelRegTime->sizePolicy().hasHeightForWidth());
        labelRegTime->setSizePolicy(sizePolicy3);
        labelRegTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelRegTime, 11, 0, 1, 1);

        labelRegTimeMS = new QLabel(groupBoxRegistrar);
        labelRegTimeMS->setObjectName(QStringLiteral("labelRegTimeMS"));
        sizePolicy3.setHeightForWidth(labelRegTimeMS->sizePolicy().hasHeightForWidth());
        labelRegTimeMS->setSizePolicy(sizePolicy3);
        labelRegTimeMS->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelRegTimeMS, 11, 1, 1, 1);

        checkBoxShowModelBox = new QCheckBox(groupBoxRegistrar);
        checkBoxShowModelBox->setObjectName(QStringLiteral("checkBoxShowModelBox"));

        gridLayout_5->addWidget(checkBoxShowModelBox, 7, 0, 1, 1);

        labelReprojErrorNumber = new QLabel(groupBoxRegistrar);
        labelReprojErrorNumber->setObjectName(QStringLiteral("labelReprojErrorNumber"));
        sizePolicy3.setHeightForWidth(labelReprojErrorNumber->sizePolicy().hasHeightForWidth());
        labelReprojErrorNumber->setSizePolicy(sizePolicy3);
        labelReprojErrorNumber->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_5->addWidget(labelReprojErrorNumber, 3, 1, 1, 1);


        gridLayout_4->addWidget(groupBoxRegistrar, 6, 0, 1, 2);

        labelShowKP = new QLabel(processTab);
        labelShowKP->setObjectName(QStringLiteral("labelShowKP"));
        sizePolicy3.setHeightForWidth(labelShowKP->sizePolicy().hasHeightForWidth());
        labelShowKP->setSizePolicy(sizePolicy3);
        labelShowKP->setFont(font1);
        labelShowKP->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(labelShowKP, 2, 0, 1, 1);

        comboBoxShowKPts = new QComboBox(processTab);
        comboBoxShowKPts->addItem(QString());
        comboBoxShowKPts->addItem(QString());
        comboBoxShowKPts->addItem(QString());
        comboBoxShowKPts->addItem(QString());
        comboBoxShowKPts->setObjectName(QStringLiteral("comboBoxShowKPts"));
        sizePolicy4.setHeightForWidth(comboBoxShowKPts->sizePolicy().hasHeightForWidth());
        comboBoxShowKPts->setSizePolicy(sizePolicy4);
        comboBoxShowKPts->setFont(font1);

        gridLayout_4->addWidget(comboBoxShowKPts, 2, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer_2, 7, 0, 1, 2);

        comboBoxTimeUnit = new QComboBox(processTab);
        comboBoxTimeUnit->addItem(QString());
        comboBoxTimeUnit->addItem(QString());
        comboBoxTimeUnit->setObjectName(QStringLiteral("comboBoxTimeUnit"));
        sizePolicy4.setHeightForWidth(comboBoxTimeUnit->sizePolicy().hasHeightForWidth());
        comboBoxTimeUnit->setSizePolicy(sizePolicy4);
        comboBoxTimeUnit->setFont(font1);

        gridLayout_4->addWidget(comboBoxTimeUnit, 1, 1, 1, 1);

        labelTimeUnit = new QLabel(processTab);
        labelTimeUnit->setObjectName(QStringLiteral("labelTimeUnit"));
        sizePolicy3.setHeightForWidth(labelTimeUnit->sizePolicy().hasHeightForWidth());
        labelTimeUnit->setSizePolicy(sizePolicy3);
        labelTimeUnit->setFont(font1);
        labelTimeUnit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_4->addWidget(labelTimeUnit, 1, 0, 1, 1);

        groupBoxDetector = new QGroupBox(processTab);
        groupBoxDetector->setObjectName(QStringLiteral("groupBoxDetector"));
        sizePolicy3.setHeightForWidth(groupBoxDetector->sizePolicy().hasHeightForWidth());
        groupBoxDetector->setSizePolicy(sizePolicy3);
        groupBoxDetector->setFont(font1);
        groupBoxDetector->setCheckable(true);
        groupBoxDetector->setChecked(false);
        gridLayout = new QGridLayout(groupBoxDetector);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        gridLayout->setHorizontalSpacing(2);
        gridLayout->setVerticalSpacing(0);
        gridLayout->setContentsMargins(2, 2, 2, 2);
        labelDetectorTimeMs = new QLabel(groupBoxDetector);
        labelDetectorTimeMs->setObjectName(QStringLiteral("labelDetectorTimeMs"));
        sizePolicy3.setHeightForWidth(labelDetectorTimeMs->sizePolicy().hasHeightForWidth());
        labelDetectorTimeMs->setSizePolicy(sizePolicy3);
        labelDetectorTimeMs->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDetectorTimeMs, 10, 2, 1, 1);

        labelDescriptors = new QLabel(groupBoxDetector);
        labelDescriptors->setObjectName(QStringLiteral("labelDescriptors"));
        sizePolicy3.setHeightForWidth(labelDescriptors->sizePolicy().hasHeightForWidth());
        labelDescriptors->setSizePolicy(sizePolicy3);
        labelDescriptors->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDescriptors, 2, 0, 1, 2);

        labelModel = new QLabel(groupBoxDetector);
        labelModel->setObjectName(QStringLiteral("labelModel"));
        sizePolicy3.setHeightForWidth(labelModel->sizePolicy().hasHeightForWidth());
        labelModel->setSizePolicy(sizePolicy3);
        labelModel->setTextFormat(Qt::AutoText);
        labelModel->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelModel, 4, 0, 1, 2);

        comboBoxFeatures = new QComboBox(groupBoxDetector);
        comboBoxFeatures->addItem(QString());
        comboBoxFeatures->addItem(QString());
        comboBoxFeatures->addItem(QString());
        comboBoxFeatures->addItem(QString());
        comboBoxFeatures->addItem(QString());
        comboBoxFeatures->addItem(QString());
        comboBoxFeatures->addItem(QString());
        comboBoxFeatures->addItem(QString());
        comboBoxFeatures->addItem(QString());
        comboBoxFeatures->setObjectName(QStringLiteral("comboBoxFeatures"));
        sizePolicy4.setHeightForWidth(comboBoxFeatures->sizePolicy().hasHeightForWidth());
        comboBoxFeatures->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(comboBoxFeatures, 0, 2, 1, 1);

        comboBoxDescriptors = new QComboBox(groupBoxDetector);
        comboBoxDescriptors->addItem(QString());
        comboBoxDescriptors->addItem(QString());
        comboBoxDescriptors->addItem(QString());
        comboBoxDescriptors->addItem(QString());
        comboBoxDescriptors->addItem(QString());
        comboBoxDescriptors->addItem(QString());
        comboBoxDescriptors->setObjectName(QStringLiteral("comboBoxDescriptors"));
        sizePolicy4.setHeightForWidth(comboBoxDescriptors->sizePolicy().hasHeightForWidth());
        comboBoxDescriptors->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(comboBoxDescriptors, 2, 2, 1, 1);

        labelModelKPts = new QLabel(groupBoxDetector);
        labelModelKPts->setObjectName(QStringLiteral("labelModelKPts"));
        sizePolicy3.setHeightForWidth(labelModelKPts->sizePolicy().hasHeightForWidth());
        labelModelKPts->setSizePolicy(sizePolicy3);
        labelModelKPts->setTextFormat(Qt::AutoText);
        labelModelKPts->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelModelKPts, 4, 2, 1, 1);

        labelScene = new QLabel(groupBoxDetector);
        labelScene->setObjectName(QStringLiteral("labelScene"));
        sizePolicy3.setHeightForWidth(labelScene->sizePolicy().hasHeightForWidth());
        labelScene->setSizePolicy(sizePolicy3);
        labelScene->setTextFormat(Qt::AutoText);
        labelScene->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelScene, 6, 0, 1, 2);

        labelSceneKPts = new QLabel(groupBoxDetector);
        labelSceneKPts->setObjectName(QStringLiteral("labelSceneKPts"));
        sizePolicy3.setHeightForWidth(labelSceneKPts->sizePolicy().hasHeightForWidth());
        labelSceneKPts->setSizePolicy(sizePolicy3);
        labelSceneKPts->setTextFormat(Qt::AutoText);
        labelSceneKPts->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelSceneKPts, 6, 2, 1, 1);

        labelDescriptorsTime = new QLabel(groupBoxDetector);
        labelDescriptorsTime->setObjectName(QStringLiteral("labelDescriptorsTime"));
        sizePolicy3.setHeightForWidth(labelDescriptorsTime->sizePolicy().hasHeightForWidth());
        labelDescriptorsTime->setSizePolicy(sizePolicy3);
        labelDescriptorsTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDescriptorsTime, 13, 0, 1, 2);

        labelFeatures = new QLabel(groupBoxDetector);
        labelFeatures->setObjectName(QStringLiteral("labelFeatures"));
        sizePolicy3.setHeightForWidth(labelFeatures->sizePolicy().hasHeightForWidth());
        labelFeatures->setSizePolicy(sizePolicy3);
        labelFeatures->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelFeatures, 0, 0, 1, 2);

        labelDescriptorsTimeMs = new QLabel(groupBoxDetector);
        labelDescriptorsTimeMs->setObjectName(QStringLiteral("labelDescriptorsTimeMs"));
        sizePolicy3.setHeightForWidth(labelDescriptorsTimeMs->sizePolicy().hasHeightForWidth());
        labelDescriptorsTimeMs->setSizePolicy(sizePolicy3);
        labelDescriptorsTimeMs->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDescriptorsTimeMs, 13, 2, 1, 1);

        labelDetectorTime = new QLabel(groupBoxDetector);
        labelDetectorTime->setObjectName(QStringLiteral("labelDetectorTime"));
        sizePolicy3.setHeightForWidth(labelDetectorTime->sizePolicy().hasHeightForWidth());
        labelDetectorTime->setSizePolicy(sizePolicy3);
        labelDetectorTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(labelDetectorTime, 10, 0, 1, 2);


        gridLayout_4->addWidget(groupBoxDetector, 3, 0, 1, 2);

        tabWidget->addTab(processTab, QString());
        parameterTab = new QWidget();
        parameterTab->setObjectName(QStringLiteral("parameterTab"));
        verticalLayout_2 = new QVBoxLayout(parameterTab);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(2, 0, 2, 0);
        detectorParamBox = new QGroupBox(parameterTab);
        detectorParamBox->setObjectName(QStringLiteral("detectorParamBox"));
        QSizePolicy sizePolicy5(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(detectorParamBox->sizePolicy().hasHeightForWidth());
        detectorParamBox->setSizePolicy(sizePolicy5);
        detectorParamBox->setFont(font1);
        detectorBoxVerticalLayout = new QVBoxLayout(detectorParamBox);
        detectorBoxVerticalLayout->setSpacing(6);
        detectorBoxVerticalLayout->setContentsMargins(11, 11, 11, 11);
        detectorBoxVerticalLayout->setObjectName(QStringLiteral("detectorBoxVerticalLayout"));
        detectorBoxVerticalLayout->setContentsMargins(2, 2, 2, 2);

        verticalLayout_2->addWidget(detectorParamBox);

        extractorParamBox = new QGroupBox(parameterTab);
        extractorParamBox->setObjectName(QStringLiteral("extractorParamBox"));
        sizePolicy5.setHeightForWidth(extractorParamBox->sizePolicy().hasHeightForWidth());
        extractorParamBox->setSizePolicy(sizePolicy5);
        extractorParamBox->setFont(font1);
        exctractorBoxVerticalLayout = new QVBoxLayout(extractorParamBox);
        exctractorBoxVerticalLayout->setSpacing(6);
        exctractorBoxVerticalLayout->setContentsMargins(11, 11, 11, 11);
        exctractorBoxVerticalLayout->setObjectName(QStringLiteral("exctractorBoxVerticalLayout"));
        exctractorBoxVerticalLayout->setContentsMargins(2, 2, 2, 2);

        verticalLayout_2->addWidget(extractorParamBox);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);

        tabWidget->addTab(parameterTab, QString());

        horizontalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1006, 22));
        menuSources = new QMenu(menuBar);
        menuSources->setObjectName(QStringLiteral("menuSources"));
        menuVideo = new QMenu(menuBar);
        menuVideo->setObjectName(QStringLiteral("menuVideo"));
        menuSize = new QMenu(menuVideo);
        menuSize->setObjectName(QStringLiteral("menuSize"));
        menuRender = new QMenu(menuBar);
        menuRender->setObjectName(QStringLiteral("menuRender"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuSources->menuAction());
        menuBar->addAction(menuVideo->menuAction());
        menuBar->addAction(menuRender->menuAction());
        menuSources->addAction(actionCamera_0);
        menuSources->addAction(actionCamera_1);
        menuSources->addAction(actionFile);
        menuSources->addSeparator();
        menuSources->addAction(actionModel);
        menuSources->addAction(actionCalibData);
        menuSources->addSeparator();
        menuSources->addAction(actionQuit);
        menuVideo->addAction(actionFlip);
        menuVideo->addAction(actionGray);
        menuVideo->addSeparator();
        menuVideo->addAction(menuSize->menuAction());
        menuSize->addAction(actionOriginalSize);
        menuSize->addAction(actionConstrainedSize);
        menuRender->addAction(actionRenderImage);
        menuRender->addAction(actionRenderPixmap);
        menuRender->addAction(actionRenderOpenGL);

        retranslateUi(MainWindow);
        QObject::connect(radioButtonCustomSize, SIGNAL(clicked(bool)), actionConstrainedSize, SLOT(setChecked(bool)));
        QObject::connect(actionConstrainedSize, SIGNAL(triggered(bool)), radioButtonCustomSize, SLOT(setChecked(bool)));
        QObject::connect(radioButtonOrigSize, SIGNAL(clicked(bool)), actionOriginalSize, SLOT(setChecked(bool)));
        QObject::connect(actionOriginalSize, SIGNAL(triggered(bool)), radioButtonOrigSize, SLOT(setChecked(bool)));
        QObject::connect(checkBoxFlip, SIGNAL(clicked(bool)), actionFlip, SLOT(setChecked(bool)));
        QObject::connect(actionFlip, SIGNAL(triggered(bool)), checkBoxFlip, SLOT(setChecked(bool)));
        QObject::connect(checkBoxGray, SIGNAL(clicked(bool)), actionGray, SLOT(setChecked(bool)));
        QObject::connect(actionGray, SIGNAL(triggered(bool)), checkBoxGray, SLOT(setChecked(bool)));

        tabWidget->setCurrentIndex(1);
        comboBoxMatcherType->setCurrentIndex(0);
        comboBoxMatcherMode->setCurrentIndex(0);
        comboBoxShowKPts->setCurrentIndex(0);
        comboBoxFeatures->setCurrentIndex(3);
        comboBoxDescriptors->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Detection / Matching / Registration", nullptr));
        actionCamera_0->setText(QApplication::translate("MainWindow", "Camera 0", nullptr));
#ifndef QT_NO_TOOLTIP
        actionCamera_0->setToolTip(QApplication::translate("MainWindow", "cam\303\251ra interne", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionCamera_0->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+0", nullptr));
#endif // QT_NO_SHORTCUT
        actionCamera_1->setText(QApplication::translate("MainWindow", "Camera 1", nullptr));
#ifndef QT_NO_TOOLTIP
        actionCamera_1->setToolTip(QApplication::translate("MainWindow", "cam\303\251ra externe", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionCamera_1->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+1", nullptr));
#endif // QT_NO_SHORTCUT
        actionFile->setText(QApplication::translate("MainWindow", "File", nullptr));
#ifndef QT_NO_TOOLTIP
        actionFile->setToolTip(QApplication::translate("MainWindow", "fichier video", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionFile->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_NO_SHORTCUT
        actionFlip->setText(QApplication::translate("MainWindow", "flip", nullptr));
        actionOriginalSize->setText(QApplication::translate("MainWindow", "Originale", nullptr));
#ifndef QT_NO_TOOLTIP
        actionOriginalSize->setToolTip(QApplication::translate("MainWindow", "taille originale", nullptr));
#endif // QT_NO_TOOLTIP
        actionConstrainedSize->setText(QApplication::translate("MainWindow", "Custom", nullptr));
        actionConstrainedSize->setIconText(QApplication::translate("MainWindow", "contrainte", nullptr));
#ifndef QT_NO_TOOLTIP
        actionConstrainedSize->setToolTip(QApplication::translate("MainWindow", "taille impos\303\251e", nullptr));
#endif // QT_NO_TOOLTIP
        actionQuit->setText(QApplication::translate("MainWindow", "Quitter", nullptr));
        actionRenderImage->setText(QApplication::translate("MainWindow", "Image", nullptr));
        actionRenderPixmap->setText(QApplication::translate("MainWindow", "Pixmap", nullptr));
        actionRenderOpenGL->setText(QApplication::translate("MainWindow", "OpenGL", nullptr));
        actionGray->setText(QApplication::translate("MainWindow", "gray", nullptr));
        actionCalibData->setText(QApplication::translate("MainWindow", "Camera parameters", nullptr));
        actionModel->setText(QApplication::translate("MainWindow", "Model image", nullptr));
        actionRecord->setText(QApplication::translate("MainWindow", "Record", nullptr));
#ifndef QT_NO_TOOLTIP
        actionRecord->setToolTip(QApplication::translate("MainWindow", "Record to file", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionRecord->setShortcut(QApplication::translate("MainWindow", "Ctrl+R", nullptr));
#endif // QT_NO_SHORTCUT
        radioButtonOrigSize->setText(QApplication::translate("MainWindow", "Originale", nullptr));
        radioButtonCustomSize->setText(QApplication::translate("MainWindow", "Custom", nullptr));
        labelWidth->setText(QApplication::translate("MainWindow", "Largeur", nullptr));
        labelHeight->setText(QApplication::translate("MainWindow", "Hauteur", nullptr));
        checkBoxFlip->setText(QApplication::translate("MainWindow", "Flip", nullptr));
        checkBoxGray->setText(QApplication::translate("MainWindow", "Gray", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(captureTab), QApplication::translate("MainWindow", "Size", nullptr));
        groupBoxMatcher->setTitle(QApplication::translate("MainWindow", "Matcher", nullptr));
        labelMatcherType->setText(QApplication::translate("MainWindow", "Type", nullptr));
        labelMatcherMode->setText(QApplication::translate("MainWindow", "Match Mode", nullptr));
        labelKnn->setText(QApplication::translate("MainWindow", "Knn", nullptr));
        comboBoxMatcherType->setItemText(0, QApplication::translate("MainWindow", "Brute Force", nullptr));
        comboBoxMatcherType->setItemText(1, QApplication::translate("MainWindow", "Brute Force L1", nullptr));
        comboBoxMatcherType->setItemText(2, QApplication::translate("MainWindow", "Brute Force Hamming", nullptr));
        comboBoxMatcherType->setItemText(3, QApplication::translate("MainWindow", "Brute Force Hamming (2)", nullptr));
        comboBoxMatcherType->setItemText(4, QApplication::translate("MainWindow", "Flann Based", nullptr));

        comboBoxMatcherMode->setItemText(0, QApplication::translate("MainWindow", "Simple", nullptr));
        comboBoxMatcherMode->setItemText(1, QApplication::translate("MainWindow", "Cross Best Knn", nullptr));
        comboBoxMatcherMode->setItemText(2, QApplication::translate("MainWindow", "Cross Radius", nullptr));

        labelRadius->setText(QApplication::translate("MainWindow", "Radius", nullptr));
        labelMached->setText(QApplication::translate("MainWindow", "Matched:", nullptr));
        labelMatchedNumber->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBoxMatchDistances->setTitle(QApplication::translate("MainWindow", "Match distances", nullptr));
        labelDistMean->setText(QApplication::translate("MainWindow", "Mean", nullptr));
        labelDistMin->setText(QApplication::translate("MainWindow", "Min/Max", nullptr));
        labelDistMeanNumber->setText(QApplication::translate("MainWindow", "0.00", nullptr));
        labelDistMinNumber->setText(QApplication::translate("MainWindow", "0.00", nullptr));
        labelMatcherTime->setText(QApplication::translate("MainWindow", "Matching time :", nullptr));
        labelMatcherTimeMs->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBoxRegistrar->setTitle(QApplication::translate("MainWindow", "Registrar", nullptr));
        checkBoxShowFrame->setText(QApplication::translate("MainWindow", "Model Frame", nullptr));
        checkBoxUsePreviousPose->setText(QApplication::translate("MainWindow", "Use Prev. Pose", nullptr));
        labelInliersNumber->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelReprojError->setText(QApplication::translate("MainWindow", "Reproj. Error:", nullptr));
        labelMethod->setText(QApplication::translate("MainWindow", "Method", nullptr));
        labelInliers->setText(QApplication::translate("MainWindow", "Inliers:", nullptr));
        checkBoxCameraSet->setText(QApplication::translate("MainWindow", "Camera param. set", nullptr));
#ifndef QT_NO_TOOLTIP
        spinBoxRansacIterations->setToolTip(QApplication::translate("MainWindow", "Ransac iterations", nullptr));
#endif // QT_NO_TOOLTIP
        checkBoxUseRansacPNP->setText(QApplication::translate("MainWindow", "Use Ransac PNP", nullptr));
        labelPrintSize->setText(QApplication::translate("MainWindow", "Print Size", nullptr));
        comboBoxMethod->setItemText(0, QApplication::translate("MainWindow", "Iterative", nullptr));
        comboBoxMethod->setItemText(1, QApplication::translate("MainWindow", "E-PnP", nullptr));
        comboBoxMethod->setItemText(2, QApplication::translate("MainWindow", "P3P", nullptr));
        comboBoxMethod->setItemText(3, QApplication::translate("MainWindow", "DLS", nullptr));
        comboBoxMethod->setItemText(4, QApplication::translate("MainWindow", "U-PnP", nullptr));

        labelReprojThres->setText(QApplication::translate("MainWindow", "Reproj. Thres.", nullptr));
        labelRegTime->setText(QApplication::translate("MainWindow", "Registration time:", nullptr));
        labelRegTimeMS->setText(QApplication::translate("MainWindow", "0", nullptr));
        checkBoxShowModelBox->setText(QApplication::translate("MainWindow", "Compute Pose", nullptr));
        labelReprojErrorNumber->setText(QApplication::translate("MainWindow", "0.00", nullptr));
        labelShowKP->setText(QApplication::translate("MainWindow", "Show Keypoints", nullptr));
        comboBoxShowKPts->setItemText(0, QApplication::translate("MainWindow", "None", nullptr));
        comboBoxShowKPts->setItemText(1, QApplication::translate("MainWindow", "All", nullptr));
        comboBoxShowKPts->setItemText(2, QApplication::translate("MainWindow", "Matched", nullptr));
        comboBoxShowKPts->setItemText(3, QApplication::translate("MainWindow", "Inliers", nullptr));

        comboBoxTimeUnit->setItemText(0, QApplication::translate("MainWindow", "\316\274s", nullptr));
        comboBoxTimeUnit->setItemText(1, QApplication::translate("MainWindow", "\316\274s/pt", nullptr));

        labelTimeUnit->setText(QApplication::translate("MainWindow", "Time unit", nullptr));
        groupBoxDetector->setTitle(QApplication::translate("MainWindow", "Detector", nullptr));
        labelDetectorTimeMs->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelDescriptors->setText(QApplication::translate("MainWindow", "Descriptors", nullptr));
        labelModel->setText(QApplication::translate("MainWindow", "Model:", nullptr));
        comboBoxFeatures->setItemText(0, QApplication::translate("MainWindow", "FAST", nullptr));
        comboBoxFeatures->setItemText(1, QApplication::translate("MainWindow", "STAR", nullptr));
        comboBoxFeatures->setItemText(2, QApplication::translate("MainWindow", "SIFT", nullptr));
        comboBoxFeatures->setItemText(3, QApplication::translate("MainWindow", "SURF", nullptr));
        comboBoxFeatures->setItemText(4, QApplication::translate("MainWindow", "ORB", nullptr));
        comboBoxFeatures->setItemText(5, QApplication::translate("MainWindow", "BRISK", nullptr));
        comboBoxFeatures->setItemText(6, QApplication::translate("MainWindow", "MSER", nullptr));
        comboBoxFeatures->setItemText(7, QApplication::translate("MainWindow", "GFTT", nullptr));
        comboBoxFeatures->setItemText(8, QApplication::translate("MainWindow", "HARRIS", nullptr));

        comboBoxDescriptors->setItemText(0, QApplication::translate("MainWindow", "SIFT", nullptr));
        comboBoxDescriptors->setItemText(1, QApplication::translate("MainWindow", "SURF", nullptr));
        comboBoxDescriptors->setItemText(2, QApplication::translate("MainWindow", "ORB", nullptr));
        comboBoxDescriptors->setItemText(3, QApplication::translate("MainWindow", "BRISK", nullptr));
        comboBoxDescriptors->setItemText(4, QApplication::translate("MainWindow", "BRIEF", nullptr));
        comboBoxDescriptors->setItemText(5, QApplication::translate("MainWindow", "FREAK", nullptr));

        labelModelKPts->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelScene->setText(QApplication::translate("MainWindow", "Scene:", nullptr));
        labelSceneKPts->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelDescriptorsTime->setText(QApplication::translate("MainWindow", "Descriptors time :", nullptr));
        labelFeatures->setText(QApplication::translate("MainWindow", "Features", nullptr));
        labelDescriptorsTimeMs->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelDetectorTime->setText(QApplication::translate("MainWindow", "Detection time :", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(processTab), QApplication::translate("MainWindow", "Process", nullptr));
        detectorParamBox->setTitle(QApplication::translate("MainWindow", "Detector parameters", nullptr));
        extractorParamBox->setTitle(QApplication::translate("MainWindow", "Extractor Parameters", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(parameterTab), QApplication::translate("MainWindow", "Parameters", nullptr));
        menuSources->setTitle(QApplication::translate("MainWindow", "Sources", nullptr));
        menuVideo->setTitle(QApplication::translate("MainWindow", "Video", nullptr));
        menuSize->setTitle(QApplication::translate("MainWindow", "taille", nullptr));
        menuRender->setTitle(QApplication::translate("MainWindow", "Render", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
