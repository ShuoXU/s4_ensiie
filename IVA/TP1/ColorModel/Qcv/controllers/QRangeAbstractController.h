/*
 * RangeController.h
 *
 *  Created on: 14 juin 2015
 *      Author: davidroussel
 */
#ifndef QRANGEABSTRACTCONTROLLER_H
#define QRANGEABSTRACTCONTROLLER_H

#include <functional>	// for binary predicates constraints
using namespace std;

#include <QMutex>
#include <QSet>
#include <QWidget>
#include <QAbstractSpinBox>
#include <QAbstractSlider>
#include <QAbstractButton>
#include <QLabel>

#include <Qcv/controllers/QAbstractController.h>
#include <utils/RangeValue.h>

/**
 * Abstract Controller template for a RangeValue<T> allowing subclasses to
 * setup and connect to a QAbstractSpinBox and/or a QAbstractSlider
 */
template <typename T>
class QRangeAbstractController : public QAbstractController<RangeValue<T>, T>
{
	protected:
		/*
		 * Reminder of QAbstractController members used here
		 * This allow to use these members without having to add
		 * QAbstractController<...>:: prefix.
		 */
		using QAbstractController<RangeValue<T>, T>::managedValue;
		using QAbstractController<RangeValue<T>, T>::lock;
		using QAbstractController<RangeValue<T>, T>::sources;
		using QAbstractController<RangeValue<T>, T>::sourceLockStatus;
		using QAbstractController<RangeValue<T>, T>::constraintControllers;
		using QAbstractController<RangeValue<T>, T>::constraints;
		using QAbstractController<RangeValue<T>, T>::callback;
		using QAbstractController<RangeValue<T>, T>::settings;
		using QAbstractController<RangeValue<T>, T>::settingsName;
		using QAbstractController<RangeValue<T>, T>::checkConstraints;
		using QAbstractController<RangeValue<T>, T>::blockSignalsOnConnectedWidgets;
		using QAbstractController<RangeValue<T>, T>::unblockSignalsOnConnectedWidgets;
		using QAbstractController<RangeValue<T>, T>::emitValueChanged;
		using QAbstractController<RangeValue<T>, T>::emitUpdated;

		/**
		 * factor between the outside values and the values in the RangeValue
		 * such that outsideValue = RangeValue * factor;
		 */
		const T factor;

	public:
		/**
		 * Constructor from RangeValue, mutex and factor
		 * @param range the RangeValue<T> to reference
		 * @param lock the lock (from the processor containing the RangeValue) to
		 * use when modifying the RangeValue
		 * @param factor the factor to apply to the range values when emitting or
		 * the factor to divide outside values when setting a new range value.
		 * @param callback the call back to call for setting the value instead
		 * of settting the value directly
		 * @param settings the settings to use to set initial value and/or
		 * save the current value (if different from default value) to settings
		 * @param settingsName the name to use when reading or writing the
		 * parameter value to settings (if any)
		 */
		explicit QRangeAbstractController(RangeValue<T> * const range,
										  QMutex * const lock = nullptr,
										  const T & factor = T(1),
										  function<void(const T)> * callback = nullptr,
										  QSettings * settings = nullptr,
										  const QString & settingsName = QString());

		/**
		 * Copy Constructor
		 * @param rc the range controller to copy
		 */
		QRangeAbstractController(const QRangeAbstractController<T> & rc);

		/**
		 * Move Constructor
		 * @param rc the range controller to move
		 */
		QRangeAbstractController(QRangeAbstractController<T> && rc);

		/**
		 * Destructor
		 */
		virtual ~QRangeAbstractController();

		/**
		 * Setup a source to reflect the managed value.
		 * Since we do not know yet what to do with the source, this method only
		 * checks if the source is already part of the connected sources or not.
		 * @param source the source to setup
		 * @return true if the source was correclty set up, false otherwise
		 * (for example when the widget was already part of connected widgets).
		 * @note subclasses overloads might actually configure this source
		 * properly and also setup connections from/to this source.
		 */
		virtual bool setupSource(QObject * source);

		/**
		 * Setup a spin box with the values in the RangeValue and connect
		 * the necessary signals and slots
		 * @param spinBox the spinbox to setup with
		 * @return true if the spinBox (whatever QSpinBox or QDoubleSpinBox)
		 * has been setup correctly.
		 * @post If this spinbox was not part of the widgets set, then the
		 * spinBox has been setup with RangeValue values and bidirectionnal
		 * connections between this and the spinBox have been created.
		 */
		virtual bool setupSpinBox(QAbstractSpinBox * spinBox);

		/**
		 * Setup a slider with the values in the RangeValue (and connects the
		 * necessary signals and slots).
		 * @param slider the slider to setup according to RangeValue values
		 * @return true if the slider has been setup correctly, false otherwise
		 * @post If this slider was not part of the widgets set, then the
		 * slider has been setup with RangeValue steps and bidirectionnal
		 * connections between this and the slider have been created.
		 */
		virtual bool setupSlider(QAbstractSlider * slider);

		/**
		 * Adds a reset button to this range controller in order to reset to the
		 * default value
		 * @param button the button to connect to the future reset slot
		 * @return true if this button was not already part of connected widgets
		 * and is not null
		 */
		virtual bool addResetButton(QAbstractButton * button) = 0;

		/**
		 * Sets the label contents to the desired value and connects the
		 * corresponding xxxChanged(T) signal to the setNum slot of a QLabel
		 * @param label the label to show the minimum value
		 * @param which the kind of value to be shown in the label
		 * @return true if the label pointer was not null
		 */
		virtual bool addNumberLabel(QLabel * label,
									typename RangeValue<T>::WhichValue which) = 0;
	// ------------------------------------------------------------------------
	// Future slots
	// ------------------------------------------------------------------------
	public :
		/**
		 * Sets new value in the range value.
		 * @param value the "outside" value to set (the "oustside" value differs
		 * from the "inside" value (stored in #managedValue) by #factor:
		 * "outside value" = "inside value" * factor
		 * @return true if the value has been set without correction, false
		 * otherwise. In any case a value is set according the rules of
		 * RangeValue<T>::setClosestValue
		 * @note slot typically used by QSpinBox when its value changes
		 * @note Please note that if result is false a signal needs to be sent
		 * to the connected UI to ajust the value since it may have been
		 * slighltly modified.
		 * @post The following signals have been emitted (by subclasses):
		 *	- valueChanged(T)
		 *	- indexChanged(size_t)
		 *	- updated()
		 * @see RangeValue<T>::setClosestValue(const T &)
		 */
		virtual bool setValue(const T value);

		/**
		 * Sets a new value corresponding to the index
		 * @param index the index to set the new value to
		 * @return true in index is valid (the corresponding value is less or
		 * equal to the max value
		 * @post The following signals have been emitted:
		 *	- valueChanged(T)
		 *	- indexChanged(size_t) if the resulting index is different from the
		 *	one requested
		 *	- updated()
		 */
		virtual bool setIndex(const int index);

		/**
		 * Set new min value in the range
		 * @param value the new min value in the range
		 * @return true if the new min value has been set without correction,
		 * false otherwise. The underlying RangeValue<T> enforce min value
		 * consistency
		 * @post The following signals have been emitted (by subclasses):
		 *	- minChanged(T)
		 *	- rangeChanged(T min, T max)
		 *	- indexRangeChanged(0, nbSteps)
		 *	- stepChanged(T) if the step value is modified by setting new min
		 * @see RangeValue<T>::setMin(const T &)
		 */
		virtual bool setMin(const T & value);

		/**
		 * Set new max value in the range
		 * @param value the new max value in the range
		 * @return true if the new max value has been set without correction,
		 * false otherwise. The underlying RangeValue<T> enforce max value
		 * consistency
		 * @post The following signals have been emitted (by subclasses):
		 *	- maxChanged(T)
		 *	- rangeChanged(T min, T max)
		 *	- indexRangeChanged(0, nbSteps)
		 *	- stepChanged(T) if the step value is modified by setting new max
		 * @see RangeValue<T>::setMax(const T &)
		 */
		virtual bool setMax(const T & value);

		/**
		 * Set new step value in the range
		 * @param value the new step value in the range
		 * @return true if the new step value has been set without correction,
		 * false otherwise. The underlying RangeValue<T> enforce step value
		 * consistency
		 * @post The following signals have been emitted (by subclasses):
		 *	- stepChanged(T)
		 *	- indexRangeChanged(0, nbsteps)
		 *	- minChanged(T) if setting a new step value modifies the min value
		 *	- maxChanged(T) if setting a new step value modifies the max value
		 * @see RangeValue<T>::setStep(const T &)
		 */
		virtual bool setStep(const T & value);

		/**
		 * Reset managed value to its default value
		 * @return true if the value has been reset to its default value exactly,
		 * false if the reset has triggered bounds or step modifications
		 */
		virtual bool reset();

		/**
		 * Refresh all connected UI.
		 * When the internal managedValue has been modified outside of this
		 * controller, all connected UIs should be refreshed
		 * @note This method should be implemented as a slot in sub-classes.
		 * @note refreshing Q[Double]SpinBoxes min & max value should also be
		 * performed in sub-classes since they might be either QSpinBox or
		 * QDoubleSpinBox.
		 * @post The following signals have been emitted (by subclasses):
		 *	- minChanged(T)
		 *	- maxChanged(T)
		 *	- rangeChanged(T min, T max)
		 *	- stepChanged(T)
		 *	- valueChanged(T)
		 *	- indexRangeChanged(0, nbSteps)
		 *	- indexChanged(size_t)
		 */
		virtual void refresh();

	protected:

		/**
		 * Value corresponding to an index in the range value.
		 * If there is a callback to set the value, we must imperatively use it
		 * to set values, so we can't use the managed value setIndex directly,
		 * and therefore we have to compute the resulting value of an index
		 * in the the range by ourselves.
		 * @param index the index in the range value
		 * @return the value corresponding to the index in the range value
		 */
		T indexValue(const int index);

		// --------------------------------------------------------------------
		// Methods to encapsulate future signals emission
		// --------------------------------------------------------------------
		/**
		 * Checks if the argument controller is compatible with this controller:
		 * By checking they both have the same range
		 * @param controller the controller to check
		 * @return true if the argument controller is compatible with
		 * this controller.
		 * @note This implementation always return true but subclasses may
		 * overload this method to introduce more compatibility controls
		 */
		bool checkMirror(QAbstractController<RangeValue<T>, T> * controller);

		/**
		 * Sends a signal when value has been set in the RangeValue which
		 * changes the index of the current value
		 * @param index the index of the new value
		 * @note typically used by QSlider
		 */
		virtual void emitIndexChanged(const int index) = 0;

		/**
		 * Sends a signal when min value in the range changes
		 * @param value the new min value in the range
		 */
		virtual void emitMinChanged(const T & value) = 0;

		/**
		 * Sends a signal when max value in the range changes
		 * @param value the new max value in the range
		 */
		virtual void emitMaxChanged(const T & value) = 0;

		/**
		 * Sends a signal when min or max value changes
		 * @param min the minimum value of the range
		 * @param max the maximum value of the range
		 */
		virtual void emitRangeChanged(const T & min, const T & max) = 0;

		/**
		 * Sends a signal when step value in the range changes
		 * @param value the new step value in the range
		 */
		virtual void emitStepChanged(const T & value) = 0;

		/**
		 * Sends a signal when min, max or step changes (because it might affect
		 * the indices of a slider for instance)
		 * @param min the minimum index (always 0)
		 * @param max the maximum index (the number of steps of the range value)
		 */
		virtual void emitIndexRangeChanged(const int & minIndex,
										   const int & maxIndex) = 0;
};

#endif // QRANGEABSTRACTCONTROLLER_H
