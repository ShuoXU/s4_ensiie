/*
 * CvColorSpaces.h
 *
 *  Created on: 25 févr. 2012
 *      Author: davidroussel
 */

#ifndef CVCOLORSPACES_H_
#define CVCOLORSPACES_H_

#include <vector>
using namespace std;

#include <utils/RangeValue.h>

#include <CvProcessor.h>
#include "Palette.h"

/**
 * Class to process source image into several color spaces such as RGB, HSV and
 * YCbCr
 */
class CvColorSpaces : public virtual CvProcessor
{
	public:
		/**
		 * Image type selected for display
		 * @note use integral(aDisplay) to convert to integer
		 */
		enum struct Display : int
		{
			INPUT = 0,	//!< color input image is selected for display
			GRAY,		//!< gray input image is selected for display
			RED,		//!< red component from BGR is selected for display
			GREEN,		//!< green component from BGR is selected for display
			BLUE,		//!< blue component from BGR is selected for display
			MAX_BGR,	//!< Maximum of R, G and B components
			XYZ_X,		//!< X component of XYZ space
			XYZ_Y,		//!< Y component of XYZ space
			XYZ_Z,		//!< Z component of XYZ space
			HUE,		//!< Hue component from HSV is selected for display
			SATURATION,	//!< Saturation component from HSV is selected for display
			VALUE,		//!< Lightness component from HSV is selected for display
			Y,			//!< Lightness component from YCrCb is selected for display
			Cr,			//!< Green/Magenta Cr component from YCrCb is selected for display
			Cb,			//!< Yellow/Blue Cb component from YCrCb is selected for display
			NBDISPLAY	//!< Number of display images
		};

		/**
		 * Hue image display mode
		 */
		enum struct HueDisplay : int
		{
			HUECOLOR = 0,	//!< Normal Hue mode
			HUESATURATE,	//!< Hue*Saturation mode
			HUEVALUE,		//!< Hue*Value mode
			HUEGRAY,		//!< Gray mode
			NBHUES			//!< Number of Hue display modes
		};

		/**
		 * Number of components in various color spaces
		 */
		static const size_t componentsSize = 3;

	protected :
		/**
		 * Indicates if individual channels should be displayed in color
		 * (when possible) or in gray
		 */
		bool showColoredChannels;

		/**
		 * Image displayed
		 */
		Mat displayImage;

		/**
		 * Gray converted image
		 */
		Mat inFrameGray;

		/**
		 * BGR individual channels
		 */
		vector<Mat> bgrChannels;

		/**
		 * Indices for RGB and Max in #bgrChannels & #bgrColoredChannels
		 * @note use integral(RGBIndex::Red or Blue or Green) to convert ot integer
		 */
		enum struct RGBIndex : int
		{
			Blue = 0,	//!< index for blue
			Green,		//!< index for green
			Red,		//!< index for blue
		};

		/**
		 * BGR colored images built from individual channels and palettes
		 */
		Mat bgrColoredChannels[componentsSize];

		/**
		 * Maximum of B & G channels
		 */
		Mat maxBGChannels;

		/**
		 * Maximum of maxBGChannels and R channel
		 */
		Mat maxBGRChannels;

		/**
		 * Colored maximum of B & G channels
		 */
		Mat maxBGChannelsColor;

		/**
		 * Colored Maximum of maxBGChannels and R channel
		 */
		Mat maxBGRChannelsColor;

		/**
		 * XYZ floating point converted image
		 */
		Mat inFrameXYZ;

		/**
		 * XYZ floating point channels
		 */
		Mat xyzGrayChannels[componentsSize];

		/**
		 * XYZ channels normalized to 0..255
		 */
		Mat xyzDisplayChannels[componentsSize];

		/**
		 * HSV converted image
		 */
		Mat inFrameHSV;

		/**
		 * HSV individual channels
		 */
		vector<Mat> hsvChannels;

		/**
		 * Hue colored image built from hue component and hsv palette
		 */
		Mat hueColorImage;

		/**
		 * Hue Mix channels to build hue colored display image
		 */
		Mat hueMixChannels[componentsSize];

		/**
		 * Hue image built from hueMixChannels
		 */
		Mat hueMixImage;

		/**
		 * Hue colored mixed image normalized from hueMixImage
		 */
		Mat hueMixedColorImage;

		/**
		 * Mix mode to create hue colored image
		 * @note use integral(hueDisplay) to convert into integer
		 */
		HueDisplay hueDisplay;

		/**
		 * YCbCr converted image
		 */
		Mat inFrameYCrCb;

		/**
		 * YCbCr channels
		 */
		vector<Mat> yCrCbChannels;

		/**
		 * Cr colored image
		 */
		Mat crColoredImage;

		/**
		 * Cb colored image
		 */
		Mat cbColoredImage;

		/**
		 * Palette to build colored red component image
		 */
		Palette redMap;

		/**
		 * Palette to build colored green component image
		 */
		Palette greenMap;

		/**
		 * Palette to build colored blue component image
		 */
		Palette blueMap;

		/**
		 * Pointers to RGB palettes
		 * pointing respectively to
		 *	- blueMap
		 *	- greenMap
		 *	- redMap
		 */
		Palette * bgrMap[3];

		/**
		 * Palette for hue component
		 */
		Palette hMap;

		/**
		 * Palette for Cb component
		 */
		Palette cbMap;

		/**
		 * Palette for Cr component
		 */
		Palette crMap;

		/**
		 * Selected image type to display
		 * @note use integral(imageDisplayIndex) to convert to integer
		 */
		Display imageDisplayIndex;

		/**
		 * True when display image changed since last update
		 */
		bool displayImageChanged;

		/**
		 * Signal to noise ratio of current #displayImage: mean pixel values
		 * divided by the standard deviation of pixel values.
		 */
		Scalar snr;

		/**
		 * Block size for computing Signal to noise ratio on each block
		 * (snrSize x snrSize)
		 */
		RangeValue<int> snrSize = {15, 3, 999, 2};

	public :
		/**
		 * Color spaces constructor
		 * @param inFrame input image
		 */
		CvColorSpaces(Mat * inFrame);

		/**
		 * Color spaces destructor
		 */
		virtual ~CvColorSpaces();

		/**
		 * Update compute selected image for display according to
		 * selected parameters such as imageDisplayIndex, showColorChannel,
		 * and eventually hueDisplay
		 */
		virtual void update();

		/**
		 * Get currently selected image index
		 * @return the currently selected image for display index
		 */
		Display getDisplayImageIndex();

		/**
		 * Select image to set in displayImage :
		 * 	- INPUT selects input image for display
		 * 	- GRAY selects gray converted input image for display
		 * 	- RED selects BGR red component image for display
		 * 	- GREEN selects BGR green component image for display
		 * 	- BLUE selects BGR blue component image for display
		 * 	- HUE selects HSV hue component image for display
		 * 	- SATURATION selects HSV saturation component image for display
		 * 	- VALUE selects HSV value component image for display
		 * 	- Y selects YCrCb Y component image for display
		 * 	- Cr selects YCrCb Cr component image for display
		 * 	- Cb selects YCrCb Cb component image for display
		 * @param index select the index to select display image
		 */
		void setDisplayImageIndex(const Display index);

		/**
		 * Read accessor for #showColoredChannels
		 * @return the current #showColoredChannels
		 */
		bool isChannelsColor() const;

		/**
		 * Write accessor for #showColoredChannels
		 * @param status the new colored channels status
		 */
		virtual void setChannelsColor(const bool status);

		/**
		 * Get currently selected hue display mode
		 * @return the currenlty selected hue display mode
		 */
		HueDisplay getHueDisplaymode();

		/**
		 * Select hue display mode :
		 *	- HUECOLOR Normal Hue mode
		 *	- HUESATURATE Hue*Saturatin mode
		 *	- HUEVALUE Hue*Value mode
		 *	- HUEGRAY Gray mode
		 * @param mode the mode so select
		 */
		void setHueDisplayMode(const HueDisplay mode);

		/**
		 * Gets the image selected for display
		 * @return the display image
		 */
		const Mat & getDisplayImage() const;

		/**
		 * Gets Image pointer corresponding to the current displayMode and
		 * edgeMode
		 * @return Image reference corresponding to the current displayMode and
		 * edgeMode
		 */
		Mat * getDisplayImagePtr();

		/**
		 * Get Signal/Noise Ratio on channel "channel" as computed by
		 * #computeSignalToNoiseRatio();
		 * @param channel the channel to get the SNR on
		 * @return the SNR of this channel
		 * @see #snr
		 */
		double getSNR(const size_t channel);

		/**
		 * Current block size for SNR computing
		 * @return current block size for SNR computing
		 * @see #snrSize
		 */
		int getSNRSize() const;

		/**
		 * Block size for SNR computing setter
		 * @param value the new block size for snr comuting
		 * @see #snrSize
		 */
		virtual void setSNRSize(const int value);

	protected:
		// --------------------------------------------------------------------
		// Setup and cleanup attributes
		// --------------------------------------------------------------------
		/**
		 * Setup internal attributes according to source image
		 * @param sourceImage a new source image
		 * @param fullSetup full setup is needed when source image is changed
		 * @pre sourceimage is not NULL
		 * @note this method should be reimplemented in sub classes
		 */
		virtual void setup(Mat * sourceImage, bool fullSetup = true);

		/**
		 * Clean up internal attributes before changing source image or
		 * cleaning up class before destruction
		 * @note this method should be reimplemented in sub classes
		 */
		virtual void cleanup();

		// --------------------------------------------------------------------
		// Utilities
		// --------------------------------------------------------------------
		/**
		 * Compute signal to noise ratio in the current #displayImage
		 */
		void computeSignalToNoiseRatio();

		/**
		 * Show Min and Max values and locations for a matrix
		 * @param m the matrix to consider
		 */
		static void showMinMaxLoc(const Mat & m);

		/**
		 * Compute Maximum of color images by comparing pixel norm
		 * rather than a per channel max like the openCV max function
		 * @param src1 the first color (or gray) image
		 * @param src2 the second color (or gray) image
		 * @param dst the color (or gray) destination
		 * @pre the norm max is only computed if arguments are of type CV_8UC3,
		 * otherwise ordinary max is performed
		 */
		static void normMax(const Mat& src1, const Mat& src2, Mat& dst);
};

#endif /* CVCOLORSPACES_H_ */
