/*
 * QcvGFilter.cpp
 *
 *  Created on: 27 févr. 2012
 *      Author: davidroussel
 */

#include <functional>
#include <QStringList>

#include "QcvGFilter.h"


/*
 * QcvGFilter constructor
 * @param image the source image
 * @param imageLock the mutex on source image
 * @param updateThread the thread in which this processor runs
 * @param parent parent QObject
 */
QcvGFilter::QcvGFilter(Mat *image,
					   QMutex * imageLock,
					   QThread * updateThread,
					   QObject *parent) :
	CvProcessor(image, VERBOSE_WARNINGS), // <-- virtual base class constructor first
	QcvProcessor(image, imageLock, updateThread, parent),
	CvGFilter(image),
//	selfLock(updateThread != NULL ?
//			 new QMutex() :
//			(imageLock != NULL ? imageLock : NULL)),
	kernelSizeController(&kernelSize,
						 locks[RESULT],
						 1,
						 new function<void(const int)>(std::bind(&CvGFilter::setKernelSize,
																 this,
																 std::placeholders::_1)),
						 (updateThread ==  nullptr ? this : nullptr)),
	sigmaController(&sigma,
					locks[RESULT],
					1.0,
					new function<void(const double)>(std::bind(&CvGFilter::setSigma,
															   this,
															   std::placeholders::_1)),
					(updateThread ==  nullptr ? this : nullptr)),
	thresholdLevelController(&thresholdLevel,
							 locks[RESULT],
							 1,
							 new function<void(const int)>(std::bind(&CvGFilter::setThresholdLevel,
																	 this,
																	 std::placeholders::_1)),
							 (updateThread ==  nullptr ? this : nullptr)),
	harrisKappaController(&harrisKappa,
						  locks[RESULT],
						  1.0,
						  new function<void(const double)>(std::bind(&CvGFilter::setHarrisKappa,
																	 this,
																	 std::placeholders::_1)),
						  (updateThread ==  nullptr ? this : nullptr)),
	displayModeController(&displayMode,
						  ImageDisplay::INPUT_IM,
						  ImageDisplay::NBDISPLAY_IM,
						  displayImagesNames,
						  nullptr, // lock is already used in QcvGFilter::setDisplayMode
						  new function<void(const int)>(std::bind(&QcvGFilter::setDisplayMode,
																  this,
																  std::placeholders::_1)),
						  (updateThread ==  nullptr ? this : nullptr)),
	edgeModeController(&edgeMode,
					   EdgeDisplay::THRESHOLD,
					   EdgeDisplay::NBEDGEDISPLAY,
					   edgeModeNames,
					   nullptr, // lock is already used in QcvGFilter::setEdgeMode
					   new function<void(const int)>(std::bind(&QcvGFilter::setEdgeMode,
															   this,
															   std::placeholders::_1)),
					   (updateThread ==  nullptr ? this : nullptr))
{
	/*
	 * Every time the kernelsize changes through the CvGFilter::setKernelSize
	 * setter in the kernelSizeController, the range value of sigma is modified
	 * so UI elements connected to sigmaController must be updated
	 */
	connect(&kernelSizeController, SIGNAL(updated()),
			&sigmaController, SLOT(refresh()));
}

/*
 * QcvGFilter destructor
 */
QcvGFilter::~QcvGFilter()
{
	message.clear();
	if (locks[RESULT] != NULL)
	{
		locks[RESULT]->lock();
		locks[RESULT]->unlock();
		// locks[RESULT] is destroyed by ~QcvProcessor
	}
}


/*
 * Get the controller for #kernelSize
 * @return the controller for #kernelSize
 */
QRangeIntController * QcvGFilter::getKernelSizeController()
{
	return &kernelSizeController;
}

/*
 * Get the controller for #sigma
 * @return the controller for #sigma
 */
QRangeDoubleController * QcvGFilter::getSigmaController()
{
	return &sigmaController;
}

/*
 * Get the controller for #thresholdLevel
 * @return the controller for #thresholdLevel
 */
QRangeIntController * QcvGFilter::getThresholdLevelController()
{
	return &thresholdLevelController;
}

/*
 * Get the controller for #harrisKappa
 * @return the controller for #harrisKappa
 */
QRangeDoubleController * QcvGFilter::getHarrisKappaController()
{
	return &harrisKappaController;
}

/*
 * Get the controller for #displayMode
 * @return the controller for #displayMode
 */
QEnumController * QcvGFilter::getDisplayModeController()
{
	return &displayModeController;
}

/*
 * Get the controller for #edgeMode
 * @return the controller for #edgeMode
 */
QEnumController * QcvGFilter::getEdgeModeController()
{
	return &edgeModeController;
}

/*
 * Changes source image slot.
 * Attributes needs to be cleaned up then set up again
 * @param image the new source Image
 */
void QcvGFilter::setSourceImage(Mat *image)
	throw (CvProcessorException)
{
	Size previousSize(sourceImage->size());
	int previousNbChannels(nbChannels);
	bool hasSourceLock = locks[SOURCE] != NULL;
	if (hasSourceLock)
	{
		locks[SOURCE]->lock();
	}

	CvProcessor::setSourceImage(image);

	if (hasSourceLock)
	{
		locks[SOURCE]->unlock();
	}

	emit imageChanged(sourceImage);

	emit imageChanged();

	if ((previousSize.width != image->cols) ||
		(previousSize.height != image->rows))
	{
		emit imageSizeChanged();
	}

	if (previousNbChannels != nbChannels)
	{
		emit imageColorsChanged();
	}

	// Force update
	// update();
}

/*
 * Sets a new display mode and emit corresponding imageChanged(Mat*)
 * signal
 * @param displayMode the new display mode to set
 */
void QcvGFilter::setDisplayMode(const int displayMode)
{
	ImageDisplay realDisplayMode = (ImageDisplay) displayMode;
	bool hasLock = locks[RESULT] != NULL;
	if (hasLock)
	{
		locks[RESULT]->lock();
	}

	CvGFilter::setDisplayMode(realDisplayMode);

	emit edgeModeChanged(realDisplayMode == CvGFilter::ImageDisplay::EDGE_MAP_IM);

	if (hasLock)
	{
		locks[RESULT]->unlock();
	}

	message.clear();
	if (this->displayMode >= ImageDisplay::INPUT_IM &&
		this->displayMode < ImageDisplay::NBDISPLAY_IM)
	{
		message.append(displayImagesNames[integral(this->displayMode)]);
	}

	switch (this->displayMode)
	{
		case ImageDisplay::INPUT_IM:
			emit imageChanged(sourceImage);
			break;
		case ImageDisplay::GRAY_IM:
			emit imageChanged(&inFrameGray);
			break;
		case ImageDisplay::BLURRED_IM:
			emit imageChanged(&blurredDisplay);
			break;
		case ImageDisplay::GRADIENT_X_IM:
			emit imageChanged(&dXDisplay);
			break;
		case ImageDisplay::GRADIENT_Y_IM:
			emit imageChanged(&dYDisplay);
			break;
		case ImageDisplay::GRADIENT_MAG_IM:
			emit imageChanged(&gradientMagDisplay);
			break;
		case ImageDisplay::GRADIENT_ANGLE_IM:
			emit imageChanged(&gradientAngleDisplay);
			break;
		case ImageDisplay::EDGE_MAP_IM:
			// Embedded setEdgeMode
			setEdgeMode_Impl(integral(edgeMode), false);
			break;
		case ImageDisplay::LAPLACIAN_IM:
			emit imageChanged(&laplacianDisplay);
			break;
		case ImageDisplay::CORNERNESS_IM:
			emit imageChanged(&cornernessDisplay);
			break;
		case ImageDisplay::HARRISCORNER_IM:
			emit imageChanged(&harrisDisplay);
			break;
		case ImageDisplay::NBDISPLAY_IM:
		default:
			break;
	}

	emit sendMessage(message, defaultTimeOut);
}

/*
 * Set a new edge didsplay mode and emit corresponding
 * imageChanged(Mat*) signal if needed
 * @param edgeMode the new edge mode
 * @param standalone if setEdgeMode is used by itself then true, but
 * false when used inside setDisplayMode
 */
void QcvGFilter::setEdgeMode_Impl(const int edgeMode, const bool standalone)
{
	EdgeDisplay realEdgeMode = (EdgeDisplay) edgeMode;
	if (standalone)
	{
		bool hasLock = locks[RESULT] != NULL;
		if (hasLock)
		{
			locks[RESULT]->lock();
		}

		CvGFilter::setEdgeMode(realEdgeMode);

		if (hasLock)
		{
			locks[RESULT]->unlock();
		}
	}

	if (displayMode == ImageDisplay::EDGE_MAP_IM)
	{
		if (standalone)
		{
			message.clear();
		}
		else
		{
			message.append(": ");
		}

		if (this->edgeMode >= EdgeDisplay::THRESHOLD &&
			this->edgeMode < EdgeDisplay::NBEDGEDISPLAY)
		{
			message.append(edgeModeNames[integral(this->edgeMode)]);
		}

		switch (this->edgeMode)
		{
			case EdgeDisplay::THRESHOLD:
				emit imageChanged(&edgeMap);
				break;
			case EdgeDisplay::CANNY:
				emit imageChanged(&cannyEdgeMap);
				break;
			case EdgeDisplay::MERGED:
				emit imageChanged(&mixEdge);
				break;
			case EdgeDisplay::NBEDGEDISPLAY:
			default:
				break;
		}

		if (standalone)
		{
			emit sendMessage(message, defaultTimeOut);
		}
	}
}

/*
 * Set a new edge didsplay mode in standalone mode and emit corresponding
 * imageChanged(Mat*) signal if needed
 * @param edgeMode the new edge mode
 */
void QcvGFilter::setEdgeMode(const int edgeMode)
{
	setEdgeMode_Impl(edgeMode, true);
}

/*
 * Update computed images slot and sends updated signal
 * required
 */
void QcvGFilter::update()
{
	for (int i = 0; i < locksSize(); i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->lock();
		}
	}

	/*
	 * Update filtered images
	 */
	CvGFilter::update();

	for (int i = 0; i < locksSize(); i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->unlock();
		}
	}

	// at the end of update, if displayImageChanged is true then display
	// image has changed
	if (displayImageChanged)
	{
		emit imageChanged(&displayImage);
	}

	/*
	 * emit updated signal
	 */
	QcvProcessor::update();
}
