/*
 * QcvCapture.h
 *
 *  Created on: 12 sep. 2017
 *	  Author: davidroussel
 */

#ifndef QCVCAPTURE_H
#define QCVCAPTURE_H

#include <QObject>
#include <QMutex>
#include <QSize>
#include <QString>

#include <opencv2/core/mat.hpp>

/**
 * Qt Abstract class base implementation of all classes providing images (from
 * still images to video streams)
 */
class QcvCapture : public QObject
{
	Q_OBJECT

	protected:
		/**
		 * Mutex to ensure atomic access to image
		 */
		QMutex mutex;

		/**
		 * Image Matrix to obtain from capture
		 * @note Please note that #image, #imageResized, #imageFlipped and
		 * #imageDisplay constitute a chain of image which shall be preserved:
		 * image -> imageResized -> imageFlipped -> imageDisplay
		 *	- If #image is changed, then #imageResized should be updated whith
		 *	by calling the #setSize(...) method with required size
		 *	- If #imageResized is changed then #imageFlipped should be updated
		 *	by calling the #setFlipped(...); method with required flipping state
		 *	- If #imageFlipped is changed then #imageDisplay should be updated
		 *	by calling the #setGray(...) method with required gray state
		 */
		cv::Mat image;

		/**
		 * image resized (if required)
		 */
		cv::Mat imageResized;

		/**
		 * [resized] image flipped (if required)
		 */
		cv::Mat imageFlipped;

		/**
		 * Image converted for display, which might be :
		 * 	- scaled
		 * 	- flipped horizontally
		 * 	- converted to gray
		 */
		cv::Mat imageDisplay;

		/**
		 * Indicates if the grabbed image must be flipped
		 */
		bool flipped;

		/**
		 * Indicates if grabbed image must be converted to gray level image
		 */
		bool gray;

		/**
		 * Indicates if the grabbed image must be resized
		 */
		bool resized;

		/**
		 * Current Image size (might be different from natural capture image
		 * size)
		 */
		QSize size;

		/**
		 * Capture natural image size (without resizing)
		 */
		QSize originalSize;

		/**
		 * current framerate based on time measured from last capture
		 */
		double frameRate;

		/**
		 * Status message to send when something changes
		 */
		QString statusMessage;

		/**
		 * Delay during wich sent message will be shown (5 seconds)
		 */
		static int messageDelay;

		/**
		 * Valued and protected constructor (to be used in subclasses)
		 * @param flipped indicate if captured image must be flipped horizontally
		 * @param gray indicate if captured image must be converted to gray
		 * @param width desired width or 0 to keep capture natural width
		 * @param height desired height or 0 to keep capture natural height
		 * @param parent the parent QObject
		 */
		explicit QcvCapture(const bool flipped = false,
							const bool gray = false,
							const size_t width = 0,
							const size_t height = 0,
							QObject *parent = nullptr);
	public:

		/**
		 * The default frame rate for synchronized capture
		 */
		static const double defaultFrameRate;

		/**
		 * Destructor.
		 * releases image(s)
		 */
		virtual ~QcvCapture();

		/**
		 * Gets the mutex used to ensure capture atomic access
		 * @return the mutex used to ensure capture atomic access
		 * @see #mutex
		 */
		QMutex * getMutex();

		/**
		 * Get the current "display" image (evt after gray conversion,
		 * flipping and resizing)
		 * @return the current "display" image
		 * @see #imageDisplay
		 */
		cv::Mat * getImage();

		/**
		 * Get the "flipped" status of the captured video
		 * @return the "flipped" status of the video
		 * @see #flipped
		 */
		bool isFlipped() const;

		/**
		 * Get the gray conversion status of the captured video
		 * @return the gray conversion status of the video
		 * @see #gray
		 */
		bool isGray() const;

		/**
		 * Get the "resized" status of the captured video
		 * @return the "resized" status of the video
		 * @see #resized
		 */
		bool isResized() const;

		/**
		 * Get the current size of captured image
		 * @return the current size of captured image
		 */
		const QSize & getSize() const;

		/**
		 * Get the original capture image size (without resizing)
		 * @return the original capture image size (without resizing)
		 */
		const QSize & getOriginalSize() const;

		/**
		 * Get the current capture frame rate
		 * @return the current capture frame rate
		 */
		double getFrameRate() const;

	signals:
		/**
		 * Signal emitted when a new image has been grabbed
		 */
		void updated();

		/**
		 * Signal to send update message when something changes
		 * @param message the message
		 * @param timeout number of ms the message should be displayed
		 */
		void messageChanged(const QString & message, int timeout = 0);

		/**
		 * Signal to send when image has changed after opening new device or
		 * setting new display size
		 * @param image the new image to send
		 */
		void imageChanged(cv::Mat * image);

		/**
		 * Signal to send when frame rate has changed
		 * @param value the new frame rate
		 */
		void frameRateChanged(const double value);

	public slots:
		/**
		 * Open new device Id
		 * @param deviceId device number to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if device has been opened and checked
		 */
		virtual bool openDevice(const int deviceId,
								const size_t width = 0,
								const size_t height = 0) = 0;

		/**
		 * Open new video file
		 * @param fileName video file to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if video has been opened
		 */
		virtual bool openFile(const QString & fileName,
							  const size_t width = 0,
							  const size_t height = 0) = 0;

		/**
		 * Open Directory containing files to read
		 * @param dirName directory to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if directory containing files to read has been opened
		 */
		virtual bool openDirectory(const QString & dirName,
								   const size_t width = 0,
								   const size_t height = 0) = 0;


		/**
		 * Sets the "flipped" status of captured video
		 * @param value the new "flipped" status
		 */
		virtual void setFlipped(const bool value);

		/**
		 * Sets the gray conversion status of captured video
		 * @param value the new value of gray conversion status
		 */
		virtual void setGray(const bool value);

		/**
		 * Sets the desired size of captured image
		 * @param width the desired width of captured image
		 * @param height the desired height of captured image
		 * @see #size
		 * @post #imageChanged(); signal is emitted
		 */
		virtual void setSize(const QSize & size);

		/**
		 * Sets the desired frame rate on capture
		 *	- If the new frame rate is 0.0 then use natural capture frame rate.
		 *	- If the new frame rate is too high for capture then capture will
		 *	keep its highest possible frame rate according to the device in use
		 * @param frameRate the new frame rate to set
		 * @note Please note that this method has no effect on capture when
		 * synchronized mode is off
		 */
		virtual void setFrameRate(const double frameRate) = 0;

	protected slots:

		/**
		 * Pure Virtual update method
		 * @return true if a new image has been grabbed successfully and false
		 * otherwise.
		 */
		virtual bool update() = 0;

	protected:
		/**
		 * Private implementation to sets the desired size of captured image
		 * @param width the desired width of captured image
		 * @param height the desired height of captured image
		 * @see #size
		 */
		virtual void setSize_Impl(const size_t width,
								  const size_t height) = 0;

		/**
		 * Wrapper method to emit updated signal for other classes to use
		 * @see #updated()
		 */
		void emitUpdated();

		/**
		 * Wrapper method to emit message changed signal for other classes to
		 * use
		 * @see #messageChanged(const QString & message, int timeout);
		 */
		void emitMessageChanged();

		/**
		 * Wrapper method to emit image changed signal for other classes to use
		 * @see #imageChanged(Mat * image);
		 */
		void emitImageChanged();

		/**
		 * Wrapper method to emit frame rate changed signal for other classes
		 * to use
		 * @see #frameRateChanged();
		 */
		void emitFrameRateChanged();
};

#endif // QCVCAPTURE_H
