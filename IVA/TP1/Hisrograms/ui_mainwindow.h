/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <Qcv/matWidgets/QcvMatWidget.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionCamera_0;
    QAction *actionCamera_1;
    QAction *actionFile;
    QAction *actionFlip;
    QAction *actionOriginalSize;
    QAction *actionConstrainedSize;
    QAction *actionQuit;
    QAction *actionRenderImage;
    QAction *actionRenderPixmap;
    QAction *actionRenderOpenGL;
    QAction *actionShowRedHistogram;
    QAction *actionShowGreenHistogram;
    QAction *actionShowBlueHistogram;
    QAction *actionShowGrayHistogram;
    QAction *actionHistogramNormal;
    QAction *actionHistogramCumulative;
    QAction *actionHistogramTimeCumul;
    QAction *actionLUTIdentity;
    QAction *actionLUTInverse;
    QAction *actionLUTGamma;
    QAction *actionLUTThreshold;
    QAction *actionLUTDynamic;
    QAction *actionLUTEqualize;
    QAction *actionLUTUse_Colors;
    QAction *actionTabSize;
    QAction *actionTabHist;
    QAction *actionTabLUT;
    QAction *actionTabTimes;
    QAction *actionLUTCLAHE;
    QAction *actionDirectory;
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QGroupBox *groupBoxLUT;
    QHBoxLayout *horizontalLayout_2;
    QcvMatWidget *widgetLUT;
    QGroupBox *groupBoxHistogram;
    QHBoxLayout *horizontalLayout;
    QcvMatWidget *widgetHistogram;
    QScrollArea *scrollArea;
    QcvMatWidget *widgetImage;
    QTabWidget *tabWidget;
    QWidget *sizeTab;
    QVBoxLayout *verticalLayout;
    QRadioButton *radioButtonOrigSize;
    QRadioButton *radioButtonCustomSize;
    QLabel *labelWidth;
    QSpinBox *spinBoxWidth;
    QLabel *labelHeight;
    QSpinBox *spinBoxHeight;
    QCheckBox *checkBoxFlip;
    QSpacerItem *verticalSpacer;
    QWidget *histTab;
    QVBoxLayout *verticalLayout_7;
    QGroupBox *groupBoxHistChannels;
    QVBoxLayout *verticalLayout_8;
    QCheckBox *checkBoxHistRed;
    QCheckBox *checkBoxHistGreen;
    QCheckBox *checkBoxHistBlue;
    QCheckBox *checkBoxHistGray;
    QGroupBox *groupBoxHistMode;
    QVBoxLayout *verticalLayout_6;
    QRadioButton *radioButtonHMNormal;
    QRadioButton *radioButtonHMCumulative;
    QRadioButton *radioButtonHMTime;
    QSpacerItem *verticalSpacer_3;
    QWidget *lutTab;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *lutBox;
    QVBoxLayout *verticalLayout_3;
    QRadioButton *radioButtonIdentity;
    QRadioButton *radioButtonInverse;
    QRadioButton *radioButtonGamma;
    QRadioButton *radioButtonThreshold;
    QRadioButton *radioButtonDynamic;
    QComboBox *comboBoxDynamicType;
    QRadioButton *radioButtonEqualize;
    QRadioButton *radioButtonClahe;
    QCheckBox *checkBoxUseColors;
    QGroupBox *percentBox;
    QVBoxLayout *verticalLayout_5;
    QSpinBox *spinBoxlutParam;
    QSlider *sliderlutParam;
    QGroupBox *groupBoxClahe;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_3;
    QLabel *labelClipLimit;
    QDoubleSpinBox *doubleSpinBoxClipLimit;
    QSlider *horizontalSliderClipLimit;
    QHBoxLayout *horizontalLayout_4;
    QLabel *labelTileSize;
    QSpinBox *spinBoxTileSize;
    QSlider *horizontalSliderTileSize;
    QGroupBox *groupBoxPostProcessing;
    QVBoxLayout *verticalLayout_12;
    QCheckBox *checkBoxPostFiltering;
    QHBoxLayout *horizontalLayout_6;
    QLabel *labelKernelSize;
    QSpinBox *spinBoxKernelSize;
    QSlider *horizontalSliderKernelSize;
    QSpacerItem *verticalSpacer_2;
    QWidget *timeTab;
    QVBoxLayout *verticalLayout_9;
    QGroupBox *groupBoxHistogram_2;
    QVBoxLayout *verticalLayout_10;
    QLabel *labelUH1;
    QLabel *labelUH1Time;
    QLabel *labelUH2;
    QLabel *labelUH2Time;
    QLabel *labelDH;
    QLabel *labelDHTime;
    QGroupBox *groupBoxLUT_2;
    QVBoxLayout *verticalLayout_11;
    QLabel *labelCL;
    QLabel *labelCLTime;
    QLabel *labelAL;
    QLabel *labelALTime;
    QLabel *labelDL;
    QLabel *labelDLTime;
    QGroupBox *groupBoxDL;
    QHBoxLayout *horizontalLayout_5;
    QLabel *labelAllTime;
    QSpacerItem *verticalSpacer_4;
    QMenuBar *menuBar;
    QMenu *menuSources;
    QMenu *menuVideo;
    QMenu *menuSize;
    QMenu *menuRender;
    QMenu *menuHistogram;
    QMenu *menuChannels;
    QMenu *menuModes;
    QMenu *menuLUT;
    QMenu *menuTabs;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(940, 800);
        actionCamera_0 = new QAction(MainWindow);
        actionCamera_0->setObjectName(QStringLiteral("actionCamera_0"));
        actionCamera_1 = new QAction(MainWindow);
        actionCamera_1->setObjectName(QStringLiteral("actionCamera_1"));
        actionFile = new QAction(MainWindow);
        actionFile->setObjectName(QStringLiteral("actionFile"));
        actionFlip = new QAction(MainWindow);
        actionFlip->setObjectName(QStringLiteral("actionFlip"));
        actionFlip->setCheckable(true);
        actionOriginalSize = new QAction(MainWindow);
        actionOriginalSize->setObjectName(QStringLiteral("actionOriginalSize"));
        actionOriginalSize->setCheckable(true);
        actionConstrainedSize = new QAction(MainWindow);
        actionConstrainedSize->setObjectName(QStringLiteral("actionConstrainedSize"));
        actionConstrainedSize->setCheckable(true);
        actionQuit = new QAction(MainWindow);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        actionRenderImage = new QAction(MainWindow);
        actionRenderImage->setObjectName(QStringLiteral("actionRenderImage"));
        actionRenderImage->setCheckable(true);
        actionRenderImage->setChecked(true);
        actionRenderPixmap = new QAction(MainWindow);
        actionRenderPixmap->setObjectName(QStringLiteral("actionRenderPixmap"));
        actionRenderPixmap->setCheckable(true);
        actionRenderOpenGL = new QAction(MainWindow);
        actionRenderOpenGL->setObjectName(QStringLiteral("actionRenderOpenGL"));
        actionRenderOpenGL->setCheckable(true);
        actionShowRedHistogram = new QAction(MainWindow);
        actionShowRedHistogram->setObjectName(QStringLiteral("actionShowRedHistogram"));
        actionShowGreenHistogram = new QAction(MainWindow);
        actionShowGreenHistogram->setObjectName(QStringLiteral("actionShowGreenHistogram"));
        actionShowBlueHistogram = new QAction(MainWindow);
        actionShowBlueHistogram->setObjectName(QStringLiteral("actionShowBlueHistogram"));
        actionShowGrayHistogram = new QAction(MainWindow);
        actionShowGrayHistogram->setObjectName(QStringLiteral("actionShowGrayHistogram"));
        actionHistogramNormal = new QAction(MainWindow);
        actionHistogramNormal->setObjectName(QStringLiteral("actionHistogramNormal"));
        actionHistogramNormal->setCheckable(true);
        actionHistogramCumulative = new QAction(MainWindow);
        actionHistogramCumulative->setObjectName(QStringLiteral("actionHistogramCumulative"));
        actionHistogramCumulative->setCheckable(true);
        actionHistogramTimeCumul = new QAction(MainWindow);
        actionHistogramTimeCumul->setObjectName(QStringLiteral("actionHistogramTimeCumul"));
        actionHistogramTimeCumul->setCheckable(true);
        actionLUTIdentity = new QAction(MainWindow);
        actionLUTIdentity->setObjectName(QStringLiteral("actionLUTIdentity"));
        actionLUTIdentity->setCheckable(true);
        actionLUTInverse = new QAction(MainWindow);
        actionLUTInverse->setObjectName(QStringLiteral("actionLUTInverse"));
        actionLUTInverse->setCheckable(true);
        actionLUTGamma = new QAction(MainWindow);
        actionLUTGamma->setObjectName(QStringLiteral("actionLUTGamma"));
        actionLUTGamma->setCheckable(true);
        actionLUTThreshold = new QAction(MainWindow);
        actionLUTThreshold->setObjectName(QStringLiteral("actionLUTThreshold"));
        actionLUTThreshold->setCheckable(true);
        actionLUTDynamic = new QAction(MainWindow);
        actionLUTDynamic->setObjectName(QStringLiteral("actionLUTDynamic"));
        actionLUTDynamic->setCheckable(true);
        actionLUTEqualize = new QAction(MainWindow);
        actionLUTEqualize->setObjectName(QStringLiteral("actionLUTEqualize"));
        actionLUTEqualize->setCheckable(true);
        actionLUTUse_Colors = new QAction(MainWindow);
        actionLUTUse_Colors->setObjectName(QStringLiteral("actionLUTUse_Colors"));
        actionTabSize = new QAction(MainWindow);
        actionTabSize->setObjectName(QStringLiteral("actionTabSize"));
        actionTabSize->setCheckable(true);
        actionTabHist = new QAction(MainWindow);
        actionTabHist->setObjectName(QStringLiteral("actionTabHist"));
        actionTabHist->setCheckable(true);
        actionTabLUT = new QAction(MainWindow);
        actionTabLUT->setObjectName(QStringLiteral("actionTabLUT"));
        actionTabLUT->setCheckable(true);
        actionTabTimes = new QAction(MainWindow);
        actionTabTimes->setObjectName(QStringLiteral("actionTabTimes"));
        actionTabTimes->setCheckable(true);
        actionLUTCLAHE = new QAction(MainWindow);
        actionLUTCLAHE->setObjectName(QStringLiteral("actionLUTCLAHE"));
        actionLUTCLAHE->setCheckable(true);
        actionDirectory = new QAction(MainWindow);
        actionDirectory->setObjectName(QStringLiteral("actionDirectory"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(6, 0, 6, 0);
        groupBoxLUT = new QGroupBox(centralWidget);
        groupBoxLUT->setObjectName(QStringLiteral("groupBoxLUT"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(groupBoxLUT->sizePolicy().hasHeightForWidth());
        groupBoxLUT->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(groupBoxLUT);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(3, 3, 3, 3);
        widgetLUT = new QcvMatWidget(groupBoxLUT);
        widgetLUT->setObjectName(QStringLiteral("widgetLUT"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(widgetLUT->sizePolicy().hasHeightForWidth());
        widgetLUT->setSizePolicy(sizePolicy1);
        widgetLUT->setMinimumSize(QSize(100, 100));

        horizontalLayout_2->addWidget(widgetLUT);


        gridLayout->addWidget(groupBoxLUT, 1, 1, 1, 1);

        groupBoxHistogram = new QGroupBox(centralWidget);
        groupBoxHistogram->setObjectName(QStringLiteral("groupBoxHistogram"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBoxHistogram->sizePolicy().hasHeightForWidth());
        groupBoxHistogram->setSizePolicy(sizePolicy2);
        groupBoxHistogram->setMinimumSize(QSize(100, 100));
        horizontalLayout = new QHBoxLayout(groupBoxHistogram);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(3, 3, 3, 3);
        widgetHistogram = new QcvMatWidget(groupBoxHistogram);
        widgetHistogram->setObjectName(QStringLiteral("widgetHistogram"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(widgetHistogram->sizePolicy().hasHeightForWidth());
        widgetHistogram->setSizePolicy(sizePolicy3);
        widgetHistogram->setMinimumSize(QSize(512, 100));

        horizontalLayout->addWidget(widgetHistogram);


        gridLayout->addWidget(groupBoxHistogram, 1, 0, 1, 1);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        sizePolicy3.setHeightForWidth(scrollArea->sizePolicy().hasHeightForWidth());
        scrollArea->setSizePolicy(sizePolicy3);
        scrollArea->setWidgetResizable(true);
        scrollArea->setAlignment(Qt::AlignCenter);
        widgetImage = new QcvMatWidget();
        widgetImage->setObjectName(QStringLiteral("widgetImage"));
        widgetImage->setGeometry(QRect(0, 0, 754, 629));
        QSizePolicy sizePolicy4(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(widgetImage->sizePolicy().hasHeightForWidth());
        widgetImage->setSizePolicy(sizePolicy4);
        scrollArea->setWidget(widgetImage);

        gridLayout->addWidget(scrollArea, 0, 0, 1, 1);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        QSizePolicy sizePolicy5(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy5);
        tabWidget->setStyleSheet(QStringLiteral("font: 10pt \"Lucida Grande\";"));
        tabWidget->setTabPosition(QTabWidget::West);
        sizeTab = new QWidget();
        sizeTab->setObjectName(QStringLiteral("sizeTab"));
        verticalLayout = new QVBoxLayout(sizeTab);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(8, 8, 8, 8);
        radioButtonOrigSize = new QRadioButton(sizeTab);
        radioButtonOrigSize->setObjectName(QStringLiteral("radioButtonOrigSize"));
        radioButtonOrigSize->setChecked(true);

        verticalLayout->addWidget(radioButtonOrigSize);

        radioButtonCustomSize = new QRadioButton(sizeTab);
        radioButtonCustomSize->setObjectName(QStringLiteral("radioButtonCustomSize"));

        verticalLayout->addWidget(radioButtonCustomSize);

        labelWidth = new QLabel(sizeTab);
        labelWidth->setObjectName(QStringLiteral("labelWidth"));

        verticalLayout->addWidget(labelWidth);

        spinBoxWidth = new QSpinBox(sizeTab);
        spinBoxWidth->setObjectName(QStringLiteral("spinBoxWidth"));
        spinBoxWidth->setMaximum(1600);
        spinBoxWidth->setSingleStep(4);

        verticalLayout->addWidget(spinBoxWidth);

        labelHeight = new QLabel(sizeTab);
        labelHeight->setObjectName(QStringLiteral("labelHeight"));

        verticalLayout->addWidget(labelHeight);

        spinBoxHeight = new QSpinBox(sizeTab);
        spinBoxHeight->setObjectName(QStringLiteral("spinBoxHeight"));
        spinBoxHeight->setMaximum(1200);
        spinBoxHeight->setSingleStep(4);

        verticalLayout->addWidget(spinBoxHeight);

        checkBoxFlip = new QCheckBox(sizeTab);
        checkBoxFlip->setObjectName(QStringLiteral("checkBoxFlip"));

        verticalLayout->addWidget(checkBoxFlip);

        verticalSpacer = new QSpacerItem(20, 228, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        tabWidget->addTab(sizeTab, QString());
        histTab = new QWidget();
        histTab->setObjectName(QStringLiteral("histTab"));
        verticalLayout_7 = new QVBoxLayout(histTab);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(8, 8, 8, 8);
        groupBoxHistChannels = new QGroupBox(histTab);
        groupBoxHistChannels->setObjectName(QStringLiteral("groupBoxHistChannels"));
        verticalLayout_8 = new QVBoxLayout(groupBoxHistChannels);
        verticalLayout_8->setSpacing(6);
        verticalLayout_8->setContentsMargins(11, 11, 11, 11);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        verticalLayout_8->setContentsMargins(2, 2, 2, 2);
        checkBoxHistRed = new QCheckBox(groupBoxHistChannels);
        checkBoxHistRed->setObjectName(QStringLiteral("checkBoxHistRed"));

        verticalLayout_8->addWidget(checkBoxHistRed);

        checkBoxHistGreen = new QCheckBox(groupBoxHistChannels);
        checkBoxHistGreen->setObjectName(QStringLiteral("checkBoxHistGreen"));

        verticalLayout_8->addWidget(checkBoxHistGreen);

        checkBoxHistBlue = new QCheckBox(groupBoxHistChannels);
        checkBoxHistBlue->setObjectName(QStringLiteral("checkBoxHistBlue"));

        verticalLayout_8->addWidget(checkBoxHistBlue);

        checkBoxHistGray = new QCheckBox(groupBoxHistChannels);
        checkBoxHistGray->setObjectName(QStringLiteral("checkBoxHistGray"));

        verticalLayout_8->addWidget(checkBoxHistGray);


        verticalLayout_7->addWidget(groupBoxHistChannels);

        groupBoxHistMode = new QGroupBox(histTab);
        groupBoxHistMode->setObjectName(QStringLiteral("groupBoxHistMode"));
        verticalLayout_6 = new QVBoxLayout(groupBoxHistMode);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(2, 2, 2, 2);
        radioButtonHMNormal = new QRadioButton(groupBoxHistMode);
        radioButtonHMNormal->setObjectName(QStringLiteral("radioButtonHMNormal"));
        radioButtonHMNormal->setChecked(true);

        verticalLayout_6->addWidget(radioButtonHMNormal);

        radioButtonHMCumulative = new QRadioButton(groupBoxHistMode);
        radioButtonHMCumulative->setObjectName(QStringLiteral("radioButtonHMCumulative"));

        verticalLayout_6->addWidget(radioButtonHMCumulative);

        radioButtonHMTime = new QRadioButton(groupBoxHistMode);
        radioButtonHMTime->setObjectName(QStringLiteral("radioButtonHMTime"));

        verticalLayout_6->addWidget(radioButtonHMTime);


        verticalLayout_7->addWidget(groupBoxHistMode);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_3);

        tabWidget->addTab(histTab, QString());
        lutTab = new QWidget();
        lutTab->setObjectName(QStringLiteral("lutTab"));
        verticalLayout_2 = new QVBoxLayout(lutTab);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(8, 8, 8, 8);
        lutBox = new QGroupBox(lutTab);
        lutBox->setObjectName(QStringLiteral("lutBox"));
        verticalLayout_3 = new QVBoxLayout(lutBox);
        verticalLayout_3->setSpacing(5);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(2, 2, 2, 2);
        radioButtonIdentity = new QRadioButton(lutBox);
        radioButtonIdentity->setObjectName(QStringLiteral("radioButtonIdentity"));
        radioButtonIdentity->setChecked(true);

        verticalLayout_3->addWidget(radioButtonIdentity);

        radioButtonInverse = new QRadioButton(lutBox);
        radioButtonInverse->setObjectName(QStringLiteral("radioButtonInverse"));

        verticalLayout_3->addWidget(radioButtonInverse);

        radioButtonGamma = new QRadioButton(lutBox);
        radioButtonGamma->setObjectName(QStringLiteral("radioButtonGamma"));

        verticalLayout_3->addWidget(radioButtonGamma);

        radioButtonThreshold = new QRadioButton(lutBox);
        radioButtonThreshold->setObjectName(QStringLiteral("radioButtonThreshold"));

        verticalLayout_3->addWidget(radioButtonThreshold);

        radioButtonDynamic = new QRadioButton(lutBox);
        radioButtonDynamic->setObjectName(QStringLiteral("radioButtonDynamic"));

        verticalLayout_3->addWidget(radioButtonDynamic);

        comboBoxDynamicType = new QComboBox(lutBox);
        comboBoxDynamicType->addItem(QString());
        comboBoxDynamicType->addItem(QString());
        comboBoxDynamicType->addItem(QString());
        comboBoxDynamicType->setObjectName(QStringLiteral("comboBoxDynamicType"));

        verticalLayout_3->addWidget(comboBoxDynamicType);

        radioButtonEqualize = new QRadioButton(lutBox);
        radioButtonEqualize->setObjectName(QStringLiteral("radioButtonEqualize"));

        verticalLayout_3->addWidget(radioButtonEqualize);

        radioButtonClahe = new QRadioButton(lutBox);
        radioButtonClahe->setObjectName(QStringLiteral("radioButtonClahe"));

        verticalLayout_3->addWidget(radioButtonClahe);

        checkBoxUseColors = new QCheckBox(lutBox);
        checkBoxUseColors->setObjectName(QStringLiteral("checkBoxUseColors"));

        verticalLayout_3->addWidget(checkBoxUseColors);


        verticalLayout_2->addWidget(lutBox);

        percentBox = new QGroupBox(lutTab);
        percentBox->setObjectName(QStringLiteral("percentBox"));
        percentBox->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        verticalLayout_5 = new QVBoxLayout(percentBox);
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(2, 2, 2, 2);
        spinBoxlutParam = new QSpinBox(percentBox);
        spinBoxlutParam->setObjectName(QStringLiteral("spinBoxlutParam"));
        spinBoxlutParam->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        spinBoxlutParam->setMinimum(5);
        spinBoxlutParam->setMaximum(95);
        spinBoxlutParam->setValue(80);

        verticalLayout_5->addWidget(spinBoxlutParam);

        sliderlutParam = new QSlider(percentBox);
        sliderlutParam->setObjectName(QStringLiteral("sliderlutParam"));
        sliderlutParam->setMinimum(5);
        sliderlutParam->setMaximum(95);
        sliderlutParam->setValue(80);
        sliderlutParam->setOrientation(Qt::Horizontal);

        verticalLayout_5->addWidget(sliderlutParam);


        verticalLayout_2->addWidget(percentBox);

        groupBoxClahe = new QGroupBox(lutTab);
        groupBoxClahe->setObjectName(QStringLiteral("groupBoxClahe"));
        verticalLayout_4 = new QVBoxLayout(groupBoxClahe);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(2, 2, 2, 2);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        labelClipLimit = new QLabel(groupBoxClahe);
        labelClipLimit->setObjectName(QStringLiteral("labelClipLimit"));

        horizontalLayout_3->addWidget(labelClipLimit);

        doubleSpinBoxClipLimit = new QDoubleSpinBox(groupBoxClahe);
        doubleSpinBoxClipLimit->setObjectName(QStringLiteral("doubleSpinBoxClipLimit"));
        doubleSpinBoxClipLimit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        doubleSpinBoxClipLimit->setDecimals(1);

        horizontalLayout_3->addWidget(doubleSpinBoxClipLimit);


        verticalLayout_4->addLayout(horizontalLayout_3);

        horizontalSliderClipLimit = new QSlider(groupBoxClahe);
        horizontalSliderClipLimit->setObjectName(QStringLiteral("horizontalSliderClipLimit"));
        horizontalSliderClipLimit->setOrientation(Qt::Horizontal);

        verticalLayout_4->addWidget(horizontalSliderClipLimit);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        labelTileSize = new QLabel(groupBoxClahe);
        labelTileSize->setObjectName(QStringLiteral("labelTileSize"));

        horizontalLayout_4->addWidget(labelTileSize);

        spinBoxTileSize = new QSpinBox(groupBoxClahe);
        spinBoxTileSize->setObjectName(QStringLiteral("spinBoxTileSize"));
        spinBoxTileSize->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_4->addWidget(spinBoxTileSize);


        verticalLayout_4->addLayout(horizontalLayout_4);

        horizontalSliderTileSize = new QSlider(groupBoxClahe);
        horizontalSliderTileSize->setObjectName(QStringLiteral("horizontalSliderTileSize"));
        horizontalSliderTileSize->setOrientation(Qt::Horizontal);

        verticalLayout_4->addWidget(horizontalSliderTileSize);


        verticalLayout_2->addWidget(groupBoxClahe);

        groupBoxPostProcessing = new QGroupBox(lutTab);
        groupBoxPostProcessing->setObjectName(QStringLiteral("groupBoxPostProcessing"));
        groupBoxPostProcessing->setFlat(false);
        groupBoxPostProcessing->setCheckable(false);
        verticalLayout_12 = new QVBoxLayout(groupBoxPostProcessing);
        verticalLayout_12->setSpacing(0);
        verticalLayout_12->setContentsMargins(11, 11, 11, 11);
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        verticalLayout_12->setContentsMargins(2, 2, 2, 2);
        checkBoxPostFiltering = new QCheckBox(groupBoxPostProcessing);
        checkBoxPostFiltering->setObjectName(QStringLiteral("checkBoxPostFiltering"));

        verticalLayout_12->addWidget(checkBoxPostFiltering);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        labelKernelSize = new QLabel(groupBoxPostProcessing);
        labelKernelSize->setObjectName(QStringLiteral("labelKernelSize"));

        horizontalLayout_6->addWidget(labelKernelSize);

        spinBoxKernelSize = new QSpinBox(groupBoxPostProcessing);
        spinBoxKernelSize->setObjectName(QStringLiteral("spinBoxKernelSize"));
        QSizePolicy sizePolicy6(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(spinBoxKernelSize->sizePolicy().hasHeightForWidth());
        spinBoxKernelSize->setSizePolicy(sizePolicy6);
        spinBoxKernelSize->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(spinBoxKernelSize);


        verticalLayout_12->addLayout(horizontalLayout_6);

        horizontalSliderKernelSize = new QSlider(groupBoxPostProcessing);
        horizontalSliderKernelSize->setObjectName(QStringLiteral("horizontalSliderKernelSize"));
        horizontalSliderKernelSize->setOrientation(Qt::Horizontal);

        verticalLayout_12->addWidget(horizontalSliderKernelSize);


        verticalLayout_2->addWidget(groupBoxPostProcessing);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_2);

        tabWidget->addTab(lutTab, QString());
        timeTab = new QWidget();
        timeTab->setObjectName(QStringLiteral("timeTab"));
        verticalLayout_9 = new QVBoxLayout(timeTab);
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setContentsMargins(11, 11, 11, 11);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        groupBoxHistogram_2 = new QGroupBox(timeTab);
        groupBoxHistogram_2->setObjectName(QStringLiteral("groupBoxHistogram_2"));
        verticalLayout_10 = new QVBoxLayout(groupBoxHistogram_2);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(11, 11, 11, 11);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        verticalLayout_10->setContentsMargins(2, 2, 2, 2);
        labelUH1 = new QLabel(groupBoxHistogram_2);
        labelUH1->setObjectName(QStringLiteral("labelUH1"));

        verticalLayout_10->addWidget(labelUH1);

        labelUH1Time = new QLabel(groupBoxHistogram_2);
        labelUH1Time->setObjectName(QStringLiteral("labelUH1Time"));
        labelUH1Time->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_10->addWidget(labelUH1Time);

        labelUH2 = new QLabel(groupBoxHistogram_2);
        labelUH2->setObjectName(QStringLiteral("labelUH2"));

        verticalLayout_10->addWidget(labelUH2);

        labelUH2Time = new QLabel(groupBoxHistogram_2);
        labelUH2Time->setObjectName(QStringLiteral("labelUH2Time"));
        labelUH2Time->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_10->addWidget(labelUH2Time);

        labelDH = new QLabel(groupBoxHistogram_2);
        labelDH->setObjectName(QStringLiteral("labelDH"));

        verticalLayout_10->addWidget(labelDH);

        labelDHTime = new QLabel(groupBoxHistogram_2);
        labelDHTime->setObjectName(QStringLiteral("labelDHTime"));
        labelDHTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_10->addWidget(labelDHTime);


        verticalLayout_9->addWidget(groupBoxHistogram_2);

        groupBoxLUT_2 = new QGroupBox(timeTab);
        groupBoxLUT_2->setObjectName(QStringLiteral("groupBoxLUT_2"));
        verticalLayout_11 = new QVBoxLayout(groupBoxLUT_2);
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setContentsMargins(11, 11, 11, 11);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        verticalLayout_11->setContentsMargins(2, 2, 2, 2);
        labelCL = new QLabel(groupBoxLUT_2);
        labelCL->setObjectName(QStringLiteral("labelCL"));

        verticalLayout_11->addWidget(labelCL);

        labelCLTime = new QLabel(groupBoxLUT_2);
        labelCLTime->setObjectName(QStringLiteral("labelCLTime"));
        labelCLTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_11->addWidget(labelCLTime);

        labelAL = new QLabel(groupBoxLUT_2);
        labelAL->setObjectName(QStringLiteral("labelAL"));

        verticalLayout_11->addWidget(labelAL);

        labelALTime = new QLabel(groupBoxLUT_2);
        labelALTime->setObjectName(QStringLiteral("labelALTime"));
        labelALTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_11->addWidget(labelALTime);

        labelDL = new QLabel(groupBoxLUT_2);
        labelDL->setObjectName(QStringLiteral("labelDL"));

        verticalLayout_11->addWidget(labelDL);

        labelDLTime = new QLabel(groupBoxLUT_2);
        labelDLTime->setObjectName(QStringLiteral("labelDLTime"));
        labelDLTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout_11->addWidget(labelDLTime);


        verticalLayout_9->addWidget(groupBoxLUT_2);

        groupBoxDL = new QGroupBox(timeTab);
        groupBoxDL->setObjectName(QStringLiteral("groupBoxDL"));
        horizontalLayout_5 = new QHBoxLayout(groupBoxDL);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(2, 2, 2, 2);
        labelAllTime = new QLabel(groupBoxDL);
        labelAllTime->setObjectName(QStringLiteral("labelAllTime"));
        labelAllTime->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(labelAllTime);


        verticalLayout_9->addWidget(groupBoxDL);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_4);

        tabWidget->addTab(timeTab, QString());

        gridLayout->addWidget(tabWidget, 0, 1, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 940, 22));
        menuSources = new QMenu(menuBar);
        menuSources->setObjectName(QStringLiteral("menuSources"));
        menuVideo = new QMenu(menuBar);
        menuVideo->setObjectName(QStringLiteral("menuVideo"));
        menuSize = new QMenu(menuVideo);
        menuSize->setObjectName(QStringLiteral("menuSize"));
        menuRender = new QMenu(menuBar);
        menuRender->setObjectName(QStringLiteral("menuRender"));
        menuHistogram = new QMenu(menuBar);
        menuHistogram->setObjectName(QStringLiteral("menuHistogram"));
        menuChannels = new QMenu(menuHistogram);
        menuChannels->setObjectName(QStringLiteral("menuChannels"));
        menuModes = new QMenu(menuHistogram);
        menuModes->setObjectName(QStringLiteral("menuModes"));
        menuLUT = new QMenu(menuBar);
        menuLUT->setObjectName(QStringLiteral("menuLUT"));
        menuTabs = new QMenu(menuBar);
        menuTabs->setObjectName(QStringLiteral("menuTabs"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        QWidget::setTabOrder(tabWidget, radioButtonOrigSize);
        QWidget::setTabOrder(radioButtonOrigSize, radioButtonCustomSize);
        QWidget::setTabOrder(radioButtonCustomSize, spinBoxWidth);
        QWidget::setTabOrder(spinBoxWidth, spinBoxHeight);
        QWidget::setTabOrder(spinBoxHeight, checkBoxFlip);
        QWidget::setTabOrder(checkBoxFlip, checkBoxHistRed);
        QWidget::setTabOrder(checkBoxHistRed, checkBoxHistGreen);
        QWidget::setTabOrder(checkBoxHistGreen, checkBoxHistBlue);
        QWidget::setTabOrder(checkBoxHistBlue, checkBoxHistGray);
        QWidget::setTabOrder(checkBoxHistGray, radioButtonHMNormal);
        QWidget::setTabOrder(radioButtonHMNormal, radioButtonHMCumulative);
        QWidget::setTabOrder(radioButtonHMCumulative, radioButtonHMTime);
        QWidget::setTabOrder(radioButtonHMTime, radioButtonIdentity);
        QWidget::setTabOrder(radioButtonIdentity, radioButtonInverse);
        QWidget::setTabOrder(radioButtonInverse, radioButtonGamma);
        QWidget::setTabOrder(radioButtonGamma, radioButtonThreshold);
        QWidget::setTabOrder(radioButtonThreshold, radioButtonEqualize);
        QWidget::setTabOrder(radioButtonEqualize, spinBoxlutParam);
        QWidget::setTabOrder(spinBoxlutParam, sliderlutParam);
        QWidget::setTabOrder(sliderlutParam, scrollArea);

        menuBar->addAction(menuSources->menuAction());
        menuBar->addAction(menuVideo->menuAction());
        menuBar->addAction(menuRender->menuAction());
        menuBar->addAction(menuHistogram->menuAction());
        menuBar->addAction(menuLUT->menuAction());
        menuBar->addAction(menuTabs->menuAction());
        menuSources->addAction(actionCamera_0);
        menuSources->addAction(actionCamera_1);
        menuSources->addAction(actionFile);
        menuSources->addAction(actionDirectory);
        menuSources->addSeparator();
        menuSources->addAction(actionQuit);
        menuVideo->addAction(actionFlip);
        menuVideo->addSeparator();
        menuVideo->addAction(menuSize->menuAction());
        menuSize->addAction(actionOriginalSize);
        menuSize->addAction(actionConstrainedSize);
        menuRender->addAction(actionRenderImage);
        menuRender->addAction(actionRenderPixmap);
        menuRender->addAction(actionRenderOpenGL);
        menuHistogram->addAction(menuChannels->menuAction());
        menuHistogram->addAction(menuModes->menuAction());
        menuChannels->addAction(actionShowRedHistogram);
        menuChannels->addAction(actionShowGreenHistogram);
        menuChannels->addAction(actionShowBlueHistogram);
        menuChannels->addAction(actionShowGrayHistogram);
        menuModes->addAction(actionHistogramNormal);
        menuModes->addAction(actionHistogramCumulative);
        menuModes->addAction(actionHistogramTimeCumul);
        menuLUT->addAction(actionLUTIdentity);
        menuLUT->addAction(actionLUTInverse);
        menuLUT->addAction(actionLUTGamma);
        menuLUT->addAction(actionLUTThreshold);
        menuLUT->addAction(actionLUTDynamic);
        menuLUT->addAction(actionLUTEqualize);
        menuLUT->addAction(actionLUTCLAHE);
        menuLUT->addSeparator();
        menuLUT->addAction(actionLUTUse_Colors);
        menuTabs->addAction(actionTabSize);
        menuTabs->addAction(actionTabHist);
        menuTabs->addAction(actionTabLUT);
        menuTabs->addAction(actionTabTimes);

        retranslateUi(MainWindow);
        QObject::connect(radioButtonCustomSize, SIGNAL(clicked(bool)), actionConstrainedSize, SLOT(setChecked(bool)));
        QObject::connect(actionConstrainedSize, SIGNAL(triggered(bool)), radioButtonCustomSize, SLOT(setChecked(bool)));
        QObject::connect(radioButtonOrigSize, SIGNAL(clicked(bool)), actionOriginalSize, SLOT(setChecked(bool)));
        QObject::connect(actionOriginalSize, SIGNAL(triggered(bool)), radioButtonOrigSize, SLOT(setChecked(bool)));
        QObject::connect(checkBoxFlip, SIGNAL(clicked(bool)), actionFlip, SLOT(setChecked(bool)));
        QObject::connect(actionFlip, SIGNAL(triggered(bool)), checkBoxFlip, SLOT(setChecked(bool)));
        QObject::connect(spinBoxlutParam, SIGNAL(valueChanged(int)), sliderlutParam, SLOT(setValue(int)));
        QObject::connect(sliderlutParam, SIGNAL(valueChanged(int)), spinBoxlutParam, SLOT(setValue(int)));
        QObject::connect(checkBoxPostFiltering, SIGNAL(toggled(bool)), spinBoxKernelSize, SLOT(setEnabled(bool)));
        QObject::connect(checkBoxPostFiltering, SIGNAL(toggled(bool)), horizontalSliderKernelSize, SLOT(setEnabled(bool)));

        tabWidget->setCurrentIndex(2);
        comboBoxDynamicType->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Histograms & LUTs", nullptr));
        actionCamera_0->setText(QApplication::translate("MainWindow", "Camera 0", nullptr));
#ifndef QT_NO_TOOLTIP
        actionCamera_0->setToolTip(QApplication::translate("MainWindow", "cam\303\251ra interne", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionCamera_0->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+0", nullptr));
#endif // QT_NO_SHORTCUT
        actionCamera_1->setText(QApplication::translate("MainWindow", "Camera 1", nullptr));
#ifndef QT_NO_TOOLTIP
        actionCamera_1->setToolTip(QApplication::translate("MainWindow", "cam\303\251ra externe", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionCamera_1->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+1", nullptr));
#endif // QT_NO_SHORTCUT
        actionFile->setText(QApplication::translate("MainWindow", "File", nullptr));
#ifndef QT_NO_TOOLTIP
        actionFile->setToolTip(QApplication::translate("MainWindow", "fichier video", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionFile->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_NO_SHORTCUT
        actionFlip->setText(QApplication::translate("MainWindow", "flip", nullptr));
        actionOriginalSize->setText(QApplication::translate("MainWindow", "Originale", nullptr));
#ifndef QT_NO_TOOLTIP
        actionOriginalSize->setToolTip(QApplication::translate("MainWindow", "taille originale", nullptr));
#endif // QT_NO_TOOLTIP
        actionConstrainedSize->setText(QApplication::translate("MainWindow", "Custom", nullptr));
        actionConstrainedSize->setIconText(QApplication::translate("MainWindow", "contrainte", nullptr));
#ifndef QT_NO_TOOLTIP
        actionConstrainedSize->setToolTip(QApplication::translate("MainWindow", "taille impos\303\251e", nullptr));
#endif // QT_NO_TOOLTIP
        actionQuit->setText(QApplication::translate("MainWindow", "Quitter", nullptr));
        actionRenderImage->setText(QApplication::translate("MainWindow", "Image", nullptr));
        actionRenderPixmap->setText(QApplication::translate("MainWindow", "Pixmap", nullptr));
        actionRenderOpenGL->setText(QApplication::translate("MainWindow", "OpenGL", nullptr));
        actionShowRedHistogram->setText(QApplication::translate("MainWindow", "Red", nullptr));
#ifndef QT_NO_TOOLTIP
        actionShowRedHistogram->setToolTip(QApplication::translate("MainWindow", "Show Red component of the historam", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionShowRedHistogram->setShortcut(QApplication::translate("MainWindow", "R", nullptr));
#endif // QT_NO_SHORTCUT
        actionShowGreenHistogram->setText(QApplication::translate("MainWindow", "Green", nullptr));
#ifndef QT_NO_TOOLTIP
        actionShowGreenHistogram->setToolTip(QApplication::translate("MainWindow", "Show Green component of the histogram", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionShowGreenHistogram->setShortcut(QApplication::translate("MainWindow", "G", nullptr));
#endif // QT_NO_SHORTCUT
        actionShowBlueHistogram->setText(QApplication::translate("MainWindow", "Blue", nullptr));
#ifndef QT_NO_TOOLTIP
        actionShowBlueHistogram->setToolTip(QApplication::translate("MainWindow", "Show Blue component of the histogram", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionShowBlueHistogram->setShortcut(QApplication::translate("MainWindow", "B", nullptr));
#endif // QT_NO_SHORTCUT
        actionShowGrayHistogram->setText(QApplication::translate("MainWindow", "Gray", nullptr));
#ifndef QT_NO_TOOLTIP
        actionShowGrayHistogram->setToolTip(QApplication::translate("MainWindow", "Show Gray component of the histogram", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionShowGrayHistogram->setShortcut(QApplication::translate("MainWindow", "K", nullptr));
#endif // QT_NO_SHORTCUT
        actionHistogramNormal->setText(QApplication::translate("MainWindow", "Normal", nullptr));
#ifndef QT_NO_TOOLTIP
        actionHistogramNormal->setToolTip(QApplication::translate("MainWindow", "Normal histogram", nullptr));
#endif // QT_NO_TOOLTIP
        actionHistogramCumulative->setText(QApplication::translate("MainWindow", "Cumulative", nullptr));
#ifndef QT_NO_TOOLTIP
        actionHistogramCumulative->setToolTip(QApplication::translate("MainWindow", "Cumulative Histogram", nullptr));
#endif // QT_NO_TOOLTIP
        actionHistogramTimeCumul->setText(QApplication::translate("MainWindow", "Time Cumul.", nullptr));
#ifndef QT_NO_TOOLTIP
        actionHistogramTimeCumul->setToolTip(QApplication::translate("MainWindow", "Time Cumulative Histigram", nullptr));
#endif // QT_NO_TOOLTIP
        actionLUTIdentity->setText(QApplication::translate("MainWindow", "Identity", nullptr));
#ifndef QT_NO_TOOLTIP
        actionLUTIdentity->setToolTip(QApplication::translate("MainWindow", "Identity transfert function", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLUTIdentity->setShortcut(QApplication::translate("MainWindow", "I", nullptr));
#endif // QT_NO_SHORTCUT
        actionLUTInverse->setText(QApplication::translate("MainWindow", "Inverse", nullptr));
#ifndef QT_NO_TOOLTIP
        actionLUTInverse->setToolTip(QApplication::translate("MainWindow", "Inverse transfert function", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLUTInverse->setShortcut(QApplication::translate("MainWindow", "V", nullptr));
#endif // QT_NO_SHORTCUT
        actionLUTGamma->setText(QApplication::translate("MainWindow", "Gamma", nullptr));
#ifndef QT_NO_TOOLTIP
        actionLUTGamma->setToolTip(QApplication::translate("MainWindow", "Gamma transfert function", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLUTGamma->setShortcut(QApplication::translate("MainWindow", "M", nullptr));
#endif // QT_NO_SHORTCUT
        actionLUTThreshold->setText(QApplication::translate("MainWindow", "Threshold", nullptr));
#ifndef QT_NO_TOOLTIP
        actionLUTThreshold->setToolTip(QApplication::translate("MainWindow", "Threshold transfert function", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLUTThreshold->setShortcut(QApplication::translate("MainWindow", "T", nullptr));
#endif // QT_NO_SHORTCUT
        actionLUTDynamic->setText(QApplication::translate("MainWindow", "Dynamic", nullptr));
#ifndef QT_NO_TOOLTIP
        actionLUTDynamic->setToolTip(QApplication::translate("MainWindow", "Dynamic % transfert function", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLUTDynamic->setShortcut(QApplication::translate("MainWindow", "D", nullptr));
#endif // QT_NO_SHORTCUT
        actionLUTEqualize->setText(QApplication::translate("MainWindow", "Equalize", nullptr));
#ifndef QT_NO_TOOLTIP
        actionLUTEqualize->setToolTip(QApplication::translate("MainWindow", "Equalize transfert function", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLUTEqualize->setShortcut(QApplication::translate("MainWindow", "E", nullptr));
#endif // QT_NO_SHORTCUT
        actionLUTUse_Colors->setText(QApplication::translate("MainWindow", "Use Colors", nullptr));
#ifndef QT_NO_TOOLTIP
        actionLUTUse_Colors->setToolTip(QApplication::translate("MainWindow", "Use all color channels", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLUTUse_Colors->setShortcut(QApplication::translate("MainWindow", "C", nullptr));
#endif // QT_NO_SHORTCUT
        actionTabSize->setText(QApplication::translate("MainWindow", "Size", nullptr));
#ifndef QT_NO_TOOLTIP
        actionTabSize->setToolTip(QApplication::translate("MainWindow", "Switch to Size Tab", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionTabSize->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_NO_SHORTCUT
        actionTabHist->setText(QApplication::translate("MainWindow", "Hist", nullptr));
#ifndef QT_NO_TOOLTIP
        actionTabHist->setToolTip(QApplication::translate("MainWindow", "Switch to Histogram Tab", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionTabHist->setShortcut(QApplication::translate("MainWindow", "Ctrl+I", nullptr));
#endif // QT_NO_SHORTCUT
        actionTabLUT->setText(QApplication::translate("MainWindow", "LUT", nullptr));
#ifndef QT_NO_TOOLTIP
        actionTabLUT->setToolTip(QApplication::translate("MainWindow", "Switch to LUT Tab", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionTabLUT->setShortcut(QApplication::translate("MainWindow", "Ctrl+L", nullptr));
#endif // QT_NO_SHORTCUT
        actionTabTimes->setText(QApplication::translate("MainWindow", "Times", nullptr));
#ifndef QT_NO_TOOLTIP
        actionTabTimes->setToolTip(QApplication::translate("MainWindow", "Switch to Processing Times Tab", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionTabTimes->setShortcut(QApplication::translate("MainWindow", "Ctrl+T", nullptr));
#endif // QT_NO_SHORTCUT
        actionLUTCLAHE->setText(QApplication::translate("MainWindow", "CLAHE", nullptr));
#ifndef QT_NO_TOOLTIP
        actionLUTCLAHE->setToolTip(QApplication::translate("MainWindow", "Contrast Limited Adaptive Histogram Equalization", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLUTCLAHE->setShortcut(QApplication::translate("MainWindow", "H", nullptr));
#endif // QT_NO_SHORTCUT
        actionDirectory->setText(QApplication::translate("MainWindow", "Image(s) or Directory", nullptr));
#ifndef QT_NO_SHORTCUT
        actionDirectory->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+O", nullptr));
#endif // QT_NO_SHORTCUT
        groupBoxLUT->setTitle(QApplication::translate("MainWindow", "LUT", nullptr));
        groupBoxHistogram->setTitle(QApplication::translate("MainWindow", "Histogram", nullptr));
        radioButtonOrigSize->setText(QApplication::translate("MainWindow", "Originale", nullptr));
        radioButtonCustomSize->setText(QApplication::translate("MainWindow", "Custom", nullptr));
        labelWidth->setText(QApplication::translate("MainWindow", "Largeur", nullptr));
        labelHeight->setText(QApplication::translate("MainWindow", "Hauteur", nullptr));
        checkBoxFlip->setText(QApplication::translate("MainWindow", "Flip", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(sizeTab), QApplication::translate("MainWindow", "Size", nullptr));
        groupBoxHistChannels->setTitle(QApplication::translate("MainWindow", "Channels", nullptr));
        checkBoxHistRed->setText(QApplication::translate("MainWindow", "Red", nullptr));
        checkBoxHistGreen->setText(QApplication::translate("MainWindow", "Green", nullptr));
        checkBoxHistBlue->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        checkBoxHistGray->setText(QApplication::translate("MainWindow", "Gray", nullptr));
        groupBoxHistMode->setTitle(QApplication::translate("MainWindow", "Hist. mode", nullptr));
        radioButtonHMNormal->setText(QApplication::translate("MainWindow", "Normal", nullptr));
        radioButtonHMCumulative->setText(QApplication::translate("MainWindow", "Cumulative", nullptr));
        radioButtonHMTime->setText(QApplication::translate("MainWindow", "Time Cumul.", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(histTab), QApplication::translate("MainWindow", "Hist", nullptr));
        lutBox->setTitle(QApplication::translate("MainWindow", "Transfert", nullptr));
        radioButtonIdentity->setText(QApplication::translate("MainWindow", "Identity", nullptr));
        radioButtonInverse->setText(QApplication::translate("MainWindow", "Inverse", nullptr));
        radioButtonGamma->setText(QApplication::translate("MainWindow", "Gamma", nullptr));
        radioButtonThreshold->setText(QApplication::translate("MainWindow", "Threshold", nullptr));
        radioButtonDynamic->setText(QApplication::translate("MainWindow", "&Dynamic", nullptr));
        comboBoxDynamicType->setItemText(0, QApplication::translate("MainWindow", "Low", nullptr));
        comboBoxDynamicType->setItemText(1, QApplication::translate("MainWindow", "High", nullptr));
        comboBoxDynamicType->setItemText(2, QApplication::translate("MainWindow", "Low + High", nullptr));

        radioButtonEqualize->setText(QApplication::translate("MainWindow", "Equalize", nullptr));
        radioButtonClahe->setText(QApplication::translate("MainWindow", "CLAHE", nullptr));
        checkBoxUseColors->setText(QApplication::translate("MainWindow", "Use all channels", nullptr));
        percentBox->setTitle(QApplication::translate("MainWindow", "Percent", nullptr));
        groupBoxClahe->setTitle(QApplication::translate("MainWindow", "CLAHE", nullptr));
        labelClipLimit->setText(QApplication::translate("MainWindow", "Clip Limit", nullptr));
        labelTileSize->setText(QApplication::translate("MainWindow", "Tiling", nullptr));
        groupBoxPostProcessing->setTitle(QApplication::translate("MainWindow", "Post-processing", nullptr));
        checkBoxPostFiltering->setText(QApplication::translate("MainWindow", "Median Blur", nullptr));
        labelKernelSize->setText(QApplication::translate("MainWindow", "Kernel", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(lutTab), QApplication::translate("MainWindow", "LUT", nullptr));
        groupBoxHistogram_2->setTitle(QApplication::translate("MainWindow", "Histogram", nullptr));
        labelUH1->setText(QApplication::translate("MainWindow", "Update 1", nullptr));
        labelUH1Time->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelUH2->setText(QApplication::translate("MainWindow", "Update 2", nullptr));
        labelUH2Time->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelDH->setText(QApplication::translate("MainWindow", "Draw", nullptr));
        labelDHTime->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBoxLUT_2->setTitle(QApplication::translate("MainWindow", "LUT", nullptr));
        labelCL->setText(QApplication::translate("MainWindow", "Compute", nullptr));
        labelCLTime->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelAL->setText(QApplication::translate("MainWindow", "Apply", nullptr));
        labelALTime->setText(QApplication::translate("MainWindow", "0", nullptr));
        labelDL->setText(QApplication::translate("MainWindow", "Draw", nullptr));
        labelDLTime->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBoxDL->setTitle(QApplication::translate("MainWindow", "All", nullptr));
        labelAllTime->setText(QApplication::translate("MainWindow", "0", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(timeTab), QApplication::translate("MainWindow", "Times", nullptr));
        menuSources->setTitle(QApplication::translate("MainWindow", "Sources", nullptr));
        menuVideo->setTitle(QApplication::translate("MainWindow", "Video", nullptr));
        menuSize->setTitle(QApplication::translate("MainWindow", "taille", nullptr));
        menuRender->setTitle(QApplication::translate("MainWindow", "Render", nullptr));
        menuHistogram->setTitle(QApplication::translate("MainWindow", "Histogram", nullptr));
        menuChannels->setTitle(QApplication::translate("MainWindow", "Channels", nullptr));
        menuModes->setTitle(QApplication::translate("MainWindow", "Modes", nullptr));
        menuLUT->setTitle(QApplication::translate("MainWindow", "LUT", nullptr));
        menuTabs->setTitle(QApplication::translate("MainWindow", "Tabs", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
