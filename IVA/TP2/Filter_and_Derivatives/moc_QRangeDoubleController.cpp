/****************************************************************************
** Meta object code from reading C++ file 'QRangeDoubleController.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Qcv/controllers/QRangeDoubleController.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QRangeDoubleController.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QRangeDoubleController_t {
    QByteArrayData data[19];
    char stringdata0[186];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QRangeDoubleController_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QRangeDoubleController_t qt_meta_stringdata_QRangeDoubleController = {
    {
QT_MOC_LITERAL(0, 0, 22), // "QRangeDoubleController"
QT_MOC_LITERAL(1, 23, 7), // "updated"
QT_MOC_LITERAL(2, 31, 0), // ""
QT_MOC_LITERAL(3, 32, 12), // "valueChanged"
QT_MOC_LITERAL(4, 45, 5), // "value"
QT_MOC_LITERAL(5, 51, 12), // "indexChanged"
QT_MOC_LITERAL(6, 64, 5), // "index"
QT_MOC_LITERAL(7, 70, 10), // "minChanged"
QT_MOC_LITERAL(8, 81, 10), // "maxChanged"
QT_MOC_LITERAL(9, 92, 12), // "rangeChanged"
QT_MOC_LITERAL(10, 105, 3), // "min"
QT_MOC_LITERAL(11, 109, 3), // "max"
QT_MOC_LITERAL(12, 113, 11), // "stepChanged"
QT_MOC_LITERAL(13, 125, 17), // "indexRangeChanged"
QT_MOC_LITERAL(14, 143, 10), // "setEnabled"
QT_MOC_LITERAL(15, 154, 8), // "setValue"
QT_MOC_LITERAL(16, 163, 8), // "setIndex"
QT_MOC_LITERAL(17, 172, 5), // "reset"
QT_MOC_LITERAL(18, 178, 7) // "refresh"

    },
    "QRangeDoubleController\0updated\0\0"
    "valueChanged\0value\0indexChanged\0index\0"
    "minChanged\0maxChanged\0rangeChanged\0"
    "min\0max\0stepChanged\0indexRangeChanged\0"
    "setEnabled\0setValue\0setIndex\0reset\0"
    "refresh"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QRangeDoubleController[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,
       3,    1,   80,    2, 0x06 /* Public */,
       5,    1,   83,    2, 0x06 /* Public */,
       7,    1,   86,    2, 0x06 /* Public */,
       8,    1,   89,    2, 0x06 /* Public */,
       9,    2,   92,    2, 0x06 /* Public */,
      12,    1,   97,    2, 0x06 /* Public */,
      13,    2,  100,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      14,    1,  105,    2, 0x0a /* Public */,
      15,    1,  108,    2, 0x0a /* Public */,
      16,    1,  111,    2, 0x0a /* Public */,
      17,    0,  114,    2, 0x0a /* Public */,
      18,    0,  115,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,    4,
    QMetaType::Void, QMetaType::Int,    6,
    QMetaType::Void, QMetaType::Double,    4,
    QMetaType::Void, QMetaType::Double,    4,
    QMetaType::Void, QMetaType::Double, QMetaType::Double,   10,   11,
    QMetaType::Void, QMetaType::Double,    4,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   10,   11,

 // slots: parameters
    QMetaType::Void, QMetaType::Bool,    4,
    QMetaType::Bool, QMetaType::Double,    4,
    QMetaType::Bool, QMetaType::Int,    6,
    QMetaType::Bool,
    QMetaType::Void,

       0        // eod
};

void QRangeDoubleController::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QRangeDoubleController *_t = static_cast<QRangeDoubleController *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updated(); break;
        case 1: _t->valueChanged((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 2: _t->indexChanged((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 3: _t->minChanged((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 4: _t->maxChanged((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 5: _t->rangeChanged((*reinterpret_cast< const double(*)>(_a[1])),(*reinterpret_cast< const double(*)>(_a[2]))); break;
        case 6: _t->stepChanged((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 7: _t->indexRangeChanged((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const int(*)>(_a[2]))); break;
        case 8: _t->setEnabled((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 9: { bool _r = _t->setValue((*reinterpret_cast< const double(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 10: { bool _r = _t->setIndex((*reinterpret_cast< const int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 11: { bool _r = _t->reset();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 12: _t->refresh(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QRangeDoubleController::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeDoubleController::updated)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QRangeDoubleController::*_t)(const double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeDoubleController::valueChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QRangeDoubleController::*_t)(const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeDoubleController::indexChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QRangeDoubleController::*_t)(const double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeDoubleController::minChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QRangeDoubleController::*_t)(const double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeDoubleController::maxChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QRangeDoubleController::*_t)(const double , const double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeDoubleController::rangeChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QRangeDoubleController::*_t)(const double );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeDoubleController::stepChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QRangeDoubleController::*_t)(const int , const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QRangeDoubleController::indexRangeChanged)) {
                *result = 7;
                return;
            }
        }
    }
}

const QMetaObject QRangeDoubleController::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QRangeDoubleController.data,
      qt_meta_data_QRangeDoubleController,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QRangeDoubleController::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QRangeDoubleController::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QRangeDoubleController.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "QRangeAbstractController<double>"))
        return static_cast< QRangeAbstractController<double>*>(this);
    return QObject::qt_metacast(_clname);
}

int QRangeDoubleController::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void QRangeDoubleController::updated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QRangeDoubleController::valueChanged(const double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QRangeDoubleController::indexChanged(const int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QRangeDoubleController::minChanged(const double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QRangeDoubleController::maxChanged(const double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QRangeDoubleController::rangeChanged(const double _t1, const double _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QRangeDoubleController::stepChanged(const double _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QRangeDoubleController::indexRangeChanged(const int _t1, const int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
