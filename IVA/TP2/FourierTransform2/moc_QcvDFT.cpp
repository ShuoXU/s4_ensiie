/****************************************************************************
** Meta object code from reading C++ file 'QcvDFT.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "QcvDFT.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QcvDFT.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QcvDFT_t {
    QByteArrayData data[13];
    char stringdata0[166];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QcvDFT_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QcvDFT_t qt_meta_stringdata_QcvDFT = {
    {
QT_MOC_LITERAL(0, 0, 6), // "QcvDFT"
QT_MOC_LITERAL(1, 7, 18), // "squareImageChanged"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 4), // "Mat*"
QT_MOC_LITERAL(4, 32, 5), // "image"
QT_MOC_LITERAL(5, 38, 20), // "spectrumImageChanged"
QT_MOC_LITERAL(6, 59, 19), // "inverseImageChanged"
QT_MOC_LITERAL(7, 79, 21), // "optimalDFTSizeChanged"
QT_MOC_LITERAL(8, 101, 4), // "size"
QT_MOC_LITERAL(9, 106, 27), // "allChannelsAvailableChanged"
QT_MOC_LITERAL(10, 134, 9), // "available"
QT_MOC_LITERAL(11, 144, 6), // "update"
QT_MOC_LITERAL(12, 151, 14) // "setSourceImage"

    },
    "QcvDFT\0squareImageChanged\0\0Mat*\0image\0"
    "spectrumImageChanged\0inverseImageChanged\0"
    "optimalDFTSizeChanged\0size\0"
    "allChannelsAvailableChanged\0available\0"
    "update\0setSourceImage"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QcvDFT[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   49,    2, 0x06 /* Public */,
       5,    1,   52,    2, 0x06 /* Public */,
       6,    1,   55,    2, 0x06 /* Public */,
       7,    1,   58,    2, 0x06 /* Public */,
       9,    1,   61,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      11,    0,   64,    2, 0x0a /* Public */,
      12,    1,   65,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::Bool,   10,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 3,    4,

       0        // eod
};

void QcvDFT::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QcvDFT *_t = static_cast<QcvDFT *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->squareImageChanged((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        case 1: _t->spectrumImageChanged((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        case 2: _t->inverseImageChanged((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        case 3: _t->optimalDFTSizeChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->allChannelsAvailableChanged((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 5: _t->update(); break;
        case 6: _t->setSourceImage((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QcvDFT::*_t)(Mat * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDFT::squareImageChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QcvDFT::*_t)(Mat * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDFT::spectrumImageChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QcvDFT::*_t)(Mat * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDFT::inverseImageChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QcvDFT::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDFT::optimalDFTSizeChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QcvDFT::*_t)(const bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDFT::allChannelsAvailableChanged)) {
                *result = 4;
                return;
            }
        }
    }
}

const QMetaObject QcvDFT::staticMetaObject = {
    { &QcvProcessor::staticMetaObject, qt_meta_stringdata_QcvDFT.data,
      qt_meta_data_QcvDFT,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QcvDFT::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QcvDFT::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QcvDFT.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "CvDFT"))
        return static_cast< CvDFT*>(this);
    return QcvProcessor::qt_metacast(_clname);
}

int QcvDFT::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QcvProcessor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void QcvDFT::squareImageChanged(Mat * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QcvDFT::spectrumImageChanged(Mat * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QcvDFT::inverseImageChanged(Mat * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QcvDFT::optimalDFTSizeChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QcvDFT::allChannelsAvailableChanged(const bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
