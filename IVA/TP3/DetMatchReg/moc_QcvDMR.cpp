/****************************************************************************
** Meta object code from reading C++ file 'QcvDMR.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "QcvDMR.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QcvDMR.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QcvDMR_t {
    QByteArrayData data[88];
    char stringdata0[1483];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QcvDMR_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QcvDMR_t qt_meta_stringdata_QcvDMR = {
    {
QT_MOC_LITERAL(0, 0, 6), // "QcvDMR"
QT_MOC_LITERAL(1, 7, 12), // "modelUpdated"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 17), // "modelImageChanged"
QT_MOC_LITERAL(4, 39, 4), // "Mat*"
QT_MOC_LITERAL(5, 44, 5), // "image"
QT_MOC_LITERAL(6, 50, 26), // "modelKeypointNumberChanged"
QT_MOC_LITERAL(7, 77, 6), // "number"
QT_MOC_LITERAL(8, 84, 32), // "updateSceneKeypointsNumberString"
QT_MOC_LITERAL(9, 117, 12), // "numberString"
QT_MOC_LITERAL(10, 130, 27), // "sceneKeypointsNumberChanged"
QT_MOC_LITERAL(11, 158, 37), // "sceneKeypointsVariationPercen..."
QT_MOC_LITERAL(12, 196, 25), // "updateDetectionTimeString"
QT_MOC_LITERAL(13, 222, 26), // "updateExtractionTimeString"
QT_MOC_LITERAL(14, 249, 24), // "updateMatchingTimeString"
QT_MOC_LITERAL(15, 274, 28), // "updateRegistrationTimeString"
QT_MOC_LITERAL(16, 303, 31), // "updateMatchedPointsNumberString"
QT_MOC_LITERAL(17, 335, 29), // "updateMeanMatchDistanceString"
QT_MOC_LITERAL(18, 365, 31), // "updateMinMaxMatchDistanceString"
QT_MOC_LITERAL(19, 397, 28), // "updateMaxMatchDistanceString"
QT_MOC_LITERAL(20, 426, 28), // "updateStdMatchDistanceString"
QT_MOC_LITERAL(21, 455, 29), // "updateReprojectionErrorString"
QT_MOC_LITERAL(22, 485, 25), // "updateInliersNumberString"
QT_MOC_LITERAL(23, 511, 23), // "recognitionStateChanged"
QT_MOC_LITERAL(24, 535, 13), // "recogStateInt"
QT_MOC_LITERAL(25, 549, 6), // "update"
QT_MOC_LITERAL(26, 556, 14), // "setSourceImage"
QT_MOC_LITERAL(27, 571, 16), // "updateModelImage"
QT_MOC_LITERAL(28, 588, 6), // "string"
QT_MOC_LITERAL(29, 595, 8), // "filename"
QT_MOC_LITERAL(30, 604, 19), // "setCameraParameters"
QT_MOC_LITERAL(31, 624, 14), // "setFeatureType"
QT_MOC_LITERAL(32, 639, 23), // "CvDetector::FeatureType"
QT_MOC_LITERAL(33, 663, 11), // "featureType"
QT_MOC_LITERAL(34, 675, 26), // "setDescriptorExtractorType"
QT_MOC_LITERAL(35, 702, 35), // "CvDetector::DescriptorExtract..."
QT_MOC_LITERAL(36, 738, 14), // "descriptorType"
QT_MOC_LITERAL(37, 753, 12), // "checkMatcher"
QT_MOC_LITERAL(38, 766, 22), // "setModelDetectorUpdate"
QT_MOC_LITERAL(39, 789, 14), // "setMatcherType"
QT_MOC_LITERAL(40, 804, 22), // "CvMatcher::MatcherType"
QT_MOC_LITERAL(41, 827, 11), // "matcherType"
QT_MOC_LITERAL(42, 839, 16), // "checkDescriptors"
QT_MOC_LITERAL(43, 856, 14), // "setMatcherMode"
QT_MOC_LITERAL(44, 871, 20), // "CvMatcher::MatchType"
QT_MOC_LITERAL(45, 892, 11), // "matcherMode"
QT_MOC_LITERAL(46, 904, 6), // "setKnn"
QT_MOC_LITERAL(47, 911, 3), // "knn"
QT_MOC_LITERAL(48, 915, 18), // "setRadiusThreshold"
QT_MOC_LITERAL(49, 934, 15), // "radiusThreshold"
QT_MOC_LITERAL(50, 950, 18), // "setReprojThreshold"
QT_MOC_LITERAL(51, 969, 15), // "reprojThreshold"
QT_MOC_LITERAL(52, 985, 16), // "setPnPConfidence"
QT_MOC_LITERAL(53, 1002, 13), // "pnpConfidence"
QT_MOC_LITERAL(54, 1016, 12), // "setPrintSize"
QT_MOC_LITERAL(55, 1029, 9), // "printSize"
QT_MOC_LITERAL(56, 1039, 14), // "setComputePose"
QT_MOC_LITERAL(57, 1054, 11), // "computePose"
QT_MOC_LITERAL(58, 1066, 22), // "setPoseComputingMethod"
QT_MOC_LITERAL(59, 1089, 19), // "poseComputingMethod"
QT_MOC_LITERAL(60, 1109, 22), // "setRansacPnPIterations"
QT_MOC_LITERAL(61, 1132, 19), // "ransacPnPIterations"
QT_MOC_LITERAL(62, 1152, 12), // "setRansacPnP"
QT_MOC_LITERAL(63, 1165, 12), // "useRansacPnP"
QT_MOC_LITERAL(64, 1178, 18), // "setUsePreviousPose"
QT_MOC_LITERAL(65, 1197, 3), // "use"
QT_MOC_LITERAL(66, 1201, 12), // "setDetecting"
QT_MOC_LITERAL(67, 1214, 9), // "detecting"
QT_MOC_LITERAL(68, 1224, 11), // "setMatching"
QT_MOC_LITERAL(69, 1236, 8), // "matching"
QT_MOC_LITERAL(70, 1245, 14), // "setRegistering"
QT_MOC_LITERAL(71, 1260, 11), // "registering"
QT_MOC_LITERAL(72, 1272, 15), // "setVerboseLevel"
QT_MOC_LITERAL(73, 1288, 12), // "VerboseLevel"
QT_MOC_LITERAL(74, 1301, 5), // "level"
QT_MOC_LITERAL(75, 1307, 16), // "setKeyPointsMode"
QT_MOC_LITERAL(76, 1324, 12), // "KeyPointShow"
QT_MOC_LITERAL(77, 1337, 4), // "mode"
QT_MOC_LITERAL(78, 1342, 17), // "setShowModelFrame"
QT_MOC_LITERAL(79, 1360, 14), // "showModelFrame"
QT_MOC_LITERAL(80, 1375, 15), // "setShowModelBox"
QT_MOC_LITERAL(81, 1391, 12), // "showModelBox"
QT_MOC_LITERAL(82, 1404, 12), // "setBoxHeight"
QT_MOC_LITERAL(83, 1417, 6), // "height"
QT_MOC_LITERAL(84, 1424, 17), // "setTimePerFeature"
QT_MOC_LITERAL(85, 1442, 5), // "value"
QT_MOC_LITERAL(86, 1448, 13), // "setRecordPose"
QT_MOC_LITERAL(87, 1462, 20) // "resetMeanProcessTime"

    },
    "QcvDMR\0modelUpdated\0\0modelImageChanged\0"
    "Mat*\0image\0modelKeypointNumberChanged\0"
    "number\0updateSceneKeypointsNumberString\0"
    "numberString\0sceneKeypointsNumberChanged\0"
    "sceneKeypointsVariationPercentChanged\0"
    "updateDetectionTimeString\0"
    "updateExtractionTimeString\0"
    "updateMatchingTimeString\0"
    "updateRegistrationTimeString\0"
    "updateMatchedPointsNumberString\0"
    "updateMeanMatchDistanceString\0"
    "updateMinMaxMatchDistanceString\0"
    "updateMaxMatchDistanceString\0"
    "updateStdMatchDistanceString\0"
    "updateReprojectionErrorString\0"
    "updateInliersNumberString\0"
    "recognitionStateChanged\0recogStateInt\0"
    "update\0setSourceImage\0updateModelImage\0"
    "string\0filename\0setCameraParameters\0"
    "setFeatureType\0CvDetector::FeatureType\0"
    "featureType\0setDescriptorExtractorType\0"
    "CvDetector::DescriptorExtractorType\0"
    "descriptorType\0checkMatcher\0"
    "setModelDetectorUpdate\0setMatcherType\0"
    "CvMatcher::MatcherType\0matcherType\0"
    "checkDescriptors\0setMatcherMode\0"
    "CvMatcher::MatchType\0matcherMode\0"
    "setKnn\0knn\0setRadiusThreshold\0"
    "radiusThreshold\0setReprojThreshold\0"
    "reprojThreshold\0setPnPConfidence\0"
    "pnpConfidence\0setPrintSize\0printSize\0"
    "setComputePose\0computePose\0"
    "setPoseComputingMethod\0poseComputingMethod\0"
    "setRansacPnPIterations\0ransacPnPIterations\0"
    "setRansacPnP\0useRansacPnP\0setUsePreviousPose\0"
    "use\0setDetecting\0detecting\0setMatching\0"
    "matching\0setRegistering\0registering\0"
    "setVerboseLevel\0VerboseLevel\0level\0"
    "setKeyPointsMode\0KeyPointShow\0mode\0"
    "setShowModelFrame\0showModelFrame\0"
    "setShowModelBox\0showModelBox\0setBoxHeight\0"
    "height\0setTimePerFeature\0value\0"
    "setRecordPose\0resetMeanProcessTime"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QcvDMR[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      50,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      18,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  264,    2, 0x06 /* Public */,
       3,    1,  265,    2, 0x06 /* Public */,
       6,    1,  268,    2, 0x06 /* Public */,
       8,    1,  271,    2, 0x06 /* Public */,
      10,    1,  274,    2, 0x06 /* Public */,
      11,    1,  277,    2, 0x06 /* Public */,
      12,    1,  280,    2, 0x06 /* Public */,
      13,    1,  283,    2, 0x06 /* Public */,
      14,    1,  286,    2, 0x06 /* Public */,
      15,    1,  289,    2, 0x06 /* Public */,
      16,    1,  292,    2, 0x06 /* Public */,
      17,    1,  295,    2, 0x06 /* Public */,
      18,    1,  298,    2, 0x06 /* Public */,
      19,    1,  301,    2, 0x06 /* Public */,
      20,    1,  304,    2, 0x06 /* Public */,
      21,    1,  307,    2, 0x06 /* Public */,
      22,    1,  310,    2, 0x06 /* Public */,
      23,    1,  313,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      25,    0,  316,    2, 0x0a /* Public */,
      26,    1,  317,    2, 0x0a /* Public */,
      27,    1,  320,    2, 0x0a /* Public */,
      30,    1,  323,    2, 0x0a /* Public */,
      31,    1,  326,    2, 0x0a /* Public */,
      34,    2,  329,    2, 0x0a /* Public */,
      34,    1,  334,    2, 0x2a /* Public | MethodCloned */,
      38,    0,  337,    2, 0x0a /* Public */,
      39,    2,  338,    2, 0x0a /* Public */,
      39,    1,  343,    2, 0x2a /* Public | MethodCloned */,
      43,    1,  346,    2, 0x0a /* Public */,
      46,    1,  349,    2, 0x0a /* Public */,
      48,    1,  352,    2, 0x0a /* Public */,
      50,    1,  355,    2, 0x0a /* Public */,
      52,    1,  358,    2, 0x0a /* Public */,
      54,    1,  361,    2, 0x0a /* Public */,
      56,    1,  364,    2, 0x0a /* Public */,
      58,    1,  367,    2, 0x0a /* Public */,
      60,    1,  370,    2, 0x0a /* Public */,
      62,    1,  373,    2, 0x0a /* Public */,
      64,    1,  376,    2, 0x0a /* Public */,
      66,    1,  379,    2, 0x0a /* Public */,
      68,    1,  382,    2, 0x0a /* Public */,
      70,    1,  385,    2, 0x0a /* Public */,
      72,    1,  388,    2, 0x0a /* Public */,
      75,    1,  391,    2, 0x0a /* Public */,
      78,    1,  394,    2, 0x0a /* Public */,
      80,    1,  397,    2, 0x0a /* Public */,
      82,    1,  400,    2, 0x0a /* Public */,
      84,    1,  403,    2, 0x0a /* Public */,
      86,    1,  406,    2, 0x0a /* Public */,
      87,    0,  409,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::Int,    7,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::Int,   24,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void, 0x80000000 | 28,   29,
    QMetaType::Void, 0x80000000 | 32,   33,
    QMetaType::Void, 0x80000000 | 35, QMetaType::Bool,   36,   37,
    QMetaType::Void, 0x80000000 | 35,   36,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 40, QMetaType::Bool,   41,   42,
    QMetaType::Void, 0x80000000 | 40,   41,
    QMetaType::Void, 0x80000000 | 44,   45,
    QMetaType::Void, QMetaType::Int,   47,
    QMetaType::Void, QMetaType::Double,   49,
    QMetaType::Void, QMetaType::Double,   51,
    QMetaType::Void, QMetaType::Double,   53,
    QMetaType::Bool, QMetaType::Double,   55,
    QMetaType::Bool, QMetaType::Bool,   57,
    QMetaType::Void, QMetaType::Int,   59,
    QMetaType::Void, QMetaType::Int,   61,
    QMetaType::Void, QMetaType::Bool,   63,
    QMetaType::Void, QMetaType::Bool,   65,
    QMetaType::Void, QMetaType::Bool,   67,
    QMetaType::Bool, QMetaType::Bool,   69,
    QMetaType::Bool, QMetaType::Bool,   71,
    QMetaType::Void, 0x80000000 | 73,   74,
    QMetaType::Void, 0x80000000 | 76,   77,
    QMetaType::Bool, QMetaType::Bool,   79,
    QMetaType::Bool, QMetaType::Bool,   81,
    QMetaType::Void, QMetaType::Double,   83,
    QMetaType::Void, QMetaType::Bool,   85,
    QMetaType::Void, QMetaType::Bool,   85,
    QMetaType::Void,

       0        // eod
};

void QcvDMR::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QcvDMR *_t = static_cast<QcvDMR *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->modelUpdated(); break;
        case 1: _t->modelImageChanged((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        case 2: _t->modelKeypointNumberChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->updateSceneKeypointsNumberString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->sceneKeypointsNumberChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->sceneKeypointsVariationPercentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->updateDetectionTimeString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->updateExtractionTimeString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->updateMatchingTimeString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->updateRegistrationTimeString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->updateMatchedPointsNumberString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 11: _t->updateMeanMatchDistanceString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: _t->updateMinMaxMatchDistanceString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: _t->updateMaxMatchDistanceString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: _t->updateStdMatchDistanceString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->updateReprojectionErrorString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: _t->updateInliersNumberString((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 17: _t->recognitionStateChanged((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 18: _t->update(); break;
        case 19: _t->setSourceImage((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        case 20: _t->updateModelImage((*reinterpret_cast< const string(*)>(_a[1]))); break;
        case 21: _t->setCameraParameters((*reinterpret_cast< const string(*)>(_a[1]))); break;
        case 22: _t->setFeatureType((*reinterpret_cast< const CvDetector::FeatureType(*)>(_a[1]))); break;
        case 23: _t->setDescriptorExtractorType((*reinterpret_cast< const CvDetector::DescriptorExtractorType(*)>(_a[1])),(*reinterpret_cast< const bool(*)>(_a[2]))); break;
        case 24: _t->setDescriptorExtractorType((*reinterpret_cast< const CvDetector::DescriptorExtractorType(*)>(_a[1]))); break;
        case 25: _t->setModelDetectorUpdate(); break;
        case 26: _t->setMatcherType((*reinterpret_cast< const CvMatcher::MatcherType(*)>(_a[1])),(*reinterpret_cast< const bool(*)>(_a[2]))); break;
        case 27: _t->setMatcherType((*reinterpret_cast< const CvMatcher::MatcherType(*)>(_a[1]))); break;
        case 28: _t->setMatcherMode((*reinterpret_cast< const CvMatcher::MatchType(*)>(_a[1]))); break;
        case 29: _t->setKnn((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 30: _t->setRadiusThreshold((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 31: _t->setReprojThreshold((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 32: _t->setPnPConfidence((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 33: { bool _r = _t->setPrintSize((*reinterpret_cast< const double(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 34: { bool _r = _t->setComputePose((*reinterpret_cast< const bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 35: _t->setPoseComputingMethod((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 36: _t->setRansacPnPIterations((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 37: _t->setRansacPnP((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 38: _t->setUsePreviousPose((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 39: _t->setDetecting((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 40: { bool _r = _t->setMatching((*reinterpret_cast< const bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 41: { bool _r = _t->setRegistering((*reinterpret_cast< const bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 42: _t->setVerboseLevel((*reinterpret_cast< const VerboseLevel(*)>(_a[1]))); break;
        case 43: _t->setKeyPointsMode((*reinterpret_cast< const KeyPointShow(*)>(_a[1]))); break;
        case 44: { bool _r = _t->setShowModelFrame((*reinterpret_cast< const bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 45: { bool _r = _t->setShowModelBox((*reinterpret_cast< const bool(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 46: _t->setBoxHeight((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 47: _t->setTimePerFeature((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 48: _t->setRecordPose((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 49: _t->resetMeanProcessTime(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QcvDMR::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::modelUpdated)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(Mat * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::modelImageChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::modelKeypointNumberChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateSceneKeypointsNumberString)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::sceneKeypointsNumberChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::sceneKeypointsVariationPercentChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateDetectionTimeString)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateExtractionTimeString)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateMatchingTimeString)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateRegistrationTimeString)) {
                *result = 9;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateMatchedPointsNumberString)) {
                *result = 10;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateMeanMatchDistanceString)) {
                *result = 11;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateMinMaxMatchDistanceString)) {
                *result = 12;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateMaxMatchDistanceString)) {
                *result = 13;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateStdMatchDistanceString)) {
                *result = 14;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateReprojectionErrorString)) {
                *result = 15;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::updateInliersNumberString)) {
                *result = 16;
                return;
            }
        }
        {
            typedef void (QcvDMR::*_t)(const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvDMR::recognitionStateChanged)) {
                *result = 17;
                return;
            }
        }
    }
}

const QMetaObject QcvDMR::staticMetaObject = {
    { &QcvProcessor::staticMetaObject, qt_meta_stringdata_QcvDMR.data,
      qt_meta_data_QcvDMR,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QcvDMR::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QcvDMR::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QcvDMR.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "CvDMR"))
        return static_cast< CvDMR*>(this);
    return QcvProcessor::qt_metacast(_clname);
}

int QcvDMR::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QcvProcessor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 50)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 50;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 50)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 50;
    }
    return _id;
}

// SIGNAL 0
void QcvDMR::modelUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QcvDMR::modelImageChanged(Mat * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QcvDMR::modelKeypointNumberChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void QcvDMR::updateSceneKeypointsNumberString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QcvDMR::sceneKeypointsNumberChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QcvDMR::sceneKeypointsVariationPercentChanged(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QcvDMR::updateDetectionTimeString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QcvDMR::updateExtractionTimeString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QcvDMR::updateMatchingTimeString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QcvDMR::updateRegistrationTimeString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void QcvDMR::updateMatchedPointsNumberString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void QcvDMR::updateMeanMatchDistanceString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 11, _a);
}

// SIGNAL 12
void QcvDMR::updateMinMaxMatchDistanceString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 12, _a);
}

// SIGNAL 13
void QcvDMR::updateMaxMatchDistanceString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void QcvDMR::updateStdMatchDistanceString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}

// SIGNAL 15
void QcvDMR::updateReprojectionErrorString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 15, _a);
}

// SIGNAL 16
void QcvDMR::updateInliersNumberString(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 16, _a);
}

// SIGNAL 17
void QcvDMR::recognitionStateChanged(const int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 17, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
