#include <QDebug>

#include "QcvAlgoParam.h"

#include "QcvAlgoParamEnumWidget.h"
#include "ui_QcvAlgoParamEnumWidget.h"

/*
 * Constructor
 * @param param the associated parameter
 * @param direct direct operation: if true indicates that parameter
 * value setting is performed directly through
 * param->setIndexValue(...) which is blocking in
 * multithread environments. If false indicates that parameter value
 * setting is performed through signal/slot mechanism, which in
 * multithreaded environments is non blocking with queued connections.
 * @param settings the settings to use to set initial value and/or
 * save the current value (if different from default value) to settings
 * @param settingsName the name to use when reading or writing the
 * parameter value to settings
 * @param parent the parent widget
 */
QcvAlgoParamEnumWidget::QcvAlgoParamEnumWidget(CvAlgoParam * param,
											   const bool direct,
											   QSettings * settings,
											   const QString & settingsName,
											   QWidget *parent) :
	QcvAlgoParamWidget(param, direct, settings, settingsName, parent),
	ui(new Ui::QcvAlgoParamEnumWidget)
{
//	qDebug() << this << "->" << Q_FUNC_INFO;

	ui->setupUi(this);

	assert(parameter->isEnum());

	// If settings are available then try to read value from settings
	readSetting();

	// Setup title
	ui->label->setText(QString::fromStdString(parameter->getName()));

	// Setup tooltip
	ui->label->setToolTip(QString::fromStdString(parameter->getDescription()));

	// Setup combobox
	ui->comboBox->blockSignals(true);

	vector<string> valueDescriptions = parameter->getValueDescriptions();
	for (size_t i = 0; i < valueDescriptions.size(); i++)
	{
		ui->comboBox->addItem(QString::fromStdString(valueDescriptions[i]),
							  QVariant::fromValue<int>(parameter->getIntValueFromIndex(i)));
	}

	// Set currently selected item
	ui->comboBox->setCurrentIndex(parameter->getValueIndex());
	bool settable = parameter->isSettable();
	if (!settable)
	{
		ui->comboBox->setEnabled(false);
		ui->resetButton->setEnabled(false);
	}

	ui->comboBox->blockSignals(false);

	// Setup connections if no direct operations
	if (!directOp && settable)
	{
		QcvAlgoParam * qParameter = dynamic_cast<QcvAlgoParam *>(parameter);
		if (qParameter != nullptr)
		{
			connect(this, SIGNAL(parameterUpdated(int)),
					qParameter, SLOT(setIndexValue(int)));
			connect(this, SIGNAL(parameterReset()),
					qParameter, SLOT(resetToDefaultValue()));
		}
		else
		{
			qDebug() << Q_FUNC_INFO
					 << "unable to convert param"
					 << parameter->getName().c_str()
					 << "to Qt flavored param with signals/slots";
			directOp = true;
		}
	}
}

/*
 * Destructor.
 * Clears the UI.
 */
QcvAlgoParamEnumWidget::~QcvAlgoParamEnumWidget()
{
//	qDebug() << this << "->" << Q_FUNC_INFO;
	delete ui;
	// All signals deconnection is performed in ~QcvAlgoParamWidget()
}

/*
 * Read value from settings (iff #settings is non null and #settingsName
 * is non empty)
 * @return true if a value has been found in settings, and if this value
 * is different from the current value of the parameter and has been set,
 * false otherwise
 */
bool QcvAlgoParamEnumWidget::readSetting()
{
	if ((settings != nullptr) &&
		!settingsName.isEmpty() &&
		!parameter->isConstant())
	{
		QVariant variant = settings->value(settingsName);
		if (variant.isValid())
		{
			int indexPrefValue = variant.toInt();
			if (indexPrefValue != parameter->getValueIndex(CvAlgoParam::WhichValue::VALUE) &&
				indexPrefValue >= parameter->getValueIndex(CvAlgoParam::WhichValue::MINVALUE) &&
				indexPrefValue <= parameter->getValueIndex(CvAlgoParam::WhichValue::MAXVALUE))
			{
//				qDebug() << Q_FUNC_INFO << settingsName << indexPrefValue;
				// Sets value directly into parameter
				parameter->setIndexValue(indexPrefValue);
				return true;
			}
		}
	}
	return false;
}


/*
 * Slot triggered when combobox item has changed
 * @param index the new index of the combobox
 * @note if the parameter is not settable, this method should
 * (theoretically) never be triggered
 * @post the integer value correponding to the new index is set
 * on the parameter and parameterUpdated() signal is emitted.
 */
void QcvAlgoParamEnumWidget::on_comboBox_currentIndexChanged(int index)
{
	if (directOp)
	{
		parameter->setIntValue(ui->comboBox->itemData(index).toInt());
		// or
		// parameter->setIndexValue(index);
	}
	else
	{
		emit parameterUpdated(index);
	}

	emit parameterUpdated();

	// If there is a setting available then set it
	writeSetting(QVariant(index));
}

/*
 * Slot triggered when reset button is clicked.
 * @post the default value index is retreived from paramter and set
 * on the combobox (This implies a call to
 * on_comboBox_currentIndexChanged())
 */
void QcvAlgoParamEnumWidget::on_resetButton_clicked()
{
	int defValIndex = parameter->getValueIndex(CvAlgoParam::WhichValue::DEFAULTVALUE);
	ui->comboBox->blockSignals(true);
	ui->comboBox->setCurrentIndex(defValIndex);
	ui->comboBox->blockSignals(false);

	if (directOp)
	{
		parameter->resetToDefaultValue();
	}
	else
	{
		emit parameterReset();
	}

	emit(parameterUpdated());

	removeSetting();
}
