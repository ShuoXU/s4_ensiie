//#include <iostream>
#include <QDebug>	// for qDebug() <<
#include <QTime>	// for timed debug
#include <QThread>	// for identifying thread in debug output

#include <Qcv/matWidgets/QcvMatWidgetLabel.h>

using namespace std;

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * OpenCV QT Widget default constructor
 * @param parent parent widget
 * @param lock the lock to use on sourceImage in multi-threaded
 * environments
 * @param mouseSense mouse sensivity
 */
QcvMatWidgetLabel::QcvMatWidgetLabel(QWidget * parent,
									 QMutex * lock,
									 MouseSense mouseSense) :
	QcvMatWidget(parent, lock, mouseSense),
	imageLabel(new QLabel())
{
	setup();
}

/*
 * OpenCV QT Widget constructor
 * @param sourceImage the source OpenCV qImage
 * @param parent parent widget
 * @param lock the lock to use on sourceImage in multi-threaded
 * environments
 * @param mouseSense mouse sensivity
 */
QcvMatWidgetLabel::QcvMatWidgetLabel(Mat * sourceImage,
									 QWidget * parent,
									 QMutex * lock,
									 MouseSense mouseSense) :
	QcvMatWidget(sourceImage, parent, lock, mouseSense),
	imageLabel(new QLabel())
{
	setup();
}

/*
 * OpenCV Widget destructor.
 */
QcvMatWidgetLabel::~QcvMatWidgetLabel(void)
{
	delete imageLabel;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

/*
 * Widget setup
 * @pre imageLabel has been allocated
 */
void QcvMatWidgetLabel::setup()
{
	layout->addWidget(imageLabel,0,Qt::AlignCenter);
}

// ----------------------------------------------------------------------------
// Protected Methods
// ----------------------------------------------------------------------------

/*
 * paint event reimplemented to draw content
 * @param event the paint event
 */
void QcvMatWidgetLabel::paintEvent(QPaintEvent * event)
{
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay()
//					   << QThread::currentThreadId()
//					   << "QcvMatWidgetLabel::paintEvent start";

	QcvMatWidget::paintEvent(event);

	if (displayImage.data != nullptr)
	{
		// Builds Qimage from RGB image data
		// and sets image as Label pixmap
		imageLabel->setPixmap(QPixmap::fromImage(QImage((uchar *) displayImage.data,
														displayImage.cols,
														displayImage.rows,
														displayImage.step,
														QImage::Format_RGB888)));
	}
//	else
//	{
//		qWarning("QcvMatWidgetLabel::paintEvent : image.data is NULL");
//	}
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay()
//					   << QThread::currentThreadId()
//					   << "QcvMatWidgetLabel::paintEvent end";

}
