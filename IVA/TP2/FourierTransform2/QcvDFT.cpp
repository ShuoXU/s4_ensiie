/*
 * QcvDFT.cpp
 *
 *  Created on: 22 févr. 2012
 *      Author: davidroussel
 */

#include <functional>	// for greater & less

#include "QcvDFT.h"


/*
 * QcvDFT constructor
 * @param image the source image
 * @param imageLock the mutex for concurrent access to the source image.
 * In order to avoid concurrent access to the same image
 * @param updateThread the thread in which this processor should run
 * @param parent parent QObject
 */
QcvDFT::QcvDFT(Mat *image,
			   QMutex * imageLock,
			   QThread * updateThread,
			   QObject *parent) :
	CvProcessor(image), // <-- virtual base class constructor first
	QcvProcessor(image, imageLock, updateThread, parent),
	CvDFT(image),
	filteringController(&filtering,
						locks[RESULT]),
	filterTypeController(&filterType,
						 CvDFT::BOX_FILTER,
						 CvDFT::NB_FILTERS,
						 filterTypeNames,
						 locks[RESULT]),
	logScaleController(&logScaleFactor,
					   locks[RESULT]),
	linkFilters(false),
	linkFiltersController(&linkFilters,
						  nullptr, // lock is used in setLinkFilters
						  new function<void(const bool)>(std::bind(&QcvDFT::setLinkFilters,
																	 this,
																	 std::placeholders::_1)))
{
	assert(lowPassFilter.size() == 3);
	assert(highPassFilter.size() == 3);

	for (size_t i = 0; i < 3; i++)
	{
		lowPassFilterController.push_back(new QRangeIntController(&lowPassFilter[i],
																  locks[RESULT]));
		highPassFilterController.push_back(new QRangeIntController(&highPassFilter[i],
																   locks[RESULT]));

		/*
		 * Low pass filters should always be above high pass filters
		 */
		if (!lowPassFilterController[i]->addConstraint(highPassFilterController[i],
													   greater<int>()))
		{
			qWarning() << Q_FUNC_INFO
					   << "Can't set constraint between low pass controller"
					   << "& high pass controller[" << i << "]";
		}

		/*
		 * High pass filters should always be below low pass filters
		 */
		if (!highPassFilterController[i]->addConstraint(lowPassFilterController[i],
														less<int>()))
		{
			qWarning() << Q_FUNC_INFO
					   << "Can't set constraint between high pass controller"
					   << "& low pass controller[" << i << "]";
		}

		/*
		 * Every time the source image size changes the ranges of low & high
		 * pass filters is modified outside of the filters controllers so
		 * they must be refreshed with updated values
		 */
		connect(this, SIGNAL(imageSizeChanged()),
				lowPassFilterController[i], SLOT(refresh()));
		connect(this, SIGNAL(imageSizeChanged()),
				highPassFilterController[i], SLOT(refresh()));

		if (i > 0) // Green and Red
		{
			connect(this, SIGNAL(allChannelsAvailableChanged(bool)),
					lowPassFilterController[i], SLOT(setEnabled(bool)));
			connect(this, SIGNAL(allChannelsAvailableChanged(bool)),
					highPassFilterController[i], SLOT(setEnabled(bool)));
		}
	}

	/*
	 * Allow linked filters only when all filters are available
	 */
	connect(this, SIGNAL(allChannelsAvailableChanged(bool)),
			&linkFiltersController, SLOT(setEnabled(bool)));

	/*
	 * Since controllers controls values directly (except linkFiltersController)
	 * controllers should reset the mean process time themselves as setters
	 * from CvDFT are not used anymore.
	 */
	connect(&filteringController, SIGNAL(updated()),
			this, SLOT(resetMeanProcessTime()));
	connect(&filterTypeController, SIGNAL(updated()),
			this, SLOT(resetMeanProcessTime()));
	for (int i = 0; i < 3; i++)
	{
		connect(lowPassFilterController[i], SIGNAL(updated()),
				this, SLOT(resetMeanProcessTime()));
		connect(highPassFilterController[i], SIGNAL(updated()),
				this, SLOT(resetMeanProcessTime()));
	}
}

/*
 * QcvDFT destructor
 */
QcvDFT::~QcvDFT()
{
	message.clear();

	for (size_t i = 0; i < 3; i++)
	{
		delete lowPassFilterController[i];
		delete highPassFilterController[i];
	}

	lowPassFilterController.clear();
	highPassFilterController.clear();

	if (updateThread != nullptr)
	{
		updateThread->wait();
	}
}

/*
 * Accessor for the filtering controller
 * @return the filtering controller pointer
 */
QBoolController * QcvDFT::getFilteringController()
{
	return &filteringController;
}

/*
 * Accessor for the filtering type controller
 * @return the filtering type controller pointer
 */
QEnumController * QcvDFT::getFilterTypeController()
{
	return &filterTypeController;
}

/*
 * Accessor for the log scale controller
 * @return the log scale controller pointer
 */
QRangeDoubleController * QcvDFT::getLogScaleController()
{
	return &logScaleController;
}

/*
 * Accessor for the ith low pas filter controller
 * @return the ith low pas filter controller pointer or null if there
 * is no such controller when index is invalid
 */
QRangeIntController * QcvDFT::getLowPassFilterController(const size_t i)
{
	if (i < lowPassFilterController.size())
	{
		return lowPassFilterController[i];
	}

	return nullptr;
}

/*
 * Accessor for the ith high pas filter controller
 * @return the ith high pas filter controller pointer or null if there
 * is no such controller when index is invalid
 */
QRangeIntController * QcvDFT::getHighPassFilterController(const size_t i)
{
	if (i < highPassFilterController.size())
	{
		return highPassFilterController[i];
	}

	return nullptr;
}

/*
 * Accessor for the linked filters controller
 * @return the linked filters controller pointer
 */
QBoolController * QcvDFT::getLinkFiltersController()
{
	return &linkFiltersController;
}

/*
 * Setter to link or unlink filters together
 * @param value the new linkage status
 */
void QcvDFT::setLinkFilters(const bool value)
{
	for (int i = 0; i < locksSize(); i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->lock();
		}
	}

	linkFilters = value;

	for (int i = 0; i < locksSize(); i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->unlock();
		}
	}

	/*
	 * Caution : Don't use locks below this point since addMirrorController will
	 * trigger a setValue on mirrored controllers which uses the lock
	 */
	if (linkFilters)
	{
		for (size_t i = 1; i < 3; i++)
		{
			lowPassFilterController[0]->addMirrorController(lowPassFilterController[i]);
			lowPassFilterController[i]->setEnabled(false);
			highPassFilterController[0]->addMirrorController(highPassFilterController[i]);
			highPassFilterController[i]->setEnabled(false);
		}
	}
	else
	{
		lowPassFilterController[0]->removeMirrorController();
		highPassFilterController[0]->removeMirrorController();
		for (size_t i = 1; i < 3; i++)
		{
			lowPassFilterController[i]->setEnabled(true);
			highPassFilterController[i]->setEnabled(true);
		}
	}
}

/*
 * Update computed images slot and sends updated signal
 * required
 */
void QcvDFT::update()
{
	// Locks all available locks
	for (int i = 0; i < locksSize(); i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->lock();
		}
	}

	/*
	 * Update DFT images
	 */
	CvDFT::update();

	// Unlocks all available locks
	for (int i = 0; i < locksSize(); i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->unlock();
		}
	}

	/*
	 * emit updated signal
	 */
	QcvProcessor::update();
}

/*
 * Changes source image slot.
 * Attributes needs to be cleaned up then set up again
 * @param image the new source Image
 */
void QcvDFT::setSourceImage(Mat *image)
	throw (CvProcessorException)
{
	Size previousSize(sourceImage->size());
	int previousNbChannels(nbChannels);
	Size previousDftSize(dftSize);

	bool hasSourceLock = locks[SOURCE] != NULL;
	if (hasSourceLock)
	{
		locks[SOURCE]->lock();
	}

	CvProcessor::setSourceImage(image);

	if (hasSourceLock)
	{
		locks[SOURCE]->unlock();
	}

	emit imageChanged(sourceImage);

	emit imageChanged();

	if ((previousSize.width != image->cols) ||
		(previousSize.height != image->rows))
	{
		emit imageSizeChanged();
	}

	if (previousNbChannels != nbChannels)
	{
		emit imageColorsChanged();
		if (nbChannels == 3)
		{
			emit allChannelsAvailableChanged(true);
		}
		else // nbChannels != 3
		{
			/*
			 * If filters are currently linked then unlink them
			 */
			linkFiltersController.setValue(false);
			emit allChannelsAvailableChanged(false);
		}
	}

	emit squareImageChanged(&inFrameSquare);

	emit spectrumImageChanged(&spectrumMagnitudeImage);

	emit inverseImageChanged(&inverseImage);

	if ((previousDftSize.width != dftSize.width) ||
		(previousDftSize.height != dftSize.height))
	{
		emit imageSizeChanged();
		emit sendMessage("Optimal DFT size = " + QString::number(optimalDFTSize));
		emit optimalDFTSizeChanged(optimalDFTSize);
	}

	// Force update
	// update();
}
