/*
 * QEnumController.h
 *
 *  Created on: 17 juin 2015
 *      Author: davidroussel
 */

#ifndef QENUMCONTROLLER_H_
#define QENUMCONTROLLER_H_

#include <cassert>

#include <QObject>
#include <QStringList>
#include <QComboBox>
#include <QRadioButton>
#include <QMenu>
#include <QAction>
#include <QSignalMapper>

#include <Qcv/controllers/QAbstractController.h>
#include <Qcv/controllers/QBoolController.h>	// to interact with boolean controllers
#include <Qcv/controllers/QRangeDoubleController.h>	// to interact with double controllers
#include <Qcv/controllers/QRangeIntController.h>	// to interact with integer controllers

/**
 * Controller of an enum parameter that can be connected to:
 * 	- a QComboBox
 * 	- a set of QRadioButton within another widget (since radio buttons are
 * 	mutually exclusives within another widget)
 * @note Please note that the enum used in the contructor must be int based type
 */
class QEnumController: public QObject, public QAbstractController<int>
{
	Q_OBJECT

	protected:
		/**
		 * Minimum value of this enum (converted in int)
		 */
		const int minValue;

		/**
		 * number of elements in the enum
		 */
		const int nbElements;

		/**
		 * Set of enumerators descriptions (in order to setup the connected UI)
		 */
		QStringList descriptions;

		/**
		 * List of QRadioButton representing the different elements of the enum
		 * There could be several sets of radioButtons in this list
		 */
		QList<QRadioButton *> radioButtons;

		/**
		 * List of QActions representing the different elements of the enum
		 * There could be several sets of qactions in this list
		 */
		QList<QAction *> actions;

		/**
		 * Signal mappers to map signals from multiple radiobuttons and/or
		 * actions to a single setValue slot
		 */
		QList<QSignalMapper *> signalMappers;

		/**
		 * Map to unable / disable specific controllers or widgets when a
		 * specific value is set on this QEnumController
		 * @note for now only QBoolController, QEnumController,
		 * QRangeDoubleController and QRangeIntController as well as QWidgets
		 * are supported as enabled objects
		 */
		QMultiMap<int, QObject *> enabledObjects;

	public:
		/**
		 * Constructor from enum values
		 * @tparam E the enum type
		 * @param parameter the adress of the managed parameter
		 * @param minValue the minimum value of this enum
		 * @param nbElements the number of elements in the enum
		 * @param descriptions the descriptions strings for the enumerators
		 * @param lock the lock (from the processor containing the RangeValue)
		 * to use when modifying the boolean parameter
		 * @param parent the parent object
		 * @param settings the settings to use to set initial value and/or
		 * save the current value (if different from default value) to settings
		 * @param settingsName the name to use when reading or writing the
		 * parameter value to settings
		 * @note This template constructor needs to be implemented in the header
		 * so it can be instantiated with a concrete enum type in any source
		 * file (.cpp) that needs a specific instantiation
		 */
		template <typename E>
		QEnumController(E * const parameter,
						const E minValue,
						const E nbElements,
						const QStringList & descriptions,
						QMutex * const lock = nullptr,
						function<void(const int)> * const callback = nullptr,
						QObject * parent = nullptr,
						QSettings * settings = nullptr,
						const QString & settingsName = QString()) :
			QObject(parent),
			// this is why enum must have a base type int vvvvvv
			QAbstractController<int, int>((int * const)parameter,
										  lock,
										  callback,
										  settings,
										  settingsName),
			minValue(static_cast<const int>(minValue)),
			nbElements(static_cast<const int>(nbElements)),
			descriptions(descriptions)
		{
			assert (*managedValue >= this->minValue);
			assert (*managedValue < this->minValue + this->nbElements);
			assert (descriptions.size() == this->nbElements);

			if (settings != nullptr && settingsName.size() > 0)
			{
				// read value from preferences
				QVariant variant = settings->value(settingsName);
				if (variant.isValid())
				{
					// set value with regular slot
					int prefIndexValue = variant.toInt();
					if ((prefIndexValue != *managedValue + this->minValue) &&
						(prefIndexValue >= 0) &&
						(prefIndexValue < this->nbElements))
					{
						setValue(prefIndexValue);
					}
				}
			}
		}

		/**
		 * Copy constructor
		 * @param ec the QEnumController to copy
		 */
		QEnumController(const QEnumController & ec);

		/**
		 * Move constructor
		 * @param ec the QEnumController to copy
		 */
		QEnumController(QEnumController && ec);

		/**
		 * Destructor
		 */
		virtual ~QEnumController();

		/**
		 * Setup one or several sources to reflect the managed value.
		 * In this case it could be a set of QRadioButtons or a QComboBox
		 * @param source the source to setup (in the case of a QComboBox) or
		 * the widget containing the sources to setup (we'll have to search for
		 * QRadioButtons inside this source)
		 * @return true if the combo box have been correctly setup or if
		 * the right number of radio buttons or checkable actions have been
		 * found and set up, false otherwise
		 */
		bool setupSource(QObject * source);

		/**
		 * Setup a combobox with (#nbElements) elements extracted
		 * from #descriptions and set the currently selected item to #managedValue
		 * @param comboBox the combobox to setup
		 * @return true if the combox have been correcty set up, false
		 * otherwise.
		 */
		bool setupComboBox(QComboBox * comboBox);

		/**
		 * Search for (#nbElements) radio buttons in this widget and setup
		 * these radio buttons with #descriptions
		 * @param widget the widget (potentially) containing radio buttons to
		 * set up
		 * @return true if the widget contained the same number of radio buttons
		 * as the #descriptions size and they all have been correctly set up.
		 */
		bool setupRadioButtons(QWidget * widget);

		/**
		 * Setup a menu with (#nbElements) elements extracted from #descriptions
		 * and set the currently selected item to #managedValue
		 * @param menu the menu to setup
		 * @return true if the menu have been correctly set up, false otherwise
		 */
		bool setupMenu(QMenu * menu);

		/**
		 * Enable / Disable a specific controller when a specific value is
		 * set on this controller.
		 * @tparam E the Value linked to the enabling of a specific controller
		 * @param controller the controller or widget to enable / disable when
		 * a specific value is set in this controller
		 * @param value the value set in this controller which leads to
		 * enable / disable the argument controller
		 */
		template <typename E>
		void enableWith(QObject * object, const E value)
		{
			int intValue = static_cast<int>(value);

			if (!enabledObjects.contains(intValue, object) && object != nullptr)
			{
				QBoolController * bController = dynamic_cast<QBoolController *>(object);
				QEnumController * eController = dynamic_cast<QEnumController *>(object);
				QRangeDoubleController * rdController = dynamic_cast<QRangeDoubleController *>(object);
				QRangeIntController * riController = dynamic_cast<QRangeIntController *>(object);
				QWidget * widget = dynamic_cast<QWidget *>(object);
				if (bController != nullptr ||
					eController != nullptr ||
					rdController != nullptr ||
					riController != nullptr ||
					widget != nullptr)
				{
					enabledObjects.insert(intValue, object);
				}
				if (bController != nullptr)
				{
					bController->setEnabled((*managedValue + this->minValue) == intValue);
				}
				if (eController != nullptr)
				{
					eController->setEnabled((*managedValue + this->minValue) == intValue);
				}
				if (rdController != nullptr)
				{
					rdController->setEnabled((*managedValue + this->minValue) == intValue);
				}
				if (riController != nullptr)
				{
					riController->setEnabled((*managedValue + this->minValue) == intValue);
				}
				if (widget != nullptr)
				{
					widget->setEnabled((*managedValue + this->minValue) == intValue);
				}
			}
		}

	public slots:
		/**
		 * Slot to change the #enabled state of GUI elements connected to this
		 * controller
		 * @param value the new enabled state
		 */
		void setEnabled(const bool value);

		/**
		 * Sets new value in the int managed value by providing a new index
		 * in the underlying enum.
		 * @param index the index of the value to set
		 * @return true if the index is valid and the value has been set
		 * without correction, false otherwise.
		 * @post The following signals have been emitted:
		 *	#valueChanged(const int &);
		 *	#updated();
		 */
		bool setValue(const int index);

		/**
		 * Set the value corresponding to a description.
		 * @param text the text corresponding to a description
		 * @return true if the description have been found and managedValue
		 * has been changed, false otherwise
		 */
		bool setValue(const QString & text);

		/**
		 * Enable a specific value amongst all enumerated values by enabling its
		 * correspondant widget element (QAction in a QMenu or specific item in
		 * a QComboBox or QRadioButton).
		 * @note By default all enumerated values are enabled.
		 * @param value the value to enable amongst enumerated values
		 * @return true if the corresponding enumerated value has been
		 * enabled, i.e, its correpondant widget or widget element is accessible
		 * again.
		 */
		bool enable(const int & value);

		/**
		 * Disable a specific value amongst all enumerated values by disabling
		 * its correspondant widget element (QAction in a QMenu or specific item
		 * in a QComboBox or QRadioButton).
		 * @note By default all enumerated values are enabled.
		 * @param value the value to disable amongst enumerated values
		 * @return true if the corresponding enumerated value has been
		 * disabled, i.e, its correpondant widget or widget element is no more
		 * accessible.
		 */
		bool disable(const int & value);

	signals:
		/**
		 * Signal to send when anything changes
		 */
		void updated();

		/**
		 * Signal to emit when value has been set
		 * @param value the new value
		 */
		void valueChanged(const int value);

	protected:
		/**
		 * Search for radio buttons corresponding to this index that haven't
		 * been checked yet (in case of multiple sets of radiobuttons connected
		 * to this controller) and check them
		 * @param the index to check
		 * @pre index is supposed to be a valid index
		 * @post All radiobuttons corresponding to this index that haven't been
		 * check yet are checked
		 * @see #radioButtons
		 * @note this method should exclusively used within
		 * #setValue(const int &) between #blockSignalsOnConnectedWidgets() and
		 * #unblockSignalsOnConnectedWidgets();
		 */
		void updateRadioButtons(const int & index);

		/**
		 * Search for actions corresponding to this index that haven't
		 * been checked yet (in case of multiple sets of actions connected
		 * to this controller) and check them
		 * @param the index to check
		 * @pre index is supposed to be a valid index
		 * @post All actions corresponding to this index that haven't been
		 * check yet are checked
		 * @see #actions
		 * @note this method should exclusively used within
		 * #setValue(const int &) between #blockSignalsOnConnectedWidgets() and
		 * #unblockSignalsOnConnectedWidgets();
		 */
		void updateActions(const int & index);

		/**
		 * Checks if the argument controller is compatible with this controller:
		 * By checking the number of elements in the enum and the min value
		 * of the enum
		 * @param controller the controller to check
		 * @return true if the argument controller is compatible with
		 * this controller.
		 */
		bool checkMirror(QAbstractController<int> * controller);

		/**
		 * Connects this controller's valueChanged signal to "controller"'s
		 * setValue Slot
		 * @param controller the controller to connect
		 * @note concrete subclasses must implement this method
		 */
		void connectMirror(QAbstractController<int> * controller);

		/**
		 * Disconnects this controller's valueChanged signal from "controller"'s
		 * setValue Slot
		 * @param controller the controller to disconnect
		 * @note concrete subclasses must implement this method
		 */
		void disconnectMirror(QAbstractController<int> * controller);

		/**
		 * Enable or disable a specific value amongst all enumerated values by
		 * enabling/disabling its correspondant widget element (QAction in a
		 * QMenu or specific item in a QComboBox or QRadioButton).
		 * @note By default all enumerated values are enabled.
		 * @param value the value to enable amongst enumerated values
		 * @param enabled the status to set on the correspondant widget
		 * @return true if the corresponding enumerated value has been set to
		 * the deisred value, i.e, its correpondant widget or widget element is
		 * accessible enabled or not.
		 * @note this method should be used in #enable(const int &) and
		 * #disable(const int &) methods
		 * @see #enable(const int &)
		 * @see #disable(const int &)
		 */
		bool enable(const int & value, const bool enabled);

		/**
		 * sends a signal when anything changes
		 */
		void emitUpdated();

		/**
		 * Sends a signal when value has been set
		 * @param value the new value
		 */
		void emitValueChanged(const int value);
};

#endif /* QENUMCONTROLLER_H_ */
