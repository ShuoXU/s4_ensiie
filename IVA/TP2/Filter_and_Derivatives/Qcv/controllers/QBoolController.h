/*
 * QBoolController.h
 *
 *  Created on: 16 juin 2015
 *      Author: davidroussel
 */

#ifndef QBOOLCONTROLLER_H_
#define QBOOLCONTROLLER_H_
#include <functional>
using namespace std;

#include <QObject>
#include <QAbstractButton>
#include <QGroupBox>
#include <QAction>
#include <QSet>
#include <QList>

#include <Qcv/controllers/QAbstractController.h>

/**
 * Controller of a boolean parameter that can be connected to any
 * QAbstractButton such as a QCheckBox, QPushButton or QToolButton
 */
class QBoolController : public QObject, public QAbstractController<bool>
{
	Q_OBJECT

	public:
		/**
		 * Constructor from boolean value, mutex lock and evt parent object
		 * @param parameter the parameter to control
		 * @param lock the lock (from the processor containing the RangeValue)
		 * to use when modifying the boolean parameter
		 * @param callback the call back to call for setting the value instead
		 * of settting the value directly
		 * @param parent the parent object
		 * @param settings the settings to use to set initial value and/or
		 * save the current value (if different from default value) to settings
		 * @param settingsName the name to use when reading or writing the
		 * parameter value to settings (if any)
		 */
		QBoolController(bool * const parameter,
						QMutex * const lock = nullptr,
						function<void(const bool)> * const callback = nullptr,
						QObject * parent = nullptr,
						QSettings * setting = nullptr,
						const QString & settingsName = QString());

		/**
		 * Copy constructor
		 * @param bc the other QBoolController to copy
		 */
		QBoolController(const QBoolController & bc);

		/**
		 * Move constructor
		 * @param bc the other QBoolController to copy
		 */
		QBoolController(QBoolController && bc);

		/**
		 * Destructor
		 */
		virtual ~QBoolController();

		/**
		 * Setup a source to reflect the managed value.
		 * In this case only a QAbstractButton or a QAction will be setup
		 * @param source the source to setup
		 * @return true if the source was correclty set up, false otherwise
		 * (for example when the widget was already part of connected widgets).
		 */
		bool setupSource(QObject *source);

		/**
		 * Setup an abstract button (QCheckBox, QPushButton or QToolButton) and
		 * connects the necessary signals and slots. Convenience method
		 * to setup QAbstractButton directly
		 * @param button the button to setup
		 * @return true if the abstract button has been set up correctly, which
		 * means the QAbstractButton has been setup and signal/slots connections
		 * have also been setup between this and the QAbstractButton
		 */
		bool setupAbstractButton(QAbstractButton * button);

		/**
		 * Setup a chekable group box and connects the necessary signals and
		 * slots.
		 * @param groupBox the checkable group box to set up.
		 * @return  true if the groupbox was checkable and has been set up
		 * correctly, which means QGroupBox has been set up and signals/slots
		 * connections have also been setup between this and the QGroupBox
		 */
		bool setupGroupBox(QGroupBox * groupBox);

		/**
		 * Setup an action (typically in a menu) and connects the necessary
		 * signals and slots. Convenience method to setup QAction directly
		 * @param action the action to setup
		 * @return true if the action has been set up correctly, which
		 * means the QAction has been setup and signal/slots connections
		 * have also been setup between this and the QAction
		 */
		bool setupAction(QAction * action);

	public slots:
		/**
		 * Slot to change the #enabled state of GUI elements connected to this
		 * controller
		 * @param value the new enabled state
		 */
		void setEnabled(const bool value);

		/**
		 * Sets new value in the boolean value.
		 * @param value the value to set
		 * @return true if the value has been set without correction, false
		 * otherwise.
		 * @note this slot needs to be reimplemented even if its implementation
		 * does not change from QAbstractController because it is now a slot;
		 */
		bool setValue(const bool value);

	signals:
		/**
		 * Signal to send when anything changes
		 */
		void updated();

		/**
		 * Signal to emit when value has been set
		 * @param value the new value
		 */
		void valueChanged(const bool value);

	protected:
		/**
		 * Connects this controller's valueChanged signal to "controller"'s
		 * setValue Slot
		 * @param controller the controller to connect
		 * @note concrete subclasses must implement this method
		 */
		void connectMirror(QAbstractController<bool> *  controller);

		/**
		 * Disconnects this controller's valueChanged signal from "controller"'s
		 * setValue Slot
		 * @param controller the controller to disconnect
		 * @note concrete subclasses must implement this method
		 */
		void disconnectMirror(QAbstractController<bool> * controller);

		/**
		 * sends a signal when anything changes
		 */
		void emitUpdated();

		/**
		 * Sends a signal when value has been set
		 * @param value the new value
		 */
		void emitValueChanged(const bool value);

//		/**
//		 * Update all sources to the same value
//		 */
//		void updateSources();
};

#endif /* QBOOLCONTROLLER_H_ */
