Bs = 15;
cf0 = 40000;
cr0 = 35000;
Is = 0.05;
J = 2454;
Kp = 1;
lf = 1.22;
lr = 1.44;
ls = 0.95;
a = 1.5;
m = 1600;
Rs = 14;
v = 20;
etaT = 0.13;
mu = 1;
L = 3.5;
cr = cr0 * mu;
cf = cf0 * mu;
b1 = 2 * cf / (m * v);
b2 = 2 * cf * lf / J;
a11 = -2 * (cr + cf) / (m * v);
a12 = -1 + (2 * (lr * cr  - lf * cf) / (m * v * v));
a21 = 2 * (lr * cr - lf  * cf) / J;
a22 = - 2 * (lr * lr * cr + lf * lf * cf) / (J * v);
TSbeta = 2 * Kp * cf * etaT / Rs;
TSgamma = 2 * Kp * cf * lf * etaT / (Rs * v);
A = [a11 a12 0 0 b1 0; a21 a22 0 0 b2 0; 0 1 0 0 0 0; v ls v 0 0 0; 0 0 0 0 0 1; TSbeta/(Is*Rs) TSgamma/(Is*Rs) 0 0 -2*Kp*cf*etaT/(Is*Rs*Rs) -Bs/Is];
B = [0; 0; 0; 0; 0; 1/(Rs*Is)];
K = [-198.5 -69.3 -355.9 -17.7 -409.9 5.5];


L1 = [1 2 3 4 5 6];
L2 = [-1 -1 -1 -1 -1 -1];
L3 = [3 3 3 3 3 3];
p = spec(A+B*K)
p1 = spec(A+B*L1);
p2 = spec(A+B*L2);
p3 = spec(A+B*L3);
r = ppol(A+B*K, B, p)
r1 = ppol(A+B*L1, B, p1);
r2 = ppol(A+B*L2, B, p2);
r3 = ppol(A+B*L3, B, p3);

disp(p,r,p1,p2,p3,r1,r2,r3);
