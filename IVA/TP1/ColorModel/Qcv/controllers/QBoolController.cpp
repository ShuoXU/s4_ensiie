/*
 * QBoolController.cpp
 *
 *  Created on: 16 juin 2015
 *      Author: davidroussel
 */
#include <cassert>

#include <QDebug>

#include <Qcv/controllers/QBoolController.h>

/*
 * Constructor from boolean value, mutex lock and evt parent object
 * @param parameter the parameter to control
 * @param lock the lock (from the processor containing the RangeValue)
 * to use when modifying the boolean parameter
 * @param callback the call back to call for setting the value instead
 * of settting the value directly
 * @param parent the parent object
 * @param settings the settings to use to set initial value and/or
 * save the current value (if different from default value) to settings
 * @param settingsName the name to use when reading or writing the
 * parameter value to settings (if any)
 */
QBoolController::QBoolController(bool * const parameter,
								 QMutex * const lock,
								 function<void(const bool)> * const callback,
								 QObject * parent,
								 QSettings * settings,
								 const QString & settingsName) :
	QObject(parent),
	QAbstractController(parameter, lock, callback, settings, settingsName)
{
	if (settings != nullptr && settingsName.size() > 0)
	{
		// read value from preferences
		QVariant variant = settings->value(settingsName);
		if (variant.isValid())
		{
			bool prefBoolValue = variant.toBool();
			if (prefBoolValue != *managedValue)
			{
				setValue(prefBoolValue);
			}
		}
	}
}

/*
 * Copy constructor
 * @param bc the other QBoolController to copy
 */
QBoolController::QBoolController(const QBoolController & bc) :
	QBoolController(bc.managedValue,
					bc.lock,
					bc.callback,
					bc.parent(),
					bc.settings,
					bc.settingsName)
{
}

/*
 * Move constructor
 * @param bc the other QBoolController to copy
 */
QBoolController::QBoolController(QBoolController && bc) :
	QBoolController(bc.managedValue,
					bc.lock,
					bc.callback,
					bc.parent(),
					bc.settings,
					bc.settingsName)
{
}

/*
 * Destructor
 */
QBoolController::~QBoolController()
{
}

/*
 * Setup a source to reflect the managed value.
 * In this case only a QAbstractButton or a QAction will be setup
 * @param source the source to setup
 * @return true if the source was correclty set up, false otherwise
 * (for example when the widget was already part of connected widgets).
 */
bool QBoolController::setupSource(QObject *source)
{
	QAbstractButton * button = dynamic_cast<QAbstractButton *>(source);
	if (button != nullptr)
	{
		return setupAbstractButton(button);
	}

	QAction * action = dynamic_cast<QAction *>(source);
	if (action != nullptr)
	{
		return setupAction(action);
	}

	return false;
}

/*
 * Setup an abstract button (QCheckBox, QPushButton or QToolButton) and
 * connects the necessary signals and slots
 * @param button the button to setup
 * @return true if the abstract button has been set up correctly
 */
bool QBoolController::setupAbstractButton(QAbstractButton * button)
{
	if (QAbstractController<bool>::setupSource(button))
	{
//		qDebug() << "Setting up" << button;
		button->setChecked(*managedValue);

		connect(button, SIGNAL(clicked(bool)),
				this, SLOT(setValue(bool)), Qt::DirectConnection);
		/*
		 * this->setValue() will block signals from all connected abstract
		 * buttons before emitting the valueChanged signal in order to
		 * avoid signal/slot loop
		 */
		connect(this, SIGNAL(valueChanged(bool)),
				button, SLOT(setChecked(bool)), Qt::DirectConnection);

		return true;
	}
	else
	{
		qWarning() << button << "has not been setup correctly";
	}

	return false;
}

/*
 * Setup a chekable group box and connects the necessary signals and
 * slots.
 * @param groupBox the checkable group box to set up.
 * @return  true if the groupbox was checkable and has been set up
 * correctly, which means QGroupBox has been set up and signals/slots
 * connections have also been setup between this and the QGroupBox
 */
bool QBoolController::setupGroupBox(QGroupBox * groupBox)
{
	if (QAbstractController<bool>::setupSource(groupBox))
	{
		if (groupBox->isCheckable())
		{
			groupBox->setChecked(*managedValue);

			connect(groupBox, SIGNAL(clicked(bool)),
					this, SLOT(setValue(bool)), Qt::DirectConnection);
			/*
			 * this->setValue() will block signals from all connected abstract
			 * buttons before emitting the valueChanged signal in order to
			 * avoid signal/slot loop
			 */
			connect(this, SIGNAL(valueChanged(bool)),
					groupBox, SLOT(setChecked(bool)), Qt::DirectConnection);

			return true;
		}
		else
		{
			qWarning() << groupBox << "is not checkable";
		}
	}
	else
	{
		qWarning() << groupBox << "has not been setup correctly";
	}

	return false;
}

/*
 * Setup an action (typically in a menu) and connects the necessary
 * signals and slots. Convenience method to setup QAction directly
 * @param action the action to setup
 * @return true if the action has been set up correctly, which
 * means the QAction has been setup and signal/slots connections
 * have also been setup between this and the QAction
 */
bool QBoolController::setupAction(QAction * action)
{
	if (QAbstractController<bool>::setupSource(action))
	{
//		qDebug() << "Setting up " << action;

		if (!action->isCheckable())
		{
			action->setCheckable(true);
		}

		action->setChecked(*managedValue);

		connect(action, SIGNAL(triggered(bool)),
				this, SLOT(setValue(bool)), Qt::DirectConnection);
		/*
		 * this->setValue() will block signals from all connected abstract
		 * buttons before emitting the valueChanged signal in order to
		 * avoid signal/slot loop
		 */
		connect(this, SIGNAL(valueChanged(bool)),
				action, SLOT(setChecked(bool)), Qt::DirectConnection);

		return true;
	}
	else
	{
		qWarning() << action << "has not been setup correctly";
	}
	return false;
}

/*
 * Slot to change the #enabled state of GUI elements connected to this
 * controller
 * @param value the new enabled state
 */
void QBoolController::setEnabled(const bool value)
{
	QAbstractController<bool>::setEnabled(value);
}

/*
 * Sets new value in the boolean value.
 * @param value the value to set
 * @return true if the value has been set without correction, false
 * otherwise.
 */
bool QBoolController::setValue(const bool value)
{
	bool oldValue = *managedValue;

	bool result = QAbstractController<bool>::setValue(value);
//	updateSources();

	// Save value to preferences (if possible)
	if ((settings != nullptr) &&
		(settingsName.size() > 0) &&
		(*managedValue != oldValue))
	{
		settings->setValue(settingsName, *managedValue);
	}

	return  result;
}

/*
 * Connects this controller's valueChanged signal to "controller"'s
 * setValue Slot
 * @param controller the controller to connect
 * @note concrete subclasses must implement this method
 */
void QBoolController::connectMirror(QAbstractController<bool> * controller)
{
	if (controller != nullptr)
	{
		QBoolController * bController = dynamic_cast<QBoolController *>(controller);
		if (bController != nullptr)
		{
			if (checkMirror(controller))
			{
				connect(this, SIGNAL(valueChanged(bool)),
						bController, SLOT(setValue(bool)));
			}
			else
			{
				qWarning() << Q_FUNC_INFO << "controller" << controller
						   << "is not compatible with"
						   << this;
			}
		}
		else
		{
			qWarning() << Q_FUNC_INFO << "controller " << controller
					   << "could not be casted to"
					   << this->metaObject()->className();
		}
	}
}

/*
 * Disconnects this controller's valueChanged signal from "controller"'s
 * setValue Slot
 * @param controller the controller to disconnect
 * @note concrete subclasses must implement this method
 */
void QBoolController::disconnectMirror(QAbstractController<bool> * controller)
{
	if (controller != nullptr)
	{
		QBoolController * bController = dynamic_cast<QBoolController *>(controller);
		if (bController != nullptr)
		{
			disconnect(this, SIGNAL(valueChanged(bool)),
					   bController, SLOT(setValue(bool)));
		}
		else
		{
			qWarning() << Q_FUNC_INFO << "controller " << controller
					   << "could not be casted to"
					   << this->metaObject()->className();
		}
	}
}

/*
 * sends a signal when anything changes
 */
void QBoolController::emitUpdated()
{
	emit updated();
}

/*
 * Sends a signal when value has been set
 * @param value the new value
 */
void QBoolController::emitValueChanged(const bool value)
{
	emit valueChanged(value);
}


/*
 * Update all sources to the same value
 */
//void QBoolController::updateSources()
//{
//	bool value = *managedValue;

//	for (int i = 0; i < sources.size(); ++i)
//	{
//		QAbstractButton * button = dynamic_cast<QAbstractButton *>(sources[i]);
//		if (button != nullptr)
//		{
//			if (button->isChecked() != value)
//			{
//				button->setChecked(value);
//			}
//		}

//		QAction * action = dynamic_cast<QAction *>(sources[i]);
//		if (action != nullptr)
//		{
//			if (action->isChecked() != value)
//			{
//				action->setChecked(value);
//			}
//		}
//	}
//}
