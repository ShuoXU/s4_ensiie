#ifndef QCVCAPTUREWORKER_H
#define QCVCAPTUREWORKER_H

#include <QThread>
#include <QElapsedTimer>
#include <QTimer>
#include <QWaitCondition>

#include <Qcv/capture/QcvThreadedVideoCapture.h>

/*
 * Forward declaration of QcvThreadedVideoCapture because
 *	- QcvThreadedVideoCapture includes QcvCaptureWorker
 *	- and QcvCaptureWorker includes QcvThreadedVideoCapture
 */
class QcvThreadedVideoCapture;

/**
 * Independant thread to update capture.
 */
class QcvCaptureWorker : public QThread
{
	Q_OBJECT
	private :
		/**
		 * The capture to work on
		 */
		QcvThreadedVideoCapture * qCapture;

		/**
		 * Wait condition to wait for at least one update from #worker's thread
		 */
		QWaitCondition firstUpdateCondition;

		/**
		 * A Timer to measure elapsed time and compute actual framerate
		 */
		QElapsedTimer timeMeasure;

		/**
		 * Indicates this worker is submitted to a constrained frame rate
		 */
		bool synchronized;

		/**
		 * The delay (in ms) used in this worker when frame rate is contrained
		 */
		unsigned long delay;

		/**
		 * The default delay (in ms) used in this worker
		 */
		static const unsigned long defaultDelay;

	public :
		/**
		 * Constructor from capture instance and parent
		 * @param capture the capture instance to work on which is also a parent
		 * object
		 * @param synchronized indicates this worker uses a QTimer to pace
		 * the updates
		 * @param delay the delay (in ms) used by the QTimer in synchronized
		 * mode [default value is 33 ms for 30 frames/s]
		 */
		explicit QcvCaptureWorker(QcvThreadedVideoCapture * qCapture,
								  const bool synchronized = true,
								  const unsigned long delay = defaultDelay);

		/**
		 * Destructor
		 */
		virtual ~QcvCaptureWorker();

		/**
		 * This thread override run method
		 * @post During the update the following signals have been emitted:
		 *	- The capture's update method emits the following signals:
		 *		- if new image is available updated() is emitted
		 *	- If capture frame rate changed capture's emitFrameRateChanged is
		 *	emitted
		 */
		void run() override;

		/**
		 * Gets the update condition.
		 * When setting up a new capture this condition is used to wake up all
		 * threads waiting on a first capture update.
		 * @return The update condition used during first capture update
		 */
		QWaitCondition & getUpdateCondition();

		/**
		 * Gets the synchronized state
		 * @return the synchronized state
		 */
		bool isSynchronized() const;

		/**
		 * Sets this worker to synchronized mode or not.
		 * When the worker is already running the mode will apply to the NEXT
		 * run evt driven by a QTimer.
		 * @param synchronized the new synchronized state
		 */
		void setSynchronized(const bool synchronized);

		/**
		 * Gets the delay (in ms) between frames in synchronized mode
		 * @return the delay (in ms) between frames in synchronized mode
		 */
		int getDelay() const;

		/**
		 * Sets the delay (in ms) for the timer when worker is in synchronized
		 * mode. If the worker is already running the delay will be applied on
		 * the NEXT run.
		 * @param value the new delay value.
		 */
		void setDelay(const int value);
};

#endif // QCVCAPTUREWORKER_H
