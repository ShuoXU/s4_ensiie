#include <cmath> // for round

#include <QDebug>

#include "QcvAlgoParamConstWidget.h"
#include "ui_QcvAlgoParamConstWidget.h"

/*
 * Constructor
 * @param param the parameter to illustrate
 * @param parent the parent widget
 */
QcvAlgoParamConstWidget::QcvAlgoParamConstWidget(CvAlgoParam * param,
												 QWidget * parent)
	: QcvAlgoParamWidget(param, true, nullptr, "", parent),
	  ui(new Ui::QcvAlgoParamConstWidget)
{
//	qDebug() << this << "->" << Q_FUNC_INFO;

	ui->setupUi(this);

	assert(parameter->isConstant());

	// Setup title
	ui->label->setText(QString::fromStdString(parameter->getName()));

	// Setup tooltip
	ui->label->setToolTip(QString::fromStdString(parameter->getDescription()));

	// Setup value label
	switch (parameter->getType())
	{
		case CvAlgoParam::ParamType::BOOLEAN:
			ui->valueLabel->setText(
				(parameter->getBoolValue() ? "true" : "false"));
			break;
		case CvAlgoParam::ParamType::INT:
			ui->valueLabel->setText(QString::number(parameter->getIntValue()));
			break;
		case CvAlgoParam::ParamType::REAL:
		{
			double dStepValue =
				parameter->getDoubleValue(CvAlgoParam::WhichValue::STEPVALUE);
			int precision = 0;
			while (dStepValue != round(dStepValue))
			{
				dStepValue *= 10;
				precision++;
			}
			ui->valueLabel->setText(QString::number(parameter->getDoubleValue(),
													'f', precision));
			break;
		}
		default:
			ui->valueLabel->setText("");
			break;
	}
}

/*
 * Destructor.
 * @note delete the UI
 */
QcvAlgoParamConstWidget::~QcvAlgoParamConstWidget()
{
//	qDebug() << this << "->" << Q_FUNC_INFO;
	delete ui;
}

/*
 * Empty implementation of superclass pure virtual method since this
 * widget represents a constant parameter
 * @return always false
 */
bool QcvAlgoParamConstWidget::readSetting()
{
	return false;
}

/*
 * Empty implementation of superclass virtual method.
 * @param value the QVariant value to save into preferences which will
 * not be saved since this widget represents a constant parameter
 */
void QcvAlgoParamConstWidget::writeSetting(const QVariant &) const
{
	// Does nothing
}

/*
 * Slot triggered when reset button is clicked.
 * @note Actually this signal is never emitted since there is no
 * reset button in this widget, so implementation is empty
 */
void QcvAlgoParamConstWidget::on_resetButton_clicked()
{
	// Does Nothing
}

