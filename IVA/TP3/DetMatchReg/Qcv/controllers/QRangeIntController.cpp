#include <cassert>

#include <QDebug>
#include <QVector>
#include <QSpinBox>

#include <Qcv/controllers/QRangeIntController.h>

/*
 * Constructor from RangeValue, mutex and parent object
 * @param range the RangeValue<int> to reference
 * @param lock the lock (from the processor containing the RangeValue) to
 * use when modifying the RangeValue
 * @param the factor to apply to the range values when emitting or
 * the factor to divide outside values when setting a new range value.
 * @param callback the call back to call for setting the value instead
 * of settting the value directly
 * @param parent parent object
 * @param settings the settings to use to set initial value and/or
 * save the current value (if different from default value) to settings
 * @param settingsName the name to use when reading or writing the
 * parameter value to settings
 */
QRangeIntController::QRangeIntController(RangeValue<int> * const range,
										 QMutex * const lock,
										 const int & factor,
										 function<void(const int)> * const callback,
										 QObject * parent,
										 QSettings * settings,
										 const QString & settingsName) :
	QObject(parent),
	QRangeAbstractController(range,
							 lock,
							 factor,
							 callback,
							 settings,
							 settingsName)
{
	if (settings != nullptr && settingsName.size() > 0)
	{
		// read "inside" value from preferences
		QVariant variant = settings->value(settingsName);
		if (variant.isValid())
		{
			int prefIntValue = variant.toInt();
			if ((prefIntValue != *managedValue) &&
				(prefIntValue >= managedValue->min()) &&
				(prefIntValue <= managedValue->max()))
			{
				// sets a new "outside" value
				setValue(prefIntValue * factor);
			}
		}
	}
}

/*
 * Copy Constructor
 * @param rc the range controller to copy
 */
QRangeIntController::QRangeIntController(const QRangeIntController & rc):
	QRangeIntController(rc.managedValue,
						rc.lock,
						rc.factor,
						rc.callback,
						rc.parent(),
						rc.settings,
						rc.settingsName)
{
}

/*
 * Move Constructor
 * @param rci the range controller to move
 */
QRangeIntController::QRangeIntController(QRangeIntController && rc) :
	QRangeIntController(rc.managedValue,
						rc.lock,
						rc.factor,
						rc.callback,
						rc.parent(),
						rc.settings,
						rc.settingsName)
{
}

/*
 * Destructor
 */
QRangeIntController::~QRangeIntController()
{
	disconnect(this);
}

/*
 * Setup a spin box with the values in the RangeValue and connect
 * the necessary signals and slots
 * @param spinBox the spinbox to setup with
 * @return true if the spinBox have been set up correctly, false
 * otherwise
 * @post If this spinbox was not part of the widgets set, then the
 * spinBox has been setup with RangeValue values and bidirectionnal
 * connections between this and the spinBox have been created.
 */
bool QRangeIntController::setupSpinBox(QAbstractSpinBox * spinBox)
{
	bool setup = QRangeAbstractController<int>::setupSpinBox(spinBox);

	if (setup)
	{
		QSpinBox * intSpinBox = dynamic_cast<QSpinBox *>(spinBox);
		if (intSpinBox != nullptr)
		{
			connect(intSpinBox, SIGNAL(valueChanged(int)),
					this, SLOT(setValue(int)));
			/*
			 * this->setValue will block signals from spinBox before emitting
			 * valueChanged in order to avoid signal/slot loop
			 */
			connect(this, SIGNAL(valueChanged(int)),
					intSpinBox, SLOT(setValue(int)));
		}
		else
		{
			qWarning() << "QRangeIntController::setupSpinBox can't cast to QSpinBox";
		}
	}

	return setup;
}

/*
 * Setup a slider with the values in the RangeValue (and connects the
 * necessary signals and slots).
 * @param slider the slider to setup according to RangeValue values
 * @return true if the slider have been set up correctly, false
 * otherwise
 * @post If this slider was not part of the widgets set, then the
 * slider has been setup with RangeValue steps and bidirectionnal
 * connections between this and the slider have been created.
 */
bool QRangeIntController::setupSlider(QAbstractSlider * slider)
{
	bool setup = QRangeAbstractController<int>::setupSlider(slider);

	if (setup)
	{
		connect(slider, SIGNAL(sliderMoved(int)), this, SLOT(setIndex(int)));
		/*
		 * this->setIndex() will block signals from slider before emitting
		 * indexChanged() signal in order to avoid signal/slot loop
		 */
		connect(this, SIGNAL(indexChanged(int)),
				slider, SLOT(setValue(int)));
		connect(this, SIGNAL(indexRangeChanged(int,int)),
				slider, SLOT(setRange(int,int)));
	}

	return setup;
}

/*
 * Adds a reset button to this range controller in order to reset to the
 * default value
 * @param button the button to connect to the reset slot
 */
bool QRangeIntController::addResetButton(QAbstractButton * button)
{
	bool setup = QAbstractController<RangeValue<int>, int>::setupSource(button);

	if (setup)
	{
		connect(button, SIGNAL(clicked()), this, SLOT(reset()));
	}

	return setup;
}

/*
 * Connects the corresponding xxxChanged(int) signal to the setNum
 * slot of a QLabel
 * @param label the label to show the minimum value
 * @param which the kind of value to be shown in the label
 * @return true if the label pointer was not null
 */
bool QRangeIntController::addNumberLabel(QLabel * label,
										 RangeValue<int>::WhichValue which)
{
	bool result = QAbstractController<RangeValue<int>, int>::setupSource(label);

	if (result)
	{
		int value;
		switch (which)
		{
			case RangeValue<int>::WhichValue::VALUE:
				value = managedValue->value();
				result = connect(this, SIGNAL(valueChanged(int)),
								 label, SLOT(setNum(int)));
				break;
			case RangeValue<int>::WhichValue::MIN_VALUE:
				value = managedValue->min();
				result = connect(this, SIGNAL(minChanged(int)),
								 label, SLOT(setNum(int)));
				break;
			case RangeValue<int>::WhichValue::MAX_VALUE:
				value = managedValue->max();
				result = connect(this, SIGNAL(maxChanged(int)),
								 label, SLOT(setNum(int)));
				break;
			case RangeValue<int>::WhichValue::STEP_VALUE:
				value = managedValue->step();
				result = connect(this, SIGNAL(stepChanged(int)),
								 label, SLOT(setNum(int)));
				break;
			default:
				result = false;
				break;
		}

		if (result)
		{
			label->setNum(value);
		}
	}

	return result;
}

/*
 * Slot to change the #enabled state of GUI elements connected to this
 * controller
 * @param value the new enabled state
 */
void QRangeIntController::setEnabled(const bool value)
{
	QAbstractController<RangeValue<int>, int>::setEnabled(value);
}

/*
 * Sets new value in the range value.
 * @param value the value to set
 * @return true if the value has been set without correction, false
 * otherwise. In any case a value is set according the rules of
 * RangeValue<T>::setClosestValue
 * @note slot typically used by QSpinBox when its value changes
 * @note Please note that if result is false a signal needs to be sent
 * to the connected UI to ajust the value since it may have been
 * slighltly modified.
 * @note This method needs to be be reimplemented since it is a slot now
 * @see RangeController::setValue(const int &)
 */
bool QRangeIntController::setValue(const int value)
{
	return QRangeAbstractController<int>::setValue(value);
}

/*
 * Sets a new value corresponding to the index
 * @param index the index to set the new value to
 * @return true in index is valid (the corresponding value is less or
 * equal to the max value
 * @note This method needs to be be reimplemented since it is a slot now
 * @see RangeController::setIndex(const int &)
 */
bool QRangeIntController::setIndex(const int index)
{
	return QRangeAbstractController<int>::setIndex(index);
}

/*
 * Reset managed value to its default value
 * @return true if the value has been reset to its default value exactly,
 * false if the reset has triggered bounds or step modifications
 * @note This method needs to be be reimplemented since it is a slot now
 */
bool QRangeIntController::reset()
{
	return QRangeAbstractController<int>::reset();
}

/*
 * Refresh all connected UI.
 * When the internal managedValue has been modified outside of this
 * controller, all connected UIs should be refreshed
 */
void QRangeIntController::refresh()
{
	QRangeAbstractController<int>::refresh();

	/*
	 * Block signals in connected widgets to avoid signal/slots loops
	 */
	blockSignalsOnConnectedWidgets();

	/*
	 * Then for widgets that don't react to signals refresh their values directly
	 * Typically
	 *	- SpinBoxes : min, max & step values have to be set through setters
	 *  - Sliders : step has to be set through setters but is always 1
	 *	so it does not change
	 */
	for (int i = 0; i < sources.size(); ++i)
	{
		QSpinBox * spinBox = dynamic_cast<QSpinBox *>(sources[i]);

		if (spinBox != nullptr)
		{
			spinBox->setMinimum(managedValue->min());
			spinBox->setMaximum(managedValue->max());
			spinBox->setSingleStep(managedValue->step());
		}
	}

	/*
	 * Unblock signals in connected widgets
	 */
	unblockSignalsOnConnectedWidgets();
}

/*
 * Connects this controller's valueChanged signal to "controller"'s
 * setValue Slot
 * @param controller the controller to connect
 * @note concrete subclasses must implement this method
 */
void QRangeIntController::connectMirror(QAbstractController<RangeValue<int>, int> * controller)
{
	if (controller != nullptr)
	{
		QRangeIntController * rController = dynamic_cast<QRangeIntController *>(controller);
		if (rController != nullptr)
		{
			if (checkMirror(controller))
			{
				connect(this, SIGNAL(valueChanged(int)),
						rController, SLOT(setValue(int)));
			}
			else
			{
				qWarning() << Q_FUNC_INFO << "controller" << controller
						   << "is not compatible with"
						   << this;
			}
		}
		else
		{
			qWarning() << Q_FUNC_INFO << "controller " << controller
					   << "could not be casted to"
					   << this->metaObject()->className();
		}
	}
}

/*
 * Disconnects this controller's valueChanged signal from "controller"'s
 * setValue Slot
 * @param controller the controller to disconnect
 * @note concrete subclasses must implement this method
 */
void QRangeIntController::disconnectMirror(QAbstractController<RangeValue<int>, int> * controller)
{
	if (controller != nullptr)
	{
		QRangeIntController * rController = dynamic_cast<QRangeIntController *>(controller);
		if (rController != nullptr)
		{
			disconnect(this, SIGNAL(valueChanged(int)),
					   rController, SLOT(setValue(int)));
		}
		else
		{
			qWarning() << Q_FUNC_INFO << "controller " << controller
					   << "could not be casted to"
					   << this->metaObject()->className();
		}
	}
}

/*
 * sends a signal when anything changes
 */
void QRangeIntController::emitUpdated()
{
	emit updated();
}

/*
 * Sends a signal when value has been set
 * @param value the new value
 */
void QRangeIntController::emitValueChanged(const int value)
{
	emit valueChanged(value);
}

/*
 * Sends a signal when value has been set in the RangeValue which
 * changes the index of the current value
 * @param index the index of the new value
 * @note typically used by QSlider
 */
void QRangeIntController::emitIndexChanged(const int index)
{
	emit indexChanged(index);
}

/*
 * Sends a signal when min value in the range changes
 * @param value the new min value in the range
 */
void QRangeIntController::emitMinChanged(const int & value)
{
	emit minChanged(value);
}

/*
 * Sends a signal when max value in the range changes
 * @param value the new max value in the range
 */
void QRangeIntController::emitMaxChanged(const int & value)
{
	emit maxChanged(value);
}

/*
 * Sends a signal when min or max value changes
 * @param min the minimum value of the range
 * @param max the maximum value of the range
 */
void QRangeIntController::emitRangeChanged(const int & min, const int & max)
{
	emit rangeChanged(min, max);
}

/*
 * Sends a signal when step value in the range changes
 * @param value the new step value in the range
 */
void QRangeIntController::emitStepChanged(const int & value)
{
	emit stepChanged(value);
}

/*
 * Sends a signal when min or max changes (because it might affect the
 * indices of a slider for instance)
 * @param min the minimum index (always 0)
 * @param max the maximum index (the number of steps of the range value)
 */
void QRangeIntController::emitIndexRangeChanged(const int & minIndex,
												const int & maxIndex)
{
	emit indexRangeChanged(minIndex, maxIndex);
}
