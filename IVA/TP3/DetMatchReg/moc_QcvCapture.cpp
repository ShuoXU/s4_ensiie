/****************************************************************************
** Meta object code from reading C++ file 'QcvCapture.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Qcv/capture/QcvCapture.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QcvCapture.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QcvCapture_t {
    QByteArrayData data[21];
    char stringdata0[175];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QcvCapture_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QcvCapture_t qt_meta_stringdata_QcvCapture = {
    {
QT_MOC_LITERAL(0, 0, 10), // "QcvCapture"
QT_MOC_LITERAL(1, 11, 7), // "updated"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 14), // "messageChanged"
QT_MOC_LITERAL(4, 35, 7), // "message"
QT_MOC_LITERAL(5, 43, 7), // "timeout"
QT_MOC_LITERAL(6, 51, 12), // "imageChanged"
QT_MOC_LITERAL(7, 64, 8), // "cv::Mat*"
QT_MOC_LITERAL(8, 73, 5), // "image"
QT_MOC_LITERAL(9, 79, 10), // "openDevice"
QT_MOC_LITERAL(10, 90, 8), // "deviceId"
QT_MOC_LITERAL(11, 99, 6), // "size_t"
QT_MOC_LITERAL(12, 106, 5), // "width"
QT_MOC_LITERAL(13, 112, 6), // "height"
QT_MOC_LITERAL(14, 119, 8), // "openFile"
QT_MOC_LITERAL(15, 128, 8), // "fileName"
QT_MOC_LITERAL(16, 137, 10), // "setFlipped"
QT_MOC_LITERAL(17, 148, 5), // "value"
QT_MOC_LITERAL(18, 154, 7), // "setGray"
QT_MOC_LITERAL(19, 162, 7), // "setSize"
QT_MOC_LITERAL(20, 170, 4) // "size"

    },
    "QcvCapture\0updated\0\0messageChanged\0"
    "message\0timeout\0imageChanged\0cv::Mat*\0"
    "image\0openDevice\0deviceId\0size_t\0width\0"
    "height\0openFile\0fileName\0setFlipped\0"
    "value\0setGray\0setSize\0size"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QcvCapture[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      13,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   79,    2, 0x06 /* Public */,
       3,    2,   80,    2, 0x06 /* Public */,
       3,    1,   85,    2, 0x26 /* Public | MethodCloned */,
       6,    1,   88,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       9,    3,   91,    2, 0x0a /* Public */,
       9,    2,   98,    2, 0x2a /* Public | MethodCloned */,
       9,    1,  103,    2, 0x2a /* Public | MethodCloned */,
      14,    3,  106,    2, 0x0a /* Public */,
      14,    2,  113,    2, 0x2a /* Public | MethodCloned */,
      14,    1,  118,    2, 0x2a /* Public | MethodCloned */,
      16,    1,  121,    2, 0x0a /* Public */,
      18,    1,  124,    2, 0x0a /* Public */,
      19,    1,  127,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    4,    5,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, 0x80000000 | 7,    8,

 // slots: parameters
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 11, 0x80000000 | 11,   10,   12,   13,
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 11,   10,   12,
    QMetaType::Bool, QMetaType::Int,   10,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 11, 0x80000000 | 11,   15,   12,   13,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 11,   15,   12,
    QMetaType::Bool, QMetaType::QString,   15,
    QMetaType::Void, QMetaType::Bool,   17,
    QMetaType::Void, QMetaType::Bool,   17,
    QMetaType::Void, QMetaType::QSize,   20,

       0        // eod
};

void QcvCapture::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QcvCapture *_t = static_cast<QcvCapture *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->updated(); break;
        case 1: _t->messageChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->messageChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->imageChanged((*reinterpret_cast< cv::Mat*(*)>(_a[1]))); break;
        case 4: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 5: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 7: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 8: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 9: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 10: _t->setFlipped((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 11: _t->setGray((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 12: _t->setSize((*reinterpret_cast< const QSize(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QcvCapture::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvCapture::updated)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QcvCapture::*_t)(const QString & , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvCapture::messageChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QcvCapture::*_t)(cv::Mat * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvCapture::imageChanged)) {
                *result = 3;
                return;
            }
        }
    }
}

const QMetaObject QcvCapture::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_QcvCapture.data,
      qt_meta_data_QcvCapture,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QcvCapture::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QcvCapture::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QcvCapture.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int QcvCapture::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 13)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 13;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 13)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 13;
    }
    return _id;
}

// SIGNAL 0
void QcvCapture::updated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QcvCapture::messageChanged(const QString & _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 3
void QcvCapture::imageChanged(cv::Mat * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
