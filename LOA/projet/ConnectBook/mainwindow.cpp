#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "treemodel.h"

#include "tablemodel.h"
#include "mydelegate.h"
#include "myfilter.h"
#include <QImage>
#include <QDebug>
#include <QDragEnterEvent>
#include <QUrl>
#include <QMimeData>
#include <QtXml/QDomDocument>
#include <QtXml/QDomNode>
#include <QtXml/QDomNodeList>
#include <QtXml/QDomText>
#include <QFile>
#include <QCoreApplication>
#include <QDateTime>
#include <QString>
#include <QMessageBox>
#include <QXmlStreamReader>
#include <QFileDialog>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    selectFlag = false;

    modelBuild();

    ui->mainToolBar->actions()[2]->setEnabled(false);

    QMessageBox import;
    QPushButton *ok = import.addButton("Import xml file", QMessageBox::ActionRole);
    import.addButton(QMessageBox::Cancel);
    import.setText("Import a xml file or start blank");
    import.setWindowTitle("Choose");
    import.exec();

    if(import.clickedButton() == ok)
    {
        on_actionImport_XML_file_triggered();

    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::selectUpdate(const QModelIndex index)
{
    if(index.row() == 14)
    {
          ui->mainToolBar->actions()[3]->setEnabled(true);

    }
    else {
        ui->mainToolBar->actions()[3]->setEnabled(false);
    }


}
void MainWindow::initMenuBar()
{

    QAction* bar[4];

    bar[0] = new QAction(QIcon(":/images/addpeople.png"),"", this);

    bar[1] = new QAction(QIcon(":/images/addorga.png"), "", this);
    bar[2] = new QAction(QIcon(":/images/delete.png"),"",this);
    bar[3] = new QAction(QIcon(":/images/turn.png"),"",this);


    bar[0]->setShortcut(Qt::Key_Control | Qt::Key_E);


    bar[0]->setToolTip("This is a ToolTip");


    ui->mainToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);


    for(int i = 0 ; i < 4 ; i++)
    {
        ui->mainToolBar->addAction(bar[i]);
    }
    ui->mainToolBar->actions()[3]->setEnabled(false);
    connect(bar[0], SIGNAL(triggered()), this, SLOT(on_actionAdd_triggered()));
    connect(bar[1], SIGNAL(triggered()), this, SLOT(on_actionAdd_Organisation_triggered()));
    connect(bar[2], SIGNAL(triggered()), this, SLOT(on_actionRemove_triggered()));
    connect(bar[3], SIGNAL(triggered()), this, SLOT(turnToOrganisation()));

}

void MainWindow::modelBuild()
{
    const QStringList headers({tr("Title")});
    treemodel = new TreeModel(headers,"a");


    proxyModel = new Myfilter(this);
    proxyModel->setSourceModel(treemodel);
    proxyModel->setFilterKeyColumn(0);

    proxyModel->setFilterRole(Qt::DisplayRole);
    proxyModel->setSortCaseSensitivity(Qt::CaseInsensitive);


    ui->treeView->setModel(proxyModel);


    initMenuBar();

    ui->treeView->setSortingEnabled(true);
    ui->treeView->sortByColumn(0,Qt::AscendingOrder);
    People p;
    p.name.firstname = "Jennifer";
    p.name.lastname = "Aniston";
    p.name.nickname = "";
    p.o.name = "rank";
    People pp;
    pp.name.firstname = "Courteney";
    pp.name.lastname = "Cox";
    pp.name.nickname = "";
    pp.o.name = "rank";
    peoples.push_back(p);
    peoples.push_back(pp);
    insertRow("Jennifer Aniston",0);
    insertRow("Courteney Cox",0);

    Organisation o;
    o.name = "rank";
    organisations.push_back(o);

    insertRow(organisations[0].name,1);

    ui->photo->installEventFilter(this);

    ui->photo->setAcceptDrops(true);

    ui->photo->setAlignment(Qt::AlignCenter);
    ui->photo->setPixmap(QPixmap(":/images/photo.png"));

    connect(ui->treeView,SIGNAL(clicked( const QModelIndex)),this,SLOT(updateTableW(const QModelIndex)));
    connect(ui->pushButton, SIGNAL(clicked()),this,SLOT(filterText()));

}


bool MainWindow::eventFilter(QObject *watched, QEvent *event) {

    if (watched == ui->photo) {

        if (event->type() == QEvent::DragEnter) {



            QDragEnterEvent *dee = dynamic_cast<QDragEnterEvent *>(event);

            dee->acceptProposedAction();

            return true;

        } else if (event->type() == QEvent::Drop) {



            QDropEvent *de = dynamic_cast<QDropEvent *>(event);

            QList<QUrl> urls = de->mimeData()->urls();



            if (urls.isEmpty()) { return true; }

            QString path = urls.first().toLocalFile();


            QImage image(path); // QImage对I/O优化过, QPixmap对显示优化

            if (!image.isNull()) {

                image = image.scaled(ui->photo->size(),

                                     Qt::KeepAspectRatio,

                                     Qt::SmoothTransformation);

                ui->photo->setPixmap(QPixmap::fromImage(image));

            }

            if(imageFlag)
                tablemodelp->peoples[tablemodelp->getIndex()].image = path;
            else {
                tablemodelo->organisation[tablemodelo->getIndex()].logo = path;
            }

            return true;

        }

    }

    return QWidget::eventFilter(watched, event);

}

void MainWindow::closeEvent(QCloseEvent *ev)
{

    QMessageBox import;
    QPushButton *ok = import.addButton("Save xml file", QMessageBox::ActionRole);
    QPushButton *quit = import.addButton("Quit", QMessageBox::ActionRole);
    import.addButton(QMessageBox::Cancel);
    import.setText("Save or quit directly");
    import.setWindowTitle("Choose");
    import.exec();

    if(import.clickedButton() == ok)
    {
       on_actionExport_XML_file_triggered();

    }
    else if(import.clickedButton() == quit)
    {
        ev->accept();
    }




}
void MainWindow::updateTableW(const QModelIndex & indexp)
{


    QModelIndex index = proxyModel->mapToSource(indexp);
    if(index.parent() == QModelIndex())
    {
        selectFlag = false;
        imageFlag = true;
        ui->mainToolBar->actions()[2]->setEnabled(false);
        return;
    }
    bool flag = index.parent().row() == 0? true:false;

    ui->mainToolBar->actions()[2]->setEnabled(true);

    if(flag)
    {
        if(tmp_indexp != -1)
        {
            if(selectFlag)
                peoples[tmp_indexp] = tablemodelp->peoples[tmp_indexp];
        }
        tmp_indexp = index.row();
        imageFlag = true;

        tablemodelp = new Tablemodel(index.row(),flag);

        tablemodelp->peoples = peoples;

        ui->tableView->setModel(tablemodelp);
        connect(ui->tableView, SIGNAL(clicked(const QModelIndex)), this, SLOT(selectUpdate(const QModelIndex)));

        ui->tableView->resizeColumnsToContents();
        ui->tableView->horizontalHeader()->setStretchLastSection(true);
        ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        //ui->tableView->resizeColumnToContents(1);

        ui->tableView->horizontalHeader()->hide();
        ui->tableView->setItemDelegate(new MyDelegate(flag,this));

        peoples[index.row()] = tablemodelp->peoples[index.row()];


        if(peoples[index.row()].image == "")
            ui->photo->setPixmap(QPixmap(":/images/photo.png"));
        else {

            QImage image(peoples[index.row()].image);

            if (!image.isNull()) {

                image = image.scaled(ui->photo->size(),
                                     Qt::KeepAspectRatio,
                                     Qt::SmoothTransformation);
                ui->photo->setPixmap(QPixmap::fromImage(image));
            }
        }

    }
    else {

        if(tmp_indexo != -1)
        {

            if(selectFlag)
                organisations[tmp_indexo] = tablemodelo->organisation[tmp_indexo];

        }
        tmp_indexo = index.row();

        imageFlag = false;



        tablemodelo = new Tablemodel(index.row(),flag);

        tablemodelo->organisation = organisations;


        ui->tableView->setModel(tablemodelo);

        ui->tableView->horizontalHeader()->hide();
        ui->tableView->setItemDelegate(new MyDelegate(flag,this));

        organisations[index.row()] = tablemodelo->organisation[index.row()];


        if(organisations[index.row()].logo == "")
            ui->photo->setPixmap(QPixmap(":/images/photo.png"));
        else {

            QImage image(organisations[index.row()].logo);

            if (!image.isNull()) {

                image = image.scaled(ui->photo->size(),
                                     Qt::KeepAspectRatio,
                                     Qt::SmoothTransformation);
                ui->photo->setPixmap(QPixmap::fromImage(image));
            }
        }
    }
    selectFlag = true;

}


void MainWindow::on_actionAdd_triggered()
{
    People *p = new People;
    peoples.push_back(*p);
    insertRow("[No data]",0);
}

void MainWindow::turnToOrganisation()
{
    QString name = tablemodelp->peoples[tablemodelp->getIndex()].o.name;
    for(int i = 0; i < organisations.size(); i++)
    {
           if(organisations[i].name == name)
           {
                   const QModelIndex index = treemodel->index(1,0);
                   const QModelIndex org = treemodel->index(i,0,index);
               ui->treeView->selectionModel()->setCurrentIndex(proxyModel->mapFromSource(org),
                                                               QItemSelectionModel::ClearAndSelect);

               updateTableW(proxyModel->mapFromSource(org));
               return;
           }
    }
    addOrganisationDialog(name);


}

void MainWindow::insertRow(QString str,int i)
{

    const QModelIndex index = treemodel->index(i,0);
    int row = treemodel->rowCount(index);

    if(!treemodel->insertRow(row,index))
        return;
    for (int column = 0; column < treemodel->columnCount(index.parent()); ++column) {
        const QModelIndex child = treemodel->index(row, column, index);

        treemodel->setData(child, QVariant(str), Qt::EditRole);

        ui->treeView->selectionModel()->setCurrentIndex(proxyModel->mapFromSource(child),
                                                        QItemSelectionModel::ClearAndSelect);

        updateTableW(proxyModel->mapFromSource(child));
    }


}
bool MainWindow::removeRow()
{
    const QModelIndex index = proxyModel->mapToSource(ui->treeView->selectionModel()->currentIndex());

    if(index.parent().row() == 0)
    {
        peoples.remove(index.row(),1);
    }
    else {
        organisations.remove(index.row());
    }

    return treemodel->removeRow(index.row(), index.parent());
}

void MainWindow::on_actionAdd_Organisation_triggered()
{
    Organisation *p = new Organisation;
    organisations.push_back(*p);
    insertRow("[No data]",1);
}

void MainWindow::addOrganisationDialog(QString name)
{
    QModelIndex currentIndex = proxyModel->mapToSource(ui->treeView->selectionModel()->currentIndex());

    QMessageBox addOrganisation;
    QPushButton *ok = addOrganisation.addButton("OK", QMessageBox::ActionRole);
    addOrganisation.addButton(QMessageBox::Cancel);
    addOrganisation.setText("You don.t have this organisation, do you want to build one?");
    addOrganisation.setWindowTitle("Warning");
    addOrganisation.exec();

    if(addOrganisation.clickedButton() == ok)
    {
        Organisation *p = new Organisation;
        organisations.push_back(*p);
        insertRow(name,1);
        QModelIndex currentIndex2 = proxyModel->mapToSource(ui->treeView->selectionModel()->currentIndex());

        organisations[currentIndex2.row()].members.push_back(peoples[currentIndex.row()]);
        tablemodelo->organisation = organisations;
        updateTableW(proxyModel->mapFromSource(currentIndex2));

    }
}



void MainWindow::filterText()
{
    QRegExp regExp(ui->lineEdit->text(),Qt::CaseInsensitive,QRegExp::Wildcard);
    proxyModel->setFilterRegExp(regExp);
}



void MainWindow::on_actionRemove_triggered()
{

    if(removeRow())
    {

        selectFlag = false;
        ui->mainToolBar->actions()[2]->setEnabled(false);

    }
}

void MainWindow::on_actionExport_XML_file_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,"Open File","","xml File(*.xml)");
        if(fileName == "")
        {
            return;
        }
    QFile file(fileName);
        if(!file.open(QFile::WriteOnly|QFile::Truncate))
            return;

        if(peoples.size() == 0 && organisations.size() == 0)
            return;
        QDomDocument doc;
        QDomProcessingInstruction instruction;
        instruction=doc.createProcessingInstruction("xml","version=\"1.0\" encoding=\"UTF-8\"");
        doc.appendChild(instruction);
        QDomElement root=doc.createElement("connectbook");
        doc.appendChild(root);
        QDateTime cdt = QDateTime::currentDateTime();
        root.setAttribute("time", cdt.toString());

        //Peoples
        QDomElement PeopleList=doc.createElement("PeopleList");
        QVector<People>::Iterator iter;
        for (iter = peoples.begin();iter != peoples.end();iter++)
            {
                QDomElement Person = doc.createElement("Person");
                QDomElement Name = doc.createElement("Name");
                QDomElement firstname = doc.createElement("Firstname");
                QDomElement lastname = doc.createElement("Lastname");
                QDomElement nickname = doc.createElement("Nickname");
                firstname.setAttribute("Firstname", iter->name.firstname);
                lastname.setAttribute("Lastname", iter->name.lastname);
                nickname.setAttribute("Nickname", iter->name.nickname);
                Name.appendChild(firstname);
                Name.appendChild(lastname);
                Name.appendChild(nickname);
                Person.appendChild(Name);

                QDomElement Sex = doc.createElement("Sex");
                if(iter->sex == true)
                    Sex.setAttribute("Sex","man");
                else
                    Sex.setAttribute("Sex", "woman");
                Person.appendChild(Sex);

                QDomElement Tel = doc.createElement("Tel");
                QDomElement home_t = doc.createElement("HomeTel");
                QDomElement work_t = doc.createElement("WorkTel");
                if(iter->tel.type == Type(0))
                    work_t.setAttribute("Work_tel", iter->tel.tel);
                else
                    home_t.setAttribute("Home_tel", iter->tel.tel);
                Tel.appendChild(home_t);
                Tel.appendChild(work_t);
                Person.appendChild(Tel);

                QDomElement Adress = doc.createElement("Adress");
                QDomElement home_add = doc.createElement("HomeAdd");
                QDomElement work_add = doc.createElement("WorkAdd");
                QDomElement r = doc.createElement("r");
                QDomElement zipc = doc.createElement("zipc");
                QDomElement loc = doc.createElement("loc");
                QDomElement reg = doc.createElement("reg");
                QDomElement ctr = doc.createElement("ctr");
                r.setAttribute("road", iter->adress.road);
                zipc.setAttribute("zipcode", iter->adress.zipcode);
                loc.setAttribute("localite", iter->adress.localite);
                reg.setAttribute("region", iter->adress.region);
                ctr.setAttribute("country", iter->adress.country);
                if(iter->adress.type == Type(0))
                {
                    work_add.appendChild(r);
                    work_add.appendChild(zipc);
                    work_add.appendChild(loc);
                    work_add.appendChild(reg);
                    work_add.appendChild(ctr);
                }
                else{
                    home_add.appendChild(r);
                    home_add.appendChild(zipc);
                    home_add.appendChild(loc);
                    home_add.appendChild(reg);
                    home_add.appendChild(ctr);
                }

                QDomElement Email = doc.createElement("Email");
                Email.setAttribute("Email", iter->email);
                QDomElement Site = doc.createElement("Site");
                Site.setAttribute("Site", iter->site);
                QDomElement Note = doc.createElement("Note");
                Note.setAttribute("Note", iter->note);
                Person.appendChild(Email);
                Person.appendChild(Site);
                Person.appendChild(Note);

                PeopleList.appendChild(Person);
            }
        root.appendChild(PeopleList);


        //Organisations
        QDomElement OrganisationList=doc.createElement("OrganisationList");
        QVector<Organisation>::Iterator iter2;
        for(iter2 = organisations.begin();iter2 != organisations.end();iter2++){
            QDomElement Org = doc.createElement("Org");
            QDomElement Name = doc.createElement("Name");
            Name.setAttribute("Name", iter2->name);
            Org.appendChild(Name);

            QDomElement Site = doc.createElement("Site");
            Site.setAttribute("Site", iter2->site);
            Org.appendChild(Site);

            QDomElement Email = doc.createElement("Email");
            Email.setAttribute("Email", iter2->email);
            Org.appendChild(Email);

            QDomElement Note = doc.createElement("Note");
            Note.setAttribute("Note", iter2->note);
            Org.appendChild(Note);

            QDomElement Adress = doc.createElement("Adress");
            QDomElement r = doc.createElement("r");
            QDomElement zipc = doc.createElement("zipc");
            QDomElement loc = doc.createElement("loc");
            QDomElement reg = doc.createElement("reg");
            QDomElement ctr = doc.createElement("ctr");
            r.setAttribute("road", iter2->adress.road);
            zipc.setAttribute("zipcode", iter2->adress.zipcode);
            loc.setAttribute("localite", iter2->adress.localite);
            reg.setAttribute("region", iter2->adress.region);
            ctr.setAttribute("country", iter2->adress.country);
            Adress.appendChild(r);
            Adress.appendChild(zipc);
            Adress.appendChild(loc);
            Adress.appendChild(reg);
            Adress.appendChild(ctr);
            Org.appendChild(Adress);

            QDomElement Tel = doc.createElement("Tel");
            Tel.setAttribute("Tel", iter2->tel.tel);
            Org.appendChild(Tel);

            OrganisationList.appendChild(Org);
        }
        root.appendChild(OrganisationList);

        //Output the file stream
        QTextStream out_stream(&file);
        doc.save(out_stream,4);
        file.close();

}

void MainWindow::on_actionImport_XML_file_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                       tr("xml file"),
                                                       "./",
                                                       tr(
                                                          "本本文件(*xml)"));

    QFile file(fileName);
        QDomDocument doc;

        if(!file.open(QIODevice::ReadOnly))
            return;
        if (!doc.setContent(&file)){
            file.close();
            return;
        }
        file.close();

        QDomNode info = doc.firstChild();
        qDebug() << info.nodeName()    << info.nodeValue();

        QDomNode connectbook = info.nextSibling();
        if (connectbook.isElement()){
            QDomElement tmp = connectbook.toElement();
            qDebug()<<tmp.tagName()<<tmp.attribute("time");
        }

        QDomNode People = connectbook.firstChild();
        QDomNode person = People.firstChild();
        while (!person.isNull()) {
            struct People P;

            QDomNode Name = person.firstChild();
            QDomElement name = Name.toElement();
            P.name.firstname = name.attribute("Firstname");
            P.name.lastname = name.attribute("Lastname");
            P.name.nickname = name.attribute("Nickname");
            //qDebug()<<name.tagName()<<name.attribute("Firstname")<<name.attribute("Lastname");

            QDomNode Sex = Name.nextSibling();
            QDomElement sex = Sex.toElement();
            if(sex.attribute("Sex") == "man")
                P.sex = true;
            else
                P.sex = false;

            QDomNode Tel = Sex.nextSibling();
            QDomElement tel = Tel.toElement();
            if(tel.attribute("Type") == "home"){
                P.tel.type = Type(1);
                P.tel.tel = tel.attribute("Number");
            }
            else{
                P.tel.type = Type(0);
                P.tel.tel = tel.attribute("Number");
            }

            QDomNode Adress = Tel.nextSibling();
            QDomElement adress = Adress.toElement();
            if(adress.attribute("Type") == "home"){
                P.adress.type = Type(1);
            }
            else
                P.adress.type = Type(0);
            P.adress.road = adress.attribute("road");
            P.adress.region = adress.attribute("region");
            P.adress.zipcode = adress.attribute("zipcode");
            P.adress.localite = adress.attribute("localite");
            P.adress.country = adress.attribute("country");

            QDomNode Email = Adress.nextSibling();
            QDomElement email = Email.toElement();
            P.email = email.attribute("Email");

            QDomNode Site = Email.nextSibling();
            QDomElement site = Site.toElement();
            P.site = site.attribute("Site");

            QDomNode Note = Site.nextSibling();
            QDomElement note = Note.toElement();
            P.note = note.attribute("Note");

            peoples.push_back(P);
            QString tmp="";
            tmp.append(name.attribute("Firstname"));
            tmp.append(" ");
            tmp.append(name.attribute("Firstname"));

            insertRow(tmp, 0);
            person = person.nextSibling();
        }
        QDomNode Organisations = People.nextSibling();
        QDomNode org = Organisations.firstChild();
        while (!org.isNull()) {
            struct Organisation O;

            QDomNode Name = org.firstChild();
            QDomElement name = Name.toElement();
            O.name = name.attribute("Name");
            //Site Email Note Adress Tel

            QDomNode Site = Name.nextSibling();
            QDomElement site = Site.toElement();
            O.site = site.attribute("Site");

            QDomNode Email = Site.nextSibling();
            QDomElement email = Email.toElement();
            O.email = email.attribute("Email");

            QDomNode Note = Email.nextSibling();
            QDomElement note = Note.toElement();
            O.note = note.attribute("Note");

            QDomNode Adress = Note.nextSibling();
            QDomElement adress = Adress.toElement();
            O.adress.road = adress.attribute("road");
            O.adress.region = adress.attribute("region");
            O.adress.country = adress.attribute("country");
            O.adress.zipcode = adress.attribute("zipcode");
            O.adress.localite = adress.attribute("localite");

            QDomNode Tel = Adress.nextSibling();
            QDomElement tel = Tel.toElement();
            O.tel.tel = tel.attribute("Number");

            organisations.push_back(O);
            insertRow(name.attribute("Name"), 1);
            org = org.nextSibling();
        }
        qDebug()<<"ttt";
        for(int i = 0; i < peoples.size();i++)
        {
            qDebug()<<peoples[i].name.firstname<<" "<<peoples[i].name.lastname;
        }

}
