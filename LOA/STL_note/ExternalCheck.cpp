/*
 * ExternalCheck.cpp
 *
 *  Created on: 9 févr. 2017
 *      Author: davidroussel
 */

//#include <cstdlib>	// for system command
#include <cstdio>	// for popen
#include <iostream>	// for cerr
#include <sstream>	// for ostringstream
#include <fstream>	// for ofstream
#include <iterator>	// for ostream_iterator

#include "ExternalCheck.h"

/*
 * Protected constructor for external checker
 * @param tool the tool to launch for this checker
 * @param baseDir the base directory to run the external tool from
 * @param begin an iterator to the first value of the tested sequence
 * @param end an iterator past the last value of the tested sequence
 * @param minValue minimum possible value in the sequence
 * @param maxValue maximum possible value in the sequence
 * @param meanValue computed mean value of the sequence
 * @param stdValue computed std value of the sequence
 * @param medianValue computed median value in the sequence
 * @param useCSV use intermediate CSV file to save values before
 * checking (the check tool will then need to load this file).
 */
template<class Iterator>
ExternalCheck<Iterator>::ExternalCheck(const string & tool,
                                       const string &,
                                       const Iterator & begin,
                                       const Iterator & end,
                                       const value_t &,
                                       const value_t &,
                                       const double &,
                                       const double &,
                                       const double &,
                                       const bool useCSV) :
	tool(tool),
	availability(false),
	useCSV(useCSV)
{
	// Check if this tool is available on the path
	ostringstream oss;
	oss << "command -v " << tool;

	const int bufSize = 128;
	char buffer[bufSize];
	ostringstream resultStream;
	const char * testcommand = oss.str().c_str();

	FILE * pipe = popen(testcommand, "r");
	if (pipe)
	{
		while(fgets(buffer, bufSize, pipe) != nullptr)
		{
			resultStream << buffer;
		}

		if (!resultStream.str().empty())
		{
			availability = true;
			if (this->useCSV)
			{
				saveToCSV(begin, end);
			}
		}
//		else
//		{
//			cerr << "command " << tool << " is not available" << endl;
//		}

		pclose(pipe);
	}
	else
	{
		cerr << "popen(" << testcommand << ") failed" << endl;
	}
}

/*
 * Destructor
 */
template<class Iterator>
ExternalCheck<Iterator>::~ExternalCheck()
{
	command.clear();
}

/*
 * Save values contained in [first, last) into a CSV file
 * @param first the iterator pointing to the first element
 * @param last the iterator pointing past the last element
 * @param fileName the filename to save in
 * @param baseDir the base directory ot the file to save in
 * @param sep the separator to use in the csv file
 */
template<class Iterator>
void ExternalCheck<Iterator>::saveToCSV(const Iterator & first,
                                        const Iterator & last,
                                        const string & fileName,
                                        const string & baseDir,
                                        const string & sep)
{
	using value_type = typename iterator_traits<Iterator>::value_type;
//	using diff_type = typename iterator_traits<Iterator>::difference_type;
	string cfileName = baseDir + fileName;

	ofstream ofs(cfileName, ofstream::out);
	if (ofs.is_open())
	{
		ofs << "Values"<< sep;
		/*
		 * Note : copy algorithm does not work here because of the extra
		 * separator at the end
		 */
		copy(first, last, ostream_iterator<value_type>(ofs, sep.c_str()));
//		diff_type nbElements = last - first;
//		diff_type i;
//		Iterator it;
//		for (i = 0, it = first; i < nbElements && it != last; ++i, ++it)
//		{
//			ofs << *it;
//			if (i < (nbElements - 1))
//			{
//				ofs << sep;
//			}
//		}
		ofs << endl;
		ofs.close();
	}
	else
	{
		cerr << "saveToCSV(" << cfileName << ") : Unable to open file" << endl;
	}

}

/*
 * Check if the required tool is available
 * @return true if the required tool has been found in the path,
 * false otherwise.
 */
template<class Iterator>
bool ExternalCheck<Iterator>::available()
{
	return availability;
}

/*
 * Executes the command in a new shell and return value
 * @return the return value of the system call with "command" argument
 * @pre The tool is supposed to be available before launching the run
 */
template<class Iterator>
int ExternalCheck<Iterator>::run()
{
	const char * ccommand = command.c_str();

//	cout << "Check::run(" << command << ")" << endl;

	int ret = 0;
	if (system(NULL))
	{
		ret = system(ccommand);
	}
	else
	{
		cerr << "Could not find command interpreter on your system, sorry"
		     << endl;
	}
	return ret;
}

// ----------------------------------------------------------------------------
// Proto instanciations
// ----------------------------------------------------------------------------
#include <vector>
#include <deque>
/*
 * Instanciation du template ExternalCheck avec des <vector::const_iterator>
 */
template class ExternalCheck<std::vector<int>::const_iterator>;
template class ExternalCheck<std::deque<int>::const_iterator>;

/*
 * Instanciation du template ExternalCheck avec des <vector::const_iterator>
 */
template class ExternalCheck<std::vector<float>::const_iterator>;
template class ExternalCheck<std::deque<float>::const_iterator>;

/*
 * Instanciation du template ExternalCheck avec des <vector::const_iterator>
 */
template class ExternalCheck<std::vector<double>::const_iterator>;
template class ExternalCheck<std::deque<double>::const_iterator>;
