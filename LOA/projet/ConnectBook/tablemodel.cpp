#include "tablemodel.h"
#include <QDebug>
#include <QLabel>

Tablemodel::Tablemodel( int i, bool addF,QObject *parent)
    :QAbstractTableModel (parent),addFlag(addF),m_index(i)
{

}
Tablemodel::~Tablemodel(){}

int Tablemodel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    if(!addFlag) return 10;
    return 15;
}

int Tablemodel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 2;
}

QVariant Tablemodel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }
    if (index.row() < 0 || index.row() >= 15)
    {
        return QVariant();
    }
    if (index.row() < 0 || index.column() >= 2)
    {
        return QVariant();
    }
    if (role == Qt::TextAlignmentRole)
        {
            QVariant value = (Qt::AlignCenter);
            return value;
        }

    if(addFlag)//people table
    {
        if(role == Qt::DecorationRole)
        {
            const auto &p = peoples.at(m_index);
            if(index.row()>=0 && index.row() < 3 && index.column() == 0)
            {
                return QIcon(":/images/name.png");
            }
            else if(index.row() == 3)
            {
                if(index.column() == 0) return QIcon(":/images/gender.png");

                if(p.sex) return QIcon(":/images/male.png");
                return QIcon(":/images/female.png");
            }
            else if(index.row() == 4 && index.column() == 0)
            {
                if(p.tel.type == 0) return QIcon(":/images/work.png");
                return QIcon(":/images/home.png");
            }
            else if(index.row() == 5 && index.column() == 0)
            {
                return QIcon(":/images/email.png");
            }
            else if(index.row() == 6 && index.column() == 0)
            {
                return QIcon(":/images/site.png");
            }
            else if(index.row() >=7 && index.row() <=12)
            {
                if(index.row() == 7 && index.column() ==1){

                    if(p.adress.type == 0) return QIcon(":/images/work.png");
                    return QIcon(":/images/home.png");
                }
                if(index.column() == 0)
                    return QIcon(":/images/adress.png");
            }
            else if(index.row() == 13 && index.column() == 0){
                return QIcon(":/images/note.png");
            }

        }
        if (role == Qt::DisplayRole) {
            if(peoples.size() == 0)
                return tr("1");
            else
            {
                const auto &p = peoples.at(m_index);

                switch (index.row()) {
                case 0:
                    if(index.column() != 1) break;
                    return p.name.firstname;
                case 1:
                    if(index.column() != 1) break;
                    return p.name.lastname;
                case 2:
                    if(index.column() != 1) break;
                    return p.name.nickname;

                case 4:
                    if(index.column() == 1)
                        return p.tel.tel;
                    if(p.tel.type == 0)
                        return tr("work");
                    else {
                        return tr("home");
                    }
                case 5:
                    if(index.column() != 1) break;
                    return p.email;
                case 6:
                    if(index.column() != 1) break;
                    return p.site;

                case 7:
                    if(index.column() !=1) break;
                    if(p.adress.type == 0)
                        return tr("work");
                    else {
                        return tr("home");
                    }
                case 8:
                    if(index.column() != 1) break;
                    return p.adress.road;
                case 9:
                    if(index.column() != 1) break;
                    return p.adress.zipcode;
                case 10:
                    if(index.column() != 1) break;
                    return p.adress.localite;
                case 11:
                    if(index.column() != 1) break;
                    return p.adress.region;
                case 12:
                    if(index.column() != 1) break;
                    return p.adress.country;
                case 13:
                    if(index.column() != 1) break;

                    return p.note;
                case 14:
                    if(index.column() !=1) break;

                    return p.o.name;

                }
            }
        }
    }
    else {//organisation table
        if(role == Qt::DecorationRole)
        {

            if(index.row() == 0  && index.column() == 0)
            {
                return QIcon(":/images/name.png");
            }
            else if(index.row() == 1 && index.column() == 0)
            {

                return QIcon(":/images/phone.png");
            }
            else if(index.row() == 2 && index.column() == 0)
            {
                return QIcon(":/images/email.png");
            }
            else if(index.row() == 3 && index.column() == 0)
            {
                return QIcon(":/images/site.png");
            }
            else if(index.row() >=4 && index.row() <=8)
            {

                if(index.column() == 0)
                    return QIcon(":/images/adress.png");
            }
            else if(index.row() == 10 && index.column() == 0){
                return QIcon(":/images/note.png");
            }

        }
        if (role == Qt::DisplayRole) {
            if(organisation.size() == 0)
                return tr("sadsad");
            else
            {
                const auto &o = organisation.at(m_index);

                switch (index.row()) {
                case 0:
                    if(index.column() != 1) break;
                    return o.name;
                case 1:
                    if(index.column() == 1)
                        return o.tel.tel;
                    break;
                case 2:
                    if(index.column() != 1) break;
                    return o.email;
                case 3:
                    if(index.column() != 1) break;
                    return o.site;
                case 4:
                    if(index.column() != 1) break;
                    return o.adress.road;
                case 5:
                    if(index.column() != 1) break;
                    return o.adress.zipcode;
                case 6:
                    if(index.column() != 1) break;
                    return o.adress.localite;
                case 7:
                    return o.adress.region;
                case 8:
                    return o.adress.country;
                case 9:
                    if(index.column() != 1) break;
                    return o.note;
                    //case 9:
                    //return o->name.lastname;
                default:
                    break;
                }
            }
        }
    }
    return QVariant();
}

QVariant Tablemodel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();
    if(addFlag)
    {
        if(orientation == Qt::Vertical){
            switch (section) {
            case 0:
                return tr("First name");
            case 1:
                return tr("Last name");
            case 2:
                return tr("Nick name");
            case 3:
                return tr("Sex");
            case 4:
                return tr("Tel");
            case 5:
                return tr("E-mail");
            case 6:
                return tr("site");
            case 7:
                return tr("Adress");
            case 8:
                return tr("Road");
            case 9:
                return tr("Zip code");
            case 10:
                return tr("localite");
            case 11:
                return tr("Region");
            case 12:
                return tr("Country");
            case 13:
                return tr("Note");
            case 14:
                return tr("Organisation");

            default:
                break;
            }
        }
    }
    else {
        if(orientation == Qt::Vertical){
            switch (section) {
            case 0:
                return tr("Name");
            case 1:
                return tr("Tel");
            case 2:
                return tr("E-mail");
            case 3:
                return tr("site");
            case 4:
                return tr("Road");
            case 5:
                return tr("Zip code");
            case 6:
                return tr("localite");
            case 7:
                return tr("Region");
            case 8:
                return tr("Country");
            case 9:
                return tr("Note");


            default:
                break;
            }
        }
    }
    return QVariant();

}

bool Tablemodel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    Q_UNUSED(role);
    if(!index.isValid())
        return true;
    if(addFlag)
    {
        if(peoples.size() == 0)
            return true;
        else
        {
            switch (index.row()) {
            case 0:
                peoples[m_index].name.firstname = value.toString();
                return true;
            case 1:
                peoples[m_index].name.lastname = value.toString();
                return true;
            case 2:
                peoples[m_index].name.nickname = value.toString();
                return true;
            case 3:
                peoples[m_index].sex = value.toInt() == 0? true:false;

                return true;
            case 4:
                if(index.column() == 0)
                {peoples[m_index].tel.type = value.toInt()== 0?home:work;
                qDebug()<<peoples[m_index].tel.type<<"tel";}
                else
                { peoples[m_index].tel.tel = value.toString();
                qDebug()<<peoples[m_index].tel.type<<"tel";}

                return true;
            case 5:
                peoples[m_index].email = value.toString();
                return true;
            case 6:
                peoples[m_index].site = value.toString();
                return true;
            case 7:
                peoples[m_index].adress.type = value.toInt()==0?home:work;
                return true;
            case 8:
                peoples[m_index].adress.road = value.toString();
                return true;
            case 9:
                peoples[m_index].adress.zipcode = value.toString();
                return true;
            case 10:
                peoples[m_index].adress.localite = value.toString();
                return true;
            case 11:
                peoples[m_index].adress.region = value.toString();
                return true;
            case 12:
                peoples[m_index].adress.country = value.toString();
                return true;
            case 13:
                peoples[m_index].note = value.toString();
                return true;
            case 14:
                peoples[m_index].o.name = value.toString();
                return true;

            }
        }
    }
    else {
        if(organisation.size() == 0)
            return true;
        else
        {
            switch (index.row()) {
            case 0:
                organisation[m_index].name = value.toString();
                return true;
            case 1:
                organisation[m_index].tel.tel = value.toString();
                return true;
            case 2:
                organisation[m_index].email = value.toString();
                return true;
            case 3:
                organisation[m_index].site = value.toString();
                return true;

            case 4:
                organisation[m_index].adress.road = value.toString();
                return true;
            case 5:
                organisation[m_index].adress.zipcode = value.toString();
                return true;
            case 6:
                organisation[m_index].adress.localite = value.toString();
                return true;
            case 7:
                organisation[m_index].adress.region = value.toString();
                return true;
            case 8:
                organisation[m_index].adress.country = value.toString();
                return true;
            case 9:
                organisation[m_index].note = value.toString();
                return true;

                return true;

            }
        }
    }
    return false;
}

Qt::ItemFlags Tablemodel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::ItemIsEnabled;
    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;

}
QVector<People> Tablemodel::getPeoples() const
{
    return peoples;
}



