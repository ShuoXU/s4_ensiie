#include <cassert>
#include <algorithm>	// for min

#include <QDebug>

#include "QcvCaptureWorker.h"

/*
 * The default delay (in ms) used in this worker
 */
const unsigned long QcvCaptureWorker::defaultDelay = static_cast<unsigned long>(
	round(1000.0 / fabs(QcvThreadedVideoCapture::defaultConstrainedFrameRate)));

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * Constructor from capture instance and parent
 * @param capture the capture instance to work on which is also a parent
 * object
 * @param synchronized indicates this worker uses a QTimer to pace
 * the updates
 * @param delay the delay (in ms) used by the QTimer in synchronized mode
 */
QcvCaptureWorker::QcvCaptureWorker(QcvThreadedVideoCapture * capture,
								   const bool synchronized,
								   const unsigned long delay) :
	QThread(capture),
	qCapture(capture),
	timeMeasure(),
	synchronized(synchronized),
	delay(delay)
{
	assert(capture != nullptr);
	assert(delay >= 0);
}

/*
 * Destructor
 */
QcvCaptureWorker::~QcvCaptureWorker()
{
}

/*
 * This thread override run method
 * @post During the update the following signals have been emitted:
 *	- The capture's update method emits the following signals:
 *		- if new image is available updated() is emitted
 *	- If capture frame rate changed capture's emitFrameRateChanged is
 *	emitted
 */
void QcvCaptureWorker::run()
{
	timeMeasure.start();

	bool updated = true;
	long overdue;

	while (updated)
	{
		updated = qCapture->update();

		if (qCapture->updateCondition != nullptr)
		{
			qCapture->updateCondition->wakeAll();
		}

		if (updated)
		{
			overdue = 0;
			unsigned long elapsed = timeMeasure.elapsed();
			double previousFrameRate = qCapture->frameRate;
			if (elapsed != 0l)
			{
				qCapture->frameRate = 1000.0 / static_cast<double>(elapsed);
				if (qCapture->frameRate != previousFrameRate)
				{
					qCapture->emitFrameRateChanged();
				}
			}

			if (synchronized)
			{
				elapsed = timeMeasure.elapsed();
				if (elapsed > delay)
				{
					overdue = static_cast<long>(elapsed - delay);
				}
			}

			timeMeasure.restart();

			if (synchronized)
			{
				if (overdue < static_cast<long>(delay))
				{
//					qDebug() << "sleep = " << (delay - overdue);
					msleep(delay - overdue);
				}
			}
		}
	}

//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO << "terminated";

}

/*
 * Gets the synchronized state
 * @return the synchronized state
 */
bool QcvCaptureWorker::isSynchronized() const
{
	return synchronized;
}

/*
 * Sets this worker to synchronized mode or not.
 * When the worker is already running the mode will apply to the NEXT
 * run evt driven by a QTimer.
 * @param synchronized the new synchronized state
 */
void QcvCaptureWorker::setSynchronized(const bool synchronized)
{
	if (this->synchronized != synchronized)
	{
		qCapture->mutex.lock();
		this->synchronized = synchronized;
		qCapture->mutex.unlock();
	}
}

/*
 * Gets the delay (in ms) between frames in synchronized mode
 * @return the delay (in ms) between frames in synchronized mode
 */
int QcvCaptureWorker::getDelay() const
{
	return delay;
}

/*
 * Sets the delay (in ms) for the timer when worker is in synchronized
 * mode. If the worker is already running the delay will be applied on
 * the NEXT run.
 * @param value the new delay value.
 */
void QcvCaptureWorker::setDelay(const int value)
{
	if (static_cast<unsigned long>(value) != delay)
	{
		qCapture->mutex.lock();
		this->delay = abs(value);
		qCapture->mutex.unlock();
	}
}
