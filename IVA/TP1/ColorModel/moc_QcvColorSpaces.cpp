/****************************************************************************
** Meta object code from reading C++ file 'QcvColorSpaces.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "QcvColorSpaces.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QcvColorSpaces.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QcvColorSpaces_t {
    QByteArrayData data[20];
    char stringdata0[307];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QcvColorSpaces_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QcvColorSpaces_t qt_meta_stringdata_QcvColorSpaces = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QcvColorSpaces"
QT_MOC_LITERAL(1, 15, 18), // "SNRChannel0Changed"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 5), // "value"
QT_MOC_LITERAL(4, 41, 18), // "SNRChannel1Changed"
QT_MOC_LITERAL(5, 60, 18), // "SNRChannel2Changed"
QT_MOC_LITERAL(6, 79, 6), // "update"
QT_MOC_LITERAL(7, 86, 20), // "setDisplayImageIndex"
QT_MOC_LITERAL(8, 107, 5), // "index"
QT_MOC_LITERAL(9, 113, 16), // "setChannelsColor"
QT_MOC_LITERAL(10, 130, 6), // "status"
QT_MOC_LITERAL(11, 137, 17), // "setHueDisplayMode"
QT_MOC_LITERAL(12, 155, 4), // "mode"
QT_MOC_LITERAL(13, 160, 20), // "getDisplayController"
QT_MOC_LITERAL(14, 181, 16), // "QEnumController*"
QT_MOC_LITERAL(15, 198, 25), // "getChannelsModeController"
QT_MOC_LITERAL(16, 224, 16), // "QBoolController*"
QT_MOC_LITERAL(17, 241, 23), // "getHueDisplayController"
QT_MOC_LITERAL(18, 265, 20), // "getSNRSizeController"
QT_MOC_LITERAL(19, 286, 20) // "QRangeIntController*"

    },
    "QcvColorSpaces\0SNRChannel0Changed\0\0"
    "value\0SNRChannel1Changed\0SNRChannel2Changed\0"
    "update\0setDisplayImageIndex\0index\0"
    "setChannelsColor\0status\0setHueDisplayMode\0"
    "mode\0getDisplayController\0QEnumController*\0"
    "getChannelsModeController\0QBoolController*\0"
    "getHueDisplayController\0getSNRSizeController\0"
    "QRangeIntController*"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QcvColorSpaces[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   69,    2, 0x06 /* Public */,
       4,    1,   72,    2, 0x06 /* Public */,
       5,    1,   75,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   78,    2, 0x0a /* Public */,
       7,    1,   79,    2, 0x0a /* Public */,
       9,    1,   82,    2, 0x0a /* Public */,
      11,    1,   85,    2, 0x0a /* Public */,
      13,    0,   88,    2, 0x0a /* Public */,
      15,    0,   89,    2, 0x0a /* Public */,
      17,    0,   90,    2, 0x0a /* Public */,
      18,    0,   91,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void, QMetaType::Bool,   10,
    QMetaType::Void, QMetaType::Int,   12,
    0x80000000 | 14,
    0x80000000 | 16,
    0x80000000 | 14,
    0x80000000 | 19,

       0        // eod
};

void QcvColorSpaces::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QcvColorSpaces *_t = static_cast<QcvColorSpaces *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SNRChannel0Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->SNRChannel1Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->SNRChannel2Changed((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->update(); break;
        case 4: _t->setDisplayImageIndex((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 5: _t->setChannelsColor((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 6: _t->setHueDisplayMode((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 7: { QEnumController* _r = _t->getDisplayController();
            if (_a[0]) *reinterpret_cast< QEnumController**>(_a[0]) = std::move(_r); }  break;
        case 8: { QBoolController* _r = _t->getChannelsModeController();
            if (_a[0]) *reinterpret_cast< QBoolController**>(_a[0]) = std::move(_r); }  break;
        case 9: { QEnumController* _r = _t->getHueDisplayController();
            if (_a[0]) *reinterpret_cast< QEnumController**>(_a[0]) = std::move(_r); }  break;
        case 10: { QRangeIntController* _r = _t->getSNRSizeController();
            if (_a[0]) *reinterpret_cast< QRangeIntController**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QcvColorSpaces::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvColorSpaces::SNRChannel0Changed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QcvColorSpaces::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvColorSpaces::SNRChannel1Changed)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QcvColorSpaces::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvColorSpaces::SNRChannel2Changed)) {
                *result = 2;
                return;
            }
        }
    }
}

const QMetaObject QcvColorSpaces::staticMetaObject = {
    { &QcvProcessor::staticMetaObject, qt_meta_stringdata_QcvColorSpaces.data,
      qt_meta_data_QcvColorSpaces,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QcvColorSpaces::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QcvColorSpaces::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QcvColorSpaces.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "CvColorSpaces"))
        return static_cast< CvColorSpaces*>(this);
    return QcvProcessor::qt_metacast(_clname);
}

int QcvColorSpaces::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QcvProcessor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void QcvColorSpaces::SNRChannel0Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QcvColorSpaces::SNRChannel1Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QcvColorSpaces::SNRChannel2Changed(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
