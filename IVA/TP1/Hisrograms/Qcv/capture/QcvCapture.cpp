/*
 * QcvCapture.cpp
 *
 *  Created on: 12 sep. 2017
 *	  Author: davidroussel
 */

#include <QDebug>
#include <QTime>	// for time in debug messages
#include <QThread>	// for thread id in debug messages

#include "QcvCapture.h"

/*
 * Delay during wich sent message will be shown (5 seconds)
 */
int QcvCapture::messageDelay = 5000;

/*
 * The default frame rate for synchronized capture
 */
const double QcvCapture::defaultFrameRate = 30.0;

/*
 * Valued and protected constructor (to be used in subclasses)
 * @param flipped indicate if captured image must be flipped horizontally
 * @param gray indicate if captured image must be converted to gray
 * @param width desired width or 0 to keep capture natural width
 * @param height desired height or 0 to keep capture natural height
 * @param parent the parent QObject
 */
QcvCapture::QcvCapture(const bool flipped,
					   const bool gray,
					   const size_t width,
					   const size_t height,
					   QObject *parent) :
	QObject(parent),
	// mutex is default constructed
	// image is default constructed
	// imageResized is default constructed
	// imageFlipped is default constructed
	// imageDisplay is default constructed,
	flipped(flipped),
	gray(gray),
	resized(false), // will be changed in subclasses when images are available
	size(0, 0), // will be changed in subclasses when images are available
	originalSize(0, 0), // will be changed in subclasses when images are available
	frameRate(0.0)
	// statusMessage is default constructed
{
	/*
	 * Width and height are not used yet, they will be used in  subclasses
	 */
	Q_UNUSED(width);
	Q_UNUSED(height);
}

/*
 * Destructor.
 * releases image(s)
 */
QcvCapture::~QcvCapture()
{
	statusMessage.clear();
	imageDisplay.release();
	imageFlipped.release();
	imageResized.release();
	image.release();
}

/*
 * Gets the mutex used to ensure capture atomic access
 * @return the mutex used to ensure capture atomic access
 * @see #mutex
 */
QMutex * QcvCapture::getMutex()
{
	return &mutex;
}

/*
 * Get the current "display" image (evt after gray conversion,
 * flipping and resizing)
 * @return the current "display" image
 * @see #imageDisplay
 */
cv::Mat * QcvCapture::getImage()
{
	return &imageDisplay;
}

/*
 * Get the "flipped" status of the captured video
 * @return the "flipped" status of the video
 * @see #flipped
 */
bool QcvCapture::isFlipped() const
{
	return flipped;
}

/*
 * Get the gray conversion status of the captured video
 * @return the gray conversion status of the video
 * @see #gray
 */
bool QcvCapture::isGray() const
{
	return gray;
}

/*
 * Get the "resized" status of the captured video
 * @return the "resized" status of the video
 * @see #resized
 */
bool QcvCapture::isResized() const
{
	return resized;
}

/*
 * Get the current size of captured image
 * @return the current size of captured image
 */
const QSize & QcvCapture::getSize() const
{
	return size;
}

/*
 * Get the original capture image size (without resizing)
 * @return the original capture image size (without resizing)
 */
const QSize & QcvCapture::getOriginalSize() const
{
	return originalSize;
}

/*
 * Get the current capture frame rate
 * @return the current capture frame rate
 */
double QcvCapture::getFrameRate() const
{
	return frameRate;
}

/*
 * Sets the "flipped" status of captured video
 * @param value the new "flipped" status
 */
void QcvCapture::setFlipped(const bool value)
{
	if (flipped != value)
	{
		mutex.lock();

		flipped = value;

		mutex.unlock();
	}
}

/*
 * Sets the gray conversion status of captured video
 * @param value the new value of gray conversion status
 */
void QcvCapture::setGray(const bool value)
{
	if (gray != value)
	{
		mutex.lock();

		gray = value;

		mutex.unlock();
	}
}

/*
 * Sets the desired size of captured image
 * @param width the desired width of captured image
 * @param height the desired height of captured image
 * @see #size
 * @post #imageChanged(); signal is emitted
 */
void QcvCapture::setSize(const QSize & size)
{
	setSize_Impl(static_cast<size_t>(abs(size.width())),
				 static_cast<size_t>(abs(size.height())));
	emitImageChanged();
}

/*
 * Wrapper method to emit updated signal for other classes to use
 * @see #updated()
 */
void QcvCapture::emitUpdated()
{
	emit updated();
}

/*
 * Wrapper method to emit message changed signal for other classes to use
 * @see #messageChanged(const QString & message, int timeout);
 */
void QcvCapture::emitMessageChanged()
{
	emit messageChanged(statusMessage, messageDelay);
}

/*
 * Wrapper method to emit image changed signal for other classes to use
 * @see #imageChanged(Mat * image);
 */
void QcvCapture::emitImageChanged()
{
	emit imageChanged(&imageDisplay);
}

/*
 * Emits frame rate changed for other classes to use
 * @see #frameRateChanged();
 */
void QcvCapture::emitFrameRateChanged()
{
	emit frameRateChanged(frameRate);
}
