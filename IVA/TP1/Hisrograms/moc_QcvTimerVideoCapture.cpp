/****************************************************************************
** Meta object code from reading C++ file 'QcvTimerVideoCapture.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Qcv/capture/QcvTimerVideoCapture.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QcvTimerVideoCapture.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QcvTimerVideoCapture_t {
    QByteArrayData data[22];
    char stringdata0[209];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QcvTimerVideoCapture_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QcvTimerVideoCapture_t qt_meta_stringdata_QcvTimerVideoCapture = {
    {
QT_MOC_LITERAL(0, 0, 20), // "QcvTimerVideoCapture"
QT_MOC_LITERAL(1, 21, 12), // "timerChanged"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 5), // "delay"
QT_MOC_LITERAL(4, 41, 9), // "stopTimer"
QT_MOC_LITERAL(5, 51, 10), // "openDevice"
QT_MOC_LITERAL(6, 62, 8), // "deviceId"
QT_MOC_LITERAL(7, 71, 6), // "size_t"
QT_MOC_LITERAL(8, 78, 5), // "width"
QT_MOC_LITERAL(9, 84, 6), // "height"
QT_MOC_LITERAL(10, 91, 8), // "openFile"
QT_MOC_LITERAL(11, 100, 8), // "fileName"
QT_MOC_LITERAL(12, 109, 13), // "openDirectory"
QT_MOC_LITERAL(13, 123, 7), // "dirName"
QT_MOC_LITERAL(14, 131, 10), // "setFlipped"
QT_MOC_LITERAL(15, 142, 5), // "value"
QT_MOC_LITERAL(16, 148, 7), // "setGray"
QT_MOC_LITERAL(17, 156, 15), // "setSynchronized"
QT_MOC_LITERAL(18, 172, 12), // "setFrameRate"
QT_MOC_LITERAL(19, 185, 9), // "frameRate"
QT_MOC_LITERAL(20, 195, 6), // "finish"
QT_MOC_LITERAL(21, 202, 6) // "update"

    },
    "QcvTimerVideoCapture\0timerChanged\0\0"
    "delay\0stopTimer\0openDevice\0deviceId\0"
    "size_t\0width\0height\0openFile\0fileName\0"
    "openDirectory\0dirName\0setFlipped\0value\0"
    "setGray\0setSynchronized\0setFrameRate\0"
    "frameRate\0finish\0update"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QcvTimerVideoCapture[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   99,    2, 0x06 /* Public */,
       4,    0,  102,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    3,  103,    2, 0x0a /* Public */,
       5,    2,  110,    2, 0x2a /* Public | MethodCloned */,
       5,    1,  115,    2, 0x2a /* Public | MethodCloned */,
      10,    3,  118,    2, 0x0a /* Public */,
      10,    2,  125,    2, 0x2a /* Public | MethodCloned */,
      10,    1,  130,    2, 0x2a /* Public | MethodCloned */,
      12,    3,  133,    2, 0x0a /* Public */,
      12,    2,  140,    2, 0x2a /* Public | MethodCloned */,
      12,    1,  145,    2, 0x2a /* Public | MethodCloned */,
      14,    1,  148,    2, 0x0a /* Public */,
      16,    1,  151,    2, 0x0a /* Public */,
      17,    1,  154,    2, 0x0a /* Public */,
      18,    1,  157,    2, 0x0a /* Public */,
      20,    0,  160,    2, 0x0a /* Public */,
      21,    0,  161,    2, 0x09 /* Protected */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 7, 0x80000000 | 7,    6,    8,    9,
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 7,    6,    8,
    QMetaType::Bool, QMetaType::Int,    6,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 7, 0x80000000 | 7,   11,    8,    9,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 7,   11,    8,
    QMetaType::Bool, QMetaType::QString,   11,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 7, 0x80000000 | 7,   13,    8,    9,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 7,   13,    8,
    QMetaType::Bool, QMetaType::QString,   13,
    QMetaType::Void, QMetaType::Bool,   15,
    QMetaType::Void, QMetaType::Bool,   15,
    QMetaType::Void, QMetaType::Bool,   15,
    QMetaType::Void, QMetaType::Double,   19,
    QMetaType::Void,
    QMetaType::Bool,

       0        // eod
};

void QcvTimerVideoCapture::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QcvTimerVideoCapture *_t = static_cast<QcvTimerVideoCapture *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->timerChanged((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 1: _t->stopTimer(); break;
        case 2: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 3: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 5: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 7: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 8: { bool _r = _t->openDirectory((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 9: { bool _r = _t->openDirectory((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 10: { bool _r = _t->openDirectory((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 11: _t->setFlipped((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 12: _t->setGray((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 13: _t->setSynchronized((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 14: _t->setFrameRate((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 15: _t->finish(); break;
        case 16: { bool _r = _t->update();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QcvTimerVideoCapture::*_t)(const int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvTimerVideoCapture::timerChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QcvTimerVideoCapture::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvTimerVideoCapture::stopTimer)) {
                *result = 1;
                return;
            }
        }
    }
}

const QMetaObject QcvTimerVideoCapture::staticMetaObject = {
    { &QcvVideoCapture::staticMetaObject, qt_meta_stringdata_QcvTimerVideoCapture.data,
      qt_meta_data_QcvTimerVideoCapture,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QcvTimerVideoCapture::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QcvTimerVideoCapture::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QcvTimerVideoCapture.stringdata0))
        return static_cast<void*>(this);
    return QcvVideoCapture::qt_metacast(_clname);
}

int QcvTimerVideoCapture::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QcvVideoCapture::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}

// SIGNAL 0
void QcvTimerVideoCapture::timerChanged(const int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void QcvTimerVideoCapture::stopTimer()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
