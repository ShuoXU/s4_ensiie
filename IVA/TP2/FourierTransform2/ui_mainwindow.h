/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <Qcv/matWidgets/QcvMatWidget.h>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionCamera_0;
    QAction *actionCamera_1;
    QAction *actionFile;
    QAction *actionFlip;
    QAction *actionOriginalSize;
    QAction *actionConstrainedSize;
    QAction *actionQuit;
    QAction *actionRenderImage;
    QAction *actionRenderPixmap;
    QAction *actionRenderOpenGL;
    QAction *actionGray;
    QAction *actionFilterBox;
    QAction *actionFilterGaussian;
    QAction *actionFilterSinc;
    QAction *actionLink_Filters;
    QAction *actionFiltering;
    QAction *actionSwitchTab;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QSplitter *splitter_2;
    QSplitter *splitter;
    QScrollArea *scrollAreaSource;
    QcvMatWidget *sourceImage;
    QScrollArea *scrollAreaSpectrum;
    QcvMatWidget *spectrumImage;
    QScrollArea *scrollAreaInverse;
    QcvMatWidget *inverseImage;
    QTabWidget *tabWidgetFilters;
    QWidget *tabImage;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBoxSize;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_6;
    QRadioButton *radioButtonOrigSize;
    QRadioButton *radioButtonCustomSize;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelWidth;
    QSpinBox *spinBoxWidth;
    QHBoxLayout *horizontalLayout_3;
    QLabel *labelHeight;
    QSpinBox *spinBoxHeight;
    QCheckBox *checkBoxAR;
    QGroupBox *groupBoxConv;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *checkBoxFlip;
    QCheckBox *checkBoxGray;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBoxFFT;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_5;
    QLabel *labelMag;
    QDoubleSpinBox *spinBoxMag;
    QHBoxLayout *horizontalLayout_6;
    QLabel *labelFFTSizeTitle;
    QLabel *labelFFTSizeValue;
    QGroupBox *groupBoxFilter;
    QVBoxLayout *verticalLayout_3;
    QCheckBox *checkBoxFiltering;
    QRadioButton *radioButtonFilterBox;
    QRadioButton *radioButtonFilterGauss;
    QRadioButton *radioButtonFilterSinc;
    QGroupBox *groupBoxTimes;
    QGridLayout *gridLayout;
    QLabel *labelDelayUnit;
    QLabel *labelFrameRateUnit;
    QLabel *labelProcessingTimeUnit;
    QLabel *labelDelay;
    QLabel *labelProcessingTimeTitle;
    QLabel *labelProcessingTime;
    QLabel *labelFrameRateTitle;
    QLabel *labelDelayTitle;
    QLabel *labelFrameRate;
    QLabel *labelProcessingRateTitle;
    QLabel *labelProcessingRate;
    QLabel *labelProcessingRateUnit;
    QLabel *labelCapture;
    QLabel *labelProcessing;
    QSpacerItem *horizontalSpacer;
    QWidget *tabLowPass;
    QGridLayout *gridLayout_2;
    QLabel *labelRedLP;
    QSlider *horizontalSliderRLP;
    QLabel *labelGreenLP;
    QSlider *horizontalSliderGLP;
    QLabel *labelBlueLP;
    QSlider *horizontalSliderBLP;
    QSpinBox *spinBoxRedLP;
    QSpinBox *spinBoxGreenLP;
    QSpinBox *spinBoxBlueLP;
    QCheckBox *checkBoxLinkLP;
    QWidget *tabHighPass;
    QGridLayout *gridLayout_3;
    QLabel *labelRedHP;
    QSlider *horizontalSliderRHP;
    QSpinBox *spinBoxRedHP;
    QLabel *labelGreenHP;
    QSlider *horizontalSliderGHP;
    QSpinBox *spinBoxGreenHP;
    QLabel *labelBlueHP;
    QSlider *horizontalSliderBHP;
    QSpinBox *spinBoxBlueHP;
    QCheckBox *checkBoxLinkHP;
    QMenuBar *menuBar;
    QMenu *menuSources;
    QMenu *menuVideo;
    QMenu *menuSize;
    QMenu *menuRender;
    QMenu *menuFilter;
    QMenu *menuFilterTypes;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(853, 470);
        actionCamera_0 = new QAction(MainWindow);
        actionCamera_0->setObjectName(QStringLiteral("actionCamera_0"));
        actionCamera_1 = new QAction(MainWindow);
        actionCamera_1->setObjectName(QStringLiteral("actionCamera_1"));
        actionFile = new QAction(MainWindow);
        actionFile->setObjectName(QStringLiteral("actionFile"));
        actionFlip = new QAction(MainWindow);
        actionFlip->setObjectName(QStringLiteral("actionFlip"));
        actionFlip->setCheckable(true);
        actionOriginalSize = new QAction(MainWindow);
        actionOriginalSize->setObjectName(QStringLiteral("actionOriginalSize"));
        actionOriginalSize->setCheckable(true);
        actionConstrainedSize = new QAction(MainWindow);
        actionConstrainedSize->setObjectName(QStringLiteral("actionConstrainedSize"));
        actionConstrainedSize->setCheckable(true);
        actionQuit = new QAction(MainWindow);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        actionRenderImage = new QAction(MainWindow);
        actionRenderImage->setObjectName(QStringLiteral("actionRenderImage"));
        actionRenderImage->setCheckable(true);
        actionRenderImage->setChecked(true);
        actionRenderPixmap = new QAction(MainWindow);
        actionRenderPixmap->setObjectName(QStringLiteral("actionRenderPixmap"));
        actionRenderPixmap->setCheckable(true);
        actionRenderOpenGL = new QAction(MainWindow);
        actionRenderOpenGL->setObjectName(QStringLiteral("actionRenderOpenGL"));
        actionRenderOpenGL->setCheckable(true);
        actionGray = new QAction(MainWindow);
        actionGray->setObjectName(QStringLiteral("actionGray"));
        actionGray->setCheckable(true);
        actionFilterBox = new QAction(MainWindow);
        actionFilterBox->setObjectName(QStringLiteral("actionFilterBox"));
        actionFilterBox->setCheckable(true);
        actionFilterGaussian = new QAction(MainWindow);
        actionFilterGaussian->setObjectName(QStringLiteral("actionFilterGaussian"));
        actionFilterGaussian->setCheckable(true);
        actionFilterSinc = new QAction(MainWindow);
        actionFilterSinc->setObjectName(QStringLiteral("actionFilterSinc"));
        actionFilterSinc->setCheckable(true);
        actionLink_Filters = new QAction(MainWindow);
        actionLink_Filters->setObjectName(QStringLiteral("actionLink_Filters"));
        actionFiltering = new QAction(MainWindow);
        actionFiltering->setObjectName(QStringLiteral("actionFiltering"));
        actionSwitchTab = new QAction(MainWindow);
        actionSwitchTab->setObjectName(QStringLiteral("actionSwitchTab"));
        actionSwitchTab->setVisible(true);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(8, 8, 8, 8);
        splitter_2 = new QSplitter(centralWidget);
        splitter_2->setObjectName(QStringLiteral("splitter_2"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(splitter_2->sizePolicy().hasHeightForWidth());
        splitter_2->setSizePolicy(sizePolicy);
        splitter_2->setOrientation(Qt::Horizontal);
        splitter_2->setHandleWidth(7);
        splitter = new QSplitter(splitter_2);
        splitter->setObjectName(QStringLiteral("splitter"));
        sizePolicy.setHeightForWidth(splitter->sizePolicy().hasHeightForWidth());
        splitter->setSizePolicy(sizePolicy);
        splitter->setOrientation(Qt::Horizontal);
        splitter->setHandleWidth(7);
        scrollAreaSource = new QScrollArea(splitter);
        scrollAreaSource->setObjectName(QStringLiteral("scrollAreaSource"));
        sizePolicy.setHeightForWidth(scrollAreaSource->sizePolicy().hasHeightForWidth());
        scrollAreaSource->setSizePolicy(sizePolicy);
        scrollAreaSource->setWidgetResizable(true);
        scrollAreaSource->setAlignment(Qt::AlignCenter);
        sourceImage = new QcvMatWidget();
        sourceImage->setObjectName(QStringLiteral("sourceImage"));
        sourceImage->setGeometry(QRect(0, 0, 271, 222));
        scrollAreaSource->setWidget(sourceImage);
        splitter->addWidget(scrollAreaSource);
        scrollAreaSpectrum = new QScrollArea(splitter);
        scrollAreaSpectrum->setObjectName(QStringLiteral("scrollAreaSpectrum"));
        scrollAreaSpectrum->setWidgetResizable(true);
        scrollAreaSpectrum->setAlignment(Qt::AlignCenter);
        spectrumImage = new QcvMatWidget();
        spectrumImage->setObjectName(QStringLiteral("spectrumImage"));
        spectrumImage->setGeometry(QRect(0, 0, 271, 222));
        scrollAreaSpectrum->setWidget(spectrumImage);
        splitter->addWidget(scrollAreaSpectrum);
        splitter_2->addWidget(splitter);
        scrollAreaInverse = new QScrollArea(splitter_2);
        scrollAreaInverse->setObjectName(QStringLiteral("scrollAreaInverse"));
        scrollAreaInverse->setWidgetResizable(true);
        scrollAreaInverse->setAlignment(Qt::AlignCenter);
        inverseImage = new QcvMatWidget();
        inverseImage->setObjectName(QStringLiteral("inverseImage"));
        inverseImage->setGeometry(QRect(0, 0, 275, 222));
        scrollAreaInverse->setWidget(inverseImage);
        splitter_2->addWidget(scrollAreaInverse);

        verticalLayout->addWidget(splitter_2);

        tabWidgetFilters = new QTabWidget(centralWidget);
        tabWidgetFilters->setObjectName(QStringLiteral("tabWidgetFilters"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tabWidgetFilters->sizePolicy().hasHeightForWidth());
        tabWidgetFilters->setSizePolicy(sizePolicy1);
        tabWidgetFilters->setStyleSheet(QStringLiteral("font: 10pt \"Lucida Grande\";"));
        tabWidgetFilters->setUsesScrollButtons(false);
        tabImage = new QWidget();
        tabImage->setObjectName(QStringLiteral("tabImage"));
        horizontalLayout = new QHBoxLayout(tabImage);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(8, 8, 8, 8);
        groupBoxSize = new QGroupBox(tabImage);
        groupBoxSize->setObjectName(QStringLiteral("groupBoxSize"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBoxSize->sizePolicy().hasHeightForWidth());
        groupBoxSize->setSizePolicy(sizePolicy2);
        horizontalLayout_4 = new QHBoxLayout(groupBoxSize);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(4, 4, 4, 4);
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(-1);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        radioButtonOrigSize = new QRadioButton(groupBoxSize);
        radioButtonOrigSize->setObjectName(QStringLiteral("radioButtonOrigSize"));
        radioButtonOrigSize->setChecked(true);

        verticalLayout_6->addWidget(radioButtonOrigSize);

        radioButtonCustomSize = new QRadioButton(groupBoxSize);
        radioButtonCustomSize->setObjectName(QStringLiteral("radioButtonCustomSize"));

        verticalLayout_6->addWidget(radioButtonCustomSize);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_2);


        horizontalLayout_4->addLayout(verticalLayout_6);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        labelWidth = new QLabel(groupBoxSize);
        labelWidth->setObjectName(QStringLiteral("labelWidth"));

        horizontalLayout_2->addWidget(labelWidth);

        spinBoxWidth = new QSpinBox(groupBoxSize);
        spinBoxWidth->setObjectName(QStringLiteral("spinBoxWidth"));
        spinBoxWidth->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        spinBoxWidth->setMaximum(1600);
        spinBoxWidth->setSingleStep(4);

        horizontalLayout_2->addWidget(spinBoxWidth);


        verticalLayout_5->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(0);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        labelHeight = new QLabel(groupBoxSize);
        labelHeight->setObjectName(QStringLiteral("labelHeight"));

        horizontalLayout_3->addWidget(labelHeight);

        spinBoxHeight = new QSpinBox(groupBoxSize);
        spinBoxHeight->setObjectName(QStringLiteral("spinBoxHeight"));
        spinBoxHeight->setAutoFillBackground(false);
        spinBoxHeight->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        spinBoxHeight->setMaximum(1200);
        spinBoxHeight->setSingleStep(4);

        horizontalLayout_3->addWidget(spinBoxHeight);


        verticalLayout_5->addLayout(horizontalLayout_3);

        checkBoxAR = new QCheckBox(groupBoxSize);
        checkBoxAR->setObjectName(QStringLiteral("checkBoxAR"));

        verticalLayout_5->addWidget(checkBoxAR);


        horizontalLayout_4->addLayout(verticalLayout_5);


        horizontalLayout->addWidget(groupBoxSize);

        groupBoxConv = new QGroupBox(tabImage);
        groupBoxConv->setObjectName(QStringLiteral("groupBoxConv"));
        sizePolicy2.setHeightForWidth(groupBoxConv->sizePolicy().hasHeightForWidth());
        groupBoxConv->setSizePolicy(sizePolicy2);
        verticalLayout_2 = new QVBoxLayout(groupBoxConv);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(4, 4, 4, 4);
        checkBoxFlip = new QCheckBox(groupBoxConv);
        checkBoxFlip->setObjectName(QStringLiteral("checkBoxFlip"));

        verticalLayout_2->addWidget(checkBoxFlip);

        checkBoxGray = new QCheckBox(groupBoxConv);
        checkBoxGray->setObjectName(QStringLiteral("checkBoxGray"));

        verticalLayout_2->addWidget(checkBoxGray);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer);


        horizontalLayout->addWidget(groupBoxConv);

        groupBoxFFT = new QGroupBox(tabImage);
        groupBoxFFT->setObjectName(QStringLiteral("groupBoxFFT"));
        sizePolicy2.setHeightForWidth(groupBoxFFT->sizePolicy().hasHeightForWidth());
        groupBoxFFT->setSizePolicy(sizePolicy2);
        verticalLayout_4 = new QVBoxLayout(groupBoxFFT);
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(4, 4, 4, 4);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(0);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        labelMag = new QLabel(groupBoxFFT);
        labelMag->setObjectName(QStringLiteral("labelMag"));

        horizontalLayout_5->addWidget(labelMag);

        spinBoxMag = new QDoubleSpinBox(groupBoxFFT);
        spinBoxMag->setObjectName(QStringLiteral("spinBoxMag"));
        spinBoxMag->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        spinBoxMag->setDecimals(0);
        spinBoxMag->setMinimum(5);
        spinBoxMag->setMaximum(30);

        horizontalLayout_5->addWidget(spinBoxMag);


        verticalLayout_4->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        labelFFTSizeTitle = new QLabel(groupBoxFFT);
        labelFFTSizeTitle->setObjectName(QStringLiteral("labelFFTSizeTitle"));

        horizontalLayout_6->addWidget(labelFFTSizeTitle);

        labelFFTSizeValue = new QLabel(groupBoxFFT);
        labelFFTSizeValue->setObjectName(QStringLiteral("labelFFTSizeValue"));
        labelFFTSizeValue->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(labelFFTSizeValue);


        verticalLayout_4->addLayout(horizontalLayout_6);


        horizontalLayout->addWidget(groupBoxFFT);

        groupBoxFilter = new QGroupBox(tabImage);
        groupBoxFilter->setObjectName(QStringLiteral("groupBoxFilter"));
        sizePolicy2.setHeightForWidth(groupBoxFilter->sizePolicy().hasHeightForWidth());
        groupBoxFilter->setSizePolicy(sizePolicy2);
        verticalLayout_3 = new QVBoxLayout(groupBoxFilter);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(4, 4, 4, 4);
        checkBoxFiltering = new QCheckBox(groupBoxFilter);
        checkBoxFiltering->setObjectName(QStringLiteral("checkBoxFiltering"));

        verticalLayout_3->addWidget(checkBoxFiltering);

        radioButtonFilterBox = new QRadioButton(groupBoxFilter);
        radioButtonFilterBox->setObjectName(QStringLiteral("radioButtonFilterBox"));
        radioButtonFilterBox->setChecked(true);

        verticalLayout_3->addWidget(radioButtonFilterBox);

        radioButtonFilterGauss = new QRadioButton(groupBoxFilter);
        radioButtonFilterGauss->setObjectName(QStringLiteral("radioButtonFilterGauss"));

        verticalLayout_3->addWidget(radioButtonFilterGauss);

        radioButtonFilterSinc = new QRadioButton(groupBoxFilter);
        radioButtonFilterSinc->setObjectName(QStringLiteral("radioButtonFilterSinc"));

        verticalLayout_3->addWidget(radioButtonFilterSinc);


        horizontalLayout->addWidget(groupBoxFilter);

        groupBoxTimes = new QGroupBox(tabImage);
        groupBoxTimes->setObjectName(QStringLiteral("groupBoxTimes"));
        gridLayout = new QGridLayout(groupBoxTimes);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(4, 4, 4, 4);
        labelDelayUnit = new QLabel(groupBoxTimes);
        labelDelayUnit->setObjectName(QStringLiteral("labelDelayUnit"));

        gridLayout->addWidget(labelDelayUnit, 2, 2, 1, 1, Qt::AlignLeft);

        labelFrameRateUnit = new QLabel(groupBoxTimes);
        labelFrameRateUnit->setObjectName(QStringLiteral("labelFrameRateUnit"));

        gridLayout->addWidget(labelFrameRateUnit, 1, 2, 1, 1, Qt::AlignLeft);

        labelProcessingTimeUnit = new QLabel(groupBoxTimes);
        labelProcessingTimeUnit->setObjectName(QStringLiteral("labelProcessingTimeUnit"));

        gridLayout->addWidget(labelProcessingTimeUnit, 4, 2, 1, 1, Qt::AlignLeft);

        labelDelay = new QLabel(groupBoxTimes);
        labelDelay->setObjectName(QStringLiteral("labelDelay"));

        gridLayout->addWidget(labelDelay, 2, 1, 1, 1, Qt::AlignRight);

        labelProcessingTimeTitle = new QLabel(groupBoxTimes);
        labelProcessingTimeTitle->setObjectName(QStringLiteral("labelProcessingTimeTitle"));

        gridLayout->addWidget(labelProcessingTimeTitle, 4, 0, 1, 1, Qt::AlignRight);

        labelProcessingTime = new QLabel(groupBoxTimes);
        labelProcessingTime->setObjectName(QStringLiteral("labelProcessingTime"));

        gridLayout->addWidget(labelProcessingTime, 4, 1, 1, 1, Qt::AlignRight);

        labelFrameRateTitle = new QLabel(groupBoxTimes);
        labelFrameRateTitle->setObjectName(QStringLiteral("labelFrameRateTitle"));

        gridLayout->addWidget(labelFrameRateTitle, 1, 0, 1, 1, Qt::AlignRight);

        labelDelayTitle = new QLabel(groupBoxTimes);
        labelDelayTitle->setObjectName(QStringLiteral("labelDelayTitle"));

        gridLayout->addWidget(labelDelayTitle, 2, 0, 1, 1, Qt::AlignRight);

        labelFrameRate = new QLabel(groupBoxTimes);
        labelFrameRate->setObjectName(QStringLiteral("labelFrameRate"));

        gridLayout->addWidget(labelFrameRate, 1, 1, 1, 1, Qt::AlignRight);

        labelProcessingRateTitle = new QLabel(groupBoxTimes);
        labelProcessingRateTitle->setObjectName(QStringLiteral("labelProcessingRateTitle"));

        gridLayout->addWidget(labelProcessingRateTitle, 5, 0, 1, 1, Qt::AlignRight);

        labelProcessingRate = new QLabel(groupBoxTimes);
        labelProcessingRate->setObjectName(QStringLiteral("labelProcessingRate"));
        labelProcessingRate->setStyleSheet(QStringLiteral("color: rgb(255, 0, 0);"));

        gridLayout->addWidget(labelProcessingRate, 5, 1, 1, 1, Qt::AlignRight);

        labelProcessingRateUnit = new QLabel(groupBoxTimes);
        labelProcessingRateUnit->setObjectName(QStringLiteral("labelProcessingRateUnit"));

        gridLayout->addWidget(labelProcessingRateUnit, 5, 2, 1, 1, Qt::AlignLeft);

        labelCapture = new QLabel(groupBoxTimes);
        labelCapture->setObjectName(QStringLiteral("labelCapture"));

        gridLayout->addWidget(labelCapture, 0, 0, 1, 3);

        labelProcessing = new QLabel(groupBoxTimes);
        labelProcessing->setObjectName(QStringLiteral("labelProcessing"));

        gridLayout->addWidget(labelProcessing, 3, 0, 1, 3);


        horizontalLayout->addWidget(groupBoxTimes);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        tabWidgetFilters->addTab(tabImage, QString());
        tabLowPass = new QWidget();
        tabLowPass->setObjectName(QStringLiteral("tabLowPass"));
        gridLayout_2 = new QGridLayout(tabLowPass);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(8, 8, 8, 8);
        labelRedLP = new QLabel(tabLowPass);
        labelRedLP->setObjectName(QStringLiteral("labelRedLP"));

        gridLayout_2->addWidget(labelRedLP, 1, 0, 1, 1);

        horizontalSliderRLP = new QSlider(tabLowPass);
        horizontalSliderRLP->setObjectName(QStringLiteral("horizontalSliderRLP"));
        horizontalSliderRLP->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(horizontalSliderRLP, 1, 1, 1, 1);

        labelGreenLP = new QLabel(tabLowPass);
        labelGreenLP->setObjectName(QStringLiteral("labelGreenLP"));

        gridLayout_2->addWidget(labelGreenLP, 2, 0, 1, 1);

        horizontalSliderGLP = new QSlider(tabLowPass);
        horizontalSliderGLP->setObjectName(QStringLiteral("horizontalSliderGLP"));
        horizontalSliderGLP->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(horizontalSliderGLP, 2, 1, 1, 1);

        labelBlueLP = new QLabel(tabLowPass);
        labelBlueLP->setObjectName(QStringLiteral("labelBlueLP"));

        gridLayout_2->addWidget(labelBlueLP, 3, 0, 1, 1);

        horizontalSliderBLP = new QSlider(tabLowPass);
        horizontalSliderBLP->setObjectName(QStringLiteral("horizontalSliderBLP"));
        horizontalSliderBLP->setOrientation(Qt::Horizontal);

        gridLayout_2->addWidget(horizontalSliderBLP, 3, 1, 1, 1);

        spinBoxRedLP = new QSpinBox(tabLowPass);
        spinBoxRedLP->setObjectName(QStringLiteral("spinBoxRedLP"));

        gridLayout_2->addWidget(spinBoxRedLP, 1, 2, 1, 1);

        spinBoxGreenLP = new QSpinBox(tabLowPass);
        spinBoxGreenLP->setObjectName(QStringLiteral("spinBoxGreenLP"));

        gridLayout_2->addWidget(spinBoxGreenLP, 2, 2, 1, 1);

        spinBoxBlueLP = new QSpinBox(tabLowPass);
        spinBoxBlueLP->setObjectName(QStringLiteral("spinBoxBlueLP"));

        gridLayout_2->addWidget(spinBoxBlueLP, 3, 2, 1, 1);

        checkBoxLinkLP = new QCheckBox(tabLowPass);
        checkBoxLinkLP->setObjectName(QStringLiteral("checkBoxLinkLP"));

        gridLayout_2->addWidget(checkBoxLinkLP, 0, 2, 1, 1);

        tabWidgetFilters->addTab(tabLowPass, QString());
        tabHighPass = new QWidget();
        tabHighPass->setObjectName(QStringLiteral("tabHighPass"));
        gridLayout_3 = new QGridLayout(tabHighPass);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setContentsMargins(8, 8, 8, 8);
        labelRedHP = new QLabel(tabHighPass);
        labelRedHP->setObjectName(QStringLiteral("labelRedHP"));

        gridLayout_3->addWidget(labelRedHP, 1, 0, 1, 1);

        horizontalSliderRHP = new QSlider(tabHighPass);
        horizontalSliderRHP->setObjectName(QStringLiteral("horizontalSliderRHP"));
        horizontalSliderRHP->setOrientation(Qt::Horizontal);

        gridLayout_3->addWidget(horizontalSliderRHP, 1, 1, 1, 1);

        spinBoxRedHP = new QSpinBox(tabHighPass);
        spinBoxRedHP->setObjectName(QStringLiteral("spinBoxRedHP"));

        gridLayout_3->addWidget(spinBoxRedHP, 1, 2, 1, 1);

        labelGreenHP = new QLabel(tabHighPass);
        labelGreenHP->setObjectName(QStringLiteral("labelGreenHP"));

        gridLayout_3->addWidget(labelGreenHP, 2, 0, 1, 1);

        horizontalSliderGHP = new QSlider(tabHighPass);
        horizontalSliderGHP->setObjectName(QStringLiteral("horizontalSliderGHP"));
        horizontalSliderGHP->setOrientation(Qt::Horizontal);

        gridLayout_3->addWidget(horizontalSliderGHP, 2, 1, 1, 1);

        spinBoxGreenHP = new QSpinBox(tabHighPass);
        spinBoxGreenHP->setObjectName(QStringLiteral("spinBoxGreenHP"));

        gridLayout_3->addWidget(spinBoxGreenHP, 2, 2, 1, 1);

        labelBlueHP = new QLabel(tabHighPass);
        labelBlueHP->setObjectName(QStringLiteral("labelBlueHP"));

        gridLayout_3->addWidget(labelBlueHP, 3, 0, 1, 1);

        horizontalSliderBHP = new QSlider(tabHighPass);
        horizontalSliderBHP->setObjectName(QStringLiteral("horizontalSliderBHP"));
        horizontalSliderBHP->setOrientation(Qt::Horizontal);

        gridLayout_3->addWidget(horizontalSliderBHP, 3, 1, 1, 1);

        spinBoxBlueHP = new QSpinBox(tabHighPass);
        spinBoxBlueHP->setObjectName(QStringLiteral("spinBoxBlueHP"));

        gridLayout_3->addWidget(spinBoxBlueHP, 3, 2, 1, 1);

        checkBoxLinkHP = new QCheckBox(tabHighPass);
        checkBoxLinkHP->setObjectName(QStringLiteral("checkBoxLinkHP"));

        gridLayout_3->addWidget(checkBoxLinkHP, 0, 2, 1, 1);

        tabWidgetFilters->addTab(tabHighPass, QString());

        verticalLayout->addWidget(tabWidgetFilters);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 853, 22));
        menuSources = new QMenu(menuBar);
        menuSources->setObjectName(QStringLiteral("menuSources"));
        menuVideo = new QMenu(menuBar);
        menuVideo->setObjectName(QStringLiteral("menuVideo"));
        menuSize = new QMenu(menuVideo);
        menuSize->setObjectName(QStringLiteral("menuSize"));
        menuRender = new QMenu(menuBar);
        menuRender->setObjectName(QStringLiteral("menuRender"));
        menuFilter = new QMenu(menuBar);
        menuFilter->setObjectName(QStringLiteral("menuFilter"));
        menuFilterTypes = new QMenu(menuFilter);
        menuFilterTypes->setObjectName(QStringLiteral("menuFilterTypes"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        QWidget::setTabOrder(radioButtonOrigSize, radioButtonCustomSize);
        QWidget::setTabOrder(radioButtonCustomSize, spinBoxWidth);
        QWidget::setTabOrder(spinBoxWidth, spinBoxHeight);
        QWidget::setTabOrder(spinBoxHeight, checkBoxFlip);
        QWidget::setTabOrder(checkBoxFlip, checkBoxGray);
        QWidget::setTabOrder(checkBoxGray, spinBoxMag);
        QWidget::setTabOrder(spinBoxMag, checkBoxFiltering);
        QWidget::setTabOrder(checkBoxFiltering, radioButtonFilterBox);
        QWidget::setTabOrder(radioButtonFilterBox, radioButtonFilterGauss);
        QWidget::setTabOrder(radioButtonFilterGauss, radioButtonFilterSinc);
        QWidget::setTabOrder(radioButtonFilterSinc, spinBoxRedLP);
        QWidget::setTabOrder(spinBoxRedLP, spinBoxGreenLP);
        QWidget::setTabOrder(spinBoxGreenLP, spinBoxBlueLP);
        QWidget::setTabOrder(spinBoxBlueLP, horizontalSliderRLP);
        QWidget::setTabOrder(horizontalSliderRLP, horizontalSliderGLP);
        QWidget::setTabOrder(horizontalSliderGLP, horizontalSliderBLP);
        QWidget::setTabOrder(horizontalSliderBLP, checkBoxLinkLP);
        QWidget::setTabOrder(checkBoxLinkLP, spinBoxRedHP);
        QWidget::setTabOrder(spinBoxRedHP, spinBoxGreenHP);
        QWidget::setTabOrder(spinBoxGreenHP, spinBoxBlueHP);
        QWidget::setTabOrder(spinBoxBlueHP, horizontalSliderRHP);
        QWidget::setTabOrder(horizontalSliderRHP, horizontalSliderGHP);
        QWidget::setTabOrder(horizontalSliderGHP, horizontalSliderBHP);
        QWidget::setTabOrder(horizontalSliderBHP, checkBoxLinkHP);
        QWidget::setTabOrder(checkBoxLinkHP, tabWidgetFilters);
        QWidget::setTabOrder(tabWidgetFilters, scrollAreaSource);
        QWidget::setTabOrder(scrollAreaSource, scrollAreaSpectrum);
        QWidget::setTabOrder(scrollAreaSpectrum, scrollAreaInverse);

        menuBar->addAction(menuSources->menuAction());
        menuBar->addAction(menuVideo->menuAction());
        menuBar->addAction(menuRender->menuAction());
        menuBar->addAction(menuFilter->menuAction());
        menuSources->addAction(actionCamera_0);
        menuSources->addAction(actionCamera_1);
        menuSources->addAction(actionFile);
        menuSources->addSeparator();
        menuSources->addAction(actionQuit);
        menuVideo->addAction(actionFlip);
        menuVideo->addAction(actionGray);
        menuVideo->addSeparator();
        menuVideo->addAction(menuSize->menuAction());
        menuSize->addAction(actionOriginalSize);
        menuSize->addAction(actionConstrainedSize);
        menuRender->addAction(actionRenderImage);
        menuRender->addAction(actionRenderPixmap);
        menuRender->addAction(actionRenderOpenGL);
        menuFilter->addAction(actionLink_Filters);
        menuFilter->addAction(actionFiltering);
        menuFilter->addAction(menuFilterTypes->menuAction());
        menuFilter->addAction(actionSwitchTab);
        menuFilterTypes->addAction(actionFilterBox);
        menuFilterTypes->addAction(actionFilterGaussian);
        menuFilterTypes->addAction(actionFilterSinc);

        retranslateUi(MainWindow);
        QObject::connect(radioButtonCustomSize, SIGNAL(clicked(bool)), actionConstrainedSize, SLOT(setChecked(bool)));
        QObject::connect(actionConstrainedSize, SIGNAL(triggered(bool)), radioButtonCustomSize, SLOT(setChecked(bool)));
        QObject::connect(radioButtonOrigSize, SIGNAL(clicked(bool)), actionOriginalSize, SLOT(setChecked(bool)));
        QObject::connect(actionOriginalSize, SIGNAL(triggered(bool)), radioButtonOrigSize, SLOT(setChecked(bool)));
        QObject::connect(checkBoxFlip, SIGNAL(clicked(bool)), actionFlip, SLOT(setChecked(bool)));
        QObject::connect(actionFlip, SIGNAL(triggered(bool)), checkBoxFlip, SLOT(setChecked(bool)));
        QObject::connect(horizontalSliderRLP, SIGNAL(valueChanged(int)), spinBoxRedLP, SLOT(setValue(int)));
        QObject::connect(horizontalSliderGLP, SIGNAL(valueChanged(int)), spinBoxGreenLP, SLOT(setValue(int)));
        QObject::connect(horizontalSliderBLP, SIGNAL(valueChanged(int)), spinBoxBlueLP, SLOT(setValue(int)));
        QObject::connect(spinBoxRedLP, SIGNAL(valueChanged(int)), horizontalSliderRLP, SLOT(setValue(int)));
        QObject::connect(spinBoxGreenLP, SIGNAL(valueChanged(int)), horizontalSliderGLP, SLOT(setValue(int)));
        QObject::connect(spinBoxBlueLP, SIGNAL(valueChanged(int)), horizontalSliderBLP, SLOT(setValue(int)));
        QObject::connect(horizontalSliderRHP, SIGNAL(valueChanged(int)), spinBoxRedHP, SLOT(setValue(int)));
        QObject::connect(horizontalSliderGHP, SIGNAL(valueChanged(int)), spinBoxGreenHP, SLOT(setValue(int)));
        QObject::connect(horizontalSliderBHP, SIGNAL(valueChanged(int)), spinBoxBlueHP, SLOT(setValue(int)));
        QObject::connect(spinBoxRedHP, SIGNAL(valueChanged(int)), horizontalSliderRHP, SLOT(setValue(int)));
        QObject::connect(spinBoxGreenHP, SIGNAL(valueChanged(int)), horizontalSliderGHP, SLOT(setValue(int)));
        QObject::connect(spinBoxBlueHP, SIGNAL(valueChanged(int)), horizontalSliderBHP, SLOT(setValue(int)));
        QObject::connect(checkBoxGray, SIGNAL(clicked(bool)), actionGray, SLOT(setChecked(bool)));
        QObject::connect(actionGray, SIGNAL(triggered(bool)), checkBoxGray, SLOT(setChecked(bool)));

        tabWidgetFilters->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Frequency Filtering", nullptr));
        actionCamera_0->setText(QApplication::translate("MainWindow", "Camera 0", nullptr));
#ifndef QT_NO_TOOLTIP
        actionCamera_0->setToolTip(QApplication::translate("MainWindow", "cam\303\251ra interne", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionCamera_0->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+0", nullptr));
#endif // QT_NO_SHORTCUT
        actionCamera_1->setText(QApplication::translate("MainWindow", "Camera 1", nullptr));
#ifndef QT_NO_TOOLTIP
        actionCamera_1->setToolTip(QApplication::translate("MainWindow", "cam\303\251ra externe", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionCamera_1->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+1", nullptr));
#endif // QT_NO_SHORTCUT
        actionFile->setText(QApplication::translate("MainWindow", "File", nullptr));
#ifndef QT_NO_TOOLTIP
        actionFile->setToolTip(QApplication::translate("MainWindow", "fichier video", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionFile->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_NO_SHORTCUT
        actionFlip->setText(QApplication::translate("MainWindow", "flip", nullptr));
        actionOriginalSize->setText(QApplication::translate("MainWindow", "Originale", nullptr));
#ifndef QT_NO_TOOLTIP
        actionOriginalSize->setToolTip(QApplication::translate("MainWindow", "taille originale", nullptr));
#endif // QT_NO_TOOLTIP
        actionConstrainedSize->setText(QApplication::translate("MainWindow", "Custom", nullptr));
        actionConstrainedSize->setIconText(QApplication::translate("MainWindow", "contrainte", nullptr));
#ifndef QT_NO_TOOLTIP
        actionConstrainedSize->setToolTip(QApplication::translate("MainWindow", "taille impos\303\251e", nullptr));
#endif // QT_NO_TOOLTIP
        actionQuit->setText(QApplication::translate("MainWindow", "Quitter", nullptr));
        actionRenderImage->setText(QApplication::translate("MainWindow", "Image", nullptr));
        actionRenderPixmap->setText(QApplication::translate("MainWindow", "Pixmap", nullptr));
        actionRenderOpenGL->setText(QApplication::translate("MainWindow", "OpenGL", nullptr));
        actionGray->setText(QApplication::translate("MainWindow", "gray", nullptr));
        actionFilterBox->setText(QApplication::translate("MainWindow", "Box", nullptr));
#ifndef QT_NO_TOOLTIP
        actionFilterBox->setToolTip(QApplication::translate("MainWindow", "Box Filtering", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionFilterBox->setShortcut(QApplication::translate("MainWindow", "Ctrl+B", nullptr));
#endif // QT_NO_SHORTCUT
        actionFilterGaussian->setText(QApplication::translate("MainWindow", "Gaussian", nullptr));
#ifndef QT_NO_TOOLTIP
        actionFilterGaussian->setToolTip(QApplication::translate("MainWindow", "Gaussian Filtering", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionFilterGaussian->setShortcut(QApplication::translate("MainWindow", "Ctrl+G", nullptr));
#endif // QT_NO_SHORTCUT
        actionFilterSinc->setText(QApplication::translate("MainWindow", "Sinc", nullptr));
#ifndef QT_NO_TOOLTIP
        actionFilterSinc->setToolTip(QApplication::translate("MainWindow", "Sinc filtering", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionFilterSinc->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_NO_SHORTCUT
        actionLink_Filters->setText(QApplication::translate("MainWindow", "Link Filters", nullptr));
#ifndef QT_NO_TOOLTIP
        actionLink_Filters->setToolTip(QApplication::translate("MainWindow", "Link Filters value together", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionLink_Filters->setShortcut(QApplication::translate("MainWindow", "Ctrl+L", nullptr));
#endif // QT_NO_SHORTCUT
        actionFiltering->setText(QApplication::translate("MainWindow", "Filtering", nullptr));
#ifndef QT_NO_TOOLTIP
        actionFiltering->setToolTip(QApplication::translate("MainWindow", "Filtering Toggle", nullptr));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_SHORTCUT
        actionFiltering->setShortcut(QApplication::translate("MainWindow", "Ctrl+F", nullptr));
#endif // QT_NO_SHORTCUT
        actionSwitchTab->setText(QApplication::translate("MainWindow", "Next Tab", nullptr));
#ifndef QT_NO_SHORTCUT
        actionSwitchTab->setShortcut(QApplication::translate("MainWindow", "Alt+Tab", nullptr));
#endif // QT_NO_SHORTCUT
        groupBoxSize->setTitle(QApplication::translate("MainWindow", "Size", nullptr));
        radioButtonOrigSize->setText(QApplication::translate("MainWindow", "Original", nullptr));
        radioButtonCustomSize->setText(QApplication::translate("MainWindow", "Custom", nullptr));
        labelWidth->setText(QApplication::translate("MainWindow", "Width", nullptr));
        labelHeight->setText(QApplication::translate("MainWindow", "Height", nullptr));
        checkBoxAR->setText(QApplication::translate("MainWindow", "Keep aspect ratio", nullptr));
        groupBoxConv->setTitle(QApplication::translate("MainWindow", "Conversions", nullptr));
        checkBoxFlip->setText(QApplication::translate("MainWindow", "Flip", nullptr));
        checkBoxGray->setText(QApplication::translate("MainWindow", "Convert to gray", nullptr));
        groupBoxFFT->setTitle(QApplication::translate("MainWindow", "FFT", nullptr));
        labelMag->setText(QApplication::translate("MainWindow", "Magnification", nullptr));
        labelFFTSizeTitle->setText(QApplication::translate("MainWindow", "FFT Size", nullptr));
        labelFFTSizeValue->setText(QApplication::translate("MainWindow", "0", nullptr));
        groupBoxFilter->setTitle(QApplication::translate("MainWindow", "Filter", nullptr));
        checkBoxFiltering->setText(QApplication::translate("MainWindow", "Filtering", nullptr));
        radioButtonFilterBox->setText(QApplication::translate("MainWindow", "Box", nullptr));
        radioButtonFilterGauss->setText(QApplication::translate("MainWindow", "Gaussian", nullptr));
        radioButtonFilterSinc->setText(QApplication::translate("MainWindow", "Sinc", nullptr));
        groupBoxTimes->setTitle(QApplication::translate("MainWindow", "Times", nullptr));
        labelDelayUnit->setText(QApplication::translate("MainWindow", "ms", nullptr));
        labelFrameRateUnit->setText(QApplication::translate("MainWindow", "frames/s", nullptr));
        labelProcessingTimeUnit->setText(QApplication::translate("MainWindow", "ms", nullptr));
        labelDelay->setText(QApplication::translate("MainWindow", "33", nullptr));
        labelProcessingTimeTitle->setText(QApplication::translate("MainWindow", "time", nullptr));
        labelProcessingTime->setText(QApplication::translate("MainWindow", "40", nullptr));
        labelFrameRateTitle->setText(QApplication::translate("MainWindow", "rate", nullptr));
        labelDelayTitle->setText(QApplication::translate("MainWindow", "delay", nullptr));
        labelFrameRate->setText(QApplication::translate("MainWindow", "30.0", nullptr));
        labelProcessingRateTitle->setText(QApplication::translate("MainWindow", "rate", nullptr));
        labelProcessingRate->setText(QApplication::translate("MainWindow", "25.0", nullptr));
        labelProcessingRateUnit->setText(QApplication::translate("MainWindow", "frames/s", nullptr));
        labelCapture->setText(QApplication::translate("MainWindow", "Capture", nullptr));
        labelProcessing->setText(QApplication::translate("MainWindow", "Processing", nullptr));
        tabWidgetFilters->setTabText(tabWidgetFilters->indexOf(tabImage), QApplication::translate("MainWindow", "Image", nullptr));
        labelRedLP->setText(QApplication::translate("MainWindow", "Red", nullptr));
        labelGreenLP->setText(QApplication::translate("MainWindow", "Green", nullptr));
        labelBlueLP->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        checkBoxLinkLP->setText(QApplication::translate("MainWindow", "Link", nullptr));
        tabWidgetFilters->setTabText(tabWidgetFilters->indexOf(tabLowPass), QApplication::translate("MainWindow", "Low Pass", nullptr));
        labelRedHP->setText(QApplication::translate("MainWindow", "Red", nullptr));
        labelGreenHP->setText(QApplication::translate("MainWindow", "Green", nullptr));
        labelBlueHP->setText(QApplication::translate("MainWindow", "Blue", nullptr));
        checkBoxLinkHP->setText(QApplication::translate("MainWindow", "Link", nullptr));
        tabWidgetFilters->setTabText(tabWidgetFilters->indexOf(tabHighPass), QApplication::translate("MainWindow", "High Pass", nullptr));
        menuSources->setTitle(QApplication::translate("MainWindow", "Sources", nullptr));
        menuVideo->setTitle(QApplication::translate("MainWindow", "Video", nullptr));
        menuSize->setTitle(QApplication::translate("MainWindow", "taille", nullptr));
        menuRender->setTitle(QApplication::translate("MainWindow", "Render", nullptr));
        menuFilter->setTitle(QApplication::translate("MainWindow", "Filter", nullptr));
        menuFilterTypes->setTitle(QApplication::translate("MainWindow", "FilterTypes", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
