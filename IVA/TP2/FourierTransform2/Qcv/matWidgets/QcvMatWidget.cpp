/*
 * QcvMatWidget.cpp
 *
 *  Created on: 28 févr. 2011
 *	  Author: davidroussel
 */

#include <QDebug>
#include <QTime>	// for timed debug
#include <QThread>	// for thread info in debug messages

#include <opencv2/imgproc.hpp>

#include <Qcv/matWidgets/QcvMatWidget.h>

// ----------------------------------------------------------------------------
// Protected static attributes
// ----------------------------------------------------------------------------

/*
 * Default size when no image has been set
 */
QSize QcvMatWidget::defaultSize(640, 480);

/*
 * Default aspect ratio when image is not set yet
 */
double QcvMatWidget::defaultAspectRatio = 4.0/3.0;

/*
 * Drawing color
 */
const Scalar QcvMatWidget::drawingColor(0xFF,0xCC,0x00,0x88);

/*
 * Drawing width
 */
const int QcvMatWidget::drawingWidth(3);

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * OpenCV QT Widget default constructor
 * @param parent parent widget
 * @param lock the lock to use on sourceImage in multi-threaded
 * environments
 * @param mouseSense mouse sensivity
 */
QcvMatWidget::QcvMatWidget(QWidget * parent,
						   QMutex * lock,
						   MouseSense mouseSense) :
	QWidget(parent),
	sourceImage(NULL),
	aspectRatio(defaultAspectRatio),
	mousePressed(false),
	mouseSense(mouseSense),
	pixelScale(devicePixelRatioF()),
	sourceLock(lock)
{
	setup();
}

/*
 * OpenCV QT Widget constructor
 * @param sourceImage the source image
 * @param parent parent widget
 * @param lock the lock to use on sourceImage in multi-threaded
 * environments
 * @param mouseSense mouse sensivity
 * @pre sourceImage is not NULL
 */
QcvMatWidget::QcvMatWidget(Mat * sourceImage,
						   QWidget * parent,
						   QMutex * lock,
						   MouseSense mouseSense) :
	QWidget(parent),
	sourceImage(sourceImage),
	aspectRatio((double)sourceImage->cols / (double)sourceImage->rows),
	mousePressed(false),
	mouseSense(mouseSense),
	pixelScale(devicePixelRatioF()),
	sourceLock(lock)
{
	setup();
}

/*
 * OpenCV Widget destructor.
 * Releases displayImage.
 */
QcvMatWidget::~QcvMatWidget()
{
	displayImage.release();
	/*
	 *  Caution : Locking/Unlocking in the destructor may lead to
	 * lock an already destroyed lock which may lead to a segmentation fault
	 */
}

/*
 * Widget minimum size is set to the contained image size
 * @return le size of the image within
 */
//QSize QcvMatWidget::minimumSize() const
//{
//	return sizeHint();
//}

/*
 * Size hint (because size depends on sourceImage properties)
 * @return size obtained from sourceImage
 */
QSize QcvMatWidget::sizeHint() const
{
	if (sourceImage != nullptr)
	{
		return QSize(sourceImage->cols, sourceImage->rows);
	}
	else
	{
		return defaultSize;
	}
}

/*
 * aspect ratio method
 * @param w width
 * @return the required height fo r this width
 */
int QcvMatWidget::heightForWidth(int w) const
{
//	qDebug ("height  = %d for width  = %d called", (int)((double)w/aspectRatio), w);
	return (int)((double)w/aspectRatio);
}

/*
 * Gets Mat widget mouse clickable status
 * @return true if widget is sensitive to mouse click
 */
bool QcvMatWidget::isMouseClickable() const
{
	return (int(mouseSense) & int(MouseSense::CLICK));
}

/*
 * Gets Mat widget mouse dragable status
 * @return true if widget is sensitive to mouse drag
 */
bool QcvMatWidget::isMouseDragable() const
{
	return (int(mouseSense) & int(MouseSense::DRAG));
}

// ----------------------------------------------------------------------------
// Protected Methods
// ----------------------------------------------------------------------------

/*
 * paint event reimplemented to draw content (in this case only
 * draw in display image since final rendering method is not yet available)
 * @param event the paint event
 */
void QcvMatWidget::paintEvent(QPaintEvent * event)
{
	Q_UNUSED(event);

	if (displayImage.data != nullptr)
	{
		// evt draw in image
		if (mousePressed)
		{
			// if MOUSE_CLICK only draws a cross
			if (mouseSense > MouseSense::NONE)
			{
				if (!(int(mouseSense) & int(MouseSense::DRAG)))
				{
					if (mouseMoved)
					{
						drawCross(draggedPoint);
					}
					else
					{
						drawCross(pressedPoint);
					}
				}
				else  // else if MOUSE_DRAG starts drawing a rectangle
				{
					drawRectangle(selectionRect);
				}
			}
		}
	}
//	else
//	{
//		qWarning("QcvMatWidget::paintEvent : image.data is NULL");
//	}
}

/*
 * Widget setup
 */
void QcvMatWidget::setup()
{
	layout = new QHBoxLayout();
	layout->setContentsMargins(0,0,0,0);
	setLayout(layout);
}

/*
 * Converts BGR or Gray source image to RGB display image
 * @see #sourceImage
 * @see #displayImage
 */
void QcvMatWidget::convertImage()
{
//	qDebug("Convert image");

	bool hasLock = (sourceLock != nullptr);

	int depth = sourceImage->depth();
	int channels = sourceImage->channels();

//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << sourceLock << ";"
//					   <<  "will lock";
	if (hasLock)
	{
		sourceLock->lock();
	}
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << sourceLock << ";"
//					   <<  "locked";

	// Converts any image type to RGB format
	switch (depth)
	{
		case CV_8U:
			switch (channels)
			{
				case 1: // gray level image
					cvtColor(*sourceImage, displayImage,CV_GRAY2RGB);
					break;
				case 3: // Color image (OpenCV produces BGR images)
					cvtColor(*sourceImage, displayImage, CV_BGR2RGB);
					break;
				default:
					qFatal("This number of channels (%d) is not supported",
						   channels);
					break;
			}
			break;
		default:
			qFatal("This image depth (%d) is not implemented in QcvMatWidget",
				   depth);
			break;
	}

	if (hasLock)
	{
		sourceLock->unlock();
	}
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << sourceLock << ";"
//					   <<  "unlocked";
}

/*
 * Callback called when mouse button pressed event occurs.
 * reimplemented to send pressPoint signal when left mouse button is
 * pressed
 * @param event mouse event
 */
void QcvMatWidget::mousePressEvent(QMouseEvent *event)
{
	if (mouseSense > MouseSense::NONE)
	{
//		qDebug("mousePressEvent(%d, %d) with button %d",
//			   event->pos().x(), event->pos().y(), event->button());
		mousePressed = true;
		pressedPoint = event->pos();
		pressedButton = event->button();

		if((event->button() == Qt::LeftButton) &&
		   (int(mouseSense) & int(MouseSense::DRAG)))
		{
			// initialise selection rect
			selectionRect.setTopLeft(pressedPoint);
			selectionRect.setBottomRight(pressedPoint);
		}

		emit pressPoint(pressedPoint, pressedButton);
	}
}

/*
 * Callback called when mouse move event occurs.
 * reimplemented to send dragPoint signal when mouse is dragged
 * (after left mouse button has been pressed)
 * @param event mouse event
 * @post The #dragPoint(QPoint); signal has been sent
 */
void QcvMatWidget::mouseMoveEvent(QMouseEvent *event)
{
	mouseMoved = true;
	draggedPoint = event->pos();

	if ((int(mouseSense) & int(MouseSense::DRAG)) && mousePressed)
	{
//		qDebug("mouseMoveEvent(%d, %d) with button %d",
//			   event->pos().x(), event->pos().y(), event->button());

		selectionRectFromPoints(pressedPoint, draggedPoint);

		emit dragPoint(draggedPoint);
	}
}

/*
 * Callback called when mouse button released event occurs.
 * reimplemented to send releasePoint signal when left mouse button is
 * released
 * @param event mouse event
 */
void QcvMatWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if ((mouseSense > MouseSense::NONE) && mousePressed)
	{
//		qDebug("mouseReleaseEvent(%d, %d) with button %d",
//			   event->pos().x(), event->pos().y(), event->button());
		mousePressed = false;
		mouseMoved = false;
		releasedPoint = event->pos();
		emit releasePoint(releasedPoint, pressedButton);

		if ((event->button() == Qt::LeftButton) &&
			(int(mouseSense) & int(MouseSense::DRAG)))
		{
			selectionRectFromPoints(pressedPoint, releasedPoint);
			emit releaseSelection(selectionRect, event->button());
		}
	}
}

/*
 * Draw Cross
 * @param p the cross center
 */
void QcvMatWidget::drawCross(const QPoint & p)
{
	int x0 = p.x();
	int y0 = p.y();
	int x1, x2, x3, x4;
	int y1, y2, y3, y4;
	int offset = 10;

	x1 = x0 - 2*offset;
	x2 = x0 - offset;
	x3 = x0 + offset;
	x4 = x0 + 2*offset;
	y1 = y0 - 2*offset;
	y2 = y0 - offset;
	y3 = y0 + offset;
	y4 = y0 + 2*offset;

	Point p1a(x1, y0);
	Point p1b(x2, y0);
	Point p2a(x3, y0);
	Point p2b(x4, y0);
	Point p3a(x0, y1);
	Point p3b(x0, y2);
	Point p4a(x0, y3);
	Point p4b(x0, y4);

	line(displayImage, p1a, p1b, drawingColor, drawingWidth, CV_AA);
	line(displayImage, p2a, p2b, drawingColor, drawingWidth, CV_AA);
	line(displayImage, p3a, p3b, drawingColor, drawingWidth, CV_AA);
	line(displayImage, p4a, p4b, drawingColor, drawingWidth, CV_AA);
}

/*
 * Draw rectangle
 * @param r the rectangle to draw
 */
void QcvMatWidget::drawRectangle(const QRect & r)
{
	int x1 = r.left();
	int x2 = r.right();
	int y1 = r.top();
	int y2 = r.bottom();

	Point p1(x1, y1);
	Point p2(x2, y2);

	rectangle(displayImage, p1, p2, drawingColor, drawingWidth, CV_AA);
}

/*
 * Modifiy selectionRect using two points
 * @param p1 first point
 * @param p2 second point
 */
void QcvMatWidget::selectionRectFromPoints(const QPoint & p1, const QPoint & p2)
{
	int left, right, top, bottom;
	if (p1.x() < p2.x())
	{
		left = p1.x();
		right = p2.x();
	}
	else
	{
		left = p2.x();
		right = p1.x();
	}

	if (p1.y() < p2.y())
	{
		top = p1.y();
		bottom = p2.y();
	}
	else
	{
		top = p2.y();
		bottom = p1.y();
	}

	selectionRect.setLeft(left);
	selectionRect.setRight(right);
	selectionRect.setTop(top);
	selectionRect.setBottom(bottom);
}

// ----------------------------------------------------------------------------
// Public Slots
// ----------------------------------------------------------------------------

/*
 * Sets new source image
 * @param sourceImage the new source image
 */
void QcvMatWidget::setSourceImage(Mat * sourceImage)
{
	// qDebug("QcvMatWidget::setSourceImage");

	this->sourceImage = sourceImage;

	// re-setup geometry since height x width may have changed
	aspectRatio = (double)sourceImage->cols / (double)sourceImage->rows;
	// qDebug ("aspect ratio changed to %4.2f", aspectRatio);

}

/*
 * Update slot customized to include convertImage before actually
 * updating
 */
void QcvMatWidget::update()
{
	convertImage();
	QWidget::update();
}

/*
 * Recompute pixel scale according to screen pixel scale.
 * Used with Hi DPI devices (such as retina screens)
 * @post pixel scale have been updated according to
 * devicePixelRatioF provided by the QPaintDevice super class
 */
void QcvMatWidget::screenChanged()
{
	pixelScale = devicePixelRatioF();
//	qDebug() << "Pixel scale updated to" << pixelScale;
}

// ----------------------------------------------------------------------------
// convertImage old algorithm
// ----------------------------------------------------------------------------
//	int cvIndex, cvLineStart;
//	// switch between bit depths
//	switch (displayImage.depth())
//	{
//		case CV_8U:
//			switch (displayImage.channels())
//			{
//				case 1: // Gray level images
//					if ( (displayImage.cols != image.width()) ||
//						 (displayImage.rows != image.height()) )
//					{
//						QImage temp(displayImage.cols, displayImage.rows,
//								QImage::Format_RGB32);
//						image = temp;
//					}
//					cvIndex = 0;
//					cvLineStart = 0;
//					for (int y = 0; y < displayImage.rows; y++)
//					{
//						unsigned char red, green, blue;
//						cvIndex = cvLineStart;
//						for (int x = 0; x < displayImage.cols; x++)
//						{
//							// DO it
//							red   = displayImage.data[cvIndex];
//							green = displayImage.data[cvIndex];
//							blue  = displayImage.data[cvIndex];
//
//							image.setPixel(x, y, qRgb(red, green, blue));
//							cvIndex++;
//						}
//						cvLineStart += displayImage.step;
//					}
//					break;
//				case 3: // BGR images (Regular OpenCV Color Capture)
//					if ( (displayImage.cols != image.width()) ||
//						 (displayImage.rows != image.height()) )
//					{
//						QImage temp(displayImage.cols, displayImage.rows,
//								QImage::Format_RGB32);
//						image = temp;
//					}
//					cvIndex = 0;
//					cvLineStart = 0;
//					for (int y = 0; y < displayImage.rows; y++)
//					{
//						unsigned char red, green, blue;
//						cvIndex = cvLineStart;
//						for (int x = 0; x < displayImage.cols; x++)
//						{
//							// DO it
//							red   = displayImage.data[cvIndex + 2];
//							green = displayImage.data[cvIndex + 1];
//							blue  = displayImage.data[cvIndex + 0];
//
//							image.setPixel(x, y, qRgb(red, green, blue));
//							cvIndex += 3;
//						}
//						cvLineStart += displayImage.step;
//					}
//					break;
//				default:
//					printf("This number of channels is not supported\n");
//					break;
//			}
//			break;
//		default:
//			printf("This type of Image is not implemented in QcvMatWidget\n");
//			break;
//	}

