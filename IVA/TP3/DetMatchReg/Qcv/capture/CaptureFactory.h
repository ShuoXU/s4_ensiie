/*
 * CaptureFactory.h
 *
 *  Created on: 11 févr. 2012
 *	  Author: davidroussel
 */

#ifndef CAPTUREFACTORY_H_
#define CAPTUREFACTORY_H_

#include <QString>
#include <QSize>
#include <QThread>
#include <Qcv/capture/QcvVideoCapture.h>
#include <Qcv/capture/QcvTimerVideoCapture.h>
#include <Qcv/capture/QcvThreadedVideoCapture.h>

/**
 * Capture Factory creates QcvVideoCapture from arguments list
 */
class CaptureFactory
{
	private:
		/**
		 * The capture instance to create
		 */
		QcvVideoCapture *capture;

		/**
		 * Device number to open. Generally :
		 * 	- 0 is internal or fisrt camera
		 * 	- 1 is external or second camera
		 */
		int deviceNumber;

		/**
		 * Indicates capture opens camera or file.
		 * Default value is true
		 */
		bool liveVideo;

		/**
		 * Video should be flipped horizontally for mirror effect
		 * Default value is false
		 */
		bool flippedVideo;

		/**
		 * Video should be converted to gray during capture.
		 * Default value is false
		 */
		bool grayVideo;

		/**
		 * Capture can skip capturing new image when previous image has not
		 * been processed yet, or can wait for the previous image to be
		 * processed before grabbing a new image.
		 */
		bool skipImages;

		/**
		 * Video preferred width (evt resize video)
		 * Default value is 0 which means no preferred width
		 */
		int preferredWidth;

		/**
		 * Video preferred height (evt resize video)
		 * Default value is 0 which means no preferred height
		 */
		int preferredHeight;

		/**
		 * Path to video file
		 */
		QString videoPath;

		/**
		 * Does capture uses an external thread (eventually the current thread)
		 * to update capture, or does it use it's own thread
		 */
		bool externalThread;

	public:
		/**
		 * Capture Factory constructor.
		 * Arguments can be
		 * 	- [-d | --device] <device number> : camera number
		 * 	- [-f | --file] <filename> : video file name
		 * 	- [-m | --mirror] : flip image horizontally
		 * 	- [-g | --gray] : convert to gray level
		 * 	- [-s | --size] <width>x<height>: preferred width and height
		 * @param argList program the argument list provided as a list of
		 * strings
		 */
		CaptureFactory(const QStringList & argList);

		/**
		 * Capture factory destructor
		 */
		virtual ~CaptureFactory();

		/**
		 * Indicates if capture is live (video fee from camera)
		 * @return true is video feed is from camera or false if video feed
		 * is from file.
		 */
		bool isLiveVideo() const;

		/**
		 * Set the capture to live (webcam) or file source
		 * @param live the video source
		 */
		void setLiveVideo(const bool live);

		/**
		 * Gets the current device number (even if video is from file)
		 * @return the current device number
		 */
		int getDeviceNumber() const;

		/**
		 * Set device number to use when instanciating the capture with
		 * live video.
		 * @param deviceNumber the device number to use
		 */
		void setDeviceNumber(const int deviceNumber);

		/**
		 * Gets the current path of video file
		 * @return the current path of video file
		 */
		QString getFile() const;

		/**
		 * Set path to video file when #liveVideo is false
		 * @param path the path to the video file source
		 */
		void setFile(const QString & path);

		/**
		 * Indicats if video feed is flipped horizontally
		 * @return the flipped state of video feed
		 */
		bool isFlipped() const;

		/**
		 * Set video horizontal flip state (useful for selfies)
		 * @param flipped the horizontal flip state
		 */
		void setFlipped(const bool flipped);

		/**
		 * Indicats if video feed is converted to gray
		 * @return the gray conversion state of video feed
		 */
		bool isGray() const;

		/**
		 * Set gray conversion
		 * @param gray the gray conversion state
		 */
		void setGray(const bool gray);

		/**
		 * Indicates if video feed can skip images (when video capture can't get
		 * lock on captured image, capture update is skipped).
		 * This situation occurs when processor performs heavy computation that
		 * might not be finished when capture update is ready.
		 * @return skippable state of the capture
		 */
		bool isSkippable() const;

		/**
		 * Set video grabbing skippable. When true, grabbing is skipped when
		 * previously grabbed image has not been processed yet. Otherwise,
		 * grabbing new image wait for the previous image to be processed.
		 * This only applies if capture is run in a separate thread.
		 * @param skip the video grabbing skippable state
		 */
		void setSkippable(const bool skip);

		/**
		 * Gets the current video requested size from capture
		 * @return the current video frame size
		 */
		QSize getSize() const;

		/**
		 * Set video size (independently of video source actual size)
		 * @param width the desired image width
		 * @param height the desired image height
		 */
		void setSize(const size_t width, const size_t height);

		/**
		 * Set video size (independently of video source actual size)
		 * @param size the desired video size
		 */
		void setSize(const QSize & size);

		/**
		 * Indicates if capture can use an external thread of if capture
		 * uses it's own internal thread to update the capture
		 * @return the external thread usage flag
		 */
		bool useExternalThread() const;

		/**
		 * Sets the external thread flag :
		 *	- if flag is true then #getCaptureInstance(QThread * updatethread)
		 *	returns a QcvTimerVideoCapture instance evt using an updateThread.
		 *	- if flag is false then #getCaptureInstance(QThread * updatethread)
		 * returns a QcvThreadedVideoCapture instance using its own update thread
		 * (updateThread is then ignored)
		 * @param value
		 */
		void setExternalThread(const bool value);

		/**
		 * Provide capture instanciated according to values
		 * extracted from argument lists
		 * @param updateThread the thread to run this capture or NULL if this
		 * capture run in the current thread
		 * @return the new capture instance
		 */
		QcvVideoCapture * getCaptureInstance(QThread * updatethread = NULL);
};

#endif /* CAPTUREFACTORY_H_ */
