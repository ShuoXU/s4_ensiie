/*
 * QEnumController.cpp
 *
 *  Created on: 17 juin 2015
 *      Author: davidroussel
 */

#include <QDebug>
#include <QStandardItemModel>
#include <QStandardItem>

#include <Qcv/controllers/QEnumController.h>

/*
 * Copy constructor
 * @param ec the QEnumController to copy
 */
QEnumController::QEnumController(const QEnumController & ec) :
	QEnumController(ec.managedValue,
					ec.minValue,
					ec.nbElements,
					ec.descriptions,
					ec.lock,
					ec.callback,
					ec.parent())
{
}

/*
 * Move constructor
 * @param ec the QEnumController to copy
 */
QEnumController::QEnumController(QEnumController && ec) :
	QEnumController(ec.managedValue,
					ec.minValue,
					ec.nbElements,
					ec.descriptions,
					ec.lock,
					ec.callback,
					ec.parent())
{
}

/*
 * Destructor
 */
QEnumController::~QEnumController()
{
	descriptions.clear();
	radioButtons.clear();
	actions.clear();
	signalMappers.clear();
	// signal mappers will be destroyed automatically as children of this object
}

/*
 * Setup one or several sources to reflect the managed value.
 * In this case it could be a set of QRadioButtons or a QComboBox
 * @param source the source to setup (in the case of a QComboBox) or
 * the widget containing the sources to setup (we'll have to search for
 * QRadioButtons inside this source)
 * @return true if the combo box have been correctly setup or if
 * the right number of radio buttons or checkable actions have been
 * found and set up, false otherwise
 */
bool QEnumController::setupSource(QObject * source)
{
	QComboBox * comboBox = dynamic_cast<QComboBox *>(source);
	QMenu * menu = dynamic_cast<QMenu *>(source);
	QWidget * widget = dynamic_cast<QWidget *>(source);
	if (comboBox != nullptr)
	{
		return setupComboBox(comboBox);
	}
	else if (menu != nullptr)
	{
		return setupMenu(menu);
	}
	else if (widget != nullptr)
	{
		return setupRadioButtons(widget);
	}

	return false;
}

/*
 * Setup a combobox with (#nbElements) elements extracted
 * from #descriptions and set the currently selected item to #managedValue
 * @param comboBox the combobox to setup
 * @return true if the combox have been correcty set up, false
 * otherwise.
 */
bool QEnumController::setupComboBox(QComboBox * comboBox)
{
	if (QAbstractController<int>::setupSource(comboBox))
	{
		// Clears the previously set elements
		comboBox->clear();

		// Adds elements from the list of descriptions
		int currentValue = minValue;
		for (QStringList::ConstIterator sit = descriptions.cbegin();
			 sit != descriptions.cend(); ++sit)
		{
			comboBox->addItem(*sit, QVariant(currentValue++));
		}

		// Set selected item
		comboBox->setCurrentIndex(*managedValue - minValue);

		// Connect signals to slots
		connect(comboBox, SIGNAL(currentIndexChanged(int)),
				this, SLOT(setValue(int)));
		connect(comboBox, SIGNAL(currentIndexChanged(QString)),
				this, SLOT(setValue(QString)));
		/*
		 * setValue(int | QString) blocks signals from connected widgets
		 * before emitting valueChanged signal in order to avoid signal/slot
		 * loops because currentIndexChanged notifier signal is
		 * currentIndexChanged
		 */
		connect(this, SIGNAL(valueChanged(int)),
				comboBox, SLOT(setCurrentIndex(int)));

		return true;
	}
	else
	{
		qWarning() << comboBox << "has not been setup correctly";
	}
	return false;
}

/*
 * Search for (#nbElements) radio buttons in this widget and setup
 * these radio buttons with #descriptions
 * @param widget the widget (potentially) containing radio buttons to
 * set up
 * @return true if the widget contained the same number of radio buttons
 * as the #descriptions size and they all have been correctly set up.
 */
bool QEnumController::setupRadioButtons(QWidget * widget)
{
	if (QAbstractController<int>::setupSource(widget))
	{
		// Remove parent widget from widget set
		int rindex = sources.indexOf(widget);
		if (rindex != -1)
		{
			sources.removeAt(rindex);
			sourceLockStatus.removeAt(rindex);
		}

		// Search for radio buttons in this widget
		QObjectList olist = widget->children();
		QList<QRadioButton *> localRadioButtons;
		for (QObjectList::ConstIterator oit = olist.cbegin();
			 oit != olist.cend(); ++oit)
		{
			QRadioButton * rb = dynamic_cast<QRadioButton *>(*oit);
			if (rb != nullptr)
			{
				localRadioButtons.push_back(rb);
			}
		}

		// If the number of radio buttons matches nbElements, then we can
		// customize these radiobuttons
		if (localRadioButtons.size() == descriptions.size())
		{

			QSignalMapper * signalMapper = new QSignalMapper(this);

			int index = 0;
			QStringList::ConstIterator sit = descriptions.cbegin();
			QList<QRadioButton *>::Iterator rit = localRadioButtons.begin();
			QList<QRadioButton *>::Iterator rlast = localRadioButtons.end();
			for (; rit != rlast; ++rit, ++sit)
			{
				QRadioButton * radioButton = *rit;
				QString title = *sit;

				sources += radioButton;
				sourceLockStatus += radioButton->signalsBlocked();

				radioButton->setText(title);
				if (index == *managedValue - minValue)
				{
					radioButton->setChecked(true);
				}

				connect(radioButton, SIGNAL(clicked()),
						signalMapper, SLOT(map()));
				signalMapper->setMapping(radioButton, index);
				index++;
			}

			connect(signalMapper, SIGNAL(mapped(int)),
					this, SLOT(setValue(int)));

			radioButtons += localRadioButtons;
			signalMappers += signalMapper;

			return true;
		}
		else
		{
			qWarning() << "QEnumController::setupWidget"
					   << "Number of radio buttons found does not match nbElements"
					   << radioButtons.size() << "!=" << nbElements;
			radioButtons.clear();
		}
	}
	else
	{
		qWarning() << widget << "has not been setup correctly";
	}

	return false;
}

/*
 * Setup a menu with (#nbElements) elements extracted from #descriptions
 * and set the currently selected item to #managedValue
 * @param menu the menu to setup
 * @return true if the menu have been correctly set up, false otherwise
 */
bool QEnumController::setupMenu(QMenu * menu)
{
	if (QAbstractController<int>::setupSource(menu))
	{
		// Remove parent widget from widget set
		int rindex = sources.indexOf(menu);
		if (rindex != -1)
		{
			sources.removeAt(rindex);
			sourceLockStatus.removeAt(rindex);
		}

		// Search for QActions in this QMenu
		QList<QAction *> menuActions = menu->actions();
		QList<QAction *> localActions;
		for (QList<QAction *>::ConstIterator oit = menuActions.cbegin();
			 oit != menuActions.cend(); ++oit)
		{
			QAction * action = *oit;
			if (action != nullptr)
			{
				/*
				 * Caution : Only checkable actions are taken into account
				 */
				if (action->isCheckable())
				{
//					qDebug() << "Adding action" << action->text() << "to QEnumContoller";
					localActions.push_back(action);
				}
//				else
//				{
//					qWarning() << "QEnumController::setupMenu"
//							   << "action" << action->text() << "is not checkable";
//				}
			}
		}

		// If the number of radio buttons matches nbElements, then we can
		// customize these actions
		if (localActions.size() == descriptions.size())
		{
			QSignalMapper * signalMapper = new QSignalMapper(this);

			int index = 0;
			QStringList::ConstIterator sit = descriptions.cbegin();
			QList<QAction *>::Iterator ait = localActions.begin();
			QList<QAction *>::Iterator alast = localActions.end();
			for (; ait != alast; ++ait, ++sit)
			{
				QAction * action = *ait;
				QString title = *sit;

				sources += action;
				sourceLockStatus += action->signalsBlocked();

				action->setText(title);
				if (index == *managedValue - minValue)
				{
					action->setChecked(true);
				}

				connect(action, SIGNAL(triggered()),
						signalMapper, SLOT(map()));
				signalMapper->setMapping(action, index);
				index++;
			}

			connect(signalMapper, SIGNAL(mapped(int)),
					this, SLOT(setValue(int)));

			actions += localActions;
			signalMappers += signalMapper;

			return true;
		}
		else
		{
			qWarning() << "QEnumController::setupWidget"
					   << "Number of checkable actions found (" << localActions.size()
					   << ") does not match nbElements (" << nbElements << ")";
			actions.clear();
		}
	}
	else
	{
		qWarning() << menu << "has not been setup correcty";
	}

	return false;
}

/*
 * Slot to change the #enabled state of GUI elements connected to this
 * controller
 * @param value the new enabled state
 */
void QEnumController::setEnabled(const bool value)
{
	QAbstractController<int>::setEnabled(value);
}

/*
 * Sets new value in the int managed value by providing a new index
 * in the underlying enum.
 * @param index the index of the value to set
 * @return true if the index is valid and the value has been set
 * without correction, false otherwise.
 * @post The following signals have been emitted:
 *	#valueChanged(const int &);
 *	#updated();
 */
bool QEnumController::setValue(const int index)
{
	if (index != (*managedValue - minValue) && index >= 0 && index < nbElements)
	{
		bool hasLock = (lock != nullptr);
		int oldValue = *managedValue;
		bool result;

		if (hasLock)
		{
			lock->lock();
		}
		if (callback == nullptr)
		{
			*managedValue = index + minValue;
		}
		else
		{
			(*callback)(index + minValue);
		}

		if (!checkConstraints())
		{
			if (callback == nullptr)
			{
				*managedValue = oldValue;
			}
			else
			{
				(*callback)(oldValue);
			}
			result = false;
		}
		else
		{
			result = true;
		}

		// Save value to preferences (if possible)
		if ((settings != nullptr) &&
			(settingsName.size() > 0) &&
			(*managedValue != oldValue))
		{
			settings->setValue(settingsName, index);
		}

		if (hasLock)
		{
			lock->unlock();
		}

		/*
		 * Block signals in connected widgets to avoid signal/slots loops
		 */
		blockSignalsOnConnectedWidgets();

		/*
		 * Emits signals
		 */
		emitValueChanged(*managedValue - minValue);
		emitUpdated();

		// Search for (other) radiobuttons or actions corresponding to this
		// index that haven't been checked yet
		updateRadioButtons(index);
		updateActions(index);

		/*
		 * Unblock signals in connected widgets
		 */
		unblockSignalsOnConnectedWidgets();

		return result;
	}

	return false;
}

/*
 * Set the value corresponding to a description.
 * @param text the text corresponding to a description
 * @return true if the description have been found and managedValue
 * has been changed, false otherwise
 */
bool QEnumController::setValue(const QString & text)
{
	// search for this element in the set of descriptions
	int index = descriptions.indexOf(text);
	if (index != -1)
	{
		return setValue(index);
	}

	return false;
}

/*
 * Enable a specific value amongst all enumerated values by enabling its
 * correspondant widget element (QAction in a QMenu or specific item in
 * a QComboBox or QRadioButton).
 * @note By default all enumerated values are enabled.
 * @param value the value to enable amongst enumerated values
 * @return true if the corresponding enumerated value has been
 * enabled, i.e, its correpondant widget or widget element is accessible
 * again.
 */
bool QEnumController::enable(const int & value)
{
	return enable(value, true);
}

/*
 * Disable a specific value amongst all enumerated values by disabling
 * its correspondant widget element (QAction in a QMenu or specific item
 * in a QComboBox or QRadioButton).
 * @note By default all enumerated values are enabled.
 * @param value the value to disable amongst enumerated values
 * @return true if the corresponding enumerated value has been
 * disabled, i.e, its correpondant widget or widget element is no more
 * accessible.
 */
bool QEnumController::disable(const int & value)
{
	return enable(value, false);
}


/*
 * Enable or disable a specific value amongst all enumerated values by
 * enabling/disabling its correspondant widget element (QAction in a
 * QMenu or specific item in a QComboBox or QRadioButton).
 * @note By default all enumerated values are enabled.
 * @param value the value to enable amongst enumerated values
 * @param enabled the status to set on the correspondant widget
 * @return true if the corresponding enumerated value has been set to
 * the deisred value, i.e, its correpondant widget or widget element is
 * accessible enabled or not.
 * @note this method should be used in #enable(const int &) and
 * #disable(const int &) methods
 * @see #enable(const int &)
 * @see #disable(const int &)
 */
bool QEnumController::enable(const int & value, const bool enabled)
{
	size_t changedItems = 0;
	if ((value >= minValue) && (value < (minValue + nbElements)))
	{
		const int index = value - minValue; // in [0..nbElements[

		/*
		 * browse #sources for widget elements representing this index
		 * sources can contain :
		 *	- radiobuttons
		 *	- actions
		 *	- comboboxes
		 * So we are looking for a source which has the same description
		 * as descriptions[index]
		 */
		QString description = descriptions[index];
		for (QList<QObject *>::iterator it = sources.begin();
			 it != sources.end();
			 ++it)
		{
			QObject * source = *it;

//			qDebug() << Q_FUNC_INFO << "current source = " << source;

			QRadioButton * radioButton = dynamic_cast<QRadioButton *>(source);
			if (radioButton != nullptr)
			{
				if (radioButton->text() == description)
				{
					radioButton->setEnabled(enabled);
					changedItems++;
				}
			}

			QAction * action = dynamic_cast<QAction *>(source);
			if (action != nullptr)
			{
				if (action->text() == description)
				{
					action->setEnabled(enabled);
					changedItems++;
				}
			}

			QComboBox * comboBox = dynamic_cast<QComboBox *>(source);
			if (comboBox != nullptr)
			{
				/*
				 * Get the model of the combobox,
				 * Get the item you want from this model
				 * Set/Unset the Qt::ItemIsEnabled flag on this item
				 */
				QStandardItemModel * model = qobject_cast<QStandardItemModel *>(comboBox->model());
				if (model != nullptr)
				{
					// We're searching a specific item in this model
					if (index < comboBox->count())
					{
						QStandardItem * item = model->item(index);
						item->setFlags(enabled ?
									   (item->flags() | Qt::ItemIsEnabled) :
									   (item->flags() & ~Qt::ItemIsEnabled));
					}
					else
					{
						qFatal("%s: Invalid index %d in comboBox %s with %d elements",
							   Q_FUNC_INFO,
							   index,
							   comboBox->objectName().toStdString().c_str(),
							   comboBox->count());
					}
				}
				else
				{
					qFatal("%s: unable to get standard item model from %s",
						   Q_FUNC_INFO,
						   comboBox->objectName().toStdString().c_str());
				}
			}
		}

		return changedItems > 0;
	}
	return false;
}

/*
 * Search for radio buttons corresponding to this index that haven't
 * been checked yet (in case of multiple sets of radiobuttons connected
 * to this controller) and check them
 * @param the index to check
 * @pre index is supposed to be a valid index
 * @post All radiobuttons corresponding to this index that haven't been
 * check yet are checked
 * @see #radioButtons
 * @note this method should exclusively used within
 * #setValue(const int &) between #blockSignalsOnConnectedWidgets() and
 * #unblockSignalsOnConnectedWidgets();
 */
void QEnumController::updateRadioButtons(const int & index)
{
	QList<QRadioButton *>::Iterator rit = radioButtons.begin();
	QList<QRadioButton *>::Iterator rend = radioButtons.end();
	QString title = descriptions[index];
	for (; rit != rend; ++rit)
	{
		QRadioButton * rb = *rit;
//		qDebug() << "inspecting radiobutton" << rb->text();
		if (rb->text() == title)
		{
//			qDebug() << "Also checking" << rb->text() << "radiobutton";
			if (!rb->isChecked())
			{
				rb->setChecked(true);
			}
//			else
//			{
//				qDebug() << rb->text() << "is already checked";
//			}
		}
		else
		{
			rb->setChecked(false);
		}
	}
}

/*
 * Search for actions corresponding to this index that haven't
 * been checked yet (in case of multiple sets of actions connected
 * to this controller) and check them
 * @param the index to check
 * @pre index is supposed to be a valid index
 * @post All actions corresponding to this index that haven't been
 * check yet are checked
 * @see #actions
 * @note this method should exclusively used within
 * #setValue(const int &) between #blockSignalsOnConnectedWidgets() and
 * #unblockSignalsOnConnectedWidgets();
 */
void QEnumController::updateActions(const int & index)
{
	QList<QAction *>::Iterator ait = actions.begin();
	QList<QAction *>::Iterator alast = actions.end();
	QString title = descriptions[index];
	for (; ait != alast; ++ait)
	{
		QAction * action = *ait;
//		qDebug() << "inspecting action" << action->text();
		if (action->text() == title)
		{
//			qDebug() << "Also checking" << action->text() << "action";
			if (!action->isChecked())
			{
				action->setChecked(true);
			}
//			else
//			{
//				qDebug() << action->text() << "is already checked";
//			}
		}
		else
		{
			action->setChecked(false);
		}
	}
}

/*
 * Checks if the argument controller is compatible with this controller:
 * By checking the number of elements in the enum and the min value
 * of the enum
 * @param controller the controller to check
 * @return true if the argument controller is compatible with
 * this controller.
 */
bool QEnumController::checkMirror(QAbstractController<int> * controller)
{
	QEnumController * eController = dynamic_cast<QEnumController *>(controller);
	if (eController != nullptr)
	{
		return (minValue == eController->minValue) &&
			   (nbElements == eController->nbElements);
	}

	return false;
}

/*
 * Connects this controller's valueChanged signal to "controller"'s
 * setValue Slot
 * @param controller the controller to connect
 * @note concrete subclasses must implement this method
 */
void QEnumController::connectMirror(QAbstractController<int> * controller)
{
	if (controller != nullptr)
	{
		QEnumController * eController = dynamic_cast<QEnumController *>(controller);
		if (eController != nullptr)
		{
			if (checkMirror(controller))
			{
				connect(this, SIGNAL(valueChanged(int)),
						eController, SLOT(setValue(int)));
			}
			else
			{
				qWarning() << Q_FUNC_INFO << "controller" << controller
						   << "is not compatible with"
						   << this;
			}
		}
		else
		{
			qWarning() << Q_FUNC_INFO << "controller " << controller
					   << "could not be casted to"
					   << this->metaObject()->className();
		}
	}
}

/*
 * Disconnects this controller's valueChanged signal from "controller"'s
 * setValue Slot
 * @param controller the controller to disconnect
 * @note concrete subclasses must implement this method
 */
void QEnumController::disconnectMirror(QAbstractController<int> * controller)
{
	if (controller != nullptr)
	{
		QEnumController * eController = dynamic_cast<QEnumController *>(controller);
		if (eController != nullptr)
		{
			disconnect(this, SIGNAL(valueChanged(int)),
					   eController, SLOT(setValue(int)));
		}
		else
		{
			qWarning() << Q_FUNC_INFO << "controller " << controller
					   << "could not be casted to"
					   << this->metaObject()->className();
		}
	}
}

/*
 * sends a signal when anything changes
 */
void QEnumController::emitUpdated()
{
	emit updated();
}

/*
 * Sends a signal when value has been set
 * @param value the new value
 */
void QEnumController::emitValueChanged(const int value)
{
	emit valueChanged(value);
}
