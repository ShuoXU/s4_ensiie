/*
 * CvDetector.cpp
 *
 *  Created on: 29 févr. 2012
 *	  Author: davidroussel
 */
#include <iostream>	// for cout, cerr, clog
#include <sstream>	// for ostringstream
#include <ctime>	// for clock (time measurement)
using namespace std;

#include <opencv2/imgproc.hpp>

#include "CvDetector.h"

/*
 * Feature names to compare with arguments
 */
const string CvDetector::FeatureNames[integral(CvDetector::FeatureType::FEATURE_NUMBER)] =
{
	"FAST",
	"STAR",
	"SIFT",
	"SURF",
	"ORB",
	"BRISK",
	"MSER",
	"GFTT",
	"KAZE",
	"AKAZE"
};

/*
 * Descriptors extractors names to compare with arguments
 */
const string CvDetector::DescriptorNames[integral(CvDetector::DescriptorExtractorType::DESCRIPTOR_NUMBER)] =
{
	"SIFT",
	"SURF",
	"ORB",
	"BRISK",
	"BRIEF",
	"FREAK",
	"KAZE",
	"AKAZE"
};

/*
 * Preferred descriptor for a feature.
 * @note We're using BRIEF descriptor as a fallback for every feature
 * which doesn't have a dedicated descriptor (FAST, STAR, MSER, GFTT)
 */
const CvDetector::DescriptorExtractorType
CvDetector::PreferredDescriptor[integral(CvDetector::FeatureType::FEATURE_NUMBER)] =
{
	CvDetector::DescriptorExtractorType::BRIEF_DESCRIPTOR,	// FAST Feature --> Brief Descriptor
	CvDetector::DescriptorExtractorType::BRIEF_DESCRIPTOR,	// STAR Feature --> Brief Descriptor
	CvDetector::DescriptorExtractorType::SIFT_DESCRIPTOR,	// SIFT Feature --> SIFT Descriptor
	CvDetector::DescriptorExtractorType::SURF_DESCRIPTOR,	// SURF Feature --> SURF Descriptor
	CvDetector::DescriptorExtractorType::ORB_DESCRIPTOR,	// ORB Feature --> ORB Descriptor
	CvDetector::DescriptorExtractorType::BRISK_DESCRIPTOR,	// BRISK Feature --> BRISK Descriptor
	CvDetector::DescriptorExtractorType::BRIEF_DESCRIPTOR,	// MSER Feature --> Brief Descriptor
	CvDetector::DescriptorExtractorType::BRIEF_DESCRIPTOR,	// GFTT Feature --> Brief Descriptor
	CvDetector::DescriptorExtractorType::KAZE_DESCRIPTOR,	// KAZE Feature --> KAZE Descriptor
	CvDetector::DescriptorExtractorType::AKAZE_DESCRIPTOR,	// AKAZE Feature --> AKAZE Descriptor
};

/*
 * Nature of the descriptor content.
 * Useful to know what kind of mathcher can be used with these descriptors
 */
const bool CvDetector::DescriptorBinary[integral(CvDetector::DescriptorExtractorType::DESCRIPTOR_NUMBER)] =
{
	false,	// SIFT descriptors contains numbers
	false,	// SURF descriptors contains numbers
	true,	// ORB descriptors contains booleans
	true,	// BRISK descriptors contains booleans
	true,	// BRIEF descriptors contains booleans
	true,	// FREAK descriptors contains booleans
	false,	// KAZE descriptors contains numbers
	true	// AKAZE descriptors contains booleans
};

/*
 * Cv feature point detector creator
 * @param sourceImage the source image
 * @param feature the type of feature points to detect
 * @param descriptor
 * @throw CvProcessorException if feature detector or descriptor
 * extractor are empty because they couldn't be created properly
 */
CvDetector::CvDetector(Mat *sourceImage,
					   const FeatureType feature,
					   const DescriptorExtractorType descriptor,
					   const VerboseLevel level)
	throw (CvProcessorException) :
	CvProcessor(sourceImage, level),
	grayInput(nbChannels == 1),
	grayImage(size, CV_8UC1),
	featureType(FeatureType::FEATURE_NUMBER), // should be != feature at first
	descriptorType(DescriptorExtractorType::DESCRIPTOR_NUMBER), // should be != descriptor at first
	detectionTime(0.0),
	meanDetectionTime(),
	extractionTime(0.0),
	meanExtractionTime(),
	detectorAndExtractorPaired(false),
	// pairedDetector is already initialized to nullptr
	autoUpdatePairedDetector(false),
	paired(false),
	updated(false)
{
	// Set Feature type
	setFeatureType(feature);
	// Set Descriptor extractor type
	setDescriptorExtractorType(descriptor);
	// Set source image (without setting up gray image, feat. type & descr. type)
	setup(sourceImage, false);

//	addImage("gray", &grayImage);

	// all set! so update at least once, this should be enough for static images
	if (sourceImage != nullptr)
	{
		update();
	}
}

/*
 * Constructor of a paired detector.
 * Allow to share an already instanciated #featureDetector and
 * #descriptorExtractor:
 * 	- source image, keyPoints and descriptors are specific to this
 * 	detector
 * 	- but the #featureDetector and #descriptorExtractor inside comes
 * 	from the other detector
 * @param sourceImage the new source image
 * @param det the detector to copy the FeatureDetector and
 * DescriptorExtractor from
 * @param autoUpdate Indicates this detector should be auto updated when
 * the detector it is paired with changes its parameters (feature type
 * or descritor extractor type)
 */
CvDetector::CvDetector(Mat * sourceImage,
					   CvDetector * det,
					   const bool autoUpdate) :
	CvProcessor(sourceImage, (det != nullptr ? det->verboseLevel : VERBOSE_NONE)),
	grayInput(nbChannels == 1),
	grayImage(size, CV_8UC1),
	featureType(FeatureType::FEATURE_NUMBER), // should be != feature at first
	descriptorType(DescriptorExtractorType::DESCRIPTOR_NUMBER), // should be != descriptor at first
	featureName(),
	descriptorName(),
	featureDetector(),
	descriptorExtractor(),
	detectionTime(0.0),
	meanDetectionTime(),
	extractionTime(0.0),
	meanExtractionTime(),
	detectorAndExtractorPaired(),
	// pairedDetector is already initialized to nullptr
	autoUpdatePairedDetector(false),
	paired(true),
	updated(false)
{
	assert(det != nullptr);

	det->pairDetector(this, autoUpdate);

//	addImage("gray", &grayImage);

	if (sourceImage != nullptr)
	{
		// all set! so update at least once, this should be enough
		// for static images
		update();
	}
}

/*
 * Cv Feature points detector destructor
 */
CvDetector::~CvDetector()
{
	cleanup();
}

/*
 * Get the image selected for display
 * @return a reference to the image selected for display
 */
const Mat & CvDetector::getDisplayImage() const
{
	return *sourceImage;
}

/*
 * Get the pointer to the image selected for display
 * @return a pointer to the image selected for display
 */
Mat * CvDetector::getDisplayImagePtr()
{
	return sourceImage;
}

/*
 * Setup internal attributes according to source image
 * @param sourceImage a new source image
 * @param fullSetup full setup is needed when source image is changed
 * @pre sourceimage is not NULL
 * @post only calls CvProcessor::setup and checks source image has only
 * one channel otherwise the detector might fail
 * @throw CvProcessorException if sourceImage is neither gray level or
 * BGR image
 */
void CvDetector::setup(Mat *sourceImage, const bool fullSetup)
	throw (CvProcessorException)
{
	// Super setup
	CvProcessor::setup(sourceImage, fullSetup);

	if (fullSetup)
	{
		grayImage.create(size, CV_8UC1);
		setFeatureType(featureType);
		setDescriptorExtractorType(descriptorType);
	}

	// Checks this image has only one channel otherwise the detector might fail
	grayInput = sourceImage->channels() == 1;

	if (grayInput)
	{
		grayImage = *sourceImage;
	}
	else
	{
		if (nbChannels != 3)
		{
			throw CvProcessorException(CvProcessorException::INVALID_IMAGE_TYPE);
		}
		// else color image will be converted in update method before processing
	}
}

/*
 * Cleanup attributes before changing source image or cleaning class
 * before destruction
 */
void CvDetector::cleanup()
{
	keyPoints.clear();
	descriptorExtractor.release();
	featureDetector.release();
	grayImage.release();

	// super cleanup
	CvProcessor::cleanup();
}

/*
 * Gets the current feature type
 * @return the current feature type
 */
CvDetector::FeatureType CvDetector::getFeatureType() const
{
	return featureType;
}

/*
 * Sets a new feature type
 * @param featureType the new feature type to set
 */
void CvDetector::setFeatureType(const FeatureType featureType)
	throw (CvProcessorException)
{
	cout << __PRETTY_FUNCTION__ << "(" << static_cast<int>(featureType) << ")" << endl;
	if (!paired)
	{
		// in any cases clears keypoints
		keyPoints.clear();

		// release feature detector if needed
		if (!featureDetector.empty())
		{
			featureDetector.release();
		}

		// clears current name
		featureName.clear();

		this->featureType = featureType;
		featureName = FeatureNames[integral(featureType)];

		/*
		 * We need to check if the #descriptorExtractor is not empty
		 * and eventually already set to the feature type we need
		 */
		if ((featureName == descriptorName) && (!descriptorExtractor.empty()))
		{
			featureDetector = descriptorExtractor;
			detectorAndExtractorPaired = true;
		}
		else
		{
			// Creates new feature detector (algorithm) from featureType
			// TODO Créez le featureDetector
			// see http://docs.opencv.org/3.1.0/d0/d13/classcv_1_1Feature2D.html
			switch (featureType)
			{
				case FeatureType::FAST_FEATURE:
					// see http://docs.opencv.org/3.1.0/df/d74/classcv_1_1FastFeatureDetector.html
					// TODO featureDetector =
                    featureDetector = FastFeatureDetector::create();
					break;
				case FeatureType::STAR_FEATURE:
					// see http://docs.opencv.org/3.1.0/dd/d39/classcv_1_1xfeatures2d_1_1StarDetector.html
					// TODO featureDetector =
                    featureDetector = StarDetector::create();
					break;
				case FeatureType::SIFT_FEATURE:
					// see http://docs.opencv.org/3.1.0/d5/d3c/classcv_1_1xfeatures2d_1_1SIFT.html
					// TODO featureDetector =
                    featureDetector = SIFT::create();
					break;
				case FeatureType::SURF_FEATURE:
					// see http://docs.opencv.org/3.1.0/d5/df7/classcv_1_1xfeatures2d_1_1SURF.html
					// TODO featureDetector =
                    featureDetector = SURF::create();
					break;
				case FeatureType::ORB_FEATURE:
					// see http://docs.opencv.org/3.1.0/db/d95/classcv_1_1ORB.html
					// TODO featureDetector =
                    featureDetector = ORB::create();
					break;
				case FeatureType::BRISK_FEATURE:
					// see http://docs.opencv.org/3.1.0/de/dbf/classcv_1_1BRISK.html
					// TODO featureDetector =
                    featureDetector = BRISK::create();
					break;
				case FeatureType::MSER_FEATURE:
					// see http://docs.opencv.org/3.1.0/d3/d28/classcv_1_1MSER.html
					// TODO featureDetector =
                    featureDetector = MSER::create();
					break;
				case FeatureType::GFTT_FEATURE:
					// see http://docs.opencv.org/3.1.0/df/d21/classcv_1_1GFTTDetector.html
					// TODO featureDetector =
                    featureDetector = GFTTDetector::create();
					break;
				case FeatureType::KAZE_FEATURE:
					// see http://docs.opencv.org/3.1.0/d3/d61/classcv_1_1KAZE.html
					// TODO featureDetector =
                    featureDetector = KAZE::create();
					break;
				case FeatureType::AKAZE_FEATURE:
					// see http://docs.opencv.org/3.1.0/d8/d30/classcv_1_1AKAZE.html
					// TODO featureDetector =
                    featureDetector = AKAZE::create();
					break;
				default:
					if (verboseLevel >= CvProcessor::VERBOSE_WARNINGS)
					{
						cerr << "Unknown Feature type " << integral(featureType) << endl;
					}
					featureDetector = ORB::create();
					break;
			}
			detectorAndExtractorPaired = false;
		}

		if (!featureDetector.empty())
		{
			if (verboseLevel >= VERBOSE_NOTIFICATIONS)
			{
				clog << (detectorAndExtractorPaired ? "Paired " : "Created ")
					 << FeatureNames[integral(featureType)] << " detector" << endl;
			}

			// Prints detector algorithm info
			// detectorInfo(featureDetector, featureType);
		}
		else
		{
			// TODO décommentez les lignes suivantes après avoir créé le
			// featureDetector
            ostringstream msg;
            msg <<  "Can't create detector [" << FeatureNames[integral(featureType)] << "]";
            throw CvProcessorException(CvProcessorException::ALLOC_FAILURE,
                                       msg.str().c_str());
		}

		// If there is a paired detector update its attributes
		if (pairedDetector != nullptr)
		{
			pairedDetector->featureType = this->featureType;
			pairedDetector->featureName.clear();
			pairedDetector->featureName= featureName;
			pairedDetector->keyPoints.clear();
			pairedDetector->featureDetector.release();
			pairedDetector->featureDetector = featureDetector;
			pairedDetector->detectorAndExtractorPaired = detectorAndExtractorPaired;
			pairedDetector->resetMeanProcessTime();
			if (autoUpdatePairedDetector)
			{
				pairedDetector->update();
			}
		}

		resetMeanProcessTime();
	}
	else
	{
		if (verboseLevel >= VERBOSE_NOTIFICATIONS)
		{
			clog << "Can't set feature type on paired CvDetector" << endl;
		}
	}
}

/*
 * Get the currently detected keypoints
 * @return the currently detected keypoints
 */
const vector<KeyPoint> & CvDetector::getKeyPoints() const
{
	return keyPoints;
}

/*
 * Get the currently detected keypoints
 * @return the currently detected keypoints
 */
const vector<KeyPoint> * CvDetector::getKeyPointsPtr() const
{
	return &keyPoints;
}

/*
 * Get the current keypoints number
 * @return the current keypoints number
 */
size_t CvDetector::getNbKeypoints() const
{
	return keyPoints.size();
}

/*
 * Extract selected keypoints into another keypoint set.
 * This method is used to extract matched keypoints or inliers
 * keypoints in order to draw them.
 * @param extracted the other keypoints set to store extrated keypoints
 * @param indexes the indices of keypoints to extract
 * @param subindexes the subset of the previous indexes to extract
 * [default value is empty subset so all indexes are extracted]
 * @post extracted is filled with extracted keypoints
 */
void CvDetector::extractSelectedKeypoints(vector<KeyPoint> & extracted,
										  const vector<int> & indexes,
										  const vector<int> & subindexes)
{
	extracted.clear();

//	clog << "extract selected Keypoints" << endl
//		 << "extracted = " << extracted.size() << endl
//		 << "indexes = " << indexes.size() << endl
//		 << "subindexes = " << subindexes.size() << endl;

	if (subindexes.size() == 0) // extract all indexes
	{
//		clog << "single extracting " << indexes.size() << " out of "
//			 << keyPoints.size() << " keypoints" << endl;

		extracted.reserve(indexes.size());

		for (size_t i = 0; i < indexes.size(); i++)
		{
			extracted.push_back(keyPoints[indexes[i]]);
		}
	}
	else // extract only subindexes from indexes
	{
//		clog << "double extracting " << subindexes.size() << " out of "
//			 << indexes.size() << " indexes out of "<< keyPoints.size()
//			 << " keypoints" << endl;

		extracted.reserve(subindexes.size());

		for (size_t i= 0; i < subindexes.size(); i++)
		{
			extracted.push_back(keyPoints[indexes[subindexes[i]]]);
		}
	}

//	clog << "extracted " << extracted.size() << " keypoints" << endl;
}


/*
 * Gets the current descriptor type
 * @return the current descriptor type
 */
CvDetector::DescriptorExtractorType CvDetector::getDescriptorExtractorType() const
{
	return descriptorType;
}

/*
 * Sets a new descriptor extrator type
 * @param descriptorType the new descriptor extractor type to set
 * @throw CvProcessorException if descriptor extractor is empty because
 * it couldn't be created properly
 */
void CvDetector::setDescriptorExtractorType(const DescriptorExtractorType descriptorType)
	throw (CvProcessorException)
{
	cout << __FUNCTION__ << "(" << static_cast<int>(descriptorType) << ")" << endl;
	if (!paired)
	{
		// release descritors matrix if needed
		if (!descriptors.empty())
		{
			descriptors.release();
		}

		// release descriptor extractor if needed
		if (!descriptorExtractor.empty())
		{
			descriptorExtractor.release();
		}

		// clears current descriptor name
		descriptorName.clear();

		this->descriptorType = descriptorType;
		descriptorName = DescriptorNames[integral(descriptorType)];

		/*
		 * We need to check if there is a non empty #featureDetector already set
		 * with the desired #descriptorType
		 */
		if ((descriptorName == featureName) && (!featureDetector.empty()))
		{
			descriptorExtractor = featureDetector;
			detectorAndExtractorPaired = true;
		}
		else
		{
			// TODO Creates new descriptor extractor
			switch (descriptorType)
			{
				case DescriptorExtractorType::SIFT_DESCRIPTOR:
					// see see http://docs.opencv.org/3.1.0/d5/d3c/classcv_1_1xfeatures2d_1_1SIFT.html
					// TODO descriptorExtractor =
                    descriptorExtractor = SIFT::create();
					break;
				case DescriptorExtractorType::SURF_DESCRIPTOR:
					// see http://docs.opencv.org/3.1.0/d5/df7/classcv_1_1xfeatures2d_1_1SURF.html
					// TODO descriptorExtractor =
                    descriptorExtractor = SURF::create();
					break;
				case DescriptorExtractorType::ORB_DESCRIPTOR:
					// see http://docs.opencv.org/3.1.0/db/d95/classcv_1_1ORB.html
					// TODO descriptorExtractor =
                    descriptorExtractor = ORB::create();
					break;
				case DescriptorExtractorType::BRISK_DESCRIPTOR:
					// TODO descriptorExtractor =
                    descriptorExtractor = BRISK::create();
					break;
				case DescriptorExtractorType::BRIEF_DESCRIPTOR:
					// see http://docs.opencv.org/3.1.0/d1/d93/classcv_1_1xfeatures2d_1_1BriefDescriptorExtractor.html
					// TODO descriptorExtractor =
                    descriptorExtractor = BriefDescriptorExtractor::create();
					break;
				case DescriptorExtractorType::FREAK_DESCRIPTOR:
					// see http://docs.opencv.org/3.1.0/df/db4/classcv_1_1xfeatures2d_1_1FREAK.html
					// TODO descriptorExtractor =
                    descriptorExtractor = FREAK::create();
					break;
				case DescriptorExtractorType::KAZE_DESCRIPTOR:
					// see http://docs.opencv.org/3.1.0/d3/d61/classcv_1_1KAZE.html
					// TODO descriptorExtractor =
                    descriptorExtractor = KAZE::create();
					break;
				case DescriptorExtractorType::AKAZE_DESCRIPTOR:
					// see http://docs.opencv.org/3.1.0/d8/d30/classcv_1_1AKAZE.html
					// TODO descriptorExtractor =
                    descriptorExtractor = AKAZE::create();
					break;
				default:
					if (verboseLevel >= CvProcessor::VERBOSE_WARNINGS)
					{
						cerr << "Unknown Descriptor type " << integral(featureType) << endl;
					}
					descriptorExtractor = ORB::create();
					break;
			}
			detectorAndExtractorPaired = false;
		}

		if (!descriptorExtractor.empty())
		{
			if (verboseLevel >= VERBOSE_NOTIFICATIONS)
			{
				clog << (detectorAndExtractorPaired ? "Paired " : "Created ")
					 << descriptorName
					 << " descriptor extractor with type : "
					 << (descriptorExtractor->descriptorType() == CV_8U ?
						 "Binary" : "Floating point")
					 << ", size : " << descriptorExtractor->descriptorSize() << endl;
				/*
				 * Descriptor type is either
				 * 	- CV_8U = 0 for binary valued descriptors
				 * 	- CV_32F = 5 for 32 bit floating points valued descriptors
				 */
			}

			// Prints descriptors algorithm info
			// extractorInfo(descriptorExtractor, descriptorType);
		}
		else
		{
			// TODO Décommentez les lignes suivantes lorsque le descriptorExtractor
			// est créé
            ostringstream msg;
            msg << "Can't create descriptor extractor ["
                << DescriptorNames[integral(descriptorType)] << "]";
            throw CvProcessorException(CvProcessorException::ALLOC_FAILURE,
                                       msg.str().c_str());
		}

		// If there is a paired detector update its attributes
		if (pairedDetector != nullptr)
		{
			pairedDetector->descriptorType = this->descriptorType;
			pairedDetector->descriptorName.clear();
			pairedDetector->descriptorName = descriptorName;
			pairedDetector->descriptors.release();
			pairedDetector->descriptorExtractor.release();
			pairedDetector->descriptorExtractor = descriptorExtractor;
			pairedDetector->detectorAndExtractorPaired =
				(pairedDetector->descriptorExtractor ==
				 pairedDetector->descriptorExtractor);
			pairedDetector->resetMeanProcessTime();
			if (autoUpdatePairedDetector)
			{
				pairedDetector->update();
			}
		}

		resetMeanProcessTime();
	}
	else
	{
		if (verboseLevel >= VERBOSE_NOTIFICATIONS)
		{
			clog << "Can't set descriptor type on paired CvDetector" << endl;
		}
	}
}

/*
 * Gets the preferred descriptor norm (should be NormTypes::NORM_HAMMING (6)
 * for HAMMING distance type suitable for binary descriptors or
 * NormTypes::NORM_L2 (4) L2 distances suitable for valued descriptors.
 * @return the descriptor size used by the descriptor extractor or -1
 * if there is no descriptor extractor
 */
int CvDetector::getDescriptorNorm() const
{
	if (!descriptorExtractor.empty())
	{
		return descriptorExtractor->defaultNorm();
	}
	else
	{
		return -1;
	}
}

/*
 * Gets the preferred descriptor size
 * @return the descriptor size used by the descriptor extractor or 0
 * if there is no descriptor extractor
 */
int CvDetector::getDescriptorSize() const
{
	if (!descriptorExtractor.empty())
	{
		return descriptorExtractor->descriptorSize();
	}
	else
	{
		return 0;
	}
}

/*
 * Gets the preferred descriptor type (should be
 * DescriptorTypes::BINARY_DESCRIPTOR (0) for binary
 * descriptors or DescriptorTypes::VALUED_DESCRIPTOR (5) for valued
 * descriptors.)
 * @return the descriptor type used by the descriptor extractor or -1
 * if there is no descriptor extractor
 */
int CvDetector::getDescriptorType() const
{
	if (!descriptorExtractor.empty())
	{
		return descriptorExtractor->descriptorType();
	}
	else
	{
		return -1;
	}
}

/*
 * Gets the current descriptors computed on keyPoints
 * @return the current descriptors computed on keyPoints
 */
const Mat & CvDetector::getDescriptors() const
{
	return descriptors;
}

/*
 * Gets the current descriptors computed on keyPoints
 * @return the current descriptors computed on keyPoints
 */
Mat * CvDetector::getDescriptorsPtr()
{
	return &descriptors;
}

/*
 * Return processor processing time of step index
 * @param index index of the step which processing time is required,
 * 0 indicates all steps, 1 indicates points detection time and
 * 2 indicates descriptors extraction time.
 * @return the processing time of step index.
 * @see #ProcessTimeIndex
 */
double CvDetector::getProcessTime(const size_t index) const
{
	clock_t time;
	switch(index)
	{
		case CvDetector::DETECTION:
			time = detectionTime;
			break;
		case CvDetector::EXTRACTION:
			time = extractionTime;
			break;
		case CvDetector::ALL:
		default:
			time = processTime;
			break;
	}

	if (timePerFeature)
	{
		return (double) time / (double) MAX(1, keyPoints.size());
	}
	else
	{
		return (double) time;
	}
}

/*
 * Return processor mean processing time of step index
 * @param index index of the step which processing time is required,
 * 0 indicates all steps, 1 indicates points detection time and
 * 2 indicates descriptors extraction time.
 * @return the processing time of step index.
 * @see #ProcessTimeIndex
 */
double CvDetector::getMeanProcessTime(const size_t index) const
{
	ProcessTime pTime;
	switch(index)
	{
		case CvDetector::DETECTION:
			pTime = meanDetectionTime;
			break;
		case CvDetector::EXTRACTION:
			pTime = meanExtractionTime;
			break;
		case CvDetector::ALL:
		default:
			pTime = meanProcessTime;
			break;
	}

	if (timePerFeature)
	{
		return pTime.mean() / (float) MAX(1, keyPoints.size());
	}
	else
	{
		return pTime.mean();
	}
}

/*
 * Return processor processing time standard deviation of step index
 * @param index index of the step which processing time is required,
 * 0 indicates all steps, 1 indicates points detection time and
 * 2 indicates descriptors extraction time.
 * @return the processing time of step index.
 * @see #ProcessTimeIndex
 */
double CvDetector::getStdProcessTime(const size_t index) const
{
	ProcessTime pTime;
	switch(index)
	{
		case CvDetector::DETECTION:
			pTime = meanDetectionTime;
			break;
		case CvDetector::EXTRACTION:
			pTime = meanExtractionTime;
			break;
		case CvDetector::ALL:
		default:
			pTime = meanProcessTime;
			break;
	}

	if (timePerFeature)
	{
		return pTime.std() / (float) MAX(1, keyPoints.size());
	}
	else
	{
		return pTime.std();
	}
}

/*
 * Reset mean and std process time in order to re-start computing
 * new mean and std process time values.
 * @note this reimplementation takes into account detectionTime and
 * extractionTime.
 */
void CvDetector::resetMeanProcessTime()
{
	CvProcessor::resetMeanProcessTime();

	meanDetectionTime.reset();
	meanExtractionTime.reset();
}

/*
 * Set the detector in argument as a paired detector of this.
 * The paired detector will then use the #featureDetector and
 * #descriptorExtractor as its own keypoints detector and descriptor
 * extractor
 * @param detector the detector to pair with this detector
 * @param autoUpdate Indicates the paired detector must be updated
 * whenever this detector changes its parameters (feature type or
 * descriptor extractor type)
 */
void CvDetector::pairDetector(CvDetector * detector,
							  const bool autoUpate)
{
	/*
	 * If a paired detector exists, then unpair it
	 */
	if (pairedDetector != nullptr)
	{
		unpairDetector();
	}

	if (detector != nullptr)
	{
		pairedDetector = detector;
		pairedDetector->paired = true;
		pairedDetector->featureType = featureType;
		pairedDetector->featureName.clear();
		pairedDetector->featureName= featureName;
		pairedDetector->keyPoints.clear();
		pairedDetector->featureDetector.release();
		pairedDetector->featureDetector = featureDetector;
		pairedDetector->descriptorType = this->descriptorType;
		pairedDetector->descriptorName.clear();
		pairedDetector->descriptorName = descriptorName;
		pairedDetector->descriptors.release();
		pairedDetector->descriptorExtractor.release();
		if (detectorAndExtractorPaired)
		{
			pairedDetector->descriptorExtractor = featureDetector;
		}
		else
		{
			pairedDetector->descriptorExtractor = descriptorExtractor;
		}
		pairedDetector->detectorAndExtractorPaired = detectorAndExtractorPaired;
		pairedDetector->resetMeanProcessTime();
		autoUpdatePairedDetector = autoUpate;
	}
}

/*
 * Reset the #pairedDetector to null
 */
void CvDetector::unpairDetector()
{
	if (pairedDetector !=  nullptr)
	{
		/*
		 * All #featureDetector and #descriptorExtractor can stay the same
		 * they will be released when one of them will change
		 */
		pairedDetector->paired = false;
		pairedDetector = nullptr;
	}
	autoUpdatePairedDetector = false;
}

/*
 * Feature points detector update.
 * Detects keyPoints in source image and compute descriptors on these
 * points
 */
void CvDetector::update()
{
//	clog << "Detector update" << endl;

	if (sourceImage != nullptr)
	{
		// --------------------------------------------------------------------
		// convert source image to gray (if needed)
		//	in: *sourceImage
		//	out: grayImage
		// --------------------------------------------------------------------
		convertSourceImageToGray();

		// --------------------------------------------------------------------
		// Detects keypoints on grayImage
		//	in: grayImage
		//	out: keyPoints
		// --------------------------------------------------------------------
		detectKeypoints();

		// --------------------------------------------------------------------
		// Extract descriptors around detected keypoints
		//	in: grayImage, keyPoints
		//	out: descriptors
		// --------------------------------------------------------------------
		extractDescriptors();

		processTime = detectionTime + extractionTime;
		meanProcessTime += processTime;
		updated = true;
	}
}

/*
 * Clears currenlty detected KeyPoints
 * @post keyPoints have been cleared
 */
void CvDetector::clear()
{
	keyPoints.clear();
}

/*
 * Get Feature detector algorithm (featureDetector)
 * @return a smart pointer to the featureDetector
 */
Ptr<Feature2D> CvDetector::getDetectorAlgorithm()
{
	return featureDetector;
}

/*
 * Get Descriptor extractor algorithm (descriptorExtractor)
 * @return a smart pointer to the descriptorExtractor
 */
Ptr<Feature2D> CvDetector::getExtractorAlgorithm()
{
	return descriptorExtractor;
}

/*
 * Gets the #updated status of the detector
 * @return the #updated status of the detector
 */
bool CvDetector::hasUpdated() const
{
	return updated;
}

/*
 * Resets the #updated status to false, typically after using
 * #hasUpdated() method to check if the detctor has been updated
 */
void CvDetector::resetUpdated()
{
	updated = false;
}


/*
 * Prints info on the detector
 * @param ptr smart pointer to the detector
 * @param  type of detector to recast the detector in the correct type
 * in order to explore its members
 */
void CvDetector::detectorInfo(const Ptr<Feature2D> & ptr,
							  const FeatureType type)
{
	if (!ptr.empty())
	{
		cout << "Detector: ";
		switch(type)
		{
			case FeatureType::FAST_FEATURE:
				cout << "FAST Corner detection";
				break;
			case FeatureType::STAR_FEATURE:
				cout << "STAR: STAR feature";
				break;
			case FeatureType::SIFT_FEATURE:
				cout << "SIFT: Scale Invariant Feature Transform";
				break;
			case FeatureType::SURF_FEATURE:
				cout << "SURF : Speeded Up Robust Features";
				break;
			case FeatureType::ORB_FEATURE:
				cout << "ORB : Oriented BRIEF Features";
				break;
			case FeatureType::BRISK_FEATURE:
				cout << "BRISK : Binary Robust Invariant Scalable Keypoints";
				break;
			case FeatureType::MSER_FEATURE:
				cout << "MSER : Maximally Stable Extremal Regions";
				break;
			case FeatureType::GFTT_FEATURE:
				cout << "GFTT : Good Features To Track";
				break;
			case FeatureType::KAZE_FEATURE:
				cout << "KAZE";
				break;
			case FeatureType::AKAZE_FEATURE:
				cout << "Accelerated KAZE";
				break;
			default:
				break;
		}
		cout << endl;

//		algorithmInfo(ptr);
	}
	else
	{
		cerr << "CvDetector::detectorInfo : empy detector" << endl;
	}
}

/*
 * Prints info on the feature points extractor
 * @param ptr smart pointer to the feature points extractor
 * @param  type of extractor
 */
void CvDetector::extractorInfo(const Ptr<Feature2D> & ptr,
							   const DescriptorExtractorType type)
{
	if (!ptr.empty())
	{
		cout << "Descriptor Extractor: ";
		switch(type)
		{
			case DescriptorExtractorType::SIFT_DESCRIPTOR:
				cout << "SIFT Descriptor: Scale Invariant Feature Transform";
				break;
			case DescriptorExtractorType::SURF_DESCRIPTOR:
				cout << "SURF Descriptor: Speeded Up Robust Features";
				break;
			case DescriptorExtractorType::ORB_DESCRIPTOR:
				cout << "ORB descriptor : Oriented BRIEF";
				break;
			case DescriptorExtractorType::BRISK_DESCRIPTOR:
				cout << "BRISK Descriptor : Binary Robust Invariant Scalable Keypoints";
				break;
			case DescriptorExtractorType::BRIEF_DESCRIPTOR:
				cout << "BRIEF Descriptor: Binary Robust Independent Elementary Features";
				break;
			case DescriptorExtractorType::FREAK_DESCRIPTOR:
				cout << "FREAK Descriptor: Fast Retina Keypoint";
				break;
			case DescriptorExtractorType::KAZE_DESCRIPTOR:
				cout << "KAZE Descriptor: M-SURF adapted to non-linear Scale Space";
				break;
			case DescriptorExtractorType::AKAZE_DESCRIPTOR:
				cout << "AKAZE Descriptor: Modified-Local Difference Binary (M-LDB) descriptor";
				break;
			default:
				break;
		}
		cout << " (default norm = " << ptr->defaultNorm()
			 << ", descriptor size = " << ptr->descriptorSize()
			 << ", descriptor type = " << ptr->descriptorType() << ")"
			 << endl;
	}
	else
	{
		cerr << "CvDetector::exctractorInfo : empy detector" << endl;
	}
}

/*
 * Prints info on any algorithm (Detector or extractor)
 * @param ptr smart pointer to the algorithm
 * @warning This method is obsolete since OpenCV 3.x Algorithm does not
 * feature introscpection anymore
 */
//void CvDetector::algorithmInfo(const Ptr<Algorithm> & ptr)
//{
//	if (!ptr.empty())
//	{
//		cout << ptr->name() << " Parameters = " << endl;
//		AlgorithmInfo * infoPtr = ptr->info();
//		vector<string> params;
//		infoPtr->getParams(params);

//		for (vector<string>::const_iterator it = params.begin(); it != params.end(); ++it)
//		{
//			string paramName = *it;
//			cout << "\t" << paramName << " = ";
//			int paramType = infoPtr->paramType(paramName.c_str());
//			vector<Mat> mats;
//			switch(paramType)
//			{
//				case Param::INT:
//					cout << ptr->getInt(paramName) << " [integer]";
//					break;
//				case Param::SHORT:
//					cout << ptr->getInt(paramName) << " [short]";
//					break;
//				case Param::BOOLEAN:
//					cout << (ptr->getBool(paramName) ? "true" : "false") << " [boolean]";
//					break;
//				case Param::REAL:
//					cout << ptr->getDouble(paramName) << " [double]";
//					break;
//				case Param::STRING:
//					cout << ptr->getString(paramName) << " [string]";
//					break;
//				case Param::MAT:
//					cout << ptr->getMat(paramName) << " [Mat]";
//					break;
//				case Param::MAT_VECTOR:
//					mats = ptr->getMatVector(paramName);
//					for (vector<Mat>::const_iterator it = mats.begin();
//						 it != mats.end(); ++it)
//					{
//						cout << "(" << *it << ") ";
//					}
//					cout << "[vector<Mat>]";
//					break;
//				case Param::ALGORITHM:
//					cout << ptr->getAlgorithm(paramName) << " [algorithm]";
//					break;
//				case Param::FLOAT:
//					cout << ptr->getDouble(paramName) << " [float]";
//					break;
//				case Param::UNSIGNED_INT:
//					cout << ptr->getInt(paramName) << " [unsigned int]";
//					break;
//				case Param::UINT64:
//					cout << ptr->getInt(paramName) << " [unsigned int64]";
//					break;
//				case Param::UCHAR:
//					cout << ptr->getInt(paramName) << " [unsigned char]";
//					break;
//				default:
//					cout << "[unkown type]";
//					break;
//			}
//			cout << " (" << ptr->paramHelp(paramName) << ")" << endl;
//		}
//	}
//}

/*
 * Converts source image to gray.
 * Used in the #update() method
 * @note This method should be reimplemented in Qt flavored sub classes
 * in order to use a lock on the source image
 */
void CvDetector::convertSourceImageToGray()
{
	if (!grayInput)
	{
		cvtColor(*sourceImage, grayImage, CV_BGR2GRAY);
	}
	else
	{
		grayImage = *sourceImage;
	}
}

/*
 * Detect keypoints on gray image.
 * Used in the #update() method
 * @pre source image has been converted to gray or is gray
 * @post detected keypoints are stored in #keyPoints
 * @post #detectionTime and #meanDetectionTime have been updated
 * @note This method should be reimplemented in Qt flavored sub classes
 * in order to use a lock on keypoints
 */
void CvDetector::detectKeypoints()
{
	if (!featureDetector.empty())
	{
		clock_t dstart = clock();

		// Detect feature points in the grayImage to produce keyPoints
		// TODO déclenchez la détection des keyPoints dans la grayImage
		// en utilisant le featureDetector
		// see http://docs.opencv.org/3.1.0/d0/d13/classcv_1_1Feature2D.html#aa4e9a7082ec61ebc108806704fbd7887
		// TODO featureDetector->...
        featureDetector->detect(grayImage, keyPoints);

		clock_t dend = clock();
		detectionTime = dend - dstart;
		meanDetectionTime += detectionTime;
	}
	else
	{
		detectionTime = 0;
	}
}

/*
 * Extract descriptors on keypoints detected in gray image.
 * Used in the #update() method.
 * @pre keypoints have been detected (otherwise this method will do nothing)
 * @post extracted descriptors are stored in #descriptors matrix
 * @post #extractionTime and #meanExtractionTime have been updated
 * @note This method should be reimplemented in Qt flavored sub classes
 * in order to use a lock on descriptors
 */
void CvDetector::extractDescriptors()
{
	if (!descriptorExtractor.empty())
	{
		clock_t estart = clock();

		// Compute descriptors on each detected points on grayImage with
		// keyPoints to produce descriptors
		// TODO calculez les descriptors à partir des keyPoints précédemment
		// détectés dans la grayImage avec le descriptorExtractor
		// see http://docs.opencv.org/3.1.0/d0/d13/classcv_1_1Feature2D.html#ab3cce8d56f4fc5e1d530b5931e1e8dc0
		// TODO descriptorExtractor->...
        descriptorExtractor->compute(grayImage, keyPoints, descriptors);

		clock_t eend = clock();
		extractionTime = eend - estart;
		meanExtractionTime += extractionTime;
	}
	else
	{
		extractionTime = 0;
	}
}
