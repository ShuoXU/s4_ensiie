#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QObject>
#include <QFileDialog>
#include <QWindow>
#include <QDebug>
#include <assert.h>

#include <Qcv/matWidgets/QcvMatWidgetImage.h>
#include <Qcv/matWidgets/QcvMatWidgetLabel.h>
#include <Qcv/matWidgets/QcvMatWidgetGL.h>

/*
 * MainWindow constructor
 * @param capture the capture QObject to capture frames from devices
 * or video files
 * @param parent parent widget
 */
MainWindow::MainWindow(QcvVideoCapture * capture,
					   QcvColorSpaces * processor,
					   QWidget * parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	capture(capture),
	processor(processor),
	preferredWidth(640),
	preferredHeight(480)
{
	ui->setupUi(this);
	ui->scrollArea->setBackgroundRole(QPalette::Mid);

	// ------------------------------------------------------------------------
	// Assertions
	// ------------------------------------------------------------------------
	assert(capture != NULL);

	assert(processor != NULL);

	// ------------------------------------------------------------------------
	// Signal/Slot connections
	// ------------------------------------------------------------------------
	// Replace QcvMatWidget instance with QcvMatWidgetImage instance and
	// sets widgetImage source for the first time
	setupImageWidget(RENDER_IMAGE);

	// Connects Mainwindow messages to status bar
	connect(this,
			SIGNAL(sendMessage(QString, int) ),
			ui->statusBar,
			SLOT(showMessage(QString, int) ));

	// Connects capture status messages to statusBar
	connect(capture,
			SIGNAL(messageChanged(QString, int) ),
			ui->statusBar,
			SLOT(showMessage(QString, int) ));

	// Connects processor status messages to statusBar
	connect(processor,
			SIGNAL(sendMessage(QString, int) ),
			ui->statusBar,
			SLOT(showMessage(QString, int) ));

	// When Processor source image changes, some attributes are reinitialised
	// So we have to set them up again according to current UI values
	connect(processor,
			SIGNAL(imageChanged()),
			this,
			SLOT(setupProcessorfromUI()));

	// Connects processor time to UI time label
	//	connect(processor, SIGNAL(processTimeUpdated(QString)),
	//			ui->labelProcessTimeValue, SLOT(setText(QString)));
	connect(processor,
			SIGNAL(processTimeUpdated(const CvProcessor::ProcessTime*)),
			this,
			SLOT(on_processTimeupdated(const CvProcessor::ProcessTime*)));

	// Connects UI requests to capture
	connect(this,
			SIGNAL(sizeChanged(const QSize &) ),
			capture,
			SLOT(setSize(const QSize &) ),
			Qt::DirectConnection);
	connect(this,
			SIGNAL(deviceChanged(int, size_t, size_t)),
			capture,
			SLOT(openDevice(int, size_t, size_t)),
			Qt::DirectConnection);
	connect(this,
			SIGNAL(fileChanged(QString, size_t, size_t)),
			capture,
			SLOT(openFile(QString, size_t, size_t)),
			Qt::DirectConnection);
	connect(this,
			SIGNAL(flipChanged(bool) ),
			capture,
			SLOT(setFlipped(bool)),
			Qt::DirectConnection);

	connect(this, SIGNAL(finished()),
			capture, SLOT(finish()));

	// Used whenever color channels are reactivated from gray channels since
	// we have to re-emit the correct hue mix mode
	connect(this,
			SIGNAL(hueDisplayModeChanged(const int)),
			processor,
			SLOT(setHueDisplayMode(const int)));

	connect(this, SIGNAL(finished()),
			processor, SLOT(finish()));

	// ------------------------------------------------------------------------
	// UI setup according to capture and processor options
	// ------------------------------------------------------------------------
	setupUIfromCapture();
	setupUIfromProcessor();

	// ------------------------------------------------------------------------
	// Show/Hide hue box according to displayed image
	// ------------------------------------------------------------------------
	// Deactivate color channels and hue mix boxes for now
	ui->checkBoxChColor->setEnabled(false);
	ui->hueBox->setEnabled(false);
	ui->menuHue_Mix->setEnabled(false);

	connect(ui->actionDisplayInput, SIGNAL(triggered()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->actionDisplayGray, SIGNAL(triggered()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->actionDisplayRGB_Red, SIGNAL(triggered()),
			this, SLOT(activateChannelColor()));
	connect(ui->actionDisplayRGB_Green, SIGNAL(triggered()),
			this, SLOT(activateChannelColor()));
	connect(ui->actionDisplayRGB_Blue, SIGNAL(triggered()),
			this, SLOT(activateChannelColor()));
	connect(ui->actionDisplayRGB_Max, SIGNAL(triggered()),
			this, SLOT(activateChannelColor()));
	connect(ui->actionDisplayXYZ_X, SIGNAL(triggered()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->actionDisplayXYZ_Y, SIGNAL(triggered()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->actionDisplayXYZ_Z, SIGNAL(triggered()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->actionDisplayHSV_Hue, SIGNAL(triggered()),
			this, SLOT(activateChannelColor()));
	connect(ui->actionDisplayHSV_Saturation, SIGNAL(triggered()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->actionDisplayHSV_Value, SIGNAL(triggered()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->actionDisplayYCbCr_Y, SIGNAL(triggered()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->actionDisplayYCbCr_Cr, SIGNAL(triggered()),
			this, SLOT(activateChannelColor()));
	connect(ui->actionDisplayYCbCr_Cb, SIGNAL(triggered()),
			this, SLOT(activateChannelColor()));

	connect(ui->actionDisplayInput, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayGray, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayRGB_Red, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayRGB_Green, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayRGB_Blue, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayRGB_Max, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayXYZ_X, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayXYZ_Y, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayXYZ_Z, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayHSV_Hue, SIGNAL(triggered()),
			this, SLOT(activateHueMixPanel()));
	connect(ui->actionDisplayHSV_Saturation, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayHSV_Value, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayYCbCr_Y, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayYCbCr_Cr, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->actionDisplayYCbCr_Cb, SIGNAL(triggered()),
			this, SLOT(deactivateHueMixPanel()));

	connect(ui->radioButtonInput, SIGNAL(clicked()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->radioButtonGray, SIGNAL(clicked()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->radioButtonRed, SIGNAL(clicked()),
			this, SLOT(activateChannelColor()));
	connect(ui->radioButtonGreen, SIGNAL(clicked()),
			this, SLOT(activateChannelColor()));
	connect(ui->radioButtonBlue, SIGNAL(clicked()),
			this, SLOT(activateChannelColor()));
	connect(ui->radioButtonMaxBGR, SIGNAL(clicked()),
			this, SLOT(activateChannelColor()));
	connect(ui->radioButtonXYZ_X, SIGNAL(clicked()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->radioButtonXYZ_Y, SIGNAL(clicked()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->radioButtonXYZ_Z, SIGNAL(clicked()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->radioButtonHue, SIGNAL(clicked()),
			this, SLOT(activateChannelColor()));
	connect(ui->radioButtonSaturation, SIGNAL(clicked()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->radioButtonValue, SIGNAL(clicked()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->radioButtonY, SIGNAL(clicked()),
			this, SLOT(deactivateChannelColor()));
	connect(ui->radioButtonCr, SIGNAL(clicked()),
			this, SLOT(activateChannelColor()));
	connect(ui->radioButtonCb, SIGNAL(clicked()),
			this, SLOT(activateChannelColor()));

	connect(ui->radioButtonInput, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonGray, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonRed, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonGreen, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonBlue, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonMaxBGR, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonXYZ_X, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonXYZ_Y, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonXYZ_Z, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonHue, SIGNAL(clicked()),
			this, SLOT(activateHueMixPanel()));
	connect(ui->radioButtonSaturation, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonValue, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonY, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonCr, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
	connect(ui->radioButtonCb, SIGNAL(clicked()),
			this, SLOT(deactivateHueMixPanel()));
}

/*
 * MainWindow destructor
 */
MainWindow::~MainWindow()
{
	delete ui;
}

/*
 * Changes widgetImage nature according to desired rendering mode.
 * Possible values for mode are:
 * 	- IMAGE: widgetImage is assigned to a QcvMatWidgetImage instance
 * 	- PIXMAP: widgetImage is assigned to a QcvMatWidgetLabel instance
 * 	- GL: widgetImage is assigned to a QcvMatWidgetGL instance
 * @param mode
 */
void MainWindow::setupImageWidget(const RenderMode mode)
{
	// Disconnect first
	disconnect(processor, SIGNAL(updated()), ui->widgetImage, SLOT(update()));

	disconnect(processor,
			   SIGNAL(imageChanged(Mat *) ),
			   ui->widgetImage,
			   SLOT(setSourceImage(Mat *) ));

	QWindow * currentWindow = windowHandle();
	if (mode == RENDER_GL)
	{
		disconnect(currentWindow,
				   SIGNAL(screenChanged(QScreen *)),
				   ui->widgetImage,
				   SLOT(screenChanged()));
	}

	// remove widget in scroll area
	QWidget * w = ui->scrollArea->takeWidget();

	if (w == ui->widgetImage)
	{
		// delete removed widget
		delete ui->widgetImage;

		// create new widget
		// Mat * image = processor->getImagePtr("display");
		Mat * image = processor->getDisplayImagePtr();
		QMutex * imageLock = processor->getLock(QcvColorSpaces::IMAGES);
		Q_ASSERT_X(imageLock != nullptr, Q_FUNC_INFO, "image lock is null");
		switch (mode)
		{
			case RENDER_PIXMAP:
				ui->widgetImage = new QcvMatWidgetLabel(image,
														ui->scrollArea,
														imageLock);
				break;
			case RENDER_GL:
				ui->widgetImage = new QcvMatWidgetGL(image,
													 ui->scrollArea,
													 imageLock);
				break;
			case RENDER_IMAGE:
			default:
				ui->widgetImage = new QcvMatWidgetImage(image,
														ui->scrollArea,
														imageLock);
				break;
		}

		if (ui->widgetImage != NULL)
		{
			ui->widgetImage->setObjectName(QString::fromUtf8("widgetImage"));

			// add it to the scroll area
			ui->scrollArea->setWidget(ui->widgetImage);

			connect(processor,
					SIGNAL(updated()),
					ui->widgetImage,
					SLOT(update()));

			connect(processor,
					SIGNAL(imageChanged(Mat *) ),
					ui->widgetImage,
					SLOT(setSourceImage(Mat *) ));

			if (mode == RENDER_GL)
			{
				connect(currentWindow,
						SIGNAL(screenChanged(QScreen *)),
						ui->widgetImage,
						SLOT(screenChanged()));
			}

			// Sends message to status bar and sets menu checks
			message.clear();
			message.append(tr("Render mode set to "));
			switch (mode)
			{
				case RENDER_IMAGE:
					ui->actionRenderPixmap->setChecked(false);
					ui->actionRenderOpenGL->setChecked(false);
					message.append(tr("QImage"));
					break;
				case RENDER_PIXMAP:
					ui->actionRenderImage->setChecked(false);
					ui->actionRenderOpenGL->setChecked(false);
					message.append(tr("QPixmap in QLabel"));
					break;
				case RENDER_GL:
					ui->actionRenderImage->setChecked(false);
					ui->actionRenderPixmap->setChecked(false);
					message.append("QGLWidget");
					break;
				default:
					break;
			}
			emit sendMessage(message, 5000);
		}
		else
		{
			qDebug("MainWindow::on_actionRenderXXX new widget is null");
		}
	}
	else
	{
		qDebug(
			"MainWindow::on_actionRenderXXX removed widget is not imageWidget");
	}
}

/*
 * Setup UI according to capture settings when app launches
 */
void MainWindow::setupUIfromCapture()
{
	// ------------------------------------------------------------------------
	// UI setup according to capture options
	// ------------------------------------------------------------------------
	// Sets size radioButton states
	if (capture->isResized())
	{
		/*
		 * Initial Size radio buttons configuration
		 */
		ui->radioButtonOrigSize->setChecked(false);
		ui->radioButtonCustomSize->setChecked(true);
		/*
		 * Initial Size menu items configuration
		 */
		ui->actionOriginalSize->setChecked(false);
		ui->actionConstrainedSize->setChecked(true);

		QSize size = capture->getSize();
		qDebug("Capture->size is %dx%d", size.width(), size.height());
		preferredWidth = size.width();
		preferredHeight = size.height();
	}
	else
	{
		/*
		 * Initial Size radio buttons configuration
		 */
		ui->radioButtonCustomSize->setChecked(false);
		ui->radioButtonOrigSize->setChecked(true);

		/*
		 * Initial Size menu items configuration
		 */
		ui->actionConstrainedSize->setChecked(false);
		ui->actionOriginalSize->setChecked(true);
	}

	// Sets spinboxes preferred size
	ui->spinBoxWidth->setValue(preferredWidth);
	ui->spinBoxHeight->setValue(preferredHeight);

	// Sets flipCheckbox and menu item states
	bool flipped = capture->isFlipped();
	ui->actionFlip->setChecked(flipped);
	ui->checkBoxFlip->setChecked(flipped);
}

/*
 * Setup UI according to processor settings when app launches
 */
void MainWindow::setupUIfromProcessor()
{
	QEnumController * eController;
	eController = processor->getDisplayController();
	eController->setupRadioButtons(ui->displayBox);
	eController->setupMenu(ui->menuDisplay);

	QBoolController * bController;
	bController = processor->getChannelsModeController();
	bController->setupAbstractButton(ui->checkBoxChColor);
	bController->setupAction(ui->actionChannelColor);

	eController = processor->getHueDisplayController();
	eController->setupRadioButtons(ui->hueBox);
	eController->setupMenu(ui->menuHue_Mix);

	QRangeIntController * riController;
	riController = processor->getSNRSizeController();
	riController->setupSpinBox(ui->spinBoxBlockSize);

	// Connects signal levels to progressbars
	connect(processor, SIGNAL(SNRChannel0Changed(int)),
			ui->progressBarNoise0, SLOT(setValue(int)));
	connect(processor, SIGNAL(SNRChannel1Changed(int)),
			ui->progressBarNoise1, SLOT(setValue(int)));
	connect(processor, SIGNAL(SNRChannel2Changed(int)),
			ui->progressBarNoise2, SLOT(setValue(int)));
}

/*
 * Setup processor from current UI settings when processor source image
 * changes
 */
void MainWindow::setupProcessorfromUI()
{
	if (ui->actionDisplayInput->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::INPUT));
	}

	if (ui->actionDisplayGray->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::GRAY));
	}

	if (ui->actionDisplayRGB_Red->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::RED));
	}

	if (ui->actionDisplayRGB_Green->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::GREEN));
	}

	if (ui->actionDisplayRGB_Blue->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::BLUE));
	}

	if (ui->actionDisplayHSV_Hue->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::HUE));
	}

	if (ui->actionDisplayHSV_Saturation->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::SATURATION));
	}

	if (ui->actionDisplayHSV_Value->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::VALUE));
	}

	if (ui->actionDisplayYCbCr_Y->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::Y));
	}

	if (ui->actionDisplayYCbCr_Cr->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::Cr));
	}

	if (ui->actionDisplayYCbCr_Cb->isChecked())
	{
		processor->setDisplayImageIndex(integral(CvColorSpaces::Display::Cb));
	}

	if (ui->actionChannelColor->isChecked())
	{
		processor->setChannelsColor(true);

		if (ui->actionMixHue->isChecked())
		{
			processor->setHueDisplayMode(integral(CvColorSpaces::HueDisplay::HUECOLOR));
		}
		else if (ui->actionMixHueSat->isChecked())
		{
			processor->setHueDisplayMode(integral(CvColorSpaces::HueDisplay::HUESATURATE));
		}
		else
		{
			processor->setHueDisplayMode(integral(CvColorSpaces::HueDisplay::HUEVALUE));
		}
	}
	else
	{
		processor->setChannelsColor(false);
		processor->setHueDisplayMode(integral(CvColorSpaces::HueDisplay::HUEGRAY));
	}
}

/*
 * Updates mean and std of process time
 * @param The updated process time (MeanValue<clock_t, double>)
 */
void MainWindow::on_processTimeupdated(const CvProcessor::ProcessTime * pt)
{
	meanTimeString.sprintf("%6.0f", pt->mean() / 1000.0);
	stdTimeString.sprintf("%5.0f", pt->std() / 1000.0);
	ui->labelProcessTimeValue->setText(meanTimeString);
	ui->labelProcessTimeStd->setText(stdTimeString);
}

/*
 * Menu action when Sources->camera 0 is selected
 * Sets capture to open device 0. If device is not available
 * menu item is set to inactive.
 */
void MainWindow::on_actionCamera_0_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	qDebug("Opening device 0 ...");
//	if (!capture->open(0, width, height))
//	{
//		qWarning("Unable to open device 0");
//		// disable menu item if camera 0 does not exist
//		ui->actionCamera_0->setDisabled(true);
//	}
	emit(deviceChanged(0, width, height));
}

/*
 * Menu action when Sources->camera 1 is selected
 * Sets capture to open device 0. If device is not available
 * menu item is set to inactive
 */
void MainWindow::on_actionCamera_1_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	qDebug("Opening device 1 ...");
//	if (!capture->open(1, width, height))
//	{
//		qWarning("Unable to open device 1");
//		// disable menu item if camera 1 does not exist
//		ui->actionCamera_1->setDisabled(true);
//	}

	emit deviceChanged(1, width, height);
}

/*
 * Menu action when Sources->file is selected.
 * Opens file dialog and tries to open selected file (is not empty),
 * then sets capture to open the selected file
 */
void MainWindow::on_actionFile_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	QString fileName = QFileDialog::getOpenFileName(
		this,
		tr("Open Video"),
		"./",
		tr("Video Files (*.avi *.m4v *.mkv *.mp4)"),
		NULL,
		QFileDialog::ReadOnly);

	// qDebug("Opening file %s ...", fileName.toStdString().c_str());

	if (fileName.length() > 0)
	{
//		if (!capture->open(fileName, width, height))
//		{
//			qWarning("Unable to open device file : %s",
//					 fileName.toStdString().c_str());
//		}

		emit fileChanged(fileName, width, height);
	}
	else
	{
		qWarning("empty file name");
	}
}

/*
 * Menu action to qui application
 */
void MainWindow::on_actionQuit_triggered()
{
	emit finished();
	this->close();
}

/*
 * Menu action when flip image is selected.
 * Sets capture to change flip status which leads to reverse
 * image horizontally
 */
void MainWindow::on_actionFlip_triggered()
{
	emit flipChanged(!capture->isFlipped());

	/*
	 * There is no need to update ui->checkBoxFlip since it is connected
	 * to ui->actionFlip through signals/slots
	 */
}

/*
 * Menu action when original image size is selected.
 * Sets capture not to resize image
 */
void MainWindow::on_actionOriginalSize_triggered()
{
	ui->actionConstrainedSize->setChecked(false);
	emit sizeChanged(QSize(0, 0));
}

/*
 * Menu action when constrained image size is selected.
 * Sets capture resize to preferred width and height
 */
void MainWindow::on_actionConstrainedSize_triggered()
{
	ui->actionOriginalSize->setChecked(false);
	emit sizeChanged(QSize(preferredWidth, preferredHeight));
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetImage instance.
 */
void MainWindow::on_actionRenderImage_triggered()
{
	setupImageWidget(RENDER_IMAGE);
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetLabel with pixmap instance.
 */
void MainWindow::on_actionRenderPixmap_triggered()
{
	setupImageWidget(RENDER_PIXMAP);
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetGL instance.
 */
void MainWindow::on_actionRenderOpenGL_triggered()
{
	setupImageWidget(RENDER_GL);
}

/*
 * Original size radioButton action.
 * Sets capture resize to off
 */
void MainWindow::on_radioButtonOrigSize_clicked()
{
	ui->actionConstrainedSize->setChecked(false);
	emit sizeChanged(QSize(0, 0));
}

/*
 * Custom size radioButton action.
 * Sets capture resize to preferred width and height
 */
void MainWindow::on_radioButtonCustomSize_clicked()
{
	ui->actionOriginalSize->setChecked(false);
	emit sizeChanged(QSize(preferredWidth, preferredHeight));
}

/*
 * Width spinbox value change.
 * Changes the preferred width and if custom size is selected apply
 * this custom width
 * @param value the desired width
 */
void MainWindow::on_spinBoxWidth_valueChanged(int value)
{
	preferredWidth = value;
	if (ui->radioButtonCustomSize->isChecked())
	{
		emit sizeChanged(QSize(preferredWidth, preferredHeight));
	}
}

/*
 * Height spinbox value change.
 * Changes the preferred height and if custom size is selected apply
 * this custom height
 * @param value the desired height
 */
void MainWindow::on_spinBoxHeight_valueChanged(int value)
{
	preferredHeight = value;
	if (ui->radioButtonCustomSize->isChecked())
	{
		emit sizeChanged(QSize(preferredWidth, preferredHeight));
	}
}

/*
 * Flip capture image horizontally.
 * changes capture flip status
 */
void MainWindow::on_checkBoxFlip_clicked()
{
	/*
	 * There is no need to update ui->actionFlip since it is connected
	 * to ui->checkBoxFlip through signals/slots
	 */
	emit flipChanged(ui->checkBoxFlip->isChecked());
}

/*
 * Deactivate "Hue mix" panel when "HSV: Hue" is not the selected
 * radiobutton
 */
void MainWindow::deactivateHueMixPanel()
{
	if (ui->hueBox != nullptr)
	{
		ui->hueBox->setEnabled(false);
	}
	if (ui->menuHue_Mix != nullptr)
	{
		ui->menuHue_Mix->setEnabled(false);
	}
}

/*
 * Activate "Hue mix" panel when "HSV: Hue" is the selected
 * radiobutton
 */
void MainWindow::activateHueMixPanel()
{
	if (ui->hueBox != nullptr)
	{
		ui->hueBox->setEnabled(true);
	}
	if (ui->menuHue_Mix != nullptr)
	{
		ui->menuHue_Mix->setEnabled(true);
	}
}

/*
 * Deactivate "Channel color" panel when the selected radio button
 * channel does not feature color option
 */
//void MainWindow::deactivateChannelColorPanel()
//{
//	if (ui->channelBox != nullptr)
//	{
//		ui->channelBox->setEnabled(false);
//	}
//	if (ui->menuChannel_Mode != nullptr)
//	{
//		ui->menuChannel_Mode->setEnabled(false);
//	}
//}

/**
 * Deactivate "Channel color" checkBox and/or action
 */
void MainWindow::deactivateChannelColor()
{
	if (ui->checkBoxChColor != nullptr)
	{
		ui->checkBoxChColor->setEnabled(false);
	}

	if (ui->actionChannelColor != nullptr)
	{
		ui->actionChannelColor->setEnabled(false);
	}
}


/*
 * Activate "Channel color" panel when the selected radio button
 * channel features color option
 */
//void MainWindow::activateChannelColorPanel()
//{
//	if (ui->channelBox != nullptr)
//	{
//		ui->channelBox->setEnabled(true);
//	}
//	if (ui->menuChannel_Mode != nullptr)
//	{
//		ui->menuChannel_Mode->setEnabled(true);
//	}
//}

/*
 * Activate "Channel color" checkBox and/or action
 */
void MainWindow::activateChannelColor()
{
	if (ui->checkBoxChColor != nullptr)
	{
		ui->checkBoxChColor->setEnabled(true);
	}

	if (ui->actionChannelColor != nullptr)
	{
		ui->actionChannelColor->setEnabled(true);
	}
}

/*
 * Re-emits the correct hue mix mode when switching to colored channels
 * mode
 * @param checked indicates this action should be performed or not
 */
void MainWindow::on_actionChannelColor_triggered(bool checked)
{
	channelColorsActivated(checked);
}

/*
 * Re-emits the correct hue mix mode when switching to colored channels
 * mode
 * @param checked indicates this action should be performed or not
 */
void MainWindow::channelColorsActivated(const bool checked)
{
	if (checked)
	{
		if (ui->radioButtonMixHue->isChecked() ||
			ui->actionDisplayHSV_Hue->isChecked())
		{
			emit hueDisplayModeChanged(integral(CvColorSpaces::HueDisplay::HUECOLOR));
		}
		else if (ui->radioButtonMixHueSat->isChecked() ||
				 ui->actionDisplayHSV_Saturation->isChecked())
		{
			emit hueDisplayModeChanged(integral(CvColorSpaces::HueDisplay::HUESATURATE));
		}
		else
		{
			emit hueDisplayModeChanged(integral(CvColorSpaces::HueDisplay::HUEVALUE));
		}
	}
}
