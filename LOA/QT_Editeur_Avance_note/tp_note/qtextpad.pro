#-------------------------------------------------
#
# Project created by QtCreator 2013-03-04T20:40:46
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qtextpad
TEMPLATE = app

SOURCES += main.cpp\
		mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

OTHER_FILES +=  tp2_2019.pdf

# Year-month-day Date
unix {
	DATE = $$system(date +%Y-%m-%d)
}
win32 {
	DATE = $$system("for /F \"usebackq tokens=1,2,3 delims=/ \" %a in \
	(`date /t`) do @echo %c-%b-%a")
}

# Archive format
ARCHIVER = zip
ARCHOPT =
ARCHEXT = zip

# Timestamped archive generation ----------------------------------------------
ARCHDIR = archives
archives.target = archives
archives.commands = mkdir $$ARCHDIR

archive.target = archive
archive.commands = $$ARCHIVER $$ARCHOPT $${ARCHDIR}/$${TARGET}-$${DATE}.$${ARCHEXT} \
$${SOURCES} $${HEADERS} $${FORMS} $${OTHER_FILES} $${TARGET}.pro
archive.depends = archives

# Extra targets to be added in the makefile -----------------------------------
QMAKE_EXTRA_TARGETS += archives archive
