/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[46];
    char stringdata0[839];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 11), // "sendMessage"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 7), // "message"
QT_MOC_LITERAL(4, 32, 7), // "timeout"
QT_MOC_LITERAL(5, 40, 11), // "sizeChanged"
QT_MOC_LITERAL(6, 52, 4), // "size"
QT_MOC_LITERAL(7, 57, 13), // "deviceChanged"
QT_MOC_LITERAL(8, 71, 8), // "deviceId"
QT_MOC_LITERAL(9, 80, 6), // "size_t"
QT_MOC_LITERAL(10, 87, 5), // "width"
QT_MOC_LITERAL(11, 93, 6), // "height"
QT_MOC_LITERAL(12, 100, 11), // "fileChanged"
QT_MOC_LITERAL(13, 112, 8), // "fileName"
QT_MOC_LITERAL(14, 121, 11), // "flipChanged"
QT_MOC_LITERAL(15, 133, 4), // "flip"
QT_MOC_LITERAL(16, 138, 11), // "grayChanged"
QT_MOC_LITERAL(17, 150, 4), // "gray"
QT_MOC_LITERAL(18, 155, 4), // "quit"
QT_MOC_LITERAL(19, 160, 20), // "setupProcessorFromUI"
QT_MOC_LITERAL(20, 181, 18), // "resetMeanFrameRate"
QT_MOC_LITERAL(21, 200, 26), // "onCaptureFrameRate_changed"
QT_MOC_LITERAL(22, 227, 5), // "value"
QT_MOC_LITERAL(23, 233, 24), // "onProcessingTime_changed"
QT_MOC_LITERAL(24, 258, 31), // "const CvProcessor::ProcessTime*"
QT_MOC_LITERAL(25, 290, 4), // "time"
QT_MOC_LITERAL(26, 295, 27), // "on_actionCamera_0_triggered"
QT_MOC_LITERAL(27, 323, 27), // "on_actionCamera_1_triggered"
QT_MOC_LITERAL(28, 351, 23), // "on_actionFile_triggered"
QT_MOC_LITERAL(29, 375, 23), // "on_actionQuit_triggered"
QT_MOC_LITERAL(30, 399, 23), // "on_actionFlip_triggered"
QT_MOC_LITERAL(31, 423, 23), // "on_actionGray_triggered"
QT_MOC_LITERAL(32, 447, 31), // "on_actionOriginalSize_triggered"
QT_MOC_LITERAL(33, 479, 34), // "on_actionConstrainedSize_trig..."
QT_MOC_LITERAL(34, 514, 30), // "on_actionRenderImage_triggered"
QT_MOC_LITERAL(35, 545, 31), // "on_actionRenderPixmap_triggered"
QT_MOC_LITERAL(36, 577, 31), // "on_actionRenderOpenGL_triggered"
QT_MOC_LITERAL(37, 609, 30), // "on_radioButtonOrigSize_clicked"
QT_MOC_LITERAL(38, 640, 32), // "on_radioButtonCustomSize_clicked"
QT_MOC_LITERAL(39, 673, 28), // "on_spinBoxWidth_valueChanged"
QT_MOC_LITERAL(40, 702, 29), // "on_spinBoxHeight_valueChanged"
QT_MOC_LITERAL(41, 732, 23), // "on_checkBoxFlip_clicked"
QT_MOC_LITERAL(42, 756, 23), // "on_checkBoxGray_clicked"
QT_MOC_LITERAL(43, 780, 21), // "on_checkBoxAR_clicked"
QT_MOC_LITERAL(44, 802, 7), // "checked"
QT_MOC_LITERAL(45, 810, 28) // "on_actionSwitchTab_triggered"

    },
    "MainWindow\0sendMessage\0\0message\0timeout\0"
    "sizeChanged\0size\0deviceChanged\0deviceId\0"
    "size_t\0width\0height\0fileChanged\0"
    "fileName\0flipChanged\0flip\0grayChanged\0"
    "gray\0quit\0setupProcessorFromUI\0"
    "resetMeanFrameRate\0onCaptureFrameRate_changed\0"
    "value\0onProcessingTime_changed\0"
    "const CvProcessor::ProcessTime*\0time\0"
    "on_actionCamera_0_triggered\0"
    "on_actionCamera_1_triggered\0"
    "on_actionFile_triggered\0on_actionQuit_triggered\0"
    "on_actionFlip_triggered\0on_actionGray_triggered\0"
    "on_actionOriginalSize_triggered\0"
    "on_actionConstrainedSize_triggered\0"
    "on_actionRenderImage_triggered\0"
    "on_actionRenderPixmap_triggered\0"
    "on_actionRenderOpenGL_triggered\0"
    "on_radioButtonOrigSize_clicked\0"
    "on_radioButtonCustomSize_clicked\0"
    "on_spinBoxWidth_valueChanged\0"
    "on_spinBoxHeight_valueChanged\0"
    "on_checkBoxFlip_clicked\0on_checkBoxGray_clicked\0"
    "on_checkBoxAR_clicked\0checked\0"
    "on_actionSwitchTab_triggered"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      31,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       8,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  169,    2, 0x06 /* Public */,
       1,    1,  174,    2, 0x26 /* Public | MethodCloned */,
       5,    1,  177,    2, 0x06 /* Public */,
       7,    3,  180,    2, 0x06 /* Public */,
      12,    3,  187,    2, 0x06 /* Public */,
      14,    1,  194,    2, 0x06 /* Public */,
      16,    1,  197,    2, 0x06 /* Public */,
      18,    0,  200,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      19,    0,  201,    2, 0x08 /* Private */,
      20,    0,  202,    2, 0x08 /* Private */,
      21,    1,  203,    2, 0x08 /* Private */,
      23,    1,  206,    2, 0x08 /* Private */,
      26,    0,  209,    2, 0x08 /* Private */,
      27,    0,  210,    2, 0x08 /* Private */,
      28,    0,  211,    2, 0x08 /* Private */,
      29,    0,  212,    2, 0x08 /* Private */,
      30,    0,  213,    2, 0x08 /* Private */,
      31,    0,  214,    2, 0x08 /* Private */,
      32,    0,  215,    2, 0x08 /* Private */,
      33,    0,  216,    2, 0x08 /* Private */,
      34,    0,  217,    2, 0x08 /* Private */,
      35,    0,  218,    2, 0x08 /* Private */,
      36,    0,  219,    2, 0x08 /* Private */,
      37,    0,  220,    2, 0x08 /* Private */,
      38,    0,  221,    2, 0x08 /* Private */,
      39,    1,  222,    2, 0x08 /* Private */,
      40,    1,  225,    2, 0x08 /* Private */,
      41,    0,  228,    2, 0x08 /* Private */,
      42,    0,  229,    2, 0x08 /* Private */,
      43,    1,  230,    2, 0x08 /* Private */,
      45,    0,  233,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::QString, QMetaType::Int,    3,    4,
    QMetaType::Void, QMetaType::QString,    3,
    QMetaType::Void, QMetaType::QSize,    6,
    QMetaType::Void, QMetaType::Int, 0x80000000 | 9, 0x80000000 | 9,    8,   10,   11,
    QMetaType::Void, QMetaType::QString, 0x80000000 | 9, 0x80000000 | 9,   13,   10,   11,
    QMetaType::Void, QMetaType::Bool,   15,
    QMetaType::Void, QMetaType::Bool,   17,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   22,
    QMetaType::Void, 0x80000000 | 24,   25,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   22,
    QMetaType::Void, QMetaType::Int,   22,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   44,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendMessage((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->sendMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->sizeChanged((*reinterpret_cast< const QSize(*)>(_a[1]))); break;
        case 3: _t->deviceChanged((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3]))); break;
        case 4: _t->fileChanged((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3]))); break;
        case 5: _t->flipChanged((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 6: _t->grayChanged((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 7: _t->quit(); break;
        case 8: _t->setupProcessorFromUI(); break;
        case 9: _t->resetMeanFrameRate(); break;
        case 10: _t->onCaptureFrameRate_changed((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 11: _t->onProcessingTime_changed((*reinterpret_cast< const CvProcessor::ProcessTime*(*)>(_a[1]))); break;
        case 12: _t->on_actionCamera_0_triggered(); break;
        case 13: _t->on_actionCamera_1_triggered(); break;
        case 14: _t->on_actionFile_triggered(); break;
        case 15: _t->on_actionQuit_triggered(); break;
        case 16: _t->on_actionFlip_triggered(); break;
        case 17: _t->on_actionGray_triggered(); break;
        case 18: _t->on_actionOriginalSize_triggered(); break;
        case 19: _t->on_actionConstrainedSize_triggered(); break;
        case 20: _t->on_actionRenderImage_triggered(); break;
        case 21: _t->on_actionRenderPixmap_triggered(); break;
        case 22: _t->on_actionRenderOpenGL_triggered(); break;
        case 23: _t->on_radioButtonOrigSize_clicked(); break;
        case 24: _t->on_radioButtonCustomSize_clicked(); break;
        case 25: _t->on_spinBoxWidth_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 26: _t->on_spinBoxHeight_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 27: _t->on_checkBoxFlip_clicked(); break;
        case 28: _t->on_checkBoxGray_clicked(); break;
        case 29: _t->on_checkBoxAR_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 30: _t->on_actionSwitchTab_triggered(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (MainWindow::*_t)(const QString & , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::sendMessage)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const QSize & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::sizeChanged)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const int , const size_t , const size_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::deviceChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const QString & , const size_t , const size_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::fileChanged)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::flipChanged)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)(const bool );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::grayChanged)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MainWindow::quit)) {
                *result = 7;
                return;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 31)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 31;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 31)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 31;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::sendMessage(const QString & _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 2
void MainWindow::sizeChanged(const QSize & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MainWindow::deviceChanged(const int _t1, const size_t _t2, const size_t _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MainWindow::fileChanged(const QString & _t1, const size_t _t2, const size_t _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)), const_cast<void*>(reinterpret_cast<const void*>(&_t3)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MainWindow::flipChanged(const bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void MainWindow::grayChanged(const bool _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void MainWindow::quit()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
