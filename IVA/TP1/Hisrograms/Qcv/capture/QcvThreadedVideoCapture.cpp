#include <QDebug>	// for qWarning & qDebug
#include <QTime>	// for time debug purpose
#include <QFileInfo>
#include <QDir>

#include <cmath>	// for basic math function (conversions)

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>

#include "QcvThreadedVideoCapture.h"

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * Constructor for live video from deviceId
 * @param deviceId the ID of the camera to open
 * @param flipVideo indicates if the video should be flipped horizontally
 * @param gray indicates if image should be converted to gray
 * @param width desired width (or 0 to keep original capture image width)
 * @param height desired height (or 0 to keep original capture image height)
 * @param parent the parent QObject
 */
QcvThreadedVideoCapture::QcvThreadedVideoCapture(const int deviceId,
												 const bool flipVideo,
												 const bool gray,
												 const size_t width,
												 const size_t height,
												 QObject * parent) :
	QcvVideoCapture(deviceId,
					flipVideo,
					gray,
					width,
					height,
					parent),
	stillImagesCapture(false),
	imagesDirectory(nullptr),
	imageFiles(nullptr),
	worker(nullptr),
	restarted(false)
{
	setupAndLaunch(width, height);
}

/*
 * Constructor from file name
 * @param fileName video file to open
 * @param flipVideo mirror image
 * @param gray convert image to gray
 * @param skip indicates capture can skip an image. When the capture
 * result has not been processed yet, or when false that capture should
 * wait for the result to be processed before grabbing a new image.
 * This only applies when #updateThread is not NULL.
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @param parent the parent QObject
 */
QcvThreadedVideoCapture::QcvThreadedVideoCapture(const QString & fileName,
												 const bool flipVideo,
												 const bool gray,
												 const size_t  width,
												 const size_t height,
												 QObject * parent) :
	QcvVideoCapture(fileName,
					flipVideo,
					gray,
					width,
					height,
					parent),
	stillImagesCapture(false),
	imagesDirectory(nullptr),
	imageFiles(nullptr),
	worker(nullptr), // synchronized with delaut delay
	restarted(false)
	// statusMessage is default constructed
{
	/*
	 * Warning, by the end of this constructor capture should have captured
	 * at least one image so this image can be used to configure processors
	 * using this image without having to wait for the worker to launch
	 * and retrieve a first image
	 */
	setupAndLaunch(width, height);
}

/*
 * Constructor from dirName or numbered file name for reading
 * numbered image files
 * @param name directory name or numbered file name
 * @param frameRate frame rate at which images should be read
 * @param flipVideo mirror image
 * @param gray convert image to gray
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @param parent the parent QObject
 * @throw std::system_error when name does not correspond to an existing
 * file or a directory
 */
QcvThreadedVideoCapture::QcvThreadedVideoCapture(const QString & name,
												 const double frameRate,
												 const bool flipVideo,
												 const bool gray,
												 const size_t width,
												 const size_t height,
												 QObject * parent)
	throw (std::system_error):
	QcvVideoCapture(flipVideo,
					gray,
					width,
					height,
					parent),
	stillImagesCapture(true),
	imagesDirectory(nullptr), // set up in setupStillImages
	imageFiles(nullptr), // set up in setupStillImages
	worker(nullptr), // synchronized with delaut delay
	restarted(false)
	// statusMessage is default constructed
{
	this->frameRate = frameRate;

	std::pair<QString *, QStringList *> stillStrings = setupStillImages(name);

	switchToNewStillCapture(stillStrings.first, stillStrings.second);
	/*
	 * Warning, by the end of this constructor capture should have captured
	 * at least one image so this image can be used to configure processors
	 * using this image without having to wait for the worker to launch
	 * and retrieve a first image
	 */
	setupAndLaunch(width, height);
}

/*
 * Destructor.
 * releases video capture and image(s)
 */
QcvThreadedVideoCapture::~QcvThreadedVideoCapture()
{
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "will lock";
	mutex.lock();
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "locked";
	if (!stillImagesCapture)
	{
		capture->release();
	}
	else
	{
		imageFilesIterator = imageFilesEnd;
		imageFiles->clear();
	}

	mutex.unlock();
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "unlocked";

	// Waits for its worker to finish
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << "waiting for worker thread";
	if (worker != nullptr)
	{
		worker->wait();
		delete worker;
	}
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << "finished waiting for worker thread";
	// Since worker is child of this the QObject destructor
	// should destroy its children
}

/*
 * Indicates if capture works in synchronized mode where
 * frame rate is imposed.
 * @return true is capture works in synchronized mode, false
 * otherwise
 */
bool QcvThreadedVideoCapture::isSynchronized() const
{
	if (worker != nullptr)
	{
		return worker->isSynchronized();
	}
	else
	{
		return false;
	}
}

/*
 * Gets the contrained frame rate at which capture should operate
 * when synchronized mode is on
 * @return the constrained frame rate
 */
double QcvThreadedVideoCapture::getContrainedFrameRate() const
{
	if (worker != nullptr)
	{
		int delay = worker->getDelay();
		return(1000.0 / static_cast<double>(delay));
	}
	else
	{
		return 0.0;
	}
}

// ----------------------------------------------------------------------------
// Public Slots
// ----------------------------------------------------------------------------

/*
 * Open new device Id
 * @param deviceId device number to open
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @return true if device has been opened and checked
 */
bool QcvThreadedVideoCapture::openDevice(const int deviceId,
										 const size_t width,
										 const size_t height)
{
	bool requestedLive = true;
	bool requestedStillImagesCapture = false;

	/*
	 * Try to create a new capture (if possible)
	 */
	cv::VideoCapture * localCapture = new cv::VideoCapture(deviceId);

	if (localCapture->isOpened())
	{
		/*
		 * If the new capture is sucessful then
		 *	- shutdown worker
		 *	- copy new capture to old one
		 *	- relaunch worker
		 */
		// switchToNewCapture(localCapture, false);
		switchToNewVideoCapture(localCapture);

		live = requestedLive;
		stillImagesCapture = requestedStillImagesCapture;

		setupAndLaunch(width, height);

		emitImageChanged();

		return true;
	}
	else
	{
		delete localCapture;
		qWarning() << "Unable to create new capture on device" << deviceId;
	}

	return false;
}

/*
 * Open new video file
 * @param fileName video file to open
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @return true if video has been opened and timer launched
 */
bool QcvThreadedVideoCapture::openFile(const QString & fileName,
									   const size_t width,
									   const size_t height)
{
	bool requestedLive = false;
	double requestedFrameRate;
	bool requestedStillImagesCapture = false;

	/*
	 * Create new local Capture
	 */
	cv::VideoCapture * localCapture = new cv::VideoCapture(fileName.toStdString());

	/*
	 * Video for Linux (V4L) does not support capture get/set properties
	 */
#ifndef Q_OS_LINUX
	double fileRate = localCapture->get(CAP_PROP_FPS);
	if (fileRate != 0.0)
	{
		requestedFrameRate = fileRate;
	}
	else
	{
		qWarning() << "Unable to get framerate from file" << fileName;
		requestedFrameRate = defaultFrameRate;
	}
#else
	requestedFrameRate = defaultFrameRate;
#endif

	if (localCapture->isOpened())
	{
		/*
		 * If the new capture is sucessful then
		 *	- shutdown worker
		 *	- copy new capture to old one
		 *	- relaunch worker
		 */
		// switchToNewCapture(localCapture, !requestedLive, requestedFrameRate);
		switchToNewVideoCapture(localCapture);

		live = requestedLive;
		frameRate = requestedFrameRate;
		stillImagesCapture = requestedStillImagesCapture;

		setupAndLaunch(width, height);

		emitImageChanged();

		return true;
	}
	else
	{
		delete localCapture;
		qWarning() << "Unable to create new capture on file" << fileName;
	}

	return false;
}

/*
 * Open Directory containing files to read (does nothing in this
 * implementation)
 * @param dirName directory to open
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @return always false
 */
bool QcvThreadedVideoCapture::openDirectory(const QString & dirName,
											const size_t width,
											const size_t height)
{
	bool requestedLive = false;
	bool requestedStillImagesCapture = true;

	/*
	 * Create new local Capture
	 */
	std::pair<QString *, QStringList *> stillStrings = setupStillImages(dirName);

	if (!stillStrings.first->isEmpty() && !stillStrings.second->isEmpty())
	{
		/*
		 * If the new capture is sucessful then
		 *	- shutdown worker
		 *	- copy new capture to old one
		 *	- relaunch worker
		 */
		switchToNewStillCapture(stillStrings.first, stillStrings.second);

		live = requestedLive;
		stillImagesCapture = requestedStillImagesCapture;

		setupAndLaunch(width, height);

		emitImageChanged();

		return true;
	}
	else
	{
		delete imagesDirectory;
		delete imageFiles;
		qWarning() << "Unable to create new still capture on directory" << dirName;
	}

	return false;
}

/*
 * Sets the "flipped" status of captured video
 * @param value the new "flipped" status
 */
//void QcvThreadedVideoCapture::setFlipped(const bool value)
//{
//	mutex.lock();

//	flipped = value;

//	mutex.unlock();
//}

/*
 * Sets the gray conversion status of captured video
 * @param value the new value of gray conversion status
 * @post The #imageChanged(cv::Mat *); signal have been emitted
 */
void QcvThreadedVideoCapture::setGray(const bool value)
{
	if (value != gray)
	{
		mutex.lock();
//		qDebug() << "+ QcvThreadedVideoCapture::setGray start"
//				 << QTime::currentTime().msecsSinceStartOfDay();

		gray = value;
		if (gray)
		{
			imageDisplay = cv::Mat(imageFlipped.size(), CV_8UC1);
		}
		else
		{
			imageDisplay = imageFlipped;
		}

		emit imageChanged(&imageDisplay);

		waitForUpdate();

//		qDebug() << "+ QcvThreadedVideoCapture::setGray end"
//				 << QTime::currentTime().msecsSinceStartOfDay();
		mutex.unlock();
	}
}

/*
 * Sets capture to synchronized mode (when true) where frame rate is
 * contrained (whenever possible). Otherwise frame rate is not
 * constrained and capture may proceed as fast as possible.
 * @param value the new value of synchronized mode
 * @post The #synchronized(bool); signal has been emitted
 */
void QcvThreadedVideoCapture::setSynchronized(const bool value)
{
	if (worker != nullptr)
	{
		if (worker->isSynchronized() != value)
		{
			/*
			 * worker's setSynchronized is using mutex's lock/unlock
			 */
			worker->setSynchronized(value);
			emit synchronized(value);
		}
	}
}


/*
 * Releases capture and emits #finished() signal
 */
void QcvThreadedVideoCapture::finish()
{
	mutex.lock();

	if (!stillImagesCapture)
	{
		if (capture != nullptr)
		{
			if (capture->isOpened())
			{
				capture->release();
			}
		}
	}
	else // stillImagesCapture
	{
		if (imageFiles != nullptr && !imageFiles->isEmpty())
		{
			imageFilesIterator = imageFilesEnd;
			imageFiles->clear();
		}
		if (imagesDirectory != nullptr)
		{
			imagesDirectory->clear();
		}
	}

	emit finished();

	mutex.unlock();
}

/*
 * Sets the desired frame rate on capture ONLY when capture is in
 * synchronized mode
 *	- If the new frame rate is 0.0 then use natural capture frame rate.
 *	- If the new frame rate is too high for capture then capture will
 *	keeps its highest possible frame rate according to the device in use
 * @param frameRate the new frame rate to set
 * @post The #constrainedFrameRateChanged(double) signal have been emitted
 */
void QcvThreadedVideoCapture::setFrameRate(const double frameRate)
{
	this->frameRate = frameRate;
	if (worker != nullptr)
	{
		int delay = static_cast<int>(round(1000.0 / fabs(frameRate)));
		if (worker->getDelay() != delay)
		{
//			qDebug() << "QcvThreadedVideoCapture::setFrameRate(" << frameRate
//					 << ") : setting up delay to" << delay;
			/*
			 * worker's setDelay is using mutex's lock/unlock
			 */
			worker->setDelay(delay);
			emit constrainedFrameRateChanged(frameRate);
		}
	}
}

// ----------------------------------------------------------------------------
// Protected Methods
// ----------------------------------------------------------------------------

/*
 * Sets the desired size of captured image
 * @param width the desired width of captured image
 * @param height the desired height of captured image
 * @post The #messageChanged(QString); signal is emitted with new size
 * values
 * @see #size
 */
void QcvThreadedVideoCapture::setSize_Impl(const size_t width,
										   const size_t height)
{
	int desiredWidth = static_cast<int>(width);
	int desiredHeight = static_cast<int>(height);

	if ((size.width() != desiredWidth) ||
		(size.height() != desiredHeight)) // size is changing
	{
		mutex.lock();

		if (!imageResized.empty())
		{
			imageResized.release();
		}

		if (desiredWidth == 0 && desiredHeight == 0) // reset to original size
		{
			desiredWidth = originalSize.width();
			desiredHeight = originalSize.height();
		}

		directResize = setDirectSize(desiredWidth, desiredHeight);

		if (directResize)
		{
			imageResized = image;
		}
		else
		{
			imageResized = cv::Mat(desiredHeight, desiredWidth, image.type());
		}

		size.setWidth(imageResized.cols);
		size.setHeight(imageResized.rows);

		mutex.unlock();

		statusMessage.clear();
		statusMessage.sprintf("Size set to %dx%d", size.width(), size.height());
		emit messageChanged(statusMessage, messageDelay);

		resized = size != originalSize;
	}
}

/*
 * Release capture by either:
 *	- releasing #capture or
 *	- setting #imageFilesIterator to the end and clearing #imageFiles
 * These opeation are performed within a critical section locked with
 * #mutex
 * Then wait for #worker to terminate it's run method.
 */
void QcvThreadedVideoCapture::releaseCapture()
{
	mutex.lock();

	if (!stillImagesCapture)
	{
		if (capture != nullptr)
		{
			/*
			 * Capture release leads to empty images which will lead to #grab()
			 * returning false value then update will return false value as well
			 * which will lead to terminate the current worker's run
			 */
			capture->release();
		}
	}
	else
	{
		if (imageFiles != nullptr)
		{
			imageFilesIterator = imageFilesEnd;
			imageFiles->clear();
		}
	}

	mutex.unlock();

	// Wait for capture worker to terminate after capture release
	if (worker != nullptr)
	{
		worker->wait();
	}
}

/*
 * Release old capture in order to switch to a new capture.
 *	- Old capture is released
 *	- Worker gets terminated and destroyed
 *	- old capture is replaced by new one
 * @param newCapture the new capture to switch on
 * @param synchronized mode for the new capture
 * @param frameRate if synchronized mode is used the frame rate for
 * the synchronized mode
 * @pre new capture needs to be non null an opened
 * @pre Capture attributes such as #stillImagesCapture,
 * QcvVideoCapture::live and QcvCapture::frameRate haven't been changed
 * yet to new values
 * @post new still needs to be setup and a neww worker reccreated and
 * launched with #setupAndLaunch();
 */
void QcvThreadedVideoCapture::switchToNewVideoCapture(cv::VideoCapture * newCapture)
//												 const bool synchronized,
//												 const double frameRate)
{
	releaseCapture();

	if (!stillImagesCapture)
	{
		delete capture;
	}
	else
	{
		delete imagesDirectory;
		imagesDirectory = nullptr;
		delete imageFiles;
		imageFiles = nullptr;
	}

	capture = newCapture;
}

/*
 * Release old capture elements in order to switch to a new video capture.
 *	- Old capture is cleaned up
 *	- Worker gets terminated and destroyed
 *	- old capture is replaced by new one
 * @param imagesDirectory the new image directory for image files
 * @param imageFiles the new list of image files
 * @pre provided imagesDirectory and imageFiles need to be non null
 * and properly initialized
 * @pre Capture attributes such as #stillImagesCapture,
 * QcvVideoCapture::live and QcvCapture::frameRate haven't been changed
 * yet to new values
 * @post new still needs to be setup and a new worker recreated and
 * launched with #setupAndLaunch();
 */
void QcvThreadedVideoCapture::switchToNewStillCapture(QString * imagesDirectory,
													  QStringList * imageFiles)
{
	releaseCapture();

	if (!stillImagesCapture)
	{
		delete capture;
		capture = nullptr;
	}
	else
	{
		delete this->imagesDirectory;
		delete this->imageFiles;
	}

	this->imagesDirectory = imagesDirectory;
	this->imageFiles = imageFiles;
	imageFilesIterator = imageFiles->cbegin();
	imageFilesEnd = imageFiles->cend();
}

/*
 * Setup Still Image capture with provided directory or file name
 * @param name directory or numbered file name
 * @return a new pair of <QString *, QStringList *> which can be applied
 * to #imagesDirectory and #imageFiles
 * @throw std::system_error when name does not correspond to an existing
 * file or a directory
 */
std::pair<QString *, QStringList *> QcvThreadedVideoCapture::setupStillImages(
	const QString & name) throw(std::system_error)
{
	QFileInfo checkName(name);
	if (checkName.exists())
	{
		QString targetFile;
		QDir filesDirectory;
		if (checkName.isFile())
		{
			filesDirectory = checkName.dir();
			targetFile = checkName.baseName();
			if (!filesPattern.exactMatch(targetFile))
			{
				QString message;
				message += Q_FUNC_INFO;
				message += " Invalid target File ";
				message += targetFile;
				message += " does not match pattern ";
				message += filesPattern.pattern();
				qWarning() << message;
				throw std::system_error(EINVAL,
										std::generic_category(),
										message.toStdString());
			}
		}
		else if (checkName.isDir())
		{
			filesDirectory = QDir(name);
		}
		else
		{
			QString message;
			message += Q_FUNC_INFO;
			message += " ";
			message += name;
			message += " is neither file or directory";
			qWarning() << message;
			throw std::system_error(EINVAL,
									std::generic_category(),
									message.toStdString());
		}

		QString newImagesDirectory = filesDirectory.absolutePath();

		filesDirectory.setNameFilters(filesExtensions);
		filesDirectory.setSorting(QDir::Name);
		filesDirectory.setFilter(QDir::Files);

		/*
		 * Get all entries in the directory corresponding to the filesPattern
		 * (evt starting at tartgetFile if it is non empty)
		 */
//		QStringList entries = filesDirectory.entryList(QStringList(filesPattern.pattern()),
//													   QDir::Files,
//													   QDir::Name);
		QStringList entries = filesDirectory.entryList(filesExtensions,
													   QDir::Files,
													   QDir::Name);
		QStringList::const_iterator it = entries.cbegin();

		if (!targetFile.isEmpty())
		{
			if (targetFile != *it)
			{
				++it;
			}
		}

		QStringList * newImagesFiles = new QStringList();

		// "it" points to targetFile or to the first file in directory
		while (it != entries.cend())
		{
			QString entryName = *it;
			if (filesPattern.exactMatch(entryName))
			{
				newImagesFiles->append(entryName);
			}
			++it;
		}

		return std::pair<QString *, QStringList *>(new QString(newImagesDirectory),
												   newImagesFiles);
	}
	else
	{
		QString message;
		message += Q_FUNC_INFO;
		message += " provided Dir or File ";
		message += name;
		message += " does not exist";
		qWarning() << message;
		throw std::system_error(ENOENT,
								std::generic_category(),
								message.toStdString());
	}

	return std::pair<QString *, QStringList *>(nullptr, nullptr);
}

/*
 * Wait for at least one call to update method in the worker thread:
 *	- blocks all signals (so other classes are NOT notified of update)
 *	- Get an update condition from the worker
 *	- Wait on this condition using update condition's wait(&mutex) method
 *	- unblocks all signals (so other classes can be notified of update)
 * @pre mutex should be locked prior to calling this method
 */
void QcvThreadedVideoCapture::waitForUpdate()
{
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO
//			 << "waiting first update start";

	blockSignals(true); // prevent an eventual update signal

//	qDebug() << QThread::currentThreadId()
//		 << Q_FUNC_INFO
//		 << "waiting for condition";

	if (worker != nullptr)
	{
		worker->getUpdateCondition().wait(&mutex);
	}

//	qDebug() << QThread::currentThreadId()
//		 << Q_FUNC_INFO
//		 << "waiting for condition terminated";

	blockSignals(false);
}


/*
 * After capture has been created,
 *	- grab a first image to setup sizes
 *	- creates and connect the worker
 *	- launch the worker
 * @pre capture is not null and opened
 */
void QcvThreadedVideoCapture::setupAndLaunch(const size_t width,
											 const size_t height)
{
	// Wait for current worker to finish
	if (worker != nullptr)
	{
		/*
		 * wait here might be unnecessary since we already
		 * waited on worker to finish within switch to new capture
		 */
		worker->wait();
		disconnect(this, SIGNAL(finished()),
				   worker, SLOT(quit()));
		delete worker;
	}

	// Get first image to setup sizes
	if (stillImagesCapture)
	{
		assert(imageFiles != nullptr);
		assert(imagesDirectory != nullptr);
		assert(!imageFiles->isEmpty());
		assert(!imagesDirectory->isEmpty());

		imageFilesIterator = imageFiles->cbegin();
		imageFilesEnd = imageFiles->cend();
		QString imageFileName = *imagesDirectory + "/" + *imageFilesIterator;
		image = cv::imread(imageFileName.toStdString());
	}
	else // Video capture
	{
		assert(capture != nullptr);
		assert(capture->isOpened());

		(*capture) >> image;
	}

	originalSize.setWidth(image.cols);
	originalSize.setHeight(image.rows);
	size = originalSize; // reset size before setSize_Impl(...)

	// set up resized, directResize, size
	setSize_Impl(width, height);

	// Cleanup unused capture elements
	if (stillImagesCapture)
	{
		if (capture != nullptr)
		{
			capture->release();
			delete capture;
		}
	}
	else
	{
		if (imagesDirectory != nullptr)
		{
			delete imagesDirectory;
			imagesDirectory = nullptr;
		}
		if (imageFiles != nullptr)
		{
			delete imageFiles;
			imageFiles = nullptr;
		}
	}

	// Launch new worker
	worker = new QcvCaptureWorker(this);
	connect(this, SIGNAL(finished()),
			worker, SLOT(quit()));

	setSynchronized(!live);
	if (!live)
	{
		setFrameRate((frameRate == 0.0 ? defaultFrameRate : frameRate));
	}

	worker->start();

	mutex.lock();
	// Wait for worker's first update
	waitForUpdate();
	mutex.unlock();
}

/*
 * Update capture (with proper locking/unlocking):
 *	- Grabs a new image
 *	- if gray is on then convert
 *	- if resize is on then resize image into imageResized
 *	(otherwise set imageResized to image)
 *	- if flipped is on then flip imageResized into imageFlipped
 *	(otherwise set imageFlipped to imageResized)
 *	- if gray is on then converts imageFlipped into imageDisplay
 *	(otherwise set imageDisplay to imageFlipped)
 * @return true if new image has been grabbed from capture or false
 * when there is no more images to grab from video capture
 * @post When no image has been grabbed resizing, flipping and
 * converting to gray are not performed
 * @post If a new image is available #updated(); signal has been emitted
 */
bool QcvThreadedVideoCapture::update()
{
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "will lock";
	mutex.lock();
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "locked";

	bool updated = grab();

	if (updated)
	{
		performResize();
		performFlip();
		performConvert();
		emitUpdated();
	}

	mutex.unlock();
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "unlocked";

#ifdef Q_OS_LINUX
	/*
	 * FIXME : Artificial 10 μs sleep to ensure proper thread scheduling on
	 * Linux systems
	 */
	QThread::currentThread()->usleep(10);
#endif

	return updated;
}

/*
 * Grabs new image from capture
 * @return true if new image has been grabbed from capture or false
 * when there is no more images to grab from video capture
 * @note This method eventually deals with automatically restarting
 * the capture when video file playback has reached the end of the file
 * @post #messageChanged(); signal has been emitted if either:
 *	- There is no more frames to capture (then capture is released)
 *	- Capture has been restarted to loop video files
 */
bool QcvThreadedVideoCapture::grab()
{
	if (!stillImagesCapture)
	{
		(*capture) >> image;
	}
	else
	{
		if (imageFilesIterator == imageFilesEnd)
		{
			image.release(); // deallocates image.data
		}
		else
		{
			QString imageFileName = *imagesDirectory + "/" + *imageFilesIterator;
//			qDebug() << Q_FUNC_INFO << "Reading image" << *imageFilesIterator;
			image = cv::imread(imageFileName.toStdString());
			++imageFilesIterator;
		}
	}

	if (!image.data)
	{
		/*
		 * Captured image has no data
		 */
		statusMessage.clear();

		if (live)
		{
			capture->release();
			statusMessage.sprintf("%s: No more frames to capture ...", Q_FUNC_INFO);
			emit messageChanged(statusMessage, 0);
			qDebug("%s", statusMessage.toStdString().c_str());
		}
		else
		{
			if (!stillImagesCapture)
			{
				if (!restarted)
				{
					// if video is playing from file, we'll try to restart it
					if (capture->set(CAP_PROP_POS_FRAMES, 0.0))
					{
						statusMessage.sprintf("Video Capture restarted");
						emit messageChanged(statusMessage,
											QcvVideoCapture::messageDelay);
						qDebug("%s", statusMessage.toStdString().c_str());

						// Then update again
						restarted = true;
						return grab();
					}
				}
				capture->release();
			}
			else // stillImagesCapture
			{
				if (!restarted && !imageFiles->isEmpty())
				{
					imageFilesIterator = imageFiles->cbegin();
					statusMessage.sprintf("Still Images Capture restarted");
					emit messageChanged(statusMessage,
										QcvVideoCapture::messageDelay);
					qDebug("%s", statusMessage.toStdString().c_str());
					restarted = true;
					return grab();
				}
			}
		}
		return false;
	}
	else
	{
		restarted = false;
		/*
		 * Captured image has data
		 */
		return true;
	}
}

/*
 * If resizing is needed, resize the image
 * @return true if actual resizing has been performed, false if
 * #imageResized has only been copied from #image
 * @warning This method does not perform locking/unlocking so it is not
 * safe to use it without proper mutex's operations
 */
bool QcvThreadedVideoCapture::performResize()
{
	if (resized && !directResize)
	{
		cv::resize(image,
				   imageResized,
				   imageResized.size(),
				   0,
				   0,
				   cv::INTER_AREA);
		return true;
	}
	else
	{
		imageResized = image;
		return false;
	}
}

/*
 * if flipping is needed, flip the image
 * @return true if actual flip has been performed and false if
 * #imageFlipped has only been copied from #imageResized
 * @warning This method does not perform locking/unlocking so it not
 * safe to use it without proper mutex's operations
 */
bool QcvThreadedVideoCapture::performFlip()
{
	if (flipped)
	{
		cv::flip(imageResized, imageFlipped, 1);
		return true;
	}
	else
	{
		imageFlipped = imageResized;
		return false;
	}
}

/*
 * if gray conversion is needed convert to gray image
 * @warning This method does not perform locking/unlocking so it is not
 * safe to use it without proper mutex's operations
 */
bool QcvThreadedVideoCapture::performConvert()
{
	if (gray)
	{
		cv::cvtColor(imageFlipped, imageDisplay, CV_BGR2GRAY);
		return true;
	}
	else
	{
		imageDisplay = imageFlipped;
		return false;
	}
}
