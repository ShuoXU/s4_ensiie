/*
 * QCvProcessor.cpp
 *
 *  Created on: 19 févr. 2012
 *	  Author: davidroussel
 */

#include <functional>	// for std::bind
#include <algorithm>	// for std::min

#include <QRegExpValidator>
#include <QMetaType>	// for registering a meta type in signal/slot
#include <QDebug>		// for qDebug() << ...
#include <QTime>		// for time in debug messages
#include <QtGlobal>		// for Q_ASSERT_X

#include <Qcv/QcvProcessor.h>

/*
 * Proto instantiation of CvProcessor template method
 * Stream & CvProcessor::toStream_Impl<Stream>(Stream &) const with concrete
 * type Qdebug
 */
template LIB_EXPORT QDebug & CvProcessor::toStream_Impl<QDebug>(QDebug &) const;

// ----------------------------------------------------------------------------
// Protected static attributes
// ----------------------------------------------------------------------------

/*
 * Default timeout to show messages
 */
int QcvProcessor::defaultTimeOut = 5000;

/*
 * Number format used to format numbers into QStrings
 */
QString QcvProcessor::numberFormat = QString::fromUtf8("%7.0f");

/*
 * The regular expression used to validate new number formats
 * @see #setNumberFormat
 */
QRegExp QcvProcessor::numberRegExp("%[+- 0#]*[0-9]*([.][0-9]+)?[efEF]");

/*
 * format used to format Mean/Std time values : <mean> ± <std>
 */
QString QcvProcessor::meanStdFormat = numberFormat + QString::fromUtf8(" ± %5.0f");

/*
 * format used to format Min/Max time values : <min> / <max>
 */
QString QcvProcessor::minMaxFormat = numberFormat +  QString::fromUtf8(" / ") +
									 numberFormat;

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * QcvProcessor constructor
 * @param image the source image
 * @param imageLock the mutex for concurrent access to the source image.
 * In order to avoid concurrent access to the same image
 * @param updateThread the thread in which this processor should run
 * @param parent parent QObject
 * @param number of locks in #locks [default is 2 : SOURCE & RESULT]
 * @param settings the settings used to set initial an value and/or
 * save a current value to settings
 */
QcvProcessor::QcvProcessor(Mat * image,
						   QMutex * imageLock,
						   QThread * updateThread,
						   QObject * parent,
						   const int nbLocks,
						   QSettings * settings) :
	CvProcessor(image),	// <-- virtual base class constructor first
	QObject(parent),
	nbLocks(std::max(static_cast<int>(TERMINATE +  1u), nbLocks)), // at least 3
	locks(new QMutex *[(nbLocks >= 0 ? nbLocks : 0)]),
	updateThread(updateThread),
	message(),
	processTimeString(),
	timePerFeatureController(&timePerFeature,
							 nullptr, // no lock,
							 new function<void(const bool)>(std::bind(&QcvProcessor::setTimePerFeature,
																	  this,
																	  std::placeholders::_1)),
							 (updateThread == nullptr ? this : nullptr),
							 settings,
							 "process/time_unit"),
	settings(settings)
{
	// Check that we have at least 2 locks
	Q_ASSERT_X(nbLocks > (int)RESULT,
			   Q_FUNC_INFO,
			   "unsufficient locks number");

	/*
	 * Initialize source lock (might be null)
	 */
	locks[SOURCE] = imageLock;

	/*
	 * Also initialize other mutexes for selflocking depending on the
	 * available updateThread
	 */
	for (int i = 1; i < nbLocks; i++)
	{
		locks[i] = (updateThread != nullptr ? new QMutex() : nullptr);
	}

	if (updateThread != nullptr)
	{
		this->moveToThread(updateThread);

		connect(this, SIGNAL(finished()), updateThread, SLOT(quit()),
				Qt::DirectConnection);

		updateThread->start();
	}
}

/*
 * QcvProcessor destructor.
 *	- clear messages
 *	- try to lock the last (TERMINATE) lock (for at least 500 ms):
 *	subclasses destructor should lock the last (TERMINATE) lock to
 *	ensure continuity in destruction (not interruptible by updates)
 *	- emit finished signal
 *	- wait for update thread (if any) which should be soon since
 *	the TERMINATE lock should prevent further updates
 *	- if there is some non null result mutexes in locks (i.e. others
 *	than SOURCE) unlock and destroy these mutexes.
 *	- and finally destroys the #locks array.
 * @post The #finished(); signal has been emitted
 */
QcvProcessor::~QcvProcessor()
{
	message.clear();
	processTimeString.clear();
	size_t terminateIndex = static_cast<size_t>(nbLocks - 1);

	/*
	 * Sub classes update method should behave as follows:
	 *	update method: "try" (and only try) to lock the last mutex before
	 *	updating and abort update if the trylock fails. Then unlock the last
	 *	mutex at the end of update method.
	 *	destructor: firmly lock the last mutex so destructor can't start until
	 *	update methods has unlocked the last mutex.
	 */
//	bool locked = false;
	if (locks[terminateIndex] != nullptr)
	{
//		qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//						   << QThread::currentThreadId() << ";"
//						   << Q_FUNC_INFO << ";"
//						   << "will try lock: TERMINATE " << locks[terminateIndex];
		// locked = locks[terminateIndex]->tryLock(500);
		locks[terminateIndex]->tryLock(500);
	}

//	if (locked)
//	{
//		qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//						   << QThread::currentThreadId() << ";"
//						   << Q_FUNC_INFO << ";"
//						   << "locked: TERMINATE " << locks[terminateIndex];
//	}

	emit finished();

	if (updateThread != nullptr)
	{
		/*
		 * Wait until update thread has received the "finished" signal through
		 * "quit" slot.
		 */
		updateThread->wait();
	}

	unlockMutexes(terminateIndex);
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << "unlocked: TERMINATE " << locks[terminateIndex];

	/*
	 * Unlock and delete all non null mutexes except locks[SOURCE] which might
	 * be owned by capture
	 * If subclasses receive external mutexes that should not be destroyed,
	 * subclasses destructors should only "forget" these mutex by setting them
	 * to nullptr.
	 */
	if (locks != nullptr)
	{
		for (int i = 1; i < nbLocks; i++)
		{
			if (locks[i] != nullptr)
			{
				/*
				 * Caution: Unlocking a mutex that is not locked results in
				 * undefined behavior.
				 * So if locks[i] is already locked then trylock will fail but
				 * unlock is legitimate.
				 * if locks[i] is not locked then trylock will succeed and the
				 * locks[i] will be locked which also legitimates the unlock
				 */
				locks[i]->tryLock();
				locks[i]->unlock();
				delete locks[i];
				locks[i] = nullptr;
			}
		}

		delete [] locks;
		locks = nullptr;
	}
}

/*
 * Sets new number format
 * @param format the new number format
 */
void QcvProcessor::setNumberFormat(const char * format)
{
	/*
	 * The format string should validate the following regex
	 * %[+- 0#]*[0-9]*([.][0-9]+)?[efEF]
	 */
	QRegExpValidator validator(numberRegExp, NULL);

	QString qFormat(format);
	int pos = 0;
	if (validator.validate(qFormat,pos) == QValidator::Acceptable)
	{
		numberFormat = format;
		meanStdFormat = format + QString::fromUtf8(" ± ") + format;
		minMaxFormat = format + QString::fromUtf8(" / ") + format;
	}
	else
	{
		qWarning("QcvProcessor::setNumberFormat(%s) : invalid format", format);
	}
}

/*
 * Get the format c-string for numbers
 * @return the format string for numbers (e.g.: "%5.2f")
 */
const char * QcvProcessor::getNumberFormat()
{
	return numberFormat.toStdString().c_str();
}

/*
 * Get the format c-string for std dev of numbers
 * @return the format string for numbers (e.g.: " ± %4.2f")
 */
const char * QcvProcessor::getStdFormat()
{
	return meanStdFormat.toLocal8Bit().data();
}

/*
 * Get the format c-string for min / max of numbers
 * @return the format string for numbers (e.g.: "%5.2f / %5.2f")
 */
const char * QcvProcessor::getMinMaxFormat()
{
	return minMaxFormat.toLocal8Bit().data();
}

/*
 * Number of mutexes in #locks
 * @return The number of mutexes in #locks
 */
int QcvProcessor::locksSize() const
{
	return nbLocks;
}

/*
 * Accessor to the array of mutexes
 * @param index the index of the mutex to get
 * @return the address og the required mutex
 */
QMutex * QcvProcessor::getLock(const int index)
{
	if ((index >= 0) && (index < nbLocks))
	{
		return locks[index];
	}

	return nullptr;
}

/*
 * Accessor to #timePerFeatureController
 * @return a pointer to #timePerFeatureController
 */
QBoolController * QcvProcessor::getTimePerFeatureController()
{
	return &timePerFeatureController;
}

/*
 * Send to stream (for showing processor attributes values)
 * @param dbg the debug stream to send to
 * @return a reference to the output stream
 */
QDebug & QcvProcessor::toDBStream(QDebug & dbg) const
{
	return toStream_Impl<QDebug>(dbg);
}

/*
 * Friend QDebug output operator
 * @param dbg the debug stream
 * @param proc the Qcvprocessor to send to debug stream
 * @return the debug stream
 */
QDebug & operator << (QDebug & dbg, const QcvProcessor & proc)
{
	proc.toDBStream(dbg.nospace());
	return dbg.space();
}

// ----------------------------------------------------------------------------
// Public Slots
// ----------------------------------------------------------------------------

/*
 * Update computed images slot and sends updated signal
 * required
 * @post The following signals have been emitted:
 *	- #updated();
 *	- #processTimeUpdated(QString &);
 *	- #processTimeUpdated(ProcessTime *);
 */
void QcvProcessor::update()
{
	/*
	 * Important note : CvProcessor::update() should NOT be called here
	 * since it should be called in QcvXXXprocessor subclasses such that
	 * QcvXXXProcessor::update method should contain :
	 *	- call to CvXXXProcessor::update() (not QCvXXXProcessor)
	 *	- emit signals from QcvXXXProcessor
	 *	- call to QcvProcessor::update() (this method) to
	 *		- emit updated signal
	 *		- emit standard process time strings signals
	 *	- or
	 *		- emit updated signal in QcvXXXProcessor
	 *		- customize your processtimes and emit time strings signals
	 */
//	qDebug() << Q_FUNC_INFO << "emits image updated";
	emit updated();
	processTimeString.sprintf(meanStdFormat.toStdString().c_str(),
							  getMeanProcessTime(0), getStdProcessTime(0));
//	processMinMaxTimeString.sprintf(minMaxFormat.toStdString().c_str(),
//									getMinProcessTime(0), getMaxProcessTime(0));
	emit processTimeUpdated(processTimeString);
//	emit processTimeMinMaxUpdated(processMinMaxTimeString);
	emit processTimeUpdated(&meanProcessTime);
}

/*
 * Changes source image slot.
 * Attributes needs to be cleaned up then set up again
 * @param image the new source Image
 * @post Various signals have been emitted:
 * 	- imageChanged(sourceImage)
 * 	- imageCchanged()
 * 	- if image size changed then imageSizeChanged() is emitted
 * 	- if image color space changed then imageColorsChanged() is emitted
 */
void QcvProcessor::setSourceImage(Mat *image)
	throw (CvProcessorException)
{
	Size previousSize(sourceImage != nullptr ? sourceImage->size() : Size(0, 0));
	int previousNbChannels(nbChannels);

	if (locks[SOURCE] != nullptr)
	{
		locks[SOURCE]->lock();
		// qDebug() << "QcvProcessor::setSourceImage: lock";
	}

	CvProcessor::setSourceImage(image);

	if (locks[SOURCE] != nullptr)
	{
		// qDebug() << "QcvProcessor::setSourceImage: unlock";
		locks[SOURCE]->unlock();
	}

	emit imageChanged(sourceImage);

	emit imageChanged();

	if ((previousSize.width != image->cols) ||
		(previousSize.height != image->rows))
	{
		emit imageSizeChanged();
	}

	if (previousNbChannels != nbChannels)
	{
		emit imageColorsChanged();
	}

	// Force update
	update();
}

/*
 * Sets Time per feature processing time unit (reimplemented as a slot).
 * @param value the time per feature value (true or false)
 */
void QcvProcessor::setTimePerFeature(const bool value)
{
	CvProcessor::setTimePerFeature(value);
	emit timePerFeatureChanged(timePerFeature);
}

/*
 * Reset mean and std process time in order to re-start computing
 * (reimplemented as a slot)
 * new mean and std process time values.
 */
void QcvProcessor::resetMeanProcessTime()
{
	CvProcessor::resetMeanProcessTime();
}

/*
 * Emits finished signal to terminate any #updateThread
 */
void QcvProcessor::finish()
{
	emit finished();
}

// ----------------------------------------------------------------------------
// Protected Methods
// ----------------------------------------------------------------------------

/*
 * Locks a single mutex (iff the mutex at this index is not null in
 * #locks array)
 * @param index the index of the mutex to lock
 * @return true if this mutex has been locked, false if the index is
 * invalid (>= #nbLocks) or if the mutex at this index in #locks in null
 */
bool QcvProcessor::lockMutexes(const size_t index)
{
	if (static_cast<int>(index) < nbLocks)
	{
		if (locks[index] != nullptr)
		{
			locks[index]->lock();
			return true;
		}
	}
	return false;
}

/*
 * Try to lock a single mutex (iff the mutex at this index is not null in
 * #locks array)
 * @param index the index of the mutex to lock
 * @return true if this mutex has been locked, false if the index is
 * invalid (>= #nbLocks) or if the mutex at this index in #locks in null
 * or simply because the mutex at this index is already locked
 */
bool QcvProcessor::tryLockMutexes(const size_t index)
{
	if (static_cast<int>(index) < nbLocks)
	{
		if (locks[index] != nullptr)
		{
			return locks[index]->tryLock();
		}
	}
	return false;
}

/*
 * UnLocks a single mutex (iff the mutex at this index is not null in
 * #locks array)
 * @param index the index of the mutex to unlock
 * @return true if this mutex has been unlocked, false if the index is
 * invalid (>= #nbLocks) or if the mutex at this index in #locks in null
 */
bool QcvProcessor::unlockMutexes(const size_t index)
{
	if (static_cast<int>(index) < nbLocks)
	{
		if (locks[index] != nullptr)
		{
			locks[index]->unlock();
			return true;
		}
	}

	return false;
}

/*
 * Locks a range of mutexes in #locks
 * @param low the index of the first mutex to lock in #locks
 * @param high the index of the last mutex to lock in #locks
 * @return the number of mutexes successfully locked (if all mutexes
 * have been successfully locked then the result should be (high-low+1)
 * @warning if low >= high no mutexes will be locked
 */
size_t QcvProcessor::lockMutexesRange(const size_t low, const size_t high)
{
	size_t nbLocked = 0;
	size_t nbLocksU = static_cast<size_t>(nbLocks);

	if (low < nbLocksU && high < nbLocksU)
	{
		for (size_t i = low; i <= high; i++)
		{
			if (locks[i] != nullptr)
			{
				locks[i]->lock();
				nbLocked++;
			}
		}
	}

	return nbLocked;
}

/*
 * Unocks a range of mutexes in #locks
 * @param low the index of the first  mutex to unlock in #locks
 * @param high the index of the last mutex to unlock in #locks
 * @return the number of mutexes successfully unlocked (if all mutexes
 * have been successfully unlocked then the result should be (high-low+1)
 * @warning if low >= high no mutexes will be unlocked
 */
size_t QcvProcessor::unlockMutexesRange(const size_t low, const size_t high)
{
	size_t nbUnlocked = 0;
	size_t nbLocksU = static_cast<size_t>(nbLocks);

	if (low < nbLocksU && high < nbLocksU)
	{
		for (size_t i = low; i <= high; i++)
		{
			if (locks[i] != nullptr)
			{
				locks[i]->unlock();
				nbUnlocked++;
			}
		}
	}

	return nbUnlocked;
}


/*
 * Converts an array of std::string to a QStringList
 * @param names the array of string
 * @param nbElements the number of elements in the array
 * @return a QStringList containing the same thing as the array of
 * string
 */
QStringList QcvProcessor::stringArray2StringList(const string names[],
												 const size_t nbElements)
{
	QStringList list;

	for (size_t i = 0; i < nbElements; i++)
	{
		list << names[i].c_str();
	}

	return list;
}
