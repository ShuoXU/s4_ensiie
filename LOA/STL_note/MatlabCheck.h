/*
 * MatlabCheck.h
 *
 *  Created on: 9 févr. 2017
 *      Author: davidroussel
 */

#ifndef MATLABCHECK_H_
#define MATLABCHECK_H_

#include "ExternalCheck.h"

/**
 * Matlab external checker
 */
template <class Iterator>
class MatlabCheck : public ExternalCheck<Iterator>
{
	/*
	 * Reminder of ExternalCheck elements reused here
	 */
	using typename ExternalCheck<Iterator>::value_t;
	using ExternalCheck<Iterator>::tool;
	using ExternalCheck<Iterator>::availability;
	using ExternalCheck<Iterator>::command;

	public:
		/**
		 * Protected constructor for Matlab checker
		 * @param baseDir the base directory to run the external tool from
		 * @param begin an iterator to the first value of the tested sequence
		 * @param end an iterator past the last value of the tested sequence
		 * @param minValue minimum possible value in the sequence
		 * @param maxValue maximum possible value in the sequence
		 * @param meanValue computed mean value of the sequence
		 * @param stdValue computed std value of the sequence
		 * @param medianValue computed median value in the sequence
		 */
		MatlabCheck(const string & baseDir,
		            const Iterator & begin,
		            const Iterator & end,
		            const value_t & minValue,
		            const value_t & maxValue,
		            const double & meanValue,
		            const double & stdValue,
		            const double & medianValue);
};

#endif /* MATLABCHECK_H_ */
