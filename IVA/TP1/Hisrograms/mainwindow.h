#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <Qcv/capture/QcvCapture.h>
#include <Qcv/matWidgets/QcvMatWidget.h>
#include "QcvHistograms.h"

/**
 * Namespace for generated UI
 */
namespace Ui {
	class MainWindow;
}

/**
 * Rendering mode for main image
 */
enum struct RenderMode : int
{
	IMAGE = 0,//!< QImage rendering mode
	PIXMAP,   //!< QPixmap in a QLabel rendering mode
	GL        //!< OpenGL in a QGLWidget rendering mode
};

/**
 * OpenCV/Qt Histograms and LUT main window
 */
class MainWindow : public QMainWindow
{
	Q_OBJECT

	public:
		/**
		 * MainWindow constructor.
		 * @param capture the capture QObject to capture frames from devices
		 * or video files
		 * @param processor processor and LUT processing class
		 * @param parent parent widget
		 */
		explicit MainWindow(QcvCapture * capture,
							QcvHistograms * histograms,
							QWidget *parent = NULL);

		/**
		 * MainWindow destructor
		 */
		virtual ~MainWindow();

	signals:
		/**
		 * Signal to send update message when something changes
		 * @param message the message
		 * @param timeout number of ms the message should be displayed
		 */
		void sendMessage(const QString & message, int timeout = 0);

		/**
		 * Signal to send when video size is changed
		 * @param size the new video size
		 */
		void sizeChanged(const QSize & size);

		/**
		 * Signal to send when requesting opening a device (camera)
		 * @param deviceId the device ID
		 * @param width the requested video width
		 * @param height the requested video height
		 */
		void deviceOpened(const int deviceId,
						  const size_t width,
						  const size_t height);
		/**
		 * Signal to send when requesting opening a video file
		 * @param fileName the video file to open
		 * @param width the requested video width
		 * @param height the requested video height
		 */
		void videoOpened(const QString & fileName,
						 const size_t width,
						 const size_t height);

		/**
		 * Signal to send when requesting opening a directory containing images to read
		 * @param dirName the directory name
		 * @param width the requested video width
		 * @param height the requested video height
		 */
		void directoryOpened(const QString & dirName,
							 const size_t width,
							 const size_t height);

		/**
		 * Signal to send when requesting video flip
		 * @param flip video flip
		 */
		void flipVideo(const bool flip);

		/**
		 * Signal to send when mainwindow closes (or when the application quits)
		 */
		void finished();

	private:
		/**
		 * The UI built in QtDesigner or QtCreator
		 */
		Ui::MainWindow *ui;

		/**
		 * The Capture object grabs frame using OpenCV HiGui
		 */
		QcvCapture * capture;

		/**
		 * The Hist and LUT object compute histograms and performs LUT
		 * on capture source image
		 */
		QcvHistograms * processor;

		/**
		 * Image preferred width
		 */
		int preferredWidth;

		/**
		 * Image preferred height
		 */
		int preferredHeight;

		/**
		 * Message to send to statusBar
		 */
		QString message;

		/**
		 * Changes all image widgets nature according to desired rendering mode.
		 * Possible values for mode are:
		 * 	- IMAGE: widgetImage is assigned to a QcvMatWidgetImage instance
		 * 	- PIXMAP: widgetImage is assigned to a QcvMatWidgetLabel instance
		 * 	- GL: widgetImage is assigned to a QcvMatWidgetGL instance
		 * @param mode the rendering mode
		 */
		void setupImageWidgets(const RenderMode mode);

		/**
		 * Change a widget image nature according to the desired rendering mode
		 * @param matWidget the QcvMatWidget to change to either
		 *	- QcvMatWidgetImage
		 *	- QcvMatWidgetLabel
		 *	- QcvMatWidgetGL
		 * @param parent the parent widget so matWiget can be removed from it
		 * recreated and reattached to it
		 * @param mode the rendering mode for the image widget
		 */
		void setupAnImageWidget(QcvMatWidget * matWidget,
								QWidget * parent,
								const RenderMode mode);

		/**
		 * Setup UI from capture settings when launching application
		 */
		void setupUIfromCapture();

		/**
		 * Setup UI from processor settings when launching application
		 */
		void setupUIfromProcessor();

	private slots:

		/**
		 * Re setup processor from UI settings when source image changes
		 */
		void setupProcessorFromUI();

		/**
		 * Menu action when Sources->camera 0 is selected
		 * Sets capture to open device 0. If device is not available
		 * menu item is set to inactive.
		 */
		void on_actionCamera_0_triggered();

		/**
		 * Menu action when Sources->camera 1 is selected
		 * Sets capture to open device 0. If device is not available
		 * menu item is set to inactive
		 */
		void on_actionCamera_1_triggered();

		/**
		 * Menu action when Sources->file is selected.
		 * Opens file dialog and tries to open selected file (if not empty),
		 * then sets capture to open the selected file
		 */
		void on_actionFile_triggered();

		/**
		 * Menu action when Source->Directory is selected
		 * Opens file dialog and tries to open selected directory (if not empty)
		 * then sets capture to open images in this directory
		 */
		void on_actionDirectory_triggered();

		/**
		 * Menu action to quit application.
		 */
		void on_actionQuit_triggered();

		/**
		 * Menu action when flip image is selected.
		 * Sets capture to change flip status which leads to reverse
		 * image horizontally
		 */
		void on_actionFlip_triggered();

		/**
		 * Menu action when original image size is selected.
		 * Sets capture not to resize image
		 */
		void on_actionOriginalSize_triggered();

		/**
		 * Menu action when constrained image size is selected.
		 * Sets capture resize to preferred width and height
		 */
		void on_actionConstrainedSize_triggered();

		/**
		 * Menu action to replace current image rendering widget by a
		 * QcvMatWidgetImage instance.
		 */
		void on_actionRenderImage_triggered();

		/**
		 * Menu action to replace current image rendering widget by a
		 * QcvMatWidgetLabel with pixmap instance.
		 */
		void on_actionRenderPixmap_triggered();

		/**
		 * Menu action to replace current image rendering widget by a
		 * QcvMatWidgetGL instance.
		 */
		void on_actionRenderOpenGL_triggered();


		/**
		 * Original size radioButton action.
		 * Sets capture resize to off
		 */
		void on_radioButtonOrigSize_clicked();

		/**
		 * Custom size radioButton action.
		 * Sets capture resize to preferred width and height
		 */
		void on_radioButtonCustomSize_clicked();

		/**
		 * Width spinbox value change.
		 * Changes the preferred width and if custom size is selected apply
		 * this custom width
		 * @param value the desired width
		 */
		void on_spinBoxWidth_valueChanged(int value);

		/**
		 * Height spinbox value change.
		 * Changes the preferred height and if custom size is selected apply
		 * this custom height
		 * @param value the desired height
		 */
		void on_spinBoxHeight_valueChanged(int value);

		/**
		 * Flip capture image horizontally.
		 * changes capture flip status
		 */
		void on_checkBoxFlip_clicked();

		/**
		 * Select one of the actionTabXXX according to the selected index in
		 * tabWidget
		 * @param index the index of the selected tab
		 */
		void on_tabWidget_currentChanged(int index);
};

#endif // MAINWINDOW_H
