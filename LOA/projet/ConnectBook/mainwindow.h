/**
 * @brief the mainly operation of this application
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QWidget>
#include <QTreeView>
#include <QTableView>
#include <datastruct.h>
#include <QTableWidgetItem>
#include <QIcon>
#include <QSortFilterProxyModel>
#include "treemodel.h"
#include "tablemodel.h"
#include "myfilter.h"


struct People;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief the constructure
     * @param parent
     */
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    /**
     * @brief add preople action
     */
    void on_actionAdd_triggered();
    /**
     * @brief update tablemodel according to the different index
     * @param index  index of treemodel
     */
    void updateTableW(const QModelIndex & index);
private slots:
    /**
     * @brief to matching the text in the searching lineedit
     */
    void filterText();
    /**
     *  @brief insert un item in treeview
     * @param str the string of the new item
     * @param i to definite the parent item of the new item is people(0) or organisation(1)
     */
    void insertRow(QString str, int i);
    /**
     * @brief remove a item of treeview
     * @return
     */
    bool removeRow();
    /**
     * @brief a event filter for adding the photo
     * @param watched
     * @param event
     * @return
     */
    bool eventFilter(QObject *watched, QEvent *event);
    /**
     * @brief an action for adding an organisation
     */
    void on_actionAdd_Organisation_triggered();

    /**
     * @ to know the subitem(organisation) for people is selected or not
     * @param index
     */
     void selectUpdate(const QModelIndex index);
     /**
      * @brief if the subitem organisation of a people is selected turn to it
      */
     void turnToOrganisation();

     /**
      * @brief anaction for removing an item of a treeview
      */
    void on_actionRemove_triggered();

    void on_actionExport_XML_file_triggered();

    void on_actionImport_XML_file_triggered();

private:
    void closeEvent(QCloseEvent *ev);
    /**
     * @brief the initialization of the toolbar
     */
     void initMenuBar();
     /**
      * @brief build mainly model: treemodel and tablemodel
      */
     void modelBuild();
     /**
      * @brief if the organisation for a people is not in the treeview, add it
      * @param name
      */
     void addOrganisationDialog(QString name);


    Ui::MainWindow *ui;
    Myfilter *proxyModel;// a model for sorting and filtering the treeview
    int tmp_indexp=-1;
    int tmp_indexo=-1;
    bool imageFlag; // true->people false->organisation
    bool selectFlag; //true->selected

    QTreeView* treeview;
    TreeModel* treemodel;
    Tablemodel* tablemodelp;
    Tablemodel* tablemodelo;
    QVector<People> peoples;
    QVector<Organisation> organisations;
    QVector<QString> m_vNumber;



};

#endif // MAINWINDOW_H
