#include <QDebug>

#include "QcvAlgoParam.h"

#include "QcvAlgoParamBooleanWidget.h"
#include "ui_QcvAlgoParamBooleanWidget.h"

/*
 * Constructor
 * @param param the associated parameter
 * @param direct direct operation: if true indicates that parameter
 * value setting is performed directly through
 * param->setBoolValue(...) which is blocking in
 * multithread environments. If false indicates that parameter value
 * setting is performed through signal/slot mechanism, which in
 * multithreaded environments is non blocking with queued connections.
 * @param settings the settings to use to set initial value and/or
 * save the current value (if different from default value) to settings
 * @param settingsName the name to use when reading or writing the
 * parameter value to settings
 * @param parent the parent widget
 */
QcvAlgoParamBooleanWidget::QcvAlgoParamBooleanWidget(CvAlgoParam * param,
													 const bool direct,
													 QSettings * settings,
													 const QString & settingsName,
													 QWidget *parent) :
	QcvAlgoParamWidget(param, direct, settings, settingsName, parent),
	ui(new Ui::QcvAlgoParamBooleanWidget)
{
//	qDebug() << this << "->" << Q_FUNC_INFO;
	ui->setupUi(this);

	assert(parameter->getType() == CvAlgoParam::ParamType::BOOLEAN);

	// If settings are available then try to read value from settings
	readSetting();

	// Setup title
	ui->checkBox->setText(QString::fromStdString(parameter->getName()));

	// Setup tooltip
	ui->checkBox->setToolTip(QString::fromStdString(parameter->getDescription()));

	// Set current state
	ui->checkBox->blockSignals(true);
	ui->checkBox->setChecked(parameter->getBoolValue());
	bool settable = parameter->isSettable();
	if (!settable)
	{
		ui->checkBox->setEnabled(false);
		ui->resetButton->setEnabled(false);
	}
	ui->checkBox->blockSignals(false);

	// Setup connections if no direct operations
	if (!directOp && settable)
	{
		QcvAlgoParam * qParameter = dynamic_cast<QcvAlgoParam *>(parameter);
		if (qParameter != nullptr)
		{
			connect(this, SIGNAL(parameterUpdated(bool)),
					qParameter, SLOT(setBoolValue(bool)));
			connect(this, SIGNAL(parameterReset()),
					qParameter, SLOT(resetToDefaultValue()));
		}
		else
		{
			qDebug() << Q_FUNC_INFO
					 << "unable to convert param"
					 << parameter->getName().c_str()
					 << "to Qt flavored param with signals/slots";
			directOp = true;
		}
	}
}

/*
 * Destructor.
 * Clears the UI.
 */
QcvAlgoParamBooleanWidget::~QcvAlgoParamBooleanWidget()
{
//	qDebug() << this << "->" << Q_FUNC_INFO;
	delete ui;
	// All signals deconnection is performed in ~QcvAlgoParamWidget()
}

/*
 * Read value from settings (iff #settings is non null and #settingsName
 * is non empty)
 * @return true if a value has been found in settings, and if this value
 * is different from the current value of the parameter and has been set,
 * false otherwise
 */
bool QcvAlgoParamBooleanWidget::readSetting()
{
	if ((settings != nullptr) &&
		!settingsName.isEmpty() &&
		!parameter->isConstant())
	{
		QVariant variant = settings->value(settingsName);
		if (variant.isValid())
		{
			bool boolPrefValue = variant.toBool();
			if (boolPrefValue != parameter->getBoolValue())
			{
//				qDebug() << Q_FUNC_INFO << settingsName << (boolPrefValue ? "true" : "false");
				// Sets value directly in the parameter
				parameter->setBoolValue(boolPrefValue);
				return true;
			}
		}
	}

	return false;
}

/*
 * Slot triggered when the checkbox value changes
 * @param value the checkbox state:
 * 	- Qt::Unchecked
 * 	- Qt::PartiallyChecked
 * 	- Qt::Checked
 * 	@note when value is Unchecked or PartiallyChecked the state is
 * 	considered to be off.
 * @note if the parameter is not settable, this method should
 * (theoretically) never be triggered
 * @post the new boolean value is set on the parameter and
 * parameterUpdated() signal is emitted.
 */
void QcvAlgoParamBooleanWidget::on_checkBox_stateChanged(int value)
{
	bool booleanValue = parameter->getBoolValue();

	switch (value)
	{
		case Qt::Unchecked:
		case Qt::PartiallyChecked:
			booleanValue = false;
			break;
		case Qt::Checked:
			booleanValue = true;
			break;
		default:
			break;
	}

//	qDebug() << Q_FUNC_INFO << "(" << (booleanValue ? "true" : "fase") << ")";

	if (directOp)
	{
		parameter->setBoolValue(booleanValue);
	}
	else
	{
		emit parameterUpdated(booleanValue);
	}

	emit parameterUpdated();

	// If settings are available then save this value to settings
	writeSetting(QVariant(booleanValue));
}

/*
 * Slot triggered when the reset button is clicked in order to reset
 * the value to its default (if it has one)
 * @post default value is retrieved from parameter and applied to
 * checkbox (this imples another call to on_checkBox_stateChanged()).
 */
void QcvAlgoParamBooleanWidget::on_resetButton_clicked()
{
	bool dValue = parameter->getBoolValue(CvAlgoParam::WhichValue::DEFAULTVALUE);

	ui->checkBox->blockSignals(true);
	ui->checkBox->setChecked(dValue);
	ui->checkBox->blockSignals(false);

	if (directOp)
	{
		parameter->resetToDefaultValue();
	}
	else
	{
		emit parameterReset();
	}

	emit parameterUpdated();

	removeSetting();
}
