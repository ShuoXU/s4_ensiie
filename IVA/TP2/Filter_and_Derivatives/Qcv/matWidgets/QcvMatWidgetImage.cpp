/*
 * QcvMatWidgetImage.cpp
 *
 *  Created on: 31 janv. 2012
 *	  Author: davidroussel
 */

#include <QPaintEvent>
#include <QSizePolicy>
#include <QDebug>	// for qDebug() <<
#include <QTime>	// for timed debug
#include <QThread>	// for identifying thread in debug output

#include <Qcv/matWidgets/QcvMatWidgetImage.h>

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * Default Constructor
 * @param parent parent widget
 * @param lock the lock to use on sourceImage in multi-threaded
 * environments
 * @param mouseSense mouse sensivity
 */
QcvMatWidgetImage::QcvMatWidgetImage(QWidget *parent,
									 QMutex * lock,
									 MouseSense mouseSense) :
	QcvMatWidget(parent, lock, mouseSense),
	qImage(NULL)
{
	setup();
}

/*
 * Constructor
 * @param sourceImage source image
 * @param parent parent widget
 * @param lock the lock to use on sourceImage in multi-threaded
 * environments
 * @param mouseSense mouse sensivity
 */
QcvMatWidgetImage::QcvMatWidgetImage(Mat * sourceImage,
									 QWidget *parent,
									 QMutex * lock,
									 MouseSense mouseSense) :
	QcvMatWidget(sourceImage, parent, lock, mouseSense),
	qImage(NULL)
{
	setSourceImage(sourceImage);
	setup();
}

/*
 * Destructor.
 */
QcvMatWidgetImage::~QcvMatWidgetImage()
{
	if (qImage != nullptr)
	{
		delete qImage;
	}
}

/*
 * Minimum size hint according to aspect ratio and min height of 100
 * @return minimum size hint
 */
//QSize QcvMatWidgetImage::minimumSizeHint () const
//{
//	// qDebug("min size called");
//	// return QSize((int)(100.0*aspectRatio), 100);
//	return sizeHint();
//}

/*
 * Size policy to keep aspect ratio right
 * @return
 */
//QSizePolicy QcvMatWidgetImage::sizePolicy () const
//{
//	return policy;
//}

/*
 * Sets new source image
 * @param sourceImage the new source image
 */
void QcvMatWidgetImage::setSourceImage(Mat * sourceImage)
{
	if (qImage != nullptr)
	{
		delete qImage;
	}
	// setup and convert image
	QcvMatWidget::setSourceImage(sourceImage);
	convertImage();
	qImage = new QImage(displayImage.data,
						displayImage.cols,
						displayImage.rows,
						displayImage.step,
						QImage::Format_RGB888);

	// re-setup geometry since height x width may have changed
	updateGeometry();
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

/*
 * Setup widget (defines size policy)
 */
void QcvMatWidgetImage::setup()
{
//	qDebug("QcvMatWidgetImage::Setup");

	/*
	 * Customize size policy
	 */
	QSizePolicy qsp(QSizePolicy::Fixed, QSizePolicy::Fixed);
	// sets height depends on width (also need to reimplement heightForWidth())
	qsp.setHeightForWidth(true);
	setSizePolicy(qsp);

	/*
	 * Customize layout
	 */

	// size policy has changed to call updateGeometry
	updateGeometry();
}

// ----------------------------------------------------------------------------
// Protected Methods
// ----------------------------------------------------------------------------

/*
 * paint event reimplemented to draw content
 * @param event the paint event
 */
void QcvMatWidgetImage::paintEvent(QPaintEvent *event)
{
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay()
//					   << QThread::currentThreadId()
//					   << "QcvMatWidgetImage::paintEvent start";

	// evt draws in image directly
	QcvMatWidget::paintEvent(event);

	if (displayImage.data != nullptr)
	{
		// then draw image
		QPainter painter(this);
		painter.setRenderHint(QPainter::SmoothPixmapTransform, true);
		if (event == NULL)
		{
			painter.drawImage(0, 0, *qImage);
		}
		else // partial repaint
		{
			painter.drawImage(event->rect(), *qImage);
		}
	}
//	else
//	{
//		qWarning("QcvMatWidgetImage::paintEvent : image.data is NULL");
//	}
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay()
//					   << QThread::currentThreadId()
//					   << "QcvMatWidgetImage::paintEvent end";
}
