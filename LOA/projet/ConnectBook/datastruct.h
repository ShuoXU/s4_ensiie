/**
 * @brief  the definition of all the structure we need
 */
#ifndef STUDENT_H
#define STUDENT_H
#include <QString>
#include <QVector>

struct Name
{
    QString lastname;
    QString firstname;
    QString nickname;

    Name():lastname(""),firstname(""),nickname(""){}
    Name(QString ln, QString fn, QString nn =""):lastname(ln),firstname(fn),nickname(nn){}

    Name& operator =(const Name& n)
    {
        lastname = n.lastname;
        firstname = n.firstname;
        nickname = n.nickname;
        return *this;
    }
};

enum Sex{male,female};

enum Type{work,home};

struct Tel
{
    Type type;
    QString tel;
    Tel():type(Type(0)),tel(""){}
    Tel(Type t, QString tels=""):type(t),tel(tels){}
    Tel& operator =(const Tel& t)
    {
        type = t.type;
        tel = t.tel;
        return *this;
    }
};

struct Adress
{
    Type type;
    QString road;
    QString zipcode;
    QString localite;
    QString region;
    QString country;
    Adress():type(Type(0)),road(""),zipcode(""),localite(""),region(""),country(""){}
    Adress(Type t,QString r="",QString z="",QString local="",QString reg="",QString coun="")
        :type(t),road(r),zipcode(z),localite(local),region(reg),country(coun){}

    Adress& operator =(const Adress& adr)
    {
        type = adr.type;
        road = adr.road;
        zipcode = adr.zipcode;
        localite = adr.localite;
        region = adr.region;
        country = adr.country;
        return *this;
    }
};

struct Organisation;
struct People
{
    Name name;
    bool sex;
    Tel tel;
    Adress adress;
    QString email;
    QString site;
    QString image;
    QString note;
    struct Organisation
    {
        QString name;
        Tel tel;
        Adress adress;
        QString email;
        QString site;
        QString logo;
        QVector<People> members;
        QString note;
        Organisation():name(""),tel(Tel()),adress(Adress()),email(""),
            site(""),logo(""),note(""){}
        Organisation& operator =(const Organisation& n)
        {
           name = n.name;
           tel = n.tel;
           adress = n.adress;
           email = n.email;
           site = n.site;
           logo = n.logo;
           members = n.members;
           note = n.note;
            return *this;
        }
    }o;
    People():name(Name()),sex(true),tel(Tel()),
        adress(Adress()),email(""),site(""),image(""),note(""),o(Organisation()){}
    People(Name n, Sex s, Tel t, Adress adr, QString e="", QString sites="",
           QString images="",QString notes=""):name(n),sex(s),tel(t),adress(adr),email(e),site(sites),
            image(images),note(notes),o(Organisation()){}
    People& operator =(const People& p)
    {
        name = p.name;
        sex = p.sex;
        tel = p.tel;
        adress = p.adress;
        email = p.email;
        site = p.site;
        image = p.image;
        note=  p.note;
        o = p.o;
        return *this;
    }
};


struct Organisation
{
    QString name;
    Tel tel;
    Adress adress;
    QString email;
    QString site;
    QString logo;
    QVector<People> members;
    QString note;
    Organisation():name(""),tel(Tel()),adress(Adress()),email(""),
        site(""),logo(""),members(),note(""){}
    Organisation& operator =(const Organisation& n)
    {
       name = n.name;
       tel = n.tel;
       adress = n.adress;
       email = n.email;
       site = n.site;
       logo = n.logo;
       members = n.members;
       note = n.note;
        return *this;
    }
};


#endif // STUDENT_H
