#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QObject>
#include <QFileDialog>
#include <QWindow>
#include <QDebug>

#include <assert.h>

#include <Qcv/matWidgets/QcvMatWidgetImage.h>
#include <Qcv/matWidgets/QcvMatWidgetLabel.h>
#include <Qcv/matWidgets/QcvMatWidgetGL.h>

/*
 * MainWindow constructor.
 * @param capture the capture QObject to capture frames from devices
 * or video files
 * @param processor the openCV image processor
 * @param parent parent widget
 */
MainWindow::MainWindow(QcvVideoCapture * capture,
					   QcvGFilter * processor,
					   QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	capture(capture),
	processor(processor),
	preferredWidth(320),
	preferredHeight(240)
{
	ui->setupUi(this);
	ui->scrollAreaSource->setBackgroundRole(QPalette::Mid);

	// ------------------------------------------------------------------------
	// Assertions
	// ------------------------------------------------------------------------
	assert(capture != NULL);

	assert(processor != NULL);

	// ------------------------------------------------------------------------
	// Special widgets initialisation
	// ------------------------------------------------------------------------
	// Replace QcvMatWidget instances with QcvMatWidgetImage instances
	// sets image widget sources for the first time
	// connects processor->update to image Widgets->updated
	// connects processor->image changed to image widgets->setSourceImage
	setRenderingMode(RENDER_IMAGE);

	// ------------------------------------------------------------------------
	// rest of Signal/Slot connections
	// ------------------------------------------------------------------------

	// Capture, processor and this messages to status bar
	connect(capture, SIGNAL(messageChanged(QString,int)),
			ui->statusBar, SLOT(showMessage(QString,int)));

	connect(processor, SIGNAL(sendMessage(QString,int)),
			ui->statusBar, SLOT(showMessage(QString,int)));

	connect(this, SIGNAL(sendMessage(QString,int)),
			ui->statusBar, SLOT(showMessage(QString,int)));

	// When Processor source image changes, some attributes are reinitialised
	// So we have to set them up again according to current UI values
	connect(processor, SIGNAL(imageChanged()),
			this, SLOT(setupProcessorFromUI()));

	// Connects UI requests to capture
	connect(this, SIGNAL(sizeChanged(const QSize &)),
			capture, SLOT(setSize(const QSize &)), Qt::DirectConnection);
	connect(this, SIGNAL(deviceChanged(int,size_t,size_t)),
			capture, SLOT(openDevice(int,size_t,size_t)), Qt::DirectConnection);
	connect(this, SIGNAL(fileChanged(QString,size_t,size_t)),
			capture, SLOT(openFile(QString,size_t,size_t)), Qt::DirectConnection);
	connect(this, SIGNAL(flipChanged(bool)),
			capture, SLOT(setFlipped(bool)), Qt::DirectConnection);
	connect(this, SIGNAL(grayChanged(bool)),
			capture, SLOT(setGray(bool)), Qt::DirectConnection);
	connect(this, SIGNAL(finished()),
			capture, SLOT(finish()));

	connect(this, SIGNAL(finished()),
			processor, SLOT(finish()));

	// ------------------------------------------------------------------------
	// UI setup according to capture and processor options
	// ------------------------------------------------------------------------
	setupUIfromCatpure();
	setupUIfromProcessor();
}

/*
 * MainWindow destructor
 */
MainWindow::~MainWindow()
{
	delete ui;
}

/*
 * Changes widgetImage nature according to desired rendering mode.
 * Possible values for mode are:
 * 	- IMAGE: widgetImage is assigned to a QcvMatWidgetImage instance
 * 	- PIXMAP: widgetImage is assigned to a QcvMatWidgetLabel instance
 * 	- GL: widgetImage is assigned to a QcvMatWidgetGL instance
 * @param mode
 */
void MainWindow::setRenderingMode(const RenderMode mode)
{
	// Disconnect signals from slots first
	disconnect(processor, SIGNAL(updated()),
			   ui->sourceImage, SLOT(update()));

	disconnect(processor, SIGNAL(imageChanged(Mat*)),
			   ui->sourceImage, SLOT(setSourceImage(Mat*)));

	QWindow * currentWindow = windowHandle();
	if (mode == RENDER_GL)
	{
		disconnect(currentWindow,
				   SIGNAL(screenChanged(QScreen *)),
				   ui->sourceImage,
				   SLOT(screenChanged()));
	}

	// remove widgets in scroll areas
	QWidget * wSource = ui->scrollAreaSource->takeWidget();

	if (wSource == ui->sourceImage)
	{
		// delete removed widgets
		delete ui->sourceImage;

		// create new widget
		// Mat * sourceMat = processor->getImagePtr("source");
		Mat * sourceMat = processor->getDisplayImagePtr();

		switch (mode)
		{
			case RENDER_PIXMAP:
				ui->sourceImage = new QcvMatWidgetLabel(sourceMat);
				break;
			case RENDER_GL:
				ui->sourceImage = new QcvMatWidgetGL(sourceMat);
				break;
			case RENDER_IMAGE:
			default:
				ui->sourceImage = new QcvMatWidgetImage(sourceMat);
				break;
		}

		if (ui->sourceImage != NULL)
		{
			// Name the new images widgets with same name as in UI files
			 ui->sourceImage->setObjectName(QString::fromUtf8("sourceImage"));

			// add to scroll areas
			ui->scrollAreaSource->setWidget(ui->sourceImage);

			// Reconnect signals to slots
			connect(processor, SIGNAL(updated()),
					ui->sourceImage, SLOT(update()));

			connect(processor, SIGNAL(imageChanged(Mat*)),
					ui->sourceImage, SLOT(setSourceImage(Mat*)));

			if (mode == RENDER_GL)
			{
				connect(currentWindow,
						SIGNAL(screenChanged(QScreen *)),
						ui->sourceImage,
						SLOT(screenChanged()));
			}

			// Sends message to status bar and sets menu checks
			message.clear();
			message.append(tr("Render more set to "));
			switch (mode)
			{
				case RENDER_IMAGE:
					ui->actionRenderPixmap->setChecked(false);
					ui->actionRenderOpenGL->setChecked(false);
					message.append(tr("QImage"));
					break;
				case RENDER_PIXMAP:
					ui->actionRenderImage->setChecked(false);
					ui->actionRenderOpenGL->setChecked(false);
					message.append(tr("QPixmap in QLabel"));
					break;
				case RENDER_GL:
					ui->actionRenderImage->setChecked(false);
					ui->actionRenderPixmap->setChecked(false);
					message.append(tr("QGLWidget"));
					break;
				default:
				break;
			}
			emit sendMessage(message, 5000);
		}
		else
		{
			qDebug("MainWindow::on_actionRenderXXX some new widget is null");
		}
	}
	else
	{
		qDebug("MainWindow::on_actionRenderXXX removed widget is not in ui->");
	}
}

/*
 * Setup UI values from capture settings
 */
void MainWindow::setupUIfromCatpure()
{
	// ------------------------------------------------------------------------
	// UI setup according to capture options
	// ------------------------------------------------------------------------
	// Sets size radioButton states
	if (capture->isResized())
	{
		/*
		 * Initial Size radio buttons configuration
		 */
		ui->radioButtonOrigSize->setChecked(false);
		ui->radioButtonCustomSize->setChecked(true);
		/*
		 * Initial Size menu items configuration
		 */
		ui->actionOriginalSize->setChecked(false);
		ui->actionConstrainedSize->setChecked(true);

		QSize size = capture->getSize();
		qDebug("Capture->size is %dx%d", size.width(), size.height());
		preferredWidth = size.width();
		preferredHeight = size.height();
	}
	else
	{
		/*
		 * Initial Size radio buttons configuration
		 */
		ui->radioButtonCustomSize->setChecked(false);
		ui->radioButtonOrigSize->setChecked(true);

		/*
		 * Initial Size menu items configuration
		 */
		ui->actionConstrainedSize->setChecked(false);
		ui->actionOriginalSize->setChecked(true);
	}

	// Sets spinboxes preferred size
	ui->spinBoxWidth->setValue(preferredWidth);
	ui->spinBoxHeight->setValue(preferredHeight);

	// Sets flipCheckbox and menu item states
	bool flipped = capture->isFlipped();
	ui->actionFlip->setChecked(flipped);
	ui->checkBoxFlip->setChecked(flipped);
}

/*
 * Setup UI values from processor attributes
 */
void MainWindow::setupUIfromProcessor()
{
	// Display image
	QEnumController * eController;
	eController = processor->getDisplayModeController();
	eController->setupComboBox(ui->comboBoxImages);
	eController->setupMenu(ui->menuDisplay);

	CvGFilter::ImageDisplay displayMode = processor->getDisplayMode();

	switch (displayMode)
	{
		case CvGFilter::ImageDisplay::INPUT_IM:
			ui->actionDisplayInput->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::INPUT_IM);
			break;
		case CvGFilter::ImageDisplay::GRAY_IM:
			ui->actionDisplayGray->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::GRAY_IM);
			break;
		case CvGFilter::ImageDisplay::BLURRED_IM:
			ui->actionDisplayBlurred->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::BLURRED_IM);
			break;
		case CvGFilter::ImageDisplay::GRADIENT_X_IM:
			ui->actionDisplayHorizontalGradient->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::GRADIENT_X_IM);
			break;
		case CvGFilter::ImageDisplay::GRADIENT_Y_IM:
			ui->actionDisplayVerticalGradient->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::GRADIENT_Y_IM);
			break;
		case CvGFilter::ImageDisplay::GRADIENT_MAG_IM:
			ui->actionDisplayGradientMagnitude->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::GRADIENT_MAG_IM);
			break;
		case CvGFilter::ImageDisplay::GRADIENT_ANGLE_IM:
			ui->actionDisplayGradientAngle->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::GRADIENT_ANGLE_IM);
			break;
		case CvGFilter::ImageDisplay::EDGE_MAP_IM:
			ui->actionDisplayEdgeMap->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::EDGE_MAP_IM);
			break;
		case CvGFilter::ImageDisplay::LAPLACIAN_IM:
			ui->actionDisplayLaplacian->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::LAPLACIAN_IM);
			break;
		case CvGFilter::ImageDisplay::CORNERNESS_IM:
			ui->actionDisplayCornerness->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::CORNERNESS_IM);
			break;
		case CvGFilter::ImageDisplay::HARRISCORNER_IM:
			ui->actionDisplayHarris->setChecked(true);
			ui->comboBoxImages->setCurrentIndex((int)CvGFilter::ImageDisplay::HARRISCORNER_IM);
			break;
		default:
			break;
	}

	// Edge Mode
	eController = processor->getEdgeModeController();
	eController->setupComboBox(ui->comboBoxEdges);
	eController->setupMenu(ui->menuEdges_mode);

	switch (processor->getEdgeMode())
	{
		case CvGFilter::EdgeDisplay::THRESHOLD:
			ui->actionEdgeModeGradient->setChecked(true);
			ui->comboBoxEdges->setCurrentIndex((int)CvGFilter::EdgeDisplay::THRESHOLD);
			break;
		case CvGFilter::EdgeDisplay::CANNY:
			ui->actionEdgeModeCanny->setChecked(true);
			ui->comboBoxEdges->setCurrentIndex((int)CvGFilter::EdgeDisplay::CANNY);
			break;
		case CvGFilter::EdgeDisplay::MERGED:
			ui->actionEdgeModeMerged->setChecked(true);
			ui->comboBoxEdges->setCurrentIndex((int)CvGFilter::EdgeDisplay::MERGED);
			break;
		default:
			break;
	}

	// Hide / show ui->comboBoxEdges & ui->menuEdges_mode accordinr to displaymode
	bool enableEdgeModes = displayMode == CvGFilter::ImageDisplay::EDGE_MAP_IM;
	ui->comboBoxEdges->setEnabled(enableEdgeModes);
	ui->menuEdges_mode->setEnabled(enableEdgeModes);

	connect(processor, SIGNAL(edgeModeChanged(bool)),
			ui->comboBoxEdges, SLOT(setEnabled(bool)));
	connect(processor, SIGNAL(edgeModeChanged(bool)),
			ui->menuEdges_mode, SLOT(setEnabled(bool)));

	QRangeIntController * riController;

	// Kernel
	riController = processor->getKernelSizeController();
	riController->setupSpinBox(ui->spinBoxKernel);
	riController->setupSlider(ui->horizontalSliderKernel);
	riController->addNumberLabel(ui->labelKernelMin,
								 RangeValue<int>::WhichValue::MIN_VALUE);
	riController->addNumberLabel(ui->labelKernelMax,
								 RangeValue<int>::WhichValue::MAX_VALUE);

	// Threshold
	riController = processor->getThresholdLevelController();
	riController->setupSpinBox(ui->spinBoxThreshold);
	riController->setupSlider(ui->horizontalSliderThreshold);
	riController->addNumberLabel(ui->labelThresholdMin,
								 RangeValue<int>::WhichValue::MIN_VALUE);
	riController->addNumberLabel(ui->labelThresholdMax,
								 RangeValue<int>::WhichValue::MAX_VALUE);

	// Sigma
	setupSigma();

	// Kappa
	QRangeDoubleController * dController = processor->getHarrisKappaController();
	dController->setupSpinBox(ui->doubleSpinBoxKappa);
	dController->setupSlider(ui->horizontalSliderKappa);
	dController->addNumberLabel(ui->labelKappaMin,
								RangeValue<double>::WhichValue::MIN_VALUE);
	dController->addNumberLabel(ui->labelKappaMax,
								RangeValue<double>::WhichValue::MAX_VALUE);
}

/*
 * Setup Sigma slider and double spinbox according to processor
 * values
 */
void MainWindow::setupSigma()
{
	QRangeDoubleController * dController = processor->getSigmaController();
	if (!dController->has(ui->doubleSpinBoxSigma))
	{
		dController->setupSpinBox(ui->doubleSpinBoxSigma);
	}
	if (!dController->has(ui->horizontalSliderSigma))
	{
		dController->setupSlider(ui->horizontalSliderSigma);
	}
	if (!dController->has(ui->labelSigmaMin))
	{
		dController->addNumberLabel(ui->labelSigmaMin,
									RangeValue<double>::WhichValue::MIN_VALUE);
	}
	if (!dController->has(ui->labelSigmaMax))
	{
		dController->addNumberLabel(ui->labelSigmaMax,
									RangeValue<double>::WhichValue::MAX_VALUE);
	}
}

/*
 * Re setup processor from UI settings when source changes
 */
void MainWindow::setupProcessorFromUI()
{
	processor->getDisplayModeController()->setValue(ui->comboBoxImages->currentIndex());
	processor->getEdgeModeController()->setValue(ui->comboBoxEdges->currentIndex());
	processor->getKernelSizeController()->setValue(ui->spinBoxKernel->value());
	setupSigma();
	processor->getThresholdLevelController()->setValue(ui->spinBoxThreshold->value());
	processor->getHarrisKappaController()->setValue(ui->doubleSpinBoxKappa->value());
}

/*
 * Menu action when Sources->camera 0 is selected
 * Sets capture to open device 0. If device is not available
 * menu item is set to inactive.
 */
void MainWindow::on_actionCamera_0_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	qDebug("Opening device 0 ...");

	emit deviceChanged(0,
					   static_cast<size_t>(width),
					   static_cast<size_t>(height));
}

/*
 * Menu action when Sources->camera 1 is selected
 * Sets capture to open device 0. If device is not available
 * menu item is set to inactive
 */
void MainWindow::on_actionCamera_1_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	qDebug("Opening device 1 ...");

	emit deviceChanged(1, width, height);
}

/*
 * Menu action when Sources->file is selected.
 * Opens file dialog and tries to open selected file (is not empty),
 * then sets capture to open the selected file
 */
void MainWindow::on_actionFile_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	QString fileName =
			QFileDialog::getOpenFileName(this,
										 tr("Open Video"),
										 "./",
										 tr("Video Files (*.avi *.mkv *.mp4 *.m4v)"),
										 NULL,
										 QFileDialog::ReadOnly);

	qDebug("Opening file %s ...", fileName.toStdString().c_str());

	if (fileName.length() > 0)
	{
		emit fileChanged(fileName, width, height);
	}
	else
	{
		qWarning("empty file name");
	}
}

/*
 * Menu action to qui application
 */
void MainWindow::on_actionQuit_triggered()
{
	emit finished();
	this->close();
}

/*
 * Menu action when flip image is selected.
 * Sets capture to change flip status which leads to reverse
 * image horizontally
 */
void MainWindow::on_actionFlip_triggered()
{
	emit flipChanged(!capture->isFlipped());
	/*
	 * There is no need to update ui->checkBoxFlip since it is connected
	 * to ui->actionFlip through signals/slots
	 */
}

/*
 * Menu action when original image size is selected.
 * Sets capture not to resize image
 */
void MainWindow::on_actionOriginalSize_triggered()
{

	ui->actionConstrainedSize->setChecked(false);

	emit sizeChanged(QSize(0, 0));
}

/*
 * Menu action when constrained image size is selected.
 * Sets capture resize to preferred width and height
 */
void MainWindow::on_actionConstrainedSize_triggered()
{
	ui->actionOriginalSize->setChecked(false);

	emit sizeChanged(QSize(preferredWidth, preferredHeight));
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetImage instance.
 */
void MainWindow::on_actionRenderImage_triggered()
{
	// qDebug("Setting image rendering to: images");
	setRenderingMode(RENDER_IMAGE);
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetLabel with pixmap instance.
 */
void MainWindow::on_actionRenderPixmap_triggered()
{
	// qDebug("Setting image rendering to: pixmaps");
	setRenderingMode(RENDER_PIXMAP);
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetGL instance.
 */
void MainWindow::on_actionRenderOpenGL_triggered()
{
	qDebug("Setting image rendering to: opengl");
	setRenderingMode(RENDER_GL);
}

/*
 * Original size radioButton action.
 * Sets capture resize to off
 */
void MainWindow::on_radioButtonOrigSize_clicked()
{
	ui->actionConstrainedSize->setChecked(false);
	emit sizeChanged(QSize(0, 0));
}

/*
 * Custom size radioButton action.
 * Sets capture resize to preferred width and height
 */
void MainWindow::on_radioButtonCustomSize_clicked()
{
	ui->actionOriginalSize->setChecked(false);
	emit sizeChanged(QSize(preferredWidth, preferredHeight));
}

/*
 * Width spinbox value change.
 * Changes the preferred width and if custom size is selected apply
 * this custom width
 * @param value the desired width
 */
void MainWindow::on_spinBoxWidth_valueChanged(int value)
{
	preferredWidth = value;
	if (ui->radioButtonCustomSize->isChecked())
	{
		emit sizeChanged(QSize(preferredWidth, preferredHeight));
	}
}

/*
 * Height spinbox value change.
 * Changes the preferred height and if custom size is selected apply
 * this custom height
 * @param value the desired height
 */
void MainWindow::on_spinBoxHeight_valueChanged(int value)
{
	preferredHeight = value;
	if (ui->radioButtonCustomSize->isChecked())
	{
		emit sizeChanged(QSize(preferredWidth, preferredHeight));
	}
}

/*
 * Flip capture image horizontally.
 * changes capture flip status
 */
void MainWindow::on_checkBoxFlip_clicked()
{
	/*
	 * There is no need to update ui->actionFlip since it is connected
	 * to ui->checkBoxFlip through signals/slots
	 */
	emit flipChanged(ui->checkBoxFlip->isChecked());
}
