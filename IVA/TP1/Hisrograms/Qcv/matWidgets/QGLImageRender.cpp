/*
 * QGLImageRender.cpp
 *
 *  Created on: 28 févr. 2011
 *	  Author: davidroussel
 */
#include <QDebug>
#include <QTime>	// for time in debug messages
#include <QThread>	// for thread source in debuf messages

#ifdef __APPLE__
	#include <gl.h>
	#include <glu.h>
#else
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif
#include <Qcv/matWidgets/QGLImageRender.h>

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * QGLImageRender Constructor
 * @param image the RGB image to draw in the pixel buffer
 * @param format pixel format
 * @param pixelScale pixel scale pointer from container
 * @param parent the parent widget
 */
QGLImageRender::QGLImageRender(const Mat & image,
							   const GLenum format,
							   float * pixelScale,
							   QWidget *parent) :
	QGLWidget(parent),
	image(image),
	pixelFormat(format),
	pixelScale(pixelScale)
{
	if (!doubleBuffer())
	{
		qWarning("QGLImageRender::QGLImageRender caution : no double buffer");
	}
	if (this->image.data == NULL)
	{
		qWarning("QGLImageRender::QGLImageRender caution : image data is null");
	}
	if (this->pixelScale == NULL)
	{
		qCritical("QGLImageRender::QGLImageRender caution : pixel scale is null");
	}
}

/*
 * QGLImageRender destructor
 */
QGLImageRender::~QGLImageRender()
{
	image.release();
}

/*
 * Size hint
 * @return Qsize containing size hint
 */
QSize QGLImageRender::sizeHint () const
{
	return minimumSizeHint();
}

/*
 * Minimum Size hint
 * @return QSize containing the minimum size hint
 */
QSize QGLImageRender::minimumSizeHint() const
{
	if (image.data != nullptr)
	{
		return QSize(image.cols, image.rows);
	}
	else
	{
		qWarning("QGLImageRender::minimumSizeHint : probably invalid sizeHint");
		return QSize(320,240);
	}
}

/*
 * Size Policy for this widget
 * @return A No resize at all policy
 */
QSizePolicy	QGLImageRender::sizePolicy () const
{
	return QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

// ----------------------------------------------------------------------------
// Protected Methods
// ----------------------------------------------------------------------------

/*
 * Initialise GL drawing (called once on each QGLContext)
 */
void QGLImageRender::initializeGL()
{
//	qDebug("GL init ...");
	glClearColor(0.0, 0.0, 0.0, 0.0);
//	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
}

/*
 * Paint GL : called whenever the widget needs to be painted
 */
void QGLImageRender::paintGL()
{
//	qDebug("GL drawing pixels ...");

	glClear(GL_COLOR_BUFFER_BIT);

	if (image.data != nullptr)
	{
		/* apply the right translate so the image drawing starts top left */
		glRasterPos4f(0.0f,(GLfloat)(image.rows), 0.0f, 1.0f);
		/*
		 * for hi dpi displays
		 *	typically pixelScale =
		 *	- 1.0 for normal displays
		 *	- 2.0 for hidpi displays
		 */
		glPixelZoom(*pixelScale, -(*pixelScale));

		glDrawPixels(image.cols,
					 image.rows,
					 pixelFormat,
					 GL_UNSIGNED_BYTE,
					 image.data);
		// In any circumstance you should NOT use glFlush or swapBuffers() here
	}
	else
	{
		qWarning("Nothing to draw");
	}
}

/*
 * Resize GL : called whenever the widget has been resized
 */
void QGLImageRender::resizeGL(int width, int height)
{
//	qDebug("GL resizeGL ...");

	glViewport(0, 0, (GLsizei) width, (GLsizei) height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (image.data != nullptr)
	{
		glOrtho(0, (GLdouble) image.cols, 0, (GLdouble) image.rows, 1.0, -1.0);
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
