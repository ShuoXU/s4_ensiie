/*
 * RangeValue.cpp
 *
 *  Created on: 22 mai 2015
 *      Author: davidroussel
 */
#include <cmath>
#include <typeinfo>       // operator typeid, std::bad_typeid
using namespace std;

#include <QMetaType>	// for Q_DECLARE_METATYPE

#include <utils/RangeValue.h>

#define MIN(a,b) (a < b ? a : b)
#define MAX(a,b) (a > b ? a : b)

// ----------------------------------------------------------------------------
// Constructors / destructor
// ----------------------------------------------------------------------------

/*
 * Default constuctor
 */
template <typename T>
RangeValue<T>::RangeValue() :
	RangeValue<T>(AbstractRangeValue<T>::EPSILON,
				  T(0),
				  2*AbstractRangeValue<T>::EPSILON,
				  AbstractRangeValue<T>::EPSILON)
{
}

/*
 * Valued constructor with currents value, range values and step value.
 * 	- minValue should be less than current value, otherwise it is set to
 * 	the current value
 * 	- maxValue should be greater than current value, otherwise it is set
 * 	to the current value
 * 	- stepValue should be such that the next value should
 * 	be less than the max value and previous value should be greater than
 * 	min value, otherwise actual step value is reduced to meet these
 * 	conditions
 * @note In any cases minValue, value & maxValue are eventually
 * reorderded to ensure minValue < value < maxValue
 * @param value the current value
 * @param min the minimum possible value
 * @param max the maximum possible value
 * @param step the step value to get to the next/previous value
 * [default is EPSILON]
 */
template <typename T>
RangeValue<T>::RangeValue(const T & value,
						  const T & min,
						  const T & max,
						  const T & step) :
	AbstractRangeValue<T>(value, min, max, step)
{
	fixStep();
	AbstractRangeValue<T>::fixBounds();
}

/*
 * Valued constructor from tuple
 * @param tupleValues tuple containing value, min, max & step
 */
template <typename T>
RangeValue<T>::RangeValue(const tuple<T, T, T, T> & tupleValues) :
	RangeValue(get<0>(tupleValues),
			   get<1>(tupleValues),
			   get<2>(tupleValues),
			   get<3>(tupleValues))
{
}

/*
 * Copy constructor
 * @param arv the other RangeValue to copy
 */
template <typename T>
RangeValue<T>::RangeValue(const AbstractRangeValue<T> & arv) :
	AbstractRangeValue<T>(arv)
{
}

/*
 * Move constructor
 * @param arv the other RangeValue to move
 */
template <typename T>
RangeValue<T>::RangeValue(AbstractRangeValue<T> && arv) :
	AbstractRangeValue<T>(arv)
{
}

/*
 * Destructor
 */
template <typename T>
RangeValue<T>::~RangeValue()
{
}

// ----------------------------------------------------------------------------
// RangeValue specific operations
// ----------------------------------------------------------------------------
/*
 * number of steps in the range
 * @return number of steps to get from min to max value
 */
template <typename T>
size_t RangeValue<T>::nbSteps() const
{
	return size_t((_maxValue - _minValue) / _stepValue);
}

/*
 * the index corresponding to the current value in terms of how many
 * steps from minimum value.
 * @return the number of steps from min value to current value
 */
template <typename T>
size_t RangeValue<T>::index() const
{
	return size_t(ceil((_value - _minValue) / double(_stepValue)));
}

/*
 * Current value setter.
 * The new value has to be within min/max value range and if there is
 * a step mode it should be accessible from the current value with a
 * finite number of steps.
 * @param value the new current value to set
 * @return true if the new current value has been set, false otherwise
 */
template <typename T>
bool RangeValue<T>::setValue(const T & value)
{
	if (value >= _minValue && value <= _maxValue)
	{
		// Number of steps to reach value
		double dstep =  double(value - _value) / double(_stepValue);
		double nstep = round(dstep);
		if (abs(dstep-nstep) < EPSILON)
		{
			_value = value;
			return true;
		}
	}

	return false;
}

/*
 * Any value setter.
 * Relies on other specialized setters to set the value according to
 * the "which" parameter.
 * @param value the new value to set
 * @param which an enum value indicating which value to set
 * @return true if the new value has been set, false otherwise
 */
template <typename T>
bool RangeValue<T>::setValue(const T & value, const WhichValue which)
{
	switch (which)
	{
		case WhichValue::DEFAULT_VALUE:
			/*
			 * Default value is not settable outside of constructor
			 */
			return false;
		case WhichValue::MIN_VALUE:
			return this->setMin(value);
		case WhichValue::MAX_VALUE:
			return this->setMax(value);
		case WhichValue::STEP_VALUE:
			return setStep(value);
		case WhichValue::VALUE:
		default:
			return setClosestValue(value);
	}
}

/*
 * Current value setter to the closest possible value.
 * if the new value is > max then the current value is set to max.
 * if the new value is < min then the current value is set to min.
 * if the new value is >= min & <= max it is set to the closest accessible
 * value according to steps.
 * @param value the new current value to set
 * @return true if the value in argument has been set exactly, false if
 * the new value is slightly different
 * @post a new value has been set as the closest value to the argument
 * @note This implementation is supposed to be more efficient than
 * the one provided by
 * AbstractRangeValue<T>::setClosestValue(const T & value) by computing
 * the reachable value instead of trying to reach it iteratively
 */
template <typename T>
bool RangeValue<T>::setClosestValue(const T & value)
{
	if (value < _minValue)
	{
		_value = _minValue;
		return false;
	}

	if (value > _maxValue)
	{
		_value = _maxValue;
		return false;
	}

	/*
	 * value is >= min && <= max so it should be reachable
	 * More efficient implementation by computing the reachable value rather
	 * than trying to reach it
	 */
	double incs = incrementsToValue(value);
	double integerIncs = round(incs);
	_value = _value + T(_stepValue * integerIncs);

//	cout << "RangeValue<T>::setClosestValue(" << value << ") --> " << _value << endl;

	/*
	 * Return true if the reached value is no different from the required value
	 */
	return abs(double(_value) - double(value)) < EPSILON;
}

/*
 * Sets the value corresponding to this index, which means the value
 * located at "index" steps distance from the min value.
 * @param index the index to set value to
 * @return true if the index was valid [i.e. corresponding to a value
 * between min and max] and value has been set, false otherwise
 */
template <typename T>
bool RangeValue<T>::setIndex(const size_t index)
{
	T value = _minValue + (_stepValue * T(index));
	if (value <= _maxValue)
	{
		_value = value;
		return true;
	}
	else
	{
		return false;
	}
}

/*
 * Step value setter.
 * The new step value should be such that the next value according to
 * this new step value is less than the max value and the previous
 * value should be greater than the min value
 * @param value the new step value. if this value is <= EPSILON then
 * this range turns its mode to NONE and can't fetch next or previous
 * values anymore.
 * @return true if the new max value has been set
 */
template <typename T>
bool RangeValue<T>::setStep(const T & value)
{
	T previousStep = _stepValue;

	AbstractRangeValue<T>::setStep(value);

	T maxUpStep = _maxValue - _value;
	T maxDownStep = _value - _minValue;
	if (_stepValue > maxUpStep || _stepValue > maxDownStep)
	{
		_stepValue = previousStep;
		return false;
	}
	else
	{
		// if _stepValue is set then fix bounds accordingly
		AbstractRangeValue<T>::fixBounds();
		return true;
	}
}

/*
 * Computes next value
 * @return the new current value after reaching the next value according
 * to _stepValue or _maxValue if next value is greater
 * than _maxValue
 */
template <typename T>
T RangeValue<T>::next()
{
	T newValue = _value + _stepValue;

	if (newValue <= _maxValue)
	{
		_value = newValue;
	}

	return _value;

}

/*
 * Indicates this range has a next value < max value
 * @return true if the next value is < max value and false otherwise or
 * if the range mode is NONE
 */
template <typename T>
bool RangeValue<T>::hasNext() const
{
	T newValue = _value + _stepValue;

	return newValue <= _maxValue;
}

/*
 * Computes previous value
 * @return the new current value after reaching the previous value
 * according to _stepValue or _minValue if previous value
 * is less than _minValue
 */
template <typename T>
T RangeValue<T>::previous()
{
	T newValue = _value - _stepValue;

	if (newValue >= _minValue)
	{
		_value = newValue;
	}

	return _value;
}

/*
 * Indicates this range has a previous value > min value
 * @return true if the previous value is > min value and false
 * otherwise or if the range mode is NONE
 */
template <typename T>
bool RangeValue<T>::hasPrevious() const
{
	T newValue = _value - _stepValue;

	return newValue >= _minValue;
}

// ----------------------------------------------------------------------------
// Operators
// ----------------------------------------------------------------------------

/*
 * Comparison operator with another AbstractRangeValue
 * @param rv another AbstractRangeValue
 * @return true if both AbstractRangeValue have the same current, min, max and step
 * and are of type RangeValue, false otherwise
 */
template <typename T>
bool RangeValue<T>::operator ==(const AbstractRangeValue<T> & rv) const
{
	return (typeid(*this) == typeid(rv)) &&
		   AbstractRangeValue<T>::operator ==(rv);
}

/*
 * Comparison operator with another RangeValue
 * @param rv another RangeValue
 * @return true if both RangeValue have the same current, min, max,
 * step, false otherwise
 */
template <typename T>
bool RangeValue<T>::operator ==(const RangeValue<T> & rv) const
{
	return AbstractRangeValue<T>::operator ==(rv);
}

/*
 * Difference operator with another RangeValue
 * @param rv another RangeValue
 * @return false if both RangeValue have the same current, min, max, step
 * and are of type RangeValue, true otherwise
 */
template <typename T>
bool RangeValue<T>::operator !=(const RangeValue<T> & rv) const
{
	return !operator ==(rv);
}

/*
 * Copy operator from another AbstractRangeValue.
 * Replaces the current attributes with those of the other AbstractRangeValue
 * @param rv another AbstractRangeValue
 * @return a reference to the modified RangeValue
 * @post The AbstractRangeValue rv is only copied if it is an RangeValue,
 * otherwise "this" is not modified
 */
template <typename T>
AbstractRangeValue<T> & RangeValue<T>::operator =(const AbstractRangeValue<T> & rv)
{
	if (typeid(*this) == typeid(rv))
	{
		AbstractRangeValue<T>::operator =(rv);
	}

	return *this;
}

/*
 * Move operator from another AbstractRangeValue.
 * Replaces the current attributes with those of the other AbstractRangeValue
 * which can then be destroyed.
 * @param rv another AbstractRangeValue
 * @return a reference to the modified RangeValue
 * @post The AbstractRangeValue rv is only copied if it is an RangeValue,
 * otherwise "this" is not modified
 */
template <typename T>
AbstractRangeValue<T> & RangeValue<T>::operator =(AbstractRangeValue<T> && rv)
{
	if (typeid(*this) == typeid(rv))
	{
		AbstractRangeValue<T>::operator =(rv);
	}

	return *this;
}

/*
 * Addition operator with a regular value.
 * The argument value is added to the current value iff it is reachable
 * by a finite number of next or previous values iterations and stays
 * within range. Otherwise the current value is not modified
 * @param value the value to add to the current value
 * @return a new RangeValue (with eventually) a modified current value
 */
template <typename T>
RangeValue<T> RangeValue<T>::operator +(const T & value) const
{
	RangeValue<T> newRange(*this);

	newRange += value;

	return newRange;
}

/*
 * Substraction operator with a regular value.
 * The argument value is substracted to the current value iff it is
 * reachable by a finite number of next or previous values iterations
 * and stays within range. Otherwise the current value is not modified
 * @param value the value to add to the current value
 * @return a new RangeValue (with eventually) a modified current value
 */
template <typename T>
RangeValue<T> RangeValue<T>::operator -(const T & value) const
{
	RangeValue<T> newRange(*this);

	newRange -= value;

	return newRange;
}

/*
 * Multiplication operator with a regular value.
 * The argument value is multiplied by the current value iff it is
 * reachable by a finite number of next or previous values iterations
 * and stays within range. Otherwise the current value is not modified
 * @param value the value to add to the current value
 * @return a new RangeValue (with eventually) a modified current value
 */
template <typename T>
RangeValue<T> RangeValue<T>::operator *(const T & value) const
{
	RangeValue<T> newRange(*this);

	newRange *= value;

	return newRange;
}

/*
 * Division operator with a regular value.
 * The current value is divided byt the argument iff it is
 * reachable by a finite number of next or previous values iterations
 * and stays within range. Otherwise the current value is not modified
 * @param value the value to add to the current value
 * @return a new RangeValue (with eventually) a modified current value
 */
template <typename T>
RangeValue<T> RangeValue<T>::operator /(const T & value) const
{
	RangeValue<T> newRange(*this);

	newRange /= value;

	return newRange;
}

// ----------------------------------------------------------------------------
// Utility methods
// ----------------------------------------------------------------------------
/*
 * Number of increments (positive or negative) needed to reach the
 * value in argument from the internal #_value
 * @param value the value to reach
 * @return the numbr of increments (positive or negative) to reach
 * this value
 * @pre value is supposed to be within [#_minValue ... #_maxValue]
 * otherwise this method might fail.
 */
template <typename T>
double RangeValue<T>::incrementsToValue(const T value)
{
	/*
	 * _stepValue should NEVER be 0
	 */
	T up = MAX(value, _value);
	T down = MIN(value, _value);
	double direction = double(up - down);

	if (value < _value)
	{
		direction = -direction;
	}

	return direction / double(_stepValue);
}

/*
 * Number of increments to reach _maxValue from _value
 * @return the number of increments or multiplications of _value to
 * reach _maxValue. If this number is not an integer then _maxValue
 * is not reachable
 */
template <typename T>
double RangeValue<T>::incrementsToMax() const
{
	/*
	 * _stepValue should NEVER be 0
	 */
	return double(_maxValue - _value) / double(_stepValue);
}

/*
 * Number of decrements to reach _minValue from _value
 * @return the number of decrements or divisions of _value to
 * reach _minValue. If this number is not an integer then _minValue
 * is not reachable
 */
template <typename T>
double RangeValue<T>::incrementsToMin() const
{
	/*
	 * _stepValue should NEVER be 0
	 */
	return double(_value - _minValue) / double(_stepValue);
}

/*
 * Ensures _maxValue is reachable adding _stepValue n times to _value.
 * 	And fix _maxValue to the closest reachable value
 */
template <typename T>
void RangeValue<T>::fixMax()
{
	double n = incrementsToMax();
	double in = round(n);
	_maxValue = _value + T(_stepValue * in);
}

/*
 * Ensures _minValue is reachable by substracting _stepValue n times
 * to _value.
 * 	And fix _minValue to the closest reachable value
 */
template <typename T>
void RangeValue<T>::fixMin()
{
	double n = incrementsToMin();
	double in = round(n);
	_minValue = _value - T(_stepValue * in);
}

/*
 * Ensure _stepValue value consistency:
 * 	- _stepValue should be positive
 * 	- _stepValue < _maxValue - _value
 * 	- _stepValue < _value - _minValue
 */
template <typename T>
void RangeValue<T>::fixStep()
{
	// Ensures _stepValue > 0 and _stepValue >= EPSILON
	AbstractRangeValue<T>::fixStep();

	// Ensure _stepValue < _maxValue - _value UNLESS _value == _maxValue already
	// Ensure _stepValue < _value - _minValue UNLESS _value == _minValue already
	// T maxStepToMax = (_value < _maxValue ? _maxValue - _value : _stepValue);
	T maxStepToMax = ((_maxValue - _value) > EPSILON ? _maxValue - _value : _stepValue);
	T maxStepToMin = ((_value- _minValue) > EPSILON ? _value - _minValue : _stepValue);

	if (_stepValue > maxStepToMax || _stepValue > maxStepToMin)
	{
		_stepValue = MIN(maxStepToMax, maxStepToMin);
	}
}

/*
 * Stream Range value to output stream
 * @param out the output stream
 * @return a reference to the modified output stream
 */
template <typename T>
ostream & RangeValue<T>::toStream(ostream & out) const
{
	AbstractRangeValue<T>::template toStream_Impl<ostream>(out);
	return toStream_Impl<ostream>(out);
}

// ----------------------------------------------------------------------------
// Proto instantiations
// ----------------------------------------------------------------------------
/**
 * Template instantiation with int
 */
// AbstractRangeValue<int> is instanciated in another translation unit
extern template class AbstractRangeValue<int>;
template class RangeValue<int>;
template ostream & RangeValue<int>::toStream_Impl(ostream &) const;
using RangeValue_int = RangeValue<int>;
Q_DECLARE_METATYPE(RangeValue_int);

/**
 * Template instantiation with float
 */
// AbstractRangeValue<float> is instanciated in another translation unit
extern template class AbstractRangeValue<float>;
template class RangeValue<float>;
template ostream & RangeValue<float>::toStream_Impl(ostream &) const;
using RangeValue_float = RangeValue<float>;
Q_DECLARE_METATYPE(RangeValue_float);

/**
 * Template instantiation with double
 */
// AbstractRangeValue<double> is instanciated in another translation unit
extern template class AbstractRangeValue<double>;
template class RangeValue<double>;
template ostream & RangeValue<double>::toStream_Impl(ostream &) const;
using RangeValue_double = RangeValue<double>;
Q_DECLARE_METATYPE(RangeValue_double);
