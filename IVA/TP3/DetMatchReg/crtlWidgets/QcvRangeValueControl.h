#ifndef QCVRANGEVALUECONTROL_H
#define QCVRANGEVALUECONTROL_H

#include <QFlags>
#include <QMutex>

#include <exception>

#include <utils/RangeValue.h>

/**
 * Base template for all RangeValue<T> widget controllers
 */
template <class T>
class QcvRangeValueControl
{
	public:
		/**
		 * The graphic style of the wigdet
		 */
		enum Option
		{
			NoSpinner = 1,	//!< Do not uses a spinner
			NoSlider = 2,	//!< Do not uses a slider
			NoResetButton = 4,//!< Do not uses a reset button
			SingleLine = 8	//!< All widgets on a single line
		};
		Q_DECLARE_FLAGS(Options, Option)

	protected:
		/**
		 * Pointer to the range value to manage with this widget
		 * @note should NOT be null
		 */
		RangeValue<T> * rangeValue;

		/**
		 * Mutex to use when modifying values (iff not NULL) when
		 * operated from multiple threads
		 */
		QMutex * lock;

	public:
		/**
		 * Valued constructor
		 * @param rangeValue the range value to control
		 * @param lock the mutex to check before setting new values
		 * @param options flags indicating the presence of the spinner, the slider
		 * and the reset button (all by default)
		 * @throw exception if rangeValue is null
		 */
		QcvRangeValueControl(RangeValue<T> * rangeValue,
							 Options options = 0,
							 QMutex * lock = nullptr) throw (exception);
		/**
		 * Destructor
		 */
		virtual ~QcvRangeValueControl();

		/**
		 * Set new value in #rangeValue
		 * @param value the new value to set
		 * @return true if the value has been set exactly, or false
		 * if the value has been set as close as possible
		 */
		bool setValue(T value);

		/**
		 * Reset value of #rangeValue to its default
		 */
		void resetValue();
};

using QcvRangeValueControlInt = QcvRangeValueControl<int>;
using QcvRangeValueControlFloat = QcvRangeValueControl<float>;
using QcvRangeValueControlDouble = QcvRangeValueControl<double>;

Q_DECLARE_OPERATORS_FOR_FLAGS(QcvRangeValueControlInt::Options)
Q_DECLARE_OPERATORS_FOR_FLAGS(QcvRangeValueControlFloat::Options)
Q_DECLARE_OPERATORS_FOR_FLAGS(QcvRangeValueControlDouble::Options)

#endif // QCVRANGEVALUECONTROL_H
