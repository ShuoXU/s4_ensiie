/*
 * QcvVideoCapture.cpp
 *
 *  Created on: 29 janv. 2012
 *	  Author: davidroussel
 */

#include <QElapsedTimer>
#include <QDebug>	// for qDebug
#include <QTime>	// for tim in debug
#include "QcvTimerVideoCapture.h"

#include <opencv2/imgproc/imgproc.hpp>

/*
 * default time interval between refresh
 */
int QcvTimerVideoCapture::defaultFrameDelay = 33;

/*
 * Number of frames to test frame rate
 */
size_t QcvTimerVideoCapture::defaultFrameNumberTest = 5;

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * Constructor. Opens the default camera (0)
 * @param flipped mirror image status
 * @param gray convert image to gray status
 * @param skip indicates capture can skip an image. When the capture
 * result has not been processed yet, or when false that capture should
 * wait for the result to be processed before grabbing a new image.
 * This only applies when #updateThread is not NULL.
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * otherwise capture is updated in the current thread.
 * @param updateThread the thread used to run this capture
 * @param parent the parent QObject
 */
QcvTimerVideoCapture::QcvTimerVideoCapture(const bool flipped,
										   const bool gray,
										   const bool skip,
										   const size_t width,
										   const size_t height,
										   QThread * updateThread,
										   QObject * parent) :
	QcvTimerVideoCapture(0,
						 flipped,
						 gray,
						 skip,
						 width,
						 height,
						 updateThread,
						 parent)
{
}

/*
 * Constructor with device Id
 * @param deviceId the id of the camera to open
 * @param flipped mirror image
 * @param gray convert image to gray
 * @param skip indicates capture can skip an image. When the capture
 * result has not been processed yet, or when false that capture should
 * wait for the result to be processed before grabbing a new image.
 * This only applies when #updateThread is not NULL.
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @param updateThread the thread used to run this capture
 * @param parent the parent QObject
 * @post If the #grabTest() has been sucessful, the following signals
 * have been emitted:
 *	- #timerChanged(int);
 *	- #frameRateChanged(double);
 */
QcvTimerVideoCapture::QcvTimerVideoCapture(const int deviceId,
										   const bool flipVideo,
										   const bool gray,
										   const bool skip,
										   const size_t width,
										   const size_t height,
										   QThread * updateThread,
										   QObject * parent) :
	QcvVideoCapture(deviceId,
					flipVideo,
					gray,
					width,
					height,
					parent),
	filename(),
	timer(new QTimer(updateThread == nullptr ? this : nullptr)),
	updateThread(updateThread),
	lockLevel(0),
	skip(skip)
{
	if (updateThread != nullptr)
	{
		connect(this, SIGNAL(finished()),
				updateThread, SLOT(quit()),
				Qt::DirectConnection);

		moveToThread(this->updateThread);

		connect(this, SIGNAL(timerChanged(int)),
				timer, SLOT(start(int)));
		connect(this, SIGNAL(stopTimer()),
				timer, SLOT(stop()));
	}

	timer->setSingleShot(false);
	connect(timer, SIGNAL(timeout()),
			this, SLOT(update()));

	if (grabTest())
	{
		setSize_Impl(width, height);
		QString message("Camera ");
		message.append(QString::number(deviceId));
		message.append(" ");
		int delay = grabInterval(message);
		if (updateThread != nullptr)
		{
			updateThread->start();
		}
		timer->start(delay);
		qDebug("timer started with %d ms delay", delay);
		emit timerChanged(delay);
		emit frameRateChanged(1000.0 / static_cast<double>(delay));
	}
	else
	{
		qDebug() << "QcvTimerVideoCapture::QcvTimerVideoCapture(" << deviceId
				 << ") : grab test failed";
	}
}

/*
 * QcvVideoCapture constructor from file name
 * @param fileName video file to open
 * @param flipped mirror image
 * @param gray convert image to gray
 * @param skip indicates capture can skip an image. When the capture
 * result has not been processed yet, or when false that capture should
 * wait for the result to be processed before grabbing a new image.
 * This only applies when #updateThread is not NULL.
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @param updateThread the thread used to run this capture
 * @param parent the parent QObject
 * @post If the #grabTest() has been sucessful, the following signals
 * have been emitted:
 *	- #timerChanged(int);
 *	- #frameRateChanged(double);
 */
QcvTimerVideoCapture::QcvTimerVideoCapture(const QString & fileName,
										   const bool flipped,
										   const bool gray,
										   const bool skip,
										   const size_t width,
										   const size_t height,
										   QThread * updateThread,
										   QObject * parent) :
	QcvVideoCapture(fileName,
					flipped,
					gray,
					width,
					height,
					parent),
	filename(fileName),
	timer(new QTimer(updateThread == nullptr ? this : nullptr)),
	updateThread(updateThread),
	lockLevel(0),
	skip(skip)
{
	if (updateThread != nullptr)
	{
		connect(this, SIGNAL(finished()),
				updateThread, SLOT(quit()),
				Qt::DirectConnection);

		moveToThread(this->updateThread);

		connect(this, SIGNAL(timerChanged(int)),
				timer, SLOT(start(int)));
		connect(this, SIGNAL(stopTimer()),
				timer, SLOT(stop()));
	}

	timer->setSingleShot(false);
	connect(timer, SIGNAL(timeout()),
			this, SLOT(update()));

	if (grabTest())
	{
		setSize_Impl(width, height);
		QString message("File ");
		message.append(fileName);
		message.append(" ");

		int delay = grabInterval(message);
		if (updateThread != nullptr)
		{
			updateThread->start();
		}
		timer->start(delay);
		qDebug("timer started with %d ms delay", delay);
		emit timerChanged(delay);
		emit frameRateChanged(1000.0 / static_cast<double>(delay));
	}
}

/*
 * Destructor.
 * releases video capture and image
 */
QcvTimerVideoCapture::~QcvTimerVideoCapture()
{
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO << "started at level"
//			 << lockLevel;

	/*
	 * We need to get a lock before going one since
	 * the QcvVideoCapture::finish slot might be executing
	 * and it locks the mutex
	 */
	if (updateThread != nullptr)
	{
		if (lockLevel == 0)
		{
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "will lock";
			mutex.lock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "locked";
		}
		lockLevel++;
	}

	if (timer != nullptr)
	{
		if (timer->isActive())
		{
			if (updateThread == nullptr ||
				QThread::currentThread() == updateThread)
			{
				timer->stop();
			}
			else
			{
				emit stopTimer();
			}
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "timer stopped";
		}

		// stops calling update
		timer->disconnect(SIGNAL(timeout()), this, SLOT(update()));
//		qDebug() << QThread::currentThreadId()
//				 << Q_FUNC_INFO << "timer disconnected";
		if (timer->parent() != this)
		{
			delete timer; // delete unparented timer
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "timer deleted";
		}
	}

	if (updateThread != nullptr)
	{
		// Wait for thread to finish
		if (QThread::currentThread() != updateThread)
		{
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "wait for thread";

			if (!updateThread->wait(1000))
			{
				/*
				 * Don't understand why but with cameras the finish slot
				 * does not finish and we get stuck waiting forever
				 */
				updateThread->terminate();
//				qDebug() << QThread::currentThreadId()
//						 << Q_FUNC_INFO << "updateThread Terminated";
				lockLevel = 1;
			}

//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "wait finished";
		}
//		else
//		{
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "not waiting for thread";
//		}

		lockLevel--;
		if (lockLevel == 0)
		{
			mutex.unlock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "unlocked";
		}
	}

	// relesase OpenCV ressources
	filename.clear();
}

/*
 * Gets the image skipping policy
 * @return true if new image can be skipped when previous one has not
 * been processed yet, false otherwise.
 */
bool QcvTimerVideoCapture::isSkippable() const
{
	return skip;
}

/*
 * Indicates if capture works in synchronized mode where
 * frame rate is imposed.
 * @return true is capture works in synchronized mode, false
 * otherwise
 * @note Please note that this implementation always return true since
 * the capture is drived by a timer, although there cannot be a
 * constrained frame rate for the very same reason.
 */
bool QcvTimerVideoCapture::isSynchronized() const
{
	return true;
}

/*
 * Gets the contrained frame rate at which capture should operate
 * when synchronized mode is on
 * @return the constrained frame rate
 * @note Please note that this implementation will always return the
 * same value as #getFrameRate() as the capture is always drived by a
 * timer (#timer)
 */
double QcvTimerVideoCapture::getContrainedFrameRate() const
{
	return getFrameRate();
}

// ----------------------------------------------------------------------------
// Public Slots
// ----------------------------------------------------------------------------

/*
 * Open new device Id
 * @param deviceId device number to open
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @return true if device has been opened and checked and timer launched
 * @post if the capture device is sucessfully opened, the following
 * signals have been sent:
 *	- #timerChanged(int);
 *	- #frameRateChanged(double);
 *	- #imageChanged(cv::Mat *);
 */
bool QcvTimerVideoCapture::openDevice(const int deviceId,
									  const size_t width,
									  const size_t height)
{
	if (updateThread != nullptr)
	{
		if (lockLevel == 0)
		{
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "will lock";
			mutex.lock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "locked";
		}
		lockLevel++;
	}

	filename.clear();
	if (timer->isActive())
	{
		if (updateThread == nullptr || QThread::currentThread() == updateThread)
		{
			emit stopTimer();
			qDebug("stop timer request emitted");
		}
		else
		{
			timer->stop();
			qDebug("timer stopped");
		}
	}

	if (capture->isOpened())
	{
		capture->release();
	}

	if (!image.empty())
	{
		image.release();
	}

	capture->open(deviceId);

	bool grabbed = grabTest();

	if (grabbed)
	{
		setSize_Impl(width, height);

		statusMessage.clear();
		statusMessage.append("Camera ");
		statusMessage.append(QString::number(deviceId));
		statusMessage.append(" ");
		int delay = grabInterval(statusMessage);
		live = true;
		// timerChanged(int) is connected to timer->start(int)
		emit timerChanged(delay);
		emit frameRateChanged(1000.0 / static_cast<double>(delay));
		emit imageChanged(&imageDisplay);
		qDebug("timer started with %d ms delay", delay);
	}
	if (updateThread != nullptr)
	{
		lockLevel--;
		if (lockLevel == 0)
		{
			mutex.unlock();
//			qDebug() << "QcvTimerVideoCapture::" << Q_FUNC_INFO << "unlock";
		}
	}

	return grabbed;
}

/*
 * Open new video file
 * @param fileName video file to open
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @return true if video has been opened and timer launched
 * @post if the file is sucessfully opened in capture, the following
 * signals have been sent:
 *	- #timerChanged(int);
 *	- #frameRateChanged(double);
 *	- #imageChanged(cv::Mat *);
 */
bool QcvTimerVideoCapture::openFile(const QString & fileName,
									const size_t width,
									const size_t height)
{
	filename = fileName;

	if (timer->isActive())
	{
		if (updateThread == nullptr || QThread::currentThread() == updateThread)
		{
			timer->stop();
		}
		else
		{
			emit stopTimer();
		}
		qDebug("timer stopped");
	}

	if (updateThread != nullptr)
	{
		if (lockLevel == 0)
		{
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << " will lock";
			mutex.lock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "locked";
		}
		lockLevel++;
	}

	if (capture->isOpened())
	{
		capture->release();
	}

	if (!image.empty())
	{
		image.release();
	}

	capture->open(fileName.toStdString());

	bool grabbed = grabTest();

	if (grabbed)
	{
		setSize_Impl(width, height);
		// qDebug() << "open setSize done";
		statusMessage.clear();
		statusMessage.append("file ");
		statusMessage.append(fileName);
		statusMessage.append(" opened");

		int delay = grabInterval(statusMessage);
		live = false;
		// timerChanged(int) is connected to timer->start(int)
		emit timerChanged(delay);
		emit frameRateChanged(1000.0 / static_cast<double>(delay));
		emit imageChanged(&imageDisplay);
		qDebug("timer started with %d ms delay", delay);
	}

	if (updateThread != nullptr)
	{
		lockLevel--;
		if (lockLevel == 0)
		{
			mutex.unlock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "unlocked";
		}
	}

	return grabbed;
}

/*
 * Sets video flipping
 * @param flipVideo flipped video or not
 * @post if flipped status is changed then the following signals
 * have been emitted:
 *	- messageChanged(QString);
 *	- imageChanged(cv::Mat *);
 */
void QcvTimerVideoCapture::setFlipped(const bool value)
{
	bool previousFlip = this->flipped;
	this->flipped = value;

	if (updateThread != nullptr)
	{
		if (lockLevel == 0)
		{
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "will lock";
			mutex.lock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "locked";
		}
		lockLevel++;
	}

	if (!imageFlipped.empty())
	{
		imageFlipped.release();
	}

	if (value)
	{
		imageFlipped = Mat(imageResized.size(), imageResized.type());
	}
	else
	{
		imageFlipped = imageResized;
	}

	if (updateThread != nullptr)
	{
		lockLevel--;
		if (lockLevel == 0)
		{
			mutex.unlock();
//			qDebug() << "QcvTimerVideoCapture::" << Q_FUNC_INFO << "unlocked";
		}
	}

	if (previousFlip != value)
	{
		statusMessage.clear();
		statusMessage.sprintf("flip video is %s", (value ? "on" : "off"));
		emit messageChanged(statusMessage, messageDelay);
		emit imageChanged(&imageDisplay);
	}

	/*
	 * imageChanged signal is delayed until setGray is called
	 */
	// refresh image chain
	setGray(gray);
}

/*
 * Sets video conversion to gray
 * @param value the gray conversion status
 */
void QcvTimerVideoCapture::setGray(const bool value)
{
	bool previousGray = gray;

	gray = value;

	if (updateThread != nullptr)
	{
		if (lockLevel == 0)
		{
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "will lock";
			mutex.lock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "locked";
		}
		lockLevel++;
	}

	if (!imageDisplay.empty())
	{
		imageDisplay.release();
	}

	if (gray)
	{
		imageDisplay = Mat(imageFlipped.size(), CV_8UC1);
	}
	else
	{
		imageDisplay = imageFlipped;
	}

	if (updateThread != nullptr)
	{
		lockLevel--;
		if (lockLevel == 0)
		{
			mutex.unlock();
//			qDebug() << "QcvTimerVideoCapture::" << Q_FUNC_INFO << "unlocked";
		}
	}

	if (previousGray != value)
	{
		statusMessage.clear();
		statusMessage.sprintf("gray video is %s", (gray ? "on" : "off"));
		emit messageChanged(statusMessage, messageDelay);
	}

	/*
	 * In any cases emit image changed since
	 * 	- setSize may have been called
	 * 	- setFlipVideo may have been called
	 */
	emit imageChanged(&imageDisplay);
}

/*
 * Sets capture to synchronized mode (when true) where frame rate is
 * contrained (whenever possible). Otherwise frame rate is not
 * constrained and capture may proceed as fast as possible.
 * @param value the new value of synchronized mode
 * @note Please note that this method has NO effect on this class
 */
void QcvTimerVideoCapture::setSynchronized(const bool value)
{
	Q_UNUSED(value);
}

/*
 * Sets the desired frame rate on capture ONLY when capture is in
 * synchronized mode
 *	- If the new frame rate is 0.0 then use natural capture frame rate.
 *	- If the new frame rate is too high for capture then capture will
 *	keeps its highest possible frame rate according to the device in use
 * @param frameRate the new frame rate to set
 * @note Please note that this method has NO effect on this class
 */
void QcvTimerVideoCapture::setFrameRate(const double frameRate)
{
	Q_UNUSED(frameRate);
}

/*
 * Releases capture and emits #finished() signal
 */
void QcvTimerVideoCapture::finish()
{
	if (updateThread != nullptr)
	{
		if (lockLevel == 0)
		{
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "will lock";
			mutex.lock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "locked";
		}
		lockLevel++;
	}

	if (capture != nullptr)
	{
		if (capture->isOpened())
		{
			capture->release();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "capture released";
		}
	}

	if (QThread::currentThread() != updateThread)
	{
		emit finished();
	}
	else
	{
		updateThread->quit();
	}
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO << "capture finished emitted";

	if (updateThread != nullptr)
	{
		lockLevel--;
		if (lockLevel == 0)
		{
			mutex.unlock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "unlocked";
		}
	}

	/*
	 * We can't wait here for the thread to finish since this would
	 * mean a thread waiting for itself
	 */

	// Wait for thread to finish
	if (updateThread != nullptr && QThread::currentThread() != updateThread)
	{
//		qDebug() << QThread::currentThreadId()
//				 << Q_FUNC_INFO << "wait for thread";
		updateThread->wait();
//		qDebug() << QThread::currentThreadId()
//				 << Q_FUNC_INFO << "wait finished";
	}
//	else
//	{
//		qDebug() << QThread::currentThreadId()
//				 << Q_FUNC_INFO << "not waiting for thread to finish";
//	}
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

/*
 * Performs a grab test to fill #image
 * @return true if capture is opened and successfully grabs a first
 * frame into #image, false otherwise
 */
bool QcvTimerVideoCapture::grabTest()
{
//	qDebug("Grab test");
	bool result = false;

	if (capture->isOpened())
	{
		/*
		 * Video for Linux (V4L) does not support capture get/set properties
		 */
#ifndef Q_OS_LINUX
		int capWidth = capture->get(CV_CAP_PROP_FRAME_WIDTH);
		int capHeight = capture->get(CV_CAP_PROP_FRAME_HEIGHT);

		qDebug("Capture grab test with %d x %d image", capWidth, capHeight);
#endif
		// grabs first frame
		if (capture->grab())
		{
			bool retrieved = capture->retrieve(image);
			if (retrieved)
			{
				size.setWidth(image.cols);
				size.setHeight(image.rows);
				originalSize.setWidth(image.cols);
				originalSize.setHeight(image.rows);

				/*
				 * Tries to determine if direct resizing in capture is possible
				 * by setting original size through properties
				 * Typically :
				 * 	- camera capture might be resizable
				 * 	- video file capture may not be resizable
				 */
				directResize = setDirectSize(image.cols, image.rows);

				qDebug("Capture direct resizing is %s",
					   (directResize ? "on" : "off"));

				result = true;
			}
			else
			{
				qFatal("Video Capture unable to retreive image");
			}
		}
		else
		{
			qFatal("Video Capture can not grab");
		}
	}
	else
	{
		qFatal("Video Capture is not opened");
	}

	return result;
}

/*
 * Get or compute interval between two frames
 * @return interval between two frames
 * @pre capture is already instanciated
 */
int QcvTimerVideoCapture::grabInterval(const QString & message)
{
	int frameDelay = defaultFrameDelay;

	/*
	 * Video for Linux (V4L) does not support capture get/set properties
	 */
#ifndef Q_OS_LINUX
	// Tries to get framerate from capture
	frameRate = capture->get(CV_CAP_PROP_FPS);
#else
	frameRate = -1.0;
#endif

	/*
	 * if capture obtained frameRate is inconsistent, then we'll try to find out
	 * by ourselves
	 */
	if (frameRate <= 0.0)
	{
		/*
		 * If live Video : grab a few images and measure elapsed time
		 */
		if (live)
		{
			QElapsedTimer localTimer;
			localTimer.start();

			for (size_t i=0; i < defaultFrameNumberTest; i++)
			{
				(*capture) >> image;
			}

			frameDelay = (int)(localTimer.elapsed() / defaultFrameNumberTest);
			frameRate = 1000.0/((double)frameDelay);
			qDebug("Measured capture frame rate is  %4.2f images/s", frameRate);
		}
		/*
		 * FIXME else ???
		 * video files read through capture should provide framerate with
		 * capture.get(CV_CAP_PROP_FPS) but what happens if they don't ???
		 */
	}
	else
	{
		qDebug("%s Capture frame rate = %4.2f", message.toStdString().c_str(),
												frameRate);
		frameDelay = 1000/frameRate;
	}

	statusMessage.sprintf("%s frame rate = %4.2f images/s",
						   message.toStdString().c_str(), frameRate);
	emit messageChanged(statusMessage, messageDelay);

	return frameDelay;
}

/*
 * Sets #imageDisplay size according to preferred width and height
 * @param width desired width
 * @param height desired height
 * @pre a first image have been grabbed
 * @post the #messageChanged(QString); signal has been emitted
 */
void QcvTimerVideoCapture::setSize_Impl(const size_t width,
										const size_t height)
{
	if ((updateThread != nullptr))
	{
		if (lockLevel == 0)
		{
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "will lock";
			mutex.lock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "locked";
		}
		lockLevel++;
	}

	unsigned int preferredWidth;
	unsigned int preferredHeight;

	// if not empty then release it
	if (!imageResized.empty())
	{
		imageResized.release();
	}

	if ((width == 0) && (height == 0)) // reset to original size
	{
		if (directResize) // direct set size to original size
		{
			setDirectSize((unsigned int)originalSize.width(),
						  (unsigned int)originalSize.height());
			// image is updated into setDirectSize
		}
		preferredWidth = image.cols;
		preferredHeight = image.rows;

		resized = false;
		imageResized = image;
	}
	else // width != 0 or height != 0
	{
		if ((width == (unsigned int)image.cols) &&
			(height == (unsigned int)image.rows)) // unchanged
		{
			preferredWidth = image.cols;
			preferredHeight = image.rows;
			imageResized = image;

			if (((int)preferredWidth == originalSize.width()) &&
				((int)preferredHeight == originalSize.height()))
			{
				resized = false;
			}
			else
			{
				resized = true;
			}
		}
		else // width or height have changed
		{
			/*
			 * Resize needed
			 */
			preferredWidth = width;
			preferredHeight = height;

			resized = true;

			if (directResize)
			{
				setDirectSize(preferredWidth, preferredHeight);
				imageResized = image;
			}
			else
			{
				imageResized = Mat(preferredHeight, preferredWidth, image.type());
			}
		}
	}

	if (updateThread != nullptr)
	{
		lockLevel--;
		if (lockLevel == 0)
		{
			mutex.unlock();
//			qDebug() << "QcvTimerVideoCapture::" << Q_FUNC_INFO << "unlocked";
		}
	}

	qDebug("QcvVideoCapture resize is %s [%s]",
		   (resized ? "ON" : "OFF"),
		   (directResize ? "direct" : "soft"));

	size.setWidth(preferredWidth);
	size.setHeight(preferredHeight);
	statusMessage.clear();
	statusMessage.sprintf("Size set to %dx%d", preferredWidth, preferredHeight);
	emit messageChanged(statusMessage, messageDelay);

	/*
	 * imageChanged signal is delayed until setGray is called into
	 * setFlipVideo
	 */
	// Refresh image chain
	setFlipped(flipped);
}

// ----------------------------------------------------------------------------
// Protected Slots
// ----------------------------------------------------------------------------

/*
 * update slot trigerred by timer : Grabs a new image and sends updated()
 * signal iff new image has been grabbed, otherwise there is no more
 * images to grab so kills timer
 * @post The following signals are emitted:
 *	- #messageChanged(QString); is emitted when
 *		- There is no more frames to capture
 *		- When capture is restarted (when reading video files) and when
 *		it fails to restart
 *	- #restarted() is emitted when capture restarts
 *	- #finished is emitted if capture restart fails
 *	- #updated(); is emitted if a new image have been grabbed
 */
void QcvTimerVideoCapture::update()
{
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO << "started "
//			 << lockLevel;
	bool locked = true;
	bool image_updated = false;

	if (updateThread != nullptr)
	{
		if (skip)
		{
			locked = mutex.tryLock();
//			qDebug() << QThread::currentThreadId()
//					 << Q_FUNC_INFO << "trying to lock"
//					 << (locked ? "succeeded" : "failed");
			if (locked)
			{
				lockLevel++;
			}
		}
		else
		{
			if (lockLevel == 0)
			{
				mutex.lock();
//				qDebug() << QThread::currentThreadId()
//						 << Q_FUNC_INFO << "locked";
			}
			lockLevel++;
		}
	}

	if (capture->isOpened() && locked)
	{
		(*capture) >> image;

		if (!image.data) // captured image has no data
		{
			statusMessage.clear();

			if (live)
			{
				if (timer->isActive())
				{
					if (updateThread == nullptr ||
						QThread::currentThread() == updateThread)
					{
						timer->stop();
					}
					else
					{
						emit stopTimer();
					}
					qDebug("timer stopped");
				}

				capture->release();

				statusMessage.sprintf("No more frames to capture ...");
				emit messageChanged(statusMessage, 0);
				qDebug("%s", statusMessage.toStdString().c_str());
			}
			else // not live video ==> video file
			{
				// We'll try to rewind the file back to frame 0
				bool restart = capture->set(CV_CAP_PROP_POS_FRAMES, 0.0);

				if (restart)
				{
					statusMessage.sprintf("Capture restarted");
					emit messageChanged(statusMessage,
										QcvTimerVideoCapture::messageDelay);
					emit restarted();
					qDebug("%s", statusMessage.toStdString().c_str());

					// Refresh image chain resized -> flipped -> gray
					QcvVideoCapture::setSize(size);
				}
				else
				{
					capture->release();

					statusMessage.sprintf("Failed to restart capture ...");
					emit messageChanged(statusMessage, 0);
					emit finished();
					qDebug("%s", statusMessage.toStdString().c_str());
				}
			}
		}
		else // capture image has data
		{
			/*
			 * CAUTION
			 * image->imageResized->imageFlipped->imageDisplay
			 * constitute an image chain, so when size is changed with
			 * setSize it should call setFlipVideo which should call
			 * setGray
			 */

			// resize image
			if (resized && !directResize)
			{
				cv::resize(image, imageResized, imageResized.size(), 0, 0,
					INTER_AREA);
			}
			/*
			 * else imageResized.data is already == image.data
			 */

			// flip image horizontally if required
			if (flipped)
			{
				flip(imageResized, imageFlipped, 1);
			}
			/*
			 * else imageFlipped.data is already == imageResized.data
			 */

			// convert image to gray if required
			if (gray)
			{
				cvtColor(imageFlipped, imageDisplay, CV_BGR2GRAY);
			}
			/*
			 * else imageDisplay.data is already == imageFlipped.data
			 */
			image_updated = true;
		}

		// unlock only if lock has been obtained
		if (updateThread != nullptr)
		{
			lockLevel--;
			if (lockLevel == 0)
			{
				mutex.unlock();
//				qDebug() << QThread::currentThreadId()
//						 << Q_FUNC_INFO << "unlocked";
			}
		}

		if (image_updated)
		{
//			qDebug() << Q_FUNC_INFO << "emits updated";
			emit updated();
		}
	}
	else
	{
		// mutex hasn't been locked, so we skipped one capture
		// qDebug() << "Capture skipped an image (level " << lockLevel << ")";
	}
}
