#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>
#include <QCloseEvent>
#include <QSettings>
#include <QFont>
#include <QFontDialog>
#include <QTextCursor>
#include <QTextDocument>
#include <QStatusBar>
#include <QInputDialog>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget * parent) :
	QMainWindow(parent),
	settings("ENSIIE", "QTextPad"),
	ui(new Ui::MainWindow)
{
	nvCount = 1;
	ui->setupUi(this);
	ui->tabWidget->removeTab(0);
	on_actionNouveau_triggered();

	documentPaths.clear();
	documentPaths = settings.value("recentDocuments").toStringList();

    //My codes
    QFont myFont ;
    myFont.fromString(settings.value("setFont").toString());
    currentTextEdit()->setFont(myFont);
    QTextDocument *document = new QTextDocument(currentTextEdit());
    QTextCursor cursor(document);
    int num = cursor.blockNumber();
    QString info = "Ligne : " + QString::number(num);
    ui->statusBar->showMessage(info);


    updateRecentMenu();
	connect(ui->menuDocuments_r_cents, SIGNAL(triggered(QAction *) ),
			this, SLOT(openRecent(QAction *) ));
    //connect(ui->statusBar->showMessage(info), SIGNAL(triggered(QAction *)));
}

MainWindow::~MainWindow()
{
	settings.setValue("recentDocuments", documentPaths);
	delete ui;
}

void MainWindow::on_actionNouveau_triggered()
{
	int ct = ui->tabWidget->addTab(
    new QTextEdit(), tr("Nouveau document ") + QString::number(nvCount++));
	ui->tabWidget->setCurrentIndex(ct);

    //My codes
    QFont myFont ;
    myFont.fromString(settings.value("setFont").toString());
    currentTextEdit()->setFont(myFont);

}

void MainWindow::on_actionCouper_triggered()
{
    currentTextEdit()->cut();
}

void MainWindow::on_actionCopier_triggered()
{
	currentTextEdit()->copy();
}

void MainWindow::on_actionColler_triggered()
{
	currentTextEdit()->paste();
}

void MainWindow::on_actionS_lectionner_tout_triggered()
{
	currentTextEdit()->selectAll();
}

void MainWindow::on_actionD_faire_triggered()
{
	currentTextEdit()->undo();
}

void MainWindow::on_actionRefaire_triggered()
{
	currentTextEdit()->redo();
}

void MainWindow::on_actionEnregistrer_sous_triggered()
{
	QString fileName = QFileDialog::getSaveFileName(
		this, tr("Enregister sous..."), "", tr("Fichiers texte (*.txt)"));
	if (!fileName.isEmpty())
	{
		saveFile(fileName);
	}
}

void MainWindow::on_actionEnregistrer_triggered()
{
	if (currentTextEdit()->property("fileName").isValid())
	{
		saveFile(currentTextEdit()->property("fileName").toString());
	}
	else
	{
		on_actionEnregistrer_sous_triggered();
	}
}

void MainWindow::saveFile(const QString & fileName)
{
	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly))
	{
		manageRecentDocument(fileName);
		QTextStream ts(&file);
		ts << currentTextEdit()->toPlainText();
		ts.flush();
		file.close();
		currentTextEdit()->setProperty("fileName", fileName);
		currentTextEdit()->document()->setModified(false);
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),
								  QFileInfo(fileName).fileName());
	}
	else
		QMessageBox::critical(
			this,
			tr("Echec de sauvegarde"),
			tr("Le fichier ") + fileName + tr(" n'a pas pu être sauvegarde."));
}

inline QTextEdit * MainWindow::currentTextEdit()
{
	return dynamic_cast<QTextEdit *>(ui->tabWidget->currentWidget());
}

void MainWindow::readFile(const QString & fileName)
{
	QFile file(fileName);
	if (file.open(QIODevice::ReadOnly))
	{
		manageRecentDocument(fileName);
		on_actionNouveau_triggered();
		QTextStream ts(&file);
		currentTextEdit()->setPlainText(ts.readAll());
		currentTextEdit()->setProperty("fileName", fileName);
		ui->tabWidget->setTabText(ui->tabWidget->currentIndex(),
								  QFileInfo(fileName).fileName());
		file.close();
	}
}

void MainWindow::updateRecentMenu()
{
	ui->menuDocuments_r_cents->clear();

	for (int i = 0; i < documentPaths.length(); i++)
	{
		if (!QFile::exists(documentPaths[i]))
		{
			documentPaths.removeAll(documentPaths[i]);
			i--;
		}
		else
		{
			ui->menuDocuments_r_cents->addAction(documentPaths[i]);
		}
	}
}

void MainWindow::on_actionOuvrir_triggered()
{
	QString fileName = QFileDialog::getOpenFileName(this,
													tr("Ouvrir un fichier"),
													"",
													tr("Text files (*.txt)"));
	if (!fileName.isEmpty())
	{
		readFile(fileName);
	}
}

void MainWindow::on_actionFermer_triggered()
{
	if (currentTextEdit()->document()->isModified())
	{
		if (QMessageBox::question(this,
								  tr("Modification détectée"),
								  tr("Le fichier en cours d'édition a été "
								  "modifié.\nVoulez-vous le sauvegarder ?"),
								  QMessageBox::Yes,
								  QMessageBox::No) == QMessageBox::Yes)
			on_actionEnregistrer_triggered();
	}
	ui->tabWidget->removeTab(ui->tabWidget->currentIndex());
}

void MainWindow::closeEvent(QCloseEvent * ce)
{
	bool allSaved = true;

	for (int i = 0; i < ui->tabWidget->count(); i++)
	{
		QTextEdit * editor = dynamic_cast<QTextEdit *>(ui->tabWidget->widget(i));
		if (editor != NULL)
		{
			allSaved = allSaved && (!editor->document()->isModified());
		}
	}

	if (!allSaved)
	{
		// demande et sauve les fichiers.
		int button =
			QMessageBox::question(this,
								  tr("Documents modifiés non sauvegardés"),
								  tr("Des fichiers n'ont pas été "
								  "sauvegardés.\nVoulez vous les enregistrer ?"),
								  QMessageBox::NoAll,
								  QMessageBox::YesAll,
								  QMessageBox::Cancel);
		if (button == QMessageBox::Cancel)
		{
			ce->ignore();
			return;
		}
		if (button == QMessageBox::YesAll)
		{
			for (int i = 0; i < ui->tabWidget->count(); i++)
			{
				ui->tabWidget->setCurrentIndex(i);
				if (currentTextEdit()->document()->isModified())
				{
					on_actionEnregistrer_triggered();
				}
			}
		}
	}
	ce->accept();
}

void MainWindow::on_tabWidget_tabCloseRequested(int index)
{
	ui->tabWidget->setCurrentIndex(index);
	on_actionFermer_triggered();
}

void MainWindow::manageRecentDocument(const QString & s)
{
	if (documentPaths.contains(s))
	{
		documentPaths.removeAll(s);
	}

	documentPaths.push_front(s);

	while (documentPaths.length() > 6)
	{
		documentPaths.pop_back();
	}

	settings.setValue("recentDocuments", documentPaths);
	updateRecentMenu();
}

void MainWindow::openRecent(QAction * a)
{
	readFile(a->text());
}

void MainWindow::on_actionR_gler_la_police_triggered()
{
    bool enable = true;
    QFont font = QFontDialog::getFont(&enable,this);
    if(enable) {

        for(int i=0; i < ui->tabWidget->count(); i++) {
            ui->tabWidget->widget(i)->setFont(font);

        }
        currentTextEdit()->setFont(font);
        settings.setValue("setFont", font);
    }else{

        settings.setValue("setFont", font);
    }
}
