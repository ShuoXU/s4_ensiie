/**
 * @brief this class is for defining the filter for treeview, inherit class QSortFilterProxyModel
 */
#ifndef MYFILTER_H
#define MYFILTER_H
#include <QSortFilterProxyModel>

class Myfilter : public QSortFilterProxyModel
{
public:
    /**
     * @brief constructor
     * @param parent
     */
    Myfilter(QObject *parent = nullptr);
protected:
    /**
     * @param sourceRow
     * @param sourceParent
     * @return Returns true if the item in the row indicated by the given source_row and source_parent should be included in the model; otherwise returns false.
     */
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;


};

#endif // MYFILTER_H
