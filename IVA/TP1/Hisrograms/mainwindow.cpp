#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QObject>
#include <QFileDialog>
#include <QWindow>
#include <QDebug>
#include <cassert>
#include <cstdint>	// for converting pointers to intptr_t

#include <Qcv/matWidgets/QcvMatWidgetImage.h>
#include <Qcv/matWidgets/QcvMatWidgetLabel.h>
#include <Qcv/matWidgets/QcvMatWidgetGL.h>

/*
 * MainWindow constructor
 * @param capture the capture QObject to capture frames from devices
 * or video files
 * @param parent parent widget
 */
MainWindow::MainWindow(QcvCapture * capture,
					   QcvHistograms * processor,
					   QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	capture(capture),
	processor(processor),
	preferredWidth(320),
	preferredHeight(240)
{
	ui->setupUi(this);
	ui->scrollArea->setBackgroundRole(QPalette::Mid);

	QActionGroup * renderActionGroup = new QActionGroup(this);
	renderActionGroup->addAction(ui->actionRenderImage);
	renderActionGroup->addAction(ui->actionRenderPixmap);
	renderActionGroup->addAction(ui->actionRenderOpenGL);
	ui->actionRenderImage->setChecked(true);

	// ------------------------------------------------------------------------
	// Assertions
	// ------------------------------------------------------------------------
	assert(capture != nullptr);

	assert(processor != nullptr);

	// ------------------------------------------------------------------------
	// Special widgets initialisation
	// ------------------------------------------------------------------------

	// Replace widgetImage QcvMatWidget instance with QcvMatWidgetImage
	// Sets Source image for widgetImage
	// Connects processor->updated to widgetImage->update
	// Connects processor->imageChanged to widgetImage->setSourceImage
	setupImageWidgets(RenderMode::IMAGE);

	// ------------------------------------------------------------------------
	// Signal/Slot connections
	// ------------------------------------------------------------------------

	// Capture, histogram and this messages to status bar
	connect(capture, SIGNAL(messageChanged(QString,int)),
			ui->statusBar, SLOT(showMessage(QString,int)));

	connect(processor, SIGNAL(sendMessage(QString,int)),
			ui->statusBar, SLOT(showMessage(QString,int)));

	connect(this, SIGNAL(sendMessage(QString,int)),
			ui->statusBar, SLOT(showMessage(QString,int)));

	// Connect UI signals to Capture slots
	connect(this, SIGNAL(sizeChanged(const QSize &)),
			capture, SLOT(setSize(const QSize &))); //, Qt::DirectConnection);
	connect(this, SIGNAL(deviceOpened(int,size_t,size_t)),
			capture, SLOT(openDevice(int,size_t, size_t))); //, Qt::DirectConnection);
	connect(this, SIGNAL(videoOpened(QString,size_t,size_t)),
			capture, SLOT(openFile(QString,size_t,size_t))); //, Qt::DirectConnection);
	connect(this, SIGNAL(directoryOpened(QString,size_t,size_t)),
			capture, SLOT(openDirectory(QString,size_t,size_t))); //, Qt::DirectConnection);
	connect(this, SIGNAL(flipVideo(bool)),
			capture, SLOT(setFlipped(bool))); //, Qt::DirectConnection);
	connect(this, SIGNAL(finished()),
			capture, SLOT(finish()));
	connect(this, SIGNAL(finished()),
			processor, SLOT(finish()));

	// When Processor source image changes, some attributes are reinitialised
	// So we have to set them up again according to current UI values
	connect(processor, SIGNAL(imageChanged()),
			this, SLOT(setupProcessorFromUI()));

	// Time measurement strings connections
	connect(processor, SIGNAL(processTimeUpdated(QString)),
			ui->labelAllTime, SLOT(setText(QString)));
	connect(processor, SIGNAL(histogramTime1Updated(QString)),
			ui->labelUH1Time, SLOT(setText(QString)));
	connect(processor, SIGNAL(histogramTime2Updated(QString)),
			ui->labelUH2Time, SLOT(setText(QString)));
	connect(processor, SIGNAL(drawHistogramTimeUpdated(QString)),
			ui->labelDHTime, SLOT(setText(QString)));
	connect(processor, SIGNAL(computeLUTTimeUpdated(QString)),
			ui->labelCLTime, SLOT(setText(QString)));
	connect(processor, SIGNAL(drawLUTTimeUpdated(QString)),
			ui->labelDLTime, SLOT(setText(QString)));
	connect(processor, SIGNAL(applyLUTTimeUpdated(QString)),
			ui->labelALTime, SLOT(setText(QString)));

	// ------------------------------------------------------------------------
	// UI setup accroding to capture ahd histogram settings
	// ------------------------------------------------------------------------
	setupUIfromCapture();
	setupUIfromProcessor();

	// ------------------------------------------------------------------------
	// Tabs navigation setup
	// ------------------------------------------------------------------------
	const int nbTabs = 4;
	QAction * tabsActions[nbTabs];
	tabsActions[0] = ui->actionTabSize;
	tabsActions[1] = ui->actionTabHist;
	tabsActions[2] = ui->actionTabLUT;
	tabsActions[3] = ui->actionTabTimes;
	QActionGroup * tabsActionsGroup = new QActionGroup(this);
	QSignalMapper * tabsMapper = new QSignalMapper(this);

	for (int i = 0; i < nbTabs; ++i)
	{
		tabsActionsGroup->addAction(tabsActions[i]);
		connect(tabsActions[i], SIGNAL(triggered()),
				tabsMapper, SLOT(map()));
		tabsMapper->setMapping(tabsActions[i], i);
	}

	tabsActions[ui->tabWidget->currentIndex()]->setChecked(true);

	connect(tabsMapper, SIGNAL(mapped(int)),
			ui->tabWidget, SLOT(setCurrentIndex(int)));

	/*
	 * on_tabWidget_currentChanged(int index) will check the corresponding
	 * ui->actionTabXXX when tabs are changed directly
	 */
}

/*
 * MainWindow destructor
 */
MainWindow::~MainWindow()
{
	delete ui;
}

/*
 * Menu action when Sources->camera 0 is selected
 * Sets capture to open device 0. If device is not available
 * menu item is set to inactive.
 */
void MainWindow::on_actionCamera_0_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	qDebug("Opening device 0 ...");
//	if (!capture->open(0, width, height))
//	{
//		qWarning("Unable to open device 0");
//		// disable menu item if camera 0 does not exist
//		ui->actionCamera_0->setDisabled(true);
//	}

	emit deviceOpened(0,
					  static_cast<size_t>(width),
					  static_cast<size_t>(height));
}

/*
 * Menu action when Sources->camera 1 is selected
 * Sets capture to open device 0. If device is not available
 * menu item is set to inactive
 */
void MainWindow::on_actionCamera_1_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	qDebug("Opening device 1 ...");
//	if (!capture->open(1, width, height))
//	{
//		qWarning("Unable to open device 1");
//		// disable menu item if camera 1 does not exist
//		ui->actionCamera_1->setDisabled(true);
//	}

	emit deviceOpened(1,
					  static_cast<size_t>(width),
					  static_cast<size_t>(height));
}

/*
 * Menu action when Sources->file is selected.
 * Opens file dialog and tries to open selected file (if not empty),
 * then sets capture to open the selected file
 */
void MainWindow::on_actionFile_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	QString fileName =
	QFileDialog::getOpenFileName(this,
								 tr("Open Video"),
								 "./",
								 tr("Video Files (*.avi *.mkv *.mp4 *.m4v)"),
								 nullptr,
								 QFileDialog::ReadOnly);

	qDebug("Opening file %s ...", fileName.toStdString().c_str());

	if (fileName.length() > 0)
	{
//		if (!capture->open(fileName, width, height))
//		{
//			qWarning("Unable to open device file : %s",
//					 fileName.toStdString().c_str());
//		}
		emit videoOpened(fileName,
						 static_cast<size_t>(width),
						 static_cast<size_t>(height));
	}
	else
	{
		qWarning("empty file name");
	}
}

void MainWindow::on_actionDirectory_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	QString dirName =
	QFileDialog::getExistingDirectory(this,
									  tr("Open Directory containing images"),
									  "./");

	qDebug("Opening directory %s ...", dirName.toStdString().c_str());

	if (dirName.length() > 0)
	{
		emit directoryOpened(dirName,
							 static_cast<size_t>(width),
							 static_cast<size_t>(height));
	}
	else
	{
		qWarning("empty directoey name");
	}
}

/*
 * Menu action to quit application
 */
void MainWindow::on_actionQuit_triggered()
{
	emit finished();
	this->close();
}

/*
 * Menu action when flip image is selected.
 * Sets capture to change flip status which leads to reverse
 * image horizontally
 */
void MainWindow::on_actionFlip_triggered()
{
	// capture->setFlipVideo(!capture->isFlipVideo());
	emit flipVideo(!capture->isFlipped());
	/*
	 * There is no need to update ui->checkBoxFlip since it is connected
	 * to ui->actionFlip through signals/slots
	 */
}

/*
 * Menu action when original image size is selected.
 * Sets capture not to resize image
 */
void MainWindow::on_actionOriginalSize_triggered()
{
	ui->actionConstrainedSize->setChecked(false);

	emit sizeChanged(QSize(0, 0));
}

/*
 * Menu action when constrained image size is selected.
 * Sets capture resize to preferred width and height
 */
void MainWindow::on_actionConstrainedSize_triggered()
{
	ui->actionOriginalSize->setChecked(false);

	emit sizeChanged(QSize(preferredWidth, preferredHeight));
}

/*
 * Changes widgetImage nature according to desired rendering mode.
 * Possible values for mode are:
 * 	- IMAGE: widgetImage is assigned to a QcvMatWidgetImage instance
 * 	- PIXMAP: widgetImage is assigned to a QcvMatWidgetLabel instance
 * 	- GL: widgetImage is assigned to a QcvMatWidgetGL instance
 * @param mode
 */
void MainWindow::setupImageWidgets(const RenderMode mode)
{
	setupAnImageWidget(ui->widgetImage,
					   ui->scrollArea,
					   mode);
	setupAnImageWidget(ui->widgetLUT,
					   ui->groupBoxLUT,
					   mode);
	setupAnImageWidget(ui->widgetHistogram,
					   ui->groupBoxHistogram,
					   mode);
}


/*
 * Change a widget image nature according to the desired rendering mode
 * @param matWidget the QcvMatWidget to change to either
 *	- QcvMatWidgetImage
 *	- QcvMatWidgetLabel
 *	- QcvMatWidgetGL
 * @param parent the parent widget so matWiget can be removed from it
 * recreated and reattached to it
 * @param mode the rendering mode for the image widget
 */
void MainWindow::setupAnImageWidget(QcvMatWidget * matWidget,
									QWidget * parent,
									const RenderMode mode)
{
	enum struct ImageType : int
	{
		OUTPUT,		// When matWidget is for output image
		LUT,		// When matWidget is for LUT display
		HISTOGRAM,	// When matWidget is for histogram display
		UNKOWN
	};

	ImageType type;

	if (matWidget == ui->widgetImage)
	{
		type = ImageType::OUTPUT;
	}
	else if (matWidget == ui->widgetLUT)
	{
		type = ImageType::LUT;
	}
	else if (matWidget == ui->widgetHistogram)
	{
		type = ImageType::HISTOGRAM;
	}
	else
	{
		type = ImageType::UNKOWN;
		qWarning() << "setupAnImageWidget unkown image widget: "
				   << matWidget->objectName();
		return;
	}

	enum struct ParentType : int
	{
		SCROLL,		// When Parent is a QScrollArea
		GROUP,		// When Parent is a QGroupBox containing a QLayout
		UNKOWN
	};

	ParentType parentType;

	QScrollArea * parentScrollArea = nullptr;
	QGroupBox * parentGroupBox = nullptr;
	QLayout * parentLayout = nullptr;

	if ((parentScrollArea = dynamic_cast<QScrollArea *>(parent)) != nullptr)
	{
		parentType = ParentType::SCROLL;
	}
	else
	{
		if ((parentGroupBox = dynamic_cast<QGroupBox *>(parent)) != nullptr)
		{
			parentType = ParentType::GROUP;
			if ((parentLayout = parentGroupBox->layout()) == nullptr)
			{
				qWarning() << "setupAnImageWidget QGroupBox "
						   << parentGroupBox->objectName() << "has no layout";
				return;
			}
		}
		else
		{
			parentType = ParentType::UNKOWN;
			qWarning() << "setupAnImageWidget Unkown parent type: "
					   << parent->objectName();
			return;
		}
	}

	/*
	 * From now on:
	 *	- type is {OUTPUT | LUT | HISTOGRAM} but not UNKOWN_IMAGE_TYPE
	 *	- parentType is {SCROLL | GROUP} but not UNKOWN_PARENT_TYPE
	 *	- if parentType is SCROLL then
	 *		- parentScrollArea is not nullptr (parentGroupBox & parentLayout are)
	 *	- if parentType is GROUP then
	 *		- parentGroupBox & parentLayout are not nullptr (parentScrollArea is)
	 */
	QWindow * currentWindow = windowHandle();

	/*
	 * old widgetImage from ui->scrollArea ui->groupBoxHistogram or ui->groupBoxLUT
	 */
	QWidget * oldWidget = nullptr; // ui->scrollArea->takeWidget();
	switch (parentType)
	{
		case ParentType::SCROLL: // type is implicitly OUTPUT
			oldWidget = ui->scrollArea->takeWidget();
			break;
		case ParentType::GROUP:
			switch (type)
			{
				case ImageType::LUT:
					oldWidget = ui->widgetLUT;
					break;
				case ImageType::HISTOGRAM:
					oldWidget = ui->widgetHistogram;
					break;
				default:
					break;
			}
			parentLayout->removeWidget(oldWidget);
			break;
		default:
			break;
	}

	QcvMatWidget * oldImageWidget = dynamic_cast<QcvMatWidget *>(oldWidget);

	// delete removed widget
	if (oldImageWidget != nullptr)
	{
		// Disconnect first
		switch (type)
		{
			case ImageType::OUTPUT:
				disconnect(processor, SIGNAL(updated()),
						   oldImageWidget, SLOT(update()));
				disconnect(processor, SIGNAL(imageChanged(Mat*)),
						   oldImageWidget, SLOT(setSourceImage(Mat*)));
				break;
			case ImageType::LUT:
				disconnect(processor, SIGNAL(lutImageUpdated()),
						   oldImageWidget, SLOT(update()));
				disconnect(processor, SIGNAL(lutImageChanged(Mat*)),
						   oldImageWidget, SLOT(setSourceImage(Mat*)));
				break;
			case ImageType::HISTOGRAM:
				disconnect(processor, SIGNAL(histogramImageUpdated()),
						   oldImageWidget, SLOT(update()));
				disconnect(processor, SIGNAL(histogramImageChanged(Mat*)),
						   oldImageWidget, SLOT(setSourceImage(Mat*)));
				break;
			default:
				break;
		}

		if (mode == RenderMode::GL)
		{
			disconnect(currentWindow, SIGNAL(screenChanged(QScreen*)),
					   oldImageWidget, SLOT(screenChanged()));
		}

		delete oldImageWidget;
	}

	// Get the corresponding image for the new widget
	Mat * image = nullptr;
	QMutex * imageLock = nullptr;
	switch (type) {
		case ImageType::OUTPUT:
			image = processor->getDisplayImagePtr();
			imageLock = processor->getLock(QcvHistograms::TRANSFORMED_IM);
			break;
		case ImageType::LUT:
			image = processor->getLUTImagePtr();
			imageLock = processor->getLock(QcvHistograms::LUT_IM);
			break;
		case ImageType::HISTOGRAM:
			image = processor->getHistImagePtr();
			imageLock = processor->getLock(QcvHistograms::HIST_IM);
			break;
		default:
			break;
	}
	if (image == nullptr)
	{
		qFatal("setupAnImageWidget: Null out image");
	}
	if (image->data == nullptr)
	{
		qFatal("setupAnImageWidget: image out NULL data");
	}

	// Create new widget
	QcvMatWidget * newImageWidget = nullptr;
	switch (mode)
	{
		case RenderMode::PIXMAP:
			newImageWidget = new QcvMatWidgetLabel(image, parent, imageLock);
			break;
		case RenderMode::GL:
			newImageWidget = new QcvMatWidgetGL(image, parent, imageLock);
			break;
		case RenderMode::IMAGE:
		default:
			newImageWidget = new QcvMatWidgetImage(image, parent, imageLock);
			break;
	}

	if (newImageWidget != nullptr)
	{
		QSizePolicy expandingPolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		expandingPolicy.setHorizontalStretch(0);
		expandingPolicy.setVerticalStretch(0);
		QSizePolicy fixedPolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		fixedPolicy.setHorizontalStretch(0);
		fixedPolicy.setVerticalStretch(0);

		newImageWidget->setObjectName(QStringLiteral("widgetImage"));

		// add it to its parent
		switch (parentType)
		{
			case ParentType::SCROLL:
				ui->widgetImage = newImageWidget;
				parentScrollArea->setWidget(ui->widgetImage);
				break;
			case ParentType::GROUP:
				switch (type)
				{
					case ImageType::LUT:
						ui->widgetLUT = newImageWidget;
						fixedPolicy.setHeightForWidth(ui->widgetLUT->sizePolicy().hasHeightForWidth());
						ui->widgetLUT->setSizePolicy(fixedPolicy);
						ui->widgetLUT->setMinimumSize(QSize(100, 100));
						parentLayout->addWidget(ui->widgetLUT);
						break;
					case ImageType::HISTOGRAM:
						ui->widgetHistogram = newImageWidget;
						expandingPolicy.setHeightForWidth(ui->widgetHistogram->sizePolicy().hasHeightForWidth());
						ui->widgetHistogram->setSizePolicy(expandingPolicy);
						ui->widgetHistogram->setMinimumSize(QSize(512, 100));
						parentLayout->addWidget(ui->widgetHistogram);
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}

		// reconnect to processor
		switch (type)
		{
			case ImageType::OUTPUT:
				connect(processor, SIGNAL(updated()),
						newImageWidget, SLOT(update()));
				connect(processor, SIGNAL(imageChanged(Mat*)),
						newImageWidget, SLOT(setSourceImage(Mat*)));
				break;
			case ImageType::LUT:
				connect(processor, SIGNAL(lutImageUpdated()),
						newImageWidget, SLOT(update()));
				connect(processor, SIGNAL(lutImageChanged(Mat*)),
						newImageWidget, SLOT(setSourceImage(Mat*)));
				break;
			case ImageType::HISTOGRAM:
				connect(processor, SIGNAL(histogramImageUpdated()),
						newImageWidget, SLOT(update()));
				connect(processor, SIGNAL(histogramImageChanged(Mat*)),
						newImageWidget, SLOT(setSourceImage(Mat*)));
				break;
			default:
				break;
		}
		if (mode == RenderMode::GL)
		{
			// reconnect to window screen change
			connect(currentWindow, SIGNAL(screenChanged(QScreen *)),
					newImageWidget, SLOT(screenChanged()));
		}
		// Sends message to status bar and sets menu checks
		message.clear();
		message.append(tr("Render more for "));
		switch (type)
		{
			case ImageType::OUTPUT:
				message.append(tr("Output"));
				break;
			case ImageType::LUT:
				message.append(tr("LUT"));
				break;
			case ImageType::HISTOGRAM:
				message.append(tr("Histogram"));
				break;
			default:
				break;
		}
		message.append(tr(" image set to "));
		switch (mode)
		{
			case RenderMode::IMAGE:
				message.append(tr("QImage"));
				break;
			case RenderMode::PIXMAP:
				message.append(tr("QPixmap in QLabel"));
				break;
			case RenderMode::GL:
				message.append("QGLWidget");
				break;
			default:
			break;
		}
		emit sendMessage(message, 5000);
	}
	else
	{
		qDebug("setupAnImageWidget new widget is null");
	}
}


/*
 * Setup UI from capture settings when launching application
 */
void MainWindow::setupUIfromCapture()
{
	// ------------------------------------------------------------------------
	// UI setup according to capture options
	// ------------------------------------------------------------------------
	// Sets size radioButton states
	if (capture->isResized())
	{
		/*
		 * Initial Size radio buttons configuration
		 */
		ui->radioButtonOrigSize->setChecked(false);
		ui->radioButtonCustomSize->setChecked(true);
		/*
		 * Initial Size menu items configuration
		 */
		ui->actionOriginalSize->setChecked(false);
		ui->actionConstrainedSize->setChecked(true);

		QSize size = capture->getSize();
		qDebug("Capture->size is %dx%d", size.width(), size.height());
		preferredWidth = size.width();
		preferredHeight = size.height();
	}
	else
	{
		/*
		 * Initial Size radio buttons configuration
		 */
		ui->radioButtonCustomSize->setChecked(false);
		ui->radioButtonOrigSize->setChecked(true);

		/*
		 * Initial Size menu items configuration
		 */
		ui->actionConstrainedSize->setChecked(false);
		ui->actionOriginalSize->setChecked(true);
	}

	// Sets spinboxes preferred size
	ui->spinBoxWidth->setValue(preferredWidth);
	ui->spinBoxHeight->setValue(preferredHeight);

	// Sets flipCheckbox and menu item states
	bool flipped = capture->isFlipped();
	ui->actionFlip->setChecked(flipped);
	ui->checkBoxFlip->setChecked(flipped);
}

/*
 * Setup UI from processor settings when launching application
 */
void MainWindow::setupUIfromProcessor()
{
	qDebug("Setting up UI from processor");

	/*
	 * Setup UI state from processor values
	 */
	QBoolController * bController = processor->getRedChannelController();
	bController->setupAbstractButton(ui->checkBoxHistRed);
	bController->setupAction(ui->actionShowRedHistogram);

	bController = processor->getGreenChannelController();
	bController->setupAbstractButton(ui->checkBoxHistGreen);
	bController->setupAction(ui->actionShowGreenHistogram);

	bController = processor->getBlueChannelController();
	bController->setupAbstractButton(ui->checkBoxHistBlue);
	bController->setupAction(ui->actionShowBlueHistogram);

	bController = processor->getGrayChannelController();
	bController->setupAbstractButton(ui->checkBoxHistGray);
	bController->setupAction(ui->actionShowGrayHistogram);

	QEnumController * eController = processor->getHistModeController();
	eController->setupRadioButtons(ui->groupBoxHistMode);
	eController->setupMenu(ui->menuModes);

	eController = processor->getLUTController();
	eController->setupRadioButtons(ui->lutBox);
	eController->setupMenu(ui->menuLUT);

	bController = processor->getColorLUTController();
	bController->setupAbstractButton(ui->checkBoxUseColors);
	bController->setupAction(ui->actionLUTUse_Colors);

	eController->enableWith(bController, CvHistograms8UC3::LUTType::THRESHOLD);
	eController->enableWith(bController, CvHistograms8UC3::LUTType::DYNAMIC);
	eController->enableWith(bController, CvHistograms8UC3::LUTType::EQUALIZE);
	eController->enableWith(bController, CvHistograms8UC3::LUTType::CLAHE);

	eController->enableWith(ui->percentBox, CvHistograms8UC3::LUTType::GAMMA);
	eController->enableWith(ui->percentBox, CvHistograms8UC3::LUTType::THRESHOLD);
	eController->enableWith(ui->percentBox, CvHistograms8UC3::LUTType::DYNAMIC);

	QEnumController * eController2 = processor->getDynamicTypeController();
	eController2->setupComboBox(ui->comboBoxDynamicType);

	eController->enableWith(eController2, CvHistograms8UC3::LUTType::DYNAMIC);
	eController->enableWith(ui->groupBoxClahe, CvHistograms8UC3::LUTType::CLAHE);

	QRangeIntController * riController = processor->getLUTParamController();
	riController->setupSpinBox(ui->spinBoxlutParam);
	riController->setupSlider(ui->sliderlutParam);

	QRangeDoubleController * rdController = processor->getCLipLimitController();
	rdController->setupSpinBox(ui->doubleSpinBoxClipLimit);
	rdController->setupSlider(ui->horizontalSliderClipLimit);

	riController = processor->getTilingController();
	riController->setupSpinBox(ui->spinBoxTileSize);
	riController->setupSlider(ui->horizontalSliderTileSize);

	bController = processor->getPostProcessingController();
	bController->setupSource(ui->checkBoxPostFiltering);

	riController = processor->getKernelSizeController();
	riController->setupSpinBox(ui->spinBoxKernelSize);
	riController->setupSlider(ui->horizontalSliderKernelSize);
}

/*
 * Re setup processor from UI settings when source image changes
 */
void MainWindow::setupProcessorFromUI()
{
	qDebug("Setting up processor from UI");

	// Sets histogram channel visibility
	processor->setShowComponent(integral(CvHistograms8UC3::ColorIdx::HIST_RED),
								ui->checkBoxHistRed->isChecked());
	processor->setShowComponent(integral(CvHistograms8UC3::ColorIdx::HIST_GREEN),
								ui->checkBoxHistGreen->isChecked());
	processor->setShowComponent(integral(CvHistograms8UC3::ColorIdx::HIST_BLUE),
								ui->checkBoxHistBlue->isChecked());
	if (processor->getNbHistograms() >= integral(CvHistograms8UC3::ColorIdx::HIST_GRAY))
	{
		processor->setShowComponent(integral(CvHistograms8UC3::ColorIdx::HIST_GRAY),
									ui->checkBoxHistGray->isChecked());
	}

	// Sets Histogram mode
	if (ui->radioButtonHMNormal->isChecked())
	{
		processor->setHistogramMode(CvHistograms8UC3::HistMode::NORMAL);
	}
	else if (ui->radioButtonHMCumulative->isChecked())
	{
		processor->setHistogramMode(CvHistograms8UC3::HistMode::CUMULATIVE);
	}
	else
	{
		processor->setHistogramMode(CvHistograms8UC3::HistMode::TIME_CUMULATIVE);
	}

	processor->setLUTParam(ui->spinBoxlutParam->value());

	processor->setColorLUT(ui->checkBoxUseColors->isChecked());

	if (ui->radioButtonIdentity->isChecked())
	{
		processor->setLutType(integral(CvHistograms8UC3::LUTType::NONE));
	}
	if (ui->radioButtonInverse->isChecked())
	{
		processor->setLutType(integral(CvHistograms8UC3::LUTType::NEGATIVE));
	}
	if (ui->radioButtonGamma->isChecked())
	{
		processor->setLutType(integral(CvHistograms8UC3::LUTType::GAMMA));
	}
	if (ui->radioButtonThreshold->isChecked())
	{
		processor->setLutType(integral(CvHistograms8UC3::LUTType::THRESHOLD));
		processor->setColorLUT(!ui->checkBoxUseColors->isChecked());
	}
	if (ui->radioButtonDynamic->isChecked())
	{
		processor->setLutType(integral(CvHistograms8UC3::LUTType::DYNAMIC));
	}
	if (ui->radioButtonEqualize->isChecked())
	{
		processor->setLutType(integral(CvHistograms8UC3::LUTType::EQUALIZE));
	}
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetImage instance.
 */
void MainWindow::on_actionRenderImage_triggered()
{
	setupImageWidgets(RenderMode::IMAGE);
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetLabel with pixmap instance.
 */
void MainWindow::on_actionRenderPixmap_triggered()
{
	setupImageWidgets(RenderMode::PIXMAP);
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetGL instance.
 */
void MainWindow::on_actionRenderOpenGL_triggered()
{
	setupImageWidgets(RenderMode::GL);
}

/*
 * Original size radioButton action.
 * Sets capture resize to off
 */
void MainWindow::on_radioButtonOrigSize_clicked()
{
	ui->actionConstrainedSize->setChecked(false);
	emit sizeChanged(QSize(0, 0));
}

/*
 * Custom size radioButton action.
 * Sets capture resize to preferred width and height
 */
void MainWindow::on_radioButtonCustomSize_clicked()
{
	ui->actionOriginalSize->setChecked(false);
	emit sizeChanged(QSize(preferredWidth, preferredHeight));
}

/*
 * Width spinbox value change.
 * Changes the preferred width and if custom size is selected apply
 * this custom width
 * @param value the desired width
 */
void MainWindow::on_spinBoxWidth_valueChanged(int value)
{
	preferredWidth = value;
	if (ui->radioButtonCustomSize->isChecked())
	{
		emit sizeChanged(QSize(preferredWidth, preferredHeight));
	}
}

/*
 * Height spinbox value change.
 * Changes the preferred height and if custom size is selected apply
 * this custom height
 * @param value the desired height
 */
void MainWindow::on_spinBoxHeight_valueChanged(int value)
{
	preferredHeight = value;
	if (ui->radioButtonCustomSize->isChecked())
	{
		emit sizeChanged(QSize(preferredWidth, preferredHeight));
	}
}

/*
 * Flip capture image horizontally.
 * changes capture flip status
 */
void MainWindow::on_checkBoxFlip_clicked()
{
	/*
	 * There is no need to update ui->actionFlip since it is connected
	 * to ui->checkBoxFlip through signals/slots
	 */
	// capture->setFlipVideo(ui->checkBoxFlip->isChecked());
	emit flipVideo(ui->checkBoxFlip->isChecked());
}

/*
 * Select one of the actionTabXXX according to the selected index in
 * tabWidget
 * @param index the index of the selected tab
 */
void MainWindow::on_tabWidget_currentChanged(int index)
{
	switch (index)
	{
		case 0:
			ui->actionTabSize->setChecked(true);
			break;
		case 1:
			ui->actionTabHist->setChecked(true);
			break;
		case 2:
			ui->actionTabLUT->setChecked(true);
			break;
		case 3:
			ui->actionTabTimes->setChecked(true);
			break;
		default:
			break;
	}
}
