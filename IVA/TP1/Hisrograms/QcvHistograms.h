/*
 * QcvHistograms.h
 *
 *  Created on: 14 févr. 2012
 *      Author: davidroussel
 */

#ifndef QCVHISTOGRAMS_H_
#define QCVHISTOGRAMS_H_

#include <QString>
#include <QMutex>

#include <Qcv/QcvProcessor.h>
#include <Qcv/controllers/QBoolController.h>
#include <Qcv/controllers/QEnumController.h>
#include <Qcv/controllers/QRangeIntController.h>
#include <Qcv/controllers/QRangeDoubleController.h>

#include "CvHistograms.h"

/**
 * Defines type for Histograms of 8 bits and 3 channels images.
 * @note this is because QObjects subclasses can NOT be templates,
 * so QcvHistograms should inherit CvHistograms<uchar,3> rather than
 * CvHistograms<T,channels>
 */
typedef CvHistograms<uchar,3> CvHistograms8UC3;

/**
 * OpenCV Color Image Histogram processing class with QT flavor
 */
class QcvHistograms: public QcvProcessor, public CvHistograms8UC3
{
	Q_OBJECT

	public:

		/**
		 * Index used to access locks in #locks
		 * @note This Enum should be overloaded in subclasses if number of
		 * mutexes changes
		 */
		enum LockIndex : size_t
		{
			SOURCE = 0,
			HIST_COMPUTE_1 = 1,
			LUT_COMPUTE = 2,
			HIST_COMPUTE_2 = 3,
			TRANSFORMED_IM = 4,
			LUT_IM = 5,
			HIST_IM = 6,
			TERMINATE = 7,
			NB_LOCKS = 8
		};

	protected:

		/**
		 * String containing update histogram formatted time
		 */
		QString updateHistogramTime1String;

		/**
		 * String containing formatted LUT computing time
		 */
		QString computeLUTTimeString;

		/**
		 * String containing formatted LUT drawng time
		 */
		QString drawLUTTimeString;

		/**
		 * String containing formatted LUT apply time
		 */
		QString applyLUTTimeString;

		/**
		 * String containing formatted histogram update time after
		 * LUT applied
		 */
		QString updateHistogramTime2String;

		/**
		 * String containing formatted histogram drawing time
		 */
		QString drawHistogramTimeString;

		/**
		 * Controller for showing red channel in histogram and LUT images
		 */
		QBoolController redChannelController;

		/**
		 * Controller for showing green channel in histogram and LUT images
		 */
		QBoolController greenChannelController;

		/**
		 * Controller for blue channel in histogram
		 */
		QBoolController blueChannelController;

		/**
		 * Controller for gray channel in histogram
		 */
		QBoolController grayChannelController;

		/**
		 * Names for histograms modes
		 */
		QStringList histogramsModeNames = {
			"Normal",
			tr("Cumulative"),
			tr("Time Cumul.")
		};

		/**
		 * Controller for histograms modes #histogramMode
		 */
		QEnumController histModeController;

		/**
		 * Names for Transfert functions
		 */
		QStringList lutNames = {
			tr("Identity"),
			"Inverse",
			"Gamma",
			tr("Threshold"),
			tr("Dynamic Range"),
			tr("Histogram Equalization (HE)"),
			tr("Contrast Limited Adaptive HE")
		};

		/**
		 * Controller for #lutType
		 */
		QEnumController lutController;

		/**
		 * Names for Optimal dynamic
		 */
		QStringList dynNames = {
			tr("Low"),
			tr("High"),
			tr("Low + High")
		};

		/**
		 * Controller for #dynamicType
		 */
		QEnumController dynamicTypeController;

		/**
		 * Controller for #colorLUT
		 */
		QBoolController colorLUTController;

		/**
		 * Controller for #rLutParam
		 */
		QRangeIntController lutParamController;

		/**
		 * Controller for #claheClipLimit
		 */
		QRangeDoubleController clipLimitController;

		/**
		 * Controller for #claheTiling
		 */
		QRangeIntController tilingController;

		/**
		 * Controller for post processing flag
		 */
		QBoolController postProcessingController;

		/**
		 * Controller for post processing filter kernel size
		 */
		QRangeIntController kernelSizeController;

	public:

		/**
		 * QcvHistograms constructor
		 * @param image the source image
		 * @param computeGray indicates if an aditionnal gray level histogram
		 * should be computed
		 * @param drawHeight histogram drawing height
		 * @param drawWidth histogram drawing width
		 * @param timeCumulation indicates if timecumulation is on for histogram
		 * computation
		 * @param imageLock the mutex for concurrent access to the source image.
		 * In order to avoid concurrent access to the same image
		 * @param updateThread the thread in which this processor should run
		 * @param parent parent QObject
		 * @param settings the settings used to load or save parameters value
		 */
		QcvHistograms(Mat * image,
					  QMutex * imageLock = nullptr,
					  QThread * updateThread = nullptr,
					  const bool computeGray = true,
					  const size_t drawHeight = 256,
					  const size_t drawWidth = 512,
					  const bool timeCumulation = false,
					  QObject * parent = nullptr,
					  QSettings * settings = nullptr);

		/**
		 * QImageHistogram destructor
		 */
		virtual ~QcvHistograms();

		/**
		 * Red Channel controller accessor
		 * @return a pointer to the Red Channel Controller
		 */
		QBoolController * getRedChannelController();

		/**
		 * Green Channel controller accessor
		 * @return a pointer to the Green Channel Controller
		 */
		QBoolController * getGreenChannelController();

		/**
		 * Blue Channel controller accessor
		 * @return a pointer to the Blue Channel Controller
		 */
		QBoolController * getBlueChannelController();

		/**
		 * Gray Channel controller accessor
		 * @return a pointer to the Gray Channel Controller
		 */
		QBoolController * getGrayChannelController();

		/**
		 * Hist. Mode controller accessor
		 * @return a pointer to the Hist. Mode Controller
		 */
		QEnumController * getHistModeController();

		/**
		 * LUT Type controller accessor
		 * @return a pointer to the Hist. Mode Controller
		 */
		QEnumController * getLUTController();

		/**
		 * Optimal Dynamic Type controller accessor
		 * @return a pointer to the Optimal Dynamic type controller
		 */
		QEnumController * getDynamicTypeController();

		/**
		 * Color LUT controller accessor
		 * @return a pointer to the Gray Channel Controller
		 */
		QBoolController * getColorLUTController();

		/**
		 * LUT Parameter controller accessor
		 * @return a pointer to the LUT Paramater Controller
		 */
		QRangeIntController * getLUTParamController();

		/**
		 * CLAHE Clip Limt controller
		 * @return a pointer to the CLAHE Clip limit controller
		 */
		QRangeDoubleController * getCLipLimitController();

		/**
		 * CLAHE Tile Size controller
		 * @return a pointer to the CLAHE Tile Size controller
		 */
		QRangeIntController * getTilingController();

		/**
		 * Post processing controller
		 * @return a pointer to the post processing controller
		 */
		QBoolController * getPostProcessingController();

		/**
		 * Post processing filter size controller
		 * @return a pointer to the post processing filter size controller
		 */
		QRangeIntController * getKernelSizeController();

	public slots:
		/**
		 * Update computed images slot and sends displayImageChanged signal if
		 * required.
		 */
		void update();

		/**
		 * Changes source image slot.
		 * Attributes needs to be cleaned up then set up again
		 * @param image the new source Image
		 */
		void setSourceImage(Mat * image) throw (CvProcessorException);

//		/**
//		 * Time cumulative histogram setting with notification
//		 * @param value the value to set for time cumulative status
//		 */
//		void setTimeCumulative(const bool value);

//		/**
//		 * Cumulative histogram status setting with notification
//		 * @param value the value to set for cumulative status
//		 */
//		void setCumulative(const bool value);

		/**
		 * Ith histogram component show setting with notifications
		 * @param i the ith histogram component
		 * @param value the value to set for this component show status
		 */
		void setShowComponent(const size_t i, const bool value);

		/**
		 * Current LUT setting with notification
		 * @param lutType the new LUT type
		 */
		void setLutType(const int lutType);

		/**
		 * Current Optimal Dynamic type with notification
		 * @param dynamicType the new Optimal Dynamic type
		 */
		void setDynamicType(const int dynamicType);

		/**
		 * Reset mean and std process time SLOT in order to re-start computing
		 * new mean and std process time values.
		 */
		void resetMeanProcessTime();

	signals:

		/**
		 * Signal sent when update is completed AND histogram image changes
		 */
		void histogramImageUpdated();

		/**
		 * Signal sent when histogram image has been reallocated
		 * @param image the new histogram image
		 */
		void histogramImageChanged(Mat * image);

		/**
		 * Signal sent when update is completed AND LUT image changes
		 */
		void lutImageUpdated();

		/**
		 * Signal sent when lut image has been reallocated;
		 * @param image the new LUT image
		 */
		void lutImageChanged(Mat * image);

		/**
		 * Signal emitted when histogram is updated with a new image
		 * @param formattedValue string containing the formatted time value
		 */
		void histogramTime1Updated(const QString & formattedValue);

		/**
		 * Signal emitted when LUT is computed
		 * @param formattedValue string containing the formatted time value
		 */
		void computeLUTTimeUpdated(const QString & formattedValue);

		/**
		 * Signal emitted when LUT is drawn
		 * @param formattedValue string containing the formatted time value
		 */
		void drawLUTTimeUpdated(const QString & formattedValue);

		/**
		 * Signal emitted when LUT is applied on image
		 * @param formattedValue string containing the formatted time value
		 */
		void applyLUTTimeUpdated(const QString & formattedValue);

		/**
		 * Signal emitted when histogram is updated after LUT has been applied
		 * @param formattedValue string containing the formatted time value
		 */
		void histogramTime2Updated(const QString & formattedValue);

		/**
		 * Signal emitted when histogram is drawn
		 * @param formattedValue string containing the formatted time value
		 */
		void drawHistogramTimeUpdated(const QString & formattedValue);
};

#endif /* QCVHISTOGRAMS_H_ */
