/****************************************************************************
** Meta object code from reading C++ file 'QcvThreadedVideoCapture.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "Qcv/capture/QcvThreadedVideoCapture.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QcvThreadedVideoCapture.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QcvThreadedVideoCapture_t {
    QByteArrayData data[18];
    char stringdata0[172];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QcvThreadedVideoCapture_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QcvThreadedVideoCapture_t qt_meta_stringdata_QcvThreadedVideoCapture = {
    {
QT_MOC_LITERAL(0, 0, 23), // "QcvThreadedVideoCapture"
QT_MOC_LITERAL(1, 24, 10), // "openDevice"
QT_MOC_LITERAL(2, 35, 0), // ""
QT_MOC_LITERAL(3, 36, 8), // "deviceId"
QT_MOC_LITERAL(4, 45, 6), // "size_t"
QT_MOC_LITERAL(5, 52, 5), // "width"
QT_MOC_LITERAL(6, 58, 6), // "height"
QT_MOC_LITERAL(7, 65, 8), // "openFile"
QT_MOC_LITERAL(8, 74, 8), // "fileName"
QT_MOC_LITERAL(9, 83, 13), // "openDirectory"
QT_MOC_LITERAL(10, 97, 7), // "dirName"
QT_MOC_LITERAL(11, 105, 7), // "setGray"
QT_MOC_LITERAL(12, 113, 5), // "value"
QT_MOC_LITERAL(13, 119, 15), // "setSynchronized"
QT_MOC_LITERAL(14, 135, 6), // "finish"
QT_MOC_LITERAL(15, 142, 12), // "setFrameRate"
QT_MOC_LITERAL(16, 155, 9), // "frameRate"
QT_MOC_LITERAL(17, 165, 6) // "update"

    },
    "QcvThreadedVideoCapture\0openDevice\0\0"
    "deviceId\0size_t\0width\0height\0openFile\0"
    "fileName\0openDirectory\0dirName\0setGray\0"
    "value\0setSynchronized\0finish\0setFrameRate\0"
    "frameRate\0update"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QcvThreadedVideoCapture[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    3,   84,    2, 0x0a /* Public */,
       1,    2,   91,    2, 0x2a /* Public | MethodCloned */,
       1,    1,   96,    2, 0x2a /* Public | MethodCloned */,
       7,    3,   99,    2, 0x0a /* Public */,
       7,    2,  106,    2, 0x2a /* Public | MethodCloned */,
       7,    1,  111,    2, 0x2a /* Public | MethodCloned */,
       9,    3,  114,    2, 0x0a /* Public */,
       9,    2,  121,    2, 0x2a /* Public | MethodCloned */,
       9,    1,  126,    2, 0x2a /* Public | MethodCloned */,
      11,    1,  129,    2, 0x0a /* Public */,
      13,    1,  132,    2, 0x0a /* Public */,
      14,    0,  135,    2, 0x0a /* Public */,
      15,    1,  136,    2, 0x0a /* Public */,
      17,    0,  139,    2, 0x09 /* Protected */,

 // slots: parameters
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 4, 0x80000000 | 4,    3,    5,    6,
    QMetaType::Bool, QMetaType::Int, 0x80000000 | 4,    3,    5,
    QMetaType::Bool, QMetaType::Int,    3,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 4, 0x80000000 | 4,    8,    5,    6,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 4,    8,    5,
    QMetaType::Bool, QMetaType::QString,    8,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 4, 0x80000000 | 4,   10,    5,    6,
    QMetaType::Bool, QMetaType::QString, 0x80000000 | 4,   10,    5,
    QMetaType::Bool, QMetaType::QString,   10,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void, QMetaType::Bool,   12,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Double,   16,
    QMetaType::Bool,

       0        // eod
};

void QcvThreadedVideoCapture::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QcvThreadedVideoCapture *_t = static_cast<QcvThreadedVideoCapture *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 1: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 2: { bool _r = _t->openDevice((*reinterpret_cast< const int(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 3: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 5: { bool _r = _t->openFile((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 6: { bool _r = _t->openDirectory((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])),(*reinterpret_cast< const size_t(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 7: { bool _r = _t->openDirectory((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const size_t(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 8: { bool _r = _t->openDirectory((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 9: _t->setGray((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 10: _t->setSynchronized((*reinterpret_cast< const bool(*)>(_a[1]))); break;
        case 11: _t->finish(); break;
        case 12: _t->setFrameRate((*reinterpret_cast< const double(*)>(_a[1]))); break;
        case 13: { bool _r = _t->update();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    }
}

const QMetaObject QcvThreadedVideoCapture::staticMetaObject = {
    { &QcvVideoCapture::staticMetaObject, qt_meta_stringdata_QcvThreadedVideoCapture.data,
      qt_meta_data_QcvThreadedVideoCapture,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QcvThreadedVideoCapture::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QcvThreadedVideoCapture::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QcvThreadedVideoCapture.stringdata0))
        return static_cast<void*>(this);
    return QcvVideoCapture::qt_metacast(_clname);
}

int QcvThreadedVideoCapture::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QcvVideoCapture::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
