/****************************************************************************
** Meta object code from reading C++ file 'QcvHistograms.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "QcvHistograms.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QcvHistograms.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QcvHistograms_t {
    QByteArrayData data[26];
    char stringdata0[368];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QcvHistograms_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QcvHistograms_t qt_meta_stringdata_QcvHistograms = {
    {
QT_MOC_LITERAL(0, 0, 13), // "QcvHistograms"
QT_MOC_LITERAL(1, 14, 21), // "histogramImageUpdated"
QT_MOC_LITERAL(2, 36, 0), // ""
QT_MOC_LITERAL(3, 37, 21), // "histogramImageChanged"
QT_MOC_LITERAL(4, 59, 4), // "Mat*"
QT_MOC_LITERAL(5, 64, 5), // "image"
QT_MOC_LITERAL(6, 70, 15), // "lutImageUpdated"
QT_MOC_LITERAL(7, 86, 15), // "lutImageChanged"
QT_MOC_LITERAL(8, 102, 21), // "histogramTime1Updated"
QT_MOC_LITERAL(9, 124, 14), // "formattedValue"
QT_MOC_LITERAL(10, 139, 21), // "computeLUTTimeUpdated"
QT_MOC_LITERAL(11, 161, 18), // "drawLUTTimeUpdated"
QT_MOC_LITERAL(12, 180, 19), // "applyLUTTimeUpdated"
QT_MOC_LITERAL(13, 200, 21), // "histogramTime2Updated"
QT_MOC_LITERAL(14, 222, 24), // "drawHistogramTimeUpdated"
QT_MOC_LITERAL(15, 247, 6), // "update"
QT_MOC_LITERAL(16, 254, 14), // "setSourceImage"
QT_MOC_LITERAL(17, 269, 16), // "setShowComponent"
QT_MOC_LITERAL(18, 286, 6), // "size_t"
QT_MOC_LITERAL(19, 293, 1), // "i"
QT_MOC_LITERAL(20, 295, 5), // "value"
QT_MOC_LITERAL(21, 301, 10), // "setLutType"
QT_MOC_LITERAL(22, 312, 7), // "lutType"
QT_MOC_LITERAL(23, 320, 14), // "setDynamicType"
QT_MOC_LITERAL(24, 335, 11), // "dynamicType"
QT_MOC_LITERAL(25, 347, 20) // "resetMeanProcessTime"

    },
    "QcvHistograms\0histogramImageUpdated\0"
    "\0histogramImageChanged\0Mat*\0image\0"
    "lutImageUpdated\0lutImageChanged\0"
    "histogramTime1Updated\0formattedValue\0"
    "computeLUTTimeUpdated\0drawLUTTimeUpdated\0"
    "applyLUTTimeUpdated\0histogramTime2Updated\0"
    "drawHistogramTimeUpdated\0update\0"
    "setSourceImage\0setShowComponent\0size_t\0"
    "i\0value\0setLutType\0lutType\0setDynamicType\0"
    "dynamicType\0resetMeanProcessTime"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QcvHistograms[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      10,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   94,    2, 0x06 /* Public */,
       3,    1,   95,    2, 0x06 /* Public */,
       6,    0,   98,    2, 0x06 /* Public */,
       7,    1,   99,    2, 0x06 /* Public */,
       8,    1,  102,    2, 0x06 /* Public */,
      10,    1,  105,    2, 0x06 /* Public */,
      11,    1,  108,    2, 0x06 /* Public */,
      12,    1,  111,    2, 0x06 /* Public */,
      13,    1,  114,    2, 0x06 /* Public */,
      14,    1,  117,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      15,    0,  120,    2, 0x0a /* Public */,
      16,    1,  121,    2, 0x0a /* Public */,
      17,    2,  124,    2, 0x0a /* Public */,
      21,    1,  129,    2, 0x0a /* Public */,
      23,    1,  132,    2, 0x0a /* Public */,
      25,    0,  135,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,
    QMetaType::Void, QMetaType::QString,    9,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 4,    5,
    QMetaType::Void, 0x80000000 | 18, QMetaType::Bool,   19,   20,
    QMetaType::Void, QMetaType::Int,   22,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void,

       0        // eod
};

void QcvHistograms::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QcvHistograms *_t = static_cast<QcvHistograms *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->histogramImageUpdated(); break;
        case 1: _t->histogramImageChanged((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        case 2: _t->lutImageUpdated(); break;
        case 3: _t->lutImageChanged((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        case 4: _t->histogramTime1Updated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 5: _t->computeLUTTimeUpdated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 6: _t->drawLUTTimeUpdated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 7: _t->applyLUTTimeUpdated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 8: _t->histogramTime2Updated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 9: _t->drawHistogramTimeUpdated((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 10: _t->update(); break;
        case 11: _t->setSourceImage((*reinterpret_cast< Mat*(*)>(_a[1]))); break;
        case 12: _t->setShowComponent((*reinterpret_cast< const size_t(*)>(_a[1])),(*reinterpret_cast< const bool(*)>(_a[2]))); break;
        case 13: _t->setLutType((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 14: _t->setDynamicType((*reinterpret_cast< const int(*)>(_a[1]))); break;
        case 15: _t->resetMeanProcessTime(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (QcvHistograms::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::histogramImageUpdated)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (QcvHistograms::*_t)(Mat * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::histogramImageChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (QcvHistograms::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::lutImageUpdated)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (QcvHistograms::*_t)(Mat * );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::lutImageChanged)) {
                *result = 3;
                return;
            }
        }
        {
            typedef void (QcvHistograms::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::histogramTime1Updated)) {
                *result = 4;
                return;
            }
        }
        {
            typedef void (QcvHistograms::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::computeLUTTimeUpdated)) {
                *result = 5;
                return;
            }
        }
        {
            typedef void (QcvHistograms::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::drawLUTTimeUpdated)) {
                *result = 6;
                return;
            }
        }
        {
            typedef void (QcvHistograms::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::applyLUTTimeUpdated)) {
                *result = 7;
                return;
            }
        }
        {
            typedef void (QcvHistograms::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::histogramTime2Updated)) {
                *result = 8;
                return;
            }
        }
        {
            typedef void (QcvHistograms::*_t)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&QcvHistograms::drawHistogramTimeUpdated)) {
                *result = 9;
                return;
            }
        }
    }
}

const QMetaObject QcvHistograms::staticMetaObject = {
    { &QcvProcessor::staticMetaObject, qt_meta_stringdata_QcvHistograms.data,
      qt_meta_data_QcvHistograms,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QcvHistograms::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QcvHistograms::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QcvHistograms.stringdata0))
        return static_cast<void*>(this);
    if (!strcmp(_clname, "CvHistograms8UC3"))
        return static_cast< CvHistograms8UC3*>(this);
    return QcvProcessor::qt_metacast(_clname);
}

int QcvHistograms::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QcvProcessor::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 16)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void QcvHistograms::histogramImageUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void QcvHistograms::histogramImageChanged(Mat * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void QcvHistograms::lutImageUpdated()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void QcvHistograms::lutImageChanged(Mat * _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void QcvHistograms::histogramTime1Updated(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void QcvHistograms::computeLUTTimeUpdated(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void QcvHistograms::drawLUTTimeUpdated(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void QcvHistograms::applyLUTTimeUpdated(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 7, _a);
}

// SIGNAL 8
void QcvHistograms::histogramTime2Updated(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void QcvHistograms::drawHistogramTimeUpdated(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
