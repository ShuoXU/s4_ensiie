/**
 * @brief definition of treemodel inherit of QAbstractItemModel
 */

#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

class TreeItem;


class TreeModel : public QAbstractItemModel
{


public:
    /**
     * @brief constructor
     * @param headers
     * @param data
     * @param parent
     */
    TreeModel(const QStringList &headers, const QString &data,
              QObject *parent = nullptr);
    ~TreeModel() override;


    /**
     * @param index
     * @param role
     * @return Returns the data stored under the given role for the item referred to by the index.
     */
    QVariant data(const QModelIndex &index, int role) const override;
    /**
     * @param section
     * @param orientation
     * @param role
     * @return Returns the data for the given role and section in the header with the specified orientation.
     */
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;

    /**
     * @param row
     * @param column
     * @param parent
     * @return Returns the index of the item in the model specified by the given row, column and parent index.
     */
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    /**
     * @param index
     * @return Returns the parent of the model item with the given index. If the item has no parent, an invalid QModelIndex is returned.
     */
    QModelIndex parent(const QModelIndex &index) const override;
    /**
     * @param parent
     * @return Returns the number of rows under the given parent.
     */

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    /**
     * @param parent
     * @return Returns the number of columns for the children of the given parent.
     */
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    /**
     * @param index
     * @return Returns the item flags for the given index.s
     */
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    /**
     * @brief Sets the role data for the item at index to value.
     * @param index
     * @param value
     * @param role
     * @return Returns true if successful; otherwise returns false.
     */
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    /**
     * @brief sets the data for the given role and section in the header with the specified orientation to the value supplied.
     * @param section
     * @param orientation
     * @param value
     * @param role
     * @return Returns true if the header's data was updated; otherwise returns false.
     */
    bool setHeaderData(int section, Qt::Orientation orientation,
                       const QVariant &value, int role = Qt::EditRole) override;

    /**
     * @param position
     * @param columns
     * @param parent
     * @return Returns true if the columns were successfully inserted; otherwise returns false.
     */
    bool insertColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex()) override;
    /**
     * @param position
     * @param columns
     * @param parent
     * @return Returns true if the columns were successfully removed; otherwise returns false.
     */
    bool removeColumns(int position, int columns,
                       const QModelIndex &parent = QModelIndex()) override;
    /**
     * @param position
     * @param rows
     * @param parent
     * @return Returns true if the rows were successfully inserted; otherwise returns false.
     */
    bool insertRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) override;
    /**
     * @param position
     * @param rows
     * @param parent
     * @return Returns true if the rows were successfully removed; otherwise returns false.
     */
    bool removeRows(int position, int rows,
                    const QModelIndex &parent = QModelIndex()) override;
    /**
     *
     * @return the root item
     */
    TreeItem* getRoot();

private:
    /**
     * @brief set up esstial dataod model
     * @param lines header information
     * @param parent
     */
    void setupModelData(const QStringList &lines, TreeItem *parent);
    /**
     * @param index
     * @return return item at index
     */
    TreeItem *getItem(const QModelIndex &index) const;

    TreeItem *rootItem;
};


#endif // TREEMODEL_H
