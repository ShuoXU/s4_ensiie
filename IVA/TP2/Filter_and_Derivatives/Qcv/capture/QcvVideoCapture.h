/*
 * QcvVideoCapture.h
 *
 *  Created on: 29 janv. 2012
 *	  Author: davidroussel
 */

#ifndef QCVVIDEOCAPTURE_H_
#define QCVVIDEOCAPTURE_H_

#include <Qcv/capture/QcvCapture.h>

#include <opencv2/highgui/highgui.hpp>
using namespace cv;

/**
 * Qt Abstract Class for capturing videos from cameras or files with OpenCV.
 * QcvVideoCapture opens streams and refresh itself automatically.
 * When frame has been refreshed a signal is emitted.
 */
class QcvVideoCapture: public QcvCapture
{
	Q_OBJECT
	protected:
		/**
		 * OpenCV Video Capture instance
		 */
		cv::VideoCapture * capture;

		/**
		 * Indicates video comes from a camera (rather than a file)
		 */
		bool live;

		/**
		 * When #resized is on, indicates if resize is performed directly
		 * within capture or through cv::resize function
		 */
		bool directResize;

		/**
		 * current framerate based on time measured from last capture
		 */
		double frameRate;

	public:
		/**
		 * Constructor for live video from deviceId
		 * @param deviceId the ID of the camera to open
		 * @param flipVideo indicates if the video should be flipped horizontally
		 * @param gray indicates if image should be converted to gray
		 * @param width desired width (or 0 to keep original capture image width)
		 * @param height desired height (or 0 to keep original capture image height)
		 * @param parent the parent QObject
		 */
		explicit QcvVideoCapture(const int deviceId = 0,
								 const bool flipVideo = false,
								 const bool gray = false,
								 const size_t width = 0,
								 const size_t height = 0,
								 QObject * parent = nullptr);

		/**
		 * Constructor from file name
		 * @param fileName video file to open
		 * @param flipVideo mirror image
		 * @param gray convert image to gray
		 * @param skip indicates capture can skip an image. When the capture
		 * result has not been processed yet, or when false that capture should
		 * wait for the result to be processed before grabbing a new image.
		 * This only applies when #updateThread is not NULL.
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @param parent the parent QObject
		 */
		explicit QcvVideoCapture(const QString & fileName,
								 const bool flipVideo = false,
								 const bool gray = false,
								 const size_t  width = 0,
								 const size_t height = 0,
								 QObject * parent = NULL);

		/**
		 * Destructor.
		 * releases video capture and image(s)
		 */
		virtual ~QcvVideoCapture();

		/**
		 * Get the "live" status of the captured video
		 * @return the "live" status of the video
		 * @see #live
		 */
		bool isLive() const;

		/**
		 * Get the "direct resizing" status of the captured video
		 * @return the "direct resizing" status of the captured video
		 * @see #directResize
		 */
		bool isDirectResize() const;

		/**
		 * Get the current capture frame rate
		 * @return the current capture frame rate
		 */
		double getFrameRate() const;

		/**
		 * Indicates if capture works in synchronized mode where
		 * frame rate is imposed.
		 * @return true is capture works in synchronized mode, false
		 * otherwise
		 */
		virtual bool isSynchronized() const = 0;

		/**
		 * Gets the contrained frame rate at which capture should operate
		 * when synchronized mode is on
		 * @return the constrained frame rate
		 */
		virtual double getContrainedFrameRate() const = 0;

	signals:

		/**
		 * Signal emitted when capture is released
		 */
		void finished();

		/**
		 * Signal to send when frame rate has changed
		 * @param value the new frame rate
		 */
		void frameRateChanged(const double value);

		/**
		 * Signal to send when the constrained frame rate has changed
		 * @param value the value of the new constrained frame rate
		 */
		void constrainedFrameRateChanged(const double value);

		/**
		 * Signal to send when video capture is restarted (typically when
		 * playing video file and reaching the end of the file, the capture
		 * will try to go back to the beginning and play it again from start).
		 */
		void restarted();

		/**
		 * Signal to send when capture changes sychrone/ansynchrone mode
		 * (typically camera capture should operate with asynchrone mode whereas
		 * video file play back should operate with synchrone mode (eventually
		 * with a frame rate extracted from the video itself))
		 * @param sync the actual synchronized/asynchrone mode
		 */
		void synchronized(const bool sync);

	public slots:
		/**
		 * Open new device Id
		 * @param deviceId device number to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if device has been opened and checked
		 */
		virtual bool openDevice(const int deviceId,
								const size_t width = 0,
								const size_t height = 0) = 0;

		/**
		 * Open new video file
		 * @param fileName video file to open
		 * @param width desired width or 0 to keep capture width
		 * @param height desired height or 0 to keep capture height
		 * @return true if video has been opened and timer launched
		 */
		virtual bool openFile(const QString & fileName,
							  const size_t width = 0,
							  const size_t height = 0) = 0;

		/**
		 * Sets the "flipped" status of captured video
		 * @param value the new "flipped" status
		 */
		virtual void setFlipped(const bool value) = 0;

		/**
		 * Sets the gray conversion status of captured video
		 * @param value the new value of gray conversion status
		 */
		virtual void setGray(const bool value) = 0;

		/**
		 * Sets capture to synchronized mode (when true) where frame rate is
		 * contrained (whenever possible). Otherwise frame rate is not
		 * constrained and capture may proceed as fast as possible.
		 * @param value the new value of synchronized mode
		 */
		virtual void setSynchronized(const bool value) = 0;

		/**
		 * Sets the desired frame rate on capture ONLY when capture is in
		 * synchronized mode
		 *	- If the new frame rate is 0.0 then use natural capture frame rate.
		 *	- If the new frame rate is too high for capture then capture will
		 *	keeps its highest possible frame rate according to the device in use
		 * @param frameRate the new frame rate to set
		 * @note Please note that this method has no effect on capture when
		 * synchronized mode is off
		 */
		virtual void setFrameRate(const double frameRate) = 0;

		/**
		 * Releases capture and emits #finished() signal
		 */
		virtual void finish();

	protected slots:

	protected:

		/**
		 * Tries to set capture size directly on capture by setting properties.
		 * 	- CV_CAP_PROP_FRAME_WIDTH to set frame width
		 * 	- CV_CAP_PROP_FRAME_HEIGHT to set frame height
		 * @param width the width property to set on capture
		 * @param height the height property to set on capture
		 * @return true if capture is opened and if width and height have been
		 * set successfully through @code capture.set(...) @endcode. Returns
		 * false otherwise.
		 * @pre mutex should be locked before calling this method when update
		 * thread is running
		 */
		bool setDirectSize(const size_t width, const size_t height);

		/**
		 * Wrapper method to emit finished signal for other classes to use
		 * @see #finished()
		 */
		void emitFinished();

		/**
		 * Wrapper method to emit frame rate changed signal for other classes
		 * to use
		 * @see #frameRateChanged();
		 */
		void emitFrameRateChanged();

		/**
		 * Wrapper method to emit restarted signal for other classes to use
		 * @see #restarted();
		 */
		void emitRestarted();
};

#endif // QCVVIDEOCAPTURE_H
