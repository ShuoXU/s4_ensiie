/*
 * BoundedRandomGenerator.h
 *
 *  Created on: 21 févr. 2013
 *      Author: davidroussel
 */

#ifndef BOUNDEDGENERATOR_H_
#define BOUNDEDGENERATOR_H_

/**
 * Foncteur permettant de générer des valeurs aléatoires comprises entre un
 * minimum et un maximum
 * @tparam le type des valeurs
 */
template <class T>
class BoundedRandomGenerator
{
	private:
		/*
		 * TODO Attributes
		 */
	T min;
	T max;
	public:
		/**
		 * Constructeur d'un Générateur de nombres aléatoires borné
		 * @param min la valeur minimale
		 * @param max la valeur maximale
		 */
		BoundedRandomGenerator(const T & min, const T & max);

		/**
		 * TODO Opérateur d'appel de fonction
		 * @return un nouveau nombre aléatoire compris entre min et max
		 */
		T operator()(void);

};

#endif /* BOUNDEDGENERATOR_H_ */
