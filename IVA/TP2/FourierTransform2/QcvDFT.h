/*
 * QcvDFT.h
 *
 *  Created on: 22 févr. 2012
 *      Author: davidroussel
 */

#ifndef QCVDFT_H_
#define QCVDFT_H_

#include <vector>

using namespace std;

#include <QMutex>

#include <Qcv/QcvProcessor.h>
#include <Qcv/controllers/QBoolController.h>
#include <Qcv/controllers/QEnumController.h>
#include <Qcv/controllers/QRangeDoubleController.h>
#include <Qcv/controllers/QRangeIntController.h>

#include "CvDFT.h"

class QcvDFT: public QcvProcessor, public CvDFT
{
	Q_OBJECT

	private:
		// --------------------------------------------------------------------
		// Controllers related to CvDFT's attributes
		// --------------------------------------------------------------------
		/**
		 * Controller for filtering toggle
		 * @see #filtering
		 */
		QBoolController filteringController;

		/**
		 * Names for filtering types
		 */
		QStringList filterTypeNames =
		{
			tr("Box"),
			tr("Gaussian"),
			tr("Sinc")
		};

		/**
		 * Controller for the kind of filtering performed
		 * @see #filterType
		 */
		QEnumController filterTypeController;

		/**
		 * Controller for the log scale magnification of spectrum display
		 * @see #logScaleFactor
		 */
		QRangeDoubleController logScaleController;

		/**
		 * Controllers for low pass filters value
		 * @see #lowPassFilter
		 */
		vector<QRangeIntController *> lowPassFilterController;

		/**
		 * Controllers for high pass filters value
		 * @see #highPassFilter
		 */
		vector<QRangeIntController *> highPassFilterController;

		/**
		 * Boolean flag indicating the low pass filters should be linked
		 * together and the high pass filters should be linked together
		 */
		bool linkFilters;

		/**
		 * The controller of the filters linkage
		 * @see #linkFilters
		 */
		QBoolController linkFiltersController;

	public:

		/**
		 * QcvDFT constructor
		 * @param image the source image
		 * @param imageLock the mutex for concurrent access to the source image.
		 * In order to avoid concurrent access to the same image
		 * @param updateThread the thread in which this processor should run
		 * @param parent parent QObject
		 */
		QcvDFT(Mat * image,
			   QMutex * imageLock = NULL,
			   QThread * updateThread = NULL,
			   QObject * parent = NULL);

		/**
		 * QcvDFT destructor
		 */
		virtual ~QcvDFT();

		// --------------------------------------------------------------------
		// Controllers accessors
		// --------------------------------------------------------------------
		/**
		 * Accessor for the filtering controller
		 * @return the filtering controller pointer
		 */
		QBoolController * getFilteringController();

		/**
		 * Accessor for the filtering type controller
		 * @return the filtering type controller pointer
		 */
		QEnumController * getFilterTypeController();

		/**
		 * Accessor for the log scale controller
		 * @return the log scale controller pointer
		 */
		QRangeDoubleController * getLogScaleController();

		/**
		 * Accessor for the ith low pas filter controller
		 * @return the ith low pas filter controller pointer or null if there
		 * is no such controller when index is invalid
		 */
		QRangeIntController * getLowPassFilterController(const size_t i = 0);

		/**
		 * Accessor for the ith high pas filter controller
		 * @return the ith high pas filter controller pointer or null if there
		 * is no such controller when index is invalid
		 */
		QRangeIntController * getHighPassFilterController(const size_t i = 0);

		/**
		 * Accessor for the linked filters controller
		 * @return the linked filters controller pointer
		 */
		QBoolController * getLinkFiltersController();

		/**
		 * Setter to link or unlink filters together
		 * @param value the new linkage status
		 */
		void setLinkFilters(const bool value);

	public slots:
		/**
		 * Update computed images slot and sends updated signal
		 * required
		 */
		void update();

		// --------------------------------------------------------------------
		// Options settings with message notification
		// --------------------------------------------------------------------
		/**
		 * Changes source image slot.
		 * Attributes needs to be cleaned up then set up again
		 * @param image the new source Image
		 */
		void setSourceImage(Mat * image)
			throw (CvProcessorException);

	signals:

//		/**
//		 * Signal sent when source image changes to adjust max filter sizes
//		 */
//		void dftSizeChanged();

		/**
		 * Signal sent when input dftSize square image has been reallocated
		 * @param image the new in square image
		 */
		void squareImageChanged(Mat * image);

		/**
		 * Signal sent when spectrum image has been reallocated
		 * @param image the new spectrum image
		 */
		void spectrumImageChanged(Mat * image);

		/**
		 * Signal sent when inverse image has been reallocated
		 * @param image the new inverse image
		 */
		void inverseImageChanged(Mat * image);

		/**
		 * Signal sent when optimal DFT size changes
		 * @param size the new optimal DFT size
		 */
		void optimalDFTSizeChanged(int size);

		/**
		 * Signal to send when all color channel becomes available or not
		 * @param available the new availability of all color channels
		 */
		void allChannelsAvailableChanged(const bool available);
};

#endif /* QCVDFT_H_ */
