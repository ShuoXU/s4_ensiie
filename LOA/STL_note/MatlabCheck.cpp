/*
 * MatlabCheck.cpp
 *
 *  Created on: 9 févr. 2017
 *      Author: davidroussel
 */

#include <sstream>		// For ostringstream
#include <algorithm>	// For copy algorihtm
#include <iterator>		// For ostream_iterator

#include "MatlabCheck.h"

/*
 * Protected constructor for external checker
 * @param baseDir the base directory to run the external tool from
 * @param begin an iterator to the first value of the tested sequence
 * @param end an iterator past the last value of the tested sequence
 * @param minValue minimum possible value in the sequence
 * @param maxValue maximum possible value in the sequence
 * @param meanValue computed mean value of the sequence
 * @param stdValue computed std value of the sequence
 * @param medianValue computed median value in the sequence
 */
template<class Iterator>
MatlabCheck<Iterator>::MatlabCheck(const string & baseDir,
                                   const Iterator & begin,
                                   const Iterator & end,
                                   const value_t & minValue,
                                   const value_t & maxValue,
                                   const double & meanValue,
                                   const double & stdValue,
                                   const double & medianValue) :
	ExternalCheck<Iterator>("matlab",
	                        baseDir,
	                        begin,
	                        end,
	                        minValue,
	                        maxValue,
	                        meanValue,
	                        stdValue,
	                        medianValue)
{
	ostringstream commandStream;
	commandStream << tool;
	commandStream << " -nodisplay -nojvm -nosplash -r \"";
	commandStream << "cd " << baseDir << "; ";
	commandStream << "check_TP_STL([";

	copy(begin, end, ostream_iterator<value_t>(commandStream, " "));

	commandStream << "], " << minValue << ", " << maxValue << ", "
	              << meanValue << ", " << stdValue << ", " << medianValue
	              << "); exit;\"";
	commandStream << " | tail -n 6"; // avoid matlab ascii splash art

	command = commandStream.str();
}

#include <vector>
#include <deque>

/*
 * Instanciation du template MatlabCheck avec des <vector::const_iterator>
 */
template class MatlabCheck<std::vector<int>::const_iterator>;
template class MatlabCheck<std::deque<int>::const_iterator>;

/*
 * Instanciation du template MatlabCheck avec des <vector::const_iterator>
 */
template class MatlabCheck<std::vector<float>::const_iterator>;
template class MatlabCheck<std::deque<float>::const_iterator>;

/*
 * Instanciation du template MatlabCheck avec des <vector::const_iterator>
 */
template class MatlabCheck<std::vector<double>::const_iterator>;
template class MatlabCheck<std::deque<double>::const_iterator>;
