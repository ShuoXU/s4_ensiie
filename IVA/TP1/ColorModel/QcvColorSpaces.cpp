/*
 * QcvColorSpaces.cpp
 *
 *  Created on: 25 févr. 2012
 *      Author: davidroussel
 */

#include <functional>
#include <cmath>

#include <QDebug>
#include <QTime>

#include "QcvColorSpaces.h"

/*
 * QcvColorSpaces constructor
 * @param inFrame the input frame from capture
 * @param imageLock the mutex on source image
 * @param updateThread the thread in which this processor runs
 * @param parent object
 */
QcvColorSpaces::QcvColorSpaces(Mat * inFrame,
							   QMutex * imageLock,
							   QThread * updateThread,
							   QObject * parent) :
	CvProcessor(inFrame),
	QcvProcessor(inFrame, imageLock, updateThread, parent, 3), // 3 mutexes
	CvColorSpaces(inFrame),
	displayController(&imageDisplayIndex,
					  Display::INPUT,
					  Display::NBDISPLAY,
					  displayImageNames,
					  nullptr, // lock is already used in #setDisplayImageIndex
					  new function<void(const int)>(std::bind(&QcvColorSpaces::setDisplayImageIndex,
															  this,
															  std::placeholders::_1)),
					  (updateThread == nullptr ? this : nullptr)),
	channelsModeController(&showColoredChannels,
						   nullptr, // lock is already used in #setChannelsColor
						   new function<void(const bool)>(std::bind(&QcvColorSpaces::setChannelsColor,
																	this,
																	std::placeholders::_1)),
						   (updateThread == nullptr ? this : nullptr)),
	hueDisplayController(&hueDisplay,
						 HueDisplay::HUECOLOR,
						 HueDisplay::HUEGRAY,	// exclude Gray Hue from options
						 hueMixNames,
						 nullptr, // lock is already used in #setHueDisplayMode
						 new function<void(const int)>(std::bind(&QcvColorSpaces::setHueDisplayMode,
																 this,
																 std::placeholders::_1)),
						 (updateThread == nullptr ? this : nullptr)),
  blockSizeController(&snrSize,
					  locks[RESULT],
					  1,
					  nullptr,
					  (updateThread == nullptr ? this : nullptr))
{
	Q_ASSERT_X(nbLocks == 3, Q_FUNC_INFO, "Wrong number of mutexes");

	QcvProcessor::numberFormat = QString::fromUtf8("%6.0f");
	QcvProcessor::meanStdFormat = QString::fromUtf8("%5.0f");
}

/*
 * QcvColorSpaces destructor
 */
QcvColorSpaces::~QcvColorSpaces()
{
	/*
	 * Lock all result mutexes in order to prevent a concurrent update
	 * They will be unlocked and destroyed in ~QcvProcessor()
	 */
	lockMutexes(RESULT, IMAGES);
}

/*
 * Update computed images and sends displayImageChanged signal if
 * required
 */
void QcvColorSpaces::update()
{
	for(int i = 0; i < nbLocks; i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->lock();
		}
	}

	CvColorSpaces::update();

	for(int i = 0; i < nbLocks; i++)
	{
		if (locks[i] != nullptr)
		{
			locks[i]->unlock();
		}
	}

	if (displayImageChanged)
	{
		emit imageChanged(&displayImage);
	}

	QcvProcessor::update(); // emits updated signal

	// Computes & emit SNR signals
	convertSNRToPercent();
}

/*
 * Select image to set in displayImage and sends notification message
 * @param index the index to select display image
 */
void QcvColorSpaces::setDisplayImageIndex(const int index)
{
	bool hasLock = locks[RESULT] != nullptr;
	if (hasLock)
	{
		locks[RESULT]->lock();
	}

	CvColorSpaces::setDisplayImageIndex((Display)index);

	if (hasLock)
	{
		locks[RESULT]->unlock();
	}

	message.clear();
	message.append(tr("Display Image set to: "));
	if (imageDisplayIndex >= Display::INPUT &&
		imageDisplayIndex < Display::NBDISPLAY)
	{
		message.append(displayImageNames[index]);
	}
	else
	{
		message.append(tr("Unkown component !!!"));
	}

	emit sendMessage(message, defaultTimeOut);
}

/*
 * Write accessor for #showColoredChannels
 * @param status the new colored channels status
 */
void QcvColorSpaces::setChannelsColor(const bool status)
{
	bool hasLock = locks[RESULT] != nullptr;
	if (hasLock)
	{
		locks[RESULT]->lock();
	}

	CvColorSpaces::setChannelsColor(status);

	if (hasLock)
	{
		locks[RESULT]->unlock();
	}

	message.clear();
	message.append(tr("Setting components shown as colored to: "));
	if (status)
	{
		message.append(tr("on"));
	}
	else
	{
		message.append(tr("off"));
	}

	emit sendMessage(message, defaultTimeOut);
}

/*
 * Select hue display mode and sends notification message
 * @param mode the mode so select
 */
void QcvColorSpaces::setHueDisplayMode(const int mode)
{
	bool hasLock = locks[RESULT] != nullptr;
	if (hasLock)
	{
		locks[RESULT]->lock();
	}

	CvColorSpaces::setHueDisplayMode((HueDisplay)mode);

	if (hasLock)
	{
		locks[RESULT]->unlock();
	}

	message.clear();
	message.append(tr("Setting hue color display as: "));
	switch ((HueDisplay)mode)
	{
		case HueDisplay::HUECOLOR:
			message.append(tr("hue only"));
			break;
		case HueDisplay::HUESATURATE:
			message.append(tr("hue x saturation"));
			break;
		case HueDisplay::HUEVALUE:
			message.append(tr("hue x value"));
			break;
		case HueDisplay::HUEGRAY:
			message.append(tr("hue as gray"));
			break;
		case HueDisplay::NBHUES:
		default:
			message.append(tr("unknown"));
			break;
	}

	emit sendMessage(message, defaultTimeOut);
}

/*
 * Get the controller for #imageDisplayIndex
 * @return the controller for #imageDisplayIndex
 */
QEnumController * QcvColorSpaces::getDisplayController()
{
	return &displayController;
}

/*
 * Get the controller for #showColoredChannels
 * @return the controller for #showColoredChannels
 */
QBoolController * QcvColorSpaces::getChannelsModeController()
{
	return &channelsModeController;
}

/*
 * Get the controller for #hueDisplay
 * @return the controller for #hueDisplay
 */
QEnumController * QcvColorSpaces::getHueDisplayController()
{
	return &hueDisplayController;
}

/*
 * Get the controller for #snrSize
 * @return the controller for #snrSize
 */
QRangeIntController * QcvColorSpaces::getSNRSizeController()
{
	return &blockSizeController;
}

/*
 * Converts SNR levels generally between 0..20 to percent levels
 * between 0..100 and emits SNRChannelXXXChanged signals
 */
void QcvColorSpaces::convertSNRToPercent()
{
	int value;
	for (size_t i = 0; i < componentsSize; ++i)
	{
		value = min<int>(100, static_cast<int>(round((snr[i]*100.0)/20.0)));
		switch (i) {
			case 0:
				emit SNRChannel0Changed(value);
				break;
			case 1:
				emit SNRChannel1Changed(value);
				break;
			case 2:
				emit SNRChannel2Changed(value);
				break;
			default:
				break;
		}
	}
}
