/*
 * QcvProcessor.h
 *
 *  Created on: 19 févr. 2012
 *	  Author: davidroussel
 */

#ifndef QCVPROCESSOR_H_
#define QCVPROCESSOR_H_

#include <QObject>
#include <QDebug>
#include <QString>
#include <QRegExp>
#include <QMutex>
#include <QThread>
#include <QSettings>

#include <Qcv/controllers/QBoolController.h>

#include <CvProcessor.h>

/*
 * Declare regular OpenCV types for signal/slots with Qt
 */
Q_DECLARE_METATYPE(Mat);
Q_DECLARE_METATYPE(Size);
Q_DECLARE_METATYPE(CvProcessor::ProcessTime);

#if defined LIB
 #define LIB_EXPORT Q_DECL_EXPORT
#else
 #define LIB_EXPORT Q_DECL_IMPORT
#endif

/**
 * Qt flavored class to process a source image with OpenCV 2+
 */
class LIB_EXPORT QcvProcessor : public QObject, public virtual CvProcessor
{
	Q_OBJECT

	public:
		/**
		 * Index used to access locks in #locks
		 * @note This Enum should be overloaded in subclasses if number of
		 * mutexes changes
		 */
		enum LockIndex : size_t
		{
			/**
			 * Mutex index generally associated with the capture producing
			 * the input image (this mutex is not allocated in this class and
			 * therefore shall not be destroyed in the destructor). It can
			 * eventually be set to "nullptr" in order to "forget" this mutex.
			 */
			SOURCE = 0,
			/**
			 * Mutex index associated with results production
			 * @warning Sub classes may specialize this index for other purposes
			 * such as mutexes provided by other classes. Subclasses will be
			 * responsible setting this index's mutex to "nullptr" before this
			 * class's destructor is called in order to avoid deleting this
			 * index's mutex
			 */
			RESULT = 1,
			/**
			 * Mutex index used to ensure proper operations sequencing when this
			 * object (or one of its descendant) is bound to be destroyed:
			 *	- When this object (or one of its descendant) is destroyed AND
			 *	also has an #updateThread, its #update(); method runs in a
			 *	separate thread which could lead to a destructor called in one
			 *	thread (therefore deleting its data) while the update method
			 *	is still running in another thread, eventually trying to read or
			 *	write already destroyed data !!!
			 *	- The idea is for the destructor to lock this mutex's index
			 *	and for the update method to only "try" to lock this mutex's
			 *	index. If the child's update method does not succeed in locking
			 *	this mutex's index, it means destructor has already started and
			 *	the update process should be aborted, therefore ensuring proper
			 *	termination of the update method and furthermore of its thread
			 *	since the destructor also triggers the #updateThread's
			 *	termination.
			 */
			TERMINATE = 2
		};

	protected:
		/**
		 * Default timeout to show messages
		 */
		static int defaultTimeOut;

		/**
		 * Number format used to format numbers into QStrings
		 */
		static QString numberFormat;

		/**
		 * The regular expression used to validate new number formats
		 * @see #setNumberFormat
		 */
		static QRegExp numberRegExp;

		/**
		 * format used to format Mean/Std time values : <mean> ± <std>
		 */
		static QString meanStdFormat;

		/**
		 * format used to format Min/Max time values : <min> / <max>
		 */
		static QString minMaxFormat;

		/**
		 * Number of QMutex * in #locks
		 * @note used an int so it can be set to -1 for no locks at all
		 */
		const int nbLocks;

		/**
		 * An array of locks used to avoid concurrent access to:
		 *	- The source image (typically the source image which may be
		 * currently modified by the capture.
		 *	- Or the result (image or not) procuced by a QcvProcessor (or one
		 * of its subclasses).
		 */
		QMutex ** locks;

		/**
		 * the thread in which this processor should run
		 */
		QThread * updateThread;

		/**
		 * Message to send when something changes
		 */
		QString message;

		/**
		 * String used to store formatted process time value
		 */
		QString processTimeString;

		/**
		 * String used to store formatted min/max time values
		 */
		QString processMinMaxTimeString;

		/**
		 * A Controller for #timePerFeature
		 */
		QBoolController timePerFeatureController;

		/**
		 * A pointer to an instance of settings so changes on some processor's
		 * parameters can be reflected in some preferences settings if the
		 * pointer is not null. Also if the pointer is not null settings can be
		 * used to set an initial value if such value is present in the
		 * preferences stored in settings
		 * @note Typically the settings might be used on QxxxControllers to
		 * read/save the values managed by these controllers into preferences.
		 * In such case controllers should provide a name for each setting.
		 */
		QSettings * settings;

	public:

		/**
		 * QcvProcessor constructor
		 * @param image the source image
		 * @param imageLock the mutex for concurrent access to the source image.
		 * In order to avoid concurrent access to the same image
		 * @param updateThread the thread in which this processor should run
		 * @param parent parent QObject
		 * @param number of locks in #locks [default is 2 : SOURCE & RESULT]
		 * @param settings the settings used to set initial an value and/or
		 * save a current value to settings
		 */
		QcvProcessor(Mat * image,
					 QMutex * imageLock = nullptr,
					 QThread * updateThread = nullptr,
					 QObject * parent = nullptr,
					 const int nbLocks = 3,
					 QSettings * settings = nullptr);

		/**
		 * QcvProcessor destructor.
		 *	- clear messages
		 *	- try to lock the last (TERMINATE) lock (for at least 500 ms):
		 *	subclasses destructor should lock the last (TERMINATE) lock to
		 *	ensure continuity in destruction (not interruptible by updates)
		 *	- emit finished signal
		 *	- wait for update thread (if any) which should be soon since
		 *	the TERMINATE lock should prevent further updates
		 *	- if there is some non null result mutexes in locks (i.e. others
		 *	than SOURCE) unlock and destroy these mutexes.
		 *	- and finally destroys the #locks array.
		 * @post The #finished(); signal has been emitted
		 */
		virtual ~QcvProcessor();

		/**
		 * Sets new number format
		 * @param format the new number format
		 * @pre format string should look like "%8.1f" or at least not be longer
		 * than 10 chars since format is a 10 chars array.
		 * @post id format string is valid and shorter than 10 chars
		 * it has been applied as the new format string.
		 */
		static void setNumberFormat(const char * format);

		/**
		 * Get the format c-string for numbers
		 * @return the format string for numbers (e.g.: "%5.2f")
		 */
		static const char * getNumberFormat();

		/**
		 * Get the format c-string for std dev of numbers
		 * @return the format string for numbers (e.g.: " ± %4.2f")
		 */
		static const char * getStdFormat();

		/**
		 * Get the format c-string for min / max of numbers
		 * @return the format string for numbers (e.g.: "%5.2f / %5.2f")
		 */
		static const char * getMinMaxFormat();

		/**
		 * Number of mutexes in #locks
		 * @return The number of mutexes in #locks
		 */
		int locksSize() const;

		/**
		 * Accessor to the array of mutexes
		 * @param index the index of the mutex to get
		 * @return the address og the required mutex
		 */
		QMutex * getLock(const int index);

		/**
		 * Accessor to #timePerFeatureController
		 * @return a pointer to #timePerFeatureController
		 */
		QBoolController * getTimePerFeatureController();

		/**
		 * Send to debug stream (for showing processor attributes values)
		 * @param dbg the debug stream to send to
		 * @return a reference to the output stream
		 */
		virtual QDebug & toDBStream(QDebug & dbg) const;

		/**
		 * Friend QDebug output operator
		 * @param dbg the debug stream
		 * @param proc the Qcvprocessor to send to debug stream
		 * @return the debug stream
		 */
		friend QDebug & operator <<(QDebug & dbg, const QcvProcessor & proc);

	public slots:
		/**
		 * Emits updated and process time signals
		 * @note QcvProcessor::update() should be used at inside subclasses's
		 * update method
		 * @code
		 * void QcvSubProcessor::update()
		 * {
		 *	// Check if destructor has already locked the last mutex
		 *	// (here TERMINATE)
		 *	if (!locks[TERMINATE]->tryLock())
		 *	{
		 *		// destructor has started: abort
		 *		return;
		 *	}
		 *	else // Or see alternative at the end
		 *	{
		 *		locks[TERMINATE]->unlock();
		 *	}
		 *
		 *	// Lock any other mutexes from SOURCE to last result (here RESULT)
		 *	lockMutexesRange(SOURCE, RESULT);
		 *
		 *	// Call superclass update
		 *	CvSubProcessor::update()
		 *
		 *	// Unlock any other mutexes from SOURCE to last result (here RESULT)
		 *	unlockMutexesRange(SOURCE, RESULT);
		 *
		 *	// Emits notifications for this class
		 *	...
		 *
		 *	// Call QcvProcessor's update to emit
		 *	//	- #updated()
		 *	//	- #processTimeUpdated(QString &);
		 *	//	- #processTimeUpdated(ProcessTime *);
		 *	QcvProcessor::update();
		 *
		 *	// Alternatively unlocks the last mutex only at the very end to
		 *	// ensure super destructor can't start until this is over
		 *	locks[TERMINATE]->unlock();
		 * }
		 * @endcode
		 * @post The following signals are emitted:
		 *	- #updated();
		 *	- #processTimeUpdated(QString &);
		 *	- #processTimeUpdated(ProcessTime *);
		 */
		virtual void update();

		/**
		 * Changes source image slot.
		 * Attributes needs to be cleaned up then set up again
		 * @param image the new source Image
		 * @throw CvProcessorException#NULL_IMAGE when new source image is NULL
		 * @post Various signals have been emitted:
		 * 	- #imageChanged(sourceImage)
		 * 	- #imageCchanged()
		 * 	- if image size changed then #imageSizeChanged() is emitted
		 * 	- if image color space changed then #imageColorsChanged() is emitted
		 */
		virtual void setSourceImage(Mat * image) throw (CvProcessorException);

		/**
		 * Sets Time per feature processing time unit (reimplemented as a slot).
		 * @param value the time per feature value (true or false)
		 */
		virtual void setTimePerFeature(const bool value);

		/**
		 * Reset mean and std process time in order to re-start computing
		 * (reimplemented as a slot)
		 * new mean and std process time values.
		 */
		virtual void resetMeanProcessTime();

		/**
		 * Emits #finished(); signal to terminate any #updateThread
		 */
		virtual void finish();

	signals:
		/**
		 * Signal emitted when update is complete
		 */
		void updated();

		/**
		 * Signal emitted when processor has finished.
		 * Used to tell helper threads to quit
		 */
		void finished();

		/**
		 * Signal emitted when source image is reallocated
		 */
		void imageChanged();

		/**
		 * Signal emitted when source image is reallocated
		 * @param image the new source image pointer or none if just
		 * image changed notification is required
		 */
		void imageChanged(Mat * image);

		/**
		 * Signal emitted when source image colors changes from color to gray
		 * or from gray to color
		 */
		void imageColorsChanged();

		/**
		 * Signal emitted when source image size changes
		 */
		void imageSizeChanged();

		/**
		 * Signal emitted when processing time has channged
		 * @param formattedValue the new value of the processing time
		 */
		void processTimeUpdated(const QString & formattedValue);

		/**
		 * Signal emitted when min/max processing time has channged
		 * @param formattedValue the new value of the processing time
		 */
		void processTimeMinMaxUpdated(const QString & formattedValue);

		/**
		 * Signal emitted when processing time has changed
		 * @param time the new processing time
		 */
		void processTimeUpdated(const CvProcessor::ProcessTime * time);

		/**
		 * Signal to send when #timePerFeature changes
		 * @param value the new value of #timePerFeature
		 */
		void timePerFeatureChanged(const bool value);

		/**
		 * Signal to send update message when something changes
		 * @param message the message
		 * @param timeout number of ms the message should be displayed
		 */
		void sendMessage(const QString & message, int timeout = defaultTimeOut);

	protected:

		/**
		 * Locks a single mutex (iff the mutex at this index is not null in
		 * #locks array)
		 * @param index the index of the mutex to lock
		 * @return true if this mutex has been locked, false if the index is
		 * invalid (>= #nbLocks) or if the mutex at this index in #locks in null
		 */
		bool lockMutexes(const size_t index);

		/**
		 * Locks multiple mutexes at once (iff the mutexes at these indexes are
		 * not null in #locks)
		 * @tparam Args the type of the indexes to lock (must be size_t)
		 * @param the first mutex to lock
		 * @param args the various indexes of other mutexes to lock
		 * @return true if all mutexes at these indexes have been locked,
		 * false if at least one of the mutexes haven't been locked because of
		 * an invalid index or null locks at one or mode indexes.
		 */
		template<typename ... Args>
		bool lockMutexes(const size_t index, Args... args)
		{
			bool result = lockMutexes(index);
			result &= lockMutexes(args...);
			return result;
		}

		/**
		 * Try to lock a single mutex (iff the mutex at this index is not null in
		 * #locks array)
		 * @param index the index of the mutex to lock
		 * @return true if this mutex has been locked, false if the index is
		 * invalid (>= #nbLocks) or if the mutex at this index in #locks in null
		 * or simply because the mutex at this index is already locked
		 */
		bool tryLockMutexes(const size_t index);

		/**
		 * Try to lock multiple mutexes at once (iff the mutexes at these
		 * indexes are not null in #locks)
		 * @tparam Args the type of the indexes to lock (must be size_t)
		 * @param the first mutex to lock
		 * @param args the various indexes of other mutexes to lock
		 * @return true if all mutexes at these indexes have been locked,
		 * false if at least one of the mutexes haven't been locked because of
		 * an invalid index or null locks at one or mode indexes, or simply
		 * because at least one of those mutexes was already locked.
		 */
		template<typename ... Args>
		bool tryLockMutexes(const size_t index, Args... args)
		{
			bool result = tryLockMutexes(index);
			result &= tryLockMutexes(args...);
			return result;
		}

		/**
		 * UnLocks a single mutex (iff the mutex at this index is not null in
		 * #locks array)
		 * @param index the index of the mutex to unlock
		 * @return true if this mutex has been unlocked, false if the index is
		 * invalid (>= #nbLocks) or if the mutex at this index in #locks in null
		 */
		bool unlockMutexes(const size_t index);

		/**
		 * Unlocks multiple mutexes at once (iff the mutexes at these indexes
		 * are not null in #locks)
		 * @tparam Args the type of the indexes to unlock (must be size_t)
		 * @param the first mutex to unlock
		 * @param args the various indexes of other mutexes to unlock
		 * @return true if all mutexes at these indexes have been unlocked,
		 * false if at least one of the mutexes haven't been unlocked because of
		 * an invalid index or null locks at one or mode indexes.
		 */
		template<typename ... Args>
		bool unlockMutexes(const size_t index, Args... args)
		{
			bool result = unlockMutexes(index);
			result &= unlockMutexes(args...);
			return result;
		}

		/**
		 * Locks a range of mutexes in #locks
		 * @param low the index of the first mutex to lock in #locks
		 * @param high the index of the last mutex to lock in #locks
		 * @return the number of mutexes successfully locked (if all mutexes
		 * have been successfully locked then the result should be (high-low+1)
		 * @warning if low >= high no mutexes will be locked
		 */
		size_t lockMutexesRange(const size_t low, const size_t high);

		/**
		 * Unocks a range of mutexes in #locks
		 * @param low the index of the first  mutex to unlock in #locks
		 * @param high the index of the last mutex to unlock in #locks
		 * @return the number of mutexes successfully unlocked (if all mutexes
		 * have been successfully unlocked then the result should be (high-low+1)
		 * @warning if low >= high no mutexes will be unlocked
		 */
		size_t unlockMutexesRange(const size_t low, const size_t high);

		/**
		 * Converts an array of std::string to a QStringList
		 * @param names the array of string
		 * @param nbElements the number of elements in the array
		 * @return a QStringList containing the same thing as the array of
		 * string
		 */
		static QStringList stringArray2StringList(const string names[],
												  const size_t nbElements);

};

#endif /* QCVPROCESSOR_H_ */
