#include "CvProcessorException.h"
#include "QcvRangeValueControl.h"

/*
 * Valued constructor
 * @param rangeValue the range value to control
 * @param lock the mutex to check before setting new values
 * @param options flags indicating the presence of the spinner, the slider
 * and the reset button (all by default)
 * @throw exception if rangeValue is null
 */
template <class T>
QcvRangeValueControl<T>::QcvRangeValueControl(RangeValue<T> * rangeValue,
											  Options options,
											  QMutex * lock)
	throw (exception) :
	rangeValue(rangeValue),
	lock(lock)
{
	if (options.testFlag(QcvRangeValueControl::NoSpinner) &&
		options.testFlag(QcvRangeValueControl::NoSlider))
	{
		qWarning("%s: no slider nor spinner", Q_FUNC_INFO);
	}
	if (this->rangeValue == nullptr)
	{
		throw CvProcessorException(CvProcessorException::NULL_DATA,
								   "Null Range Value");
	}
}

/*
 * Destructor
 */
template <class T>
QcvRangeValueControl<T>::~QcvRangeValueControl()
{
	rangeValue = nullptr;
}

/*
 * Set new value in #rangeValue
 * @param value the new value to set
 * @return true if the value has been set exactly, or false
 * if the value has been set as close as possible
 */
template <class T>
bool QcvRangeValueControl<T>::setValue(T value)
{
	bool hasLock = lock != nullptr;
	if (hasLock)
	{
		lock->lock();
	}

	bool result = rangeValue->setClosestValue(value);

	if (hasLock)
	{
		lock->unlock();
	}

	return result;
}

/*
 * Reset value of #rangeValue to its default
 */
template <class T>
void QcvRangeValueControl<T>::resetValue()
{
	bool hasLock = lock != nullptr;
	if (hasLock)
	{
		lock->lock();
	}

	rangeValue->reset();

	if (hasLock)
	{
		lock->unlock();
	}
}

// ----------------------------------------------------------------------------
// Template instanciations
// ----------------------------------------------------------------------------
template class QcvRangeValueControl<int>;
template class QcvRangeValueControl<float>;
template class QcvRangeValueControl<double>;
