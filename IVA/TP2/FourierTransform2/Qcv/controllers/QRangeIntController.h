/*
 * QRangeIntController.h
 *
 *  Created on: 12 juin 2015
 *      Author: davidroussel
 */
#ifndef RANGEINTCONTROLLER_H
#define RANGEINTCONTROLLER_H

#include <QObject>

#include <Qcv/controllers/QRangeAbstractController.h>

/**
 * Controller for a RangeValue<int> connectable to QSpinBox and/or a QSlider
 */
class QRangeIntController : public QObject, public QRangeAbstractController<int>
{
	Q_OBJECT

	public:
		/**
		 * Constructor from RangeValue, mutex and parent object
		 * @param range the RangeValue<int> to reference
		 * @param lock the lock (from the processor containing the RangeValue) to
		 * use when modifying the RangeValue
		 * @param the factor to apply to the range values when emitting or
		 * the factor to divide outside values when setting a new range value.
		 * @param callback the call back to call for setting the value instead
		 * of settting the value directly
		 * @param parent parent object
		 * @param settings the settings to use to set initial value and/or
		 * save the current value (if different from default value) to settings
		 * @param settingsName the name to use when reading or writing the
		 * parameter value to settings
		 */
		explicit QRangeIntController(RangeValue<int> * const range,
									 QMutex * const lock = nullptr,
									 const int & factor = 1,
									 function<void(const int)> * callback = nullptr,
									 QObject * parent = nullptr,
									 QSettings * settings = nullptr,
									 const QString & settingsName = QString());

		/**
		 * Copy Constructor
		 * @param rc the range controller to copy
		 */
		QRangeIntController(const QRangeIntController & rc);

		/**
		 * Move Constructor
		 * @param rc the range controller to move
		 */
		QRangeIntController(QRangeIntController && rc);

		/**
		 * Destructor
		 */
		virtual ~QRangeIntController();

		/**
		 * Setup a spin box with the values in the RangeValue and connect
		 * the necessary signals and slots
		 * @param spinBox the spinbox to setup with
		 * @return true if the spinBox have been set up correctly, false
		 * otherwise
		 * @post If this spinbox was not part of the widgets set, then the
		 * spinBox has been setup with RangeValue values and bidirectionnal
		 * connections between this and the spinBox have been created.
		 */
		bool setupSpinBox(QAbstractSpinBox * spinBox);

		/**
		 * Setup a slider with the values in the RangeValue (and connects the
		 * necessary signals and slots).
		 * @param slider the slider to setup according to RangeValue values
		 * @return true if the slider have been set up correctly, false
		 * otherwise
		 * @post If this slider was not part of the widgets set, then the
		 * slider has been setup with RangeValue steps and bidirectionnal
		 * connections between this and the slider have been created.
		 */
		bool setupSlider(QAbstractSlider * slider);

		/**
		 * Adds a reset button to this range controller in order to reset to the
		 * default value
		 * @param button the button to connect to the reset slot
		 */
		bool addResetButton(QAbstractButton * button);

		/**
		 * Connects the corresponding xxxChanged(T) signal to the setNum
		 * slot of a QLabel
		 * @param label the label to show the minimum value
		 * @param which the kind of value to be shown in the label
		 * @return true if the label pointer was not null
		 */
		bool addNumberLabel(QLabel * label,
							RangeValue<int>::WhichValue which);

	public slots:
		/**
		 * Slot to change the #enabled state of GUI elements connected to this
		 * controller
		 * @param value the new enabled state
		 */
		void setEnabled(const bool value);

		/**
		 * Sets new value in the range value.
		 * @param value the value to set
		 * @return true if the value has been set without correction, false
		 * otherwise. In any case a value is set according the rules of
		 * RangeValue<T>::setClosestValue
		 * @note slot typically used by QSpinBox when its value changes
		 * @note Please note that if result is false a signal needs to be sent
		 * to the connected UI to ajust the value since it may have been
		 * slighltly modified.
		 * @note This method needs to be be reimplemented since it is a slot now
		 * @see RangeController::setValue(const int &)
		 */
		bool setValue(const int value);

		/**
		 * Sets a new value corresponding to the index
		 * @param index the index to set the new value to
		 * @return true in index is valid (the corresponding value is less or
		 * equal to the max value
		 * @note This method needs to be be reimplemented since it is a slot now
		 * @see RangeController::setIndex(const int &)
		 */
		bool setIndex(const int index);

		/**
		 * Reset managed value to its default value
		 * @return true if the value has been reset to its default value exactly,
		 * false if the reset has triggered bounds or step modifications
		 * @note This method needs to be be reimplemented since it is a slot now
		 */
		bool reset();

		/**
		 * Refresh all connected UI.
		 * When the internal managedValue has been modified outside of this
		 * controller, all connected UIs should be refreshed
		 */
		void refresh();

	signals:
		/**
		 * Signal to send when anything changes
		 */
		void updated();

		/**
		 * Signal to emit when value has been set
		 * @param value the new value
		 */
		void valueChanged(const int value);

		/**
		 * Signal emitted when value has been set in the RangeValue which
		 * changes the index of the current value
		 * @param index the index of the new value
		 * @note typically used by QSlider
		 */
		void indexChanged(const int index);

		/**
		 * Signal to emit when new min has been set
		 * @param value the new min value
		 */
		void minChanged(const int value);

		/**
		 * Signal to emit when new max has been set
		 * @param value the new max value
		 */
		void maxChanged(const int value);

		/**
		 * Signal to emit when min or max values have changed
		 * @param min the minimum value of the range
		 * @param max the maximum value of the range
		 */
		void rangeChanged(const int min, const int max);

		/**
		 * Signal to emit when new step has been set
		 * @param value the new step value
		 */
		void stepChanged(const int value);

		/**
		 * Signal to emit when min, max or step values have changed
		 * @param min the minimum value of the range
		 * @param max the maximum value of the range
		 */
		void indexRangeChanged(const int min, const int max);

	protected:
		// --------------------------------------------------------------------
		// Utility methods
		// --------------------------------------------------------------------
		/**
		 * Format a value of type T in a text
		 * @param value the value to be formated as text
		 * @return a new string containing the formatted text
		 */
		QString formatValue(const int & value);

		// ---------------------------------------------------------------------
		// Implementation of QRangeAbstractController<T> abstract methods
		// ---------------------------------------------------------------------
		/**
		 * Connects this controller's valueChanged signal to "controller"'s
		 * setValue Slot
		 * @param controller the controller to connect
		 * @note concrete subclasses must implement this method
		 */
		void connectMirror(QAbstractController<RangeValue<int>, int> * controller);

		/**
		 * Disconnects this controller's valueChanged signal from "controller"'s
		 * setValue Slot
		 * @param controller the controller to disconnect
		 * @note concrete subclasses must implement this method
		 */
		void disconnectMirror(QAbstractController<RangeValue<int>, int> * controller);

		/**
		 * sends a signal when anything changes
		 */
		void emitUpdated();

		/**
		 * Sends a signal when value has been set
		 * @param value the new value
		 */
		void emitValueChanged(const int value);

		/**
		 * Sends a signal when value has been set in the RangeValue which
		 * changes the index of the current value
		 * @param index the index of the new value
		 * @note typically used by QSlider
		 */
		void emitIndexChanged(const int index);

		/**
		 * Sends a signal when min value in the range changes
		 * @param value the new min value in the range
		 */
		void emitMinChanged(const int & value);

		/**
		 * Sends a signal when max value in the range changes
		 * @param value the new max value in the range
		 */
		void emitMaxChanged(const int & value);

		/**
		 * Sends a signal when min or max value changes
		 * @param min the minimum value of the range
		 * @param max the maximum value of the range
		 */
		void emitRangeChanged(const int & min, const int & max);

		/**
		 * Sends a signal when step value in the range changes
		 * @param value the new step value in the range
		 */
		void emitStepChanged(const int & value);

		/**
		 * Sends a signal when min, max or step changes (because it might affect
		 * the indices of a slider for instance)
		 * @param min the minimum index (always 0)
		 * @param max the maximum index (the number of steps of the range value)
		 */
		void emitIndexRangeChanged(const int & minIndex, const int & maxIndex);
};

#endif // RANGEINTCONTROLLER_H
