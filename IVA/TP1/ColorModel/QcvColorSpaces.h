/*
 * QcvColorSpaces.h
 *
 *  Created on: 25 févr. 2012
 *      Author: davidroussel
 */

#ifndef QCVCOLORSPACES_H_
#define QCVCOLORSPACES_H_

#include <QMutex>

#include <Qcv/controllers/QEnumController.h>
#include <Qcv/controllers/QBoolController.h>
#include <Qcv/controllers/QRangeIntController.h>

#include <Qcv/QcvProcessor.h>
#include "CvColorSpaces.h"

/**
 * Qt oriented Colorspaces
 */
class QcvColorSpaces : public QcvProcessor, public CvColorSpaces
{
	Q_OBJECT
	public:
		/**
		 * Index used to access locks in #locks
		 * @note Consequently #locksSize() should return 3
		 */
		enum LockIndex : size_t
		{
			SOURCE = 0,
			RESULT = 1,
			IMAGES = 2
		};

	protected:
//		/**
//		 * Self lock for operations in multiple threads
//		 * @note may be NULL if not multithreaded
//		 */
//		QMutex * selfLock;

//		/**
//		 * Lock to ensure produced image is complete before display
//		 * in QcvMatWidgets in multiple threads environment
//		 */
//		QMutex imageLock;

		/**
		 * Image names corresponding to #imageDisplayIndex
		 */
		QStringList displayImageNames = {
			tr("Input"),
			tr("Gray"),
			tr("RGB: Red"),
			tr("RGB: Green"),
			tr("RGB: Blue"),
			tr("RGB: Max"),
			tr("XYZ: X"),
			tr("XYZ: Y"),
			tr("XYZ: Z"),
			tr("HSV: Hue"),
			tr("HSV: Saturation"),
			tr("HSV: Value"),
			tr("YCbCr: Y"),
			tr("YCbCr: Cb"),
			tr("YCbCr: Cr")
		};

		/**
		 * Enum controller for #imageDisplayIndex
		 */
		QEnumController displayController;

		/**
		 * Enum controller for color/gray channels modes #showColoredChannels
		 */
		QBoolController channelsModeController;

		/**
		 * Names of Hue mix modes
		 */
		QStringList hueMixNames = {
			"Hue",
			"Hue x Sat",
			"Hue x Value"
		};

		/**
		 * Enum controller for #hueDisplay
		 */
		QEnumController hueDisplayController;

		/**
		 * Range Controller for #snrSize
		 */
		QRangeIntController blockSizeController;

	public:
		/**
		 * QcvColorSpaces constructor
		 * @param inFrame the input frame from capture
		 * @param imageLock the mutex on source image
		 * @param updateThread the thread in which this processor runs
		 * @param parent object
		 */
		QcvColorSpaces(Mat * inFrame,
					   QMutex * imageLock = NULL,
					   QThread * updateThread = NULL,
					   QObject * parent = NULL);

		/**
		 * QcvColorSpaces destructor
		 */
		virtual ~QcvColorSpaces();

	public slots:
		/**
		 * Update computed images and sends displayImageChanged signal if
		 * required
		 */
		void update() override;

		/**
		 * Select image to set in displayImage and sends notification message
		 * @param index the index to select display image
		 */
		void setDisplayImageIndex(const int index);

		/**
		 * Write accessor for #showColoredChannels
		 * @param status the new status of colored individual channels
		 */
		void setChannelsColor(const bool status) override;

		/**
		 * Select hue display mode and sends notification message
		 * @param mode the mode so select
		 */
		void setHueDisplayMode(const int mode);

		/**
		 * Get the controller for #imageDisplayIndex
		 * @return the controller for #imageDisplayIndex
		 */
		QEnumController * getDisplayController();

		/**
		 * Get the controller for #showColoredChannels
		 * @return the controller for #showColoredChannels
		 */
		QBoolController * getChannelsModeController();

		/**
		 * Get the controller for #hueDisplay
		 * @return the controller for #hueDisplay
		 */
		QEnumController * getHueDisplayController();

		/**
		 * Get the controller for #snrSize
		 * @return the controller for #snrSize
		 */
		QRangeIntController * getSNRSizeController();

	signals:
		/**
		 * signal emitted when SNR changes on channel 0
		 * @param value the new value SNR on channel 0 expressed in % [0..100]
		 */
		void SNRChannel0Changed(int value);

		/**
		 * signal emitted when SNR changes on channel 1
		 * @param value the new value SNR on channel 1 expressed in % [0..100]
		 */
		void SNRChannel1Changed(int value);

		/**
		 * signal emitted when SNR changes on channel 2
		 * @param value the new value SNR on channel 2 expressed in % [0..100]
		 */
		void SNRChannel2Changed(int value);

	protected:
		/**
		 * Converts SNR levels generally between 0..20 to percent levels
		 * between 0..100 and emits SNRChannelXXXChanged signals
		 */
		void convertSNRToPercent();
};

#endif /* QCVCOLORSPACES_H_ */
