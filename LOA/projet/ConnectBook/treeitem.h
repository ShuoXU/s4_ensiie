/**
 * the definition of treeitem for treemodel
 */

#ifndef TREEITEM_H
#define TREEITEM_H

#include <QVariant>
#include <QVector>

class TreeItem
{
public:
    /**
     * @brief constructor
     * @param data
     * @param parent
     */
     TreeItem(const QVector<QVariant> &data, TreeItem *parent = nullptr);
    ~TreeItem();

    /**
     * @param number
     * @return return child item at number
     */
    TreeItem *child(int number);
    /**
     * @return the number of children
     */
    int childCount() const;
    /**
     * @return the number of column
     */
    int columnCount() const;
    /**
     * @param column
     * @return the data of this column
     */
    QVariant data(int column) const;
    /**
     * @param position
     * @param count
     * @param columns
     * @return true if insertion of children successed
     */
    bool insertChildren(int position, int count, int columns);
    /**
     * @param position
     * @param columns
     * @return true if insertion of a column successed
     */
    bool insertColumns(int position, int columns);
    /**
     * @return return parent item
     */
    TreeItem *parent();
    /**
     * @param position
     * @param count
     * @return true if removement of children successed;
     */
    bool removeChildren(int position, int count);
    /**
     * @param position
     * @param columns
     * @return true if removement of children successed;
     */
    bool removeColumns(int position, int columns);
    /**
     * @return the number of children
     */
    int childNumber() const;
    /**
     * @brief set data for item
     * @param column
     * @param value
     * @return
     */
    bool setData(int column, const QVariant &value);

private:
    QVector<TreeItem*> childItems;
    QVector<QVariant> itemData;
    TreeItem *parentItem;
};

#endif // TREEITEM_H
