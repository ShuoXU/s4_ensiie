#ifndef QCVALGOPARAMWIDGET_H
#define QCVALGOPARAMWIDGET_H

#include <QWidget>
#include <QSettings>

#include "CvAlgoParam.h"

/**
 * Common abstract class to all QcvAlgoParamXXXWidget representing the visualisation
 * and interaction widget for a CvAlgoParam.
 * @author David Roussel
 * @date 2014/05/13
 */
class QcvAlgoParamWidget : public QWidget
{
	Q_OBJECT

	protected:
		/**
		 * The algorithm parameter to be modified by this widget
		 */
		CvAlgoParam * parameter;

		/**
		 * Indicates if operations should be performed directly on parameter
		 * (if true) or through signal/slot mechanism (if false).
		 */
		bool directOp;

		/**
		 * A pointer to an instance of settings so changes in this widget
		 * can be reflected in some preferences settings if the pointer is not
		 * null. Also if the pointer is not null settings can be used to set
		 * an initial value if such value is present in the preferences stored
		 * in settings
		 */
		QSettings * settings;

		/**
		 * The name to use to read or write values from or to settings
		 * if #settings is not null
		 */
		QString settingsName;

	public:
		/**
		 * Constructor
		 * @param param the algorithm parameter
		 * @param direct direct operation: if true indicates that parameter
		 * value setting is performed directly through
		 * param->set[Bool|Int|Double]Value(...) which is blocking in
		 * multithread environments. If false indicates that parameter value
		 * setting is performed through signal/slot mechanism, which in
		 * multithreaded environments is non blocking with queued connections.
		 * @param settings the settings to use to set initial value and/or
		 * save the current value (if different from default value) to settings
		 * @param settingsName the name to use when reading or writing the
		 * parameter value to settings
		 * @param parent the parent widget
		 */
		explicit QcvAlgoParamWidget(CvAlgoParam * param,
									const bool direct,
									QSettings * settings,
									const QString & settingsName,
									QWidget * parent);

		/**
		 * Destructor.
		 * does nothing here but except removing all algorithms from parameter
		 * and clears UI in subclasses.
		 * @post the algorithms associated to this parameter have been released
		 * from this parameter before it is destroyed.
		 */
		virtual ~QcvAlgoParamWidget();

	signals:
		/**
		 * Signal emitted when reset button is clicked in order to reset
		 * parameter to its default value (if it has any)
		 */
		void parameterReset();

		/**
		 * Signal emitted when the parameter value has been changed
		 */
		void parameterUpdated();

	private:
		/**
		 * [Not yet]Slot triggered when reset button is clicked.
		 * @note abstract method is reimplemented in every subclass in order
		 * to reset the algorithm parameter to its default value
		 */
		virtual void on_resetButton_clicked() = 0;

	protected:

		/**
		 * Read value from settings (iff #settings is non null and #settingsName
		 * is non empty)
		 * @return true if a value has been found in settings, and if this value
		 * is different from the current value of the parameter and has been set,
		 * false otherwise
		 * @note Should be implemented in subclasses to actually use the type
		 * of the parameter to read and set it.
		 * @note This method should typically be used in the constructor, it
		 * sets value directly into the #parameter and does not update any
		 * connected widget.
		 */
		virtual bool readSetting() = 0;

		/**
		 * Save a value to settings (iff #settings is non null and
		 * #settingsName is non empty)
		 * @param value the QVariant value to save into preferences
		 */
		virtual void writeSetting(const QVariant & value) const;

		/**
		 * Remove settings corresponding to #settingsName from preferences (iff
		 * #settings are available and #settingsName is non empty)
		 */
		virtual void removeSetting() const;
};

#endif // QCVALGOPARAMWIDGET_H
