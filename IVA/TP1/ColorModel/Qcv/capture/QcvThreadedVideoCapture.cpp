#include <QDebug>	// for qWarning & qDebug
#include <QTime>	// for time debug purpose

#include <cmath>	// for basic math function (conversions)

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>

#include "QcvThreadedVideoCapture.h"

/*
 * The default frame rate for synchronized capture
 */
const double QcvThreadedVideoCapture::defaultConstrainedFrameRate = 30.0;

// ----------------------------------------------------------------------------
// Public Methods
// ----------------------------------------------------------------------------

/*
 * Constructor for live video from deviceId
 * @param deviceId the ID of the camera to open
 * @param flipVideo indicates if the video should be flipped horizontally
 * @param gray indicates if image should be converted to gray
 * @param width desired width (or 0 to keep original capture image width)
 * @param height desired height (or 0 to keep original capture image height)
 * @param parent the parent QObject
 */
QcvThreadedVideoCapture::QcvThreadedVideoCapture(const int deviceId,
												 const bool flipVideo,
												 const bool gray,
												 const size_t width,
												 const size_t height,
												 QObject * parent) :
	QcvVideoCapture(deviceId,
					flipVideo,
					gray,
					width,
					height,
					parent),
	worker(new QcvCaptureWorker(this, false)), // unsynchronized
	updateCondition(nullptr),
	restarted(false)
{
	assert(worker != nullptr);

	connect(this, SIGNAL(finished()),
			worker, SLOT(quit()));

	setupAndLaunch(width, height);
}

/*
 * Constructor from file name
 * @param fileName video file to open
 * @param flipVideo mirror image
 * @param gray convert image to gray
 * @param skip indicates capture can skip an image. When the capture
 * result has not been processed yet, or when false that capture should
 * wait for the result to be processed before grabbing a new image.
 * This only applies when #updateThread is not NULL.
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @param parent the parent QObject
 */
QcvThreadedVideoCapture::QcvThreadedVideoCapture(const QString & fileName,
												 const bool flipVideo,
												 const bool gray,
												 const size_t  width,
												 const size_t height,
												 QObject * parent) :
	QcvVideoCapture(fileName,
					flipVideo,
					gray,
					width,
					height,
					parent),
	worker(new QcvCaptureWorker(this, true)), // synchronized with delaut delay
	updateCondition(nullptr),
	restarted(false)
	// statusMessage is default constructed
{
	assert(worker != nullptr);

	connect(this, SIGNAL(finished()),
			worker, SLOT(quit()));

	/*
	 * Warning, by the end of this constructor capture should have captured
	 * at least one image so this image can be used to configure processors
	 * using this image without having to wait for the worker to launch
	 * and retrieve a first image
	 */
	setupAndLaunch(width, height);
}

/*
 * Destructor.
 * releases video capture and image(s)
 */
QcvThreadedVideoCapture::~QcvThreadedVideoCapture()
{
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "will lock";
	mutex.lock();
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "locked";

	capture->release();
	mutex.unlock();
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "unlocked";

	// Waits for its worker to finish
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << "waiting for worker thread";
	worker->wait();
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << "finished waiting for worker thread";
	// Since worker is child of this the QObject destructor
	// should destroy its children
}

/*
 * Indicates if capture works in synchronized mode where
 * frame rate is imposed.
 * @return true is capture works in synchronized mode, false
 * otherwise
 */
bool QcvThreadedVideoCapture::isSynchronized() const
{
	if (worker != nullptr)
	{
		return worker->isSynchronized();
	}
	else
	{
		return false;
	}
}

/*
 * Gets the contrained frame rate at which capture should operate
 * when synchronized mode is on
 * @return the constrained frame rate
 */
double QcvThreadedVideoCapture::getContrainedFrameRate() const
{
	if (worker != nullptr)
	{
		int delay = worker->getDelay();
		return(1000.0 / static_cast<double>(delay));
	}
	else
	{
		return 0.0;
	}
}

// ----------------------------------------------------------------------------
// Public Slots
// ----------------------------------------------------------------------------

/*
 * Open new device Id
 * @param deviceId device number to open
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @return true if device has been opened and checked
 */
bool QcvThreadedVideoCapture::openDevice(const int deviceId,
										 const size_t width,
										 const size_t height)
{
	/*
	 * Try to create a new capture (if possible)
	 */
	cv::VideoCapture * localCapture = new cv::VideoCapture(deviceId);
	live = true;

	if (localCapture->isOpened())
	{
		/*
		 * If the new capture is sucessful then
		 *	- shutdown worker
		 *	- copy new capture to old one
		 *	- relaunch worker
		 */
		switchToNewCapture(localCapture, false);
		setupAndLaunch(width, height);

		return true;
	}
	else
	{
		delete localCapture;
		qWarning() << "Unable to create new capture on device" << deviceId;
	}

	return false;
}

/*
 * Open new video file
 * @param fileName video file to open
 * @param width desired width or 0 to keep capture width
 * @param height desired height or 0 to keep capture height
 * @return true if video has been opened and timer launched
 */
bool QcvThreadedVideoCapture::openFile(const QString & fileName,
									   const size_t width,
									   const size_t height)
{
	/*
	 * Try to create a new capture (if possible)
	 */
	cv::VideoCapture * localCapture = new cv::VideoCapture(fileName.toStdString());
	live = false;
	double requestedFrameRate;

	/*
	 * Video for Linux (V4L) does not support capture get/set properties
	 */
#ifndef Q_OS_LINUX
	double fileRate = localCapture->get(CV_CAP_PROP_FPS);
	if (fileRate != 0.0)
	{
		requestedFrameRate = fileRate;
	}
	else
	{
		qWarning() << "Unable to get framerate from file" << fileName;
		requestedFrameRate = defaultConstrainedFrameRate;
	}
#else
	requestedFrameRate = defaultConstrainedFrameRate;
#endif

	if (localCapture->isOpened())
	{
		/*
		 * If the new capture is sucessful then
		 *	- shutdown worker
		 *	- copy new capture to old one
		 *	- relaunch worker
		 */
		switchToNewCapture(localCapture, true, requestedFrameRate);
		setupAndLaunch(width, height);

		return true;
	}
	else
	{
		delete localCapture;
		qWarning() << "Unable to create new capture on file" << fileName;
	}

	return false;
}

/*
 * Sets the "flipped" status of captured video
 * @param value the new "flipped" status
 */
void QcvThreadedVideoCapture::setFlipped(const bool value)
{
	mutex.lock();

	flipped = value;

	mutex.unlock();
}

/*
 * Sets the gray conversion status of captured video
 * @param value the new value of gray conversion status
 * @post The #imageChanged(cv::Mat *); signal have been emitted
 */
void QcvThreadedVideoCapture::setGray(const bool value)
{
	if (value != gray)
	{
		mutex.lock();
//		qDebug() << "+ QcvThreadedVideoCapture::setGray start"
//				 << QTime::currentTime().msecsSinceStartOfDay();

		gray = value;
		if (gray)
		{
			imageDisplay = cv::Mat(imageFlipped.size(), CV_8UC1);
		}
		else
		{
			imageDisplay = imageFlipped;
		}

		emit imageChanged(&imageDisplay);

		waitForUpdate();

//		qDebug() << "+ QcvThreadedVideoCapture::setGray end"
//				 << QTime::currentTime().msecsSinceStartOfDay();
		mutex.unlock();
	}
}

/*
 * Sets capture to synchronized mode (when true) where frame rate is
 * contrained (whenever possible). Otherwise frame rate is not
 * constrained and capture may proceed as fast as possible.
 * @param value the new value of synchronized mode
 * @post The #synchronized(bool); signal has been emitted
 */
void QcvThreadedVideoCapture::setSynchronized(const bool value)
{
	if (worker != nullptr)
	{
		if (worker->isSynchronized() != value)
		{
			/*
			 * worker's setSynchronized is using mutex's lock/unlock
			 */
			worker->setSynchronized(value);
			emit synchronized(value);
		}
	}
}

/*
 * Sets the desired frame rate on capture ONLY when capture is in
 * synchronized mode
 *	- If the new frame rate is 0.0 then use natural capture frame rate.
 *	- If the new frame rate is too high for capture then capture will
 *	keeps its highest possible frame rate according to the device in use
 * @param frameRate the new frame rate to set
 * @post The #constrainedFrameRateChanged(double) signal have been emitted
 */
void QcvThreadedVideoCapture::setFrameRate(const double frameRate)
{
	if (worker != nullptr)
	{
		int delay = static_cast<int>(round(1000.0 / fabs(frameRate)));
		if (worker->getDelay() != delay)
		{
//			qDebug() << "QcvThreadedVideoCapture::setFrameRate(" << frameRate
//					 << ") : setting up delay to" << delay;
			/*
			 * worker's setDelay is using mutex's lock/unlock
			 */
			worker->setDelay(delay);
			emit constrainedFrameRateChanged(frameRate);
		}
	}
}

// ----------------------------------------------------------------------------
// Protected Methods
// ----------------------------------------------------------------------------

/*
 * Sets the desired size of captured image
 * @param width the desired width of captured image
 * @param height the desired height of captured image
 * @post The #messageChanged(QString); signal is emitted with new size
 * values
 * @see #size
 */
void QcvThreadedVideoCapture::setSize_Impl(const size_t width,
										   const size_t height)
{
	int desiredWidth = static_cast<int>(width);
	int desiredHeight = static_cast<int>(height);

	if ((size.width() != desiredWidth) ||
		(size.height() != desiredHeight)) // size is changing
	{
		mutex.lock();

		if (!imageResized.empty())
		{
			imageResized.release();
		}

		if (desiredWidth == 0 && desiredHeight == 0) // reset to original size
		{
			desiredWidth = originalSize.width();
			desiredHeight = originalSize.height();
		}

		directResize = setDirectSize(desiredWidth, desiredHeight);

		if (directResize)
		{
			imageResized = image;
		}
		else
		{
			imageResized = cv::Mat(desiredHeight, desiredWidth, image.type());
		}

		size.setWidth(imageResized.cols);
		size.setHeight(imageResized.rows);

		mutex.unlock();

		statusMessage.clear();
		statusMessage.sprintf("Size set to %dx%d", size.width(), size.height());
		emit messageChanged(statusMessage, messageDelay);

		resized = size != originalSize;
	}
}

/*
 * Release old capture in order to switch to a new capture.
 *	- Old capture is released
 *	- Worker gets terminated and destroyed
 *	- old capture is replaced by new one
 * @param newCapture the new capture to switch on
 * @param synchronized mode for the new capture
 * @param frameRate if synchronized mode is used the frame rate for
 * the synchronized mode
 * @pre new capture needs to be non null an opened
 * @post new still needs to be setup and a neww worker reccreated and
 * launched with #setupAndLaunch();
 */
void QcvThreadedVideoCapture::switchToNewCapture(cv::VideoCapture * newCapture,
												 const bool synchronized,
												 const double frameRate)
{
	mutex.lock();

	capture->release();

	mutex.unlock();

	// Wait for capture worker to terminate after capture release
	worker->wait();

	delete capture;
	capture = newCapture;

	setSynchronized(synchronized);
	if (synchronized)
	{
		setFrameRate(frameRate);
	}
}

/*
 * Wait for at least one call to update method in the worker thread:
 *	- blocks all signals (so other classes are NOT notified of update)
 *	- Create a new #updateCondition
 *	- Wait on this condition using updateCondition->wait(&mutex)
 *	- Then delete #updateCondition and set it to nullptr
 *	- unblocks all signals (so other classes can be notified of update)
 * @pre #updateCondition shall be nullptr
 * @pre mutex should be locked prior to calling this method
 */
void QcvThreadedVideoCapture::waitForUpdate()
{
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO
//			 << "waiting first update start";
	blockSignals(true); // prevent an eventual update signal
	updateCondition = new QWaitCondition();
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO
//			 << "waiting for condition";
	updateCondition->wait(&mutex); // Waiting on locked mutex
//	qDebug() << QThread::currentThreadId()
//			 << Q_FUNC_INFO
//			 << "waiting for condition terminated";
	delete updateCondition;
	updateCondition = nullptr;
	blockSignals(false);
}


/*
 * After capture has been created,
 *	- grab a first image to setup sizes
 *	- creates and connect the worker
 *	- launch the worker
 * @pre capture is not null and opened
 */
void QcvThreadedVideoCapture::setupAndLaunch(const size_t width,
											 const size_t height)
{
	assert(capture != nullptr);
	assert(worker != nullptr);
	assert(capture->isOpened());
	assert(!worker->isRunning());

	(*capture) >> image;

	originalSize.setWidth(image.cols);
	originalSize.setHeight(image.rows);
	size = originalSize; // reset size before setSize_Impl(...)

	// set up resized, directResize, size
	setSize_Impl(width, height);

	worker->start();

	mutex.lock();
	waitForUpdate();
	mutex.unlock();
}

/*
 * Update capture (with proper locking/unlocking):
 *	- Grabs a new image
 *	- if gray is on then convert
 *	- if resize is on then resize image into imageResized
 *	(otherwise set imageResized to image)
 *	- if flipped is on then flip imageResized into imageFlipped
 *	(otherwise set imageFlipped to imageResized)
 *	- if gray is on then converts imageFlipped into imageDisplay
 *	(otherwise set imageDisplay to imageFlipped)
 * @return true if new image has been grabbed from capture or false
 * when there is no more images to grab from video capture
 * @post When no image has been grabbed resizing, flipping and
 * converting to gray are not performed
 * @post If a new image is available #updated(); signal has been emitted
 */
bool QcvThreadedVideoCapture::update()
{
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "will lock";
	mutex.lock();
//	qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//					   << QThread::currentThreadId() << ";"
//					   << Q_FUNC_INFO << ";"
//					   << &mutex << ";"
//					   << "locked";

	bool updated = grab();

	if (updated)
	{
		performResize();
	}

	if (updated)
	{
		performFlip();
		performConvert();
		emitUpdated();
	}

	mutex.unlock();
//    qDebug().noquote() << QTime::currentTime().msecsSinceStartOfDay() << ";"
//                       << QThread::currentThreadId() << ";"
//                       << Q_FUNC_INFO << ";"
//                       << &mutex << ";"
//                       << "unlocked";
#ifdef Q_OS_LINUX
	/*
	 * FIXME : Artificial 10 μs sleep to ensure proper thread scheduling on
	 * Linux systems
	 */
	QThread::currentThread()->usleep(10);
#endif

	return updated;
}

/*
 * Grabs new image from capture
 * @return true if new image has been grabbed from capture or false
 * when there is no more images to grab from video capture
 * @note This method eventually deals with automatically restarting
 * the capture when video file playback has reached the end of the file
 * @post #messageChanged(); signal has been emitted if either:
 *	- There is no more frames to capture (then capture is released)
 *	- Capture has been restarted to loop video files
 */
bool QcvThreadedVideoCapture::grab()
{
	(*capture) >> image;

	if (!image.data)
	{
		/*
		 * Captured image has no data
		 */
		statusMessage.clear();

		if (live)
		{
			capture->release();
			statusMessage.sprintf("No more frames to capture ...");
			emit messageChanged(statusMessage, 0);
			qDebug("%s", statusMessage.toStdString().c_str());
		}
		else
		{
			if (!restarted)
			{
				// if video is playing from file, we'll try to restart it
				if (capture->set(CV_CAP_PROP_POS_FRAMES, 0.0))
				{
					statusMessage.sprintf("Capture restarted");
					emit messageChanged(statusMessage,
										QcvVideoCapture::messageDelay);
					qDebug("%s", statusMessage.toStdString().c_str());

					// Then update again
					restarted = true;
					return grab();
				}
			}
			capture->release();
		}
		return false;
	}
	else
	{
		restarted = false;
		/*
		 * Captured image has data
		 */
		return true;
	}
}

/*
 * If resizing is needed, resize the image
 */
void QcvThreadedVideoCapture::performResize()
{
	if (resized && !directResize)
	{
		cv::resize(image,
				   imageResized,
				   imageResized.size(),
				   0,
				   0,
				   cv::INTER_AREA);
	}
	else
	{
		imageResized = image;
	}
}

/*
 * if flipping is needed, flip the image
 */
void QcvThreadedVideoCapture::performFlip()
{
	if (flipped)
	{
		cv::flip(imageResized, imageFlipped, 1);
	}
	else
	{
		imageFlipped = imageResized;
	}
}

/*
 * if gray conversion is needed convert to gray image
 */
void QcvThreadedVideoCapture::performConvert()
{
	if (gray)
	{
		cv::cvtColor(imageFlipped, imageDisplay, CV_BGR2GRAY);
	}
	else
	{
		imageDisplay = imageFlipped;
	}
}
