/*
 * CvDetector.h
 *
 *  Created on: 29 févr. 2012
 *	  Author: davidroussel
 */

#ifndef CVDETECTOR_H_
#define CVDETECTOR_H_

#include <string>
using namespace std;

#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>
using namespace cv::xfeatures2d;

#include <CvProcessor.h>
#include <utils/MeanValue.h>

/**
 * OpenCV Feature points detector processor.
 * Detects feature points in (evt gray converted) source image and compute
 * descriptors on these keyPoints.
 * This detector dose not provide any display image (except the ones provided
 * by the CvProcessor class).
 * Which means another class will have to get the detected keyPoints and
 * eventually draw them on another image.
 * @note Please note that all virtual methods may be reimplemented in
 * CvDetector progeny such as QcvDetector which will reimplement these
 * methods with added Qt capabilities (such as signals and slots)
 * @author David Roussel
 * @date 2012/02/29
 */
class CvDetector: virtual public CvProcessor
{
	public:
		/**
		 * Feature type to detect
		 * @note The underlying type must be "int" instead of "unsigned int" to
		 * facilitate future use in widgets or controllers
		 */
		enum struct FeatureType : int
		{
			FAST_FEATURE = 0, //!< FAST: FAST Corner detection
			STAR_FEATURE = 1, //!< STAR: STAR feature
			SIFT_FEATURE = 2, //!< SIFT: Scale Invariant Feature Transform
			SURF_FEATURE = 3, //!< SURF : Speeded Up Robust Features
			ORB_FEATURE = 4,  //!< ORB : Oriented BRIEF Features
			BRISK_FEATURE = 5, //!< BRISK : Binary Robust Invariant Scalable Keypoints
			MSER_FEATURE = 6,   //!< MSER : Maximally Stable Extremal Regions
			GFTT_FEATURE = 7,   //!< GFTT : Good Features To Track
			KAZE_FEATURE = 8,   //!< KAZE Features
			AKAZE_FEATURE = 9,  //!< Accelerated KAZE Features
			FEATURE_NUMBER = 10 //!< Number of Feature types
		};

		/**
		 * Feature names to compare with arguments
		 *	- "FAST",
		 *	- "STAR",
		 *	- "SIFT",
		 *	- "SURF",
		 *	- "ORB",
		 *	- "BRISK",
		 *	- "MSER",
		 *	- "GFTT",
		 *	- "KAZE"
		 *	- "AKAZE"
		 */
		static const string FeatureNames[integral(FeatureType::FEATURE_NUMBER)];

		/**
		 * Descriptor extractor types
		 * @note The underlying type must be "int" instead of "unsigned int" to
		 * facilitate future use in widgets or controllers
		 */
		enum struct DescriptorExtractorType : int
		{
			SIFT_DESCRIPTOR = 0, //!< SIFT Descriptor: Scale Invariant Feature Transform
			SURF_DESCRIPTOR = 1, //!< SURF Descriptor: Speeded Up Robust Features
			ORB_DESCRIPTOR = 2,   //!< ORB descriptor : Oriented BRIEF
			BRISK_DESCRIPTOR = 3, //!< BRISK Descriptor : Binary Robust Invariant
								  //! Scalable Keypoints
			BRIEF_DESCRIPTOR = 4, //!< BRIEF Descriptor: Binary Robust Independent
								  //! Elementary Features
			FREAK_DESCRIPTOR = 5, //!< FREAK Descriptor: Fast Retina Keypoint
			KAZE_DESCRIPTOR = 6,  //!< KAZE Descriptor
			AKAZE_DESCRIPTOR = 7, //!< Accelerated KAZE Descriptor
			DESCRIPTOR_NUMBER = 8 //!< Number of descriptors
		};

		/**
		 * Descriptor types (as provided by Feature2D->descriptorType())
		 */
		enum struct DescriptorTypes : int
		{
			/**
			 * Unknown descriptor type
			 */
			UNKOWN_DESCRIPTOR = -1,
			/**
			 * Binary descriptor type used by AKAZE, BRIEF, BRISK, FREAK & ORB
			 */
			BINARY_DESCRIPTOR = 0,
			/**
			 * Valued descriptor type used by KAZE, SIFT & SURF
			 */
			VALUED_DESCRIPTOR = 5
		};

		/**
		 * Descriptors extractors names to compare with arguments
		 *	- "SIFT",
		 *	- "SURF",
		 *	- "ORB",
		 *	- "BRISK",
		 *	- "BRIEF",
		 *	- "FREAK"
		 *	- "KAZE"
		 *	- "AKAZE"
		 */
		static const string DescriptorNames[integral(
			DescriptorExtractorType::DESCRIPTOR_NUMBER)];

		/**
		 * Preferred descriptor for a feature.
		 * @note We're using BRIEF descriptor as a fallback for every feature
		 * which doesn't have a dedicated descriptor (FAST, STAR, MSER, GFTT)
		 */
		static const DescriptorExtractorType PreferredDescriptor[integral(
			FeatureType::FEATURE_NUMBER)];

		/**
		 * Nature of the descriptor content.
		 * Useful to know what kind of mathcher can be used with these descriptors
		 *	- SIFT: false, SIFT descriptors contains numbers
		 *	- SURF: false, SURF descriptors contains numbers
		 *	- ORB: true, ORB descriptors contains booleans
		 *	- BRISK: true, BRISK descriptors contains booleans
		 *	- BRIEF: true, BRIEF descriptors contains booleans
		 *	- FREAK: true, FREAK descriptors contains booleans
		 *	- KAZE: false, KAZE descriptors contains numbers
		 *	- AKAZE: true, KAZE descriptors contains booleans
		 */
		static const bool DescriptorBinary[integral(
			DescriptorExtractorType::DESCRIPTOR_NUMBER)];

		/**
		 * Index of processing time
		 */
		typedef enum
		{
			ALL = 0, //!< All processing (points detection + descriptors extraction)
			DETECTION, //!< Feature points detection time index
			EXTRACTION //!< Descriptors extraction time index
		} ProcessTimeIndex;

	protected:
		/**
		 * Indicates sourceimage is already gray image
		 */
		bool grayInput;

		/**
		 * Gray converted image to perform detection on;
		 */
		Mat grayImage;

		/**
		 * The type of features this detector should detect
		 */
		FeatureType featureType;

		/**
		 * The kind of descriptors to compute on keyPoints
		 */
		DescriptorExtractorType descriptorType;

		/**
		 * the name of the corresponding feature to provide to the Feature
		 * detector create method
		 */
		string featureName;

		/**
		 * the name of the corresponding descriptor to provide to the Descriptor
		 * Extractor create method
		 */
		string descriptorName;

		/**
		 * Feature detector.
		 */
		Ptr<Feature2D> featureDetector;

		/**
		 * Descriptor extractor to extract descriptors from keypoints.
		 * featureDetector and descriptorExtractor are differents which allow
		 * to extract SIFT descriptors on SURF points for instance
		 */
		Ptr<Feature2D> descriptorExtractor;

		/**
		 * Keypoints detected by the detector
		 */
		vector<KeyPoint> keyPoints;

		/**
		 * Descriptors matrix computed on the keypoints.
		 * Row i contains the descriptor of ith key point
		 * This matrix is constantly modified by DescriptorExtractor#compute
		 * so we don't initialize it
		 */
		Mat descriptors;

		/**
		 * feature points detection time in ticks
		 */
		clock_t detectionTime;

		/**
		 * feature points mean detection time and std in ticks
		 */
		ProcessTime meanDetectionTime;

		/**
		 * descriptors extraction time in ticks
		 */
		clock_t extractionTime;

		/**
		 * descriptors mean extraction time and std in ticks
		 */
		ProcessTime meanExtractionTime;

		/**
		 * Flag indicating #featureDetector and #descriptorExtractor are the
		 * same (or not)
		 */
		bool detectorAndExtractorPaired;

		/**
		 * A paired detector that will mirror this detector's parameters
		 * except the source image by sharing #featureDetector and
		 * #descriptorExtractor
		 */
		CvDetector * pairedDetector = nullptr;

		/**
		 * Indicates that paired detector (if any) must be updated every time
		 * this detector changes its parameters (feature type, descriptor type)
		 */
		bool autoUpdatePairedDetector;

		/**
		 * Indicates this CvDetector is paired to another one.
		 * In such case all setters related to #featureDetector and
		 * #descriptorExtractor are neutralized so only the master
		 * CvDetector can modify these
		 */
		bool paired;

		/**
		 * Indicates this detector has been updated.
		 * This attribute is set to true at the end of the #update method.
		 * This attribute should be accessed through the #hasUpdated() method
		 * and should be reset through the #resetUpdated(); method.
		 */
		bool updated;

		/**
		 * Setup internal attributes according to source image
		 * @param sourceImage a new source image
		 * @param fullSetup full setup is needed when source image is changed
		 * @pre sourceimage is not NULL
		 * @post only calls CvProcessor::setup and checks source image has only
		 * one channel otherwise the detector might fail
		 * @throw CvProcessorException if sourceImage is neither gray level or
		 * BGR image
		 */
		virtual void setup(Mat * sourceImage, const bool fullSetup = true)
			throw (CvProcessorException);

		/**
		 * Cleanup attributes before changing source image or cleaning class
		 * before destruction
		 */
		virtual void cleanup();

	public:
		/**
		 * Cv feature point detector creator
		 * @param sourceImage the source image
		 * @param feature the type of feature points to detect [default value is
		 * SURF Feature]
		 * @param descriptor the type of descriptor to compute on detected
		 * points [default value is SURF Descriptor]
		 * @param level Verbose level
		 * @throw CvProcessorException if feature detector or descriptor
		 * extractor are empty because they couldn't be created properly
		 */
		CvDetector(Mat * sourceImage,
				   const FeatureType feature = FeatureType::SURF_FEATURE,
				   const DescriptorExtractorType descriptor =
					   DescriptorExtractorType::SURF_DESCRIPTOR,
				   const VerboseLevel level = VERBOSE_NONE)
					   throw (CvProcessorException);

		/**
		 * Constructor of a paired detector.
		 * Allow to share an already instanciated #featureDetector and
		 * #descriptorExtractor:
		 * 	- source image, keyPoints and descriptors are specific to this
		 * 	detector
		 * 	- but the #featureDetector and #descriptorExtractor inside comes
		 * 	from the other detector
		 * @param sourceImage the new source image
		 * @param det the detector to copy the FeatureDetector and
		 * DescriptorExtractor from
		 * @param autoUpdate Indicates this detector should be auto updated when
		 * the detector it is paired with changes its parameters (feature type
		 * or descritor extractor type)
		 */
		CvDetector(Mat * sourceImage,
				   CvDetector * det,
				   const bool autoUpdate = true);

		/**
		 * Cv Feature points detector destructor
		 */
		virtual ~CvDetector();

		/**
		 * Get the image selected for display
		 * @return a reference to the image selected for display
		 */
		const Mat & getDisplayImage() const;

		/**
		 * Get the pointer to the image selected for display
		 * @return a pointer to the image selected for display
		 */
		Mat * getDisplayImagePtr();

		/**
		 * Feature points detector update.
		 * Detects keyPoints in source image and compute descriptors on these
		 * points
		 */
		virtual void update();

		/**
		 * Clears currenlty detected KeyPoints
		 * @post keyPoints have been cleared
		 */
		virtual void clear();

		/**
		 * Gets the current feature type
		 * @return the current feature type
		 */
		FeatureType getFeatureType() const;

		/**
		 * Sets a new feature type and creates the #featureDetector
		 * @param featureType the new feature type to set
		 * @post If there was an already set #descriptorExtractor to the desired
		 * #featureType then the #descriptorExtractor smart pointer has been
		 * copied to the #featureDetector smart pointer directly and
		 * #detectorAndExtractorPaired has been set to true
		 * @post If a #pairedDetector has been set then its #featureDetector,
		 * #featureName and #detectorAndExtractorPaired are set accordingly
		 * @throw CvProcessorException if feature detector is empty because
		 * it couldn't be created properly
		 */
		void setFeatureType(const FeatureType featureType)
			throw (CvProcessorException);

		/**
		 * Get the currently detected keypoints
		 * @return the currently detected keypoints
		 */
		const vector<KeyPoint> & getKeyPoints() const;

		/**
		 * Get the currently detected keypoints pointer
		 * @return the currently detected keypoints
		 */
		const vector<KeyPoint> * getKeyPointsPtr() const;

		/**
		 * Get the current keypoints number
		 * @return the current keypoints number
		 */
		size_t getNbKeypoints() const;

		/**
		 * Extract selected keypoints into another keypoint set.
		 * This method is used to extract matched keypoints or inliers
		 * keypoints in order to draw them.
		 * @param extracted the other keypoints set to store extrated keypoints
		 * @param indexes the indices of keypoints to extract
		 * @param subindexes the subset of the previous indexes to extract
		 * [default value is empty subset so all indexes are extracted]
		 * @post extracted is filled with extracted keypoints
		 */
		void extractSelectedKeypoints(vector<KeyPoint> & extracted,
									  const vector<int> & indexes,
									  const vector<int> & subindexes = vector<
										  int>(0));

		/**
		 * Gets the current descriptor type
		 * @return the current descriptor type
		 */
		DescriptorExtractorType getDescriptorExtractorType() const;

		/**
		 * Sets a new descriptor extrator type
		 * @param descriptorType the new descriptor extractor type to set
		 * @post Tf there was an already set #featureDetector to the desired
		 * #descriptorType then the #featureDetector smart pointer has been
		 * copied to the #descriptorExtractor smart pointer directly and
		 * #detectorAndExtractorPaired has been set to true
		 * @post If a #pairedDetector has been set then its #descriptorExtractor,
		 * #descriptorName and #detectorAndExtractorPaired are set acoordingly
		 * @throw CvProcessorException if descriptor extractor is empty because
		 * it couldn't be created properly
		 */
		void setDescriptorExtractorType(const DescriptorExtractorType descriptorType)
			throw (CvProcessorException);

		/**
		 * Gets the preferred descriptor norm (should be NormTypes::NORM_HAMMING (6)
		 * for HAMMING distance type suitable for binary descriptors or
		 * NormTypes::NORM_L2 (4) L2 distances suitable for valued descriptors.
		 * @return the descriptor size used by the descriptor extractor or -1
		 * if there is no descriptor extractor
		 */
		int getDescriptorNorm() const;

		/**
		 * Gets the preferred descriptor size
		 * @return the descriptor size used by the descriptor extractor or 0
		 * if there is no descriptor extractor or if the algorithm does not
		 * provide descriptors
		 */
		int getDescriptorSize() const;

		/**
		 * Gets the preferred descriptor type (should be
		 * DescriptorTypes::BINARY_DESCRIPTOR (0) for binary
		 * descriptors or DescriptorTypes::VALUED_DESCRIPTOR (5) for valued
		 * descriptors.)
		 * @return the descriptor type used by the descriptor extractor or -1
		 * if there is no descriptor extractor
		 */
		int getDescriptorType() const;

		/**
		 * Gets keypoints descriptors Matrix
		 * @return the current descriptors computed on keyPoints
		 */
		const Mat & getDescriptors() const;

		/**
		 * Gets keypoints descriptors Matrix pointer
		 * @return the current descriptors computed on keyPoints
		 */
		Mat * getDescriptorsPtr();

		/**
		 * Return processor processing time of step index
		 * @param index index of the step which processing time is required,
		 * 0 indicates all steps, 1 indicates points detection time and
		 * 2 indicates descriptors extraction time.
		 * @return the processing time of step index.
		 * @see #ProcessTimeIndex
		 */
		double getProcessTime(const size_t index = 0) const;

		/**
		 * Return processor mean processing time of step index
		 * @param index index of the step which processing time is required,
		 * 0 indicates all steps, 1 indicates points detection time and
		 * 2 indicates descriptors extraction time.
		 * @return the processing time of step index.
		 * @see #ProcessTimeIndex
		 */
		double getMeanProcessTime(const size_t index = 0) const;

		/**
		 * Return processor processing time standard deviation of step index
		 * @param index index of the step which processing time is required,
		 * 0 indicates all steps, 1 indicates points detection time and
		 * 2 indicates descriptors extraction time.
		 * @return the processing time of step index.
		 * @see #ProcessTimeIndex
		 */
		double getStdProcessTime(const size_t index = 0) const;

		/**
		 * Reset mean and std process time in order to re-start computing
		 * new mean and std process time values.
		 * @note this reimplementation takes into account detectionTime and
		 * extractionTime.
		 */
		void resetMeanProcessTime();

		/**
		 * Set the detector in argument as a paired detector of this.
		 * The paired detector will then use the #featureDetector and
		 * #descriptorExtractor as its own keypoints detector and descriptor
		 * extractor
		 * @param detector the detector to pair with this detector
		 * @param autoUpdate Indicates the paired detector must be updated
		 * whenever this detector changes its parameters (feature type or
		 * descriptor extractor type)
		 */
		virtual void pairDetector(CvDetector * detector,
								  const bool autoUpdate = true);

		/**
		 * Reset the #pairedDetector to null
		 */
		virtual void unpairDetector();

		/**
		 * Get Feature detector algorithm (featureDetector)
		 * @return a smart pointer to the featureDetector
		 * @note don't forget to release this pointer when you're done in order
		 * for this smart pointer to automatically destroye its content
		 */
		Ptr<Feature2D> getDetectorAlgorithm();

		/**
		 * Get Descriptor extractor algorithm (descriptorExtractor)
		 * @return a smart pointer to the descriptorExtractor
		 * @note don't forget to release this pointer when you're done in order
		 * for this smart pointer to automatically destroye its content
		 */
		Ptr<Feature2D> getExtractorAlgorithm();

		/**
		 * Gets the #updated status of the detector
		 * @return the #updated status of the detector
		 */
		bool hasUpdated() const;

		/**
		 * Resets the #updated status to false, typically after using
		 * #hasUpdated() method to check if the detctor has been updated
		 */
		void resetUpdated();


	private:
		/**
		 * Prints info on the detector
		 * @param ptr smart pointer to the detector
		 * @param  type of detector
		 */
		void detectorInfo(const Ptr<Feature2D> & ptr, const FeatureType type);
		/**
		 * Prints info on the feature points extractor
		 * @param ptr smart pointer to the feature points extractor
		 * @param  type of extractor
		 */
		void extractorInfo(const Ptr<Feature2D> & ptr,
						   const DescriptorExtractorType type);

//		/*
//		 * Prints info on any algorithm (Detector or extractor)
//		 * @param ptr smart pointer to the algorithm
//		 * @warning This method is obsolete since OpenCV 3.x Algorithm does not
//		 * feature introscpection anymore
//		 */
//		void algorithmInfo(const Ptr<Algorithm> & ptr);

	protected:
		/**
		 * Converts source image to gray.
		 * Used in the #update() method
		 * @note This method should be reimplemented in Qt flavored sub classes
		 * in order to use a lock on the source image
		 */
		virtual void convertSourceImageToGray();

		/**
		 * Detect keypoints on gray image.
		 * Used in the #update() method
		 * @pre source image has been converted to gray or is gray
		 * @post detected keypoints are stored in #keyPoints
		 * @post #detectionTime and #meanDetectionTime have been updated
		 * @note This method should be reimplemented in Qt flavored sub classes
		 * in order to use a lock on keypoints
		 */
		virtual void detectKeypoints();

		/**
		 * Extract descriptors on keypoints detected in gray image.
		 * Used in the #update() method.
		 * @pre keypoints have been detected (otherwise this method will do nothing)
		 * @post extracted descriptors are stored in #descriptors matrix
		 * @post #extractionTime and #meanExtractionTime have been updated
		 * @note This method should be reimplemented in Qt flavored sub classes
		 * in order to use a lock on descriptors
		 */
		virtual void extractDescriptors();
};

#endif /* CVDETECTOR_H_ */
