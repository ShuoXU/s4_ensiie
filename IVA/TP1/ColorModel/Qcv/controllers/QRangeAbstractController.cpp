#include <cassert>

#include <QDebug>
#include <QVector>
#include <QSpinBox>
#include <QDoubleSpinBox>

#include <Qcv/controllers/QRangeAbstractController.h>

/*
 * Constructor from RangeValue, mutex and factor
 * @param range the RangeValue<T> to reference
 * @param lock the lock (from the processor containing the RangeValue) to
 * use when modifying the RangeValue
 * @param factor the factor to apply to the range values when emitting or
 * the factor to divide outside values when setting a new range value.
 * @param callback the call back to call for setting the value instead
 * of settting the value directly
 * @param settings the settings to use to set initial value and/or
 * save the current value (if different from default value) to settings
 * @param settingsName the name to use when reading or writing the
 * parameter value to settings (if any)
 */
template<typename T>
QRangeAbstractController<T>::QRangeAbstractController(RangeValue<T> * const range,
													  QMutex * const lock,
													  const T & factor,
													  function<void(const T)> * const callback,
													  QSettings * settings,
													  const QString & settingsName) :
	QAbstractController<RangeValue<T>, T>(range,
										  lock,
										  callback,
										  settings,
										  settingsName),
	factor(factor)
{
	assert(factor >= T(1));
}

/*
 * Copy Constructor
 * @param rc the range controller to copy
 */
template<typename T>
QRangeAbstractController<T>::QRangeAbstractController(const QRangeAbstractController<T> & rc) :
	QRangeAbstractController<T>(rc.managedValue,
								rc.lock,
								rc.factor,
								rc.callback,
								rc.settings,
								rc.settingsName)
{
}

/*
 * Move Constructor
 * @param rc the range controller to move
 */
template<typename T>
QRangeAbstractController<T>::QRangeAbstractController(QRangeAbstractController<T> && rc) :
	QRangeAbstractController<T>(rc.managedValue,
								rc.lock,
								rc.factor,
								rc.callback,
								rc.settings,
								rc.settingsName)
{
}

/*
 * Destructor
 */
template<typename T>
QRangeAbstractController<T>::~QRangeAbstractController()
{
}

/*
 * Setup a widget to reflect the managed value.
 * Since we do not know yet what to do with the widget, this method only
 * checks if the widget is already part of the connected widgets or not.
 * @param widget the widget to setup
 * @return true if the widget was correclty set up, false otherwise
 * (for example when the widget was already part of connected widgets).
 * @note subclasses overloads might actually configure this widget
 * properly and also setup connections from/to this widget.
 */
template<typename T>
bool QRangeAbstractController<T>::setupSource(QObject *source)
{
	QAbstractSpinBox * spinBox = dynamic_cast<QAbstractSpinBox *>(source);
	if (spinBox != nullptr)
	{
		return setupSpinBox(spinBox);
	}

	QAbstractSlider * slider = dynamic_cast<QAbstractSlider *>(source);
	if (slider != nullptr)
	{
		return setupSlider(slider);
	}

	QAbstractButton * button = dynamic_cast<QAbstractButton *>(source);
	if (button != nullptr)
	{
		return addResetButton(button);
	}

	qWarning() << "QRangeAbstractController<T>::setupWidget:"
			   << "Can't cast to QAbstract[SpinBox|Slider|Button]";

	return false;
}

/*
 * Setup a spin box with the values in the RangeValue and connect
 * the necessary signals and slots
 * @param spinBox the spinbox to setup with
 * @post If this spinbox was not part of the widgets set, then the
 * spinBox has been setup with RangeValue values and bidirectionnal
 * connections between this and the spinBox have been created.
 */
template<typename T>
bool QRangeAbstractController<T>::setupSpinBox(QAbstractSpinBox * spinBox)
{
	if (QAbstractController<RangeValue<T>, T>::setupSource(spinBox))
	{
		bool setupOk = false;

		QSpinBox * intSpinBox = dynamic_cast<QSpinBox *>(spinBox);
		if (intSpinBox != nullptr)
		{
			intSpinBox->setMinimum(managedValue->min() * factor);
			intSpinBox->setMaximum(managedValue->max() * factor);
			intSpinBox->setSingleStep(managedValue->step() * factor);
			intSpinBox->setValue(managedValue->value() * factor);
			setupOk = true;
		}

		QDoubleSpinBox * doubleSpinBox = dynamic_cast<QDoubleSpinBox *>(spinBox);
		if (doubleSpinBox != nullptr)
		{
			doubleSpinBox->setMinimum(managedValue->min() * factor);
			doubleSpinBox->setMaximum(managedValue->max() * factor);
			doubleSpinBox->setSingleStep(managedValue->step() * factor);
			doubleSpinBox->setValue(managedValue->value() * factor);
			setupOk = true;
		}

		if (!setupOk)
		{
			int rindex = sources.indexOf(spinBox);
			if (rindex != -1)
			{
				sources.removeAt(rindex);
				sourceLockStatus.removeAt(rindex);
			}
		}

		return setupOk;

		/*
		 * NOTE: in subclasses you'll have to setup the following connections
		 * 	spinBox --> valueChanged(T) to this <-- setValue(T)
		 * 	this --> valueChanged(T) to spinBox <-- setValue(T)
		 * 	And to avoid signal/slot loops this->setValue should block
		 * 	signals on all connected widgets (including spinBox) before
		 * 	emitting a valueChanged signal.
		 */
	}
	else
	{
		qWarning() << spinBox << "has not been setup correctly";
	}

	return false;
}

/*
 * Setup a slider with the values in the RangeValue (and connects the
 * necessary signals and slots).
 * @param slider the slider to setup according to RangeValue values
 * @post If this slider was not part of the widgets set, then the
 * slider has been setup with RangeValue steps and bidirectionnal
 * connections between this and the slider have been created.
 */
template<typename T>
bool QRangeAbstractController<T>::setupSlider(QAbstractSlider * slider)
{
	if (QAbstractController<RangeValue<T>, T>::setupSource(slider))
	{
		slider->setMinimum(0);
		slider->setMaximum(static_cast<int>(managedValue->nbSteps()));
		slider->setSingleStep(1);
		slider->setValue(static_cast<int>(managedValue->index()));

		/*
		 * NOTE: in subclasses you'll have to setup the following connections
		 * 	slider --> sliderMoved(int) to this <-- setIndex(int)
		 * 	this --> indexChanged(int) to slider <-- setValue(int)
		 *	this --> indexRangeChanged(int,int) to slider <-- setRange(int,int)
		 * 	And to avoid signal/slot loops this->setIndex should block
		 * 	signals on all connected widgets (including slider) before
		 * 	emitting an indexChanged signal.
		 */

		return true;
	}
	else
	{
		qWarning() << slider << "has not been setup correctly";
	}


	return false;
}

/*
 * Sets new value in the range value.
 * @param value the "outside" value to set (the "oustside" value differs
 * from the "inside" value (stored in #managedValue) by #factor:
 * "outside value" = "inside value" * factor
 * @return true if the value has been set without correction, false
 * otherwise. In any case a value is set according the rules of
 * RangeValue<T>::setClosestValue
 * @note slot typically used by QSpinBox when its value changes
 * @note Please note that if result is false a signal needs to be sent
 * to the connected UI to ajust the value since it may have been
 * slighltly modified.
 * @post The following signals have been emitted (by subclasses):
 *	- valueChanged(T)
 *	- indexChanged(size_t)
 *	- updated()
 * @see RangeValue<T>::setClosestValue(const T &)
 */
template<typename T>
bool QRangeAbstractController<T>::setValue(const T value)
{
	/*
	 * Note that "value" is "outside value" here and needs to be converted
	 * to "inside value"  = "outside value" / factor before storage
	 */
	if (value != (managedValue->value() * factor))
	{
		bool result = false;
		T oldValue = managedValue->value();

		bool hasLock = (lock != nullptr);

		if (hasLock)
		{
			lock->lock();
		}

		if (callback == nullptr)
		{
			result = managedValue->setClosestValue(value/factor);
		}
		else
		{
			result = true;
			(*callback)(value/factor);
		}

		if (!checkConstraints())
		{
			// Get back to old value
			if (callback == nullptr)
			{
				managedValue->setValue(oldValue);
			}
			else
			{
				(*callback)(oldValue);
			}
			result = false;
		}

		// Saves the current "inside" value to preferences (if possible)
		if ((settings != nullptr) &&
			(settingsName.size() > 0) &&
			(managedValue->value() != oldValue))
		{
			settings->setValue(settingsName, managedValue->value());
		}

		if (hasLock)
		{
			lock->unlock();
		}

		T newValue = managedValue->value();

		/*
		 * Block signals in connected widgets
		 */
		blockSignalsOnConnectedWidgets();

		/*
		 * Emits signals
		 */
		emitValueChanged(newValue * factor);
		emitIndexChanged(static_cast<int>(managedValue->index()));
		emitUpdated();

		/*
		 * Unblock signals in connected widgets
		 */
		unblockSignalsOnConnectedWidgets();

		return result;
	}

	return true;
}

/*
 * Sets a new value corresponding to the index
 * @param index the index to set the new value to
 * @return true in index is valid (the corresponding value is less or
 * equal to the max value) and different from current index
 * @post The following signals have been emitted:
 *	- valueChanged(T)
 *	- indexChanged(size_t) if the resulting index is different from the
 *	one requested
 *	- updated()
 */
template<typename T>
bool QRangeAbstractController<T>::setIndex(const int index)
{
	int oldIndex = static_cast<int>(managedValue->index());
	T oldValue = managedValue->value();
	if (index != oldIndex)
	{
		bool hasLock = (lock != nullptr);
		if (hasLock)
		{
			lock->lock();
		}

		bool result = false;
		/*
		 * If there is an active callback, we should use it instead of
		 * the direct setIndex, so we have to compute the resulting value
		 * ourself instead of using the setIndex of the managed value
		 */
		if (callback != nullptr)
		{
			T eqValue = indexValue(index);
			(*callback)(eqValue);
			result = true;
		}
		else
		{
			result = managedValue->setIndex(static_cast<size_t>(index));
		}

		if (!checkConstraints())
		{
			// Get back to old index
			if (callback != nullptr)
			{
				(*callback)(oldValue);
			}
			else
			{
				managedValue->setIndex(static_cast<size_t>(oldIndex));
			}
			result = false;
		}

		if (hasLock)
		{
			lock->unlock();
		}

		/*
		 * Block signals in connected widgets
		 */
		blockSignalsOnConnectedWidgets();

		/*
		 * Emits signals
		 */
		emitValueChanged(managedValue->value() * factor);

		if (!result) // index was invalid so it's not what was requested
		{
			emitIndexChanged(static_cast<int>(managedValue->index()));
		}
		emitUpdated();

		/*
		 * Unblock signals in connected widgets
		 */
		unblockSignalsOnConnectedWidgets();

		return result;
	}
	else
	{
		return false;
	}
}

/*
 * Set new min value in the range
 * @param value the new min value in the range
 * @return true if the new min value has been set without correction,
 * false otherwise. The underlying RangeValue<T> enforce min value
 * consistency
 * @post The following signals have been emitted (by subclasses):
 *	- minChanged(T)
 *	- rangeChanged(T min, T max)
 *	- indexRangeChanged(0, nbSteps)
 *	- stepChanged(T) if the step value is modified by setting new min
 * @see RangeValue<T>::setMin(const T &)
 */
template<typename T>
bool QRangeAbstractController<T>::setMin(const T & value)
{
	bool result = false;

	if (value != managedValue->min())
	{
		T oldStepValue = managedValue->step();
		result = managedValue->setMin(value);

//		blockSignalsOnConnectedWidgets();

		T _min = managedValue->min();
		T _max = managedValue->max();
		emitMinChanged(_min);
		emitRangeChanged(_min, _max);
		emitIndexRangeChanged(0, managedValue->nbSteps());

		T newStepValue = managedValue->step();
		if (oldStepValue != newStepValue)
		{
			emitStepChanged(newStepValue);
		}

//		unblockSignalsOnConnectedWidgets();
	}

	return result;
}

/*
 * Set new max value in the range
 * @param value the new max value in the range
 * @return true if the new max value has been set without correction,
 * false otherwise. The underlying RangeValue<T> enforce max value
 * consistency
 * @post The following signals have been emitted (by subclasses):
 *	- maxChanged(T)
 *	- rangeChanged(T min, T max)
 *	- indexRangeChanged(0, nbSteps)
 *	- stepChanged(T) if the step value is modified by setting new max
 * @see RangeValue<T>::setMax(const T &)
 */
template<typename T>
bool QRangeAbstractController<T>::setMax(const T & value)
{
	bool result = false;

	if (value != managedValue->max())
	{
		T oldStepValue = managedValue->step();
		result = managedValue->setMax(value);

//		blockSignalsOnConnectedWidgets();

		T _min = managedValue->min();
		T _max = managedValue->max();
		emitMaxChanged(_max);
		emitRangeChanged(_min, _max);
		emitIndexRangeChanged(0, managedValue->nbSteps());

		T newStepValue = managedValue->step();
		if (oldStepValue != newStepValue)
		{
			emitStepChanged(newStepValue);
		}

//		unblockSignalsOnConnectedWidgets();
	}

	return result;
}

/*
 * Set new step value in the range
 * @param value the new step value in the range
 * @return true if the new step value has been set without correction,
 * false otherwise. The underlying RangeValue<T> enforce step value
 * consistency
 * @see RangeValue<T>::setStep(const T &)
 */
template<typename T>
bool QRangeAbstractController<T>::setStep(const T & value)
{
	bool result = false;

	if (value != managedValue->step())
	{
		T oldMin = managedValue->min();
		T oldMax = managedValue->max();

		result = managedValue->setStep(value);

//		blockSignalsOnConnectedWidgets();

		emitStepChanged(managedValue->step());
		emitIndexRangeChanged(0, managedValue->nbSteps());

		if (result)
		{
			// min & max may have been modified by setStep
			T newMin = managedValue->min();
			if (oldMin != newMin)
			{
				emitMinChanged(newMin);
			}

			T newMax = managedValue->max();
			if (oldMax != newMax)
			{
				emitMaxChanged(newMax);
			}
		}

//		unblockSignalsOnConnectedWidgets();
	}

	return result;
}

/*
 * Reset managed value to its default value
 * @return true if the value has been reset to its default value exactly,
 * false if the reset has triggered bounds or step modifications
 */
template<typename T>
bool QRangeAbstractController<T>::reset()
{
	return setValue(managedValue->defaultValue());
}

/*
 * Refresh all connected UI.
 * When the internal managedValue has been modified outside of this
 * controller, all connected UIs should be refreshed
 * @note This method should be implemented as a slot in sub-classes.
 * @post The following signals have been emitted (by subclasses):
 *	- minChanged(T)
 *	- maxChanged(T)
 *	- rangeChanged(T min, T max)
 *	- stepChanged(T)
 *	- valueChanged(T)
 *	- indexRangeChanged(0, nbSteps)
 *	- indexChanged(size_t)
 */
template<typename T>
void QRangeAbstractController<T>::refresh()
{
	/*
	 * Block signals in connected widgets to avoid signal/slots loops
	 */
	blockSignalsOnConnectedWidgets();

	// first emit all possible signals
	T _min = managedValue->min();
	T _max = managedValue->max();
	emitMinChanged(_min);
	emitMaxChanged(_max);
	emitRangeChanged(_min, _max);
	emitStepChanged(managedValue->step());
	emitValueChanged(managedValue->value());
	emitIndexRangeChanged(0, managedValue->nbSteps());
	emitIndexChanged(managedValue->index());

//	qDebug().nospace() << sources.at(0)
//					   << "Range value updated = ["
//					   << managedValue->min()
//					   << "..."
//					   << managedValue->value()
//					   << "..."
//					   << managedValue->max()
//					   << "](±"
//					   << managedValue->step()
//					   << ")";


	/*
	 * Then for widgets that don't react to signals refresh their values directly
	 * Typically
	 *	- SpinBoxes : min, max & step values have to be set through setters
	 *  - Sliders : step has to be set through setters but is always 1
	 * This could only be done in subclasses
	 */

	/*
	 * Unblock signals in connected widgets
	 */
	unblockSignalsOnConnectedWidgets();
}

/*
 * Value corresponding to an index in the range value.
 * If there is a callback to set the value, we must imperatively use it
 * to set values, so we can't use the managed value setIndex directly,
 * and therefore we have to compute the resulting value of an index
 * in the the range by ourselves.
 * @param index the index in the range value
 * @return the value corresponding to the index in the range value
 */
template<typename T>
T QRangeAbstractController<T>::indexValue(const int index)
{
	T _min = managedValue->min();
	T _max = managedValue->max();
	T _step = managedValue->step();
	if (index < 0)
	{
		return _min;
	}
	T value = _min + (_step * T(index));
	if (value <= _max)
	{
		return value;
	}
	else
	{
		return _max;
	}
}

/*
 * Checks if the argument controller is compatible with this controller:
 * By checking they both have the same range
 * @param controller the controller to check
 * @return true if the argument controller is compatible with
 * this controller.
 * @note This implementation always return true but subclasses may
 * overload this method to introduce more compatibility controls
 */
template<typename T>
bool QRangeAbstractController<T>::checkMirror(QAbstractController<RangeValue<T>, T> * controller)
{
	QRangeAbstractController<T> * erController = dynamic_cast<QRangeAbstractController<T> *>(controller);
	if (erController != nullptr)
	{
		return (managedValue->min() == erController->managedValue->min()) &&
			   (managedValue->max() == erController->managedValue->max());
	}

	return false;
}

// ----------------------------------------------------------------------------
// Proto instantiation of QRangeAbstractController<int>
// ----------------------------------------------------------------------------
template class QRangeAbstractController<int>;
template class QRangeAbstractController<double>;

