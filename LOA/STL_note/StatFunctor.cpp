/*
 * StatFunctor.cpp
 *
 *  Created on: 21 févr. 2013
 *      Author: davidroussel
 */

#include "StatFunctor.h"

#include <cmath>	// for sqrt

/*
 * Constructeur par défaut.
 * Initialise la somme des éléments, la somme des éléments au carré
 * ainsi que le nombre d'éléments à 0.
 */
template<class T>
#if __cplusplus < 201703L
StatFunctor<T>::StatFunctor() throw (TypeException)
#else
StatFunctor<T>::StatFunctor()
#endif
// TODO Liste d'initialisation
:sum(0),sum_carre(0),size(0)
{
	if (!is_arithmetic<T>::value)
	{
		throw TypeException("StatFunctor<T>::StatFunctor: Not arithmetic type");
	}
}

/*
 * TODO Opérateur d'appel de fonction
 * @param value valeur à ajouter à la somme des valeurs et à la somme
 * des valeurs au carré.
 * @post le nombre d'éléments est incrémenté.
 */
template<class T>
void StatFunctor<T>::operator()(const T & v){
	this->sum += v;
	this->sum_carre += v*v;
	this->size += 1;
}
/*
 * TODO calcul de la valeur de la moyenne : E(X) = sum/nbElements
 * @return la valeur moyenne des valeurs additionnées.
 */
template<class T>
double StatFunctor<T>::moyenne(){
	double means = sum/(double)size;
	return means;
}

/*
 * TODO calcul de l'écart type : sqrt(E(X^2) - E(X)^2)
 * @return l'écart type des valeurs additionnées
 */
template<class T>
double StatFunctor<T>::ecart_type(){
	double std = sqrt(sum_carre/(double)size - (sum/(double)size * sum/(double)size));
	return std;
}

/*
 * TODO Remise à zéro des statistiques
 * @post la somme des élements, la somme des éléments au carré ainsi
 * que le nombre d'éléments ont été remis à 0
 */
template<class T>
void StatFunctor<T>::remise(){
	this->sum = 0;
	this->sum_carre = 0;
	this->size = 0;
}

// ----------------------------------------------------------------------------
// Proto instanciations
// ----------------------------------------------------------------------------
/*
 * Instanciation du template StatFunctor avec des <int>
 */
template class StatFunctor<int>;

/*
 * Instanciation du template StatFunctor avec des <float>
 */
template class StatFunctor<float>;

/*
 * Instanciation du template StatFunctor avec des <double>
 */
template class StatFunctor<double>;
