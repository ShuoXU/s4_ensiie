/********************************************************************************
** Form generated from reading UI file 'QcvRangeValueIntWidget.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QCVRANGEVALUEINTWIDGET_H
#define UI_QCVRANGEVALUEINTWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QcvRangeValueIntWidget
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_1;
    QLabel *titleLabel;
    QSpinBox *spinBox;
    QPushButton *resetButton;
    QHBoxLayout *horizontalLayout_2;
    QSlider *slider;

    void setupUi(QWidget *QcvRangeValueIntWidget)
    {
        if (QcvRangeValueIntWidget->objectName().isEmpty())
            QcvRangeValueIntWidget->setObjectName(QStringLiteral("QcvRangeValueIntWidget"));
        QcvRangeValueIntWidget->resize(104, 60);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(QcvRangeValueIntWidget->sizePolicy().hasHeightForWidth());
        QcvRangeValueIntWidget->setSizePolicy(sizePolicy);
        QcvRangeValueIntWidget->setMinimumSize(QSize(0, 0));
        verticalLayout = new QVBoxLayout(QcvRangeValueIntWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_1 = new QHBoxLayout();
        horizontalLayout_1->setSpacing(9);
        horizontalLayout_1->setObjectName(QStringLiteral("horizontalLayout_1"));
        titleLabel = new QLabel(QcvRangeValueIntWidget);
        titleLabel->setObjectName(QStringLiteral("titleLabel"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(titleLabel->sizePolicy().hasHeightForWidth());
        titleLabel->setSizePolicy(sizePolicy1);

        horizontalLayout_1->addWidget(titleLabel);

        spinBox = new QSpinBox(QcvRangeValueIntWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_1->addWidget(spinBox);

        resetButton = new QPushButton(QcvRangeValueIntWidget);
        resetButton->setObjectName(QStringLiteral("resetButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(resetButton->sizePolicy().hasHeightForWidth());
        resetButton->setSizePolicy(sizePolicy2);
        resetButton->setMaximumSize(QSize(20, 20));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/icons/rotate-16.png"), QSize(), QIcon::Normal, QIcon::Off);
        resetButton->setIcon(icon);

        horizontalLayout_1->addWidget(resetButton);


        verticalLayout->addLayout(horizontalLayout_1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        slider = new QSlider(QcvRangeValueIntWidget);
        slider->setObjectName(QStringLiteral("slider"));
        slider->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(slider);


        verticalLayout->addLayout(horizontalLayout_2);


        retranslateUi(QcvRangeValueIntWidget);
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));
        QObject::connect(slider, SIGNAL(valueChanged(int)), spinBox, SLOT(setValue(int)));

        QMetaObject::connectSlotsByName(QcvRangeValueIntWidget);
    } // setupUi

    void retranslateUi(QWidget *QcvRangeValueIntWidget)
    {
        QcvRangeValueIntWidget->setWindowTitle(QApplication::translate("QcvRangeValueIntWidget", "Form", nullptr));
        titleLabel->setText(QApplication::translate("QcvRangeValueIntWidget", "title", nullptr));
        resetButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class QcvRangeValueIntWidget: public Ui_QcvRangeValueIntWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QCVRANGEVALUEINTWIDGET_H
