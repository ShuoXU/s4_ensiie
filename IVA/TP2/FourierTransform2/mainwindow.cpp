#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QObject>
#include <QFileDialog>
#include <QWindow>
#include <QDebug>
#include <assert.h>

#include <Qcv/matWidgets/QcvMatWidgetImage.h>
#include <Qcv/matWidgets/QcvMatWidgetLabel.h>
#include <Qcv/matWidgets/QcvMatWidgetGL.h>

/*
 * MainWindow constructor.
 * @param capture the capture QObject to capture frames from devices
 * or video files
 * @param processor Fourier transform and filter processor
 * @param parent parent widget
 */
MainWindow::MainWindow(QcvVideoCapture * capture,
					   QcvDFT * processor,
					   QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow),
	capture(capture),
	processor(processor),
	preferredWidth(341),
	preferredHeight(256),
	preferredAspectRatio(double(preferredWidth) / double(preferredHeight)),
	keepAspectRatio(true),
	meanFrameRate(0.0)
{
	ui->setupUi(this);

	// ------------------------------------------------------------------------
	// Assertions
	// ------------------------------------------------------------------------
	assert(capture != NULL);

	assert(processor != NULL);

	ui->scrollAreaSource->setBackgroundRole(QPalette::Mid);
	ui->scrollAreaSpectrum->setBackgroundRole(QPalette::Mid);
	ui->scrollAreaInverse->setBackgroundRole(QPalette::Mid);

	connect(this, SIGNAL(sendMessage(QString,int)),
			ui->statusBar, SLOT(showMessage(QString,int)));

	// ------------------------------------------------------------------------
	// Special widgets initialisation
	// ------------------------------------------------------------------------
	// Replace QcvMatWidget instances with QcvMatWidgetImage instances
	// sets image widget sources for the first time
	// connects processor->update to image Widgets->updated
	// connects processor->image changed to image widgets->setSourceImage
	setRenderingMode(RENDER_IMAGE);

	// ------------------------------------------------------------------------
	// UI setup according to processor
	// ------------------------------------------------------------------------
	setupUIFromProcessor();

	// ------------------------------------------------------------------------
	// UI setup according to capture options
	// ------------------------------------------------------------------------
	// Capture, processor and this messages to status bar
	connect(capture, SIGNAL(messageChanged(QString,int)),
			ui->statusBar, SLOT(showMessage(QString,int)));
	connect(capture, SIGNAL(frameRateChanged(double)),
			this, SLOT(onCaptureFrameRate_changed(double)));
	connect(capture, SIGNAL(constrainedFrameRateChanged(double)),
			this, SLOT(resetMeanFrameRate()));
	connect(capture, SIGNAL(imageChanged(cv::Mat*)),
			this, SLOT(resetMeanFrameRate()));

	// Sets size radioButton states
	if (capture->isResized())
	{
		/*
		 * Initial Size radio buttons configuration
		 */
		ui->radioButtonOrigSize->setChecked(false);
		ui->radioButtonCustomSize->setChecked(true);
		/*
		 * Initial Size menu items configuration
		 */
		ui->actionOriginalSize->setChecked(false);
		ui->actionConstrainedSize->setChecked(true);

		QSize size = capture->getSize();
		qDebug("Capture->size is %dx%d", size.width(), size.height());
		preferredWidth = size.width();
		preferredHeight = size.height();
	}
	else
	{
		/*
		 * Initial Size radio buttons configuration
		 */
		ui->radioButtonCustomSize->setChecked(false);
		ui->radioButtonOrigSize->setChecked(true);

		/*
		 * Initial Size menu items configuration
		 */
		ui->actionConstrainedSize->setChecked(false);
		ui->actionOriginalSize->setChecked(true);
	}

	// Sets spinboxes preferred size (without aspect ratio)
	bool oldKeepAspectRatio = keepAspectRatio;
	keepAspectRatio = false;
	ui->spinBoxWidth->setValue(preferredWidth);
	ui->spinBoxHeight->setValue(preferredHeight);
	keepAspectRatio = oldKeepAspectRatio;
	ui->checkBoxAR->setChecked(keepAspectRatio);

	// Sets flipCheckbox and menu item states
	bool flipped = capture->isFlipped();
	ui->actionFlip->setChecked(flipped);
	ui->checkBoxFlip->setChecked(flipped);

	// Sets grayCheckbox and menu item states
	bool gray = capture->isGray();
	ui->actionGray->setChecked(gray);
	ui->checkBoxGray->setChecked(gray);

	// Connects UI requests to capture
	connect(this, SIGNAL(sizeChanged(const QSize &)),
			capture, SLOT(setSize(const QSize &)));
	connect(this, SIGNAL(deviceChanged(int,size_t,size_t)),
			capture, SLOT(openDevice(int,size_t,size_t)));
	connect(this, SIGNAL(fileChanged(QString,size_t,size_t)),
			capture, SLOT(openFile(QString,size_t,size_t)));
	connect(this, SIGNAL(flipChanged(bool)),
			capture, SLOT(setFlipped(bool)));
	connect(this, SIGNAL(grayChanged(bool)),
			capture, SLOT(setGray(bool)));

	connect(this, SIGNAL(quit()),
			capture, SLOT(finish()));
	connect(this, SIGNAL(quit()),
			processor, SLOT(finish()));

}

/*
 * MainWindow destructor
 */
MainWindow::~MainWindow()
{
	delete ui;
}

/*
 * Menu action when Sources->camera 0 is selected
 * Sets capture to open device 0. If device is not available
 * menu item is set to inactive.
 */
void MainWindow::on_actionCamera_0_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	qDebug("Opening device 0 ...");
//	if (!capture->open(0, width, height))
//	{
//		qWarning("Unable to open device 0");
//		// disable menu item if camera 0 does not exist
//		ui->actionCamera_0->setDisabled(true);
//	}
	emit deviceChanged(0, width, height);
}

/*
 * Menu action when Sources->camera 1 is selected
 * Sets capture to open device 0. If device is not available
 * menu item is set to inactive
 */
void MainWindow::on_actionCamera_1_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	qDebug("Opening device 1 ...");
//	if (!capture->open(1, width, height))
//	{
//		qWarning("Unable to open device 1");
//		// disable menu item if camera 1 does not exist
//		ui->actionCamera_1->setDisabled(true);
//	}
	emit deviceChanged(1, width, height);
}

/*
 * Menu action when Sources->file is selected.
 * Opens file dialog and tries to open selected file (is not empty),
 * then sets capture to open the selected file
 */
void MainWindow::on_actionFile_triggered()
{
	int width = 0;
	int height = 0;

	if (ui->radioButtonCustomSize->isChecked())
	{
		width = preferredWidth;
		height = preferredHeight;
	}

	QString fileName =
			QFileDialog::getOpenFileName(this,
										 tr("Open Video"),
										 "./",
										 tr("Video Files (*.avi *.mkv *.mp4 *.m4v)"),
										 NULL,
										 QFileDialog::ReadOnly);

	qDebug("Opening file %s ...", fileName.toStdString().c_str());

	if (fileName.length() > 0)
	{
//		if (!capture->open(fileName))
//		{
//			qWarning("Unable to open device file : %s",
//					 fileName.toStdString().c_str());
//		}
//		// setupProcessorFromUI(); // already done from connection
		emit fileChanged(fileName, width, height);
	}
	else
	{
		qWarning("empty file name");
	}
}

/*
 * Menu action to qui application
 */
void MainWindow::on_actionQuit_triggered()
{
	emit quit();
	this->close();
}

/*
 * Menu action when flip image is selected.
 * Sets capture to change flip status which leads to reverse
 * image horizontally
 */
void MainWindow::on_actionFlip_triggered()
{
	emit flipChanged(!capture->isFlipped());
	/*
	 * There is no need to update ui->checkBoxFlip since it is connected
	 * to ui->actionFlip through signals/slots
	 */
}

/*
 * Menu action when gray image is selected.
 * Sets capture to change gray status which leads convert captured image
 * to gray or not
 */
void MainWindow::on_actionGray_triggered()
{
	bool isGray = !capture->isGray();

	emit grayChanged(isGray);
}

/*
 * Menu action when original image size is selected.
 * Sets capture not to resize image
 */
void MainWindow::on_actionOriginalSize_triggered()
{

	ui->actionConstrainedSize->setChecked(false);

	emit sizeChanged(QSize(0, 0));
}

/*
 * Menu action when constrained image size is selected.
 * Sets capture resize to preferred width and height
 */
void MainWindow::on_actionConstrainedSize_triggered()
{
	ui->actionOriginalSize->setChecked(false);

	emit sizeChanged(QSize(preferredWidth, preferredHeight));
}

/*
 * Changes widgetImage nature according to desired rendering mode.
 * Possible values for mode are:
 * 	- IMAGE: widgetImage is assigned to a QcvMatWidgetImage instance
 * 	- PIXMAP: widgetImage is assigned to a QcvMatWidgetLabel instance
 * 	- GL: widgetImage is assigned to a QcvMatWidgetGL instance
 * @param mode
 */
void MainWindow::setRenderingMode(const RenderMode mode)
{
	// Disconnect signals from slots first
	disconnect(processor, SIGNAL(updated()),
			   ui->sourceImage, SLOT(update()));
	disconnect(processor, SIGNAL(updated()),
			   ui->spectrumImage, SLOT(update()));
	disconnect(processor, SIGNAL(updated()),
			   ui->inverseImage, SLOT(update()));

	disconnect(processor, SIGNAL(squareImageChanged(Mat*)),
			   ui->sourceImage, SLOT(setSourceImage(Mat*)));
	disconnect(processor, SIGNAL(spectrumImageChanged(Mat*)),
			   ui->spectrumImage, SLOT(setSourceImage(Mat*)));
	disconnect(processor, SIGNAL(inverseImageChanged(Mat*)),
			   ui->inverseImage, SLOT(setSourceImage(Mat*)));

	QWindow * currentWindow = windowHandle();
	if (mode == RENDER_GL)
	{
		disconnect(currentWindow,
				   SIGNAL(screenChanged(QScreen *)),
				   ui->sourceImage,
				   SLOT(screenChanged()));
		disconnect(currentWindow,
				   SIGNAL(screenChanged(QScreen*)),
				   ui->spectrumImage,
				   SLOT(screenChanged()));
		disconnect(currentWindow,
				   SIGNAL(screenChanged(QScreen*)),
				   ui->inverseImage,
				   SLOT(screenChanged()));
	}

	// remove widgets in scroll areas
	QWidget * wSource = ui->scrollAreaSource->takeWidget();
	QWidget * wSpectrum = ui->scrollAreaSpectrum->takeWidget();
	QWidget * wInverse = ui->scrollAreaInverse->takeWidget();

	if ((wSource == ui->sourceImage) &&
		(wSpectrum == ui->spectrumImage) &&
		(wInverse == ui->inverseImage))
	{
		// delete removed widgets
		delete ui->sourceImage;
		delete ui->spectrumImage;
		delete ui->inverseImage;

		// create new widget
//		Mat * sourceMat = processor->getImagePtr("square");
//		Mat * spectrumMat = processor->getImagePtr("spectrum");
//		Mat * inverseMat = processor->getImagePtr("inverse");
		Mat * sourceMat = processor->getInframeSquarePtr();
		Mat * spectrumMat = processor->getSpectrumMagPtr();
		Mat * inverseMat = processor->getInverseImagePtr();

		switch (mode)
		{
			case RENDER_PIXMAP:
				ui->sourceImage = new QcvMatWidgetLabel(sourceMat);
				ui->spectrumImage = new QcvMatWidgetLabel(spectrumMat);
				ui->inverseImage = new QcvMatWidgetLabel(inverseMat);
				break;
			case RENDER_GL:
				ui->sourceImage = new QcvMatWidgetGL(sourceMat);
				ui->spectrumImage = new QcvMatWidgetGL(spectrumMat);
				ui->inverseImage = new QcvMatWidgetGL(inverseMat);
				break;
			case RENDER_IMAGE:
			default:
				ui->sourceImage = new QcvMatWidgetImage(sourceMat);
				ui->spectrumImage = new QcvMatWidgetImage(spectrumMat);
				ui->inverseImage = new QcvMatWidgetImage(inverseMat);
				break;
		}

		if ((ui->sourceImage != NULL) &&
			(ui->spectrumImage != NULL) &&
			(ui->inverseImage != NULL))
		{
			// Name the new images widgets with same name as in UI files
			 ui->sourceImage->setObjectName(QString::fromUtf8("sourceImage"));
			 ui->spectrumImage->setObjectName(QString::fromUtf8("spectrumImage"));
			 ui->inverseImage->setObjectName(QString::fromUtf8("inverseImage"));

			// add to scroll areas
			ui->scrollAreaSource->setWidget(ui->sourceImage);
			ui->scrollAreaSpectrum->setWidget(ui->spectrumImage);
			ui->scrollAreaInverse->setWidget(ui->inverseImage);

			// Reconnect signals to slots
			connect(processor, SIGNAL(updated()),
					ui->sourceImage, SLOT(update()));
			connect(processor, SIGNAL(updated()),
					ui->spectrumImage, SLOT(update()));
			connect(processor, SIGNAL(updated()),
					ui->inverseImage, SLOT(update()));

			connect(processor, SIGNAL(squareImageChanged(Mat*)),
					ui->sourceImage, SLOT(setSourceImage(Mat*)));
			connect(processor, SIGNAL(spectrumImageChanged(Mat*)),
					ui->spectrumImage, SLOT(setSourceImage(Mat*)));
			connect(processor, SIGNAL(inverseImageChanged(Mat*)),
					ui->inverseImage, SLOT(setSourceImage(Mat*)));

			if (mode == RENDER_GL)
			{
				connect(currentWindow,
						SIGNAL(screenChanged(QScreen *)),
						ui->sourceImage,
						SLOT(screenChanged()));
				connect(currentWindow,
						SIGNAL(screenChanged(QScreen *)),
						ui->spectrumImage,
						SLOT(screenChanged()));
				connect(currentWindow,
						SIGNAL(screenChanged(QScreen *)),
						ui->inverseImage,
						SLOT(screenChanged()));
			}

			// Sends message to status bar and sets menu checks
			message.clear();
			message.append(tr("Render more set to "));
			switch (mode)
			{
				case RENDER_IMAGE:
					ui->actionRenderPixmap->setChecked(false);
					ui->actionRenderOpenGL->setChecked(false);
					message.append(tr("QImage"));
					break;
				case RENDER_PIXMAP:
					ui->actionRenderImage->setChecked(false);
					ui->actionRenderOpenGL->setChecked(false);
					message.append(tr("QPixmap in QLabel"));
					break;
				case RENDER_GL:
					ui->actionRenderImage->setChecked(false);
					ui->actionRenderPixmap->setChecked(false);
					message.append(tr("QGLWidget"));
					break;
				default:
				break;
			}
			emit sendMessage(message, 5000);
		}
		else
		{
			qDebug("MainWindow::on_actionRenderXXX some new widget is null");
		}
	}
	else
	{
		qDebug("MainWindow::on_actionRenderXXX removed widget is not in ui->");
	}
}

/*
 * Re setup processor from UI settings when source changes
 */
void MainWindow::setupProcessorFromUI()
{
	processor->setLogScaleFactor((double)ui->spinBoxMag->value());

	if (ui->radioButtonFilterBox->isChecked())
	{
		processor->setFilterType(CvDFT::BOX_FILTER);
	}
	else if (ui->radioButtonFilterGauss->isChecked())
	{
		processor->setFilterType(CvDFT::GAUSS_FILTER);
	}
	else
	{
		processor->setFilterType(CvDFT::SINC_FILTER);
	}

	processor->setFiltering(ui->checkBoxFiltering->isChecked());

	processor->setLowPassFilter(CvDFT::BLUE, ui->spinBoxBlueLP->value());
	processor->setLowPassFilter(CvDFT::GREEN, ui->spinBoxGreenLP->value());
	processor->setLowPassFilter(CvDFT::RED, ui->spinBoxRedLP->value());

	processor->setHighPassFilter(CvDFT::BLUE, ui->spinBoxBlueHP->value());
	processor->setHighPassFilter(CvDFT::GREEN, ui->spinBoxGreenHP->value());
	processor->setHighPassFilter(CvDFT::RED, ui->spinBoxRedHP->value());
}

/*
 * Reset Mean Frame rate when somthing changes on capture
 */
void MainWindow::resetMeanFrameRate()
{
	meanFrameRate.reset();
}


/*
 * Update Capture's frame rate and delay labels
 * @param value the new capture frame rate
 */
void MainWindow::onCaptureFrameRate_changed(const double value)
{
	double frameRate = meanFrameRate.mean();
	if (fabs(frameRate - value) > 15.0) // Drastic change in framerate
	{
		meanFrameRate.reset();
	}
	meanFrameRate += value;

	QString frameRateString;
	QString delayString;
	frameRate = meanFrameRate.mean();
	const int delay = static_cast<int>(round(1000.0 / frameRate));

	delayString.sprintf("%02d", delay);
	frameRateString.sprintf("%5.2f", frameRate);

	ui->labelFrameRate->setText(frameRateString);
	ui->labelDelay->setText(delayString);
}

/*
 * Update Processing time(s) label(s)
 * @param time the new processing time
 */
void MainWindow::onProcessingTime_changed(const CvProcessor::ProcessTime * time)
{
	QString processTimeString;
	QString processRateString;
	double processTime = time->mean() / 1000.0;
	double processRate = 1000.0 / processTime;
	processTimeString.sprintf("%02d", static_cast<int>(round(processTime)));
	processRateString.sprintf("%5.2f", processRate);

	ui->labelProcessingTime->setText(processTimeString);
	ui->labelProcessingRate->setText(processRateString);
	if (processRate < meanFrameRate.mean())
	{
		ui->labelProcessingRate->setStyleSheet(QStringLiteral("color: rgb(255, 0, 0);"));
	}
	else
	{
		ui->labelProcessingRate->setStyleSheet(QStringLiteral("color: rgb(0, 128, 0);"));
	}
}

/*
 * Set filters spinBoxes and sliders link state
 * @param linked the link status
 * @post When link is on all sliders/spinboxes of low pass and high pass
 * filters are linked together, moving/changing one changes the others.
 * When link os off sliders/spinboxes are not linked together
 */
//void MainWindow::setLinkedFilterSizes(bool linked)
//{
//	if (linked)
//	{
//		qDebug("Linking Sliders together");
//		// check all link checkboxes since we don't know which one lead here
//		ui->checkBoxLinkLP->setChecked(true);
//		ui->checkBoxLinkHP->setChecked(true);

//		// set blue/green values to red value
//		int redLPValue = ui->spinBoxRedLP->value();
//		ui->spinBoxGreenLP->setValue(redLPValue);
//		ui->spinBoxBlueLP->setValue(redLPValue);
//		int redHPValue = ui->spinBoxRedHP->value();
//		ui->spinBoxGreenHP->setValue(redHPValue);
//		ui->spinBoxBlueHP->setValue(redHPValue);

//		// link all filter sizes sliders and spinboxes together
//		// only spinboxes are affected since sliders and spinboxes
//		// arer already connected together

//		// red -> green
//		connect(ui->spinBoxRedLP, SIGNAL(valueChanged(int)),
//				ui->spinBoxGreenLP, SLOT(setValue(int)));
//		connect(ui->spinBoxRedHP, SIGNAL(valueChanged(int)),
//				ui->spinBoxGreenHP, SLOT(setValue(int)));

//		// red -> blue
//		connect(ui->spinBoxRedLP, SIGNAL(valueChanged(int)),
//				ui->spinBoxBlueLP, SLOT(setValue(int)));
//		connect(ui->spinBoxRedHP, SIGNAL(valueChanged(int)),
//				ui->spinBoxBlueHP, SLOT(setValue(int)));

//		// green -> red
//		connect(ui->spinBoxGreenLP, SIGNAL(valueChanged(int)),
//				ui->spinBoxRedLP, SLOT(setValue(int)));
//		connect(ui->spinBoxGreenHP, SIGNAL(valueChanged(int)),
//				ui->spinBoxRedHP, SLOT(setValue(int)));

//		// green -> blue
//		connect(ui->spinBoxGreenLP, SIGNAL(valueChanged(int)),
//				ui->spinBoxBlueLP, SLOT(setValue(int)));
//		connect(ui->spinBoxGreenHP, SIGNAL(valueChanged(int)),
//				ui->spinBoxBlueHP, SLOT(setValue(int)));

//		// blue -> red
//		connect(ui->spinBoxBlueLP, SIGNAL(valueChanged(int)),
//				ui->spinBoxRedLP, SLOT(setValue(int)));
//		connect(ui->spinBoxBlueHP, SIGNAL(valueChanged(int)),
//				ui->spinBoxRedHP, SLOT(setValue(int)));

//		// blue -> green
//		connect(ui->spinBoxBlueLP, SIGNAL(valueChanged(int)),
//				ui->spinBoxGreenLP, SLOT(setValue(int)));
//		connect(ui->spinBoxBlueHP, SIGNAL(valueChanged(int)),
//				ui->spinBoxGreenHP, SLOT(setValue(int)));
//	}
//	else
//	{
//		qDebug("Unlink sliders from each other");
//		// uncheck all link checkboxes since we don't know which one lead here
//		ui->checkBoxLinkLP->setChecked(false);
//		ui->checkBoxLinkHP->setChecked(false);

//		// unlink all filter sizes sliders and spinboxes from each other
//		// only spinboxes are affected since sliders and spinboxes
//		// arer already connected together

//		// red -> green
//		disconnect(ui->spinBoxRedLP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxGreenLP, SLOT(setValue(int)));
//		disconnect(ui->spinBoxRedHP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxGreenHP, SLOT(setValue(int)));

//		// red -> blue
//		disconnect(ui->spinBoxRedLP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxBlueLP, SLOT(setValue(int)));
//		disconnect(ui->spinBoxRedHP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxBlueHP, SLOT(setValue(int)));

//		// green -> red
//		disconnect(ui->spinBoxGreenLP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxRedLP, SLOT(setValue(int)));
//		disconnect(ui->spinBoxGreenHP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxRedHP, SLOT(setValue(int)));

//		// green -> blue
//		disconnect(ui->spinBoxGreenLP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxBlueLP, SLOT(setValue(int)));
//		disconnect(ui->spinBoxGreenHP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxBlueHP, SLOT(setValue(int)));

//		// blue -> red
//		disconnect(ui->spinBoxBlueLP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxRedLP, SLOT(setValue(int)));
//		disconnect(ui->spinBoxBlueHP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxRedHP, SLOT(setValue(int)));

//		// blue -> green
//		disconnect(ui->spinBoxBlueLP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxGreenLP, SLOT(setValue(int)));
//		disconnect(ui->spinBoxBlueHP, SIGNAL(valueChanged(int)),
//				   ui->spinBoxGreenHP, SLOT(setValue(int)));
//	}
//}

/*
 * Setup UI elements according to processor's state
 */
void MainWindow::setupUIFromProcessor()
{
	/*
	 * Please setup enum controllers before boolean controllers when using
	 * actions from the same menu: some non checkable actions setup in boolean
	 * controllers can become checkable and then can be misinterpreted by enum
	 * controllers
	 */
	QEnumController * eController = processor->getFilterTypeController();
	eController->setupRadioButtons(ui->groupBoxFilter);
	eController->setupMenu(ui->menuFilterTypes);

	QBoolController * bController = processor->getFilteringController();
	bController->setupAbstractButton(ui->checkBoxFiltering);
	bController->setupAction(ui->actionFiltering);

	QRangeDoubleController * rdController = processor->getLogScaleController();
	rdController->setupSpinBox(ui->spinBoxMag);

	QSlider * lowPassSlider[3] =
	{
		ui->horizontalSliderBLP,
		ui->horizontalSliderGLP,
		ui->horizontalSliderRLP
	};

	QSlider * highPassSlider[3] =
	{
		ui->horizontalSliderBHP,
		ui->horizontalSliderGHP,
		ui->horizontalSliderRHP
	};

	QSpinBox * lowPassSpinner[3] =
	{
		ui->spinBoxBlueLP,
		ui->spinBoxGreenLP,
		ui->spinBoxRedLP
	};

	QSpinBox * highPassSpinner[3] =
	{
		ui->spinBoxBlueHP,
		ui->spinBoxGreenHP,
		ui->spinBoxRedHP
	};

	QRangeIntController * riController = nullptr;

	for (size_t i = 0; i < 3; i++)
	{
		riController = processor->getLowPassFilterController(i);
		riController->setupSlider(lowPassSlider[i]);
		riController->setupSpinBox(lowPassSpinner[i]);

		riController = processor->getHighPassFilterController(i);
		riController->setupSlider(highPassSlider[i]);
		riController->setupSpinBox(highPassSpinner[i]);
	}

	bController = processor->getLinkFiltersController();
	bController->setupAbstractButton(ui->checkBoxLinkLP);
	bController->setupAbstractButton(ui->checkBoxLinkHP);
	bController->setupAction(ui->actionLink_Filters);

	// ------------------------------------------------------------------------
	// Processor's Signal connections to Slots
	// ------------------------------------------------------------------------
	// processor->sendText --> labelFFTSizeValue->setText when source image
	// changes, fft size might also change
	ui->labelFFTSizeValue->setNum(processor->getOptimalDftSize());
	connect(processor, SIGNAL(optimalDFTSizeChanged(int)),
			ui->labelFFTSizeValue, SLOT(setNum(int)));

	connect(processor, SIGNAL(sendMessage(QString,int)),
			ui->statusBar, SLOT(showMessage(QString,int)));


	// When Processor source image changes, some attributes are reinitialised
	// So we have to set them up again according to current UI values
	connect(processor, SIGNAL(imageChanged()),
			this, SLOT(setupProcessorFromUI()));

	connect(processor, SIGNAL(processTimeUpdated(const CvProcessor::ProcessTime*)),
			this, SLOT(onProcessingTime_changed(const CvProcessor::ProcessTime*)));
}

/*
 * Indicates if a number is a power of 2
 * @param number the number to check
 * @return true if this number is a power of 2, false otherwise
 */
bool MainWindow::isPowerOf2(const int number)
{
	return (number != 0) && ((number & (number - 1)) == 0);
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetImage instance.
 */
void MainWindow::on_actionRenderImage_triggered()
{
	qDebug("Setting image rendering to: images");
	setRenderingMode(RENDER_IMAGE);
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetLabel with pixmap instance.
 */
void MainWindow::on_actionRenderPixmap_triggered()
{
	qDebug("Setting image rendering to: pixmaps");
	setRenderingMode(RENDER_PIXMAP);
}

/*
 * Menu action to replace current image rendering widget by a
 * QcvMatWidgetGL instance.
 */
void MainWindow::on_actionRenderOpenGL_triggered()
{
	qDebug("Setting image rendering to: opengl");
	setRenderingMode(RENDER_GL);
}

/*
 * Original size radioButton action.
 * Sets capture resize to off
 */
void MainWindow::on_radioButtonOrigSize_clicked()
{
	ui->actionConstrainedSize->setChecked(false);
	emit sizeChanged(QSize(0, 0));
}

/*
 * Custom size radioButton action.
 * Sets capture resize to preferred width and height
 */
void MainWindow::on_radioButtonCustomSize_clicked()
{
	ui->actionOriginalSize->setChecked(false);
	emit sizeChanged(QSize(preferredWidth, preferredHeight));
}

/*
 * Width spinbox value change.
 * Changes the preferred width and if custom size is selected apply
 * this custom width
 * @param value the desired width
 */
void MainWindow::on_spinBoxWidth_valueChanged(int value)
{
//	qDebug() << Q_FUNC_INFO << "(" << value << ")";
	preferredWidth = value;
	if (keepAspectRatio)
	{
		// apply aspect ratio to height
		int newHeight = static_cast<int>(round(preferredWidth / preferredAspectRatio));
		if (newHeight != preferredHeight)
		{
			preferredHeight = newHeight;
			this->blockSignals(true);
			ui->spinBoxHeight->blockSignals(true);
			on_spinBoxHeight_valueChanged(preferredHeight);
			ui->spinBoxHeight->setValue(preferredHeight);
			ui->spinBoxHeight->blockSignals(false);
			this->blockSignals(false);
			// setValue emits a valueChanged which will trigger on_spinBoxHeight_valueChanged
		}
	}
	else
	{
		// recalculates aspect ratio
		preferredAspectRatio = preferredWidth / (double) preferredHeight;
	}
	if (isPowerOf2(preferredWidth))
	{
		// Apply greenish background
		 ui->spinBoxWidth->setStyleSheet(QStringLiteral("background-color: rgb(224, 255, 224);"));
	}
	else
	{
		// Revert to default style
		ui->spinBoxWidth->setStyleSheet(styleSheet());
	}

	if (ui->radioButtonCustomSize->isChecked())
	{
		emit sizeChanged(QSize(preferredWidth, preferredHeight));
	}
}

/*
 * Height spinbox value change.
 * Changes the preferred height and if custom size is selected apply
 * this custom height
 * @param value the desired height
 */
void MainWindow::on_spinBoxHeight_valueChanged(int value)
{
//	qDebug() << Q_FUNC_INFO << "(" << value << ")";
	preferredHeight = value;

	if (keepAspectRatio)
	{
		// apply aspect ratio to width
		int newWidth = static_cast<int>(round(preferredAspectRatio * double(preferredHeight)));
		if (newWidth != preferredWidth)
		{
			preferredWidth = newWidth;
			ui->spinBoxWidth->blockSignals(true);
			this->blockSignals(true);
			on_spinBoxWidth_valueChanged(preferredWidth);
			ui->spinBoxWidth->setValue(preferredWidth);
			this->blockSignals(false);
			ui->spinBoxWidth->blockSignals(false);
			// setValue emits a valueChanged which will trigger on_spinBoxWidth_valueChanged
		}
	}
	else
	{
		// recalculates aspect ratio
		preferredAspectRatio = preferredWidth / (double) preferredHeight;
	}

	if (isPowerOf2(preferredHeight))
	{
		// Apply greenish background
		 ui->spinBoxHeight->setStyleSheet(QStringLiteral("background-color: rgb(224, 255, 224);"));
	}
	else
	{
		// Revert to default style
		ui->spinBoxHeight->setStyleSheet(styleSheet());
	}

	if (ui->radioButtonCustomSize->isChecked())
	{
		emit sizeChanged(QSize(preferredWidth, preferredHeight));
	}
}

/*
 * Flip capture image horizontally.
 * changes capture flip status
 */
void MainWindow::on_checkBoxFlip_clicked()
{
	/*
	 * There is no need to update ui->actionFlip since it is connected
	 * to ui->checkBoxFlip through signals/slots
	 */
	emit flipChanged(ui->checkBoxFlip->isChecked());
}

/*
 * convert capture image to gray level.
 * changes cpature gray conversion status
 */
void MainWindow::on_checkBoxGray_clicked()
{
	bool isGray = ui->checkBoxGray->isChecked();
	emit grayChanged(isGray);
}

/*
 * Preserve (or not) image aspect ratio.
 * When aspect ratio is preserved a modification of width or height
 * also causes a modification of height or width to preserve th current
 * aspect ratio
 * @param checked flag to preserve or not image aspect ratio
 */
void MainWindow::on_checkBoxAR_clicked(bool checked)
{
	keepAspectRatio = checked;
}

/*
 * Activates Next Tab
 */
void MainWindow::on_actionSwitchTab_triggered()
{
	int currentTabIndex = ui->tabWidgetFilters->currentIndex();
	int numberOfTabs = ui->tabWidgetFilters->count();
	ui->tabWidgetFilters->setCurrentIndex(++currentTabIndex % numberOfTabs);
}
